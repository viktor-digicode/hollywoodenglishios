//
//  LevelObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LevelObject+CoreDataProperties.h"

@implementation LevelObject (CoreDataProperties)

@dynamic level_description;
@dynamic level_id;
@dynamic level_name;
@dynamic level_order;
@dynamic level_ranks;

@end
