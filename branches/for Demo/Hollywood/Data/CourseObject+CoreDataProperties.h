//
//  CourseObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CourseObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface CourseObject (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *course_description;
@property (nullable, nonatomic, retain) NSString *course_id;
@property (nullable, nonatomic, retain) NSString *course_name;
@property (nullable, nonatomic, retain) NSString *course_source_language;
@property (nullable, nonatomic, retain) NSSet<LevelObject *> *course_levels;

@end

@interface CourseObject (CoreDataGeneratedAccessors)

- (void)addCourse_levelsObject:(LevelObject *)value;
- (void)removeCourse_levelsObject:(LevelObject *)value;
- (void)addCourse_levels:(NSSet<LevelObject *> *)values;
- (void)removeCourse_levels:(NSSet<LevelObject *> *)values;

@end

NS_ASSUME_NONNULL_END
