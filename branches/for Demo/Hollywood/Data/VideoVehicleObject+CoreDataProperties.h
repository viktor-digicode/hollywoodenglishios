//
//  VideoVehicleObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VideoVehicleObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface VideoVehicleObject (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *text;
@property (nullable, nonatomic, retain) NSString *video;
@property (nullable, nonatomic, retain) NSString *videoPreview;

@end

NS_ASSUME_NONNULL_END
