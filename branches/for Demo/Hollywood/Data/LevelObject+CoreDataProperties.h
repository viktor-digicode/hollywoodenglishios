//
//  LevelObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LevelObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface LevelObject (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *level_description;
@property (nullable, nonatomic, retain) NSNumber *level_id;
@property (nullable, nonatomic, retain) NSString *level_name;
@property (nullable, nonatomic, retain) NSNumber *level_order;
@property (nullable, nonatomic, retain) NSSet<RankObject *> *level_ranks;

@end

@interface LevelObject (CoreDataGeneratedAccessors)

- (void)addLevel_ranksObject:(RankObject *)value;
- (void)removeLevel_ranksObject:(RankObject *)value;
- (void)addLevel_ranks:(NSSet<RankObject *> *)values;
- (void)removeLevel_ranks:(NSSet<RankObject *> *)values;

@end

NS_ASSUME_NONNULL_END
