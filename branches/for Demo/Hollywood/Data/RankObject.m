//
//  RankObject.m
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "RankObject.h"
#import "LessonObject.h"

@implementation RankObject

- (void) setData:(NSDictionary*)dataDictionary{
    if([dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.rank_name = [self objectOrNilForKey:kLevelRanksRankName fromDictionary:dataDictionary];
        self.rank_lesson_Background = [self objectOrNilForKey:kLevelRanksRankLessonBackground fromDictionary: dataDictionary];
        self.rank_icon = [self objectOrNilForKey:kLevelRanksRankIcon fromDictionary: dataDictionary];
        self.rank_number = [NSNumber numberWithInteger:[[self objectOrNilForKey:kLevelRanksRankNumber fromDictionary:dataDictionary]integerValue]];
        self.rank_teaser_text = [self objectOrNilForKey:kLevelRanksRankTeaserText fromDictionary: dataDictionary];
        self.rank_diploma_design = [NSNumber numberWithInteger:[[self objectOrNilForKey:kLevelRanksRankDiplomaDesign fromDictionary:dataDictionary]integerValue]];
        self.rank_id = [NSNumber numberWithInteger:[[self objectOrNilForKey:kLevelRanksRankId fromDictionary:dataDictionary]integerValue]];
        self.rank_achieved_audio = [self objectOrNilForKey:kLevelRanksRankAchievedAudio fromDictionary: dataDictionary];
        self.rank_rank_achieved_text = [self objectOrNilForKey:kLevelRanksRankRankAchievedText fromDictionary: dataDictionary];
        
        NSObject *receivedRankLessons = [ dataDictionary  objectForKey:kLevelRanksRankLessons];
        //NSMutableArray *parsedRankLessons = [NSMutableArray array];
        if ([receivedRankLessons isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedRankLessons) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    LessonObject *importedObject = [LessonObject MR_createEntity];
                    [importedObject setData:item];
                    //[parsedRankLessons addObject:importedObject];
                    [self addRank_lessonsObject:importedObject];
                }
            }
        } else if ([receivedRankLessons isKindOfClass:[NSDictionary class]]) {
            LessonObject *importedObject = [LessonObject MR_createEntity];
            [importedObject setData:(NSDictionary *)receivedRankLessons];
            [self addRank_lessonsObject:importedObject];
            //[parsedRankLessons addObject:importedObject];
        }
        
       /* NSSet *rankLessonsSet = [[NSSet alloc] init];
        [rankLessonsSet setByAddingObjectsFromArray:[NSArray arrayWithArray:parsedRankLessons]];
        self.rank_lessons = rankLessonsSet;
        */
    
        
    }

}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}
@end
