//
//  RankObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RankObject+CoreDataProperties.h"

@implementation RankObject (CoreDataProperties)

@dynamic rank_achieved_audio;
@dynamic rank_diploma_design;
@dynamic rank_icon;
@dynamic rank_id;
@dynamic rank_lesson_Background;
@dynamic rank_name;
@dynamic rank_number;
@dynamic rank_rank_achieved_text;
@dynamic rank_teaser_text;
@dynamic rank_lessons;

@end
