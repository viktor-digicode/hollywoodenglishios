//
//  BaseVehiclesObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BaseVehiclesObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface BaseVehiclesObject (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *instanceID;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *order;
@property (nullable, nonatomic, retain) NSNumber *showInstruction;
@property (nullable, nonatomic, retain) NSString *title;
@property (nullable, nonatomic, retain) NSNumber *type;
@property (nullable, nonatomic, retain) NSNumber *unitID;

@end

NS_ASSUME_NONNULL_END
