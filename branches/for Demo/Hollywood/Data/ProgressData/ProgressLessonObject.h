//
//  ProgressLessonObject.h
//  Hollywood
//
//  Created by Kiril Kiroski on 8/10/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ProgressVehicleObject;

NS_ASSUME_NONNULL_BEGIN

@interface ProgressLessonObject : NSManagedObject

- (void) setData:(NSDictionary*)dataDictionary;
- (void) setInitialData;

- (ProgressVehicleObject*)getCurrentProgressVehicleObject;

@end

NS_ASSUME_NONNULL_END

#import "ProgressLessonObject+CoreDataProperties.h"
