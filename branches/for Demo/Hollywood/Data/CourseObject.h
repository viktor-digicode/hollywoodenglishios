//
//  CourseObject.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class LevelObject;

NS_ASSUME_NONNULL_BEGIN

@interface CourseObject : NSManagedObject

- (void) setData:(NSDictionary*)dataDictionary;
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END

#import "CourseObject+CoreDataProperties.h"
