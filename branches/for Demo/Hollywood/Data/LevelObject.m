//
//  LevelObject.m
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "LevelObject.h"
#import "RankObject.h"


@interface LevelObject ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation LevelObject


- (void) setData:(NSDictionary*)dataDictionary{
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if([dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.level_id = [NSNumber numberWithInteger:[[self objectOrNilForKey:kCourseLevelsLevelId fromDictionary:dataDictionary]integerValue]];
        self.level_name = [self objectOrNilForKey:kCourseLevelsLevelName fromDictionary:dataDictionary];

        self.level_description = [self objectOrNilForKey:kCourseLevelsLevelDescription fromDictionary:dataDictionary];
        self.level_order = [NSNumber numberWithInteger:[[self objectOrNilForKey:kCourseLevelsLevelOrder fromDictionary:dataDictionary]integerValue]];
            
        NSObject *receivedLevelRanks = [dataDictionary objectForKey:kCourseLevelsLevelRanks];
        //NSMutableArray *parsedLevelRanks = [NSMutableArray array];
        if ([receivedLevelRanks isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLevelRanks) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    RankObject *importedObject = [RankObject MR_createEntity];
                    [importedObject setData:item];
                    [self addLevel_ranksObject:importedObject];
                    //[parsedLevelRanks addObject:importedObject];
                }
            }
        } else if ([receivedLevelRanks isKindOfClass:[NSDictionary class]]) {
            RankObject *importedObject = [RankObject MR_createEntity];
            [importedObject setData:(NSDictionary *)receivedLevelRanks];
            [self addLevel_ranksObject:importedObject];
            //[parsedLevelRanks addObject:importedObject];
        }
        //NSSet *levelRanksSet = [[NSSet alloc] init];
        //[levelRanksSet setByAddingObjectsFromArray:[NSArray arrayWithArray:parsedLevelRanks]];
        //self.level_ranks = levelRanksSet;
        
        
    }
}

- (NSSet*)takeRangs{
    return self.level_ranks;
}
#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}
@end
