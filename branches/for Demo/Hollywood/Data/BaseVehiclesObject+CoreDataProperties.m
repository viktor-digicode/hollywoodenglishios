//
//  BaseVehiclesObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BaseVehiclesObject+CoreDataProperties.h"

@implementation BaseVehiclesObject (CoreDataProperties)

@dynamic instanceID;
@dynamic name;
@dynamic order;
@dynamic showInstruction;
@dynamic title;
@dynamic type;
@dynamic unitID;

@end
