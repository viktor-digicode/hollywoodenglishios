//
//  CourseObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CourseObject+CoreDataProperties.h"

@implementation CourseObject (CoreDataProperties)

@dynamic course_description;
@dynamic course_id;
@dynamic course_name;
@dynamic course_source_language;
@dynamic course_levels;

@end
