//
//  RankObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RankObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface RankObject (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *rank_achieved_audio;
@property (nullable, nonatomic, retain) NSNumber *rank_diploma_design;
@property (nullable, nonatomic, retain) NSString *rank_icon;
@property (nullable, nonatomic, retain) NSNumber *rank_id;
@property (nullable, nonatomic, retain) NSString *rank_lesson_Background;
@property (nullable, nonatomic, retain) NSString *rank_name;
@property (nullable, nonatomic, retain) NSNumber *rank_number;
@property (nullable, nonatomic, retain) NSString *rank_rank_achieved_text;
@property (nullable, nonatomic, retain) NSString *rank_teaser_text;
@property (nullable, nonatomic, retain) NSSet<LessonObject *> *rank_lessons;

@end

@interface RankObject (CoreDataGeneratedAccessors)

- (void)addRank_lessonsObject:(LessonObject *)value;
- (void)removeRank_lessonsObject:(LessonObject *)value;
- (void)addRank_lessons:(NSSet<LessonObject *> *)values;
- (void)removeRank_lessons:(NSSet<LessonObject *> *)values;

@end

NS_ASSUME_NONNULL_END
