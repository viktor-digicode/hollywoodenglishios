//
//  QuizInstanceAnswer+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 2/6/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "QuizInstanceAnswer+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface QuizInstanceAnswer (CoreDataProperties)

+ (NSFetchRequest<QuizInstanceAnswer *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *answerId;
@property (nullable, nonatomic, copy) NSString *answerText;
@property (nullable, nonatomic, copy) NSString *imageName;
@property (nullable, nonatomic, copy) NSString *imageUrl;
@property (nonatomic) BOOL isCorrect;

@end

NS_ASSUME_NONNULL_END
