//
//  RankObject.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class LessonObject;

NS_ASSUME_NONNULL_BEGIN

@interface RankObject : NSManagedObject

- (void) setData:(NSDictionary*)dataDictionary;

@end

NS_ASSUME_NONNULL_END

#import "RankObject+CoreDataProperties.h"
