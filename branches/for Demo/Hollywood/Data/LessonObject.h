//
//  LessonObject.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseVehiclesObject.h"

@class BaseVehiclesObject;

NS_ASSUME_NONNULL_BEGIN

@interface LessonObject : NSManagedObject

- (void) setData:(NSDictionary*)dataDictionary;

@end

NS_ASSUME_NONNULL_END

#import "LessonObject+CoreDataProperties.h"
