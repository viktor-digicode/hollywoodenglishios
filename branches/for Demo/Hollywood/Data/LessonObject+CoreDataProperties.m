//
//  LessonObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LessonObject+CoreDataProperties.h"

@implementation LessonObject (CoreDataProperties)

@dynamic initial;
@dynamic lesson_description;
@dynamic lesson_home_page_image;
@dynamic lesson_id;
@dynamic lesson_name;
@dynamic lesson_teaser;
@dynamic lesson_vehicles;

@end
