//
//  AppDelegate.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/24/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#define APP_DELEGATE ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(strong, nonatomic) UINavigationController *navigationController;
@property(weak, nonatomic) LMBaseViewController *baseController;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (void)buildLessonStack;
- (void)buildLessonStackContinue:(BOOL)shouldContinue;
- (void)buildHomePageStack;
- (void)buildUserZoneStack;
- (void)buildVipZoneStack;
- (void)buildVipQuizzesStack;
- (void)buildUserStatusSelectionStack;

@end

