//
//  Constants.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/24/16.
//  Copyright © 2016 aa. All rights reserved.
//



/* WIDTH  &  HEIGHT*/
#define LM_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define LM_HEIGHT ([UIScreen mainScreen].bounds.size.height)

#define DEVICE_BIGER_SIDE (LM_WIDTH >LM_HEIGHT ? LM_WIDTH : LM_HEIGHT)
#define DEVICE_SMALL_SIDE (LM_WIDTH >LM_HEIGHT ? LM_HEIGHT : LM_WIDTH)

#define SCENE_SIZE_HEIGHT (DEVICE_BIGER_SIDE >= 568? 568:DEVICE_BIGER_SIDE)
#define SCENE_SIZE_WIDTH 320

#define INSTANCE_START_POSITION_X 44
#define SPACE_QUESTIONS_ANSWERS 20

/*data*/
#define SEARCH_TERM @"title"
#define SEARCH_PREDICATE @"course_name contains[c] %@" //@"title contains[c] %@"
#define LEVEL_SEARCH_PREDICATE @"level_name contains[c] %@" //@"title contains[c] %@"

#define kCourseObjectCourseSourceLanguage   @"course_source_language"
#define kCourseObjectCourseName   @"course_name"
#define kCourseObjectCourseId   @"course_id"
#define kCourseObjectCourseLevels   @"course_levels"
#define kCourseObjectCourseDescription   @"course_description "

#define kCourseLevelsLevelId @"level_id"
#define kCourseLevelsLevelDescription @"level_description"
#define kCourseLevelsLevelOrder @"level_order"
#define kCourseLevelsLevelRanks @"level_ranks"
#define kCourseLevelsLevelName @"level_name"

#define kLevelRanksRankName @"rank_name" 
#define kLevelRanksRankLessons @"rank_lessons" 
#define kLevelRanksRankLessonBackground @"rank_lesson_Background" 
#define kLevelRanksRankIcon @"rank_icon" 
#define kLevelRanksRankNumber @"rank_number" 
#define kLevelRanksRankTeaserText @"rank_teaser_text" 
#define kLevelRanksRankDiplomaDesign @"rank_diploma_design" 
#define kLevelRanksRankId @"rank_id" 
#define kLevelRanksRankAchievedAudio @"rank_achieved_audio" 
#define kLevelRanksRankRankAchievedText @"rank_rank_achieved_text" 

#define kRankLessonsLessonHomePageImage @"lesson_home_page_image" 
#define kRankLessonsLessonId @"lesson_id" 
#define kRankLessonsLessonTeaser @"lesson_teaser" 
#define kRankLessonsLessonVehicles @"lesson_vehicles" 
#define kRankLessonsLessonDescription @"lesson_description" 
#define kRankLessonsLessonName @"lesson_name" 
#define kRankLessonsInitial @"initial"


#define kProgressMaxLevel @"maxLevel"
#define kProgressCurrentLevel @"currentLevel"
#define kProgressScore @"score"
#define kProgressVipPasses @"vipPasses"
#define kProgressRank @"rank"

#define kBaseClassVipCourse  @"vip_course"
#define kVipCourseVipCategories  @"vip_categories"
#define kVipCategoriesCategoryId  @"category_id"
#define kVipCategoriesImageUrl  @"image_url"
#define kVipCategoriesCategoryName  @"category_name"
#define kVipCategoriesQuizList  @"quiz_list"
#define kVipCategoriesDisplayOrder  @"display_order"
#define kVipCategoriesFirstQuizInstanceList  @"first_quiz_Instance_list"
#define kVipCategoriesImageName  @"image_name"
#define kVipCategoriesAvailability  @"availability"

#define kQuizListDescription @"description" 
#define kQuizListOrderInCategory @"order_in_category" 
#define kQuizListQuizId @"quiz_id" 
#define kQuizListQuizName @"quiz_name" 
#define kQuizListAvEndDate @"av_end_date" 
#define kQuizListImageUrl @"image_url" 
#define kQuizListPrice @"price" 
#define kQuizListAvailability @"availability" 
#define kQuizListAvStartDate @"av_start_date" 
#define kQuizListIsPromo @"is_promo" 
#define kQuizListTeaser @"teaser" 
#define kQuizListImageName @"image_name" 
#define kQuizListInstanceList @"instance_list" 


#define kWordPattern @"\\<[[a-zA-Z0-9\\w\\s\\'’]'+]*\\>"
#define kUnderlineWord @"__________"

/*Colors*/
#define APP_GOLD_COLOR [UIColor colorWithRed:138.0/255.0 green:128.0/255.0 blue:72.0/255.0 alpha:1.0]
#define APP_GOLD_COLOR_NEW [UIColor colorWithRed:112.0/255.0 green:102.0/255.0 blue:55.0/255.0 alpha:1.0]
#define APP_CORRECT_COLOR color(67, 170, 0, 1.0)
#define APP_INCORRECT_COLOR color(255, 4, 0, 1.0)

/* Singletons */

#define AUDIO_SINGLETON 1
#define USER_SINGLETON 1
#define ANALYTICS_SINGLETON 1

/* Sounds */

#define kSoundTypeBackground @"SFX_Background"
#define kSoundTypeButtonTap @"SFX_ButtonTap"
#define kSoundTypeSwipe @"SFX_Swipe"
#define kSoundTypeError @"SFX_Error"
#define kSoundTypeSuccess @"SFX_Success"

#define kSoundTypeResults @"ButtonClickFX"
#define kSoundTypeDeletePicture @"PicDeletedFX"
#define kSoundTypeTakePicture @"TakePicFX"

#define kSoundFileType @"mp3"


#define kUserMSISDN       @"msisdn"
#define kUserCode         @"code"
#define kUserDeviceId     @"device_id"
#define kSearchWords      @"words"
#define kPreferencesCheckedList @"CheckedList"
#define kPreferencesflag @"flag"
#define kWord      @"word"

#define kUserManagerInitialized @"user_manager_initialized"
#define kUserManagerDefault @"000000000"
#define kLastValidMSISDNPath @"last_msisdn"
#define kLastValidLoginUser @"last_valid_login_user"
#define kLastValidMSISDN @"last_valid_msisdn"
#define kLastValidUniqueID @"last_valid_unique_ID"
#define kLastValidUserStatus @"last_valid_user_status"
#define kLastValidBillingDate @"last_valid_user_billing_date"
#define kLastTimeActiveUserLoginDate @"lastTimeActiveUserLogin"
#define kStartAplicationDate @"startAplicationDate"
#define kLastSaveDateDate @"lastSaveDateDate"
#define kLastValidContentVersion @"last_valid_content_version"
#define kLastValidLevelData @"last_valid_level_data"
#define kLastValidCurrentLevel @"last_valid_current_level"

/* Purchases */

#define kMonthlyPurchaseId @"com.lamark.FrenchCourseGeneric.monthly"
#define kMonthlyPurchaseIdShort @"com.lamark.FrenchCourseGeneric.monthly"
#define kKeywordPurchaseIdShort @"ios"


/* Constants */

#define kCamfindSearchesForGuest 5
#define kCamfindSearchesForUser 25
#define kCamfindSearchesForOther 0

#define kUserStatusGuestString @"Guest"
#define kUserStatusNotRegistered @"NotRegistered"
#define kUserStatusActiveString @"Active"
#define kUserStatusToBeDeactivatedString @"ToBeCanceled"
#define kUserStatusDeactivatedString @"Cancelled"
#define kUserStatusDisabledString @"Disabled"
#define kUserStatusIAP @"IAP"
