//  LMAudioManager.h
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//



@protocol LMAudioManagerDelegate <NSObject>

- (void)recordingComplete;
- (void)playbackItemComplete;
- (void)playbackComplete;
- (void)playbackPaused;

- (void)recordingCompleteWithRecordFile:(NSURL *)audioUrl;

@end


typedef enum {
	kSoundButtonTap,
	kSoundSwipe,
	kSoundError,
	kSoundSuccess,
	kSoundResults,
	kSoundDeletePicture,
	kSoundTakePicture,
    kSoundRecord,
    kSoundStopRecord,
    kSoundTestCorrect,
    kSoundTestWrong,
} ESoundType;

typedef enum {
  kAudioIdle,
  kAudioActive,
  kAudioPaused
} EAudioStatus;

@interface LMAudioManager : NSObject<AVAudioPlayerDelegate, AVAudioRecorderDelegate>

@property (weak, nonatomic) id <LMAudioManagerDelegate> delegate;
@property (assign, nonatomic) EAudioStatus recordingStatus;


- (void)playBackgroundMusic;
- (void)pauseBackgroundMusic;
- (void)playSound:(ESoundType)type;
- (BOOL)playAudio:(NSURL *)path;
- (BOOL)playAudioFromDocumentPath:(NSString *)fileName;
- (BOOL)playAudioFromDocumentPath:(NSString *)fileName componentSender:(id)sender;

- (double)getSoundDuration:(NSString *)fileName;

- (BOOL)stopPlaying;
- (void)setEnabled:(BOOL)enabled;

- (void)record:(NSString *)path;
- (void)stopRecording;
- (void)cancelRecording;
- (void)deleteRecording;

-(void)playRightSound;
-(void)playWrongSound;

- (void)playInitialSound;

- (BOOL)playSoundFromAudioFile:(NSString *)fileName;

@end


#ifdef AUDIO_SINGLETON

@interface LMAudioManager ()
+ (id)sharedInstance;
@end

#define AUDIO_MANAGER ((LMAudioManager *)[LMAudioManager sharedInstance])
#endif
