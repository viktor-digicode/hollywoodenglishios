//
//  CacheManager.m
//  TestAsyncImages
//
//  Created by Dimitar Shopovski on 9/29/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import "CacheManager.h"
#import "CustomDownloadObject.h"
#import "VipCategoryObject+CoreDataClass.h"
#import "VipQuizObject+CoreDataClass.h"

@implementation CacheManager

- (id)init
{
    self = [super init];
    if (self) {
        
        //init
        self.resourcesArray = [NSArray new];
        
    }
    return self;
}

+ (id)sharedInstance {
    
    static CacheManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
    
}

#pragma mark - Initialize resources
- (void)setResourcesToBeCached:(NSArray *)resources {
    
    self.resourcesArray = resources;
    
}

#pragma mark - Start downloading
- (void)startDownloadingAndCachingResources:(NSArray *)resources {
    
    [self setResourcesToBeCached:resources];
    __block NSInteger localCounter = 0;

    CustomDownloadObject *downloadObject;
    CustomDownloadObject *downloadObjectQuiz;
    CustomDownloadObject *downloadObjectQuizInstance;

    //downloading
    for (VipCategoryObject *vipObject in resources) {
        
        //new
        
        NSSet *quizList = vipObject.quizList;
        
        for (VipQuizObject *quizObject in quizList) {
            
            downloadObjectQuiz = [CustomDownloadObject new];
            [downloadObjectQuiz loadImageFromURL:[NSURL URLWithString:quizObject.imageUrl] withCompletitionBlock:^(BOOL isFinished) {
                
            }];
            
            NSSet *set2 = quizObject.instanceList;
            NSArray *arrayInstances = [set2 allObjects];

            for (QuizInstance *quizInstance in arrayInstances) {
                
                NSSet *setAnswers = quizInstance.answersForInstance;
                NSArray *arrayAnswers = [setAnswers allObjects];
                
                downloadObjectQuiz = [CustomDownloadObject new];
                [downloadObjectQuiz loadImageFromURL:[NSURL URLWithString:quizInstance.bigImageUrl] withCompletitionBlock:^(BOOL isFinished) {
                    
                }];

                for (QuizInstanceAnswer *answer in arrayAnswers) {
                    
                    downloadObjectQuizInstance = [CustomDownloadObject new];
                    [downloadObjectQuizInstance loadImageFromURL:[NSURL URLWithString:answer.imageUrl] withCompletitionBlock:^(BOOL isFinished) {
                        
                    }];
                }
            }
        }
        
        //end new
        downloadObject = [CustomDownloadObject new];
        [downloadObject loadImageFromURL:[NSURL URLWithString:vipObject.imageUrl] withCompletitionBlock:^(BOOL isFinished) {
           
            if (isFinished) {
                
                localCounter++;
                if (localCounter == [resources count]) {
                    
                    //Download completed
                    if ([self.delegate respondsToSelector:@selector(cacheManagerFinishedDownloading:)]) {
                        [self.delegate cacheManagerFinishedDownloading:nil];

                    }
                }
            }
        }];
    }
}

#pragma mark - Get Image From Cache
- (UIImage *)getImageFromCache:(NSString *)imageUrlString {
    
    CustomDownloadObject *downloadObject = [CustomDownloadObject new];
    
    if ([downloadObject checkIsImageInCache:[NSURL URLWithString:imageUrlString]]) {
        
        return [downloadObject getImageFromCache:[NSURL URLWithString:imageUrlString]];
    }
    
    return nil;
}

@end
