//
//  CacheManager.h
//  TestAsyncImages
//
//  Created by Dimitar Shopovski on 9/29/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CACHE_MANAGER ((CacheManager *)[CacheManager sharedInstance])

@protocol CacheManagerDelegate <NSObject>

@optional
- (void)cacheManagerFinishedDownloading:(id)sender;

@end

@interface CacheManager : NSObject

@property (nonatomic, strong) id<CacheManagerDelegate> delegate;
@property (nonatomic, strong) NSArray *resourcesArray;

+ (id)sharedInstance;
- (void)setResourcesToBeCached:(NSArray *)resources;
- (void)startDownloadingAndCachingResources:(NSArray *)resources;
- (UIImage *)getImageFromCache:(NSString *)imageUrlString;

@end
