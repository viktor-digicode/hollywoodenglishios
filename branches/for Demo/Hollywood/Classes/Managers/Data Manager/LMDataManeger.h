//
//  LMDataManeger.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandMethods.h>

#import "ProgressUserObject.h"
#import "ProgressLevelObject.h"
#import "ProgressLessonObject.h"
#import "ProgressVehicleObject.h"

#import "VipCategoryObject+CoreDataClass.h"
#import "VipQuizObject+CoreDataClass.h"
#import "VipCourseObject+CoreDataClass.h"

#define DATA_MANAGER ((LMDataManeger *)[LMDataManeger sharedInstance])


@interface LMDataManeger : NSObject

+ (id)sharedInstance;

- (NSArray*)takeDataForLevel:(NSNumber*)levelOrder;
- (NSArray*)takeVehicleForCurrentLesson;
- (NSArray*)getAllProgressObject;
- (void) saveData;

- (NSArray*)getAllVipCategory;


- (ProgressLessonObject*)getCurrentLessonObject;
- (ProgressUserObject*)getCurrentUserProgressObject;
- (ProgressLevelObject*)getCurrentProgressLevelObject;

- (NSArray *)getVIPCategories;
- (NSArray*)getAllVipCategory;

@end
