//
//  AnalyticConstants.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 10/4/16.
//  Copyright © 2016 aa. All rights reserved.
//

#ifndef AnalyticConstants_h
#define AnalyticConstants_h

#pragma mark - Constants
#pragma mark - Tracking id

#define  kTrackingId  @"UA-67867042-1"

#pragma mark - Product name

#define  kProductName  @"Hollywood English Course"

#pragma mark - Event action

#define kEventLaunch                @"Launch"

#define kQuizNSuffGuest @"QuizClick_NSuff_Guest"
#define kQuizNSuffActive @"QuizClick_NSuff_Active"
#define kQuizPurchase @"QuizClick_PurchaseAction"
#define kQuizAlreadyPurchased @"QuizClick_Already_Purchase"
#define kQuizFree @"QuizClick_Free"

#define kQuizScreen @"Quizzes_Screen"

#define kQuizVehiclePresented @"Quiz_Vehicle_Presented"
#define kQuizSwipeInstance @"Quiz_Vehicle_Swiped"

#endif /* AnalyticConstants_h */
