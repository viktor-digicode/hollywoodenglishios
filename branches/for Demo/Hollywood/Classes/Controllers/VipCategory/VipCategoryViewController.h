//
//  VipCategoryViewController.h
//  Hollywood
//
//  Created by Kiril Kiroski on 9/29/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "LMBaseViewController.h"
#import "VipCategoriCollectionViewCell.h"

@interface VipCategoryViewController : LMBaseViewController<UICollectionViewDataSource,UICollectionViewDelegate>

@end
