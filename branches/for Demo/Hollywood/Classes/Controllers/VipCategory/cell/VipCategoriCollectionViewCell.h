//
//  VipCategoriCollectionViewCell.h
//  Hollywood
//
//  Created by Kiril Kiroski on 9/29/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VipCategoryObject+CoreDataClass.h"

@interface VipCategoriCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cellImageView;
@property (weak, nonatomic) IBOutlet UIImageView *comingSoonImageView;
@property (weak, nonatomic) IBOutlet UILabel *comingSoonLabel;


- (void)setVipCategoryData:(VipCategoryObject*)categoryData;

@end
