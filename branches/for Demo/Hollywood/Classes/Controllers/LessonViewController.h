//
//  LessonViewController.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/24/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewVehicle.h"
#import "SwipeKircaVehicle.h"
#import "SwipeCarouselVehicle.h"
#import "KSVideoPlayerView.h"
#import "LessonProgressView.h"
#import "VoiceRecoginitionVehicle.h"


@interface LessonViewController : LMBaseViewController <UIScrollViewDelegate, BaseViewVehicleDelegate,playerViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, assign) NSInteger lessonNumber;
@property (nonatomic, assign) BOOL continueSession;
@property (nonatomic, assign) BOOL isLoadingFinished;
@property (nonatomic, assign) BOOL isPopupOpened;


@end
