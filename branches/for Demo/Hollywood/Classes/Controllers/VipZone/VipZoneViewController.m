//
//  VipZoneViewController.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 8/10/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "VipZoneViewController.h"

@interface VipZoneViewController () {
    
    __weak IBOutlet UILabel *userScoreLabel;
    __weak IBOutlet UILabel *userVipPassesLabel;
    
    __strong ProgressUserObject *currentProgressUserObject;
    __strong ProgressLevelObject *currentProgressLevelObject;

    __weak IBOutlet UIImageView *imageViewUserProfile;
    __weak IBOutlet UIImageView *imageViewUserRank;

}


@end

@implementation VipZoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)configureUI{
    [super configureUI];

    self.navigationController.navigationBarHidden = YES;
    
    imageViewUserProfile.layer.cornerRadius = 4.0;
    imageViewUserProfile.layer.borderWidth = 1.0;
    imageViewUserProfile.layer.borderColor = [UIColor blackColor].CGColor;
    imageViewUserProfile.layer.masksToBounds = YES;
    
    imageViewUserProfile.image = [USER_MANAGER takeCurrentUserImage];

}

- (void)loadData {
    [super loadData];
    
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
    
    userScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
    userVipPassesLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];
    
    imageViewUserRank.image = [UIImage imageNamed:[NSString stringWithFormat:@"Awards_%02d", (currentProgressLevelObject.maxUnit%6)+1]];
    
    //Get JSON
    
    NSLog(@"Categories - %@", [DATA_MANAGER getVIPCategories]);

}

#pragma mark - Back to home page

- (IBAction)goToHomePage:(id)sender {
    
    [APP_DELEGATE buildHomePageStack];
}

@end
