//
//  LessonViewController.m
//  Hollywood1
// 
//  Created by Kiril Kiroski on 3/24/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "LessonViewController.h"
#import "VideoVehicle.h"
#import "SwipeVehicleView.h"
#import "ClickToLearnVehicle.h"
#import "MultipleChoiceText.h"
#import "SpellingVehicle.h"
#import "FillMissingWordsVehicle.h"

#import "ProgressLessonObject.h"
#import "ProgressVehicleObject.h"

@interface LessonViewController ()<CacheManagerDelegate> {
    
    __weak IBOutlet LessonProgressView *currentLessonProgress;
    __weak IBOutlet NSLayoutConstraint *instructionButton;
    __weak IBOutlet UIView *topBarView;
    __weak IBOutlet UIView *questionBarView;
    __weak IBOutlet UIScrollView *mainScrollView;
    __weak IBOutlet UIView *instructionView;
    __weak IBOutlet NSLayoutConstraint *instructionViewBottomConstrains;
    KSVideoPlayerView* player;
    KSVideoPlayerView* preloadPlayer;
    __weak IBOutlet NSLayoutConstraint *instructionViewTopConstrains;
    CGFloat initialConstantForBottomConstrains;
    NSMutableArray *vehiclesArray;
    UISwipeGestureRecognizer *leftSwipe,*rightSwipe;
    NSInteger lastCurrentPage;
    __weak IBOutlet UIButton *rightArrowButton;
    __weak IBOutlet UIButton *leftArrowButton;
    
    __weak IBOutlet VocabView *questionBarVocabView;
    __weak IBOutlet UILabel *questionBarLabel;
    __weak IBOutlet UILabel *scoreLabel;
    __weak IBOutlet UILabel *vipScoreLabel;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *vipStaticTextLabel;
    
    __weak IBOutlet UIImageView *imageViewUserProfile;
    __weak IBOutlet UIImageView *imageViewUserRank;
    
    int totalScore;
    //int vipScopre;
    float pointForVipScopre;
    
    __strong ProgressLessonObject *currentProgressLessonObject;
    __strong ProgressUserObject *currentProgressUserObject;
    __strong ProgressLevelObject *currentProgressLevelObject;
    
    __weak IBOutlet UIView *loadingView;
    __weak IBOutlet UIActivityIndicatorView *activityIndicatorLoading;
    __weak IBOutlet UILabel *loadingLabel;

}

@end

@implementation LessonViewController

- (void)configureUI{
    [super configureUI];
    [self initialScroll];
    self.navigationController.navigationBarHidden = YES;
    instructionView.translatesAutoresizingMaskIntoConstraints = NO;
    [currentLessonProgress configureUI];
    
    imageViewUserProfile.layer.cornerRadius = 4.0;
    imageViewUserProfile.layer.borderWidth = 1.0;
    imageViewUserProfile.layer.borderColor = [UIColor blackColor].CGColor;
    imageViewUserProfile.layer.masksToBounds = YES;

    scoreLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:30.0];
    vipScoreLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:15.0];
    vipStaticTextLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:18.0];

    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deactivateCurrentVehicle) name:@"deactivateCurrentVehicle" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pauseCurrentVehicle) name:@"deactivateCurrentVehicle" object:nil];
    
    currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
    currentProgressLessonObject = [DATA_MANAGER getCurrentLessonObject];
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
     [self updateLabels];
}

- (void)loadData {
    
    [super loadData];
    [AUDIO_MANAGER  playInitialSound];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //Popup Logic

    NSLog(@"Defaults - %d, current unit - %d, maxUnit - %d, current vehicle - %d", [defaults boolForKey:@"openLessonSession"], currentProgressLevelObject.currentUnit,currentProgressLevelObject.maxUnit, currentProgressLessonObject.currentVehicle);
    
    if ([defaults boolForKey:@"openLessonSession"] && currentProgressLevelObject.currentUnit == currentProgressLevelObject.maxUnit
        && currentProgressLessonObject.currentVehicle != 0) {
        
        self.isPopupOpened = YES;
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:nil
                                                                        message:@"Do you want to continue the previous active session or you want to start new session?"
                                                                 preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *continueAction = [UIAlertAction
                                         actionWithTitle:@"Continue"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                        
                                             _continueSession = YES;
                                             self.isPopupOpened = NO;
                                             
                                             if (self.isLoadingFinished) {
                                                 [self activeteFirstVehicle];
                                             }
                                             
                                             [alert dismissViewControllerAnimated:YES completion:nil];

                                         }];
        
        [alert addAction:continueAction];
        
        UIAlertAction *newAction = [UIAlertAction
                                    actionWithTitle:@"New session"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        self.isPopupOpened = NO;
                                        
                                        if (self.isLoadingFinished) {
                                            [self activeteFirstVehicle];
                                        }
                                        
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                        
                                    }];
        
        [alert addAction:newAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if (currentProgressLevelObject.currentUnit == currentProgressLevelObject.maxUnit) {
        
        _continueSession = YES;
        
        if (self.isLoadingFinished) {
            [self activeteFirstVehicle];
        }
    }
    
    [defaults setBool:YES forKey:@"openLessonSession"];
    [defaults synchronize];

    [self addLoadingVehiclesIndicator];
    [self performSelector:@selector(loadScrollData) withObject:self afterDelay:1.0];
    //[self loadScrollData];

    self.lessonNumber = currentProgressLevelObject.currentUnit;
    NSLog(@"currentProgressLessonObject.scoreForLesson %d",currentProgressLessonObject.scoreForLesson);
    totalScore = currentProgressLessonObject.scoreForLesson;
    pointForVipScopre = 0 ;
    //vipScopre = kParam014;
    [self updateLabels];
    
    imageViewUserRank.image = [UIImage imageNamed:[NSString stringWithFormat:@"Awards_%02d", (currentProgressLevelObject.maxUnit%6)+1]];
    imageViewUserProfile.image = [USER_MANAGER takeCurrentUserImage];
    

}

- (void)initialScroll {
    
    topBarView.backgroundColor = APP_GOLD_COLOR;
    
    [mainScrollView setDelegate:self];
    [mainScrollView setBouncesZoom:YES];
    [mainScrollView setContentMode:UIViewContentModeCenter];
    mainScrollView.userInteractionEnabled = YES;
    
    mainScrollView.contentSize = CGSizeMake(DEVICE_BIGER_SIDE*9, mainScrollView.contentSize.height);
    [mainScrollView setScrollEnabled:YES];
    [mainScrollView setShowsHorizontalScrollIndicator:FALSE];
    [mainScrollView setShowsVerticalScrollIndicator:FALSE];
    
    mainScrollView.scrollsToTop = NO;
    mainScrollView.pagingEnabled = YES;
    
    initialConstantForBottomConstrains = [mainScrollView height];//instructionViewBottomConstrains.constant;
    instructionViewBottomConstrains.constant = initialConstantForBottomConstrains;
    instructionViewTopConstrains.constant = -initialConstantForBottomConstrains;
    [self.view layoutIfNeeded];
    
    topBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar_bg_up"]];
    questionBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar_bg_down"]];
    
    
}

- (void)configureObservers {
    
    [super configureObservers];
}

- (void)addSwipeGesture {
    
    leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeActionScorllContainer:)];
    [leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
    leftSwipe.delegate = self;
    [self.view addGestureRecognizer:leftSwipe];
    
    rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeActionScorllContainer:)];
    [rightSwipe setDirection:UISwipeGestureRecognizerDirectionRight];
    rightSwipe.delegate = self;
    [self.view addGestureRecognizer:rightSwipe];
}

- (void)removeSwipeGesture {
    
    
    [self.view removeGestureRecognizer:rightSwipe];
    [self.view removeGestureRecognizer:leftSwipe];
}

- (void)activeteFirstVehicle {
    
    BaseViewVehicle *currentVehicle;
    
    if (self.continueSession) {

        currentVehicle = [vehiclesArray objectAtIndex:currentProgressLessonObject.currentVehicle];
        [currentVehicle activateVehicle];
        lastCurrentPage = currentProgressLessonObject.currentVehicle;
        
        ProgressVehicleObject *tempProgressVehicle =  [currentProgressLessonObject getCurrentProgressVehicleObject];
        tempProgressVehicle.achived = YES;
        
        [DATA_MANAGER saveData];
        
        if (currentProgressLessonObject.currentVehicle == 0) {
            leftArrowButton.hidden = YES;

        }
        
        [mainScrollView setContentOffset:CGPointMake(currentProgressLessonObject.currentVehicle*DEVICE_BIGER_SIDE, 0) animated:YES];
        
        [currentLessonProgress configureUI];
        [currentLessonProgress changeProgressWithValue:0.04*currentProgressLessonObject.currentVehicle];
        
        
    }
    else {
        
        currentVehicle = [vehiclesArray objectAtIndex:0];
        [currentVehicle activateVehicle];
        
        currentProgressLessonObject.currentVehicle = 0;
        
        leftArrowButton.hidden = YES;
        
        ProgressVehicleObject *tempProgressVehicle =  [currentProgressLessonObject getCurrentProgressVehicleObject];
        tempProgressVehicle.achived = YES;
        
        [DATA_MANAGER saveData];
        
        [currentLessonProgress configureUI];
        
    }
}

- (void)loadScrollData {
    
    //mainScrollView.contentSize = CGSizeMake(DEVICE_BIGER_SIDE*17, mainScrollView.contentSize.height);
    //[self addActivityIndicator];

    float vehicleWidth = mainScrollView.frame.size.width;
    float vehicleHeight = mainScrollView.frame.size.height;
    vehiclesArray = [[NSMutableArray alloc]init];
    
    
    VideoVehicle *tempVehivleNew =  [[VideoVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)];
    tempVehivleNew.vehicleDelegate = self;
    tempVehivleNew.goNextAfterVideoFinish = YES;
    tempVehivleNew.videoUrlString = @"http://s3.amazonaws.com/test_media_files/ios/demo_video_2.m4v";
    [self addVehicle:tempVehivleNew];
    //
    
    int counter = 1;
    float delayInSeconds = 0.1;
    
   
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        VoiceRecoginitionVehicle *voice = [[VoiceRecoginitionVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight) data:@{                                                                                                                                                                                                                @"recordingText":  @"My name is Bond. James Bond.",                                                                                                                                                                                                                                                                                                                                                                   @"audioDataUrl":@"Voice_Recognition_1.mp3",                                                                                                                                                                                                                                                                                                                                                                                                @"image": @"494656106",                                                                                                                                                                                                                                                                                                                                                                                            @"questionText": @"Grave a frase slogan de James Bond."}];
        voice.vehicleDelegate = self;
        [self addVehicle:voice];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        VoiceRecoginitionVehicle *voice = [[VoiceRecoginitionVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight) data:@{                                                                                                                                                                                                                @"recordingText":  @"How are you?",                                                                                                                                                                                                                                                                                                                                                                   @"audioDataUrl":@"Voice_Recognition_2.mp3",                                                                                                                                                                                                                                                                                                                                                                                                @"image": @"72758439",                                                                                                                                                                                                                                                                                                                                                                                            @"questionText": @"Grave a frase."}];
        voice.vehicleDelegate = self;
        [self addVehicle:voice];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        VoiceRecoginitionVehicle *voice = [[VoiceRecoginitionVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight) data:@{                                                                                                                                                                                                                @"recordingText":  @"Nice to meet you",                                                                                                                                                                                                                                                                                                                                                                   @"audioDataUrl":@"Voice_Recognition_3.mp3",                                                                                                                                                                                                                                                                                                                                                                                                @"image": @"605874250",                                                                                                                                                                                                                                                                                                                                                                                            @"questionText": @"Grave a frase."}];
        voice.vehicleDelegate = self;
        [self addVehicle:voice];
    });
    
   /* dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        VideoVehicle *tempVehivleNew =  [[VideoVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)];
        tempVehivleNew.vehicleDelegate = self;
        tempVehivleNew.goNextAfterVideoFinish = YES;
        tempVehivleNew.videoUrlString = @"http://cdn.kantoo.com/Hollywood/Hollywood%20Demo%20-%20part%205.mp4";
        [self addVehicle:tempVehivleNew];
    });*/
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        SwipeKircaVehicle *swipeView2 = [[SwipeKircaVehicle alloc]
                                         initWithFrame:
                                         CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                         
                                         data:@{
                                                @"afterAudio":@"Swipe_2_1.mp3",
                                                @"questionText": @"Deslize o texto até a imagem correspondente.",
                                                @"swipeImageVehicle":@NO,
                                                @"answers": @[
                                                        @{@"text": @"", @"isCorrect": @YES, @"image": @"164230659"},
                                                        @{@"text": @"", @"isCorrect": @NO, @"image": @"143482927"}],
                                                @"movingViewData": @{
                                                        @"textAnswer":  @"lawyer",
                                                        @"isText":      @YES,
                                                        @"imageAnswer": @""
                                                        }
                                                }];
        swipeView2.vehicleDelegate = self;
        [self addVehicle:swipeView2];
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        SwipeKircaVehicle *swipeView2 = [[SwipeKircaVehicle alloc]
                                         initWithFrame:
                                         CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                         
                                         data:@{
                                                @"afterAudio":@"Swipe_2_2.mp3",
                                                @"questionText": @"Deslize o texto até a imagem correspondente.",
                                                @"swipeImageVehicle":@NO,
                                                @"answers": @[
                                                        @{@"text": @"", @"isCorrect": @YES, @"image": @"473752182"},
                                                        @{@"text": @"", @"isCorrect": @NO, @"image": @"492478901"}],
                                                @"movingViewData": @{
                                                        @"textAnswer":  @"actress",
                                                        @"isText":      @YES,
                                                        @"imageAnswer": @""
                                                        }
                                                }];
        swipeView2.vehicleDelegate = self;
        [self addVehicle:swipeView2];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        SwipeKircaVehicle *swipeView3 = [[SwipeKircaVehicle alloc]
                                         initWithFrame:
                                         CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                         
                                         data:@{
                                                @"afterAudio":@"Swipe_2_3.mp3",
                                                @"questionText": @"Deslize o texto até a imagem correspondente.",
                                                @"swipeImageVehicle":@NO,
                                                @"answers": @[
                                                        @{@"text": @"", @"isCorrect": @YES, @"image": @"2040035"},
                                                        @{@"text": @"", @"isCorrect": @NO, @"image": @"484321383"}],
                                                @"movingViewData": @{
                                                        @"textAnswer":  @"cop",
                                                        @"isText":      @YES,
                                                        @"imageAnswer": @""
                                                        }
                                                }];
        swipeView3.vehicleDelegate = self;
        [self addVehicle:swipeView3];
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        SwipeKircaVehicle *swipeView1 = [[SwipeKircaVehicle alloc]
                                         initWithFrame:
                                         CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                         
                                         data:@{
                                                @"afterAudio":@"Swipe_1_1.mp3",
                                                @"questionText": @"Deslize a imagem até a frase correspondente.",
                                                @"swipeImageVehicle":@YES,
                                                @"answers": @[
                                                        @{@"text": @"He is", @"isCorrect": @NO, @"image": @""},
                                                        @{@"text": @"She is", @"isCorrect": @YES, @"image": @""}],
                                                @"movingViewData": @{
                                                        @"textAnswer":  @"",
                                                        @"isText":      @NO,
                                                        @"imageAnswer": @"488503490"
                                                        }
                                                }];
        swipeView1.vehicleDelegate = self;
        [self addVehicle:swipeView1];
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        SwipeKircaVehicle *swipeView1 = [[SwipeKircaVehicle alloc]
                                         initWithFrame:
                                         CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                         
                                         data:@{
                                                @"afterAudio":@"Swipe_1_2.mp3",
                                                @"questionText": @"Deslize a imagem até a frase correspondente.",
                                                @"swipeImageVehicle":@YES,
                                                @"answers": @[
                                                        @{@"text": @"He is", @"isCorrect": @YES, @"image": @""},
                                                        @{@"text": @"She is", @"isCorrect": @NO, @"image": @""}],
                                                @"movingViewData": @{
                                                        @"textAnswer":  @"Deslize a imagem até a frase correspondente.",
                                                        @"isText":      @NO,
                                                        @"imageAnswer": @"144012294"
                                                        }
                                                }];
        swipeView1.vehicleDelegate = self;
        [self addVehicle:swipeView1];
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        ClickToLearnVehicle *clickVehicleAnswers = [[ClickToLearnVehicle alloc] initWithFrame:
                                                    CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                                                                         data:@{
                                                                                                @"answers": @[
                                                                                                        @{@"text": @"doctor", @"isCorrect": @(YES), @"image": @"159836194",@"targetText": @"médico",
                                                                                                            @"sound": @"Click_To_Learn_1.mp3"},
                                                                                                        @{@"text": @"lawyer", @"isCorrect": @(YES), @"image": @"850482",@"targetText": @"advogado",
                                                                                                            @"sound" : @"Swipe_2_1.mp3"},
                                                                                                        @{@"text": @"astronaut", @"isCorrect": @(NO), @"image": @"1592329",@"targetText": @"astronauta",
                                                                                                            @"sound": @"Click_To_Learn_2.mp3"},
                                                                                                            ],
                                                                                                @"isOneImageLayout": @(NO),
                                                                                                @"bigImage": @"family",
                                                                                                @"questionText": @"Toque nas imagens para aprender as palavras em inglês."
                                                                                                
                                                                                                } isLearnVehicle:YES isOrdered:NO];
        clickVehicleAnswers.vehicleDelegate = self;
        [self addVehicle:clickVehicleAnswers];
        
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        ClickToLearnVehicle *clickVehicleAnswers = [[ClickToLearnVehicle alloc] initWithFrame:
                                                    CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                                                                         data:@{
                                                                                                @"answers": @[
                                                                                                        @{@"text": @"pilot", @"isCorrect": @(YES), @"image": @"137078260",@"targetText": @"piloto",
                                                                                                          @"sound": @"Click_To_Learn_3.mp3"},
                                                                                                        @{@"text": @"cop", @"isCorrect": @(YES), @"image": @"470662826",@"targetText": @"policial",
                                                                                                          @"sound": @"Swipe_2_3.mp3"},
                                                                                                        @{@"text": @"actor", @"isCorrect": @(NO), @"image": @"174173578",@"targetText": @"ator",
                                                                                                          @"sound": @"Click_To_Learn_4.mp3"},
                                                                                                        @{@"text": @"actress", @"isCorrect": @(NO), @"image": @"73005463",@"targetText": @"atriz",
                                                                                                          @"sound": @"Swipe_2_2.mp3"}],
                                                                                                @"isOneImageLayout": @(NO),
                                                                                                @"bigImage": @"family",
                                                                                                @"questionText": @"Toque nas imagens para aprender as palavras em inglês."
                                                                                                } isLearnVehicle:YES isOrdered:NO];
        clickVehicleAnswers.vehicleDelegate = self;
        [self addVehicle:clickVehicleAnswers];
        
    });
    
    

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        MultipleChoiceText *multiChoiceText = [[MultipleChoiceText alloc] initWithFrame:
                                               CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                                                                   data:@{
                                                                                          @"answers": @[
                                                                                                  @{@"text": @"My name is Jennifer Lopez.", @"isCorrect": @(NO),
                                                                                                    @"sound": @"Multiple_Choice_Text_1.mp3"},
                                                                                                  @{@"text": @"I am Jennifer Lopez.", @"isCorrect": @(YES),
                                                                                                    @"sound": @"Multiple_Choice_Text_2.mp3"},
                                                                                                  @{@"text": @"She is Jennifer Lopez.", @"isCorrect": @(NO),
                                                                                                    @"sound": @"Multiple_Choice_Text_3.mp3"},
                                                                                                  @{@"text": @"You are Jennifer Lopez.", @"isCorrect": @(NO),
                                                                                                    @"sound": @"Multiple_Choice_Text_4.mp3"}],
                                                                                          @"isOneImageLayout": @(YES),
                                                                                          @"bigImage": @"464247578",
                                                                                          @"questionText": @"Traduza: Eu sou Jennifer Lopez.",
                                                                                          @"afterAudio": @"Multiple_Choice_Text_2.mp3"
                                                                                          } isOrdered:NO];
        multiChoiceText.vehicleDelegate = self;
        [self addVehicle:multiChoiceText];
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        MultipleChoiceText *multiChoiceText = [[MultipleChoiceText alloc] initWithFrame:
                                               CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                                                                   data:@{
                                                                                          @"answers": @[
                                                                                                  @{@"text": @"I am Tom Cruise.", @"isCorrect": @(NO),
                                                                                                    @"sound": @"Multiple_Choice_Text_5.mp3"},
                                                                                                  @{@"text": @"My name is Tom Cruise.", @"isCorrect": @(YES),
                                                                                                    @"sound": @"Multiple_Choice_Text_6.mp3"},
                                                                                                  @{@"text": @"He is Tom Cruise.", @"isCorrect": @(NO),
                                                                                                    @"sound": @"Multiple_Choice_Text_7.mp3"},
                                                                                                  @{@"text": @"You are Tom Cruise.", @"isCorrect": @(NO),
                                                                                                    @"sound": @"Multiple_Choice_Text_8.mp3"}],
                                                                                          @"isOneImageLayout": @(NO),
                                                                                          @"bigImage": @"168293161",
                                                                                          @"questionText": @"Traduza: Meu nome é Tom Cruise.",
                                                                                          @"afterAudio": @"Multiple_Choice_Text_6.mp3"
                                                                                          } isOrdered:NO];
        multiChoiceText.vehicleDelegate = self;
        [self addVehicle:multiChoiceText];
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        ClickToLearnVehicle *clickVehicleAnswers = [[ClickToLearnVehicle alloc] initWithFrame:
                                                    CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                                                                         data:@{
                                                                                                @"answers": @[
                                                                                                        @{@"text": @"PILOT", @"isCorrect": @(YES), @"image": @"908078",@"targetText": @"PILOT"},
                                                                                                        @{@"text": @"COP", @"isCorrect": @(NO), @"image": @"143479967",@"targetText": @"COP"},
                                                                                                        @{@"text": @"ACTOR", @"isCorrect": @(NO), @"image": @"152919026",@"targetText": @"ACTOR"},
                                                                                                        @{@"text": @"ACTRESS", @"isCorrect": @(NO), @"image": @"470565654",@"targetText": @"ACTRESS"}],
                                                                                                @"isOneImageLayout": @(NO),
                                                                                                @"bigImage": @"family",
                                                                                                @"questionText": @"Clique na imagem correspondente a TEACHER.",
                                                                                                @"afterAudio": @"Multiple_Choice_Image_1.mp3"
                                                                                                } isLearnVehicle:NO isOrdered:NO];
        clickVehicleAnswers.vehicleDelegate = self;
        [self addVehicle:clickVehicleAnswers];
        
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        ClickToLearnVehicle *clickVehicleAnswers = [[ClickToLearnVehicle alloc] initWithFrame:
                                                    CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                                                                         data:@{
                                                                                                @"answers": @[
                                                                                                        @{@"text": @"PILOT", @"isCorrect": @(YES), @"image": @"585297647",@"targetText": @"PILOT"},
                                                                                                        @{@"text": @"COP", @"isCorrect": @(NO), @"image": @"499860156",@"targetText": @"COP"}],
                                                                                                @"isOneImageLayout": @(NO),
                                                                                                @"bigImage": @"family",
                                                                                                @"questionText": @"Clique na imagem correspondente a The young Ian McKellen.",
                                                                                                @"afterAudio":@"Multiple_Choice_Image_2.mp3",
                                                                                                } isLearnVehicle:NO isOrdered:NO];
        clickVehicleAnswers.vehicleDelegate = self;
        [self addVehicle:clickVehicleAnswers];
        
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        SpellingVehicle *spelling = [[SpellingVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                                                      data:@{
                                                                             @"image": @"162599256",
                                                                             @"answer": @"<I> <AM> AN <ACTOR>.",
                                                                             @"questionText": @"Traduza: Eu sou ator.",
                                                                             @"afterAudio": @"Spelling_1.mp3"
                                                                             }];
        spelling.vehicleDelegate = self;
        [self addVehicle:spelling];
        
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        SwipeCarouselVehicle *carouseView = [[SwipeCarouselVehicle alloc]
                                             initWithFrame:
                                             CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                             data:@{   @"answers": @[
                                                               @{@"text": @"Selecione",@"image": @"908078", @"isCorrect": @YES},
                                                               @{@"text": @"Selecione",@"image": @"143479967", @"isCorrect": @NO},
                                                               @{@"text": @"Selecione",@"image": @"152919026", @"isCorrect": @NO},
                                                               @{@"text": @"Selecione",@"image": @"470565654", @"isCorrect": @NO},
                                                               @{@"text": @"Selecione",@"image": @"904722", @"isCorrect": @NO}],
                                                       
                                                       @"questionTextVocabs" : @(YES),
                                                       @"questionText": @"Clique na imagem correspondente a TEACHER."
                                                       
                                                       }];
        carouseView.vehicleDelegate = self;
        [self addVehicle:carouseView];
        
    });
    
    
    ///real
    mainScrollView.contentSize = CGSizeMake(DEVICE_BIGER_SIDE*(counter+2), mainScrollView.contentSize.height);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(18 * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self removeActivityIndicator];
        
        if (!self.isPopupOpened) {
            
            [self activeteFirstVehicle];

        }
        
        self.isLoadingFinished = YES;

    });
    
}

- (void)loadScrollData1 {
    
    vehiclesArray = [[NSMutableArray alloc]init];
    float vehicleWidth = mainScrollView.frame.size.width;
    float vehicleHeight = mainScrollView.frame.size.height;
//    SpellingVehicle *spelling = [[SpellingVehicle alloc] initWithFrame:CGRectMake(60, 0, self.view.frame.size.width-120, 250) data:@{
//                                                                                                                                      @"answer": @"Erm. <You>'<re> a good man."
//                                                                                                                                      }];
//    spelling.vehicleDelegate = self;
//    [self addVehicle:spelling];
    
    MultipleChoiceText *multiChoiceText = [[MultipleChoiceText alloc] initWithFrame:
                                         CGRectMake(0, 0, vehicleWidth, vehicleHeight)
                                                                              data:@{
                                                                                     @"answers": @[
                                                                                             @{@"text": @"Four", @"isCorrect": @(YES)},
                                                                                             @{@"text": @"Five", @"isCorrect": @(NO)},
                                                                                             @{@"text": @"Three", @"isCorrect": @(NO)},
                                                                                             @{@"text": @"One", @"isCorrect": @(NO)}],
                                                                                     @"isOneImageLayout": @(YES),
                                                                                     @"bigImage": @"the-usual-suspects",
                                                                                     @"questionText": @"How many people are in this photo of the \"Usual Suspects\" movie?"
                                                                                     
                                                                                     } isOrdered:NO];
    multiChoiceText.vehicleDelegate = self;
    [self addVehicle:multiChoiceText];
    
    SwipeKircaVehicle *swipeView21 = [[SwipeKircaVehicle alloc]
                                     initWithFrame:
                                     CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                     
                                     data:@{
                                            @"afterAudio":@"It is.wav",
                                            @"beforeAudio":@"It is.wav",
                                            @"swipeImageVehicle":@NO,
                                            @"answers": @[
                                                    @{@"text": @"", @"isCorrect": @NO, @"image": @"Layer 28"},
                                                    @{@"text": @"", @"isCorrect": @YES, @"image": @"Taylor-In-Black"}],
                                            @"movingViewData": @{
                                                    @"textAnswer":  @"A Black Dress",
                                                    @"isText":      @YES,
                                                    @"imageAnswer": @""
                                                    }
                                            }];
    swipeView21.vehicleDelegate = self;
    [self addVehicle:swipeView21];

    
    FillMissingWordsVehicle *missingWordsVehicle = [[FillMissingWordsVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*mainScrollView.frame.size.width, 0, vehicleWidth, vehicleHeight) data:@{
                                                                                                                                                                                       @"answers": @[
                                                                                                                                                                                               @{@"text": @"Isn't", @"isCorrect":@YES, @"correctPlace":@0},
                                                                                                                                                                                               @{@"text": @"Tall", @"isCorrect":@YES,@"correctPlace":@1},
                                                                                                                                                                                               @{@"text": @"Short", @"isCorrect":@NO,@"correctPlace":@-1},
                                                                                                                                                                                               @{@"text": @"Aren't", @"isCorrect":@NO,@"correctPlace":@-1}],
                                                                                                                                                                                       
                                                                                                                                                                                       @"question":@"He <Isn't> <Tall>",                                                                                                                    @"answers": @"He Isn't Tall",
                                                                                            @"bigImage": @"oskars",                                                                                           }];
    missingWordsVehicle.vehicleDelegate = self;
    [self addVehicle:missingWordsVehicle];
    
    FillMissingWordsVehicle *missingWordsVehicle2 = [[FillMissingWordsVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*mainScrollView.frame.size.width, 0, vehicleWidth, vehicleHeight) data:@{
                                                                                                                                                                                       @"answers": @[
                                                                                                                                                                                               @{@"text": @"Isn'tIsn't", @"isCorrect":@YES, @"correctPlace":@0},
                                                                                                                                                                                               @{@"text": @"T", @"isCorrect":@YES,@"correctPlace":@1},
                                                                                                                                                                                               @{@"text": @"Short", @"isCorrect":@NO,@"correctPlace":@-1},
                                                                                                                                                                                               @{@"text": @"Aren't", @"isCorrect":@NO,@"correctPlace":@-1}],
                                                                                                                                                                                       
                                                                                                                                                                                       @"question":@"He <Isn'tIsn't> <T>",                                                                                                                    @"answers": @"He Isn't Tall",                                                                                         }];
    missingWordsVehicle2.vehicleDelegate = self;
    [self addVehicle:missingWordsVehicle2];
    return;
    
    /*BaseViewVehicle *tempVehivle =  [[VideoVehicle alloc] initWithFrame:CGRectMake(0,0,mainScrollView.frame.size.width, mainScrollView.frame.size.height)];
    tempVehivle.vehicleDelegate = self;
    [mainScrollView addSubview:tempVehivle];
    [tempVehivle activateVehicle];*/
/*SwipeKircaVehicle *swipeView1 = [[SwipeKircaVehicle alloc]
                                   initWithFrame:
                                   CGRectMake(0, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)
                                   
                                   data:@{
                                          @"movingViewData": @{
                                                  @"textAnswer":  @"Angelina Jolie",
                                                  @"isText":      @(YES),
                                                  @"imageAnswer": @"angelina"
                                                  }
                                          }];
    swipeView1.vehicleDelegate = self;
    [mainScrollView addSubview:swipeView1];*/
    
   /* SwipeVehicleView *swipeView = [[SwipeVehicleView alloc]
                                   initWithFrame:
                                   CGRectMake(0, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)
                                   
                                   data:@{
                                            @"movingViewData": @{
                                            @"textAnswer":  @"Angelina Jolie",
                                            @"isText":      @(YES),
                                            @"imageAnswer": @"angelina"
                                        }
                                            }];
    swipeView.vehicleDelegate = self;
    [mainScrollView addSubview:swipeView];*/
    
    SwipeCarouselVehicle *swipeView2 = [[SwipeCarouselVehicle alloc]
                                   initWithFrame:
                                   CGRectMake(1*mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)
                                        data:@{   @"answers": @[
                                                          @{@"text": @"Select",@"image": @"Carousel1", @"isCorrect": @YES},
                                                          @{@"text": @"Select",@"image": @"Carousel2", @"isCorrect": @NO},
                                                          @{@"text": @"Select",@"image": @"Carousel3", @"isCorrect": @NO}],
                                                  @"questionText": @"Which of these family members is the mother?"  }];
    swipeView2.vehicleDelegate = self;
    [self addVehicle:swipeView2];
   /* VoiceRecoginitionVehicle *voice = [[VoiceRecoginitionVehicle alloc] initWithFrame:CGRectMake(1*mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height) data:@{                                                                                                                                                                                                                @"recordingText":  @"They have six children.",                                                                                                                                                                                                                 @"image": @"bredAngelina"
        }];
    voice.vehicleDelegate = self;
    [self addVehicle:voice];*/
    
    
    ClickToLearnVehicle *clickVehicle = [[ClickToLearnVehicle alloc] initWithFrame:
                                   CGRectMake(2*mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)
                                   data:@{
                                          @"answers": @[
                                                  @{@"text": @"mother", @"isCorrect": @(YES)},
                                                  @{@"text": @"sister", @"isCorrect": @(NO)},
                                                  @{@"text": @"brother", @"isCorrect": @(NO)},
                                                  @{@"text": @"father", @"isCorrect": @(NO)}],
                                          @"isOneImageLayout": @(YES),
                                          @"bigImage": @"family"
                                          
                                          } isLearnVehicle:NO];
    clickVehicle.vehicleDelegate = self;
    [self addVehicle:clickVehicle];
    
    ClickToLearnVehicle *clickVehicleAnswers = [[ClickToLearnVehicle alloc] initWithFrame:
                                         CGRectMake(3*mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)
                                                                              data:@{
                                                                                     @"answers": @[
                                                                                             @{@"text": @"Jennifer Lopez", @"isCorrect": @(YES), @"image": @"answer1"},
                                                                                             @{@"text": @"Billy Bob Thornton", @"isCorrect": @(NO), @"image": @"answer2"},
                                                                                             @{@"text": @"Elizabeth Taylor", @"isCorrect": @(NO), @"image": @"answer3"},
                                                                                             @{@"text": @"Kim Kardashian", @"isCorrect": @(NO), @"image": @"answer4"}],
                                                                                     @"isOneImageLayout": @(NO),
                                                                                     @"bigImage": @"family"
                                                                                     
                                                                                     } isLearnVehicle:NO];
    clickVehicleAnswers.vehicleDelegate = self;
    [self addVehicle:clickVehicleAnswers];
    
    VideoVehicle *tempVehivle =  [[VideoVehicle alloc] initWithFrame:CGRectMake(4*mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)];
    tempVehivle.vehicleDelegate = self;
    tempVehivle.goNextAfterVideoFinish = YES;
    
    [self addVehicle:tempVehivle];
    
    
    SwipeKircaVehicle *swipeView1 = [[SwipeKircaVehicle alloc]
                                     initWithFrame:
                                     CGRectMake(5*mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)
                                     
                                     data:@{
                                            @"swipeImageVehicle":@YES,
                                            @"answers": @[
                                                    @{@"text": @"A Green Shirt", @"isCorrect": @YES, @"image": @""},
                                                    @{@"text": @"A Red Shirt", @"isCorrect": @NO, @"image": @""}],
                                            @"movingViewData": @{
                                                    @"textAnswer":  @"",
                                                    @"isText":      @NO,
                                                    @"imageAnswer": @"swipeImage"
                                                    }
                                            }];
    swipeView1.vehicleDelegate = self;
    //[swipeView1 setVehicleType];
    [self addVehicle:swipeView1];
    
    VideoVehicle *tempVehivle2 =  [[VideoVehicle alloc] initWithFrame:CGRectMake(6*mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)];
    tempVehivle2.vehicleDelegate = self;
    tempVehivle2.goNextAfterVideoFinish = NO;
    [self addVehicle:tempVehivle2];
    
    
    SwipeKircaVehicle *swipeView3 = [[SwipeKircaVehicle alloc]
                                     initWithFrame:
                                     CGRectMake(7 * mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)
                                     
                                     data:@{
                                            @"swipeImageVehicle":@NO,
                                            @"answers": @[
                                                    @{@"text": @"", @"isCorrect": @NO, @"image": @"Layer 28"},
                                                    @{@"text": @"", @"isCorrect": @YES, @"image": @"Taylor-In-Black"}],
                                            @"movingViewData": @{
                                                    @"textAnswer":  @"A Black Dress",
                                                    @"isText":      @YES,
                                                    @"imageAnswer": @""
                                                    }
                                            }];
    swipeView3.vehicleDelegate = self;
    swipeView3.swipeImageVehicle = YES;
    //[swipeView3 setVehicleType];
    [self addVehicle:swipeView3];
    
    /*NSLog(@"1");
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Background work
        NSLog(@"2");
        NSMutableArray *vehicleArray = [[DATA_MANAGER takeVehicleForCurrentLesson] mutableCopy];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"3");
            for(int i = 0; i<5; i++){
                ECVehicleDisplay *tempVehivle =  [[VideoVehicle alloc] initWithFrame:CGRectMake(0+(i*mainScrollView.frame.size.width),0,mainScrollView.frame.size.width, mainScrollView.frame.size.height)];
                //tempVehivle.backgroundColor = [UIColor whiteColor];
                [mainScrollView addSubview:tempVehivle];
            }
        });
        
    });
    NSLog(@"4");*/
/* dispatch_async(dispatch_get_main_queue(), ^{
     NSMutableArray *vehicleArray = [[DATA_MANAGER takeVehicleForCurrentLesson] mutableCopy];
    for(int i = 3; i<=5; i++){
    ECVehicleDisplay *tempVehivle =  [[VideoVehicle alloc] initWithFrame:CGRectMake(0+(i*mainScrollView.frame.size.width),0,mainScrollView.frame.size.width, mainScrollView.frame.size.height)];
        //tempVehivle.backgroundColor = [UIColor whiteColor];
        [mainScrollView addSubview:tempVehivle];
    }
    [super updateViewConstraints];
     });*/
}

- (void) addVehicle:(BaseViewVehicle*)newVehicle{
    //NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    //[DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
   // NSLog(@"addVehicle time: %@",[DateFormatter stringFromDate:[NSDate date]]);
    
    [mainScrollView addSubview:newVehicle];
    [vehiclesArray addObject:newVehicle];
    
    
//    mainScrollView.contentSize = CGSizeMake(DEVICE_BIGER_SIDE*[vehiclesArray count], mainScrollView.contentSize.height);
}
- (void)vehicleWasFinished:(id)sender {

    if ([sender isKindOfClass:[ClickToLearnVehicle class]]) {
        
        [self performSelector:@selector(gotoNextPage) withObject:nil afterDelay:0.0];

    }
    else {
        
        [self performSelector:@selector(gotoNextPage) withObject:nil afterDelay:0.0];
    }
    
    
}


#pragma mark - Vehicle delegate methods

- (void)gotoPreviousPage {
    if(player){
        [player stop];
        [self removeActivityIndicator];
        [player removeFromSuperview];
        player = nil;
    }
    CGFloat xOffset = mainScrollView.contentOffset.x;
    NSInteger currentPage = xOffset/DEVICE_BIGER_SIDE;
    if(currentPage < 1) {
        
        leftArrowButton.userInteractionEnabled = YES;
        return;
    }
    
    if (currentPage-1 == 0) {
        
        leftArrowButton.hidden = YES;
    }
    
    if (currentPage == vehiclesArray.count) {
        
        //to show the right button
        rightArrowButton.hidden = NO;
    }
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        [mainScrollView setContentOffset:CGPointMake((currentPage-1)*DEVICE_BIGER_SIDE, 0) animated:NO];
        
    } completion:^(BOOL finished) {
        
        if(lastCurrentPage>=0){
            if(lastCurrentPage < [vehiclesArray count]){
                
                BaseViewVehicle *lastVehicle = [vehiclesArray objectAtIndex:lastCurrentPage];
                
//                NSLog(@"-------------------------------------------------------------->>>>>> DEACTIVIRAM - %ld", lastCurrentPage);
                
                [lastVehicle deactivateVehicle];
                currentProgressLessonObject.currentVehicle = (int)lastCurrentPage;
                [DATA_MANAGER saveData];
            }
        }
        [self activateNewViecle2];
        leftArrowButton.userInteractionEnabled = YES;

        
    }];

    
    
    //[mainScrollView scrollRectToVisible:CGRectMake((currentPage-1)*DEVICE_BIGER_SIDE, 0, DEVICE_BIGER_SIDE, mainScrollView.frame.size.height) animated:YES];

     [self removeSwipeGesture];
}

- (void)gotoNextPage {
    
    if(player){
        [player stop];
        [self removeActivityIndicator];
        [player removeFromSuperview];
        player = nil;
    }
    CGFloat xOffset = mainScrollView.contentOffset.x;
    NSInteger currentPage = xOffset/DEVICE_BIGER_SIDE;
    
    if (currentPage+1 == vehiclesArray.count) {
        
//        TO DO: logic for SUMMARY PAGE
        
        rightArrowButton.userInteractionEnabled = YES;
        rightArrowButton.hidden = YES;
        
    }
    else if (currentPage+1 > vehiclesArray.count) {
        
        rightArrowButton.userInteractionEnabled = YES;
        return;
        
    }
    
    if (currentPage+1 == 1) {
        
        //to show the left button
        leftArrowButton.hidden = NO;
    }
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        [mainScrollView setContentOffset:CGPointMake((currentPage+1)*DEVICE_BIGER_SIDE, 0) animated:NO];

    } completion:^(BOOL finished) {

        [self activateNewViecle];
        rightArrowButton.userInteractionEnabled = YES;

    }];
    
    if (currentPage+1 == vehiclesArray.count) {
        
        [self fakeSummaryScreen];
    }
    
    [self removeSwipeGesture];
    //[mainScrollView scrollRectToVisible:CGRectMake((currentPage+1)*DEVICE_BIGER_SIDE, 0, [mainScrollView width], [mainScrollView height]) animated:YES];
}


- (void)addPoints:(int)points {
    totalScore += points;
    currentProgressUserObject.score += points;
    pointForVipScopre += points;

    
    while(pointForVipScopre >= kParam007){
        //vipScopre += kParam015;
        currentProgressUserObject.vipPasses += kParam015;
        pointForVipScopre -= kParam007;
    }
    [self updateLabels];
}
- (void)updateLabels{
    //scoreLabel.text = [NSString stringWithFormat:@"%d",totalScore];
    scoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
    vipScoreLabel.text = [NSString stringWithFormat:@"%d",currentProgressUserObject.vipPasses];
}

- (void)enableScrolling:(id)sender{
    
    
}

- (void)disableScrolling:(id)sender{
    
    
}


- (void)enablePageControlButtons{
    
    
}

- (void)disablePageControlButtons{
    
    
}


#pragma mark - ScrollView Delegate methods




- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
}


- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    //NSLog(@"scrollViewDidEndScrollingAnimation");
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self activateNewViecle];
    
}


-(void)activateNewViecle {
    /*initialConstantForBottomConstrains = [mainScrollView height]+20 ;
    if(instructionViewBottomConstrains.constant == 0){
        //close
        instructionViewBottomConstrains.constant = initialConstantForBottomConstrains;
        instructionViewTopConstrains.constant = -initialConstantForBottomConstrains;
        [UIView animateWithDuration:0.4f animations:^{
            [self.view layoutIfNeeded];
        }];
    }*/
    
    
    
    if(lastCurrentPage>=0){
        if(lastCurrentPage < [vehiclesArray count]){

            BaseViewVehicle *lastVehicle = [vehiclesArray objectAtIndex:lastCurrentPage];
            [lastVehicle deactivateVehicle];
            currentProgressLessonObject.currentVehicle = (int)lastCurrentPage;
            [DATA_MANAGER saveData];
        }
    }
    
    CGFloat xOffset = mainScrollView.contentOffset.x;
    NSInteger currentPage = xOffset/DEVICE_BIGER_SIDE;
    NSInteger nextPage = currentPage+1;
    
    if (currentPage == 0) {
        leftArrowButton.hidden = YES;
    }
    else {
        leftArrowButton.hidden = NO;
    }
    if (currentPage == vehiclesArray.count) {
        rightArrowButton.hidden = YES;
    }
    else {
        rightArrowButton.hidden = NO;
    }
    
    if(currentPage < [vehiclesArray count]){
        BaseViewVehicle *currentVehicle = [vehiclesArray objectAtIndex:currentPage];
        if ([currentVehicle isKindOfClass:[VideoVehicle class]]) {
            [self addActivityIndicator];
            [self addVideoVehiclesIndicator];
        }
        if ([currentVehicle isKindOfClass:[FillMissingWordsVehicle class]]) {
            mainScrollView.scrollEnabled = NO;
        }
        [currentVehicle activateVehicle];
        
        
        currentProgressLessonObject.currentVehicle = (int)currentPage;
        if((int)currentPage > currentProgressLessonObject.maxVehicle){
            currentProgressLessonObject.maxVehicle = (int)currentPage;
        }
        
        ProgressVehicleObject *tempProgressVehicle =  [currentProgressLessonObject getCurrentProgressVehicleObject];
        tempProgressVehicle.achived = YES;
        
        [DATA_MANAGER saveData];
    }
    if(nextPage < [vehiclesArray count]){
        BaseViewVehicle *nextVehicle = [vehiclesArray objectAtIndex:nextPage];
        if ([nextVehicle isKindOfClass:[VideoVehicle class]]) {
            VideoVehicle *tempVehicle = (VideoVehicle*)nextVehicle;
            [self preloadVideoWithUrl:tempVehicle.videoUrlString];
        }
    }
    float addProgressPercent = 0.0;
    
    if(lastCurrentPage<currentPage){
        addProgressPercent = 0.04;
    }else{
        addProgressPercent = -0.04;
    }
    
    if (currentPage+1 == vehiclesArray.count) {
        
        [self fakeSummaryScreen];
    }
    
    [currentLessonProgress addProgres:addProgressPercent];
    lastCurrentPage = currentPage;
}

- (void)activateNewViecle2 {
    
    CGFloat xOffset = mainScrollView.contentOffset.x;
    NSInteger currentPage = xOffset/DEVICE_BIGER_SIDE;
    NSInteger nextPage = currentPage;
    if(currentPage < [vehiclesArray count]){
        BaseViewVehicle *currentVehicle = [vehiclesArray objectAtIndex:currentPage];
        if ([currentVehicle isKindOfClass:[VideoVehicle class]]) {
            [self addActivityIndicator];
        }
        if ([currentVehicle isKindOfClass:[FillMissingWordsVehicle class]]) {
            mainScrollView.scrollEnabled = NO;
        }
        [currentVehicle activateVehicle];
//        NSLog(@"-------------------------------------------------------------->>>>>> ACTIVIRAM - %ld", currentPage);

        
        currentProgressLessonObject.currentVehicle = (int)currentPage;
        if((int)currentPage > currentProgressLessonObject.maxVehicle){
            currentProgressLessonObject.maxVehicle = (int)currentPage;
        }
        
        ProgressVehicleObject *tempProgressVehicle =  [currentProgressLessonObject getCurrentProgressVehicleObject];
        tempProgressVehicle.achived = YES;
        
        [DATA_MANAGER saveData];
    }
    if(nextPage < [vehiclesArray count]){
        BaseViewVehicle *nextVehicle = [vehiclesArray objectAtIndex:nextPage];
        if ([nextVehicle isKindOfClass:[VideoVehicle class]]) {
            VideoVehicle *tempVehicle = (VideoVehicle*)nextVehicle;
            [self preloadVideoWithUrl:tempVehicle.videoUrlString];
        }
    }
    float addProgressPercent = 0.0;
    
    if(lastCurrentPage<currentPage){
        addProgressPercent = 0.04;
    }else{
        addProgressPercent = -0.04;
    }
    [currentLessonProgress addProgres:addProgressPercent];
    lastCurrentPage = currentPage;
}


- (void)setQuestionBarText:(NSString *)newText {

    //New
//    NSDictionary *font = [FontManager takeFontSizeForQuestionArea:newText];
//    [questionBarLabel setFont:[UIFont systemFontOfSize:[[font objectForKey:@"size"] floatValue]]];
//    [questionBarLabel setNumberOfLines:[[font objectForKey:@"line"] integerValue]];
    
    questionBarLabel.text = newText;
    questionBarVocabView.hidden = YES;
    questionBarLabel.hidden = NO;

//    [questionBarView setNeedsUpdateConstraints];
//    [questionBarView updateConstraints];

}

- (void)setQuestionBarTextWithVocab:(NSString *)newText {
    
    [questionBarVocabView cleanControl];
    
    NSDictionary *font = [FontManager takeFontSizeForQuestionArea:newText];
    
    [questionBarVocabView buildTextViewFromString:newText withFont:[font objectForKey:@"size"]];
    [questionBarVocabView createUX];
    
    questionBarVocabView.hidden = NO;
    questionBarLabel.hidden = YES;
    
}


#pragma mark - Back to home page

- (IBAction)goToHomePage:(id)sender {
    
    [self deactivateCurrentVehicle];
    
    [AUDIO_MANAGER stopPlaying];
    
    [self updateProgressOnExit];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"openLessonSession"];
    [defaults synchronize];
    
    [APP_DELEGATE buildHomePageStack];
}

//for applicationDidEnterBackground
- (void)pauseCurrentVehicle {
    if([vehiclesArray count] > lastCurrentPage ){
        BaseViewVehicle *lastVehicle = [vehiclesArray objectAtIndex:lastCurrentPage];
        if ([lastVehicle isKindOfClass:[SpellingVehicle class]]) {
            
        }else{
            [lastVehicle deactivateVehicle];
        }
    }
}

- (void)deactivateCurrentVehicle {
    if([vehiclesArray count] > lastCurrentPage ){
        BaseViewVehicle *lastVehicle = [vehiclesArray objectAtIndex:lastCurrentPage];
        [lastVehicle deactivateVehicle];
    }
}

-(void)updateProgressOnExit{
    NSLog(@"currentProgressLessonObject.scoreForLesson %d",currentProgressLessonObject.scoreForLesson);
    currentProgressLessonObject.scoreForLesson = totalScore;
    NSLog(@"currentProgressLessonObject.scoreForLesson %d",currentProgressLessonObject.scoreForLesson);
    [DATA_MANAGER saveData];
}
- (void)showCorrectSpellingFeedback:(id)sender {
    
    SpellingVehicle *spellVehicle = (SpellingVehicle *)sender;
    SpellingTextField *spellTextField = (SpellingTextField *)[spellVehicle currentTextField];
    
    NSInteger tag = spellTextField.tag+1;
    [spellVehicle goToNextTextField:@(tag)];

}

-(void)fakeSummaryScreen{
    currentProgressLevelObject.currentUnit = (int)self.lessonNumber+1;
    if ((currentProgressLevelObject.currentUnit) > currentProgressLevelObject.maxUnit){
        currentProgressLevelObject.maxUnit = currentProgressLevelObject.currentUnit;
    }
}

#pragma mark Video Vehicle
- (void)preloadVideoWithUrl:(NSString*)videoUrl {
    if(preloadPlayer && [[preloadPlayer.contentURL absoluteString] isEqualToString:videoUrl]){
        return;
    }else if(preloadPlayer){
        preloadPlayer = nil;
    }
    preloadPlayer = [[KSVideoPlayerView alloc] initWithFrame:self.view.frame
                                           contentURL:[NSURL URLWithString:videoUrl]];
}

- (void)showVideoWithUrl:(NSString*)videoUrl and:(BOOL)automaticGoNext{
    [self.view insertSubview:currentLessonProgress aboveSubview:questionBarView];
    [self.view insertSubview:leftArrowButton aboveSubview:currentLessonProgress];
    [self.view insertSubview:rightArrowButton aboveSubview:leftArrowButton];
    [self addVideoVehiclesIndicator];
    if(player)
        return;
    if(preloadPlayer && [[preloadPlayer.contentURL absoluteString] isEqualToString:videoUrl]){
        player = preloadPlayer;
        [self removeActivityIndicator];
        [self removeVideoIndicator];
    }else{
        /*player = [[KSVideoPlayerView alloc] initWithFrame:self.view.frame
                                               contentURL:[NSURL URLWithString:videoUrl]];*/
         if(preloadPlayer){
             preloadPlayer = nil;
        }
        preloadPlayer = [[KSVideoPlayerView alloc] initWithFrame:self.view.frame
                                                      contentURL:[NSURL URLWithString:videoUrl]];
        player = preloadPlayer;
    }
    player.delegate = self;
    player.automaticGoToNextVehicle = automaticGoNext;
    //[self.view addSubview:player];
    [self.view insertSubview:player belowSubview:leftArrowButton];
    player.tintColor = [UIColor redColor];
    [player play];
    //[NSTimer scheduledTimerWithTimeInterval:3.3 target:self selector:@selector(playVideo) userInfo:nil repeats:NO];
    //[self addActivityIndicator];
    [self addSwipeGesture];
    
}

- (void)playVideo{
    if(player)
        [player play];
}
#pragma mark playerViewDelegate
-(void)hideArrows{
    CGFloat xOffset = mainScrollView.contentOffset.x;
    NSInteger currentPage = xOffset/DEVICE_BIGER_SIDE;
    NSInteger nextPage = currentPage+1;
    if(currentPage < [vehiclesArray count]){
        BaseViewVehicle *currentVehicle = [vehiclesArray objectAtIndex:currentPage];
        if ([currentVehicle isKindOfClass:[VideoVehicle class]]) {
            
            [UIView animateWithDuration:0.3 animations:^{
                leftArrowButton.layer.opacity = 0;
                rightArrowButton.layer.opacity = 0;
            }];
        }
    }
}

-(void)showArrows{
    [UIView animateWithDuration:0.3 animations:^{
        leftArrowButton.layer.opacity = 1;
        rightArrowButton.layer.opacity = 1;
    }];
}

-(void)videoFinishedPlaying{
    [self removeActivityIndicator];
    [self removeVideoIndicator];
    [player stop];
    [player removeFromSuperview];
    player = nil;
    [self removeSwipeGesture];
    [self gotoNextPage];
    
}
-(void)playerLoadVideo{
    [self removeActivityIndicator];
    [self removeVideoIndicator];
}
#pragma mark - Left swipe action

- (void)leftSwipeActionScorllContainer:(id)sender {
    
    //[self nextButtonAction:self.lmPagingControl withSpeed:0.5];
    if(player){
        [self videoFinishedPlaying];
    }
    
}

- (void)rightSwipeActionScorllContainer:(id)sender {
    [self removeSwipeGesture];
    [self gotoPreviousPage];
    // [self previousButtonSelected:self.lmPagingControl withSpeed:0.5];
    
}

#pragma mark UIGestureRecognizerDelegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return true;
}

#pragma mark Interface functions

- (IBAction)openVipZone:(id)sender {
    
    [self deactivateCurrentVehicle];
    [AUDIO_MANAGER stopPlaying];
    [self updateProgressOnExit];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"openLessonSession"];
    [defaults synchronize];


    [self.view bringSubviewToFront:loadingView];
    loadingView.hidden = NO;
    [activityIndicatorLoading startAnimating];
    
    //donwload resoureces
    NSArray *dataArray = [DATA_MANAGER getAllVipCategory];
    
    [CACHE_MANAGER setDelegate:self];
    [CACHE_MANAGER startDownloadingAndCachingResources:dataArray];
    
}

#pragma mark - Cache Manager Delegate
- (void)cacheManagerFinishedDownloading:(id)sender {
    
    NSLog(@"========= Images were downloaded =========");
    
    [APP_DELEGATE buildVipZoneStack];
    
    [activityIndicatorLoading stopAnimating];
    loadingView.hidden = YES;
}

- (IBAction)pressInstructionButton:(id)sender {
    initialConstantForBottomConstrains = [mainScrollView height]+20 ;
    if(instructionViewBottomConstrains.constant != 0){
        //open
        instructionViewBottomConstrains.constant = 0;
        instructionViewTopConstrains.constant = 0;
        [self.view insertSubview:leftArrowButton belowSubview:instructionView];
        [self.view insertSubview:rightArrowButton belowSubview:instructionView];
        [self.view insertSubview:currentLessonProgress belowSubview:instructionView];
        
    }else{
        //close
        instructionViewBottomConstrains.constant = initialConstantForBottomConstrains;
        instructionViewTopConstrains.constant = -initialConstantForBottomConstrains;
        float delayInSeconds = 0.5;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.view insertSubview:currentLessonProgress aboveSubview:questionBarView];
            [self.view insertSubview:leftArrowButton aboveSubview:currentLessonProgress];
            [self.view insertSubview:rightArrowButton aboveSubview:leftArrowButton];
        });
        
    }
    [UIView animateWithDuration:0.4f animations:^{
        [self.view layoutIfNeeded];
    }];
}
- (IBAction)leftButtonPress:(id)sender {
    
    leftArrowButton.userInteractionEnabled = NO;
    [self gotoPreviousPage];
}
- (IBAction)rightButtonPress:(id)sender {
    
    rightArrowButton.userInteractionEnabled = NO;
    [self gotoNextPage];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (BOOL)shouldAutorotate {
    
    return YES;
}

@end
