//
//  LMQuizProgressBar.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 10/19/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LMQuizProgressBar : UIView

@property (nonatomic, assign) NSInteger currentDotIndex;
@property (nonatomic) NSInteger numberOfDots;
@property (nonatomic, assign) NSInteger correctAnswers;

@property (nonatomic, strong) UILabel *correctAnswersLabel;
@property (nonatomic, strong) UILabel *totalQuestionsLabel;

- (instancetype)initWithFrame:(CGRect)frame withNumberOfDots:(NSInteger)dotsNumber;
- (void)changeStatus:(NSInteger)status forDotWithIndex:(NSInteger)dotIndex;

@end
