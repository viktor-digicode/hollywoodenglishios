//
//  SwipeMovingView.m
//  NavigationHollywood
//
//  Created by Dimitar Shopovski on 3/21/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import "SwipeMovingView.h"
#import "UILabel+FontSizeToFit.h"

@implementation SwipeMovingView
@synthesize swipeGestureLeft;
@synthesize swipeGestureRight;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data {
    
    if (self = [super initWithFrame:frame]) {
        
        BOOL isTextAnswer = [[data objectForKey:@"isText"] boolValue];
        _isTextElement = isTextAnswer;
        _movingViewData = data;
    }
    
    return self;
}

/*- (void)drawRect:(CGRect)rect {

    if (_isTextElement) {
        
        if (labelAnswer == nil) {
            
            labelAnswer = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
            [labelAnswer setTextColor:[UIColor whiteColor]];
            labelAnswer.text = @"Test test";
            [self addSubview:labelAnswer];
        }
        
        labelAnswer.numberOfLines = 0;
        labelAnswer.text = [_movingViewData objectForKey:@"textAnswer"];
        labelAnswer.frame = CGRectMake(labelAnswer.frame.origin.x, labelAnswer.frame.origin.y, self.frame.size.width, 21);
        [labelAnswer sizeToFit];
        
    }
    else {
        
        imageViewAnswer = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [self addSubview:imageViewAnswer];
        
        imageViewAnswer.image = [UIImage imageNamed:[_movingViewData objectForKey:@"imageAnswer"]];

    }
    
    swipeGestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftGestureAction:)];
    swipeGestureLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeGestureLeft.delegate = self;
    swipeGestureLeft.delaysTouchesBegan = YES;
    swipeGestureLeft.cancelsTouchesInView = YES;
    [self addGestureRecognizer:swipeGestureLeft];
    
    swipeGestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeAction:)];
    swipeGestureRight.direction = UISwipeGestureRecognizerDirectionRight;
    swipeGestureRight.delegate = self;
    swipeGestureRight.delaysTouchesBegan = YES;
    swipeGestureRight.cancelsTouchesInView = YES;
    [self addGestureRecognizer:swipeGestureRight];
    
}*/

- (int) wordCount:(NSString*)newString
{
    NSArray *words = [newString componentsSeparatedByString:@""];
    return (int)[words count];
}

- (void)setText:(NSString*)newText{
    
    _isTextElement= YES;
   
    labelAnswer.text = newText;
    
    NSDictionary *fontDictionary = [FontManager takeFontSize:newText forVehicle:KASwipeVehicleSwipeTextType];
    int fontSize = [[fontDictionary objectForKey:@"size"] floatValue];
    labelAnswer.font = [UIFont systemFontOfSize:fontSize];
    if([newText wordCount] == 1){
        labelAnswer.numberOfLines = 1;
    }
    [labelAnswer fontSizeToFit];
    
}

- (void)setImage:(NSString*)imgString{
    
    imageViewAnswer.image = [UIImage imageNamed:imgString];
}

- (void)makeTextLabel:(CGRect)frame{
    UIView *helpArea =  [[UIView alloc]initWithFrame:frame];
    [helpArea setBackgroundColor:color(1, 1, 1, 0.48)];
    //[self addSubview:helpArea];
     [self insertSubview:helpArea atIndex:0];
    [helpArea autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0.0f];
    [helpArea autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0.0f];
    [helpArea autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0.0f];
    [helpArea autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0.0f];
    
    labelAnswer = [[UILabel alloc] initWithFrame:frame];
    labelAnswer.textAlignment = NSTextAlignmentCenter;
    labelAnswer.lineBreakMode = NSLineBreakByWordWrapping;
    labelAnswer.backgroundColor = [UIColor clearColor];
    labelAnswer.numberOfLines = 0;
    labelAnswer.textColor = [UIColor whiteColor];
    labelAnswer.font = [UIFont systemFontOfSize:30];
    [self addSubview:labelAnswer];
    [labelAnswer fontSizeToFit];
    
    
    [labelAnswer autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:10.0f];
    [labelAnswer autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10.0f];
    
    if(DEVICE_BIGER_SIDE < 568){
        [labelAnswer autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:6.0f];
        [labelAnswer autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:6.0f];
    }else{
        [labelAnswer autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:12.0f];
        [labelAnswer autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:12.0f];
    }
   /* labelAnswer.numberOfLines = 3;
    labelAnswer.adjustsFontSizeToFitWidth = YES;
    //labelAnswer.lineBreakMode = NSLineBreakByClipping;
    */
}
- (void)makeImage:(CGRect)frame{
    imageViewAnswer = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    //[self addSubview:imageViewAnswer];
    [self insertSubview:imageViewAnswer atIndex:0];
    [imageViewAnswer autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
    [imageViewAnswer autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
    [imageViewAnswer autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [imageViewAnswer autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [imageViewAnswer setContentMode:UIViewContentModeScaleAspectFill];

}
#pragma mark - UIGestureRecognizer Actions

- (IBAction)leftGestureAction:(id)sender {
    
    [self.swipeViewDelegate leftSwipeDetected:self];
    
}

- (IBAction)rightSwipeAction:(id)sender {
    
    [self.swipeViewDelegate rightSwipeDetected:self];
}

#pragma mark - Gesture Recognizer Delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return YES;
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return YES;
}

@end
