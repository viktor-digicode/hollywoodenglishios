//
//  ECVehicleDisplay.m
//  TransitionsTest
//
//  Created by Dimitar Shopovski on 11/13/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "ECVehicleDisplay.h"

@implementation ECVehicleDisplay



- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
    }
    
    return self;
}



#pragma mark - Reset instance and completed instance layout


- (void)resetTheInstance {
    
}

- (void)showLayout:(BOOL)forCompleted {
    self.isCompleted = forCompleted;
    self.isCompleted ? [self showLayoutForCompletedInstance] : [self showLayoutForNewVehicle];
}

- (void)showLayoutForCompletedInstance {
    
    
}

- (void)showLayoutForNewVehicle {
    
}

#pragma mark enable/Disable
- (void) activeteVehicle{
    //Vehicle is shown on phone screen
    self.isCurrent = YES;
}

- (void) deactiveteVehicle{
    //Vehicle is dismist from phone screen
    //[AUDIO_MANAGER stopRecording];
    //[AUDIO_MANAGER stopPlaying];
    self.isCurrent = NO;
}

- (void)disableVehicleInteractions {
    self.isActive = NO;
}

- (void)enableVehicleInteractions {
    self.isActive = YES;
}

- (void)feedSwipe
{

}

- (void)updateViewConstraints{
    
}

@end
