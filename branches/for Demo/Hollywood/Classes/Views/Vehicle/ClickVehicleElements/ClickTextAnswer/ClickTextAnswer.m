//
//  ClickTextAnswer.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 4/7/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "ClickTextAnswer.h"

@implementation ClickTextAnswer
@synthesize labelWord;
@synthesize viewContainerLabelWord;
@synthesize imageViewFeedback;
@synthesize buttonSound;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data type:(BOOL)type {
    
    if (self = [super initWithFrame:frame]) {
        
        viewContainerLabelWord = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [viewContainerLabelWord setBackgroundColor:[UIColor blackColor]];
        viewContainerLabelWord.layer.cornerRadius = 4.0;
        viewContainerLabelWord.layer.borderColor = [UIColor whiteColor].CGColor;
        viewContainerLabelWord.layer.borderWidth = 1.0;
        viewContainerLabelWord.userInteractionEnabled = NO;
        viewContainerLabelWord.layer.masksToBounds = YES;
        [self addSubview:viewContainerLabelWord];
        
        //button sound
        
        float soundButtonSpace = 10.0;
        float wordOffset = 3.0;

        if (![data isKindOfClass:[QuizInstanceAnswer class]]) {
            buttonSound = [UIButton buttonWithType:UIButtonTypeCustom];
            [buttonSound setImage:[UIImage imageNamed:@"sound-mct"] forState:UIControlStateNormal];
            [buttonSound setFrame:CGRectMake(frame.size.width-38, (frame.size.height-34)/2, 34, 34)];
            [buttonSound setAlpha:0.64];
            [buttonSound addTarget:self action:@selector(playAnswerSound:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:buttonSound];
            
            soundButtonSpace = 38.0;
        }

        
        labelWord = [[UILabel alloc] initWithFrame:CGRectMake(wordOffset, 5, frame.size.width-soundButtonSpace-wordOffset, frame.size.height)];
        [labelWord setFont:[UIFont systemFontOfSize:18.0]];
        labelWord.userInteractionEnabled = NO;
        [labelWord setTextColor:[UIColor whiteColor]];
        labelWord.numberOfLines = 0;
        if ([data isKindOfClass:[QuizInstanceAnswer class]]) {
            labelWord.text = [(QuizInstanceAnswer *)data answerText];

        }
        else {
            labelWord.text = [data objectForKey:@"text"];

        }
        [labelWord sizeToFit];
        [self addSubview:labelWord];
        
        if (labelWord.height > frame.size.height) {
            
            float tempFontSize = 18.0;
            
            do {
                
                labelWord.frame = CGRectMake(wordOffset, 5, frame.size.width-soundButtonSpace-wordOffset, frame.size.height);
                [labelWord setFont:[UIFont systemFontOfSize:tempFontSize]];
                if ([data isKindOfClass:[QuizInstanceAnswer class]]) {
                    labelWord.text = [(QuizInstanceAnswer *)data answerText];
                    
                }
                else {
                    labelWord.text = [data objectForKey:@"text"];
                    
                }
                [labelWord sizeToFit];
                
                labelWord.frame = CGRectMake(wordOffset, (frame.size.height-labelWord.height)/2, labelWord.width, labelWord.height);
                
                tempFontSize -= 1.0;
                
            } while (labelWord.height > frame.size.height);
            
        }
        else {
            
            labelWord.frame = CGRectMake(wordOffset, (frame.size.height-labelWord.height)/2, labelWord.width, labelWord.height);

        }

        
        _answerWasShown = NO;
        
        imageViewFeedback = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        imageViewFeedback.hidden = YES;
        imageViewFeedback.layer.cornerRadius = 4.0;
        imageViewFeedback.layer.masksToBounds = YES;
        imageViewFeedback.alpha = 0.6;
        imageViewFeedback.userInteractionEnabled = NO;

        [self addSubview:imageViewFeedback];
        
        self.dataAnswer = data;
        self.isAnswer = type;
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
        [self addGestureRecognizer:tapGesture];
        
    }
    
    return self;
}

#pragma mark - Answer sound
- (IBAction)playAnswerSound:(id)sender {
    
    if (![self.dataAnswer isKindOfClass:[QuizInstanceAnswer class]]) {
        [AUDIO_MANAGER playSoundFromAudioFile:[self.dataAnswer objectForKey:@"sound"]];
    }
    else {
        
        
    }
}

#pragma mark - Tap Gesture action

- (IBAction)tapGestureAction:(id)sender {
    
    if (self.isAnswer) {
        
        if ([self.dataAnswer isKindOfClass:[QuizInstanceAnswer class]]) {
            
            if ([(QuizInstanceAnswer *)self.dataAnswer isCorrect]) {
                
                [self.delegateClickText textAnswerWasClicked:self correctAnswer:YES];
            }
            else {
                
                [self.delegateClickText textAnswerWasClicked:self correctAnswer:NO];
            }
        }
        else {
            
            if ([[self.dataAnswer objectForKey:@"isCorrect"] boolValue]) {
                
                [self.delegateClickText textAnswerWasClicked:self correctAnswer:YES];
            }
            else {
                
                [self.delegateClickText textAnswerWasClicked:self correctAnswer:NO];
            }
        }
    }
    
    _answerWasShown = !_answerWasShown;
}

- (void)showFeedbackWithStatus:(BOOL)correct {
    
    if (correct) {
     
        self.layer.cornerRadius = 4.0;
        self.layer.borderColor = APP_CORRECT_COLOR.CGColor;
        self.layer.borderWidth = 2.0;
    }
    else {
        
        self.layer.cornerRadius = 4.0;
        self.layer.borderColor = APP_INCORRECT_COLOR.CGColor;
        self.layer.borderWidth = 2.0;
    }
}

- (void)showFeedbackWithStatus {
    
    if ([self.dataAnswer isKindOfClass:[QuizInstanceAnswer class]]) {

        if ([(QuizInstanceAnswer *)self.dataAnswer isCorrect]) {
            
            self.layer.cornerRadius = 4.0;
            self.layer.borderColor = APP_CORRECT_COLOR.CGColor;
            self.layer.borderWidth = 2.0;
        }
        else {
            
            self.layer.cornerRadius = 4.0;
            self.layer.borderColor = APP_INCORRECT_COLOR.CGColor;
            self.layer.borderWidth = 2.0;
        }
    }
    else {
        
        if ([[self.dataAnswer objectForKey:@"isCorrect"] boolValue]) {
            
            self.layer.cornerRadius = 4.0;
            self.layer.borderColor = APP_CORRECT_COLOR.CGColor;
            self.layer.borderWidth = 2.0;
        }
        else {
            
            self.layer.cornerRadius = 4.0;
            self.layer.borderColor = APP_INCORRECT_COLOR.CGColor;
            self.layer.borderWidth = 2.0;
        }
    }
}

- (void)hideAnswer {
    
    imageViewFeedback.hidden = YES;
    _answerWasShown = NO;
    
    viewContainerLabelWord.layer.cornerRadius = 4.0;
    viewContainerLabelWord.layer.borderColor = [UIColor whiteColor].CGColor;
    viewContainerLabelWord.layer.borderWidth = 1.0;
    
    self.layer.cornerRadius = 4.0;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 1.0;
}


@end
