//
//  ClickImageView.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 3/30/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ClickImageViewDelegate <NSObject>

@optional

- (void)answerWasClicked:(id)sender;
- (void)answerWasClicked:(id)sender changeImage:(NSString *)image;

@end



@interface ClickImageView : UIView

@property (nonatomic, strong) id<ClickImageViewDelegate> delegateClickImageView;

//data
@property (nonatomic, assign) BOOL answerWasShown;
@property (nonatomic, assign) BOOL isLearnOrAnswer;
@property (nonatomic, assign) BOOL isOneImageLayout;
@property (nonatomic, assign) BOOL answerIsClosed;

@property (nonatomic, strong) id dataAnswer;


//visual elements
@property (nonatomic, weak) IBOutlet UIImageView *imageViewContent;
@property (nonatomic, weak) IBOutlet UIView *viewContainerTargetWord;
@property (nonatomic, weak) IBOutlet UIView *viewTargetLabelMask;
@property (nonatomic, weak) IBOutlet UILabel *labelTargetWord;

@property (nonatomic, weak) IBOutlet UIView *viewContainerSourceText;
@property (nonatomic, weak) IBOutlet UILabel *labelSourceText;


@property (nonatomic, weak) IBOutlet UIView *viewFeedback;
@property (nonatomic, weak) IBOutlet UIImageView *imageViewFeedback;
@property (nonatomic, weak) IBOutlet UIImageView *imageViewFeedbackSmall;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data type:(BOOL)isLearn isOneImage:(BOOL)isOneImage withFontSize:(CGFloat)fontSize;
- (void)showFeedbackWithStatus;
- (void)hideAnswer;

@end
