//
//  SwipeKircaVehicle.m
//  Hollywood
//
//  Created by Kiril Kiroski on 3/31/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "SwipeKircaVehicle.h"
#import "FLAnimatedImage.h"

@implementation SwipeKircaVehicle{
    UIImageView *leftLineImage;
    UIImageView *rightLineImage;
    UIView *handView;
    int corectDistance;
}

- (instancetype)initWithFrame:(CGRect)frame data:(id)data {
    
    if (self = [super initWithFrame:frame]) {
        //4 135
        //5 169
        //6 199
        corectDistance = frame.size.width * 0.299;
        NSLog(@"corectDistance %d",corectDistance);
        if(DEVICE_BIGER_SIDE < 568){
            corectDistance = 126;
        }
        _vehicleData = data;
        self.afterAudioString = [_vehicleData objectForKey:@"afterAudio"];
        //self.afterAudioString = nil;
        
        self.beforeAudioString  = [_vehicleData objectForKey:@"beforeAudio"];
        
        swipeDestinationViewLeft = [[SwipeDestinationView alloc] initWithFrame:CGRectMake(0, 0, 70, 200)];
        //[swipeDestinationViewLeft setBackgroundColor:color(112, 103, 56, 0.48)];
        [self addSubview:swipeDestinationViewLeft];
       
        
        swipeDestinationViewRight = [[SwipeDestinationView alloc] initWithFrame:CGRectMake(frame.size.width-70, 0, 70, 200)];
        //[swipeDestinationViewRight setBackgroundColor:color(112, 103, 56, 0.48)];
        [self addSubview:swipeDestinationViewRight];
        
        swipeHolderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 233, 170)];
 
        [swipeHolderView setBackgroundColor:color(112, 103, 56, 0.5)];
        [self addSubview:swipeHolderView];
        

        
        //hand image
        /*leftLineImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Swipe Left"]];
        [swipeHolderView addSubview:leftLineImage];
       
        
        
        rightLineImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Swipe Right"]];
        [swipeHolderView addSubview:rightLineImage];
         */

        
        
        NSDictionary *dictMovingView = [_vehicleData objectForKey:@"movingViewData"];
        swipeMovingView = [[SwipeMovingView alloc] initWithFrame:CGRectMake(0, 1, 233, 168) data:dictMovingView];
        swipeMovingView.swipeViewDelegate = self;
        [self addSubview:swipeMovingView];
        
        /*UIView *helpAreaForMovingView =  [[UIView alloc]initWithFrame:CGRectMake(-233/2, 0, 233, 168)];
        [helpAreaForMovingView setBackgroundColor:[UIColor redColor]];
        [swipeMovingView addSubview:helpAreaForMovingView];
        */
        
        swipeMovingView.userInteractionEnabled = NO;
        swipeMovingView.isTextElement = NO;
        [swipeMovingView autoSetDimension:ALDimensionWidth toSize:[self width]*0.18];//115
        [swipeMovingView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:20+11];
        [swipeMovingView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20+11];
        //[swipeMovingView autoSetDimension:ALDimensionHeight toSize:168];
        [swipeMovingView setBackgroundColor:[UIColor clearColor]];
        self.dragViewX = [swipeMovingView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:self.frame.size.width/2 - (([self width]*0.18)/2)];
        //[swipeMovingView autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
        
        handView = [[UIView alloc]initWithFrame:CGRectMake(0, [swipeMovingView height]-30, [self width]*0.18, 30)];
        handView.backgroundColor = color(0,0,0,0.5);
        handView.userInteractionEnabled = NO;
        [swipeMovingView addSubview:handView];
        //UIImageView *handImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"SwipeHand"]];
        //handImage.userInteractionEnabled = NO;
        
        
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"HLWD_Arrow_" withExtension:@"gif"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:data];
        FLAnimatedImageView *leftImageView = [[FLAnimatedImageView alloc] init];
        leftImageView.animatedImage = image;
        leftImageView.frame = CGRectMake(15, 5, 25, 19);
        [handView addSubview:leftImageView];
        
        NSURL *url2 = [[NSBundle mainBundle] URLForResource:@"HLWD_Arrow_2Right" withExtension:@"gif"];
        NSData *data2 = [NSData dataWithContentsOfURL:url2];
        FLAnimatedImage *image2 = [FLAnimatedImage animatedImageWithGIFData:data2];
        FLAnimatedImageView *rightImageView = [[FLAnimatedImageView alloc] init];
        rightImageView.animatedImage = image2;
        rightImageView.frame = CGRectMake([handView width] - 25 - 15, 5, 25, 19);
        
         if(DEVICE_BIGER_SIDE < 568){
            leftImageView.frame = CGRectMake(5, 5, 25, 19);
             rightImageView.frame = CGRectMake([handView width] - 25 - 5, 5, 25, 19);
         }
        [handView addSubview:rightImageView];
        
        //[handView addSubview:handImage];
        //[handImage autoCenterInSuperview];
        //[handImage autoSetDimensionsToSize:CGSizeMake([handImage width], [handImage height])];
        
        panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move)];
        [panGesture setMinimumNumberOfTouches:1];
        [panGesture setMaximumNumberOfTouches:1];
        //[panGesture setDelegate:self];
        [swipeHolderView addGestureRecognizer:panGesture];
        
        [self setStyle];
        [self setupConstraints];
        
        self.swipeImageVehicle = [[_vehicleData objectForKey:@"swipeImageVehicle"] boolValue];
        [self setVehicleType];
    }
    
    return self;
}

-(void) setStyle{
    
    [[swipeMovingView layer]setMasksToBounds:TRUE];
    swipeMovingView.clipsToBounds = YES;
    [[swipeMovingView layer]setCornerRadius:2.0f];
    [[swipeMovingView layer]setBorderWidth:0.5f];
    [[swipeMovingView layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    [[swipeDestinationViewLeft layer]setMasksToBounds:TRUE];
    swipeDestinationViewLeft.clipsToBounds = YES;
    [[swipeDestinationViewLeft layer]setCornerRadius:2.0f];
    [[swipeDestinationViewLeft layer]setBorderWidth:0.5f];
    [[swipeDestinationViewLeft layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    [[swipeDestinationViewRight layer]setMasksToBounds:TRUE];
    swipeDestinationViewRight.clipsToBounds = YES;
    [[swipeDestinationViewRight layer]setCornerRadius:2.0f];
    [[swipeDestinationViewRight layer]setBorderWidth:0.5f];
    [[swipeDestinationViewRight layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    [[swipeHolderView layer]setMasksToBounds:TRUE];
    swipeHolderView.clipsToBounds = YES;
    [[swipeHolderView layer]setCornerRadius:2.0f];
    [[swipeHolderView layer]setBorderWidth:0.5f];
    [[swipeHolderView layer] setBorderColor:[UIColor whiteColor].CGColor];
    
    /*[leftLineImage autoSetDimension:ALDimensionWidth toSize:[leftLineImage width]];
    [leftLineImage autoSetDimension:ALDimensionHeight toSize:[leftLineImage height]];
    [leftLineImage autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    [leftLineImage autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:10];
    
    [rightLineImage autoSetDimension:ALDimensionWidth toSize:[rightLineImage width]];
    [rightLineImage autoSetDimension:ALDimensionHeight toSize:[rightLineImage height]];
    [rightLineImage autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    [rightLineImage autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:10];*/
    
}
- (void)setupConstraints{
    [swipeDestinationViewLeft autoSetDimension:ALDimensionWidth toSize:[self width]*0.18];
    //[swipeDestinationViewLeft autoSetDimension:ALDimensionHeight toSize:170.0];
    [swipeDestinationViewLeft autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:41+11];
    [swipeDestinationViewLeft autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:20+11];
    [swipeDestinationViewLeft autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20+11];
    //[swipeDestinationViewLeft autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    
    [swipeDestinationViewRight autoSetDimension:ALDimensionWidth toSize:[self width]*0.18];
    //[swipeDestinationViewRight autoSetDimension:ALDimensionHeight toSize:170.0];
    [swipeDestinationViewRight autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:41+11];
    [swipeDestinationViewRight autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:20+11];
    [swipeDestinationViewRight autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20+11];
    //[swipeDestinationViewRight autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    
    /*[swipeHolderView autoSetDimension:ALDimensionWidth toSize:[self width]-84];
    [swipeHolderView autoSetDimension:ALDimensionHeight toSize:[self height]-65];
    [swipeHolderView autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [swipeHolderView autoAlignAxisToSuperviewAxis:ALAxisHorizontal];*/
    
    [swipeHolderView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20];
    [swipeHolderView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:20];
    [swipeHolderView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:41];
    [swipeHolderView autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:41];
    
    [handView autoSetDimension:ALDimensionWidth toSize:[handView width]];
    [handView autoSetDimension:ALDimensionHeight toSize:[handView height]];
    //[handView autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    [handView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
    [handView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    
    
}
- (void)setVehicleType{
    if(self.swipeImageVehicle){
        [self makeSwipeImage];
    }else{
        [self makeSwipeText];
    }
}

-(void)randomiseText{
    NSArray *arrayAnswers = [_vehicleData objectForKey:@"answers"];
    int randomNum = arc4random_uniform(2);
    NSDictionary *rightDictionary = arrayAnswers[randomNum];
    NSDictionary *leftDictionary = arrayAnswers[1-randomNum];
    
    [swipeDestinationViewRight setText:[rightDictionary objectForKey:@"text"]];
    [swipeDestinationViewLeft  setText:[leftDictionary objectForKey:@"text"]];
    
    swipeDestinationViewRight.tag = [[rightDictionary objectForKey:@"isCorrect"]boolValue];
    swipeDestinationViewLeft.tag = [[leftDictionary objectForKey:@"isCorrect"]boolValue];
    
}
- (void)makeSwipeImage{
    
    [swipeHolderView setBackgroundColor:color(1, 1, 1, 0.48)];
    
    [swipeMovingView makeImage:swipeMovingView.frame];
    //[swipeMovingView setImage:@"swipeImage"];
    [swipeMovingView setImage:[swipeMovingView.movingViewData objectForKey:@"imageAnswer"]];
    [swipeMovingView setContentMode:UIViewContentModeScaleAspectFill];

    
    [swipeDestinationViewRight makeTextLabel:swipeDestinationViewRight.frame];
    [swipeDestinationViewLeft makeTextLabel:swipeDestinationViewRight.frame];
    
    
    
    [self insertSubview:swipeMovingView belowSubview:swipeDestinationViewLeft];
    [self insertSubview:swipeHolderView belowSubview:swipeMovingView];

    //return;
    UIImageView *downViewRight = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Swipe_text_bacground"]];
    [self addSubview:downViewRight];
    
    UIImageView *downViewLeft = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Swipe_text_bacground"]];
    [self addSubview:downViewLeft];
    
    
    [self insertSubview:downViewLeft belowSubview:swipeMovingView];
    [self insertSubview:downViewRight belowSubview:swipeMovingView];
    
    [downViewLeft autoSetDimension:ALDimensionWidth toSize:[self width]*0.18];
    [downViewLeft autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:41+11];
    [downViewLeft autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:20+11];
    [downViewLeft autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20+11];
    
    [downViewRight autoSetDimension:ALDimensionWidth toSize:[self width]*0.18];
    [downViewRight autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:41+11];
    [downViewRight autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:20+11];
    [downViewRight autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20+11];

    [self randomiseText];
}

- (void)makeSwipeText{
    [swipeHolderView setBackgroundColor:color(1, 1, 1, 0.48)];

    [swipeMovingView makeTextLabel:swipeMovingView.frame];
    
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Swipe_text_bacground"]];
    [self addSubview:tempImageView];

    [tempImageView autoSetDimension:ALDimensionWidth toSize:[self width]*0.18];//115
    [tempImageView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:20+11];
    [tempImageView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20+11];
    [tempImageView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:self.frame.size.width/2 - (([self width]*0.18)/2)];
    //[tempImageView autoAlignAxisToSuperviewAxis:ALAxisVertical];

    /*if(DEVICE_BIGER_SIDE < 568){
        UIImageView *tempImageView = [[UIImageView alloc]initWithFrame:swipeMovingView.frame];
        tempImageView.image = [UIImage imageNamed:@"Swipe_text_midle_bacground"];
        [swipeHolderView addSubview:tempImageView];
        [tempImageView setX: -30];
    }else{
        swipeHolderView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Swipe_text_midle_bacground"]];
    }*/
    //[swipeMovingView setText:@"A Black Dress"];
    [swipeMovingView setText:[swipeMovingView.movingViewData objectForKey:@"textAnswer"]];
    
    [swipeDestinationViewRight makeImage:swipeDestinationViewRight.frame];
    [swipeDestinationViewLeft makeImage:swipeDestinationViewRight.frame];
    
    [self insertSubview:tempImageView  belowSubview:swipeMovingView];//new
    [self insertSubview:swipeDestinationViewRight belowSubview:swipeMovingView];//new
    [self insertSubview:swipeDestinationViewLeft belowSubview:swipeMovingView];//new
    
    [self randomiseImage];
    
}
-(void)randomiseImage{
    NSArray *arrayAnswers = [_vehicleData objectForKey:@"answers"];
    int randomNum = arc4random_uniform(2);
    NSDictionary *rightDictionary = arrayAnswers[randomNum];
    NSDictionary *leftDictionary = arrayAnswers[1-randomNum];
    
    [swipeDestinationViewRight setImage:[rightDictionary objectForKey:@"image"]];
    [swipeDestinationViewLeft  setImage:[leftDictionary objectForKey:@"image"]];
    
    swipeDestinationViewRight.tag = [[rightDictionary objectForKey:@"isCorrect"]boolValue];
    swipeDestinationViewLeft.tag = [[leftDictionary objectForKey:@"isCorrect"]boolValue];
}

- (void)activateVehicle{
    [super activateVehicle];
    panGesture.enabled = YES;
    handView.alpha = 1;
    if(self.swipeImageVehicle){
        [self.vehicleDelegate setQuestionBarText:@"Swipe the image to the correct phrase."];
    }else{
        [self.vehicleDelegate setQuestionBarText:@"Swipe the phrase to the correct image."];
        [swipeMovingView setText:[swipeMovingView.movingViewData objectForKey:@"textAnswer"]];
    }
    if(self.beforeAudioString){
        self.vehicleState = KAPlayBeforeAudioState;
        [AUDIO_MANAGER playAudio:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:self.beforeAudioString ofType:@""]]];
        [AUDIO_MANAGER setDelegate:self];
        self.beforeAudioString = nil;
    }
    if ([_vehicleData objectForKey:@"questionTextVocabs"]) {
        
        if ([[_vehicleData objectForKey:@"questionTextVocabs"] boolValue]) {
            [self.vehicleDelegate setQuestionBarTextWithVocab:[_vehicleData objectForKey:@"questionText"]];
            
        }
        else {
            [self.vehicleDelegate setQuestionBarText:[_vehicleData objectForKey:@"questionText"]];
            
        }
    }
    else {
        [self.vehicleDelegate setQuestionBarText:[_vehicleData objectForKey:@"questionText"]];
    }
}
- (void)deactivateVehicle{
    [super deactivateVehicle];
    if(self.swipeImageVehicle){
        [self randomiseText];
    }else{
        [self randomiseImage];
    }
    [self setStyle];
    [self returnToStartLocationAnimated:YES];
    [AUDIO_MANAGER setDelegate:nil];
    [AUDIO_MANAGER stopPlaying];
}

#pragma mark - SwipeMovingView Delegate implementation

- (void)leftSwipeDetected:(id)sender {
    //return;
    NSLog(@"Left swipe was detected");
    
    if (self.isCompleted) {
        return;
    }
    
    [UIView animateWithDuration:1.0 animations:^{
        
        swipeMovingView.center = swipeDestinationViewLeft.center;
        
    } completion:^(BOOL finished) {
        
        self.isCompleted = YES;
        [self.vehicleDelegate vehicleWasFinished:self];
        
    }];
    
}

- (void)rightSwipeDetected:(id)sender {
    //return;
    NSLog(@"Right swipe was detected");
    
    if (self.isCompleted) {
        return;
    }
    
    [UIView animateWithDuration:1.0 animations:^{
        
        swipeMovingView.center = swipeDestinationViewRight.center;
        
    } completion:^(BOOL finished) {
        
        self.isCompleted = YES;
        [self.vehicleDelegate vehicleWasFinished:self];
        
    }];
}

#pragma mark - Actions


- (void)move {
    if (panGesture.state == UIGestureRecognizerStateChanged) {
        [self moveObject];
    } else if (panGesture.state == UIGestureRecognizerStateEnded) {
        if (self.isGoalReached) {
            NSLog(@"isGoalReached");
            //[self finishVehicle];
            if([self isRightGoal]){
                [self showCorrectFeedback];
            }else{
                [self showIncorrectFeedback];
            }
        }
        else {
            NSLog(@"returnToStartLocationAnimated");
            [self returnToStartLocationAnimated:YES];
            //[self showIncorrectFeedback];
        }
    }
}

- (void)showCorrectFeedback{
    panGesture.enabled = NO;
    [super showCorrectFeedback];
    
    float distanceFromLeft =  swipeMovingView.center.x - swipeDestinationViewLeft.center.x;
    self.dragViewX.constant = (distanceFromLeft < corectDistance) ? [swipeDestinationViewLeft x] : [swipeDestinationViewRight x];
    self.dragViewY.constant = 0.0;
    (distanceFromLeft < corectDistance) ? [self showFeedbackWithStatus:YES forView:swipeDestinationViewLeft] : [self showFeedbackWithStatus:YES forView:swipeDestinationViewRight];
    if(self.swipeImageVehicle){
       (distanceFromLeft < corectDistance) ? [self showFeedbackWithStatus:YES forView:swipeDestinationViewLeft] : [self showFeedbackWithStatus:YES forView:swipeDestinationViewRight];
    }else{
      //self.dragViewX.constant = (distanceFromLeft < corectDistance) ? [swipeDestinationViewLeft x]+2 : [swipeDestinationViewRight x]+2;
      //[swipeMovingView autoSetDimension:ALDimensionWidth toSize:103.0];
      //(distanceFromLeft < corectDistance) ? [self showFeedbackWithStatus:YES forView:swipeMovingView forBorderView:swipeDestinationViewLeft ] : [self showFeedbackWithStatus:YES forView:swipeMovingView forBorderView:swipeDestinationViewRight];
         [self showFeedbackWithStatus:YES forView:swipeMovingView];
    }
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [self layoutIfNeeded];
    } completion:nil];
}
- (void)showIncorrectFeedback{
    float distanceFromLeft =  swipeMovingView.center.x - swipeDestinationViewLeft.center.x;
    self.dragViewX.constant = (distanceFromLeft < corectDistance) ? [swipeDestinationViewLeft x] : [swipeDestinationViewRight x];
    self.dragViewY.constant = 0.0;
    if(!self.swipeImageVehicle){
        [self showFeedbackWithStatus:NO forView:swipeMovingView];
        //self.dragViewX.constant = (distanceFromLeft < corectDistance) ? [swipeDestinationViewLeft x]+2 : [swipeDestinationViewRight x]+2;
        //[swipeMovingView autoSetDimension:ALDimensionWidth toSize:103.0];
        //(distanceFromLeft < corectDistance) ? [self showFeedbackWithStatus:NO forView:swipeMovingView forBorderView:swipeDestinationViewLeft ] : [self showFeedbackWithStatus:NO forView:swipeMovingView forBorderView:swipeDestinationViewRight];
    }else{
        (distanceFromLeft < corectDistance) ? [self showFeedbackWithStatus:NO forView:swipeDestinationViewLeft] : [self showFeedbackWithStatus:NO forView:swipeDestinationViewRight];
    }
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [self layoutIfNeeded];
    } completion:nil];
    
    panGesture.enabled = NO;
    //[super showIncorrectFeedback];
    
    //new
    [AUDIO_MANAGER playWrongSound];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(showCorectAnswer) userInfo:nil repeats:NO];
    
}
- (void)showCorectAnswer{
    float distanceFromLeft =  swipeMovingView.center.x - swipeDestinationViewLeft.center.x;
    self.dragViewX.constant = (distanceFromLeft < corectDistance) ? [swipeDestinationViewRight x] : [swipeDestinationViewLeft x];
    self.dragViewY.constant = 0.0;
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        (distanceFromLeft < corectDistance) ? [self showFeedbackWithStatus:YES forView:swipeDestinationViewRight]:[self showFeedbackWithStatus:YES forView:swipeDestinationViewLeft];
        if(self.swipeImageVehicle){
            (distanceFromLeft < corectDistance) ? [self showFeedbackWithStatus:YES forView:swipeDestinationViewRight]:[self showFeedbackWithStatus:YES forView:swipeDestinationViewLeft] ;
        }else{
            [self showFeedbackWithStatus:YES forView:swipeMovingView];
        }

        [super showCorrectFeedbackWithOutPoins];
    }];
    
    
}
#pragma mark BaseViewVehicle

- (void)finishVehicle{
    
    [super finishVehicle];
    if(self.afterAudioString){
        self.vehicleState = KAPlayAfterAudioState;
        [AUDIO_MANAGER playAudio:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:self.afterAudioString ofType:@""]]];
        [AUDIO_MANAGER setDelegate:self];
        //self.afterAudioString = nil;
    }else{
        self.isCompleted = YES;
        [self.vehicleDelegate vehicleWasFinished:self];
        //[self.vehicleDelegate setQuestionBarText:@""];
        [AUDIO_MANAGER setDelegate:nil];
    }
    
}

#pragma mark - UI updates

- (void)moveObject {
    CGFloat minX = DRAG_AREA_PADDING;
    CGFloat maxX = self.bounds.size.width - swipeMovingView.bounds.size.width - DRAG_AREA_PADDING;
    
    CGFloat minY = DRAG_AREA_PADDING;
    CGFloat maxY = self.bounds.size.height - swipeMovingView.bounds.size.height - DRAG_AREA_PADDING;
    
    CGPoint translation = [panGesture translationInView:self];
    
    CGFloat dragViewX = self.dragViewX.constant + translation.x;
    CGFloat dragViewY = self.dragViewY.constant + translation.y;
    
    if (dragViewX < minX) {
        dragViewX = minX;
        translation.x += self.dragViewX.constant - minX;
    }
    else if (dragViewX > maxX) {
        dragViewX = maxX;
        translation.x += self.dragViewX.constant - maxX;
    }
    else {
        translation.x = 0;
    }
    
    if (dragViewY < minY) {
        //dragViewY = minY;
        translation.y += self.dragViewY.constant - minY;
    }
    else if (dragViewY > maxY) {
        //dragViewY = maxY;
        translation.y += self.dragViewY.constant - maxY;
    }
    else {
        translation.y = 0;
    }
    //NSLog(@"dragViewX %f dragViewY %f",dragViewX,dragViewY);
    self.dragViewX.constant = dragViewX;
    //self.dragViewY.constant = dragViewY;
    
    [panGesture setTranslation:translation inView:self];
    
    [UIView animateWithDuration:0.5 animations:^{
        if(handView.alpha){
            handView.alpha = 0;
        }
    }];
    [UIView animateWithDuration:0.05 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [self layoutIfNeeded];
    } completion:nil];
    
   // [self updateGoalView];
}

- (void)updateGoalView {
    //UIColor *goalColor = self.isGoalReached ? [UIColor whiteColor] : [UIColor colorWithRed:174/255.0 green:0 blue:0 alpha:1];
    
    swipeDestinationViewLeft.layer.borderColor = [UIColor whiteColor].CGColor;//goalColor.CGColor;
    
    /*self.dragHereLabel.textColor = goalColor;
    self.dragHereLabel.text = self.isGoalReached ? @"Drop!" : @"Drag here!";*/
}

- (void)returnToStartLocationAnimated:(BOOL)animated {
    self.dragViewX.constant = self.frame.size.width/2 -(([self width]*0.18)/2);
    self.dragViewY.constant = 0.0;
    [UIView animateWithDuration:0.2 animations:^{
        if(!handView.alpha){
            handView.alpha = 1;
        }
    }];
    if (animated) {
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
            [self layoutIfNeeded];
        } completion:nil];
    }
}

#pragma mark - Getters

- (BOOL)isGoalReached {
    //return NO;
    float distanceFromLeft =  swipeMovingView.center.x - swipeDestinationViewLeft.center.x;
    float distanceFromRight =  swipeDestinationViewRight.center.x - swipeMovingView.center.x;
    NSLog(@"distanceFromLeft %f",distanceFromLeft);
    NSLog(@"distanceFromRight %f",distanceFromRight);
    if(distanceFromLeft < corectDistance){
        return  YES ;
    }else  if(distanceFromRight < corectDistance){
        return  YES ;
    }
    return  NO;;
}
- (BOOL)isRightGoal {
    //return NO;
    float distanceFromLeft =  swipeMovingView.center.x - swipeDestinationViewLeft.center.x;
    float distanceFromRight =  swipeDestinationViewRight.center.x - swipeMovingView.center.x;
    if(distanceFromLeft < corectDistance){
        return swipeDestinationViewLeft.tag ? YES : NO;
    }else  if(distanceFromRight < corectDistance){
        return swipeDestinationViewRight.tag ? YES : NO;
    }
    return  NO;;
}
#pragma mark - AVWrapperDelegate

- (void)recordingComplete {
}

- (void)playbackItemComplete {
    
}

- (void)playbackComplete {
    
    if(self.vehicleState == KAPlayBeforeAudioState){
        
    }else if(self.vehicleState == KAPlayAfterAudioState){
        self.vehicleState = KADefaultState;
        self.isCompleted = YES;
        [self.vehicleDelegate vehicleWasFinished:self];
        //[self.vehicleDelegate setQuestionBarText:@""];
        [AUDIO_MANAGER setDelegate:nil];
    }
}

- (void)playbackPaused {
    // do nothing
}
@end
