//
//  MissingWordsTextAnswer.m
//  Hollywood
//
//  Created by Kiril Kiroski on 5/13/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "MissingWordsTextAnswer.h"

@implementation MissingWordsTextAnswer{
    UIView *viewContainerLabelWord;
    UILabel *labelWord;
    UIImageView *imageViewFeedback;
    CGPoint lastLocation;
    CGPoint startLocation;
    BOOL buttonTouched;
    NSString *initialString;
    
}
//dolu tie so se mrdaat
- (instancetype)initWithFrame:(CGRect)frame data:(id)data type:(BOOL)type {
    
    if (self = [super initWithFrame:frame]) {
        
        viewContainerLabelWord = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        /*[viewContainerLabelWord setBackgroundColor:[UIColor blackColor]];
        viewContainerLabelWord.layer.cornerRadius = 4.0;
        viewContainerLabelWord.layer.borderColor = [UIColor whiteColor].CGColor;
        viewContainerLabelWord.layer.borderWidth = 1.0;
        viewContainerLabelWord.layer.masksToBounds = YES;*/
        [self addSubview:viewContainerLabelWord];
        
        labelWord = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [labelWord setFont:[UIFont systemFontOfSize:26.0]];
        [labelWord setTextColor:[UIColor whiteColor]];
         [labelWord setBackgroundColor:[UIColor clearColor]];
        labelWord.numberOfLines = 0;
        labelWord.text = [[data objectForKey:@"text"] capitalizedString];
        initialString = [data objectForKey:@"text"];
        [labelWord sizeToFit];
        
        [self addSubview:labelWord];
        
        labelWord.frame = CGRectMake((frame.size.width-labelWord.width)/2, (frame.size.height-labelWord.height)/2, labelWord.width, labelWord.height);
        self.backgroundColor = [UIColor redColor];
        startLocation = frame.origin;
        /*UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(detectPan:)];
        self.gestureRecognizers = @[panRecognizer];*/
    }
    
    return self;
}

//gore tie so se
- (instancetype)initWithFrame:(CGRect)frame withText:(NSString*)infoText andReplaceString:(NSString*)newReplaceString{
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = NO;
        viewContainerLabelWord = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [viewContainerLabelWord setBackgroundColor:[UIColor clearColor]];
        [self addSubview:viewContainerLabelWord];
        
        labelWord = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [labelWord setFont:[UIFont systemFontOfSize:26.0]];
        [labelWord setTextColor:[UIColor whiteColor]];
        [labelWord setBackgroundColor:[UIColor clearColor]];
        labelWord.numberOfLines = 0;
        labelWord.text = infoText;
        
        initialString = infoText;
        if([LMHelper isWordMatchingPattern:infoText pattern:kWordPattern]) {
            labelWord.text = newReplaceString;//kUnderlineWord;
        }
        
        [labelWord sizeToFit];
        
        viewContainerLabelWord.frame = labelWord.frame;
        [self addSubview:labelWord];
        [self setHeight: labelWord.height];
        [self setWidth:labelWord.width];
        
        startLocation = CGPointMake(frame.origin.x-(frame.size.width-labelWord.width)/2,frame.origin.y-(frame.size.height-labelWord.height)/2);;
    }
    
    return self;
}

- (void)setInitialString{
    
    labelWord.text = initialString ;
}
- (void)setCorrectAnswer{
    labelWord.alpha = 0.5;
}
- (void)resetLabel{
    labelWord.alpha = 1;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSLog(@"touchesBegan");
    // test wether the button is touched
    UITouch *touch = [touches anyObject];
    CGPoint touchBegan = [touch locationInView:self.superview.superview];
     NSLog(@"touchBegan %.2f %.2f",touchBegan.x,touchBegan.y);
        buttonTouched = YES;
    lastLocation = self.center;
    
}

- (CGPoint)takeFinalLocation{
    return startLocation;
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    //NSLog(@"touchesMoved");
    if(buttonTouched) {
        // do it here
        UITouch *touch = [touches anyObject];
        CGPoint touchMoved = [touch locationInView:self.superview];
        // NSLog(@"touchMoved %.2f %.2f",touchMoved.x,touchMoved.y);
        CGRect newFrame = CGRectMake(touchMoved.x-([self width]/2),
                                     touchMoved.y-([self height]/2),
                                     [self width],
                                     [self height]);
        self.frame = newFrame;
     
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    NSLog(@"touchesEnded");
    buttonTouched = NO;
    UITouch *touch = [touches anyObject];
    CGPoint touchMoved = [touch locationInView:self.superview];
    CGPoint nextPoint = [self.vehicleDelegate chekAnswer:labelWord.text andPoint:touchMoved];
    if (CGPointEqualToPoint(nextPoint, CGPointZero)){
        nextPoint = startLocation;
        [_vehicleDelegate answerIsCorect:NO];
    }else{
        nextPoint = CGPointMake(nextPoint.x,nextPoint.y);
        [_vehicleDelegate answerIsCorect:YES];
    }
    [UIView animateWithDuration:0.2 animations:^{
        [self setOrigin:nextPoint];
    } completion:^(BOOL finished) {
        [self.vehicleDelegate setCorectAnswer:labelWord.text andPoint:touchMoved];
    }];
    
}

- (NSString*)wordString{
    return [initialString  capitalizedString];
}

-(NSString*)getLabelWordText{
    return labelWord.text;
}

- (void)reset{
    
    [self setOrigin:startLocation];
}
@end
