//
//  MissingWordsTextAnswer.h
//  Hollywood
//
//  Created by Kiril Kiroski on 5/13/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MissingWordsTextAnswerDelegate <NSObject>

- (CGPoint) chekAnswer:(NSString*)curentText andPoint:(CGPoint)loc;
- (void) setCorectAnswer:(NSString*)curentText andPoint:(CGPoint)loc;

- (void) answerIsCorect:(BOOL)answerFlag;

@end

@interface MissingWordsTextAnswer : UIView<UIGestureRecognizerDelegate>

@property (nonatomic, strong) id<MissingWordsTextAnswerDelegate>vehicleDelegate;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data type:(BOOL)type;
- (instancetype)initWithFrame:(CGRect)frame withText:(NSString*)infoText andReplaceString:(NSString*)newReplaceString;
- (NSString*)wordString;
- (void)setInitialString;
- (void)setCorrectAnswer;
- (void)resetLabel;
- (CGPoint)takeFinalLocation;
- (NSString*)getLabelWordText;
- (void)reset;

@end
