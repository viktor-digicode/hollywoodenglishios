//
//  FillMissingWordsVehicle.m
//  Hollywood
//
//  Created by Kiril Kiroski on 5/12/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "FillMissingWordsVehicle.h"

@implementation FillMissingWordsVehicle{
    id vehicleData;
    NSArray *words;
    NSString *question;
    NSString *answer;
    UIView *viewAnswerContent;
    NSMutableArray *questionWordView;
    NSMutableArray *dragWordViewArray;
    NSString *replaceString;
    int hit;
    int wordsForFill;
    UIView *viewQuestionContent;
    UIImageView *imageViewQuestion;
}

- (void)activateVehicle{
    [super activateVehicle];
    hit = 0;
    self.nAttemptsLeft = 2;
}

- (void)deactivateVehicle{
    [super deactivateVehicle];
    for (int i= 0; i < [dragWordViewArray count]; i++) {
        MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[dragWordViewArray objectAtIndex:i];
        [tempClickTextAnswer reset];
    }
    for (int i= 0; i < [questionWordView count]; i++) {
        MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[questionWordView objectAtIndex:i];
        [tempClickTextAnswer resetLabel];
    }
}
- (instancetype)initWithFrame:(CGRect)frame data:(id)data {
    
    if (self = [super initWithFrame:frame]) {
        vehicleData = data;
        self.backgroundColor = [UIColor clearColor];
        answer = [[vehicleData objectForKey:@"answer"] stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        question = [[vehicleData objectForKey:@"question"] stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        words = [question componentsSeparatedByString:@" "];
        
        [self createQuestionImage];
        [self createLayout:[LMHelper shuffleArray:[vehicleData objectForKey:@"answers"]]];

    }
    
    return self;
}


- (void)createLayout:(NSArray *)arrayAnswers {
    
    //Create answer content view
    
    float answerContentWidth = (DEVICE_BIGER_SIDE-120-20)/2;
    float answerContentHeight = DEVICE_SMALL_SIDE-64-90;
    float startYCordinate = DEVICE_SMALL_SIDE/2 - (answerContentHeight/3)-20;
    if (viewAnswerContent == nil) {
        
        viewAnswerContent = [[UIView alloc] initWithFrame:CGRectMake(60+answerContentWidth+20, 30, answerContentWidth, answerContentHeight)];
        [viewAnswerContent setBackgroundColor:[UIColor clearColor]];
        [self addSubview:viewAnswerContent];
    }
    
    
    
    
    //Create answers
    MissingWordsTextAnswer *clickTextAnswer;
    CGFloat answerWidth = answerContentWidth/2;
    float answerHeight = (answerContentHeight-3*5)/4;
    
    questionWordView = [[NSMutableArray alloc]init];
    NSInteger startX = [viewAnswerContent x];
    NSInteger startY = 30;
    
    NSString *tempReplaceString;
    replaceString = @"_";
    
    for (id tempString in words) {
        if([LMHelper isWordMatchingPattern:tempString pattern:kWordPattern]) {
            tempReplaceString = [[tempString componentsSeparatedByCharactersInSet:[[NSCharacterSet letterCharacterSet] invertedSet]] componentsJoinedByString:@"_"];
            tempReplaceString = [[tempReplaceString componentsSeparatedByCharactersInSet:[NSCharacterSet letterCharacterSet]] componentsJoinedByString:@"_"];
        }
        if([replaceString length]<[tempReplaceString length]){
            replaceString = tempReplaceString;
        }
    }
    UILabel *testLabel = [[UILabel alloc] init];
    [testLabel setFont:[UIFont systemFontOfSize:26.0]];
    testLabel.text = replaceString;
    [testLabel sizeToFit];
    
    if(answerWidth < [testLabel width]){
        answerWidth = [testLabel width];
    }
    
    wordsForFill = 0;
    for (id dataClickImage in words) {
        
        clickTextAnswer = [[MissingWordsTextAnswer alloc] initWithFrame:CGRectMake(startX,startY, answerWidth, answerHeight) withText:dataClickImage andReplaceString:replaceString];
        [clickTextAnswer setBackgroundColor:[UIColor clearColor]];
        [self addSubview:clickTextAnswer];
        [questionWordView addObject:clickTextAnswer];
        
        if([[clickTextAnswer getLabelWordText] isEqualToString:replaceString]){
            wordsForFill ++;
        }
        
        startX += [clickTextAnswer width]+10;
        if(startX >= (DEVICE_BIGER_SIDE-120)){
            startX = [viewAnswerContent x];;
            startY = startY+answerHeight;
        }
    }
    
    NSInteger order = 0;
    NSInteger row = 0;
    NSInteger column = 0;
    answerWidth = answerContentWidth/2;
    
    dragWordViewArray = [[NSMutableArray alloc]init];
    
    for (id dataClickImage in arrayAnswers) {
        
        UIView *clickTextBox = [[UIView alloc] initWithFrame:CGRectMake([viewAnswerContent x] + column*(answerWidth+5),[viewAnswerContent y] + startYCordinate + row*(answerHeight+5), answerWidth, answerHeight)];
        [clickTextBox setBackgroundColor:[UIColor blackColor]];
        clickTextBox.layer.cornerRadius = 4.0;
        clickTextBox.layer.borderColor = [UIColor whiteColor].CGColor;
        clickTextBox.layer.borderWidth = 1.0;
        clickTextBox.layer.masksToBounds = YES;
        clickTextBox.userInteractionEnabled = NO;
        [self addSubview:clickTextBox];
        
        clickTextAnswer = [[MissingWordsTextAnswer alloc] initWithFrame:CGRectMake([viewAnswerContent x] + column*(answerWidth+5),[viewAnswerContent y] + startYCordinate + row*(answerHeight+5), answerWidth, answerHeight) data:dataClickImage type:YES];
        //clickTextAnswer.delegateClickText = self;
        [clickTextAnswer setBackgroundColor:[UIColor clearColor]];
        clickTextAnswer.vehicleDelegate = self;
        [self addSubview:clickTextAnswer];
        [dragWordViewArray addObject:clickTextAnswer];
        
        order++;
        row = order/2;
        column = order%2;
    }
    
}

- (void)createQuestionImage {
    
    UIImage *originalImage = [UIImage imageNamed:[vehicleData objectForKey:@"bigImage"]];
    UIImage *newImage = [UIImage imageWithImage:originalImage scaledToHeight:DEVICE_SMALL_SIDE-64-90];
    
    CGFloat contentQuestionWidth = newImage.size.width;
    CGFloat contentQuestionHeight = newImage.size.height;
    
    NSLog(@"New width - %f, new height - %f", newImage.size.width, newImage.size.height);
    
    float answerContentWidth = (DEVICE_BIGER_SIDE-100-20)/4;
    if (newImage.size.width > 3*answerContentWidth) {
        
        contentQuestionWidth = 3*answerContentWidth;
    }
    
    if (viewQuestionContent == nil) {
        
        viewQuestionContent = [[UIView alloc] initWithFrame:CGRectMake(50, 20, contentQuestionWidth, contentQuestionHeight)];
        [viewQuestionContent setBackgroundColor:[UIColor blackColor]];
        [self addSubview:viewQuestionContent];
        
        viewQuestionContent.layer.cornerRadius = 4.0;
        viewQuestionContent.layer.borderWidth = 1.0;
        viewQuestionContent.layer.borderColor = [UIColor whiteColor].CGColor;
        viewQuestionContent.layer.masksToBounds = YES;
    }
    
    if (imageViewQuestion == nil) {
        
        imageViewQuestion = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, contentQuestionWidth-6, contentQuestionHeight-6)];
        [imageViewQuestion setImage:newImage];
        [imageViewQuestion setContentMode:UIViewContentModeScaleAspectFit];
        [viewQuestionContent addSubview:imageViewQuestion];
        
    }
}

#pragma mark MissingWordsTextAnswerDelegate

- (CGPoint) chekAnswer:(NSString*)curentText andPoint:(CGPoint)loc{
    
    //CGPoint loc = [tapGesture locationInView:carouselPickerView];
    for (int i= 0; i < [questionWordView count]; i++) {
        MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[questionWordView objectAtIndex:i];
        if (CGRectContainsPoint(tempClickTextAnswer.frame, loc)){
            //NSLog(@"###%@ == %@###",[NSString stringWithFormat:@"<%@>",curentText],[tempClickTextAnswer wordString]);
            if([[NSString stringWithFormat:@"<%@>",curentText] isEqualToString:[tempClickTextAnswer wordString]]) {
                NSLog(@"# yess");
                //[tempClickTextAnswer setCorrectAnswer];
                return [tempClickTextAnswer takeFinalLocation];;
            }
        }else{
            NSLog(@"# no");
            //return NO;
        }
    }
    return CGPointZero;
}

- (void) setCorectAnswer:(NSString*)curentText andPoint:(CGPoint)loc{
    for (int i= 0; i < [questionWordView count]; i++) {
        MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[questionWordView objectAtIndex:i];
        if (CGRectContainsPoint(tempClickTextAnswer.frame, loc)){
            //NSLog(@"###%@ == %@###",[NSString stringWithFormat:@"<%@>",curentText],[tempClickTextAnswer wordString]);
            if([[NSString stringWithFormat:@"<%@>",curentText] isEqualToString:[tempClickTextAnswer wordString]]) {
                NSLog(@"# yess");
                [tempClickTextAnswer setCorrectAnswer];
                return ;
            }
        }
    }
    return;
}

- (void) answerIsCorect:(BOOL)answerFlag{
    if(answerFlag){
        [self showCorrectFeedback];
        hit ++;
    }else{
        [self showIncorrectFeedback];
        self.nAttemptsLeft--;
    }
}

#pragma mark BaseViewVehicle

- (void)finishVehicle{
    if((hit < wordsForFill) && (self.nAttemptsLeft > 0) ) {
        return;
    }
   

    [super finishVehicle];
    if(self.afterAudioString){
        self.vehicleState = KAPlayAfterAudioState;
        [AUDIO_MANAGER playAudio:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:self.afterAudioString ofType:@""]]];
        [AUDIO_MANAGER setDelegate:self];
        self.afterAudioString = nil;
    }else{
        self.isCompleted = YES;
        [self.vehicleDelegate vehicleWasFinished:self];
        //[self.vehicleDelegate setQuestionBarText:@""];
        [AUDIO_MANAGER setDelegate:nil];
    }
    
}
@end
