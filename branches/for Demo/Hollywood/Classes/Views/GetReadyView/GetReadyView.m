//
//  GetReadyView.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 10/21/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "GetReadyView.h"

@implementation GetReadyView


- (void)awakeFromNib {
    
    [super awakeFromNib];
    currentNumber = 3;
    [self countSeconds:0];
    if (!counterTimer) {
        
        counterTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countSeconds:) userInfo:nil repeats:YES];
    }
}

#pragma mark - Timer action

- (void)countSeconds:(id)sender {
    
    
    if (currentNumber == 0) {
        
        [self skipThisScreen:nil];
    }
    else {
        _numberCounterLabel.alpha = 1;
        _numberCounterLabel.text = [NSString stringWithFormat:@"%ld", currentNumber];
        
        [UIView animateWithDuration:1.0 animations:^{
            _numberCounterLabel.alpha = 0;
        }];
         
       /* [UIView animateWithDuration:0.8 delay:0.2 usingSpringWithDamping:1 initialSpringVelocity:1 options:nil animations:^{
            _numberCounterLabel.alpha = 0;
        } completion:^(BOOL finished) {
          //
        }];*/
    }
    currentNumber--;
    
}

- (IBAction)skipThisScreen:(id)sender {
    
    NSLog(@"Go to next screen");

    if (counterTimer) {
        [counterTimer invalidate];

        if ([self.delegate respondsToSelector:@selector(gotoNewScreen:)]) {
            

            [self.delegate gotoNewScreen:self];
        }
    }
    
}

@end
