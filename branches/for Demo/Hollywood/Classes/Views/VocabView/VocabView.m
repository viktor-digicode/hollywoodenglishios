//
//  VocabView.m
//  VocabTest
//
//  Created by Dimitar Shopovski on 7/19/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import "VocabView.h"
#import "CustomDataLabel.h"

@implementation VocabView
@synthesize imgBallon;

- (instancetype)initWithFrame:(CGRect)frame withData:(id)data {
    
    if (self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[UIColor clearColor]];
        NSString *textString = [data objectForKey:@"title"];
        [self buildTextViewFromString:textString];
        
        [self createUX];
    }
    
    return self;
}

- (void)buildTextViewFromString:(NSString *)localizedString withFont:(NSNumber *)fontSize {
    
    self.fontSize = fontSize;
    [self buildTextViewFromString:localizedString];
    
}

- (void)buildTextViewFromString:(NSString *)localizedString
{
    
    CGFloat fontSizeValue = [self.fontSize floatValue];
    
    if (!fontSizeValue) {
        fontSizeValue = 20.0;
    }
    
    isFirstRow = YES;

    // 1. Split the localized string on the # sign:
    NSArray *localizedStringPieces = [localizedString componentsSeparatedByString:@" "];
    
    // 2. Loop through all the pieces:
    NSUInteger msgChunkCount = localizedStringPieces ? localizedStringPieces.count : 0;
    CGPoint wordLocation = CGPointMake(0.0, 0.0);
    for (NSUInteger i = 0; i < msgChunkCount; i++)
    {
        NSString *chunk = [localizedStringPieces objectAtIndex:i];
        if ([chunk isEqualToString:@""])
        {
            continue;     // skip this loop if the chunk is empty
        }
        
        // 3. Determine what type of word this is:
        BOOL isTranslationText = [chunk hasPrefix:@"#"];
//        BOOL isPrivacyPolicyLink  = [chunk hasPrefix:@"<pp>"];
//        BOOL isLink = (BOOL)(isTranslationText || isPrivacyPolicyLink);
        
        BOOL isLink = isTranslationText;
        
        // 4. Create label, styling dependent on whether it's a link:
        CustomDataLabel *label = [[CustomDataLabel alloc] init];
        label.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSizeValue];
        label.text = chunk;
        label.userInteractionEnabled = isLink;
        
        if (isLink)
        {
            label.textColor = [UIColor colorWithRed:110/255.0f green:181/255.0f blue:229/255.0f alpha:1.0];
            label.highlightedTextColor = [UIColor yellowColor];
            
            // 5. Set tap gesture for this clickable text:
            SEL selectorAction = @selector(tapOnVocabWord:);
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                         action:selectorAction];
            [label addGestureRecognizer:tapGesture];
            
            chunk = [chunk stringByReplacingOccurrencesOfString:@"#" withString:@""];
            NSString *link = [self findTranslateWord:chunk betweenTag:@"link"];
            NSString *translation = [self findTranslateWord:chunk betweenTag:@"trans"];
            
            
            label.text = link;
            [label setData:@{@"translation": translation}];
            
            
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] init];
            
            [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:label.text
                                                                                     attributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                                                                                  NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                                                  NSFontAttributeName: [UIFont italicSystemFontOfSize:fontSizeValue]
                                                                                                  }]];
            
            [label setAttributedText:attributedString];
        }
        else
        {
            label.textColor = [UIColor whiteColor];
        }
        
        // 6. Lay out the labels so it forms a complete sentence again:
        
        // If this word doesn't fit at end of this line, then move it to the next
        // line and make sure any leading spaces are stripped off so it aligns nicely:
        
        [label sizeToFit];
        
        if (self.frame.size.width < wordLocation.x + label.bounds.size.width + 4.0)
        {
            wordLocation.x = 0.0;                       // move this word all the way to the left...
            wordLocation.y += label.frame.size.height;  // ...on the next line
            
            // And trim of any leading white space:
            NSRange startingWhiteSpaceRange = [label.text rangeOfString:@"^\\s*"
                                                                options:NSRegularExpressionSearch];
            if (startingWhiteSpaceRange.location == 0)
            {
                label.text = [label.text stringByReplacingCharactersInRange:startingWhiteSpaceRange
                                                                 withString:@""];
                [label sizeToFit];
            }
        }
        else {
            
            if (!isFirstRow) {
                wordLocation.x = wordLocation.x + 4.0;
            }
            else {
                isFirstRow = NO;
            }
        }
        
        // Set the location for this label:
        label.frame = CGRectMake(wordLocation.x,
                                 wordLocation.y,
                                 label.frame.size.width,
                                 label.frame.size.height);
        // Show this label:
        [self addSubview:label];
        
        // Update the horizontal position for the next word:
        wordLocation.x += label.frame.size.width;
    }
}

- (void)tapOnVocabWord:(UITapGestureRecognizer *)tapGesture
{
    CustomDataLabel *selectedLabel = (CustomDataLabel *)tapGesture.view;
    
    if (tapGesture.state == UIGestureRecognizerStateEnded)
    {
        
        
        if (selectedLabel == currentSelectedVocabLabel && isTranslationShown) {
            
            if (_vocabTimer != nil) {
                
                [_vocabTimer invalidate];
                _vocabTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(stopTheTimer:) userInfo:nil repeats:NO];

            }
        }
        else {
            
            
            CGFloat xPos = tapGesture.view.center.x - self.translationView.frame.size.width/2;
            CGFloat yPos = tapGesture.view.frame.origin.y + tapGesture.view.frame.size.height;
            
            
            self.translationView.hidden = NO;
            
            _translationWordLabel.frame = CGRectMake(_translationWordLabel.x, _translationWordLabel.y, 200, _translationWordLabel.height);
            _translationWordLabel.text = [selectedLabel.data objectForKey:@"translation"];
            [_translationWordLabel sizeToFit];
            
            imgBallon.frame = CGRectMake(0, 0, _translationWordLabel.width + 28, 36);
            
            if (xPos-30 < 0) {
                
                self.translationView.frame = CGRectMake(-30, yPos, _translationWordLabel.width + 29 + 4 + 10, 36);

            }
            else {
                
                self.translationView.frame = CGRectMake(xPos-30, yPos, _translationWordLabel.width + 29 + 4 + 10, 36);

            }
//            _buttonFavouritres.frame = CGRectMake(_translationView.width-37, 10, 20, 20);

            //new code label alignment
            _translationWordLabel.center = CGPointMake(imgBallon.center.x, _translationWordLabel.center.y);
            
            isTranslationShown = YES;
            
            if (selectedLabel != currentSelectedVocabLabel && _vocabTimer != nil) {
                [_vocabTimer invalidate];
            }
            currentSelectedVocabLabel = selectedLabel;

            _vocabTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(stopTheTimer:) userInfo:nil repeats:NO];

        }
    }
}

- (void)stopTheTimer:(id)sender {
    
    self.translationView.hidden = YES;
    isTranslationShown = NO;

}

#pragma mark - createUX

- (void)createUX {
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.translationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 110, 36)];
    self.translationView.hidden = YES;
    
    //image
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(6, 6, 6, 12);
    UIImage *backgroundButtonImage = [[UIImage imageNamed:@"ballon-img"]
                                      resizableImageWithCapInsets:edgeInsets];
    
    
    //image
    imgBallon = [[UIImageView alloc] initWithImage:backgroundButtonImage];
    imgBallon.frame = CGRectMake(0, 0, 72, 36);
    [imgBallon setContentMode:UIViewContentModeScaleToFill];
    [self.translationView addSubview:imgBallon];
    
    _translationWordLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 8, 72-32, 18)];
    _translationWordLabel.text = @"translation";
    [_translationWordLabel setTextColor:[UIColor colorWithRed:110/255.0f green:181/255.0f blue:229/255.0f alpha:1.0]];
    [_translationWordLabel setFont:[UIFont systemFontOfSize:20.0]];
    [self.translationView addSubview:_translationWordLabel];
    
//    _buttonFavouritres = [UIButton buttonWithType:UIButtonTypeCustom];
//    _buttonFavouritres.frame = CGRectMake(self.translationView.frame.size.width-37, 10, 20, 20);
//    [_buttonFavouritres setImage:[UIImage imageNamed:@"star-normal"] forState:UIControlStateNormal];
//    [_buttonFavouritres addTarget:self action:@selector(addToFavourites:) forControlEvents:UIControlEventTouchUpInside];
//    [self.translationView addSubview:_buttonFavouritres];
    
    [self addSubview:self.translationView];
    
}

#pragma mark - Remove subviews

- (void)cleanControl {
    
    for (UIView *vv in self.subviews) {
        [vv removeFromSuperview];
    }
}

#pragma mark - Helper

- (NSString *)findTranslateWord:(NSString *)sourceText betweenTag:(NSString *)tag {

    NSString *s = sourceText;
    
    NSRange r1 = [s rangeOfString:[NSString stringWithFormat:@"<%@>", tag]];
    NSRange r2 = [s rangeOfString:[NSString stringWithFormat:@"</%@>", tag]];
    NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
    NSString *sub = [s substringWithRange:rSub];
    
    return sub;
}

#pragma mark - Set the word in favourites

- (IBAction)addToFavourites:(id)sender {
    
    if (_vocabTimer != nil && isTranslationShown) {
            
        [_vocabTimer invalidate];
        _vocabTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(stopTheTimer:) userInfo:nil repeats:NO];
            
    }
    
    UIButton *buttonFav = (UIButton *)sender;
    
    NSData *data1 = UIImagePNGRepresentation(buttonFav.currentImage);
    NSData *data2 = UIImagePNGRepresentation([UIImage imageNamed:@"star-normal"]);
    
    if (![data1 isEqual:data2]) {
        
        [buttonFav setImage:[UIImage imageNamed:@"star-normal"] forState:UIControlStateNormal];

    }
    else {
        [buttonFav setImage:[UIImage imageNamed:@"star-selected"] forState:UIControlStateNormal];

    }
    
}


@end
