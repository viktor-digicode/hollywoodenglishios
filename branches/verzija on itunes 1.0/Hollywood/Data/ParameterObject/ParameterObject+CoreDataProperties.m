//
//  ParameterObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 8/21/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "ParameterObject+CoreDataProperties.h"

@implementation ParameterObject (CoreDataProperties)

+ (NSFetchRequest<ParameterObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ParameterObject"];
}

@dynamic key;
@dynamic value;

@end
