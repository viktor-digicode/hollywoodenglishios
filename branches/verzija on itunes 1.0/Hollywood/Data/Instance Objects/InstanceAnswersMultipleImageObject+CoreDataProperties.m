//
//  InstanceAnswersMultipleImageObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersMultipleImageObject+CoreDataProperties.h"

@implementation InstanceAnswersMultipleImageObject (CoreDataProperties)

+ (NSFetchRequest<InstanceAnswersMultipleImageObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"InstanceAnswersMultipleImageObject"];
}

@dynamic afterClickImage;
@dynamic afterClickText;
@dynamic answerClickAudio;
@dynamic answerImage;
@dynamic correctAnswer;

@end
