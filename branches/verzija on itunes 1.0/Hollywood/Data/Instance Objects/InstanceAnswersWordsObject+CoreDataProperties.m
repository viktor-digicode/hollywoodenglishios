//
//  InstanceAnswersWordsObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersWordsObject+CoreDataProperties.h"

@implementation InstanceAnswersWordsObject (CoreDataProperties)

+ (NSFetchRequest<InstanceAnswersWordsObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"InstanceAnswersWordsObject"];
}

@dynamic audio;
@dynamic free;
@dynamic word;

@end
