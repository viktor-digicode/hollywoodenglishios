//
//  InstanceAnswersImageObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersImageObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface InstanceAnswersImageObject (CoreDataProperties)

+ (NSFetchRequest<InstanceAnswersImageObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *afterAudio;
@property (nullable, nonatomic, copy) NSString *defaultImage;
@property (nullable, nonatomic, copy) NSNumber *free;
@property (nullable, nonatomic, copy) NSString *sourceLanguageText;
@property (nullable, nonatomic, copy) NSString *targetLanguageText;

@end

NS_ASSUME_NONNULL_END
