//
//  VipCategoryObject+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 10/3/16.
//  Copyright © 2016 aa. All rights reserved.
////
#import "Constants.h"

#import "VipCategoryObject+CoreDataClass.h"
#import "VipQuizObject+CoreDataClass.h"
//#import "VipQuizObject.h"

@implementation VipCategoryObject

- (void) setData:(NSDictionary*)dataDictionary{
    if([dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.categoryId = [NSNumber numberWithDouble:(double)[[self objectOrNilForKey:kVipCategoriesCategoryId fromDictionary:dataDictionary] doubleValue]];
        self.imageUrl = [self objectOrNilForKey:kVipCategoriesImageUrl fromDictionary:dataDictionary];
        self.categoryName = [self objectOrNilForKey:kVipCategoriesCategoryName fromDictionary:dataDictionary];
        //self.displayOrder = [NSNumber numberWithDouble:[[self objectOrNilForKey:kVipCategoriesDisplayOrder fromDictionary:dataDictionary] doubleValue]];
        self.imageName = [self objectOrNilForKey:kVipCategoriesImageName fromDictionary:dataDictionary];
        self.availability = @1;//[NSNumber numberWithDouble:[[self objectOrNilForKey:kVipCategoriesAvailability fromDictionary:dataDictionary] doubleValue]];
        NSObject *receivedQuizList = [dataDictionary objectForKey:kVipCategoriesQuizList];
        if ([receivedQuizList isKindOfClass:[NSArray class]]) {
            int displayOrder = 0;
            for (NSDictionary *item in (NSArray *)receivedQuizList) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    VipQuizObject *tempVipQuizObject = [VipQuizObject MR_createEntity];
                    tempVipQuizObject.orderInCategory = [NSNumber numberWithInteger:displayOrder];
                    displayOrder++;
                    [tempVipQuizObject setData:item];
                    [self addQuizListObject:tempVipQuizObject];
                }
            }
        } else if ([receivedQuizList isKindOfClass:[NSDictionary class]]) {
            VipQuizObject *tempVipQuizObject = [VipQuizObject MR_createEntity];
            [tempVipQuizObject setData:(NSDictionary*)receivedQuizList];
            [self addQuizListObject:tempVipQuizObject];
        }
        
    }
    
}


#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}
@end
