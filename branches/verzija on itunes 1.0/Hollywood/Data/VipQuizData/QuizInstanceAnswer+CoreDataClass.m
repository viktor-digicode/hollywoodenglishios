//
//  QuizInstanceAnswer+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 10/28/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "QuizInstanceAnswer+CoreDataClass.h"

@implementation QuizInstanceAnswer


- (void) setData:(NSDictionary*)dataDictionary{
    self.answerId =  [NSNumber numberWithDouble:(double)[[self objectOrNilForKey:@"id" fromDictionary: dataDictionary] doubleValue]];
    self.isCorrect = [[self objectOrNilForKey:@"correctAnswer" fromDictionary: dataDictionary] boolValue];
    
    self.imageName = @"";
    self.imageUrl = [self objectOrNilForKey:@"answerImage" fromDictionary:dataDictionary];
    self.answerText = [self objectOrNilForKey:@"answerText" fromDictionary:dataDictionary];
    self.answerClickAudio = [self objectOrNilForKey:@"answerClickAudio" fromDictionary:dataDictionary];
    
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
