//
//  QuizInstance+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 2/6/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "QuizInstance+CoreDataProperties.h"

@implementation QuizInstance (CoreDataProperties)

+ (NSFetchRequest<QuizInstance *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"QuizInstance"];
}

@dynamic bigImageUrl;
@dynamic imageName;
@dynamic imageUrl;
@dynamic instanceQuestion;
@dynamic isOneImageLayout;
@dynamic quizInstanceId;
@dynamic instanceType;
@dynamic answersForInstance;

@end
