//
//  VehicleObjectMultipleImage+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectMultipleImage+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VehicleObjectMultipleImage (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectMultipleImage *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSSet<InstanceAnswersMultipleImageObject *> *instanceAnswers;

@end

@interface VehicleObjectMultipleImage (CoreDataGeneratedAccessors)

- (void)addInstanceAnswersObject:(InstanceAnswersMultipleImageObject *)value;
- (void)removeInstanceAnswersObject:(InstanceAnswersMultipleImageObject *)value;
- (void)addInstanceAnswers:(NSSet<InstanceAnswersMultipleImageObject *> *)values;
- (void)removeInstanceAnswers:(NSSet<InstanceAnswersMultipleImageObject *> *)values;

@end

NS_ASSUME_NONNULL_END
