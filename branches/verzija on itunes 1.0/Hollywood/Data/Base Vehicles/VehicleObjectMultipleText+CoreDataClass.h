//
//  VehicleObjectMultipleText+CoreDataClass.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseVehiclesObject+CoreDataClass.h"

@class InstanceAnswersMultipleTextObject;

NS_ASSUME_NONNULL_BEGIN

@interface VehicleObjectMultipleText : BaseVehiclesObject

- (void) setData:(NSDictionary*)dataDictionary;

@end

NS_ASSUME_NONNULL_END

#import "VehicleObjectMultipleText+CoreDataProperties.h"
