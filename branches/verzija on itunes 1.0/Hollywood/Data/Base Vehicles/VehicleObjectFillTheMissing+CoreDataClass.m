//
//  VehicleObjectFillTheMissing+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectFillTheMissing+CoreDataClass.h"
#import "InstanceAnswersWordsObject+CoreDataClass.h"

@implementation VehicleObjectFillTheMissing

- (void) setData:(NSDictionary*)dataDictionary{
    [super setData:dataDictionary];
    //DLog(@"## %@",dataDictionary);
    
    if( [dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.fillWholeSentence = [NSNumber numberWithInteger:[[self objectOrNilForKey:kVehicleObjectFillWholeSentence  fromDictionary:dataDictionary]integerValue]];
        self.sentence = [self objectOrNilForKey:kVehicleObjectSentence fromDictionary:dataDictionary];
        
        NSObject *receivedLessonsVehicles = [dataDictionary objectForKey:kVehicleObjectWords];
        
        if ([receivedLessonsVehicles isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLessonsVehicles) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    InstanceAnswersWordsObject *importedObject = [InstanceAnswersWordsObject MR_createEntity];
                    //importedObject.instanceID  = (NSNumber*)item;
                    [importedObject setData:item];
                    //[parsedLessonsVehicles addObject:importedObject];
                    [self addVehicleWordsObject:importedObject];
                }
            }
        }
    }
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
