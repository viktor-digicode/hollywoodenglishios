//
//  VocabsObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 9/5/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VocabsObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VocabsObject (CoreDataProperties)

+ (NSFetchRequest<VocabsObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *vocab_id;
@property (nullable, nonatomic, copy) NSString *word;
@property (nullable, nonatomic, copy) NSString *wordAudio;
@property (nullable, nonatomic, retain) NSSet<WordsObject *> *nativeVocabs;

@end

@interface VocabsObject (CoreDataGeneratedAccessors)

- (void)addNativeVocabsObject:(WordsObject *)value;
- (void)removeNativeVocabsObject:(WordsObject *)value;
- (void)addNativeVocabs:(NSSet<WordsObject *> *)values;
- (void)removeNativeVocabs:(NSSet<WordsObject *> *)values;

@end

NS_ASSUME_NONNULL_END
