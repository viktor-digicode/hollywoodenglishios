//
//  VocabsObject+CoreDataClass.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 9/5/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VocabsObject+CoreDataClass.h"
#import "WordsObject+CoreDataClass.h"
#import "VocabsObject+CoreDataProperties.h"



@implementation VocabsObject

- (void)setData:(NSDictionary*)dataDictionary
{
    
    if([dataDictionary isKindOfClass:[NSDictionary class]])
    {
        self.vocab_id = [NSNumber numberWithInteger:[[self objectOrNilForKey:kVocabObjectVocabId fromDictionary:dataDictionary]integerValue]];
        self.word = [self objectOrNilForKey: kWord fromDictionary: dataDictionary];
        self.wordAudio = [self objectOrNilForKey: kVocabWordAudio fromDictionary: dataDictionary];
        
        NSDictionary *nativeDictionary = [dataDictionary objectForKey: kVocabNative];
        for(NSDictionary *word in nativeDictionary)
        {
            WordsObject *importedObject = [WordsObject MR_createEntity];
            [importedObject setData:word];
            
            [self addNativeVocabsObject: importedObject];
        }
    }
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


@end
