//
//  WordsObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 9/5/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "WordsObject+CoreDataProperties.h"

@implementation WordsObject (CoreDataProperties)

+ (NSFetchRequest<WordsObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"WordsObject"];
}

@dynamic word;
@dynamic wordAudio;

@end
