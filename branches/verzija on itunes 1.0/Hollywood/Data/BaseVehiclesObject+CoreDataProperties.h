//
//  BaseVehiclesObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "BaseVehiclesObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface BaseVehiclesObject (CoreDataProperties)

+ (NSFetchRequest<BaseVehiclesObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *audio;
@property (nullable, nonatomic, copy) NSNumber *free;
@property (nullable, nonatomic, copy) NSString *image;
@property (nullable, nonatomic, copy) NSNumber *instanceID;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSNumber *order;
@property (nullable, nonatomic, copy) NSString *questionAudioId;
@property (nullable, nonatomic, copy) NSString *questionText;
@property (nullable, nonatomic, copy) NSString *questionType;
@property (nullable, nonatomic, copy) NSNumber *showInstruction;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSNumber *type;
@property (nullable, nonatomic, copy) NSNumber *unitID;
@property (nullable, nonatomic, copy) NSNumber *isOneTry;

@end

NS_ASSUME_NONNULL_END
