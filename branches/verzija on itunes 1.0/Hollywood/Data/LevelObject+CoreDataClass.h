//
//  LevelObject+CoreDataClass.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RankObject;

NS_ASSUME_NONNULL_BEGIN

@interface LevelObject : NSManagedObject

- (void) setData:(NSDictionary*)dataDictionary;
- (NSSet*)takeRangs;

@end

NS_ASSUME_NONNULL_END

#import "LevelObject+CoreDataProperties.h"
