//
//  AchievementsObject+CoreDataClass.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/11/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface AchievementsObject : NSManagedObject

- (void) setData:(NSDictionary*)dataDictionary;

@end

NS_ASSUME_NONNULL_END

#import "AchievementsObject+CoreDataProperties.h"
