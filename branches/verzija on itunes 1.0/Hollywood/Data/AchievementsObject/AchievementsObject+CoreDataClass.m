//
//  AchievementsObject+CoreDataClass.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/11/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "AchievementsObject+CoreDataClass.h"

@implementation AchievementsObject

- (void) setData:(NSDictionary*)dataDictionary
{
    if([dataDictionary isKindOfClass:[NSDictionary class]])
    {
        self.achievement_id = [NSNumber numberWithInteger:[[self objectOrNilForKey:kRankLessonsLessonId fromDictionary:dataDictionary]integerValue]];
        self.achievementType = [self objectOrNilForKey:kAchievementsObjectType fromDictionary: dataDictionary];
        self.image = [self objectOrNilForKey:kInstanceObjectImage fromDictionary: dataDictionary];
        self.afterImage = [self objectOrNilForKey:kAchievementsObjectAfterImage fromDictionary: dataDictionary];
        self.achivmentText = [self objectOrNilForKey:kAchievementsObjectAchivmentText fromDictionary:dataDictionary];
        self.teaserText = [self objectOrNilForKey:kAchievementsObjectTeaserText fromDictionary:dataDictionary];
        self.award = [NSNumber numberWithInteger:[[self objectOrNilForKey:kAchievementsObjectAward fromDictionary:dataDictionary]integerValue]];
        self.criteria = [NSNumber numberWithInteger:[[self objectOrNilForKey:kAchievementsObjectCriteria fromDictionary:dataDictionary]integerValue]];
    }
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
