//
//  CustomDownloadObject.h
//  TestAsyncImages
//
//  Created by Dimitar Shopovski on 9/29/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ImageCacheObject.h"
#import "ImageCache.h"


@interface CustomDownloadObject : NSObject<NSURLSessionDataDelegate> {
    
    NSMutableData *dataImage;
    NSString *urlString; // key for image cache dictionary
    
    
}
typedef void (^IsCahcingFinished)(BOOL isFinished);

@property (copy, nonatomic) IsCahcingFinished completeBlockGlobal;

- (void)loadImageFromURL:(NSURL *)url withCompletitionBlock:(IsCahcingFinished)completitionBlock;
- (BOOL)checkIsImageInCache:(NSURL *)urlImage;
- (UIImage *)getImageFromCache:(NSURL *)urlImage;

@end
