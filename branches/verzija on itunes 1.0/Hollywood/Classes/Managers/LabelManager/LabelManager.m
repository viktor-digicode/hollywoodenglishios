//
//  LMLabelManager.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 6/21/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LabelManager.h"

@implementation LabelManager


#ifdef LABELMANAGER_SINGLETON



SINGLETON_GCD(LabelManager)
#endif

- (NSArray *)getArrayLabelManager
{
    return [[LabelTextObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
}

- (NSString *)setLocalizeLabelText:(NSString *)text
{
    NSString *resultText = [DATA_MANAGER getLabelTextForKey:text];
    
    if(resultText)
    {
        return resultText;
    }
    return Localized(text);
}


- (void)deleteLabellObjects:(int)labelOrder
{
    NSArray *dataArray = [[LabelTextObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    
    for(int i = 0; i < [dataArray count]; i++)
    {
        LabelTextObject *labelTextObject = [dataArray objectAtIndex: i];
        [labelTextObject MR_deleteEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    }
    
    [self saveData];
}

- (void)saveData
{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
            NSLog(@"You successfully saved your context.");
        } else if (error) {
            NSLog(@"Error saving context: %@", error.description);
        }
    }];
}



@end


