//
//  LMAnalyticsManager.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 10/4/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AnalyticConstants.h"

#pragma mark - Analytics singlethon

@interface LMAnalyticsManager : NSObject

@property(nonatomic, strong) NSString *productName;

- (id)initWithProductName:(NSString *)productName;

#pragma mark - Events

- (void)sendAnalyticsForScreen:(NSString *)screenName;
- (void)sendAnalyticsForSceeen:(NSString *)screenName withAction:(NSString *)action;
- (void)sendAnalyticsForEvent:(NSString *)eventName
                       action:(NSString *)actionName
               additionalData:(NSString *)data;

- (void)sendAnalyticsForEvent:(NSString *)eventName
                       action:(NSString *)actionName
                    labelName:(NSString *)label
                        value:(NSNumber *)value;

- (void)sendCategorySelectedEvent:(NSInteger)categoryId;
- (void)sendQuizSelectedEvent:(NSInteger)quizId withActionName:(NSString *)actionName;
- (void)sendQuizVehiclePresented:(NSInteger)vehicleId withActionName:(NSString *)actionName;
- (void)sendSwipeBetweenInstance:(NSString *)actionName;

- (void)enterTheVehicleEvent:(NSString *)vehicleName;
- (void)leaveTheVehicleEvent:(NSString *)vehicleName otherData:(NSDictionary *)statsDictionary;



@end

#ifdef ANALYTICS_SINGLETON

@interface LMAnalyticsManager ()
+ (id)sharedInstance;
@end

#define ANALYTICS_MANAGER ((LMAnalyticsManager *)[LMAnalyticsManager sharedInstance])
#endif

