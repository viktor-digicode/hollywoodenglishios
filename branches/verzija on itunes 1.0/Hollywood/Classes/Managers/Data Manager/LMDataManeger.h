//
//  LMDataManeger.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandMethods.h>

#import "ProgressUserObject.h"
#import "ProgressLevelObject.h"
#import "ProgressLessonObject.h"
#import "ProgressVehicleObject.h"

#import "VipCategoryObject+CoreDataClass.h"
#import "VipQuizObject+CoreDataClass.h"
#import "VipCourseObject+CoreDataClass.h"

#import "LabelTextObject+CoreDataClass.h"
#import "AchievementsObject+CoreDataClass.h"
#import "VocabsObject+CoreDataClass.h"
#import "LessonObject+CoreDataClass.h"
#import "BaseVehiclesObject+CoreDataClass.h"

#define DATA_MANAGER ((LMDataManeger *)[LMDataManeger sharedInstance])


@interface LMDataManeger : NSObject

+ (id)sharedInstance;

- (void)initializeStartData:(NSDictionary*)startDictionary;

- (LessonObject*)takeLessonObjectObject:(int)lessonOrder;
- (void)resetLessonOrder;
- (NSArray*)takeDataForLevel:(NSNumber*)levelOrder;
- (NSArray*)takeVehicleForCurrentLesson;
- (NSArray*)getAllProgressObject;
- (void)deleteProgressData;

- (BaseVehiclesObject*)takeVehicleObject:(int)vehiclelessonOrder  andLessonObject:(int)lessonOrder  andRankObject:(int)rankOrder andLevelObject:(int)levelOrder;
- (NSArray*)takeAllVehiclesObjectInLesson:(int)lessonOrder  andRankObject:(int)rankOrder andLevelObject:(int)levelOrder;
- (NSArray*)takeAllVehiclesObjectInLesson:(int)lessonOrder;
- (NSArray*)takeAllLessonObject;

- (void)makeNewLabelsData:(NSDictionary*)dataDictionary;

- (void)deleteLevelObject:(int)levelOrder;

- (void) saveData;

//////////////////////////////////////////////Progress Data//////////////////////////////////////////////

- (ProgressLessonObject*)getCurrentLessonObject;
- (ProgressUserObject*)getCurrentUserProgressObject;
- (ProgressLevelObject*)getCurrentProgressLevelObject;
- (ProgressLessonObject*)getLessonObjectForIndex:(int)lessonOrder;


//////////////////////////////////////////////Vip Data//////////////////////////////////////////////
- (void)makeVipCategoryCourseData:(NSDictionary*)dataDictionary;

- (VipCourseObject*)getVipCourseObject;
- (NSArray *)getVIPCategories;
- (NSArray*)getAllVipCategory;
- (NSArray*)takeAllRankObjects;

- (NSString *)getLabelTextForKey:(NSString *)key;
- (NSString *)getParameterTextForKey:(NSString *)key;
- (NSNumber*)getLessonOrder;

- (void)saveLessonIdInFreeLessons:(NSNumber *)lessonId;
- (BOOL)isFreeLessonWithId:(NSNumber *)lessonId;

@end
