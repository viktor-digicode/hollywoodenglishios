//
//  LMDataManeger.m
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "LMDataManeger.h"
#import "CourseObject+CoreDataClass.h"
#import "LevelObject+CoreDataClass.h"
#import "RankObject+CoreDataClass.h"




@implementation LMDataManeger{
    NSNumber *currentLesson;
    int startLessonOrder;
}

SINGLETON_GCD(LMDataManeger)


- (id)init
{
    self = [super init];
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    //[self initializeStartData];
    if (self) {
        startLessonOrder = -1;
       /* if ([[self getStartData] count]) {
            
        } else {
            [self initializeStartData];
            [self initProgressData];
        }*/
    }
    return self;
}

- (NSNumber*)getLessonOrder{
    startLessonOrder ++;
    return [NSNumber numberWithInt:startLessonOrder];
}

- (void)resetLessonOrder{
    startLessonOrder = -1;
}

- (NSArray*)getStartData{
    NSPredicate *dataPredicate1 = [NSPredicate predicateWithFormat:LEVEL_SEARCH_PREDICATE, @"Beginner"];
    NSArray *sortArray1 = [[LevelObject MR_findAllSortedBy:@"level_name"
                                                 ascending:YES
                                             withPredicate:dataPredicate1
                                                 inContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    if([sortArray1 count]){
        LevelObject *levelObject1 = [sortArray1 objectAtIndex:0];
        NSLog(@"level_name: %@ ",levelObject1.level_name);
        NSSet *set1 = [levelObject1 takeRangs] ;
        //NSArray *myArray = [set1 allObjects];
        //RankObject *object = [myArray objectAtIndex:0];
    }
    
    
    
    NSPredicate *dataPredicate = [NSPredicate predicateWithFormat:SEARCH_PREDICATE, @"Hollywood"];
    NSArray *sortArray = [[CourseObject MR_findAllSortedBy:@"course_name"
                                            ascending:YES
                                        withPredicate:dataPredicate
                                            inContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    if([sortArray count]){
        CourseObject *CourseObject1 = [sortArray objectAtIndex:0];
        @try {
            
        NSSet *set1 = CourseObject1.course_levels ;
        NSArray *myArray = [set1 allObjects];
        LevelObject *rObject = [myArray objectAtIndex:0];
        
        NSSet *set2 = rObject.level_ranks ;
        NSArray *myArray2 = [set2 allObjects];
        RankObject *object2 = [myArray2 objectAtIndex:0];
        
        NSSet *set3 = object2.rank_lessons;
        NSArray *myArray3 = [set3 allObjects];
        LessonObject *object3 = [myArray3 objectAtIndex:0];
        
            
        NSSet *set4 = object3.lesson_vehicles;
        NSArray *myArray4 = [set4 allObjects];
        BaseVehiclesObject *object4 = [myArray4 objectAtIndex:0];
         
        }
        @finally {
            
        }

        return sortArray;
    }
    return @[];
    
}

- (void)initializeStartData:(NSDictionary*)startDictionary{
    if ([self takeLevelObject:0]) {
        
    }else{
        ///
        //remove lesson after revision change
        NSArray *allLessonObject = [self takeAllLessonObject];
        for (NSInteger i=0; i < [allLessonObject count]; i++) {
            LessonObject *lessonObject = [allLessonObject objectAtIndex:i];
            [lessonObject MR_deleteEntityInContext:[NSManagedObjectContext MR_defaultContext]];
        }
        //
        [self makeCourseData:startDictionary];
        [self saveData];
        
        [self initProgressData];
        
        //[self makeVipCategoryCourseData:[self takeInitialVipCategoryJson]];
        [self saveData];
    }
}

- (void)initializeStartData{
    [self makeCourseData:[self takeInitialJson]];
    [self saveData];
    [self getStartData];
    
    //[self makeVipCategoryCourseData:[self takeInitialVipCategoryJson]];
    [self saveData];
}

- (void)saveData{
    dispatch_async(
                   dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                           if (success) {
                               NSLog(@"You successfully saved your context.");
                           } else if (error) {
                               NSLog(@"Error saving context: %@", error.description);
                           }
                       }];
                   }
    );

}

- (void)makeCourseData:(NSDictionary*)dataDictionary{
    if(!dataDictionary)
        return;
    NSDictionary *responseDictionary = [self objectOrNilForKey:kApiObjectResponse fromDictionary:dataDictionary];
    
    CourseObject *importedObject =[CourseObject MR_createEntity];
    [importedObject setData:responseDictionary];
    
    [self makeLabelsData: responseDictionary];
    [self makeAchievementsData: responseDictionary];
    [self makeVocabsData:responseDictionary];
}

- (void)makeLabelsData:(NSDictionary*)dataDictionary{
    if(!dataDictionary)
        return;
    NSDictionary *responseDictionary = [self objectOrNilForKey:kApiObjectLabels fromDictionary:dataDictionary];

    for (NSDictionary *dict in [self objectOrNilForKey:kApiObjectLabelList fromDictionary:responseDictionary])
    {
        LabelTextObject *importObject = [LabelTextObject MR_createEntity];
        [importObject setDataWithKey: dict.description andValue: [[self objectOrNilForKey:kApiObjectLabelList fromDictionary:responseDictionary] valueForKey:dict.description]];
    }
}

- (void)makeNewLabelsData:(NSDictionary*)dataDictionary{
    if(!dataDictionary)
        return;
    
    NSDictionary *responseDictionary = [self objectOrNilForKey:kApiObjectResponse fromDictionary:dataDictionary];
    
    for (NSDictionary *dict in [self objectOrNilForKey:kApiObjectLabelList fromDictionary:responseDictionary])
    {
        LabelTextObject *importObject = [LabelTextObject MR_createEntity];
        [importObject setDataWithKey: dict.description andValue: [[self objectOrNilForKey:kApiObjectLabelList fromDictionary:responseDictionary] valueForKey:dict.description]];
    }
}
- (void)makeAchievementsData:(NSDictionary *)dataDictionary
{
    if(!dataDictionary)
        return;
    
    NSDictionary *responseDictionary = [self objectOrNilForKey:kApiObjectAchievements fromDictionary:dataDictionary];
    
    for(NSDictionary *dict in responseDictionary)
    {
        AchievementsObject *importObject = [AchievementsObject MR_createEntity];
        [importObject setData:dict];
    }
}

- (void)makeVocabsData:(NSDictionary *)dataDictionary
{
    if(!dataDictionary)
        return;
    
    NSDictionary *responseDictionary = [self objectOrNilForKey: kApiObjectVocabs fromDictionary:dataDictionary];
    
    for(NSDictionary *dict in responseDictionary)
    {
        VocabsObject *importObject = [VocabsObject MR_createEntity];
        [importObject setData:dict];
    }
    
//    NSString *s1 = [VOCAB_MANAGER getVocabForString:@"frase"];
//    NSLog(@"%@", s1);
    
}



- (NSMutableDictionary *)takeInitialJson
{
    
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"InitialData" ofType:@"bundle"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:bundlePath]) {
        NSBundle *bundle = [[NSBundle alloc] initWithPath:bundlePath];
        if (bundle) {
            [bundle load];
            NSString *jsonPath = [bundle pathForResource:@"course_prototype" ofType:@"json"];
            if (![[NSFileManager defaultManager] fileExistsAtPath:jsonPath])
                return nil;
            
            NSData *data = [NSData dataWithContentsOfFile:jsonPath];
            NSError *error = nil;
            NSDictionary *jsonRoot = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            return [jsonRoot copy];
        }
    }
    return nil;
}

- (NSArray*)takeDataForLevel:(NSNumber*)levelOrder{
    NSArray *dataArray = @[];
    return dataArray;
}

- (NSArray*)takeVehicleForCurrentLesson{
    NSArray *dataArray = @[@1];
    return dataArray;
}
- (NSArray*)takeVehicleForLesson:(NSNumber*)lessonOrder{
    
    NSArray *dataArray = @[@1];
    return dataArray;
}
///
#pragma mark Level objects
- (LevelObject*)takeLevelObject:(int)levelOrder{
    NSPredicate *dataPredicate1 = [NSPredicate predicateWithFormat:LEVEL_SEARCH_PREDICATE, @"Beginner"];
    NSArray *sortArray1 = [[LevelObject MR_findAllSortedBy:@"level_name"
                                                 ascending:YES
                                             withPredicate:dataPredicate1
                                                 inContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    if([sortArray1 count] > levelOrder){
       
        LevelObject *tempLevelObject = [sortArray1 objectAtIndex:levelOrder];
        NSLog(@"level_name: %@ ",tempLevelObject.level_name);
        return tempLevelObject;
    }
    
    return nil;
}

- (void)deleteLevelObject:(int)levelOrder
{
    NSManagedObject *obj = [self takeLevelObject:0];
    [obj MR_deleteEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    
    
    NSArray *dataArray = [[RankObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    for(RankObject *rankObj in dataArray)
    {
         [rankObj MR_deleteEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    }
    
    NSArray *dataLessonArray = [[LessonObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    for(LessonObject *lessonObj in dataLessonArray)
    {
        [lessonObj MR_deleteEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    }
    
    NSArray *dataVehiclesArray = [[BaseVehiclesObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    
    for(BaseVehiclesObject *tempVehiclesObject in dataVehiclesArray)
    {
        [tempVehiclesObject MR_deleteEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    }
    
    
    [self saveData];
}

- (RankObject*)takeRankObject:(int)rankOrder andLevelObject:(int)levelOrder{
    LevelObject *tempLevelObject = [self takeLevelObject:levelOrder];
    NSSet *set1 = [tempLevelObject takeRangs] ;
    NSArray *myArray2 = [set1 allObjects];
    
    if([myArray2 count] > rankOrder){
        ////sorting
        NSSortDescriptor *sortDescriptor2;
         sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"rank_number"
         ascending:YES];
         NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
         NSArray *sorted2Array = [myArray2 sortedArrayUsingDescriptors:sortDescriptors2];
        //
        RankObject *tempRankObject = [sorted2Array objectAtIndex:rankOrder];
        return tempRankObject;
    }
    return nil;
}

- (NSArray*)takeAllLessonObject{
    //[MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    NSArray *dataArray = [[LessonObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"lesson_order"
                                                  ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [dataArray sortedArrayUsingDescriptors:sortDescriptors2];
    return sorted2Array;
}

- (LessonObject*)takeLessonObject:(int)lessonOrder andRankObject:(int)rankOrder andLevelObject:(int)levelOrder{
    
    RankObject *tempRankObject = [self takeRankObject:rankOrder andLevelObject:levelOrder];
    NSSet *set3 = tempRankObject.rank_lessons;
    NSArray *myArray3 = [set3 allObjects];
    if([myArray3 count] > lessonOrder){
        NSSortDescriptor *sortDescriptor2;
        sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"lesson_order"
                                                      ascending:YES];
        NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
        NSArray *sorted2Array = [myArray3 sortedArrayUsingDescriptors:sortDescriptors2];
        
        LessonObject *tempLessonObject = [sorted2Array objectAtIndex:lessonOrder];
        return tempLessonObject;
    }
    return nil;
}

- (LessonObject*)takeLessonObjectObject:(int)lessonOrder{
    NSArray *allLessonObject = [DATA_MANAGER takeAllLessonObject];
    LessonObject *tempLessonObject;
    if([allLessonObject count] > lessonOrder){
        tempLessonObject = [allLessonObject objectAtIndex:lessonOrder];
    }else{
        tempLessonObject = [allLessonObject objectAtIndex:lessonOrder-1];
    }
    return tempLessonObject;
}

- (BaseVehiclesObject*)takeVehicleObject:(int)vehiclelessonOrder  andLessonObject:(int)lessonOrder  andRankObject:(int)rankOrder andLevelObject:(int)levelOrder{

    LessonObject *tempLessonObject = [self takeLessonObject:lessonOrder  andRankObject:rankOrder andLevelObject:levelOrder];
    NSSet *set4 = tempLessonObject.lesson_vehicles;
    NSArray *myArray4 = [set4 allObjects];

    if([myArray4 count] > lessonOrder){
        NSSortDescriptor *sortDescriptor2;
        sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"order"
                                                      ascending:YES];
        NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
        NSArray *sorted2Array = [myArray4 sortedArrayUsingDescriptors:sortDescriptors2];
        
        BaseVehiclesObject *tempBaseVehiclesObject = [sorted2Array objectAtIndex:vehiclelessonOrder];
        return tempBaseVehiclesObject;
    }
    return nil;
}

- (NSArray*)takeAllVehiclesObjectInLesson:(int)lessonOrder  andRankObject:(int)rankOrder andLevelObject:(int)levelOrder{

    LessonObject *tempLessonObject = [self takeLessonObject:lessonOrder  andRankObject:rankOrder andLevelObject:levelOrder];
    NSSet *set4 = tempLessonObject.lesson_vehicles;
    NSArray *myArray4 = [set4 allObjects];
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"order"
                                                  ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [myArray4 sortedArrayUsingDescriptors:sortDescriptors2];
    
    return sorted2Array;
}

- (NSArray*)takeAllVehiclesObjectInLesson:(int)lessonOrder{
    NSArray *allLessonObject = [DATA_MANAGER takeAllLessonObject];
    LessonObject *tempLessonObject;
    if([allLessonObject count] > lessonOrder){
        tempLessonObject = [allLessonObject objectAtIndex:lessonOrder];
    }else{
        tempLessonObject = [allLessonObject objectAtIndex:lessonOrder-1];
    }
    NSSet *set4 = tempLessonObject.lesson_vehicles;
    NSArray *myArray4 = [set4 allObjects];
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"order"
                                                  ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [myArray4 sortedArrayUsingDescriptors:sortDescriptors2];
    
    return sorted2Array;
    
}



- (NSArray*)takeAllRankObjects{
    NSArray *dataArray = [[RankObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"rank_number"
                                                  ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [dataArray sortedArrayUsingDescriptors:sortDescriptors2];
    return sorted2Array;
}

#pragma mark VipCategory Data
- (NSMutableDictionary *)takeInitialVipCategoryJson
{
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"InitialData" ofType:@"bundle"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:bundlePath]) {
        NSBundle *bundle = [[NSBundle alloc] initWithPath:bundlePath];
        if (bundle) {
            [bundle load];
            NSString *jsonPath = [bundle pathForResource:@"vip_categori" ofType:@"json"];
            if (![[NSFileManager defaultManager] fileExistsAtPath:jsonPath])
                return nil;
            
            NSData *data = [NSData dataWithContentsOfFile:jsonPath];
            NSError *error = nil;
            NSDictionary *jsonRoot = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            return [jsonRoot copy];
        }
    }
    return nil;
}

- (void)makeVipCategoryCourseData:(NSDictionary*)dataDictionary{
    if(!dataDictionary)
        return;
    
    VipCourseObject *importedObject =[VipCourseObject MR_createEntity];
    [importedObject setData:dataDictionary];
}

- (VipCourseObject*)getVipCourseObject{
//    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    NSArray *dataArray = [[VipCourseObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    if([dataArray count] == 0){
        return nil;
    }
    VipCourseObject *tempVipCourseObject = [dataArray objectAtIndex:0];
    return tempVipCourseObject;
}

- (NSArray*)getAllVipCategory {
    
    VipCourseObject *tempVipCourseObject = [self getVipCourseObject];
    
    NSSet *set2 = tempVipCourseObject.vipCategories;
    NSArray *data2Array = [set2 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"displayOrder"
                                                  ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [data2Array sortedArrayUsingDescriptors:sortDescriptors2];
    
    sorted2Array = [self checkForUnlockedQuizes:sorted2Array];
    
    return sorted2Array;
}

#pragma mark - Help method

- (NSArray *)checkForUnlockedQuizes:(NSArray *)sourceArray {
    
    NSMutableArray *arrayVipCategories = [sourceArray mutableCopy];
    
    for (VipCategoryObject *vipQategory in arrayVipCategories) {
        
        NSSet *set = [vipQategory quizList];
        NSArray *arrayQuizes = [set allObjects];
        
        for (VipQuizObject *vipQuiz in arrayQuizes) {
            
            NSArray *arrayPurchasedQuizes = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDataQuizList];
            BOOL isQuizPurchased = [self theQuizIsPurchased:arrayPurchasedQuizes quizToCompare:vipQuiz.quizId];

            vipQuiz.purchased = isQuizPurchased;
        }
    }
    
    
    return arrayVipCategories;
}

- (BOOL)theQuizIsPurchased:(NSArray *)arrayQizzesId quizToCompare:(NSNumber *)quizId {
    
    for (NSNumber *currentQuizId in arrayQizzesId) {
        
        if (currentQuizId == quizId) {
            
            return YES;
        }
    }
    
    return NO;
}


#pragma mark Progress Data

- (void)initProgressData
{
//    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    if ([[self getAllProgressObject] count]) {
        
    } else {
        [self initializeProgressData];
    }
}

- (NSArray*)getAllProgressObject{
//    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    NSArray *dataArray = [[ProgressUserObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    return dataArray;
}

- (void)initializeProgressData{
    
    
    ProgressUserObject *progresssObject =[ProgressUserObject MR_createEntity];
    [progresssObject setInitialData];
       
    [self saveData];
}

- (void)deleteProgressData{
    [self resetLessonOrder];
//    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    NSArray *dataArray = [[ProgressUserObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    NSManagedObject *obj = dataArray[0];
    [obj MR_deleteEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    
    [self saveData];
    
}
#pragma mark get curent progress

- (ProgressUserObject*)getCurrentUserProgressObject{
//    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    NSArray *dataArray = [[ProgressUserObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    ProgressUserObject *tempProgressUser = [dataArray objectAtIndex:0];
    return tempProgressUser;
}

- (ProgressLessonObject*)getLessonObjectForIndex:(int)lessonOrder {
//    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    NSArray *dataArray = [[ProgressUserObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    ProgressUserObject *tempProgressUser = [dataArray objectAtIndex:0];
    
    NSSet *set2 = tempProgressUser.levelsProgres ;
    NSArray *data2Array = [set2 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"levelOrder"
                                                  ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [data2Array sortedArrayUsingDescriptors:sortDescriptors2];
    /////
    
    ProgressLevelObject *tempProgressLevel = [sorted2Array objectAtIndex:tempProgressUser.currentLevel];
    NSSet *set3 = tempProgressLevel.lessonsProgres ;
    NSArray *data3Array = [set3 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor3;
    sortDescriptor3 = [[NSSortDescriptor alloc] initWithKey:@"lessonOrder"
                                                  ascending:YES];
    NSArray *sortDescriptors3 = [NSArray arrayWithObject:sortDescriptor3];
    NSArray *sorted3Array = [data3Array sortedArrayUsingDescriptors:sortDescriptors3];
    ///
    
    ProgressLessonObject *tempProgressLesson = [sorted3Array objectAtIndex:lessonOrder];
    //ProgressLessonObject *tempProgressLesson = [sorted3Array objectAtIndex:tempProgressLevel.currentUnit];
    
    return tempProgressLesson;
}


- (ProgressLessonObject*)getCurrentLessonObject{
//    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    NSArray *dataArray = [[ProgressUserObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    ProgressUserObject *tempProgressUser = [dataArray objectAtIndex:0];
    
    NSSet *set2 = tempProgressUser.levelsProgres ;
    NSArray *data2Array = [set2 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"levelOrder"
                                                 ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [data2Array sortedArrayUsingDescriptors:sortDescriptors2];
    /////
    
    ProgressLevelObject *tempProgressLevel = [sorted2Array objectAtIndex:tempProgressUser.currentLevel];
    NSSet *set3 = tempProgressLevel.lessonsProgres ;
    NSArray *data3Array = [set3 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor3;
    sortDescriptor3 = [[NSSortDescriptor alloc] initWithKey:@"lessonOrder"
                                                  ascending:YES];
    NSArray *sortDescriptors3 = [NSArray arrayWithObject:sortDescriptor3];
    NSArray *sorted3Array = [data3Array sortedArrayUsingDescriptors:sortDescriptors3];
    ///
    
    ProgressLessonObject *tempProgressLesson = [sorted3Array objectAtIndex:tempProgressLevel.currentUnit];
    
    return tempProgressLesson;
}

- (ProgressLevelObject*)getCurrentProgressLevelObject{
//    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    NSArray *dataArray = [[ProgressUserObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    ProgressUserObject *tempProgressUser = [dataArray objectAtIndex:0];
    
    NSSet *set2 = tempProgressUser.levelsProgres ;
    NSArray *data2Array = [set2 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"levelOrder"
                                                  ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [data2Array sortedArrayUsingDescriptors:sortDescriptors2];
    /////
    
    ProgressLevelObject *tempProgressLevel = [sorted2Array objectAtIndex:tempProgressUser.currentLevel];
    return tempProgressLevel;
}

#pragma mark - Get Categories
- (NSArray *)getVIPCategories {
    
    //get json
    NSArray *jsonCategories = [self getJSONObjectFromFile:@"categories"];
    
    return jsonCategories;
}

#pragma mark - Helper method take JSON data

- (id)getJSONObjectFromFile:(NSString *)fileName {
    
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:jsonPath]) {
        
        NSData *data = [NSData dataWithContentsOfFile:jsonPath];
        NSError *error = nil;
        id jsonRoot = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        return [jsonRoot copy];
        
    }
    return nil;
    
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - TextForKey support methods

- (NSString *)getLabelTextForKey:(NSString *)key
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat: TEXT_SEARCH_PREDICATE, key];
    LabelTextObject *result = [LabelTextObject MR_findFirstWithPredicate:predicate];
    
    return result.value;
}

- (NSString *)getParameterTextForKey:(NSString *)key
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat: TEXT_SEARCH_PREDICATE, key];
    ParameterObject *result = [ParameterObject MR_findFirstWithPredicate: predicate];
    
    return [result.value description];
}

#pragma mark - User Defaults methods for is free

- (NSArray *)getAllFreeLessons {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    return [defaults objectForKey:@"freeLessonsIds"];
}

- (BOOL)isFreeLessonWithId:(NSNumber *)lessonId {
    
    NSArray *arrayFreeLessonIds = [self getAllFreeLessons];
    
    if ([arrayFreeLessonIds containsObject:lessonId]) {
        
        return YES;
    }
    
    return NO;
}

- (void)saveLessonIdInFreeLessons:(NSNumber *)lessonId {
    
    NSMutableArray *tempArray = [[self getAllFreeLessons] mutableCopy];
    if (!tempArray) {
        tempArray = [NSMutableArray new];
    }
    
    if (![tempArray containsObject:lessonId]) {
        
        [tempArray addObject:lessonId];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:tempArray forKey:@"freeLessonsIds"];
        [defaults synchronize];
    }
    
}

@end



