//
//  CustomDownloadObject.m
//  TestAsyncImages
//
//  Created by Dimitar Shopovski on 9/29/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import "CustomDownloadObject.h"

//
// Key's are URL strings.
// Value's are ImageCacheObject's
//
static ImageCache *imageCache = nil;

@implementation CustomDownloadObject

#pragma mark - LoadImage

- (void)loadImageFromURL:(NSURL *)url withCompletitionBlock:(IsCahcingFinished)completitionBlock {
    
    self.completeBlockGlobal = completitionBlock;
    
    if (dataImage != nil) {
        
        dataImage = nil;
    }
    
    if (imageCache == nil) // lazily create image cache
        imageCache = [[ImageCache alloc] initWithMaxSize:2*1024*1024];  // 2 MB Image cache
    
    
    urlString = [[url absoluteString] copy];
    UIImage *cachedImage = [imageCache imageForKey:urlString];
    if (cachedImage != nil) {
    
        self.completeBlockGlobal(YES);
    }
    else {
    
        NSURLRequest *request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:60.0];
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
        NSURLSessionTask *task = [session dataTaskWithRequest:request];
        [task resume];
    }
    
}

#pragma mark - URL Session delegate

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    
    if (dataImage==nil) {
        dataImage = [[NSMutableData alloc] initWithCapacity:2048];
    }
    [dataImage appendData:data];
    
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    
    if (!error) {
        
        UIImage *image = [UIImage imageWithData:dataImage];
        [imageCache insertImage:image withSize:[dataImage length] forKey:urlString];
        dataImage = nil;
        self.completeBlockGlobal(YES);

    }
    else {
        
        self.completeBlockGlobal(NO);

    }
}

#pragma mark - Check image in cache
- (BOOL)checkIsImageInCache:(NSURL *)urlImage {
    
    if (imageCache == nil) // lazily create image cache
        imageCache = [[ImageCache alloc] initWithMaxSize:2*1024*1024];  // 2 MB Image cache
    
    
    urlString = [[urlImage absoluteString] copy];
    UIImage *cachedImage = [imageCache imageForKey:urlString];
    if (cachedImage) {
    
        return YES;
    }

    return NO;
}

- (UIImage *)getImageFromCache:(NSURL *)urlImage {
    
    if (imageCache == nil) // lazily create image cache
        imageCache = [[ImageCache alloc] initWithMaxSize:2*1024*1024];  // 2 MB Image cache
    
    
    urlString = [[urlImage absoluteString] copy];
    UIImage *cachedImage = [imageCache imageForKey:urlString];
    if (cachedImage) {
        
        return cachedImage;
    }
    
    return nil;
}

@end
