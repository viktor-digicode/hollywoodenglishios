//
//  ParameterManager.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 8/21/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "ParameterManager.h"

@implementation ParameterManager


#ifdef PARAMETERMANAGER_SINGLETON



SINGLETON_GCD(ParameterManager)
#endif

- (NSArray *)getArrayParameterManager
{
    return [[ParameterObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
}

- (NSString *)getLocalizeParameterText:(NSString *)text
{
    NSString *resultText = [DATA_MANAGER getParameterTextForKey: text];
    
    if(resultText)
    {
        return resultText;
    }
    return Localized(text);
}

- (int)getParameterAttempts
{
    return 2;
}

- (void)deleteParameterObjects
{
    NSArray *dataArray = [[ParameterObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    
    for(int i = 0; i < [dataArray count]; i++)
    {
        ParameterObject *parameterObject = [dataArray objectAtIndex: i];
        [parameterObject MR_deleteEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    }
    
    [self saveData];
}

- (void)saveData
{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
            NSLog(@"You successfully saved your context.");
        } else if (error) {
            NSLog(@"Error saving context: %@", error.description);
        }
    }];
}

- (void)initializeStartData:(NSDictionary*)dictionary
{
    NSDictionary *parameterDictonary = [dictionary valueForKey: kApiObjectAppParameters];
    NSDictionary *parameterValueDictionary = [parameterDictonary valueForKey:@"appParamaterValues"];

    for (NSDictionary *dict in parameterValueDictionary)
    {
        ParameterObject *importObject = [ParameterObject MR_createEntity];
        [importObject setDataWithKey: dict.description andValue: [parameterValueDictionary valueForKey:dict.description]];
        NSLog(@"%@", importObject);
    }
    
    [self saveData];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
