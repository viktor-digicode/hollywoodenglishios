//
//  LMAlertManager.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 8/7/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FCAlertView/FCAlertView.h>

#define ALERT_MANAGER ((AlertManager *)[AlertManager sharedInstance])

@interface AlertManager : NSObject

- (FCAlertView *)setAlertViewWithTitle:(NSString *)title andSubstring: (NSString *)subtitle;
- (void)setAlertViewWith:(NSString *)title and: (NSString *)subtitle;
- (void)showCustomAlertWithTitle:(NSString *)title andMessage:(NSString *)message;

@end


#ifdef ALERTMANAGER_SINGLETON

@interface AlertManager ()

+ (id)sharedInstance;

@end

#define ALERT_MANAGER ((AlertManager *)[AlertManager sharedInstance])
#endif
