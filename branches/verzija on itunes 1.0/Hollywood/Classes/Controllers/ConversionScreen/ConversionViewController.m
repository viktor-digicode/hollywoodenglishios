//
//  ConversionViewController.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 10/21/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "ConversionViewController.h"

@interface ConversionViewController ()

@end

@implementation ConversionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kConversionScreen action:@"Not_Free_Lesson_Conversion" additionalData:nil];
}

- (IBAction)closeConversionScreen:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Orientations
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskPortrait;
    
}

@end
