//
//  SelectCountryViewController.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/12/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LMBaseViewController.h"
#import <MWFeedParser/MWFeedParser.h>
#import <TPKeyboardAvoidingScrollView.h>

@interface SelectCountryViewController : LMBaseViewController<MWFeedParserDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *selectCountryTextField;
@property (weak, nonatomic) IBOutlet UITextField *msisdnTextField;
@property (weak, nonatomic) IBOutlet UITextField *prefixTextField;


@property (weak, nonatomic) IBOutlet UIImageView *imageViewUserProfile;
@property (weak, nonatomic) IBOutlet UILabel *userVipPassesLabel;
@property (weak, nonatomic) IBOutlet UILabel *vipStaticTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleTextLabel;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@property (nonatomic, copy) NSArray *countryPickerArray;

@end
