//
//  HomePageCollectionViewCell.m
//  Hollywood
//
//  Created by Kiril Kiroski on 8/8/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "HomePageCollectionViewCell.h"
#import <GPUImage/GPUImageSaturationFilter.h>
#import <GPUImage/GPUImagePicture.h>

@implementation HomePageCollectionViewCell{
    
    __weak IBOutlet NSLayoutConstraint *arrowHeightConstraint;
    __weak IBOutlet NSLayoutConstraint *arrowWithConstraint;
    __weak IBOutlet NSLayoutConstraint *thumbnailImageWithConstraint;
    __weak IBOutlet NSLayoutConstraint *numberBottomConstraint;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setLessonData:(id)lessonData {
    
    NSInteger status = [[lessonData objectForKey:@"lessonStatus"] integerValue];
    
    self.labelLessonInfo.numberOfLines = 0;
    self.labelLessonInfo.x = 4.0;
    self.labelLessonInfo.y = 2.0;
    self.labelLessonInfo.width = 100.0;
    self.labelLessonInfo.height = 37.0;
    
    [self.lessonThumbnailImageView setImageWithURL:[NSURL URLWithString:[lessonData objectForKey:@"lessonImage"]] placeholderImage:nil];
    
    switch (status) {
        case 1:
            //unlocked
            arrowHeightConstraint.constant = 40;
            arrowWithConstraint.constant = 41;
            self.lessonStatusImageView.image = [UIImage imageNamed:@"lesson-unlocked"];
            self.lessonStatusImageView.contentMode = UIViewContentModeScaleAspectFit;
            self.cellBg.image = [UIImage imageNamed:@"hpc-bg"];
            self.lessonThumbnailImageView.hidden = NO;
            //self.constraintStatusVsThumb.constant = 0;
            self.constraintStatusVsThumb.constant = self.lessonThumbnailImageView.width/2;
            self.labelLessonInfo.text = @"Lesson\nCompleted.";
            [self.labelLessonInfo setTextColor:color (138, 128, 72,1)];
            [self.labelLessonNumber setTextColor:color (138, 128, 72,1)];
            break;
        case 2:
            //current
            arrowHeightConstraint.constant = 88;
            arrowWithConstraint.constant = 77;
            if(self.fromLessonSummary){
                arrowHeightConstraint.constant = 66;
                //arrowWithConstraint.constant = 50;
            }
            self.lessonStatusImageView.image = [UIImage imageNamed:@"lesson-current"];
            self.lessonStatusImageView.contentMode = UIViewContentModeScaleAspectFit;
            self.cellBg.image = [UIImage imageNamed:@"hpc-bg"];
            self.lessonThumbnailImageView.hidden = YES;
            self.constraintStatusVsThumb.constant = self.lessonThumbnailImageView.width/2;
            self.labelLessonInfo.text = @"Now playing:\nLesson";
            [self.labelLessonInfo setTextColor:color (138, 128, 72,1)];
            [self.labelLessonNumber setTextColor:color (138, 128, 72,1)];
            break;
        case 3:
            //locked
            arrowHeightConstraint.constant = 45;
            arrowWithConstraint.constant = 45;
            
            self.lessonStatusImageView.image = [UIImage imageNamed:@"lesson-locked"];
            self.lessonStatusImageView.contentMode = UIViewContentModeScaleAspectFit;
            self.cellBg.image = nil;
            self.lessonThumbnailImageView.hidden = NO;
            //self.constraintStatusVsThumb.constant = 0;
            self.constraintStatusVsThumb.constant = self.lessonThumbnailImageView.width/2;
            self.labelLessonInfo.text = @"Lesson";
            
            [self.labelLessonInfo setTextColor:color (138, 128, 72,1)];
            [self.labelLessonNumber setTextColor:color (138, 128, 72,1)];
            [self makeSaturation];
            
            break;
        default:
            break;
    }
    
    [self.labelLessonInfo sizeToFit];
    NSInteger lessonNumber = [[lessonData objectForKey:@"lessonNumber"] integerValue];
    self.labelLessonNumber.text = [NSString stringWithFormat:@"%ld", lessonNumber];
    
    NSNumber *spacing = [NSNumber numberWithFloat:-4];
    NSMutableAttributedString *s;
    s = [[NSMutableAttributedString alloc] initWithString:self.labelLessonNumber.text];
    [s addAttribute:NSKernAttributeName
              value:spacing
              range:NSMakeRange(0, s.length)];
    self.labelLessonNumber.attributedText = s;
    
    if (IS_IOS8) {
        self.labelLessonNumber.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:46.0];
    }
    if(self.fromLessonSummary){
        thumbnailImageWithConstraint.constant = 100;
        numberBottomConstraint.constant = 0;
        self.labelLessonNumber.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:45.0];
    }
}

- (void)makeSaturation {
    
    GPUImagePicture *stillImageSource = [[GPUImagePicture alloc] initWithImage:self.lessonThumbnailImageView.image];
    GPUImageSaturationFilter *saturationFilter = [GPUImageSaturationFilter new];
    saturationFilter.saturation = 0.20;
    [stillImageSource addTarget:saturationFilter];
    [saturationFilter useNextFrameForImageCapture];
    [stillImageSource processImage];
    
    UIImage *currentFilteredVideoFrame =  [saturationFilter imageFromCurrentFramebufferWithOrientation:UIImageOrientationUp];// [saturationFilter imageFromCurrentFramebuffer];
    self.lessonThumbnailImageView.image = currentFilteredVideoFrame;
    
    
}


@end
