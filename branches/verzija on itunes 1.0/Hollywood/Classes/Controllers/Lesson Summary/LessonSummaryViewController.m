//
//  LessonSummaryViewController.m
//  Hollywood
//
//  Created by Kiril Kiroski on 5/22/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LessonSummaryViewController.h"
#import "AchievementsTableViewCell.h"
#import "HomePageCollectionViewCell.h"
#import "LessonObject+CoreDataClass.h"

@interface LessonSummaryViewController (){
    
    __weak IBOutlet UIScrollView *leftScrollView;
    IBOutlet UIView *leftOutsideView;
    
    __weak IBOutlet UIView *topBarView;
    __weak IBOutlet UILabel *userScoreLabel;
    __weak IBOutlet UILabel *vipScoreLabel;
    __weak IBOutlet UILabel *vipStaticTextLabel;
    __weak IBOutlet UILabel *performanceTextLabel;
    __weak IBOutlet UIImageView *rankImageView;
    
    __strong ProgressUserObject *currentProgressUserObject;
    __strong ProgressLevelObject *currentProgressLevelObject;
    __strong ProgressLessonObject *currentProgressLessonObject;
    
    __weak IBOutlet UIImageView *nextLessonImageView;
    __weak IBOutlet UILabel *currentLessonNumberLabel;
    __weak IBOutlet UILabel *currentLessonPointsLabel;
    __weak IBOutlet UILabel *currentLessonProcentLabel;
    
    __weak IBOutlet UIImageView *star1ImageView;
    __weak IBOutlet UIImageView *star2ImageView;
    __weak IBOutlet UIImageView *star3ImageView;
    
    __weak IBOutlet UIView *containerNextLessonImageView;
    __weak IBOutlet UIImageView *bigSummaryScreenImageView;
    
    __weak IBOutlet UITableView *achievementsTableView;
    NSMutableArray *achievementsArray;
    
    __weak IBOutlet UICollectionView *mainCollectionView;
    NSMutableArray *lessonsInfoArray;
    NSInteger currentLesson;
    NSInteger maximumLessonNumber;
    
    int numberOfStar;
}

@end

@implementation LessonSummaryViewController

- (void)configureUI {
    
    [super configureUI];
    
    bigSummaryScreenImageView.layer.cornerRadius = 4.0;
    bigSummaryScreenImageView.layer.borderWidth = 1.0;
    bigSummaryScreenImageView.layer.borderColor = [UIColor colorWithRed:111.0/255.0 green:102.0/255.0 blue:55.0/255.0 alpha:1.0].CGColor;
    bigSummaryScreenImageView.layer.masksToBounds = YES;
    
    
    self.navigationController.navigationBarHidden = YES;
//    topBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar_bg_up"]];
    
    vipScoreLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:15.0];
    vipStaticTextLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:18.0];
    userScoreLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:30.0];
    
    //[currentLessonNumberLabel fontSizeToFit];
    currentLessonProcentLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:18.0];
    currentLessonPointsLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:25.0];
    
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    
    [leftScrollView addSubview:leftOutsideView];
    leftScrollView.contentSize = leftOutsideView.frame.size;
    [self updateLabels];
    
    [achievementsTableView registerNib:[UINib nibWithNibName:@"AchievementsTableViewCell" bundle:nil] forCellReuseIdentifier:@"CellAchievements"];
    
    [mainCollectionView registerNib:[UINib nibWithNibName:@"HomePageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
    vipStaticTextLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"vip_bar_label"];
    rankImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"Awards_%02d", [[USER_MANAGER takeMaxRank] intValue]+1]];
    //[self loadData];
    
    //NSArray *allLessonObject = [DATA_MANAGER takeAllLessonObject];
    //NSMutableArray *tempArrayLessons = [allLessonObject mutableCopy];
    
    //Points, vip passes
    
    //    achievementsArray = [[ACHIEVEMENT_MANAGER getArrayAchievementManager] mutableCopy];
    //    [achievementsTableView reloadData];
    
    //userScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
    //userVipPassesLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];
    
    //TO DO: Get lesson info
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
    
    currentLesson = currentProgressLevelObject.currentUnit;
    long tempLesson = currentLesson - 1;
    currentProgressLessonObject = [DATA_MANAGER getLessonObjectForIndex:(int)tempLesson];
    
    
    currentLessonPointsLabel.text = [NSString stringWithFormat:@"%ld %@",(long)currentProgressLessonObject.scoreForLesson,[LABEL_MANAGER setLocalizeLabelText:@"points_shortcut"]];
    currentLessonNumberLabel.text = [NSString stringWithFormat:@"%ld",(long)currentLesson];
    //[currentLessonNumberLabel fontSizeToFit];
    if(DEVICE_BIGER_SIDE > 568){
        currentLessonNumberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:100.0];
    }else  if(DEVICE_BIGER_SIDE == 568){
        currentLessonNumberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:71.0];
    }else if(DEVICE_BIGER_SIDE < 568){
        currentLessonNumberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:71.0];
    }
    float procent = (self.vehiclePassCorrect / self.vehicleNumber)*100;
    if(procent > 100){
        procent = 100;
    }
    if(!procent){
        procent = 0;
    }
    if (procent != procent) {
        procent = 0;//for NaN
    }
    currentLessonProcentLabel.text = [NSString stringWithFormat:@"%.0f %%",procent];
    numberOfStar = 0;
    
    if(procent > kParam006){
        numberOfStar = 3;
        star1ImageView.hidden = NO;
        star2ImageView.hidden = NO;
        star3ImageView.hidden = NO;
    }else if(procent > kParam005){
        numberOfStar = 2;
        star2ImageView.hidden = NO;
        star3ImageView.hidden = NO;
    }else if(procent > kParam004){
        numberOfStar = 1;
        star2ImageView.hidden = NO;
    }
    /////
}
- (void)updateLabels{
    vipScoreLabel.text = [NSString stringWithFormat:@"%d",currentProgressUserObject.vipPasses];
    userScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
}
- (void)updateData{
    
    NSDictionary *dictHeaders = @{
                                  kProgressLessonOrder : [NSString stringWithFormat:@"%d", [self.currentLessonObject.lesson_order intValue]] ,
                                  kProgressRankOrder : [NSString stringWithFormat:@"%d", [self.currentLessonObject.rankParentNumber intValue]],
                                  kProgressLevelOrder : [NSString stringWithFormat:@"%d", currentProgressUserObject.currentLevel],
                                  kProgressLessonCompleted :@"true",
                                  kProgressStarNumber : [NSString stringWithFormat:@"%d", numberOfStar],
                                  kProgressScore : [NSString stringWithFormat:@"%d", currentProgressUserObject.score]
                                  
                                  };
    [self addBaseActivityIndicator];
    [self chekInternet];
    
    __weak LessonSummaryViewController *weakSelf = self;
    [REQUEST_MANAGER getDataFor:kRequestSaveUserProgressionAPICall headers:dictHeaders withSuccessCallBack:^(NSDictionary *responseObject) {
        NSDictionary *responseData = [responseObject objectForKey:kApiObjectResponse];
        if(responseData){
            [USER_MANAGER setUserData:responseData];
            NSArray *arrayAchievementsList = [responseData objectForKey:@"achievmentList"];
            [weakSelf findAchievementObjects:arrayAchievementsList];
            [weakSelf updateLabels];
            
        }
        [weakSelf removeActivityIndicator];
        //[self.navigationController popViewControllerAnimated:YES];
    } andWithErrorCallBack:^(NSString *errorMessage) {
        
        [weakSelf removeActivityIndicator];
        
    }];
}

- (void)findAchievementObjects:(NSArray *)arrayAchievementList {
    
    if (!achievementsArray) {
        achievementsArray = [NSMutableArray new];
    }
    
    
    NSArray *arraySeenAchievements = [ACHIEVEMENT_MANAGER getAchievementsListedInSummary];
    
    for (NSNumber *achievementId in arrayAchievementList) {
        
        //check is in the list
        if (![arraySeenAchievements containsObject:achievementId]) {
            
            AchievementsObject *achievementObject = [ACHIEVEMENT_MANAGER getAchievementById:achievementId];
            if(achievementObject){
            [achievementsArray addObject:achievementObject];
            
            [ACHIEVEMENT_MANAGER storeAchievementID:achievementId];
            }
            else{
                NSLog(@"MISS AchievementsObject");
            }
        }
        else {
            
        }
        
    }
    
    [achievementsTableView reloadData];
    
}

- (void)loadData {
    
    [super loadData];
    
    NSArray *allLessonObject = [DATA_MANAGER takeAllLessonObject];
    NSMutableArray *tempArrayLessons = [allLessonObject mutableCopy];
    
    //Points, vip passes
    
//    achievementsArray = [[ACHIEVEMENT_MANAGER getArrayAchievementManager] mutableCopy];
//    [achievementsTableView reloadData];
    
    //userScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
    //userVipPassesLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];
    
    //TO DO: Get lesson info
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
    
    currentLesson = currentProgressLevelObject.currentUnit;
    long tempLesson = currentLesson - 1;
    currentProgressLessonObject = [DATA_MANAGER getLessonObjectForIndex:(int)tempLesson];
    
    
    currentLessonPointsLabel.text = [NSString stringWithFormat:@"%ld %@",(long)currentProgressLessonObject.scoreForLesson,[LABEL_MANAGER setLocalizeLabelText:@"points_shortcut"]];
    currentLessonNumberLabel.text = [NSString stringWithFormat:@"%ld",(long)currentLesson];
    //[currentLessonNumberLabel fontSizeToFit];
    if(DEVICE_BIGER_SIDE > 568){
        currentLessonNumberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:100.0];
    }else  if(DEVICE_BIGER_SIDE == 568){
        currentLessonNumberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:71.0];
    }else if(DEVICE_BIGER_SIDE < 568){
        currentLessonNumberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:71.0];
    }
    float procent = (self.vehiclePassCorrect / self.vehicleNumber)*100;
    if(procent > 100){
        procent = 100;
    }
    if(!procent){
        procent = 0;
    }
    if (procent != procent) {
        procent = 0;//for NaN
    }
    currentLessonProcentLabel.text = [NSString stringWithFormat:@"%.0f %%",procent];
    numberOfStar = 0;
    
    if(procent > kParam006){
        numberOfStar = 3;
        star1ImageView.hidden = NO;
        star2ImageView.hidden = NO;
        star3ImageView.hidden = NO;
    }else if(procent > kParam005){
        numberOfStar = 2;
        star2ImageView.hidden = NO;
        star3ImageView.hidden = NO;
    }else if(procent > kParam004){
        numberOfStar = 1;
        star2ImageView.hidden = NO;
    }
    
    maximumLessonNumber = currentProgressLevelObject.maxUnit;
    
    currentLessonNumberLabel.text = [NSString stringWithFormat:@"%ld",(long)currentLesson];
    
    lessonsInfoArray = [NSMutableArray new];
    
    
    for (NSInteger i=0; i<=maximumLessonNumber; i++) {
        if([tempArrayLessons count] <= i){
            break;
        }
        LessonObject *lessonObject = [tempArrayLessons objectAtIndex:i];
       
        if (i+1 == maximumLessonNumber+1) {
            [lessonsInfoArray addObject:@{
                                               @"lessonStatus":@(2),
                                               @"lessonNumber":@(i+1),
                                               @"lessonImage": lessonObject.lesson_home_page_image,
                                               @"leasonTeaserImage": lessonObject.summary_Image
                                               }];
            
        }
        else {
            
            [lessonsInfoArray addObject:@{
                                               @"lessonStatus":@(1),
                                               @"lessonNumber":@(i+1),
                                               @"lessonImage":lessonObject.lesson_home_page_image,
                                               @"leasonTeaserImage": lessonObject.summary_Image
                                               }];
        }
    }
    
    for (NSInteger i=maximumLessonNumber+1; i<[tempArrayLessons count]; i++) {
        
        LessonObject *lessonObject = [tempArrayLessons objectAtIndex:i];
        
        [lessonsInfoArray addObject:@{
                                           @"lessonStatus":@(3),
                                           @"lessonNumber":@(i+1),
                                           @"lessonImage": lessonObject.lesson_home_page_image,
                                           @"leasonTeaserImage": lessonObject.summary_Image
                                           }];
        
    }
    
    [mainCollectionView reloadData];
    if([tempArrayLessons count] > maximumLessonNumber){
        [mainCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:maximumLessonNumber inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    }else{
        [mainCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:maximumLessonNumber-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    }
   
    NSString *teaserImageUrl = [[lessonsInfoArray objectAtIndex:maximumLessonNumber-1] objectForKey:@"leasonTeaserImage"];
    bigSummaryScreenImageView.image = [UIImage imageNamed:teaserImageUrl];
    [bigSummaryScreenImageView setImageWithURL:[NSURL URLWithString:teaserImageUrl] placeholderImage:[UIImage imageNamed:@""]];
    [self updateData];
    
    if(currentProgressLevelObject.currentUnit == [lessonsInfoArray count]){
        [self showAlert:@"Parabéns! Você completou o terceiro Rank de inglês! Volte para continuar praticando mais exercícios e quizzes divertidos!"];
    }
    
    //[self removeAllCache];
}
-(void)removeAllCache{
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [self showMemoryInfo];
    
}

-(void)showMemoryInfo{
    //(in bytes)
    NSString *infoMemory = [NSString stringWithFormat:@"DiskCache: %@ of %@ \nMemoryCache: %@ of %@", @([[NSURLCache sharedURLCache] currentDiskUsage]), @([[NSURLCache sharedURLCache] diskCapacity]),@([[NSURLCache sharedURLCache] currentMemoryUsage]), @([[NSURLCache sharedURLCache] memoryCapacity])];
    NSLog(@"infoMemory : %@",infoMemory);
}

- (void)showAlert:(NSString*)stringForSubtitle{
    FCAlertView *alert = [[FCAlertView alloc] init];
    alert.alertBackgroundColor = [UIColor colorWithRed:112.0f/255.0f green:102.0f/255.0f blue:55.0f/255.0f alpha:1.0];
    
    [alert showAlertInView:self
                 withTitle:@""
              withSubtitle:stringForSubtitle
           withCustomImage:nil
       withDoneButtonTitle:nil
                andButtons:nil];
    
    alert.backgroundColor = [UIColor clearColor];
    alert.colorScheme = [UIColor blackColor];
    alert.doneButtonTitleColor = [UIColor whiteColor];
    alert.hideSeparatorLineView = YES;
    alert.blurBackground = NO;
}
#pragma mark Interface functions
- (IBAction)goToHomePage:(id)sender {
    [AUDIO_MANAGER stopPlaying];
    [APP_DELEGATE buildHomePageStack];
}

- (IBAction)openUserZone:(id)sender {
    
    [APP_DELEGATE buildUserZoneStack];
}

- (IBAction)goToVipCategories:(id)sender {
    //[APP_DELEGATE buildVipZoneStack];
    //return;
    ///
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kVipScreen action:@"Click_on_VIP" additionalData:nil];
    
    [self addBaseActivityIndicator];
    id vipCourseObject = [DATA_MANAGER getVipCourseObject];
    if(vipCourseObject){
        //donwload resoureces
        NSArray *dataArray = [DATA_MANAGER getAllVipCategory];
        
        [CACHE_MANAGER setDelegate:self];
        [CACHE_MANAGER startDownloadingAndCachingResources:dataArray];
    }else{
        [self addBaseActivityIndicator];
        [self chekInternet];
        [REQUEST_MANAGER getDataFor:kRequestGetVipDataAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
            [self removeActivityIndicator];
            NSDictionary *responseDictionary = [responseObject objectForKey:kApiObjectResponse];
            [DATA_MANAGER makeVipCategoryCourseData:responseDictionary];
            [DATA_MANAGER saveData];
            
            [self goToVipCategories:0];
        } andWithErrorCallBack:^(NSString *errorMessage) {
            [self removeActivityIndicator];
        }];
        //makeVipCategoryCourseData
    }
}

#pragma mark - Cache Manager Delegate
- (void)cacheManagerFinishedDownloading:(id)sender {
    
    NSLog(@"========= Images were downloaded =========");
    
    [APP_DELEGATE buildVipZoneStack];
    
}
#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [achievementsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AchievementsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellAchievements"];
    AchievementsObject *achievementObject = [achievementsArray objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setData:achievementObject];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 84.0;
}

#pragma mark UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [lessonsInfoArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    HomePageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    cell.fromLessonSummary = YES;
    //[cell setDataArray:dataArray[indexPath.row]];
    
    id lessonData = [lessonsInfoArray objectAtIndex:indexPath.row];
    [cell setLessonData:lessonData];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(160, 80);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    id lessonData = [lessonsInfoArray objectAtIndex:indexPath.row];
    NSInteger status = [[lessonData objectForKey:@"lessonStatus"] integerValue];
    
    if (status == 3) {
        
        [ALERT_MANAGER showCustomAlertWithTitle:nil andMessage:[LABEL_MANAGER setLocalizeLabelText:@"lesson_locked_message"]];
        return;
    }
    else {
        
        currentProgressLevelObject.currentUnit = (int)[[lessonData objectForKey:@"lessonNumber"] integerValue]-1;
        LessonObject *currentLessonObject = [DATA_MANAGER takeLessonObjectObject:currentProgressLevelObject.currentUnit];
        NSNumber *lessonId = currentLessonObject.lesson_id;
        
        if (status == 2) {
            
            if ([USER_MANAGER isUserDisabled]) {
                
                if (![DATA_MANAGER isFreeLessonWithId:lessonId]) {
                    
                    [self showAlert:[LABEL_MANAGER setLocalizeLabelText:@"error_message_2"]];
                    return;
                }
            }
            
            NSDictionary *dictHeaders = @{
                                          kLessonId : [NSString stringWithFormat:@"%d", [currentLessonObject.lesson_id intValue]]
                                          };
            NSArray *vehiclesDataArray = [DATA_MANAGER takeAllVehiclesObjectInLesson:currentProgressLevelObject.currentUnit];
            if(([USER_MANAGER isActive] || ([USER_MANAGER isUserDisabled] && [DATA_MANAGER isFreeLessonWithId:lessonId])) && ([vehiclesDataArray count] == 0) ){
                [self addBaseActivityIndicator];
                [self chekInternet];
                [REQUEST_MANAGER getDataFor:kRequestGetLessonDataAPICall headers:dictHeaders withSuccessCallBack:^(NSDictionary *responseObject) {
                    [self removeActivityIndicator];
                    long statusNumber = [[responseObject objectForKey:kApiStatus] longValue];
                    if(statusNumber == -10){
                        [self showAlert:[LABEL_MANAGER setLocalizeLabelText:@"error_message_2"]];
                        
                    }else{
                        
                        NSDictionary *responseData = [responseObject objectForKey:kApiObjectResponse];
                        if(responseData){
                            [currentLessonObject setVehiclesData:responseData];
                        }
                        [APP_DELEGATE buildLessonStack];
                        [self removeActivityIndicator];
                    }
                } andWithErrorCallBack:^(NSString *errorMessage) {
                    if([errorMessage isEqualToString:@"Request failed: payment required (402)"]){
                        [self showAlert:[LABEL_MANAGER setLocalizeLabelText:@"error_message_2"]];
                    }
                    [self removeActivityIndicator];
                    
                }];
            }
            else {
                
                [APP_DELEGATE buildLessonStack];
            }
        }
        else {
            
            NSDictionary *dictHeaders = @{
                                          kLessonId : [NSString stringWithFormat:@"%d", [currentLessonObject.lesson_id intValue]]
                                          };
            NSArray *vehiclesDataArray = [DATA_MANAGER takeAllVehiclesObjectInLesson:currentProgressLevelObject.currentUnit];
            if( ([USER_MANAGER isActive] || [USER_MANAGER isUserDisabled]) && ([vehiclesDataArray count] == 0) ){
                [self addBaseActivityIndicator];
                [self chekInternet];
                [REQUEST_MANAGER getDataFor:kRequestGetLessonDataAPICall headers:dictHeaders withSuccessCallBack:^(NSDictionary *responseObject) {
                    [self removeActivityIndicator];
                    long statusNumber = [[responseObject objectForKey:kApiStatus] longValue];
                    if(statusNumber == -10){
                        [self showAlert:[LABEL_MANAGER setLocalizeLabelText:@"error_message_2"]];
                        
                    }else{
                        
                        NSDictionary *responseData = [responseObject objectForKey:kApiObjectResponse];
                        if(responseData){
                            [currentLessonObject setVehiclesData:responseData];
                        }
                        [APP_DELEGATE buildLessonStack];
                        [self removeActivityIndicator];
                    }
                } andWithErrorCallBack:^(NSString *errorMessage) {
                    if([errorMessage isEqualToString:@"Request failed: payment required (402)"]){
                        [self showAlert:[LABEL_MANAGER setLocalizeLabelText:@"error_message_2"]];
                    }
                    [self removeActivityIndicator];
                    
                }];
            }else{
                
                [APP_DELEGATE buildLessonStack];
            }
        }
    }
}

@end
