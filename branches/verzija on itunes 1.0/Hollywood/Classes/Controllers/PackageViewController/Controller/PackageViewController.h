//
//  PackageViewController.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/14/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LMBaseViewController.h"
#import "PackageCollectionViewCell.h"
#import <StoreKit/StoreKit.h>

@interface PackageViewController : LMBaseViewController
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *titleTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *vipStaticTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *userVipScoreLabel;

@property (nonatomic) BOOL isBackButtonHidden;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end
