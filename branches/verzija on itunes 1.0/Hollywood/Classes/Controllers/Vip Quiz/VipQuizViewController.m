//
//  VipQuizViewController.m
//  Hollywood
//
//  Created by Kiril Kiroski on 10/12/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "VipQuizViewController.h"
#import "Constants.h"
#import "ClickToLearnVehicle.h"
#import "QuizVehicleMultiText.h"
#import "QuizVehicle.h"
#import "LMAnalyticsManager.h"
#import "VipCategoryViewController.h"
#import "LMQuizProgressBar.h"
#import "QuizSummaryViewController.h"

@interface VipQuizViewController ()<BaseViewVehicleDelegate, UIGestureRecognizerDelegate, QuizVehicleDelegate> {
    
    __weak IBOutlet UIView *topBarView;
    __weak IBOutlet UIView *questionBarView;
    __weak IBOutlet UILabel *questionBarLabel;
    __weak IBOutlet UILabel *questionNumberLabel;
    __weak IBOutlet UILabel *titleLabel;

    __weak IBOutlet UILabel *vipScoreLabel;
    __weak IBOutlet UILabel *vipStaticTextLabel;

    __weak IBOutlet UIView *bottomProgressView;
    __weak IBOutlet UIScrollView *vehicleContainerScrollView;
    
    __strong ProgressUserObject *currentProgressUserObject;
    
    VipQuizObject *currentVipQuizObject;
    NSInteger tempCounter;
    NSArray *dataArray;
    
    UISwipeGestureRecognizer *leftSwipe;
}

@property (nonatomic, strong) LMQuizProgressBar *quizProgressBar;
@property (nonatomic, strong) NSMutableArray *vehiclesArray;
@property (nonatomic, assign) NSInteger currentVehicleIndex;


@end

@implementation VipQuizViewController
@synthesize vehiclesArray;
@synthesize currentVehicleIndex;

- (void)configureUI{
    [super configureUI];
    self.navigationController.navigationBarHidden = YES;
    topBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar_bg_up"]];
    questionBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar_bg_down"]];
    
    titleLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:30.0];
    vipScoreLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:15.0];
    vipStaticTextLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:18.0];
    
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kVipScreen action:@"Quiz_Vehicle_Presented" additionalData:nil];

    [self initialScroll];
    
//    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightToLeftSwipeAction:)];
//    leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
//    [vehicleContainerScrollView addGestureRecognizer:leftSwipe];

    [self addSwipeGesture];
    
    [self updateLabels];
}

- (void)addSwipeGesture {
    
    leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightToLeftSwipeAction:)];
    [leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
//    leftSwipe.delegate = self;
    [vehicleContainerScrollView addGestureRecognizer:leftSwipe];
    
}

- (void)removeSwipeGesture {
    
    [vehicleContainerScrollView removeGestureRecognizer:leftSwipe];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self performSelector:@selector(loadResources) withObject:nil afterDelay:0.25];
}

- (void)loadResources {
    
    tempCounter = 0;
    
    currentVehicleIndex = 0;
    
    vehiclesArray = [NSMutableArray new];
    
    NSSet *set2 = currentVipQuizObject.instanceList;
    NSArray *data2Array = [set2 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"quizInstanceId"
                                                  ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [data2Array sortedArrayUsingDescriptors:sortDescriptors2];
    
    vehiclesArray = sorted2Array;
    //dummy data
    //    for (int i=0; i<5; i++) {
    //
    //        [vehiclesArray addObject:@{
    //                                   @"answers": @[
    //                                           @{@"text": @"Jennifer Lopez", @"isCorrect": @(YES), @"image": @"jl-2"},
    //                                           @{@"text": @"Billy Bob Thornton", @"isCorrect": @(NO), @"image": @"bbt-2"},
    //                                           @{@"text": @"Elizabeth Taylor", @"isCorrect": @(NO), @"image": @"et-2"},
    //                                           @{@"text": @"Kim Kardashian", @"isCorrect": @(NO), @"image": @"kk-2"}],
    //                                   @"isOneImageLayout": @(NO),
    //                                   @"bigImage": @"family",
    //                                   @"questionText": @"Can you guess which of the following stars got married the most?"
    //
    //                                   }];
    //
    //        [vehiclesArray addObject:@{
    //                                   @"answers": @[
    //                                           @{@"text": @"Bruce Willis", @"isCorrect": @(NO), @"image": @"bruceWillis"},
    //                                           @{@"text": @"George Clooney", @"isCorrect": @(YES), @"image": @"georgeClooney"},
    //                                           @{@"text": @"Tom Cruise", @"isCorrect": @(NO), @"image": @"tcruise"},
    //                                           @{@"text": @"Tom Hanks", @"isCorrect": @(NO), @"image": @"th"}],
    //                                   @"isOneImageLayout": @(NO),
    //                                   @"bigImage": @"family",
    //                                   @"questionText": @"Cuál de las siguientes STARS actuado de Batman?"
    //
    //                                   }];
    //    }
    
    
    self.quizProgressBar = [[LMQuizProgressBar alloc] initWithFrame:CGRectMake(42.0, DEVICE_SMALL_SIDE-25.0, DEVICE_BIGER_SIDE-84.0, 25.0) withNumberOfDots:10];
    [self.view addSubview:self.quizProgressBar];
    
    questionNumberLabel.text = [NSString stringWithFormat:@"%d", 1];
    
    [self createVehicleWithIndex:currentVehicleIndex atPosition:0];
    [self createVehicleWithIndex:currentVehicleIndex atPosition:1];
    
    QuizInstance *fisrtVehicleData = [vehiclesArray objectAtIndex:0];
    //questionBarLabel.lineBreakMode = NSLineBreakByWordWrapping;
    //questionBarLabel.numberOfLines = 2;

    questionBarLabel.text = [fisrtVehicleData instanceQuestion];
    //[questionBarLabel fontSizeToFit];
}

- (void)loadData {
    [super loadData];
    
    
}

- (void)initialScroll {
    
//    [vehicleContainerScrollView setDelegate:self];
    [vehicleContainerScrollView setBouncesZoom:YES];
    [vehicleContainerScrollView setContentMode:UIViewContentModeCenter];
    vehicleContainerScrollView.userInteractionEnabled = YES;
    
    vehicleContainerScrollView.contentSize = CGSizeMake(DEVICE_BIGER_SIDE*2, vehicleContainerScrollView.contentSize.height);
    [vehicleContainerScrollView setScrollEnabled:NO];
    [vehicleContainerScrollView setShowsHorizontalScrollIndicator:FALSE];
    [vehicleContainerScrollView setShowsVerticalScrollIndicator:FALSE];
    
    vehicleContainerScrollView.scrollsToTop = NO;
    vehicleContainerScrollView.pagingEnabled = YES;
    
}

- (void)setVipQuizObject:(VipQuizObject*)tempVipQuizObject{
    currentVipQuizObject = tempVipQuizObject;
}
#pragma mark - Create vehicle

- (void)createVehicleWithIndex:(NSInteger)index atPosition:(NSInteger)position {
    
    if (index >= [vehiclesArray count]) {
        
        if (index == [vehiclesArray count]) {
            
            //create summary screen
        }
        
        return;
    }
    
    QuizInstance *vehicleData = [vehiclesArray objectAtIndex:index];
    
    //0 - visible
    //1 - in memory but invisible for user
    
    float vehicleWidth = DEVICE_BIGER_SIDE;
    float vehicleHeight = vehicleContainerScrollView.frame.size.height;
    
    NSInteger instanceType = [vehicleData.instanceType integerValue];
//    NSInteger instanceType = 2;

    if (instanceType == 2) {
        
        QuizVehicle *clickVehicle = [[QuizVehicle alloc] initWithFrame:CGRectMake(position*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                                       quizVehicleData:vehicleData
                                                              position:position];
        clickVehicle.vehicleDelegate = self;
        clickVehicle.quizVehicleDelegate = self;
        if (position == 0) {
            [clickVehicle activateVehicle];

            [ANALYTICS_MANAGER sendAnalyticsForEvent:kVipQuizScreen action:@"Quiz_Vehicle_Presented" labelName:@"Vehicle_ID" value:vehicleData.quizInstanceId];
        }
        
        [vehicleContainerScrollView addSubview:clickVehicle];
    }
    else {
        
        QuizVehicleMultiText *multiChoiceVehicle = [[QuizVehicleMultiText alloc]
                                                    initWithFrame:CGRectMake(position*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                                    data:vehicleData
                                                    position:position];
        
        multiChoiceVehicle.vehicleDelegate = self;

        if (position == 0) {
            [multiChoiceVehicle activateVehicle];
            [ANALYTICS_MANAGER sendAnalyticsForEvent:kVipQuizScreen action:@"Quiz_Vehicle_Presented" labelName:@"Vehicle_ID" value:vehicleData.quizInstanceId];
        }
        
        [vehicleContainerScrollView addSubview:multiChoiceVehicle];
    }
    
    currentVehicleIndex++;
}

- (void)answerClickedInQuiz {
    
    [self removeSwipeGesture];
}

#pragma mark - rightToLeftSwipeAction

- (void)rightToLeftSwipeAction:(id)sender {
    
    if ([sender isKindOfClass:[UISwipeGestureRecognizer class]]) {
        
        [self.quizProgressBar changeStatus:2 forDotWithIndex:self.quizProgressBar.currentDotIndex+1];

        [ANALYTICS_MANAGER sendSwipeBetweenInstance:kQuizSwipeInstance];
        [ANALYTICS_MANAGER sendAnalyticsForEvent:kVipScreen action:@"Quiz_Vehicle_Answered" additionalData:@"Skip"];
    }
    
    float vehicleWidth = DEVICE_BIGER_SIDE;
    float vehicleHeight = vehicleContainerScrollView.frame.size.height;

    for (QuizVehicle *vehicle in vehicleContainerScrollView.subviews) {
        
        if ([vehicle isKindOfClass:[QuizVehicle class]] || [vehicle isKindOfClass:[QuizVehicleMultiText class]]) {
            
            [UIView animateWithDuration:0.5 animations:^{
                
                [vehicle setFrame:CGRectMake((vehicle.positionInQuiz-1)*vehicleWidth, 0, vehicleWidth, vehicleHeight)];
                
            } completion:^(BOOL finished) {
                
                if (vehicle.positionInQuiz == 0) {
                    
                    [vehicle removeFromSuperview];
                }
                else if (vehicle.positionInQuiz == 1) {
                    
                    vehicle.positionInQuiz = 0;
                    [vehicle activateVehicle];
                }
            }];
        }
        
    }
    
    questionNumberLabel.adjustsFontSizeToFitWidth = YES;
    questionNumberLabel.text = [NSString stringWithFormat:@"%ld", currentVehicleIndex];
    [questionNumberLabel sizeToFit];

    QuizInstance *nextVehicleData = [vehiclesArray objectAtIndex:currentVehicleIndex-1];
    questionBarLabel.lineBreakMode = NSLineBreakByWordWrapping;
    questionBarLabel.numberOfLines = 2;

    questionBarLabel.text = [nextVehicleData instanceQuestion];
    
    [questionBarLabel fontSizeToFit];
    
    
    [self createVehicleWithIndex:currentVehicleIndex atPosition:1];

    [ANALYTICS_MANAGER sendQuizVehiclePresented:currentVehicleIndex withActionName:kQuizVehiclePresented];
    
    NSLog(@"self.quizProgressBar.currentDotIndex %d",self.quizProgressBar.currentDotIndex);
    
    
    if(self.quizProgressBar.currentDotIndex >= 10){
        
        QuizSummaryViewController *viewController = [QuizSummaryViewController new];
        viewController.corectAnswersNumber = self.quizProgressBar.correctAnswers;
        viewController.totalAnswers = self.quizProgressBar.numberOfDots;
        viewController.currentVipCategoryObject = _currentVipCategoryObject;
        [viewController setVipQuizObject:currentVipQuizObject];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

#pragma mark - Vehicle delegate methods

- (void)vehicleWasFinished:(id)sender {
    
    NSLog(@"Vehicle finished");
    [self rightToLeftSwipeAction:nil];
    [self addSwipeGesture];
    
    QuizVehicle *vehicle = (QuizVehicle *)sender;
    if ([vehicle isKindOfClass:[QuizVehicle class]] || [vehicle isKindOfClass:[QuizVehicleMultiText class]]) {
        
        if (vehicle.isAnsweredCorrect) {
            [self.quizProgressBar changeStatus:1 forDotWithIndex:self.quizProgressBar.currentDotIndex+1];
            [ANALYTICS_MANAGER sendAnalyticsForEvent:kVipScreen action:@"Quiz_Vehicle_Answered" additionalData:@"Correct"];

        }
        else {
            
            [self.quizProgressBar changeStatus:2 forDotWithIndex:self.quizProgressBar.currentDotIndex+1];
            [ANALYTICS_MANAGER sendAnalyticsForEvent:kVipScreen action:@"Quiz_Vehicle_Answered" additionalData:@"Incorrect"];
        }

    }
    NSLog(@"self.quizProgressBar.currentDotIndex %d",self.quizProgressBar.currentDotIndex);
    if(self.quizProgressBar.currentDotIndex >= 10){
        QuizSummaryViewController *viewController = [QuizSummaryViewController new];
        viewController.corectAnswersNumber = self.quizProgressBar.correctAnswers;
        viewController.totalAnswers = self.quizProgressBar.numberOfDots;
        viewController.currentVipCategoryObject = _currentVipCategoryObject;
        [viewController setVipQuizObject:currentVipQuizObject];
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
}

- (void)addPoints:(int)points {
    
    
}

- (void)gotoNextPage {
    
    NSLog(@"TEST 1");
}

- (void)gotoPreviousPage {
    
}

- (void)setQuestionBarText:(NSString *)newText {
    
    NSLog(@"TEST 2");
}

#pragma mark Interface functions

- (IBAction)goToHomePage:(id)sender {
    [AUDIO_MANAGER stopPlaying];
    [APP_DELEGATE buildHomePageStack];
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kVipScreen action:@"Quiz_Exited" additionalData:@"Home_Page_Click"];
}

- (IBAction)openUserZone:(id)sender {
    
    [APP_DELEGATE buildUserZoneStack];
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kVipScreen action:@"Quiz_Exited" additionalData:@"User_Zone_Click"];
}

- (void)updateLabels{
    vipScoreLabel.text = [NSString stringWithFormat:@"%d",currentProgressUserObject.vipPasses];
}

- (IBAction)goToVipCategories:(id)sender {
    
    NSArray *controllersArray = self.navigationController.viewControllers;
    if (controllersArray && controllersArray.count>=2) {
        
        UIViewController *vipCategoryController = [controllersArray objectAtIndex:0];
        if ([vipCategoryController isKindOfClass:[VipCategoryViewController class]])
            [self.navigationController popToViewController:vipCategoryController animated:YES];
    }
    
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kVipScreen action:@"Quiz_Exited" additionalData:@"VIP_Click"];

}

@end
