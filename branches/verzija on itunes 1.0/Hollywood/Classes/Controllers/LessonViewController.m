    //
//  LessonViewController.m
//  Hollywood1
//
//  Created by Kiril Kiroski on 3/24/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "LessonViewController.h"
#import "VideoVehicle.h"
#import "SwipeVehicleView.h"
#import "ClickToLearnVehicle.h"
#import "MultipleChoiceText.h"
#import "SpellingVehicle.h"
#import "FillMissingWordsVehicle.h"
#import "PackageViewController.h"

#import "ProgressLessonObject.h"
#import "ProgressVehicleObject.h"
#import "LessonSummaryViewController.h"
#import "SelectCountryViewController.h"
#import "LessonObject+CoreDataClass.h"

@interface LessonViewController () <NSURLSessionDelegate, NSURLSessionDataDelegate> {
    
    __weak IBOutlet LessonProgressView *currentLessonProgress;
    __weak IBOutlet NSLayoutConstraint *instructionButton;
    __weak IBOutlet UIView *topBarView;
    __weak IBOutlet UIView *questionBarView;
    __weak IBOutlet UIScrollView *mainScrollView;
    __weak IBOutlet UIView *instructionView;
    __weak IBOutlet NSLayoutConstraint *instructionViewBottomConstrains;
    KSVideoPlayerView* player;
    KSVideoPlayerView* preloadPlayer;
    __weak IBOutlet NSLayoutConstraint *instructionViewTopConstrains;
    CGFloat initialConstantForBottomConstrains;
    NSMutableArray *vehiclesArray;
    UISwipeGestureRecognizer *leftSwipe,*rightSwipe;
    NSInteger lastCurrentPage;
    __weak IBOutlet UIButton *rightArrowButton;
    __weak IBOutlet UIButton *leftArrowButton;
    
    __weak IBOutlet VocabView *questionBarVocabView;
    __weak IBOutlet UILabel *questionBarLabel;
    __weak IBOutlet UILabel *scoreLabel;
    __weak IBOutlet UILabel *vipScoreLabel;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *vipStaticTextLabel;
    
    __weak IBOutlet UIImageView *imageViewUserProfile;
    __weak IBOutlet UIImageView *imageViewUserRank;
    
    int totalScore;
    //int vipScopre;
    float pointForVipScopre;
    
    __strong ProgressLessonObject *currentProgressLessonObject;
    __strong ProgressUserObject *currentProgressUserObject;
    __strong ProgressLevelObject *currentProgressLevelObject;
    
    __weak IBOutlet UIView *loadingView;
    __weak IBOutlet UIActivityIndicatorView *activityIndicatorLoading;
    __weak IBOutlet UILabel *loadingLabel;
    BOOL haveSummary;
    BOOL haveConversion;
    
    int vehiclePassCorrect;
    int vehicleWithPoints;
    LessonObject *currentLessonObject;
    
    double stepForProgressBar;
    NSInteger stepsBetweenPoints;
    double percentBetweenPoints;
    NSInteger nComponentsProgressBar;
    
    BOOL isSideButtonUsed;
    
    NSMutableArray *vehiclePassArray;
}

@property (nonatomic) CFAbsoluteTime startTime;
@property (nonatomic) CFAbsoluteTime stopTime;
@property (nonatomic) long long bytesReceived;
@property (nonatomic, copy) void (^speedTestCompletionHandler)(CGFloat megabytesPerSecond, NSError *error);

@end

@implementation LessonViewController

- (void)configureUI{
    
    [super configureUI];
    [self initialScroll];
    self.navigationController.navigationBarHidden = YES;
    instructionView.translatesAutoresizingMaskIntoConstraints = NO;
    [currentLessonProgress configureUI];
    
   /* imageViewUserProfile.layer.cornerRadius = 4.0;
    imageViewUserProfile.layer.borderWidth = 1.0;
    imageViewUserProfile.layer.borderColor = [UIColor blackColor].CGColor;
    imageViewUserProfile.layer.masksToBounds = YES;*/
    self.vehicleHeight = DEVICE_SMALL_SIDE;
    self.vehicleWidth = DEVICE_BIGER_SIDE;
    
    scoreLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:30.0];
    vipScoreLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:15.0];
    vipStaticTextLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:18.0];
    
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deactivateCurrentVehicle) name:@"deactivateCurrentVehicle" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pauseCurrentVehicle) name:@"deactivateCurrentVehicle" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backFromConversionScreen) name:@"backFromConversionScreen" object:nil];

    currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
    currentProgressLessonObject = [DATA_MANAGER getCurrentLessonObject];
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    [self updateLabels];
    vehiclePassCorrect = 0;
    vehicleWithPoints = 0;
    haveSummary = NO;
    haveConversion = NO;
    
    __weak LessonViewController *weakSelf = self;
    [self testDownloadSpeedWithTimout:1.0 completionHandler:^(CGFloat megabytesPerSecond, NSError *error) {
        //NSLog(@"DOWNLOAD SPEED Bandwidth Megabytes per second => %0.4f; error = %@", megabytesPerSecond, error);
        
        if (megabytesPerSecond > 1.5) {
            
            weakSelf.videoHihgQuality = YES;
        }
        else {
            
            weakSelf.videoHihgQuality = NO;
        }
        
    }];
    
    stepForProgressBar = 0.0;
    nComponentsProgressBar = 5;
    percentBetweenPoints = 1.0/nComponentsProgressBar;
    
}

- (void)loadData {
    
    if (isBackFromConversion) {
        
        isBackFromConversion = NO;
        return;
    }
    [super loadData];
    
    [self clearVideoDatainDefaults];
    
    [AUDIO_MANAGER  playInitialSound];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    vehiclePassArray = [NSMutableArray new];
    //Popup Logic
    
    NSLog(@"Defaults - %d, current unit - %d, maxUnit - %d, current vehicle - %d", [defaults boolForKey:@"openLessonSession"], currentProgressLevelObject.currentUnit,currentProgressLevelObject.maxUnit, currentProgressLessonObject.currentVehicle);
    
    
    if ([defaults boolForKey:@"openLessonSession"] && currentProgressLevelObject.currentUnit == currentProgressLevelObject.maxUnit
        && currentProgressLessonObject.currentVehicle != 0) {
        
        self.isPopupOpened = YES;
        [self showAlert:[LABEL_MANAGER setLocalizeLabelText:@"continue_playing_from_last_vehicle"]];
        /*UIAlertController * alert = [UIAlertController alertControllerWithTitle:nil
                                                                        message:[LABEL_MANAGER setLocalizeLabelText:@"continue_playing_from_last_vehicle"]
                                                                 preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *continueAction = [UIAlertAction
                                         actionWithTitle:[LABEL_MANAGER setLocalizeLabelText:@"continue_message"]
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             
                                             _continueSession = YES;
                                             self.isPopupOpened = NO;
                                             
                                             if (self.isLoadingFinished) {
                                                 [self activeteFirstVehicle];
                                             }
                                             
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                             
                                         }];
        
        [alert addAction:continueAction];
        //TODO no label into documentation
        UIAlertAction *newAction = [UIAlertAction
                                    actionWithTitle:[LABEL_MANAGER setLocalizeLabelText:@"new_session_message"]
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        self.isPopupOpened = NO;
                                        
                                        if (self.isLoadingFinished) {
                                            [self activeteFirstVehicle];
                                        }
                                        
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                        
                                    }];
        
        [alert addAction:newAction];
        
        [self presentViewController:alert animated:YES completion:nil];
         */
    }
    else if (currentProgressLevelObject.currentUnit == currentProgressLevelObject.maxUnit) {
        
        _continueSession = YES;
        
        if (self.isLoadingFinished) {
            [self activeteFirstVehicle];
        }
    }
    
    [defaults setBool:YES forKey:@"openLessonSession"];
    [defaults synchronize];
    
    [self addLoadingVehiclesIndicator];
    [self performSelector:@selector(loadScrollData) withObject:self afterDelay:1.0];
    //[self loadScrollData];
    
    self.lessonNumber = currentProgressLevelObject.currentUnit;
    NSLog(@"currentProgressLessonObject.scoreForLesson %d",currentProgressLessonObject.scoreForLesson);
    totalScore = 0;//if you need to continue then open this//currentProgressLessonObject.scoreForLesson;
    pointForVipScopre = 0 ;
    //NSLog(@"kParam007 %d",kParam007);
    //NSLog(@"currentProgressUserObject.score %d",currentProgressUserObject.score);
    //int resMod = currentProgressUserObject.score % kParam007;
    //pointForVipScopre = resMod;
    
    //vipScopre = kParam014;
    [self updateLabels];
    
    imageViewUserRank.image = [UIImage imageNamed:[NSString stringWithFormat:@"Awards_%02d", [[USER_MANAGER takeMaxRank] intValue]+1]];
    
    imageViewUserProfile.image = [USER_MANAGER takeCurrentUserImage];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
   /* [vehiclesArray removeAllObjects];
    vehiclesArray = nil;
    
    [vehiclePassArray removeAllObjects];
    vehiclePassArray = nil;*/
    
    if (haveConversion)
        return;
    if(player){
        player.delegate = nil;
        [player stop];
        [player removeFromSuperview];
        player = nil;
    }
}
- (void)showAlert:(NSString*)stringForSubtitle{
    FCAlertView *alertView = [[FCAlertView alloc] init];
    alertView.alertBackgroundColor = [UIColor colorWithRed:112.0f/255.0f green:102.0f/255.0f blue:55.0f/255.0f alpha:1.0];
    alertView.backgroundColor = [UIColor clearColor];
    alertView.colorScheme = [UIColor blackColor];
    alertView.doneButtonTitleColor = [UIColor whiteColor];
    alertView.hideSeparatorLineView = NO;
    alertView.blurBackground = NO;
    [alertView setSubTitle:stringForSubtitle];
    [alertView setTitle:@""];
    
    
    [alertView showAlertWithTitle:@"" withSubtitle:stringForSubtitle withCustomImage:nil withDoneButtonTitle:[LABEL_MANAGER setLocalizeLabelText:@"no_label"] andButtons:nil];
    [alertView doneActionBlock:^{
        _continueSession = YES;
        self.isPopupOpened = NO;
        if (self.isLoadingFinished) {
            [self activeteFirstVehicle];
        }
    }];
    
    [alertView addButton:[LABEL_MANAGER setLocalizeLabelText:@"yes_label"] withActionBlock:^{
        
        self.isPopupOpened = NO;
        if (self.isLoadingFinished) {
            [self activeteFirstVehicle];
        }
        
    }];
    
    alertView.secondButtonTitleColor  = [UIColor whiteColor];
    alertView.secondButtonBackgroundColor = [UIColor blackColor];
    alertView.firstButtonTitleColor  = [UIColor whiteColor];
    alertView.firstButtonBackgroundColor = [UIColor blackColor];
}

- (void)initialScroll {
    
    topBarView.backgroundColor = APP_GOLD_COLOR;
    rightArrowButton.userInteractionEnabled = NO;
    leftArrowButton.userInteractionEnabled = NO;
    
    [mainScrollView setDelegate:self];
    [mainScrollView setBouncesZoom:YES];
    [mainScrollView setContentMode:UIViewContentModeCenter];
    mainScrollView.userInteractionEnabled = NO;
    
    mainScrollView.contentSize = CGSizeMake(DEVICE_BIGER_SIDE*9, mainScrollView.contentSize.height);
    [mainScrollView setScrollEnabled:YES];
    [mainScrollView setShowsHorizontalScrollIndicator:FALSE];
    [mainScrollView setShowsVerticalScrollIndicator:FALSE];
    
    mainScrollView.scrollsToTop = NO;
    mainScrollView.pagingEnabled = YES;
    
    initialConstantForBottomConstrains = 559;//[mainScrollView height];//instructionViewBottomConstrains.constant;
    instructionViewBottomConstrains.constant = initialConstantForBottomConstrains;
    instructionViewTopConstrains.constant = -initialConstantForBottomConstrains;
    [self.view layoutIfNeeded];
    
//    topBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar_bg_up"]];
//    questionBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar_bg_down"]];
}

- (void)configureObservers {
    
    [super configureObservers];
}

- (void)addSwipeGesture {
     [self removeSwipeGesture];
    
    leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeActionScorllContainer:)];
    [leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
    leftSwipe.delegate = self;
    [self.view addGestureRecognizer:leftSwipe];
    
    rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeActionScorllContainer:)];
    [rightSwipe setDirection:UISwipeGestureRecognizerDirectionRight];
    rightSwipe.delegate = self;
    [self.view addGestureRecognizer:rightSwipe];
}

- (void)removeSwipeGesture {
    
    
    [self.view removeGestureRecognizer:rightSwipe];
    [self.view removeGestureRecognizer:leftSwipe];
}

- (void)activeteFirstVehicle {
    
    BaseViewVehicle *currentVehicle;
    if([vehiclesArray count] == 0){
        return;
    }
    if (self.continueSession) {
        if(currentProgressLessonObject.currentVehicle >= [vehiclesArray count] ){
            currentProgressLessonObject.currentVehicle = [vehiclesArray count] - 1;
        }
        currentVehicle = [vehiclesArray objectAtIndex:currentProgressLessonObject.currentVehicle];
        if([currentVehicle vehicleIsAvalible]){
            [currentVehicle activateVehicle];
        }else{
            [self showConversionPoint];
        }
        lastCurrentPage = currentProgressLessonObject.currentVehicle;
        
        ProgressVehicleObject *tempProgressVehicle =  [currentProgressLessonObject getCurrentProgressVehicleObject];
        tempProgressVehicle.achived = YES;
        
        [DATA_MANAGER saveData];
        
        if (currentProgressLessonObject.currentVehicle == 0) {
            leftArrowButton.hidden = YES;
            
        }
        
        [mainScrollView setContentOffset:CGPointMake(currentProgressLessonObject.currentVehicle*DEVICE_BIGER_SIDE, 0) animated:YES];
        
        [currentLessonProgress configureUI];
        [currentLessonProgress changeProgressWithValue:stepForProgressBar*currentProgressLessonObject.currentVehicle];
        
    }
    else {
        
        currentVehicle = [vehiclesArray objectAtIndex:0];
        [currentVehicle activateVehicle];
        
        currentProgressLessonObject.currentVehicle = 0;
        
        leftArrowButton.hidden = YES;
        
        ProgressVehicleObject *tempProgressVehicle =  [currentProgressLessonObject getCurrentProgressVehicleObject];
        tempProgressVehicle.achived = YES;
        
        [DATA_MANAGER saveData];
        
        [currentLessonProgress configureUI];
        
    }
}
- (void)loadScrollData {
    [self disableScrolling:0];
    
    rightArrowButton.userInteractionEnabled = NO;
    leftArrowButton.userInteractionEnabled = NO;
    
    
    self.vehicleWidth = mainScrollView.frame.size.width;
    self.vehicleHeight = mainScrollView.frame.size.height;
    
    //mainScrollView.contentSize = CGSizeMake(DEVICE_BIGER_SIDE*17, mainScrollView.contentSize.height);
    [self addActivityIndicator];
    vehiclesArray = [[NSMutableArray alloc]init];
    int counter = 0;
    float delayInSeconds = 0.1;
    
    currentLessonObject = [DATA_MANAGER takeLessonObjectObject:currentProgressLevelObject.currentUnit];
    NSArray *vehiclesDataArray = [DATA_MANAGER takeAllVehiclesObjectInLesson:currentProgressLevelObject.currentUnit];
    
    //Logic count the progres bar step
    stepForProgressBar = percentBetweenPoints/(vehiclesDataArray.count/nComponentsProgressBar);
    stepsBetweenPoints = vehiclesDataArray.count/nComponentsProgressBar;
    
    for (id vehicleData in vehiclesDataArray) {
        //VehicleObjectVideo
        // makeVehicleObjectVideo
        BaseViewVehicle *tempVehicleView;
        if([vehicleData isKindOfClass: [VehicleObjectVideo class]])
        {
            tempVehicleView= [self createVehicleObjectVideo:vehicleData];//
        }
        else if([vehicleData isKindOfClass: [VehicleObjectVoiceRecognition class]])
        {
             tempVehicleView= [self createVehicleObjectVoiceRecognition:vehicleData];//
        }
        else if([vehicleData isKindOfClass: [VehicleObjectSwipeCarousel class]])
        {
             tempVehicleView= [self createVehicleObjectSwipeCarousel:vehicleData];//
        }
        else if([vehicleData isKindOfClass: [VehicleObjectMultipleText class]])
        {
             tempVehicleView= [self createVehicleObjectMultipleText:vehicleData];//
        }
        else if([vehicleData isKindOfClass: [VehicleObjectMultipleImage class]])
        {
            //                   tempVehicleView= [self :vehicleData];
            tempVehicleView= [self createVehicleObjectClickToLearn:vehicleData];//
        }
        else if([vehicleData isKindOfClass: [VehicleObjectFillTheMissing class]])
        {
             tempVehicleView= [self createVehicleObjectFillTheMissing:vehicleData];//
        }
        else if([vehicleData isKindOfClass: [VehicleObjectClickToLearn class]])
        {
             tempVehicleView= [self createVehicleObjectClickToLearn:vehicleData];//
        }
        else if([vehicleData isKindOfClass: [VehicleObjectSwipe class]])
        {
             tempVehicleView= [self createVehicleObjectSwipe :vehicleData]; //kirca
        }
        else if([vehicleData isKindOfClass: [VehicleObjectSpelling class]])
        {
             tempVehicleView= [self createVehicleObjectSpelling :vehicleData];//dimitar
        }
        
        ++counter;
        //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([vehicleData isKindOfClass: [VehicleObjectVideo class]]) {

            [(VideoVehicle *)tempVehicleView setDataForVehicle:vehicleData isHighQuality:self.videoHihgQuality];
        }
        else {
        
            [tempVehicleView setDataForVehicle:vehicleData];
        }
            tempVehicleView.vehicleDelegate = self;
            [self addVehicle:tempVehicleView];
        //});

    }
    //end for 
    /////////////////
    mainScrollView.contentSize = CGSizeMake(DEVICE_BIGER_SIDE*(counter+2), mainScrollView.contentSize.height);
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((counter+2) * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if (!self.isPopupOpened) {
            
            [self activeteFirstVehicle];
            
        }
        [self removeActivityIndicator];
        self.isLoadingFinished = YES;
        mainScrollView.userInteractionEnabled = YES;
        rightArrowButton.userInteractionEnabled = YES;
        leftArrowButton.userInteractionEnabled = YES;
        [self addSwipeGesture];
        [self enableScrolling:0];
    });
}

- (void)loadScrollDataOld {
    
    [self disableScrolling:0];
    mainScrollView.contentSize = CGSizeMake(DEVICE_BIGER_SIDE*17, mainScrollView.contentSize.height);
    [self addActivityIndicator];
    
    
    vehiclesArray = [[NSMutableArray alloc]init];
    
    
    VideoVehicle *tempVehivleNew =  [[VideoVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)];
    tempVehivleNew.vehicleDelegate = self;
    tempVehivleNew.goNextAfterVideoFinish = YES;
    tempVehivleNew.videoUrlString = @"http://cdn.kantoo.com/Hollywood/Hollywood%20Demo%20-%20part1.mp4";
    [self addVehicle:tempVehivleNew];
    //
    
    int counter = 1;
    float delayInSeconds = 0.1;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        MultipleChoiceText *multiChoiceText3 = [[MultipleChoiceText alloc] initWithFrame:
                                                CGRectMake([vehiclesArray count]*self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)
                                                                                    data:@{
                                                                                           @"videoUrlString" : @"http://cdn.kantoo.com/Hollywood/Hollywood%20Demo%20-%20part1.mp4",
                                                                                           @"answers": @[
                                                                                                   @{@"text": @"Father & Daughter", @"isCorrect": @(NO)},
                                                                                                   @{@"text": @"Husband & Wife", @"isCorrect": @(YES)},
                                                                                                   @{@"text": @"Brother & Sister", @"isCorrect": @(NO)},
                                                                                                   @{@"text": @"Son & Mather", @"isCorrect": @(NO)}],
                                                                                           @"isOneImageLayout": @(YES),
                                                                                           @"bigImage": @"sjp-mct",
                                                                                           @"questionText": @"Matthew Broderick & Sarah Jessica Parker are ?"
                                                                                           
                                                                                           } isOrdered:NO];
        multiChoiceText3.vehicleDelegate = self;
        [self addVehicle:multiChoiceText3];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        FillMissingWordsVehicle *missingWordsVehicle = [[FillMissingWordsVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*mainScrollView.frame.size.width, 0, self.vehicleWidth, self.vehicleHeight) data:@{
                                                                                                                                                                                                                         @"answers": @[
                                                                                                                                                                                                                                 @{@"text": @"Isn't", @"sound": @"Dog.wav",@"isCorrect":@YES, @"correctPlace":@0},
                                                                                                                                                                                                                                 @{@"text": @"Tall", @"sound": @"Dog.wav",@"isCorrect":@YES,@"correctPlace":@1},
                                                                                                                                                                                                                                 @{@"text": @"Short", @"sound": @"Dog.wav",@"isCorrect":@NO,@"correctPlace":@-1},
                                                                                                                                                                                                                                 @{@"text": @"Aren't", @"sound": @"Dog.wav",@"isCorrect":@NO,@"correctPlace":@-1}],
                                                                                                                                                                                                                         
                                                                                                                                                                                                                         @"question":@"He <Isn't> <tall>",                                                                                                                    @"answer": @"He Isn't tall",
                                                                                                                                                                                                                         @"bigImage":@"sjp-mct",                                                                                           }];
        missingWordsVehicle.vehicleDelegate = self;
        [self addVehicle:missingWordsVehicle];
        
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        FillMissingWordsVehicle *missingWordsVehicle = [[FillMissingWordsVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*mainScrollView.frame.size.width, 0, self.vehicleWidth, self.vehicleHeight) data:@{
                                                                                                                                                                                                                         @"answers": @[
                                                                                                                                                                                                                                 @{@"text": @"Isn't", @"isCorrect":@YES, @"correctPlace":@0},
                                                                                                                                                                                                                                 @{@"text": @"Tall", @"isCorrect":@YES,@"correctPlace":@1},
                                                                                                                                                                                                                                 @{@"text": @"Aren't", @"isCorrect":@NO,@"correctPlace":@-1}],
                                                                                                                                                                                                                         
                                                                                                                                                                                                                         @"question":@"He <Isn't> <tall>",                                                                                                                    @"answer": @"He Isn't tall",
                                                                                                                                                                                                                         @"bigImage":@"sjp-mct",                                                                                           }];
        missingWordsVehicle.vehicleDelegate = self;
        [self addVehicle:missingWordsVehicle];
        
    });
    /*
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
     FillMissingWordsVehicle *missingWordsVehicle = [[FillMissingWordsVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*mainScrollView.frame.size.width, 0, vehicleWidth, vehicleHeight) data:@{
     @"answers": @[
     
     @{@"text": @"is", @"isCorrect":@NO,@"correctPlace":@-1},
     @{@"text": @"She", @"isCorrect":@NO,@"correctPlace":@0},
     
     @{@"text": @"superwoman", @"isCorrect":@YES,@"correctPlace":@1}],
     
     @"question":@"<She> is a, or not is <superwoman>",                                                                                                                    @"answers": @"She is a, or not is superwoman",
     @"bigImage": @"oskars",                                                                                           }];
     missingWordsVehicle.vehicleDelegate = self;
     [self addVehicle:missingWordsVehicle];
     
     });
     */
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        FillMissingWordsVehicle *missingWordsVehicle = [[FillMissingWordsVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*mainScrollView.frame.size.width, 0, self.vehicleWidth, self.vehicleHeight) data:@{
                                                                                                                                                                                                                         @"answers": @[
                                                                                                                                                                                                                                 @{@"text": @"He", @"isCorrect":@NO, @"correctPlace":@-1},
                                                                                                                                                                                                                                 @{@"text": @"She", @"isCorrect":@YES,@"correctPlace":@0} ],
                                                                                                                                                                                                                         
                                                                                                                                                                                                                         @"question":@"<She> is.",                                                                                                                    @"answer": @"She is.",
                                                                                                                                                                                                                         @"bigImage": @"bredAngelina",                                                                                           }];
        missingWordsVehicle.vehicleDelegate = self;
        [self addVehicle:missingWordsVehicle];
        
    });
    /*
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
     FillMissingWordsVehicle *missingWordsVehicle = [[FillMissingWordsVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*mainScrollView.frame.size.width, 0, vehicleWidth, vehicleHeight) data:@{
     @"answers": @[
     
     @{@"text": @"He1", @"isCorrect":@NO, @"correctPlace":@-1},
     
     @{@"text": @"He2", @"isCorrect":@NO, @"correctPlace":@-1},
     @{@"text": @"He3", @"isCorrect":@NO, @"correctPlace":@-1},
     
     @{@"text": @"He4", @"isCorrect":@NO, @"correctPlace":@-1},
     @{@"text": @"She", @"isCorrect":@YES,@"correctPlace":@0} ],
     
     @"question":@"<She> is.",                                                                                                                    @"answers": @"She is .",
     @"bigImage": @"oskars",                                                                                           }];
     missingWordsVehicle.vehicleDelegate = self;
     [self addVehicle:missingWordsVehicle];
     
     });
     */
    /*dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
     FillMissingWordsVehicle *missingWordsVehicle = [[FillMissingWordsVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*mainScrollView.frame.size.width, 0, vehicleWidth, vehicleHeight) data:@{
     @"answers": @[
     
     @{@"text": @"He1", @"isCorrect":@NO, @"correctPlace":@-1},
     
     @{@"text": @"He2", @"isCorrect":@NO, @"correctPlace":@-1},
     @{@"text": @"He3", @"isCorrect":@NO, @"correctPlace":@-1},
     
     @{@"text": @"He4", @"isCorrect":@NO, @"correctPlace":@-1},
     
     @{@"text": @"He5", @"isCorrect":@NO, @"correctPlace":@-1},
     
     @{@"text": @"She", @"isCorrect":@YES,@"correctPlace":@0} ],
     
     @"question":@"<She> is.",                                                                                                                    @"answers": @"She is .",
     @"bigImage": @"oskars",                                                                                           }];
     missingWordsVehicle.vehicleDelegate = self;
     [self addVehicle:missingWordsVehicle];
     
     });
     */
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        SwipeKircaVehicle *swipeView2 = [[SwipeKircaVehicle alloc]
                                         initWithFrame:
                                         CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)
                                         
                                         data:@{
                                                @"afterAudio":@"It is.wav",
                                                @"swipeImageVehicle":@NO,
                                                @"answers": @[
                                                        @{@"text": @"", @"isCorrect": @NO, @"image": @"Layer 28"},
                                                        @{@"text": @"", @"isCorrect": @YES, @"image": @"Taylor-In-Black"}],
                                                @"movingViewData": @{
                                                        @"textAnswer":  @"A Black Dress",
                                                        @"isText":      @YES,
                                                        @"imageAnswer": @""
                                                        }
                                                }];
        swipeView2.vehicleDelegate = self;
        [self addVehicle:swipeView2];
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        SwipeCarouselVehicle *carouseView = [[SwipeCarouselVehicle alloc]
                                             initWithFrame:
                                             CGRectMake([vehiclesArray count]*self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)
                                             data:@{   @"answers": @[
                                                               @{@"text": @"Select",@"image": @"0801", @"isCorrect": @YES},
                                                               @{@"text": @"Select",@"image": @"Carousel2", @"isCorrect": @NO},
                                                               @{@"text": @"Select",@"image": @"Carousel3", @"isCorrect": @NO}],
                                                       
                                                       @"questionTextVocabs" : @(YES),
                                                       @"questionText": @"Select the image that shows six #<link>actors</link><trans>actores</trans># ."
                                                       
                                                       }];
        carouseView.vehicleDelegate = self;
        [self addVehicle:carouseView];
        
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        ClickToLearnVehicle *clickVehicleAnswers = [[ClickToLearnVehicle alloc] initWithFrame:
                                                    CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)
                                                                                         data:@{
                                                                                                @"answers": @[
                                                                                                        @{@"text": @"Jennifer Lopez", @"isCorrect": @(YES), @"image": @"jl-2"},
                                                                                                        @{@"text": @"Billy Bob Thornton", @"isCorrect": @(YES), @"image": @"bbt-2"},
                                                                                                        @{@"text": @"Elizabeth Taylor", @"isCorrect": @(NO), @"image": @"et-2"},
                                                                                                        @{@"text": @"Kim Kardashian", @"isCorrect": @(NO), @"image": @"kk-2"}],
                                                                                                @"isOneImageLayout": @(NO),
                                                                                                @"bigImage": @"family",
                                                                                                @"questionText": @"Can you guess which of the following stars got married the most?"
                                                                                                
                                                                                                } isLearnVehicle:NO isOrdered:NO];
        clickVehicleAnswers.vehicleDelegate = self;
        [self addVehicle:clickVehicleAnswers];
        
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        ClickToLearnVehicle *clickVehicle2Answers = [[ClickToLearnVehicle alloc] initWithFrame:
                                                     CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)
                                                                                          data:@{
                                                                                                 @"answers": @[
                                                                                                         @{@"text": @"Joker", @"isCorrect": @(YES), @"image": @"cl-2-1"},
                                                                                                         @{@"text": @"Liam Neason", @"isCorrect": @(NO), @"image": @"cl-2-2"}],
                                                                                                 @"isOneImageLayout": @(NO),
                                                                                                 @"bigImage": @"family",
                                                                                                 @"questionText": @"Who is the Joker?"
                                                                                                 
                                                                                                 } isLearnVehicle:NO];
        clickVehicle2Answers.vehicleDelegate = self;
        [self addVehicle:clickVehicle2Answers];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        ClickToLearnVehicle *clickVehicle3Answers = [[ClickToLearnVehicle alloc] initWithFrame:
                                                     CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)
                                                                                          data:@{
                                                                                                 @"answers": @[
                                                                                                         @{@"text": @"Jennifer Lopez", @"isCorrect": @(YES), @"image": @"cl-1-1"},
                                                                                                         @{@"text": @"Elizabeth Taylor", @"isCorrect": @(NO), @"image": @"cl-1-2"},
                                                                                                         @{@"text": @"Kim Kardashian", @"isCorrect": @(NO), @"image": @"cl-1-3"}],
                                                                                                 @"isOneImageLayout": @(NO),
                                                                                                 @"bigImage": @"family",
                                                                                                 @"questionText": @"Can you guess which of the following stars got married the most?"
                                                                                                 
                                                                                                 } isLearnVehicle:NO];
        clickVehicle3Answers.vehicleDelegate = self;
        [self addVehicle:clickVehicle3Answers];
    });
    
    ///////
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        VoiceRecoginitionVehicle *voice = [[VoiceRecoginitionVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight) data:@{                                                                                                                                                                                                                @"recordingText":  @"They have six children.",                                                                                                                                                                                                                                                                                                                                                                   @"audioDataUrl":@"It is.wav",                                                                                                                                                                                                                                                                                                                                                                                                @"image": @"bredAngelina"                                                                                                                                                                                                                                                                                                                                                                                                }];
        voice.vehicleDelegate = self;
        [self addVehicle:voice];
    });
    //////
    ///////
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        VoiceRecoginitionVehicle *voice = [[VoiceRecoginitionVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight) data:@{                                                                                                                                                                                                                @"recordingText":  @"They children.",                                                                                                                                                                                                                                                                                                                                                                   @"audioDataUrl":@"It is.wav",                                                                                                                                                                                                                                                                                                                                                                                                @"image": @"bredAngelina"                                                                                                                                                                                                                                                                                                                                                                                                }];
        voice.vehicleDelegate = self;
        [self addVehicle:voice];
    });
    
    ///////
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        VoiceRecoginitionVehicle *voice = [[VoiceRecoginitionVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight) data:@{                                                                                                                                                                                                                @"recordingText":  @"Children.",                                                                                                                                                                                                                                                                                                                                                                   @"audioDataUrl":@"It is.wav",                                                                                                                                                                                                                                                                                                                                                                                                @"image": @"bredAngelina"                                                                                                                                                                                                                                                                                                                                                                                                }];
        voice.vehicleDelegate = self;
        [self addVehicle:voice];
    });
    
    ///////
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        VoiceRecoginitionVehicle *voice = [[VoiceRecoginitionVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight) data:@{                                                                                                                                                                                                                @"recordingText":  @"They have dog.",                                                                                                                                                                                                                                                                                                                                                                   @"audioDataUrl":@"It is.wav",                                                                                                                                                                                                                                                                                                                                                                                                @"image": @"bredAngelina"                                                                                                                                                                                                                                                                                                                                                                                                }];
        voice.vehicleDelegate = self;
        [self addVehicle:voice];
    });
    //////
    ///////
    /*dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
     VoiceRecoginitionVehicle *voice = [[VoiceRecoginitionVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight) data:@{                                                                                                                                                                                                                @"recordingText":  @"I have an apple.",                                                                                                                                                                                                                                                                                                                                                                   @"audioDataUrl":@"It is.wav",                                                                                                                                                                                                                                                                                                                                                                                                @"image": @"bredAngelina"                                                                                                                                                                                                                                                                                                                                                                                                }];
     voice.vehicleDelegate = self;
     [self addVehicle:voice];
     });*/
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        MultipleChoiceText *multiChoiceText = [[MultipleChoiceText alloc] initWithFrame:
                                               CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)
                                                                                   data:@{
                                                                                          @"answers": @[
                                                                                                  @{@"text": @"Four", @"isCorrect": @(NO)},
                                                                                                  @{@"text": @"Five", @"isCorrect": @(YES)},
                                                                                                  @{@"text": @"Three", @"isCorrect": @(NO)},
                                                                                                  @{@"text": @"One", @"isCorrect": @(NO)}],
                                                                                          @"isOneImageLayout": @(YES),
                                                                                          @"bigImage": @"the-usual-suspects",
                                                                                          @"questionText": @"How many people are in this photo of the \"Usual Suspects\" movie?"
                                                                                          
                                                                                          } isOrdered:NO];
        multiChoiceText.vehicleDelegate = self;
        [self addVehicle:multiChoiceText];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        MultipleChoiceText *multiChoiceText2 = [[MultipleChoiceText alloc] initWithFrame:
                                                CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)
                                                                                    data:@{
                                                                                           @"answers": @[
                                                                                                   @{@"text": @"Father & Daughter", @"isCorrect": @(NO)},
                                                                                                   @{@"text": @"Husband & Wife", @"isCorrect": @(YES)},
                                                                                                   @{@"text": @"Brother & Sister", @"isCorrect": @(NO)},
                                                                                                   @{@"text": @"Son & Mather", @"isCorrect": @(NO)}],
                                                                                           @"isOneImageLayout": @(YES),
                                                                                           @"bigImage": @"sjp-mct",
                                                                                           @"questionText": @"Matthew Broderick & Sarah Jessica Parker are ?"
                                                                                           
                                                                                           } isOrdered:NO];
        multiChoiceText2.vehicleDelegate = self;
        [self addVehicle:multiChoiceText2];
        
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //////////////
        ClickToLearnVehicle *clickLearnOne = [[ClickToLearnVehicle alloc] initWithFrame:
                                              CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)
                                                                                   data:@{
                                                                                          @"answers": @[
                                                                                                  @{@"text": @"Gato", @"isCorrect": @(YES), @"image": @"cat", @"sound": @"Cat.wav", @"targetText": @"Cat"},
                                                                                                  @{@"text": @"Perro", @"isCorrect": @(NO), @"image": @"dog", @"sound": @"Dog.wav", @"targetText": @"Dog"},
                                                                                                  @{@"text": @"Mono", @"isCorrect": @(NO), @"image": @"monkey", @"sound": @"Monkey.wav", @"targetText": @"Monkey"},
                                                                                                  @{@"text": @"Caballo", @"isCorrect": @(NO), @"image": @"mustang", @"sound": @"Horse.wav", @"targetText": @"Horse"}
                                                                                                  ],
                                                                                          @"isOneImageLayout": @(NO),
                                                                                          @"bigImage": @"family",
                                                                                          @"questionTextVocabs" : @(YES),
                                                                                          @"questionText": @"Click on each of the following #<link>animals</link><trans>Animales</trans># to hear and see what they’re called in English."
                                                                                          
                                                                                          } isLearnVehicle:YES];
        clickLearnOne.vehicleDelegate = self;
        [self addVehicle:clickLearnOne];
        //////////////
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //Click to learn 2 and 3 image layout
        
        ClickToLearnVehicle *clickToLearn2Layout = [[ClickToLearnVehicle alloc] initWithFrame:
                                                    CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)
                                                                                         data:@{
                                                                                                @"answers": @[
                                                                                                        @{@"text": @"Sorrir", @"isCorrect": @(YES), @"image": @"cl-2-1", @"targetText": @"Smile"},
                                                                                                        @{@"text": @"Sério", @"isCorrect": @(NO), @"image": @"cl-2-2", @"targetText": @"Serious"}],
                                                                                                @"isOneImageLayout": @(NO),
                                                                                                @"bigImage": @"family",
                                                                                                @"questionText": @"Click to learn, 2 images layout"
                                                                                                
                                                                                                } isLearnVehicle:YES];
        clickToLearn2Layout.vehicleDelegate = self;
        [self addVehicle:clickToLearn2Layout];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        ClickToLearnVehicle *clickToLearn3Layout = [[ClickToLearnVehicle alloc] initWithFrame:
                                                    CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)
                                                                                         data:@{
                                                                                                @"answers": @[
                                                                                                        @{@"text": @"Correndo", @"isCorrect": @(YES), @"image": @"cl-1-1", @"targetText": @"Running"},
                                                                                                        @{@"text": @"Navegação", @"isCorrect": @(NO), @"image": @"cl-1-2", @"targetText": @"Sailing"},
                                                                                                        @{@"text": @"Dançando", @"isCorrect": @(NO), @"image": @"cl-1-3", @"targetText": @"Dancing"}],
                                                                                                @"isOneImageLayout": @(NO),
                                                                                                @"bigImage": @"family",
                                                                                                @"questionText": @"Click to learn, 3 images layout"
                                                                                                
                                                                                                } isLearnVehicle:YES];
        clickToLearn3Layout.vehicleDelegate = self;
        [self addVehicle:clickToLearn3Layout];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        SpellingVehicle *spelling = [[SpellingVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)
                                                                      data:@{
                                                                             @"image": @"MJ-spelling",
                                                                             @"answer": @"<You>'<re> a beautiful woman.",
                                                                             @"questionText": @"Shorten: You are a beautiful women",
                                                                             @"afterAudio": @"It is.wav"
                                                                             }];
        spelling.vehicleDelegate = self;
        [self addVehicle:spelling];
        
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        SwipeKircaVehicle *swipeView1 = [[SwipeKircaVehicle alloc]
                                         initWithFrame:
                                         CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)
                                         
                                         data:@{
                                                @"afterAudio":@"It's a bird it's a plane....wav",
                                                @"beforeAudio":@"It's a bird it's a plane....wav",
                                                
                                                @"swipeImageVehicle":@YES,
                                                @"answers": @[
                                                        @{@"text": @"A green dress", @"isCorrect": @YES, @"image": @""},
                                                        @{@"text": @"A red dress", @"isCorrect": @NO, @"image": @""}],
                                                @"movingViewData": @{
                                                        @"textAnswer":  @"",
                                                        @"isText":      @NO,
                                                        @"imageAnswer": @"swipeImage"
                                                        }
                                                }];
        swipeView1.vehicleDelegate = self;
        [self addVehicle:swipeView1];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        SwipeKircaVehicle *swipeView2 = [[SwipeKircaVehicle alloc]
                                         initWithFrame:
                                         CGRectMake([vehiclesArray count]* self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)
                                         
                                         data:@{
                                                @"afterAudio":@"It is.wav",
                                                @"beforeAudio":@"It is.wav",
                                                @"swipeImageVehicle":@NO,
                                                @"answers": @[
                                                        @{@"text": @"", @"isCorrect": @NO, @"image": @"Layer 28"},
                                                        @{@"text": @"", @"isCorrect": @YES, @"image": @"Taylor-In-Black"}],
                                                @"movingViewData": @{
                                                        @"textAnswer":  @"A Black Dress",
                                                        @"isText":      @YES,
                                                        @"imageAnswer": @""
                                                        }
                                                }];
        swipeView2.vehicleDelegate = self;
        [self addVehicle:swipeView2];
    });
    
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //        ClickToLearnVehicle *clickLearnMulti = [[ClickToLearnVehicle alloc] initWithFrame:
    //                                                CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
    //                                                                                     data:@{
    //                                                                                            @"answers": @[
    //                                                                                                    @{@"text": @"Hija", @"image": @"Daugther_After.png", @"isCorrect": @(YES), @"targetText": @"Daughter"},
    //                                                                                                    @{@"text": @"Mama", @"image": @"Mother_After.png", @"isCorrect": @(NO), @"targetText": @"Mother"},
    //                                                                                                    @{@"text": @"Padre", @"image": @"Father_After.png", @"isCorrect": @(NO), @"targetText": @"Father"},
    //                                                                                                    @{@"text": @"Hijo", @"image": @"Son_After.png", @"isCorrect": @(NO), @"targetText": @"Son"},
    //                                                                                                    @{@"text": @"Mascota", @"image": @"Family-Pet_After.png", @"sound": @"Dog.wav", @"isCorrect": @(NO), @"targetText": @"Dog"}],
    //                                                                                            @"isOneImageLayout": @(YES),
    //                                                                                            @"bigImage": @"Family_Full.png",
    //                                                                                            @"questionText": @"Click on each of the family members to hear and see what they’re called in English."
    //
    //                                                                                            } isLearnVehicle:YES];
    //        clickLearnMulti.vehicleDelegate = self;
    //        [self addVehicle:clickLearnMulti];
    //    });
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //        VideoVehicle *tempVehivle =  [[VideoVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)];
    //        tempVehivle.vehicleDelegate = self;
    //        tempVehivle.videoUrlString =  @"http://cdn.kantoo.com/Hollywood/Hollywood%20Demo%20-%20part%205.mp4";
    //        tempVehivle.goNextAfterVideoFinish = YES;
    //
    //        [self addVehicle:tempVehivle];
    //
    //
    //    });
    
    
    //        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(++counter * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //        ClickToLearnVehicle *clickMultiImageOneLayout = [[ClickToLearnVehicle alloc] initWithFrame:
    //                                                         CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
    //                                                                                              data:@{
    //                                                                                                     @"answers": @[
    //                                                                                                             @{@"text": @"Hija", @"image": @"Daugther_After.png", @"isCorrect": @(NO), @"targetText": @"Daughter"},
    //                                                                                                             @{@"text": @"Mama", @"image": @"Mother_After.png", @"isCorrect": @(NO), @"targetText": @"Mother"},
    //                                                                                                             @{@"text": @"Padre", @"image": @"Father_After.png", @"isCorrect": @(NO), @"targetText": @"Father"},
    //                                                                                                             @{@"text": @"Hijo", @"image": @"Son_After.png", @"isCorrect": @(NO), @"targetText": @"Son"},
    //                                                                                                             @{@"text": @"Mascota", @"image": @"Family-Pet_After.png", @"sound": @"Dog.wav", @"isCorrect": @(YES), @"targetText": @"Dog"}],
    //                                                                                                     @"isOneImageLayout": @(YES),
    //                                                                                                     @"bigImage": @"Family_Full.png",
    //                                                                                                     @"questionText": @"Click on the dog."
    //
    //                                                                                                     } isLearnVehicle:NO];
    //        clickMultiImageOneLayout.vehicleDelegate = self;
    //        [self addVehicle:clickMultiImageOneLayout];
    //
    //    });
    mainScrollView.contentSize = CGSizeMake(DEVICE_BIGER_SIDE*(counter+2), mainScrollView.contentSize.height);
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((counter+2) * delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if (!self.isPopupOpened) {
            
            [self activeteFirstVehicle];
            
        }
        [self removeActivityIndicator];
        self.isLoadingFinished = YES;
        [self enableScrolling:0];
    });
    
}

- (void)loadScrollData1 {
    
    vehiclesArray = [[NSMutableArray alloc]init];
    float vehicleWidth = mainScrollView.frame.size.width;
    float vehicleHeight = mainScrollView.frame.size.height;
    //    SpellingVehicle *spelling = [[SpellingVehicle alloc] initWithFrame:CGRectMake(60, 0, self.view.frame.size.width-120, 250) data:@{
    //                                                                                                                                      @"answer": @"Erm. <You>'<re> a good man."
    //                                                                                                                                      }];
    //    spelling.vehicleDelegate = self;
    //    [self addVehicle:spelling];
    
    MultipleChoiceText *multiChoiceText = [[MultipleChoiceText alloc] initWithFrame:
                                           CGRectMake(0, 0, vehicleWidth, vehicleHeight)
                                                                               data:@{
                                                                                      @"answers": @[
                                                                                              @{@"text": @"Four", @"isCorrect": @(YES)},
                                                                                              @{@"text": @"Five", @"isCorrect": @(NO)},
                                                                                              @{@"text": @"Three", @"isCorrect": @(NO)},
                                                                                              @{@"text": @"One", @"isCorrect": @(NO)}],
                                                                                      @"isOneImageLayout": @(YES),
                                                                                      @"bigImage": @"the-usual-suspects",
                                                                                      @"questionText": @"How many people are in this photo of the \"Usual Suspects\" movie?"
                                                                                      
                                                                                      } isOrdered:NO];
    multiChoiceText.vehicleDelegate = self;
    [self addVehicle:multiChoiceText];
    
    SwipeKircaVehicle *swipeView21 = [[SwipeKircaVehicle alloc]
                                      initWithFrame:
                                      CGRectMake([vehiclesArray count]*vehicleWidth, 0, vehicleWidth, vehicleHeight)
                                      
                                      data:@{
                                             @"afterAudio":@"It is.wav",
                                             @"beforeAudio":@"It is.wav",
                                             @"swipeImageVehicle":@NO,
                                             @"answers": @[
                                                     @{@"text": @"", @"isCorrect": @NO, @"image": @"Layer 28"},
                                                     @{@"text": @"", @"isCorrect": @YES, @"image": @"Taylor-In-Black"}],
                                             @"movingViewData": @{
                                                     @"textAnswer":  @"A Black Dress",
                                                     @"isText":      @YES,
                                                     @"imageAnswer": @""
                                                     }
                                             }];
    swipeView21.vehicleDelegate = self;
    [self addVehicle:swipeView21];
    
    
    FillMissingWordsVehicle *missingWordsVehicle = [[FillMissingWordsVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*mainScrollView.frame.size.width, 0, vehicleWidth, vehicleHeight) data:@{
                                                                                                                                                                                                           @"answers": @[
                                                                                                                                                                                                                   @{@"text": @"Isn't", @"isCorrect":@YES, @"correctPlace":@0},
                                                                                                                                                                                                                   @{@"text": @"Tall", @"isCorrect":@YES,@"correctPlace":@1},
                                                                                                                                                                                                                   @{@"text": @"Short", @"isCorrect":@NO,@"correctPlace":@-1},
                                                                                                                                                                                                                   @{@"text": @"Aren't", @"isCorrect":@NO,@"correctPlace":@-1}],
                                                                                                                                                                                                           
                                                                                                                                                                                                           @"question":@"He <Isn't> <Tall>",                                                                                                                    @"answers": @"He Isn't Tall",
                                                                                                                                                                                                           @"bigImage": @"oskars",                                                                                           }];
    missingWordsVehicle.vehicleDelegate = self;
    [self addVehicle:missingWordsVehicle];
    
    FillMissingWordsVehicle *missingWordsVehicle2 = [[FillMissingWordsVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*mainScrollView.frame.size.width, 0, vehicleWidth, vehicleHeight) data:@{
                                                                                                                                                                                                            @"answers": @[
                                                                                                                                                                                                                    @{@"text": @"Isn'tIsn't", @"isCorrect":@YES, @"correctPlace":@0},
                                                                                                                                                                                                                    @{@"text": @"T", @"isCorrect":@YES,@"correctPlace":@1},
                                                                                                                                                                                                                    @{@"text": @"Short", @"isCorrect":@NO,@"correctPlace":@-1},
                                                                                                                                                                                                                    @{@"text": @"Aren't", @"isCorrect":@NO,@"correctPlace":@-1}],
                                                                                                                                                                                                            
                                                                                                                                                                                                            @"question":@"He <Isn'tIsn't> <T>",                                                                                                                    @"answers": @"He Isn't Tall",                                                                                         }];
    missingWordsVehicle2.vehicleDelegate = self;
    [self addVehicle:missingWordsVehicle2];
    return;
    
    /*BaseViewVehicle *tempVehivle =  [[VideoVehicle alloc] initWithFrame:CGRectMake(0,0,mainScrollView.frame.size.width, mainScrollView.frame.size.height)];
     tempVehivle.vehicleDelegate = self;
     [mainScrollView addSubview:tempVehivle];
     [tempVehivle activateVehicle];*/
    /*SwipeKircaVehicle *swipeView1 = [[SwipeKircaVehicle alloc]
     initWithFrame:
     CGRectMake(0, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)
     
     data:@{
     @"movingViewData": @{
     @"textAnswer":  @"Angelina Jolie",
     @"isText":      @(YES),
     @"imageAnswer": @"angelina"
     }
     }];
     swipeView1.vehicleDelegate = self;
     [mainScrollView addSubview:swipeView1];*/
    
    /* SwipeVehicleView *swipeView = [[SwipeVehicleView alloc]
     initWithFrame:
     CGRectMake(0, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)
     
     data:@{
     @"movingViewData": @{
     @"textAnswer":  @"Angelina Jolie",
     @"isText":      @(YES),
     @"imageAnswer": @"angelina"
     }
     }];
     swipeView.vehicleDelegate = self;
     [mainScrollView addSubview:swipeView];*/
    
    SwipeCarouselVehicle *swipeView2 = [[SwipeCarouselVehicle alloc]
                                        initWithFrame:
                                        CGRectMake(1*mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)
                                        data:@{   @"answers": @[
                                                          @{@"text": @"Select",@"image": @"Carousel1", @"isCorrect": @YES},
                                                          @{@"text": @"Select",@"image": @"Carousel2", @"isCorrect": @NO},
                                                          @{@"text": @"Select",@"image": @"Carousel3", @"isCorrect": @NO}],
                                                  @"questionText": @"Which of these family members is the mother?"  }];
    swipeView2.vehicleDelegate = self;
    [self addVehicle:swipeView2];
    /* VoiceRecoginitionVehicle *voice = [[VoiceRecoginitionVehicle alloc] initWithFrame:CGRectMake(1*mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height) data:@{                                                                                                                                                                                                                @"recordingText":  @"They have six children.",                                                                                                                                                                                                                 @"image": @"bredAngelina"
     }];
     voice.vehicleDelegate = self;
     [self addVehicle:voice];*/
    
    
    ClickToLearnVehicle *clickVehicle = [[ClickToLearnVehicle alloc] initWithFrame:
                                         CGRectMake(2*mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)
                                                                              data:@{
                                                                                     @"answers": @[
                                                                                             @{@"text": @"mother", @"isCorrect": @(YES)},
                                                                                             @{@"text": @"sister", @"isCorrect": @(NO)},
                                                                                             @{@"text": @"brother", @"isCorrect": @(NO)},
                                                                                             @{@"text": @"father", @"isCorrect": @(NO)}],
                                                                                     @"isOneImageLayout": @(YES),
                                                                                     @"bigImage": @"family"
                                                                                     
                                                                                     } isLearnVehicle:NO];
    clickVehicle.vehicleDelegate = self;
    [self addVehicle:clickVehicle];
    
    ClickToLearnVehicle *clickVehicleAnswers = [[ClickToLearnVehicle alloc] initWithFrame:
                                                CGRectMake(3*mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)
                                                                                     data:@{
                                                                                            @"answers": @[
                                                                                                    @{@"text": @"Jennifer Lopez", @"isCorrect": @(YES), @"image": @"answer1"},
                                                                                                    @{@"text": @"Billy Bob Thornton", @"isCorrect": @(NO), @"image": @"answer2"},
                                                                                                    @{@"text": @"Elizabeth Taylor", @"isCorrect": @(NO), @"image": @"answer3"},
                                                                                                    @{@"text": @"Kim Kardashian", @"isCorrect": @(NO), @"image": @"answer4"}],
                                                                                            @"isOneImageLayout": @(NO),
                                                                                            @"bigImage": @"family"
                                                                                            
                                                                                            } isLearnVehicle:NO];
    clickVehicleAnswers.vehicleDelegate = self;
    [self addVehicle:clickVehicleAnswers];
    
    VideoVehicle *tempVehivle =  [[VideoVehicle alloc] initWithFrame:CGRectMake(4*mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)];
    tempVehivle.vehicleDelegate = self;
    tempVehivle.goNextAfterVideoFinish = YES;
    
    [self addVehicle:tempVehivle];
    
    
    SwipeKircaVehicle *swipeView1 = [[SwipeKircaVehicle alloc]
                                     initWithFrame:
                                     CGRectMake(5*mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)
                                     
                                     data:@{
                                            @"swipeImageVehicle":@YES,
                                            @"answers": @[
                                                    @{@"text": @"A Green Shirt", @"isCorrect": @YES, @"image": @""},
                                                    @{@"text": @"A Red Shirt", @"isCorrect": @NO, @"image": @""}],
                                            @"movingViewData": @{
                                                    @"textAnswer":  @"",
                                                    @"isText":      @NO,
                                                    @"imageAnswer": @"swipeImage"
                                                    }
                                            }];
    swipeView1.vehicleDelegate = self;
    //[swipeView1 setVehicleType];
    [self addVehicle:swipeView1];
    
    VideoVehicle *tempVehivle2 =  [[VideoVehicle alloc] initWithFrame:CGRectMake(6*mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)];
    tempVehivle2.vehicleDelegate = self;
    tempVehivle2.goNextAfterVideoFinish = NO;
    [self addVehicle:tempVehivle2];
    
    
    SwipeKircaVehicle *swipeView3 = [[SwipeKircaVehicle alloc]
                                     initWithFrame:
                                     CGRectMake(7 * mainScrollView.frame.size.width, 0, mainScrollView.frame.size.width, mainScrollView.frame.size.height)
                                     
                                     data:@{
                                            @"swipeImageVehicle":@NO,
                                            @"answers": @[
                                                    @{@"text": @"", @"isCorrect": @NO, @"image": @"Layer 28"},
                                                    @{@"text": @"", @"isCorrect": @YES, @"image": @"Taylor-In-Black"}],
                                            @"movingViewData": @{
                                                    @"textAnswer":  @"A Black Dress",
                                                    @"isText":      @YES,
                                                    @"imageAnswer": @""
                                                    }
                                            }];
    swipeView3.vehicleDelegate = self;
    swipeView3.swipeImageVehicle = YES;
    //[swipeView3 setVehicleType];
    [self addVehicle:swipeView3];
    
    /*NSLog(@"1");
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
     // Background work
     NSLog(@"2");
     NSMutableArray *vehicleArray = [[DATA_MANAGER takeVehicleForCurrentLesson] mutableCopy];
     dispatch_async(dispatch_get_main_queue(), ^{
     NSLog(@"3");
     for(int i = 0; i<5; i++){
     ECVehicleDisplay *tempVehivle =  [[VideoVehicle alloc] initWithFrame:CGRectMake(0+(i*mainScrollView.frame.size.width),0,mainScrollView.frame.size.width, mainScrollView.frame.size.height)];
     //tempVehivle.backgroundColor = [UIColor whiteColor];
     [mainScrollView addSubview:tempVehivle];
     }
     });
     
     });
     NSLog(@"4");*/
    /* dispatch_async(dispatch_get_main_queue(), ^{
     NSMutableArray *vehicleArray = [[DATA_MANAGER takeVehicleForCurrentLesson] mutableCopy];
     for(int i = 3; i<=5; i++){
     ECVehicleDisplay *tempVehivle =  [[VideoVehicle alloc] initWithFrame:CGRectMake(0+(i*mainScrollView.frame.size.width),0,mainScrollView.frame.size.width, mainScrollView.frame.size.height)];
     //tempVehivle.backgroundColor = [UIColor whiteColor];
     [mainScrollView addSubview:tempVehivle];
     }
     [super updateViewConstraints];
     });*/
}

- (void) addVehicle:(BaseViewVehicle*)newVehicle{
    //NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    //[DateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    // NSLog(@"addVehicle time: %@",[DateFormatter stringFromDate:[NSDate date]]);
    
    [mainScrollView addSubview:newVehicle];
    if(newVehicle){
        [vehiclesArray addObject:newVehicle];
    }
    
    //    mainScrollView.contentSize = CGSizeMake(DEVICE_BIGER_SIDE*[vehiclesArray count], mainScrollView.contentSize.height);
}
- (void)vehicleWasFinished:(id)sender {
    
    if ([sender isKindOfClass:[ClickToLearnVehicle class]]) {
        [self performSelector:@selector(gotoNextPage) withObject:nil afterDelay:0.0];
    }
    else {
        [self performSelector:@selector(gotoNextPage) withObject:nil afterDelay:0.0];
    }
    
}

- (void)vehiclePass:(BOOL)correct{
   /* vehicleWithPoints++;
    if(correct){
        vehiclePassCorrect++;
    }*/
}

- (void)vehiclesendStatus:(int)vehiclePlayStatus forID:(int)idForVehicle{
    
    
    NSLog(@"addObject idForVehicle:%d vehiclePlayStatus:%d",idForVehicle,vehiclePlayStatus);
    
    for (NSDictionary *dict in vehiclePassArray)
    {
        int lastId = [[dict valueForKey:@"idForVehicle"]intValue];
        if(lastId == idForVehicle){
            [vehiclePassArray removeObject:dict];
            break;
        }
    }
    
    NSDictionary *dictHeaders = @{@"vehiclePlayStatus":[NSNumber numberWithInt:vehiclePlayStatus],@"idForVehicle":[NSNumber numberWithInt:idForVehicle]};
    [vehiclePassArray addObject:dictHeaders];
    
    
    vehicleWithPoints = (int)[vehiclePassArray count] ;;
    vehiclePassCorrect = 0;
    for (NSDictionary *dict in vehiclePassArray)
    {
        if([[dict valueForKey:@"vehiclePlayStatus"]intValue] == 2){
            vehiclePassCorrect++;
        }
    }
    
    NSLog(@"vehicleWithPoints:%d vehiclePassCorrect:%d",vehicleWithPoints,vehiclePassCorrect);
    
}

#pragma mark - Vehicle delegate methods

- (void)gotoPreviousPage {
    
    if (player && [player isPlaying]) {
        
        AVPlayerItem *currentItem = player.moviePlayer.currentItem;
        CMTime duration = currentItem.duration; //total time
        CMTime currentTime = currentItem.currentTime; //playing time
        if(duration.value){
        NSInteger durationInSeconds = duration.value/duration.timescale;
        NSInteger currentTimeInSeconds = currentTime.value/currentTime.timescale;
        
            if (durationInSeconds - currentTimeInSeconds > 5) {
                
                [player pause];
                [self removeActivityIndicator];
                [player removeFromSuperview];
                
                [self saveActiveVideoData:[NSNumber numberWithInt:currentProgressLessonObject.currentVehicle] time:currentTime];
                player = nil;
                
            }
            else {
                
                [player stop];
                [self removeActivityIndicator];
                [player removeFromSuperview];
                player = nil;
                
            }
        }else{
            [player stop];
            [self removeActivityIndicator];
            [player removeFromSuperview];
            player = nil;
        }
    }
    
    CGFloat xOffset = mainScrollView.contentOffset.x;
    NSInteger currentPage = xOffset/DEVICE_BIGER_SIDE;
    if(currentPage < 1) {
        
        leftArrowButton.userInteractionEnabled = YES;
        return;
    }
    
    if (currentPage-1 == 0) {
        
        leftArrowButton.hidden = YES;
    }
    
    if (currentPage == vehiclesArray.count) {
        
        //to show the right button
        rightArrowButton.hidden = NO;
    }
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        [mainScrollView setContentOffset:CGPointMake((currentPage-1)*DEVICE_BIGER_SIDE, 0) animated:NO];
        
    } completion:^(BOOL finished) {
        
        if(lastCurrentPage>=0){
            if(lastCurrentPage < [vehiclesArray count]){
                
                BaseViewVehicle *lastVehicle = [vehiclesArray objectAtIndex:lastCurrentPage];
                [lastVehicle deactivateVehicle];
                currentProgressLessonObject.currentVehicle = (int)lastCurrentPage;
                [DATA_MANAGER saveData];
            }
        }
        [self activateNewViecle2];
        leftArrowButton.userInteractionEnabled = YES;
        isSideButtonUsed = NO;
    }];
    
    [self removeSwipeGesture];
}

- (void)gotoNextPage {
    
    //if (player && [player isPlaying]) {
     if (player && [player isLoad]) {
        AVPlayerItem *currentItem = player.moviePlayer.currentItem;
        CMTime duration = currentItem.duration; //total time
        CMTime currentTime = currentItem.currentTime; //playing time
        NSInteger durationInSeconds;
        if(duration.timescale >0){
             durationInSeconds = duration.value/duration.timescale;
        }else{
             durationInSeconds = 0;
            [player stop];
            [self removeActivityIndicator];
            [player removeFromSuperview];
            player = nil;

        }
        NSInteger currentTimeInSeconds = currentTime.value/currentTime.timescale;
        
        if (durationInSeconds - currentTimeInSeconds > 5) {
            
            [player pause];
            [self removeActivityIndicator];
            [player removeFromSuperview];
            
            [self saveActiveVideoData:[NSNumber numberWithInt:currentProgressLessonObject.currentVehicle] time:currentTime];
            player = nil;

        }
        else {
            
            [player stop];
            [self removeActivityIndicator];
            [player removeFromSuperview];
            player = nil;
        }
     }else if(player){
         [player stop];
         [self removeActivityIndicator];
         [player removeFromSuperview];
         player = nil;
     }
    
    CGFloat xOffset = mainScrollView.contentOffset.x;
    NSInteger currentPage = xOffset/DEVICE_BIGER_SIDE;
    
    if (currentPage == vehiclesArray.count) {
        
        //TO DO: logic for SUMMARY PAGE
        [self gotoSummaryScreen];
        rightArrowButton.userInteractionEnabled = YES;
        rightArrowButton.hidden = YES;
        
    }
    else if (currentPage+1 > vehiclesArray.count) {
        
        rightArrowButton.userInteractionEnabled = YES;
        return;
        
    }
    
    if (currentPage+1 == 1) {
        
        //to show the left button
        leftArrowButton.hidden = NO;
    }
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
//        [mainScrollView setContentOffset:CGPointMake((currentPage+15)*DEVICE_BIGER_SIDE, 0) animated:NO];
        
        [mainScrollView setContentOffset:CGPointMake((currentPage+1)*DEVICE_BIGER_SIDE, 0) animated:NO];
        
    } completion:^(BOOL finished) {
        
        [self activateNewViecle];
        rightArrowButton.userInteractionEnabled = YES;
        isSideButtonUsed = NO;
    }];
    
    [self removeSwipeGesture];
}


- (void)addPoints:(int)points {
    totalScore += points;
    currentProgressUserObject.score += points;
    pointForVipScopre += points;
    //NSLog(@"kParam007 %d",kParam007);
    //NSLog(@"kParam015 %d",kParam015);
    while(pointForVipScopre >= kParam007){
        //vipScopre += kParam015;
       
        currentProgressUserObject.vipPasses += kParam015;
        pointForVipScopre -= kParam007;
    }
    [self updateLabels];
}
- (void)updateLabels{
    //scoreLabel.text = [NSString stringWithFormat:@"%d",totalScore];
    scoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
    vipScoreLabel.text = [NSString stringWithFormat:@"%d",currentProgressUserObject.vipPasses];
}

- (void)enableScrolling:(id)sender{
    mainScrollView.scrollEnabled = YES;
}

- (void)disableScrolling:(id)sender{
    mainScrollView.scrollEnabled = NO;
}


- (void)enablePageControlButtons{
    
    
}

- (void)disablePageControlButtons{
    
    
}


#pragma mark - ScrollView Delegate methods



- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    CGFloat xOffset = mainScrollView.contentOffset.x;
    NSInteger currentPage = xOffset/DEVICE_BIGER_SIDE;
    if (currentPage+1 == vehiclesArray.count) {
        [self gotoSummaryScreen];
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat xOffset = mainScrollView.contentOffset.x;
    NSInteger currentPage = xOffset/DEVICE_BIGER_SIDE;
    if(self.isLoadingFinished /*&& vehiclesArray && vehiclesArray.count > 0*/){
        if (currentPage == vehiclesArray.count) {
            
            [self gotoSummaryScreen];
        }
    }
    
    if (scrollView == mainScrollView) {
        
        if (isSideButtonUsed) {
            return;
        }
        NSInteger page;
        CGFloat pageWidth = DEVICE_BIGER_SIDE;

        page = floor((mainScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        
        if (lastPage!=page) {
            
            if (vehiclesArray.count%nComponentsProgressBar != 0) {
                
                if (page > vehiclesArray.count - stepsBetweenPoints - vehiclesArray.count%nComponentsProgressBar && currentPage<vehiclesArray.count) {
                    
                    stepForProgressBar = percentBetweenPoints/(stepsBetweenPoints+vehiclesArray.count%nComponentsProgressBar);
                }
                else if (page <= vehiclesArray.count - stepsBetweenPoints - vehiclesArray.count%nComponentsProgressBar){
                    
                    stepForProgressBar = percentBetweenPoints/stepsBetweenPoints;
                }
            }
            
            
            float addProgressPercent = 0.0;
            
            if(lastPage<page){
                addProgressPercent = stepForProgressBar;
            }else{
                addProgressPercent = -stepForProgressBar;
            }
            [currentLessonProgress addProgres:addProgressPercent];
            
            if (page>=0 && page<[vehiclesArray count]) {
                BaseViewVehicle *currentVehicle = [vehiclesArray objectAtIndex:page];
                if (![currentVehicle vehicleIsAvalible]) {
                    [self showConversionPoint];
                }
            }
            
            
            if (lastPage>=0 && lastPage < [vehiclesArray count]) {
                
                BaseViewVehicle *lastVehicle = [vehiclesArray objectAtIndex:lastPage];
                if([lastVehicle isKindOfClass: [BaseViewVehicle class]]){
                    [lastVehicle recordVehicleData];
                    currentProgressLessonObject.currentVehicle = (int)lastPage;
                    [DATA_MANAGER saveData];
                }else{
                    NSLog(@"Krashuvashe");
                }
            }
            
            lastPage=page;
        }
        else {
            return;
        }
    
        currentPage = page;
    }
}



- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    //NSLog(@"scrollViewDidEndScrollingAnimation");
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self activateNewViecle];
    
}


-(void)activateNewViecle {
    /*initialConstantForBottomConstrains = [mainScrollView height]+20 ;
     if(instructionViewBottomConstrains.constant == 0){
     //close
     instructionViewBottomConstrains.constant = initialConstantForBottomConstrains;
     instructionViewTopConstrains.constant = -initialConstantForBottomConstrains;
     [UIView animateWithDuration:0.4f animations:^{
     [self.view layoutIfNeeded];
     }];
     }*/
    
    CGFloat xOffset = mainScrollView.contentOffset.x;
    NSInteger currentPage = xOffset/DEVICE_BIGER_SIDE;
    
    if(lastCurrentPage>=0){
        if(lastCurrentPage < [vehiclesArray count]){
            if(lastCurrentPage  != currentPage ){
                //if you don't chek this will be reset Vehicle on small movement
                BaseViewVehicle *lastVehicle = [vehiclesArray objectAtIndex:lastCurrentPage];
                if([lastVehicle isKindOfClass: [BaseViewVehicle class]]){
                    [lastVehicle deactivateVehicle];
                    currentProgressLessonObject.currentVehicle = (int)lastCurrentPage;
                    [DATA_MANAGER saveData];
                }else{
                    NSLog(@"potencijalen krash");
                }
            }
        }
    }
    
    
    NSInteger nextPage = currentPage+1;
    
    if (currentPage == 0) {
        leftArrowButton.hidden = YES;
    }
    else {
        leftArrowButton.hidden = NO;
    }
    if (currentPage == vehiclesArray.count) {
        rightArrowButton.hidden = YES;
    }
    else {
        rightArrowButton.hidden = NO;
    }
    
    if(currentPage < [vehiclesArray count]){
        BaseViewVehicle *currentVehicle = [vehiclesArray objectAtIndex:currentPage];
        if ([currentVehicle isKindOfClass:[VideoVehicle class]]) {
            [self addActivityIndicator];
        }
        if ([currentVehicle isKindOfClass:[FillMissingWordsVehicle class]]) {
            // mainScrollView.scrollEnabled = NO;
        }else{
            mainScrollView.scrollEnabled = YES;
        }
        if([currentVehicle vehicleIsAvalible]){
            [currentVehicle activateVehicle];
        }else{
            [self showConversionPoint];
        }
        
        
        currentProgressLessonObject.currentVehicle = (int)currentPage;
        if((int)currentPage > currentProgressLessonObject.maxVehicle){
            currentProgressLessonObject.maxVehicle = (int)currentPage;
        }
        
        ProgressVehicleObject *tempProgressVehicle =  [currentProgressLessonObject getCurrentProgressVehicleObject];
        tempProgressVehicle.achived = YES;
        
        [DATA_MANAGER saveData];
    }
    if(nextPage < [vehiclesArray count]){
        BaseViewVehicle *nextVehicle = [vehiclesArray objectAtIndex:nextPage];
        if ([nextVehicle isKindOfClass:[VideoVehicle class]]) {
            VideoVehicle *tempVehicle = (VideoVehicle*)nextVehicle;
            [self preloadVideoWithUrl:tempVehicle.videoUrlString vehicleOrder:nextPage];
        }
    }
    
    lastCurrentPage = currentPage;
}

- (void)activateNewViecle2 {
    
    CGFloat xOffset = mainScrollView.contentOffset.x;
    NSInteger currentPage = xOffset/DEVICE_BIGER_SIDE;
    NSInteger nextPage = currentPage;
    if(currentPage < [vehiclesArray count]){
        BaseViewVehicle *currentVehicle = [vehiclesArray objectAtIndex:currentPage];
        if ([currentVehicle isKindOfClass:[VideoVehicle class]]) {
            [self addActivityIndicator];
        }
        if ([currentVehicle isKindOfClass:[FillMissingWordsVehicle class]]) {
            //mainScrollView.scrollEnabled = NO;
        }
        if([currentVehicle vehicleIsAvalible]){
            [currentVehicle activateVehicle];
        }else{
            [self showConversionPoint];
        }
        //        NSLog(@"-------------------------------------------------------------->>>>>> ACTIVIRAM - %ld", currentPage);
        
        
        currentProgressLessonObject.currentVehicle = (int)currentPage;
        if((int)currentPage > currentProgressLessonObject.maxVehicle){
            currentProgressLessonObject.maxVehicle = (int)currentPage;
        }
        
        ProgressVehicleObject *tempProgressVehicle =  [currentProgressLessonObject getCurrentProgressVehicleObject];
        tempProgressVehicle.achived = YES;
        
        [DATA_MANAGER saveData];
    }
    if (nextPage < [vehiclesArray count]){
        BaseViewVehicle *nextVehicle = [vehiclesArray objectAtIndex:nextPage];
        
        if ([nextVehicle isKindOfClass:[VideoVehicle class]]) {
            
            VideoVehicle *tempVehicle = (VideoVehicle*)nextVehicle;
            [self preloadVideoWithUrl:tempVehicle.videoUrlString vehicleOrder:nextPage];
            
        }
    }
    
//    if (vehiclesArray.count%nComponentsProgressBar != 0) {
//
//        if (currentPage >= vehiclesArray.count - stepsBetweenPoints - vehiclesArray.count%nComponentsProgressBar && currentPage<vehiclesArray.count) {
//
//            stepForProgressBar = percentBetweenPoints/(stepsBetweenPoints+vehiclesArray.count%nComponentsProgressBar);
//        }
//        else if (currentPage < vehiclesArray.count - (stepsBetweenPoints + vehiclesArray.count%nComponentsProgressBar)){
//
//            stepForProgressBar = percentBetweenPoints/stepsBetweenPoints;
//
//        }
//    }
//
//
//    float addProgressPercent = 0.0;
//
//    if(lastCurrentPage<currentPage){
//        addProgressPercent = stepForProgressBar;
//    }else{
//        addProgressPercent = -stepForProgressBar;
//    }
//    [currentLessonProgress addProgres:addProgressPercent];
    lastCurrentPage = currentPage;
}


- (void)setQuestionBarText:(NSString *)newText {
    
    //New
    //    NSDictionary *font = [FontManager takeFontSizeForQuestionArea:newText];
    //    [questionBarLabel setFont:[UIFont systemFontOfSize:[[font objectForKey:@"size"] floatValue]]];
    //    [questionBarLabel setNumberOfLines:[[font objectForKey:@"line"] integerValue]];
    
    questionBarLabel.text = newText;
    questionBarVocabView.hidden = YES;
    questionBarLabel.hidden = NO;
    
    //    [questionBarView setNeedsUpdateConstraints];
    //    [questionBarView updateConstraints];
    
    [self setQuestionBarTextWithVocab:newText];
    
}

- (void)setQuestionBarTextWithVocab:(NSString *)newText {
    
    [questionBarVocabView cleanControl];
    
    NSDictionary *font = [FontManager takeFontSizeForQuestionArea:newText];
    
    [questionBarVocabView buildTextViewFromString:newText withFont:[font objectForKey:@"size"]];
    [questionBarVocabView createUX];
    
    questionBarVocabView.hidden = NO;
    questionBarLabel.hidden = YES;
    
}


#pragma mark - Back to home page

- (IBAction)goToHomePage:(id)sender {
    
    [self deactivateCurrentVehicle];
    
    [AUDIO_MANAGER stopPlaying];
    
    [self updateProgressOnExit];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"openLessonSession"];
    [defaults synchronize];
    
   // [APP_DELEGATE buildHomePageStack];
     [self updateDataForVip:NO];
}

//for applicationDidEnterBackground
- (void)pauseCurrentVehicle {
    if([vehiclesArray count] > lastCurrentPage ){
        BaseViewVehicle *lastVehicle = [vehiclesArray objectAtIndex:lastCurrentPage];
        if ([lastVehicle isKindOfClass:[SpellingVehicle class]]) {
            
        }else{
            [lastVehicle deactivateVehicle];
        }
    }
}

- (void)deactivateCurrentVehicle {
    if([vehiclesArray count] > lastCurrentPage ){
        BaseViewVehicle *lastVehicle = [vehiclesArray objectAtIndex:lastCurrentPage];
        [lastVehicle deactivateVehicle];
    }
}

- (void)backFromConversionScreen {
    isBackFromConversion = YES;
    haveConversion = NO;
    BaseViewVehicle *currentVehicle = [vehiclesArray objectAtIndex:currentProgressLessonObject.currentVehicle];
    
    if ([currentVehicle isKindOfClass:[VideoVehicle class]]) {
        
        [self playVideo];
    }
    
//    [currentVehicle activateVehicle];
}

-(void)updateProgressOnExit{
    NSLog(@"currentProgressLessonObject.scoreForLesson %d",currentProgressLessonObject.scoreForLesson);
    currentProgressLessonObject.scoreForLesson = totalScore;
    NSLog(@"currentProgressLessonObject.scoreForLesson %d",currentProgressLessonObject.scoreForLesson);
    [DATA_MANAGER saveData];
}
- (void)showCorrectSpellingFeedback:(id)sender {
    
    SpellingVehicle *spellVehicle = (SpellingVehicle *)sender;
    SpellingTextField *spellTextField = (SpellingTextField *)[spellVehicle currentTextField];
    
    NSInteger tag = spellTextField.tag+1;
    [spellVehicle goToNextTextField:@(tag)];
    
}

-(void)gotoSummaryScreen{
    if(haveSummary)
        return;
    [self updateProgressOnExit];
    haveSummary = YES;
    rightArrowButton.userInteractionEnabled = NO;
    rightArrowButton.hidden = YES;
    
    currentProgressLevelObject.currentUnit = (int)self.lessonNumber+1;
    NSArray *allLessonObject = [DATA_MANAGER takeAllLessonObject];
    if ((currentProgressLevelObject.currentUnit) >  [allLessonObject count]){
        currentProgressLevelObject.currentUnit = (int32_t)[allLessonObject count];
    }
    if ((currentProgressLevelObject.currentUnit) > currentProgressLevelObject.maxUnit){
        currentProgressLevelObject.maxUnit = currentProgressLevelObject.currentUnit;
    }
    [DATA_MANAGER saveData];
    
    LessonSummaryViewController *viewController = [LessonSummaryViewController new];
    viewController.currentLessonObject = currentLessonObject;
    viewController.vehiclePassCorrect = vehiclePassCorrect;
    viewController.vehicleNumber = vehicleWithPoints;//vehiclesArray.count;
    [self.navigationController pushViewController:viewController animated:YES];
    return;

}
- (void)showConversionPoint{
    
    if(haveConversion)
        return;
    
    haveConversion = YES;
    if([USER_MANAGER getDateExpirationForActiveUser])
    {
        PackageViewController *viewController = [PackageViewController new];
        [self.navigationController pushViewController:viewController animated:YES];
        
    }
    else
    {
        BaseViewVehicle *currentVehicle = [vehiclesArray objectAtIndex:currentProgressLessonObject.currentVehicle];
        
        if ([currentVehicle isKindOfClass:[VideoVehicle class]]) {
            
            if (player) {
                [player pause];
            }
        }
        
        SelectCountryViewController *viewController = [SelectCountryViewController new];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

- (void)updateDataForVip:(BOOL)showVipScreen{
    [activityIndicatorLoading startAnimating];
    NSDictionary *dictHeaders = @{
                                  kProgressLessonOrder : [NSString stringWithFormat:@"%d", [currentLessonObject.lesson_order intValue]] ,
                                  kProgressRankOrder : [NSString stringWithFormat:@"%d", [currentLessonObject.rankParentNumber intValue]],
                                  kProgressLevelOrder : [NSString stringWithFormat:@"%d", currentProgressUserObject.currentLevel],
                                  kProgressLessonCompleted :@"false",
                                  kProgressStarNumber : @"0",
                                  kProgressScore : [NSString stringWithFormat:@"%d", currentProgressUserObject.score]
                                  
                                  };
    [self addBaseActivityIndicator];
    [self chekInternet];
    
    __weak LessonViewController *weakSelf = self;
    [REQUEST_MANAGER getDataFor:kRequestSaveUserProgressionAPICall headers:dictHeaders withSuccessCallBack:^(NSDictionary *responseObject) {
        NSDictionary *responseData = [responseObject objectForKey:kApiObjectResponse];
        [weakSelf removeActivityIndicator];
        if(responseData){
            [USER_MANAGER setUserData:responseData];
        }
        if(showVipScreen){
            [weakSelf goToVip];
            //[APP_DELEGATE buildVipZoneStack];
        }else{
            [weakSelf goHome];
            //[APP_DELEGATE buildHomePageStack];
        }
    } andWithErrorCallBack:^(NSString *errorMessage) {
        [weakSelf removeActivityIndicator];
        //[activityIndicatorLoading stopAnimating];
    }];
}

-(void)goHome{
   [APP_DELEGATE buildHomePageStack];
}
-(void)goToVip{
    [APP_DELEGATE buildVipZoneStack];
}
#pragma mark Video Vehicle
- (void)preloadVideoWithUrl:(NSString*)videoUrl vehicleOrder:(NSInteger)vehicleOrder {
    
    return;
    NSLog(@"COMPARE \n \n \n \n%@\nvs\n%@", videoUrl, [preloadPlayer.contentURL absoluteString]);
    
    dispatch_async(
                   dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       //sleep(0.2);
                       
                       dispatch_async(dispatch_get_main_queue(), ^{
                           if(preloadPlayer && [[preloadPlayer.contentURL absoluteString] isEqualToString:videoUrl]){
                               
                               CMTime videoCurrentTime = [self getActiveVideoTime:vehicleOrder];
                               [preloadPlayer.moviePlayer seekToTime:videoCurrentTime];

                               return;
                           } else if (preloadPlayer) {
                               preloadPlayer = nil;
                           }
                           preloadPlayer = [[KSVideoPlayerView alloc] initWithFrame:self.view.frame
                                                                         contentURL:[NSURL URLWithString:videoUrl]];
                           
                           CMTime videoCurrentTime = [self getActiveVideoTime:vehicleOrder];
                           [preloadPlayer.moviePlayer seekToTime:videoCurrentTime];

                           
                       });
                   });
    
}

- (void)showVideoWithUrl:(NSString *)videoUrl and:(BOOL)automaticGoNext isFree:(BOOL)isFree freeLength:(NSInteger)length {
    
    if (haveSummary)
        return;
    if (haveConversion)
        return;
    [self.view insertSubview:currentLessonProgress aboveSubview:questionBarView];
    [self.view insertSubview:leftArrowButton aboveSubview:currentLessonProgress];
    [self.view insertSubview:rightArrowButton aboveSubview:leftArrowButton];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if(haveSummary)
            return;
        if(haveConversion)
            return;
        if (player) {
            
            player = preloadPlayer;
            player.isFreeVideo = isFree;
            player.freeVideoLength = length;

            [self removeActivityIndicator];
            [self.view insertSubview:player belowSubview:leftArrowButton];
            [player play];
            
            [self addSwipeGesture];
            [self vehiclePass:YES];
            player.delegate = self;
            player.automaticGoToNextVehicle = automaticGoNext;
            player.tintColor = [UIColor redColor];
            return;
            
        }
        
        if (preloadPlayer && [[preloadPlayer.contentURL absoluteString] isEqualToString:videoUrl]){
            player = preloadPlayer;
            [self removeActivityIndicator];
        }
        else {
            
            /*player = [[KSVideoPlayerView alloc] initWithFrame:self.view.frame
             contentURL:[NSURL URLWithString:videoUrl]];*/
            
            if(preloadPlayer){
                preloadPlayer = nil;
            }
            preloadPlayer = [[KSVideoPlayerView alloc] initWithFrame:self.view.frame
                                                          contentURL:[NSURL URLWithString:videoUrl]];
            preloadPlayer.isFreeVideo = isFree;
            preloadPlayer.freeVideoLength = length;
            player = preloadPlayer;
        }
        player.delegate = self;
        player.automaticGoToNextVehicle = automaticGoNext;
        //[self.view addSubview:player];
        [self.view insertSubview:player belowSubview:leftArrowButton];
        player.tintColor = [UIColor redColor];
        [player play];
        //[NSTimer scheduledTimerWithTimeInterval:3.3 target:self selector:@selector(playVideo) userInfo:nil repeats:NO];
        //[self addActivityIndicator];
        [self addSwipeGesture];
        //[self vehiclePass:YES];
    });
    
}

- (void)playVideo{
    if(player)
        [player play];
}

#pragma mark - Video vehicle helpers

- (void)saveActiveVideoData:(NSNumber *)instanceNumber time:(CMTime)currentTime {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *dataV = [defaults objectForKey:@"activeVideos"];
    NSMutableDictionary *activeVideos = [[NSKeyedUnarchiver unarchiveObjectWithData:dataV] mutableCopy];
    
    if (!activeVideos) {
        activeVideos = [NSMutableDictionary new];
    }
    
    NSMutableDictionary *dictCMTime = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       @(currentTime.value), @"cmValue",
                                       @(currentTime.timescale), @"cmScale",
                                       @([instanceNumber integerValue]), @"lastVideoInstanceOrder", nil];
    
    [activeVideos setObject:dictCMTime forKey:[NSString stringWithFormat:@"video_%ld", [instanceNumber integerValue]]];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:activeVideos];
    
    [defaults setObject:data forKey:@"activeVideos"];
    [defaults synchronize];
    
}

- (NSDictionary *)getActiveVideoData:(NSInteger)instanceNumber {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"activeVideos"];
    NSDictionary *dictVideos = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    //CMTime time;
    
    if (!dictVideos) {
        
        //time = CMTimeMake(-1, 1000);
        return nil;
    }
    else {
        
        if ([dictVideos objectForKey:[NSString stringWithFormat:@"video_%ld", instanceNumber]]) {
            
            NSDictionary *startValue = [dictVideos objectForKey:[NSString stringWithFormat:@"video_%ld", instanceNumber]];
            
            NSInteger cmValue = [[startValue valueForKey:@"cmValue"] longLongValue];
            NSInteger cmScale = [[startValue valueForKey:@"cmScale"] integerValue];
            CMTime realTime = CMTimeMake(cmValue, cmScale);
            
            NSValue *miValue = [NSValue value: &realTime
                                 withObjCType:@encode(CMTime)];
            NSMutableDictionary *dictVideoInfo = [NSMutableDictionary new];
            [dictVideoInfo setValue:miValue forKey:@"cmTime"];
            [dictVideoInfo setValue:@([[startValue objectForKey:@"lastVideoInstanceOrder"] integerValue]) forKey:@"lastVideoInstanceOrder"];
            
            
            return dictVideoInfo;
            
        }
    }
    
    //time = CMTimeMake(-1, 1000);
    
    return nil;
}


- (CMTime)getActiveVideoTime:(NSInteger)instanceNumber {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"activeVideos"];
    NSDictionary *dictVideos = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    CMTime time;
    
    if (!dictVideos) {
        
        time = CMTimeMake(-1, 1000);
        return time;
    }
    else {
        
        if ([dictVideos objectForKey:[NSString stringWithFormat:@"video_%ld", instanceNumber]]) {
            
            NSDictionary *startValue = [dictVideos objectForKey:[NSString stringWithFormat:@"video_%ld", instanceNumber]];
            
            NSInteger cmValue = [[startValue valueForKey:@"cmValue"] longLongValue];
            NSInteger cmScale = [[startValue valueForKey:@"cmScale"] integerValue];
            CMTime realTime = CMTimeMake(cmValue, cmScale);
            
            return realTime;
            
        }
    }
    
    time = CMTimeMake(-1, 1000);
    
    return time;
}

- (void)clearVideoDatainDefaults {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:@"activeVideos"];
    [defaults synchronize];
}


#pragma mark playerViewDelegate
-(void)hideArrows{
    CGFloat xOffset = mainScrollView.contentOffset.x;
    NSInteger currentPage = xOffset/DEVICE_BIGER_SIDE;
    //NSInteger nextPage = currentPage+1;
    if(currentPage < [vehiclesArray count]){
        BaseViewVehicle *currentVehicle = [vehiclesArray objectAtIndex:currentPage];
        if ([currentVehicle isKindOfClass:[VideoVehicle class]]) {
            
            [UIView animateWithDuration:0.3 animations:^{
                leftArrowButton.layer.opacity = 0;
                rightArrowButton.layer.opacity = 0;
            }];
        }
    }
}

-(void)showArrows{
    [UIView animateWithDuration:0.3 animations:^{
        leftArrowButton.layer.opacity = 1;
        rightArrowButton.layer.opacity = 1;
    }];
}

-(void)videoFinishedPlaying{
    [self removeActivityIndicator];
    [player stop];
    [player removeFromSuperview];
    player = nil;
    [self removeSwipeGesture];
    [self gotoNextPage];
    
}
-(void)playerLoadVideo{
    [self removeActivityIndicator];
}

#pragma mark - Left swipe action

- (void)leftSwipeActionScorllContainer:(id)sender {
    
    //[self nextButtonAction:self.lmPagingControl withSpeed:0.5];
//    if(player){
//        [self videoFinishedPlaying];
//    }
    
    [self removeSwipeGesture];
    [self gotoNextPage];
}

- (void)rightSwipeActionScorllContainer:(id)sender {
    [self removeSwipeGesture];
    [self gotoPreviousPage];
    // [self previousButtonSelected:self.lmPagingControl withSpeed:0.5];
    
}

#pragma mark UIGestureRecognizerDelegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return true;
}

#pragma mark Interface functions
- (IBAction)openUserZone:(id)sender {
    
    [APP_DELEGATE buildUserZoneStack];
}

- (IBAction)openVipZone:(id)sender {
    
    [self deactivateCurrentVehicle];
    [AUDIO_MANAGER stopPlaying];
    [self updateProgressOnExit];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"openLessonSession"];
    [defaults synchronize];
    
    
    [self.view bringSubviewToFront:loadingView];
    loadingView.hidden = NO;
    [activityIndicatorLoading startAnimating];
    
    //donwload resoureces
    id vipCourseObject = [DATA_MANAGER getVipCourseObject];
    if(vipCourseObject){
        //donwload resoureces
        NSArray *dataArray = [DATA_MANAGER getAllVipCategory];
        
        [CACHE_MANAGER setDelegate:self];
        [CACHE_MANAGER startDownloadingAndCachingResources:dataArray];
    }else{
        [self addBaseActivityIndicator];
        [self chekInternet];
        [REQUEST_MANAGER getDataFor:kRequestGetVipDataAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
            [self removeActivityIndicator];
            NSDictionary *responseDictionary = [responseObject objectForKey:kApiObjectResponse];
            [DATA_MANAGER makeVipCategoryCourseData:responseDictionary];
            [DATA_MANAGER saveData];
            [self openVipZone:0];
        } andWithErrorCallBack:^(NSString *errorMessage) {
            [self removeActivityIndicator];
        }];
        //makeVipCategoryCourseData
    }
    
}

#pragma mark - Cache Manager Delegate
- (void)cacheManagerFinishedDownloading:(id)sender {
    
    NSLog(@"========= Images were downloaded =========");
     //[APP_DELEGATE buildVipZoneStack];
     [activityIndicatorLoading stopAnimating];
     loadingView.hidden = YES;
     [self updateDataForVip:YES];
}

- (IBAction)pressInstructionButton:(id)sender {
    initialConstantForBottomConstrains = [mainScrollView height]+20 ;
    if(instructionViewBottomConstrains.constant != 0){
        //open
        instructionViewBottomConstrains.constant = 0;
        instructionViewTopConstrains.constant = 0;
        [self.view insertSubview:leftArrowButton belowSubview:instructionView];
        [self.view insertSubview:rightArrowButton belowSubview:instructionView];
        [self.view insertSubview:currentLessonProgress belowSubview:instructionView];
        
    }else{
        //close
        instructionViewBottomConstrains.constant = initialConstantForBottomConstrains;
        instructionViewTopConstrains.constant = -initialConstantForBottomConstrains;
        float delayInSeconds = 0.5;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.view insertSubview:currentLessonProgress aboveSubview:questionBarView];
            [self.view insertSubview:leftArrowButton aboveSubview:currentLessonProgress];
            [self.view insertSubview:rightArrowButton aboveSubview:leftArrowButton];
        });
        
    }
    [UIView animateWithDuration:0.4f animations:^{
        [self.view layoutIfNeeded];
    }];
}
- (IBAction)leftButtonPress:(id)sender {
    
    leftArrowButton.userInteractionEnabled = NO;
    isSideButtonUsed = YES;
    [self gotoPreviousPage];
}
- (IBAction)rightButtonPress:(id)sender {
    
    rightArrowButton.userInteractionEnabled = NO;
    isSideButtonUsed = YES;
    [self gotoNextPage];
}

/// Test speed of download
///
/// Test the speed of a connection by downloading some predetermined resource. Alternatively, you could add the
/// URL of what to use for testing the connection as a parameter to this method.
///
/// @param timeout             The maximum amount of time for the request.
/// @param completionHandler   The block to be called when the request finishes (or times out).
///                            The error parameter to this closure indicates whether there was an error downloading
///                            the resource (other than timeout).
///
/// @note                      Note, the timeout parameter doesn't have to be enough to download the entire
///                            resource, but rather just sufficiently long enough to measure the speed of the download.

- (void)testDownloadSpeedWithTimout:(NSTimeInterval)timeout completionHandler:(nonnull void (^)(CGFloat megabytesPerSecond, NSError * _Nullable error))completionHandler {
    NSURL *url = [NSURL URLWithString:@"http://cdn.kantoo.com/Hollywood/Hollywood%20Demo%20-%20part1.mp4"];
    
    self.startTime = CFAbsoluteTimeGetCurrent();
    self.stopTime = self.startTime;
    self.bytesReceived = 0;
    self.speedTestCompletionHandler = completionHandler;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    configuration.timeoutIntervalForResource = timeout;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    [[session dataTaskWithURL:url] resume];
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data {
    self.bytesReceived += [data length];
    self.stopTime = CFAbsoluteTimeGetCurrent();
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    CFAbsoluteTime elapsed = self.stopTime - self.startTime;
    CGFloat speed = elapsed != 0 ? self.bytesReceived / (CFAbsoluteTimeGetCurrent() - self.startTime) / 1024.0 / 1024.0 : -1;
    
    // treat timeout as no error (as we're testing speed, not worried about whether we got entire resource or not
    
    if (error == nil || ([error.domain isEqualToString:NSURLErrorDomain] && error.code == NSURLErrorTimedOut)) {
        self.speedTestCompletionHandler(speed, nil);
    } else {
        self.speedTestCompletionHandler(speed, error);
    }
}



- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotate {
    
    return YES;
}

#pragma mark - Create Vehicle Objects

-(BaseViewVehicle*)createVehicleObjectSpelling:(BaseVehiclesObject*)vehicleData{
    
    SpellingVehicle *tempVehivleNew =  [[SpellingVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)];
    return tempVehivleNew;
}

-(BaseViewVehicle*)createVehicleObjectSwipe:(BaseVehiclesObject*)vehicleData{
    
    SwipeKircaVehicle *tempVehivleNew =  [[SwipeKircaVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)];
    return tempVehivleNew;
}

-(BaseViewVehicle*)createVehicleObjectClickToLearn:(BaseVehiclesObject*)vehicleData{
    
    ClickToLearnVehicle *tempVehivleNew =  [[ClickToLearnVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)];
    return tempVehivleNew;
}

-(BaseViewVehicle*)createVehicleObjectFillTheMissing:(BaseVehiclesObject*)vehicleData{
    
    FillMissingWordsVehicle *tempVehivleNew =  [[FillMissingWordsVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)];
    return tempVehivleNew;
}

//-(BaseViewVehicle*)createVehicleObjectMultipleImage:(BaseVehiclesObject*)vehicleData{

//    MultipleImageVehicle *tempVehivleNew =  [[MultipleChoiceText alloc] initWithFrame:CGRectMake([vehiclesArray count]*self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)];
//    return tempVehivleNew;
//}

-(BaseViewVehicle*)createVehicleObjectMultipleText:(BaseVehiclesObject*)vehicleData{
    
    MultipleChoiceText *tempVehivleNew =  [[MultipleChoiceText alloc] initWithFrame:CGRectMake([vehiclesArray count]*self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)];
   
    return tempVehivleNew;
}

-(BaseViewVehicle*)createVehicleObjectSwipeCarousel:(BaseVehiclesObject*)vehicleData{
    
    SwipeCarouselVehicle *tempVehivleNew =  [[SwipeCarouselVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)];
    return tempVehivleNew;
}

-(BaseViewVehicle*)createVehicleObjectVoiceRecognition:(BaseVehiclesObject*)vehicleData{
    
    VoiceRecoginitionVehicle *tempVehivleNew =  [[VoiceRecoginitionVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)];
    return tempVehivleNew;
}

-(BaseViewVehicle*)createVehicleObjectVideo:(BaseVehiclesObject*)vehicleData{
    
    VideoVehicle *tempVehivleNew =  [[VideoVehicle alloc] initWithFrame:CGRectMake([vehiclesArray count]*self.vehicleWidth, 0, self.vehicleWidth, self.vehicleHeight)];
    return tempVehivleNew;
}

#pragma mark - Video Player seconds

- (void)showConversionPointFromFreeVideo {
    if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled){
        return;
    }
    if (player) {
        
        [player pause];
    }
    [self showConversionPoint];
}

@end
