//
//  LanguagesCollectionViewCell.h
//  Hollywood
//
//  Created by Kiril Kiroski on 7/12/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguagesCollectionViewCell : UICollectionViewCell

- (void)setCellData:(id)lessonData;

@end
