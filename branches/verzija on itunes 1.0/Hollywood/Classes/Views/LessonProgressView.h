//
//  LessonProgressView.h
//  Hollywood
//
//  Created by Kiril Kiroski on 4/14/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JEProgressView.h"

@interface LessonProgressView : UIView

- (void)configureUI;
- (void)addProgres:(float)addValue;
- (void)changeProgressWithValue:(float)value;

@end
