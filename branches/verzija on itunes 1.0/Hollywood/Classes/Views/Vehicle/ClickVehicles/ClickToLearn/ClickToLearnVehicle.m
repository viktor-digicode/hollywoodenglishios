//
//  ClickToLearnVehicle.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 3/30/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "ClickToLearnVehicle.h"
#import "LMAudioManager.h"
#import "FontManager.h"
#import "InstanceAnswersImageObject+CoreDataClass.h"
#import "InstanceAnswersMultipleImageObject+CoreDataClass.h"

@implementation ClickToLearnVehicle
@synthesize imageViewOneImageAnswer;
@synthesize viewAnswerContent;
@synthesize constraintContainterHeight, constraintContainterWidth;
@synthesize isOrdered;
@synthesize isQuizVehicle;
@synthesize arrayCorrectAnswers;

- (void)awakeFromNib {
    
    [super awakeFromNib];
}

- (instancetype)initWithFrame:(CGRect)frame quizVehicleData:(id)data position:(NSInteger)position {
    
    if (self = [self initWithFrame:frame data:data isLearnVehicle:NO]) {
        
        isQuizVehicle = YES;
        self.nAttemptsLeft = 1;
        self.positionInQuiz = position;
    }
    
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame data:(id)data isLearnVehicle:(BOOL)type {
    
    if (self = [super initWithFrame:frame]) {
        
        isOrdered = YES;
        BOOL isOneImage = [[data objectForKey:@"isOneImageLayout"] boolValue];
        self.isOneImageLayout = isOneImage;
        
        UIView *vv = [self createContentView:CGRectMake(44, 20, frame.size.width-88, frame.size.height-40)];
        [vv setBackgroundColor:[UIColor clearColor]];
        
        [self addSubview:vv];
        
        self.dataClickToLearnInstance = data;
        self.afterAudioString = [data objectForKey:@"afterAudio"];
        self.isLearnVehicle = type;
        
        if (!self.isLearnVehicle) {
            
            NSArray *arrayAnswers = (NSArray *)[data objectForKey:@"answers"];
            
            if ([arrayAnswers count] >= 3) {
                self.nAttemptsLeft = [PARAMETER_MANAGER getParameterAttempts];
//                self.nAttemptsLeft = 2;
            }
            else {
                self.nAttemptsLeft = 1;
            }
            if ([self.dataVehicleObject.isOneTry integerValue] == 1) {
                
                self.nAttemptsLeft = 1;
            }

        }
        
        CGFloat minFontSize = [self getMinimumFontSize:[data objectForKey:@"answers"] isLearn:self.isLearnVehicle];
        
        if (isOneImage) {
            
            [self createOneImageLayout:data minFont:minFontSize];
        }
        else {
            
            [self createLayout:[data objectForKey:@"answers"] minFont:minFontSize];
        }
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame data:(id)data isLearnVehicle:(BOOL)type isOrdered:(BOOL)ordered {
    
    if (self = [super initWithFrame:frame]) {
        
        BOOL isOneImage = [[data objectForKey:@"isOneImageLayout"] boolValue];
        self.isOneImageLayout = isOneImage;
        
        isOrdered = ordered;
        if (!isOrdered) {
            arrayCorrectAnswers = [NSMutableArray new];
        }
        
        UIView *vv = [self createContentView:CGRectMake(44, 20, frame.size.width-88, frame.size.height-40)];
        [vv setBackgroundColor:[UIColor clearColor]];
        
        [self addSubview:vv];
        
        self.dataClickToLearnInstance = data;
        self.afterAudioString = [data objectForKey:@"afterAudio"];
        self.isLearnVehicle = type;
        
        if (!self.isLearnVehicle) {
            
            NSArray *arrayAnswers = (NSArray *)[self.dataClickToLearnInstance objectForKey:@"answers"];
            
            if ([arrayAnswers count] >= 3) {
                self.nAttemptsLeft = [PARAMETER_MANAGER getParameterAttempts];
            }
            else {
                self.nAttemptsLeft = 1;
            }
            if ([self.dataVehicleObject.isOneTry integerValue] == 1) {
                
                self.nAttemptsLeft = 1;
            }

        }
        
        CGFloat minFontSize = [self getMinimumFontSize:[data objectForKey:@"answers"] isLearn:self.isLearnVehicle];
        
        if (isOneImage) {
            
            [self createOneImageLayout:data minFont:minFontSize];
        }
        else {
            
            [self createLayout:[data objectForKey:@"answers"] minFont:minFontSize+6];
            
        }
        
    }
    
    return self;
}

- (void)setDataForVehicle:(BaseVehiclesObject*)tempVehiclesObject {
    
    [super setDataForVehicle:tempVehiclesObject];
    
    self.dataVehicleObject = tempVehiclesObject;
    
    self.afterAudioString = [self.dataVehicleObject questionAudioId];

    isOrdered = YES;
    
    NSSet *set1;
    BOOL isTheInstanceLearn;
    if ([tempVehiclesObject isKindOfClass:[VehicleObjectClickToLearn class]]) {
        
        set1 = [(VehicleObjectClickToLearn *)self.dataVehicleObject instanceImage];
        isTheInstanceLearn = YES;
    }
    else {
        
        set1 = [(VehicleObjectMultipleImage *)self.dataVehicleObject instanceAnswers];
        isTheInstanceLearn = NO;
    }
    
    NSArray *myArray2 = [set1 allObjects];
    
    BOOL isOneImage = NO;
    if([myArray2 count] == 1){
        isOneImage = YES;
    }
    self.isOneImageLayout = isOneImage;
    
    UIView *vv = [self createContentView:CGRectMake(44, 20, self.frame.size.width-88, self.frame.size.height-40)];
    [vv setBackgroundColor:[UIColor clearColor]];
    
    [self addSubview:vv];
    
   // self.dataClickToLearnInstance = data;
   // self.afterAudioString = [data objectForKey:@"afterAudio"];
   // self.isLearnVehicle = type;
    self.isLearnVehicle = isTheInstanceLearn;
    
    if (!self.isLearnVehicle) {
        
        NSArray *arrayAnswers = myArray2;//(NSArray *)[data objectForKey:@"answers"];
        
        if ([arrayAnswers count] >= 3) {

            self.nAttemptsLeft = [PARAMETER_MANAGER getParameterAttempts];
        }
        else {
            self.nAttemptsLeft = 1;
        }
    }
    if ([self.dataVehicleObject.isOneTry integerValue] == 1) {
        
        self.nAttemptsLeft = 1;
    }

    
    CGFloat minFontSize = 18;//[self getMinimumFontSize:[data objectForKey:@"answers"] isLearn:self.isLearnVehicle];
    
    if (isOneImage) {
        //nema da ima //[self createOneImageLayout:[myArray2 objectAtIndex:0] minFont:minFontSize];
    }
    else {
        [self createLayout:myArray2 minFont:minFontSize];
    }
    
    
}
#pragma mark - Helper method return min font size to use

- (CGFloat)getMinimumFontSize:(NSArray *)arrayAnswers isLearn:(BOOL)isLearn {
    
    NSDictionary *fontDictionary;
    CGFloat minFontSize = 100.0;
    CGFloat currentFontSize = 0.0;
    
    for (id data in arrayAnswers) {
        
        if (isLearn) {
            fontDictionary = [FontManager takeFontSize:[data objectForKey:@"text"] forVehicle:KAClickToLearnVehiclesType];
        }
        else {
            fontDictionary = [FontManager takeFontSize:[data objectForKey:@"text"] forVehicle:KAMultipleChoiceVehiclesType];
        }
        NSInteger numberOfLines = 0;
        
        if (fontDictionary) {
            
            if ([fontDictionary objectForKey:@"line"]) {
                numberOfLines = [[fontDictionary objectForKey:@"line"] integerValue];
            }
            
            if ([fontDictionary objectForKey:@"size"]) {
                currentFontSize = [[fontDictionary objectForKey:@"size"] floatValue];
            }
            
        }
        
        if (numberOfLines && currentFontSize) {
            
            if (currentFontSize < minFontSize) {
                minFontSize = currentFontSize;
            }
        }
    }
    
    
    return minFontSize;
}

- (NSInteger)getLabelLines:(NSArray *)arrayAnswers isLearn:(BOOL)isLearn {
    
    NSDictionary *fontDictionary;
    CGFloat minFontSize = 100.0;
    CGFloat currentFontSize = 0.0;
    
    NSInteger currentLines = 1;
    
    for (id data in arrayAnswers) {
        
        if (isLearn) {
            fontDictionary = [FontManager takeFontSize:[data objectForKey:@"text"] forVehicle:KAClickToLearnVehiclesType];
        }
        else {
            fontDictionary = [FontManager takeFontSize:[data objectForKey:@"text"] forVehicle:KAMultipleChoiceVehiclesType];
        }
        NSInteger numberOfLines = 0;
        
        if (fontDictionary) {
            
            if ([fontDictionary objectForKey:@"line"]) {
                numberOfLines = [[fontDictionary objectForKey:@"line"] integerValue];
            }
            
            if ([fontDictionary objectForKey:@"size"]) {
                currentFontSize = [[fontDictionary objectForKey:@"size"] floatValue];
            }
            
        }
        
        if (numberOfLines && currentFontSize) {
            
            if (currentFontSize < minFontSize) {
                minFontSize = currentFontSize;
                currentLines = numberOfLines;
            }
        }
    }
    
    
    return currentLines;
}


- (UIView *)createContentView:(CGRect)frame {
    
    if (viewAnswerContent == nil) {
        
        viewAnswerContent = [[UIView alloc] initWithFrame:frame];
        
        if (self.isOneImageLayout) {
            
            viewAnswerContent.backgroundColor = [UIColor blackColor];
            viewAnswerContent.layer.cornerRadius = 4.0;
            viewAnswerContent.layer.borderWidth = 1.0;
            viewAnswerContent.layer.borderColor = [UIColor whiteColor].CGColor;
            viewAnswerContent.layer.masksToBounds = YES;
        }
        else {
            viewAnswerContent.backgroundColor = [UIColor clearColor];
        }
        
    }
    
    return viewAnswerContent;
}



- (void)createLayout:(NSArray *)arrayAnswers minFont:(CGFloat)minFontSize {
    
    ClickImageView *clickImage;
    NSInteger order = 0;
    
    NSInteger numberOfAnswers = [arrayAnswers count];
    CGFloat answerWidth = (viewAnswerContent.frame.size.width-(numberOfAnswers-1)*15)/numberOfAnswers;
    
    NSMutableArray *arryRandomNumbers=[[NSMutableArray alloc]init];
    while (arryRandomNumbers.count<numberOfAnswers) {
        NSInteger randomNumber=arc4random()%numberOfAnswers;
        if (![arryRandomNumbers containsObject:[NSString stringWithFormat:@"%ld",randomNumber]])       {
            [arryRandomNumbers addObject:[NSString stringWithFormat:@"%ld",randomNumber]];
        }
        continue;
    }
    
    InstanceAnswersImageObject *dataClickImage = nil;
    NSInteger index;
    
    for (int i=0; i<numberOfAnswers; i++) {
        
        index = [[arryRandomNumbers objectAtIndex:i] integerValue];
        dataClickImage = [arrayAnswers objectAtIndex:index];
        
        clickImage = [[ClickImageView alloc] initWithFrame:CGRectMake(order*(answerWidth+15), 0, answerWidth, viewAnswerContent.height) data:dataClickImage type:self.isLearnVehicle isOneImage:NO withFontSize:minFontSize];
        clickImage.delegateClickImageView = self;
        clickImage.tag = order;
        
        [viewAnswerContent addSubview:clickImage];
        
        order++;
    }
    
    if (!isOrdered) {
        self.nCorrectAnswers = [arrayCorrectAnswers count];
    }
    else {
        self.nCorrectAnswers = 1;
    }
}

- (void)createOneImageLayout:(id)data minFont:(CGFloat)minFontSize {
    
    InstanceAnswersImageObject *dataClickImage = (InstanceAnswersImageObject*)data;
    
    imageViewOneImageAnswer = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, viewAnswerContent.frame.size.width-6, viewAnswerContent.frame.size.height-6)];
    [imageViewOneImageAnswer setImage:[UIImage imageNamed:[data objectForKey:@"bigImage"]]];
    [imageViewOneImageAnswer setContentMode:UIViewContentModeScaleAspectFill];
    [viewAnswerContent addSubview:imageViewOneImageAnswer];
    
    imageViewOneImageAnswer.layer.cornerRadius = 4.0;
    imageViewOneImageAnswer.layer.masksToBounds = YES;
    
    
    
    ClickImageView *clickImage;
    NSInteger order = 0;
    
    NSArray *arrayAnswers = [data objectForKey:@"answers"];
    
    CGFloat answerWidth = viewAnswerContent.frame.size.width/[arrayAnswers count];
    
    for (id dataClickImage in arrayAnswers) {
        
        clickImage = [[ClickImageView alloc] initWithFrame:CGRectMake(order*answerWidth, 0, answerWidth, viewAnswerContent.height) data:dataClickImage type:self.isLearnVehicle isOneImage:YES withFontSize:minFontSize];
        clickImage.delegateClickImageView = self;
        [clickImage setBackgroundColor:[UIColor clearColor]];
        [viewAnswerContent addSubview:clickImage];
        
        order++;
    }
    
    self.nCorrectAnswers = 1;
    
}

#pragma mark - ClickImageViewDelegate

- (void)answerWasClicked:(id)sender {
    
    //    NSLog(@"is Current - %d", self.isCurrent);
    //    NSLog(@"isCompleted - %d", self.isCompleted);
    
    if  (!self.isCurrent) {
        return;
    }
    if (self.isCompleted || self.nCorrectAnswers == 0) {
        return;
    }
    
    if ((self.nAttemptsLeft<=0 && !self.isLearnVehicle) || sender == self.currentAnswer) {
        return;
    }
    
    self.currentAnswer = (ClickImageView *)sender;
    
    if (!self.isLearnVehicle) {
        
        ClickImageView *clickImage = (ClickImageView *)sender;
        BOOL status = [[(InstanceAnswersMultipleImageObject *)clickImage.dataAnswer correctAnswer] boolValue];
        
        if (!status) {
            
            self.isAnsweredCorrect = NO;
            [self showIncorrectFeedback];
            [self.currentAnswer showFeedbackWithStatus];
            self.nAttemptsLeft--;
            
            if (self.nAttemptsLeft == 0) {
                //finish instance go to next
                //                NSLog(@"isCompleted set to YES 1");
                
                [ANALYTICS_MANAGER sendAnalyticsForEvent:kMCIScreen action:@"MCI_End" labelName:@"Correctness" value:@(0)];
                self.isCompleted = YES;
            }
            else {
                //hide after 2 seconds
                [self performSelector:@selector(hideAllFeedbacks) withObject:nil afterDelay:1.0];
                return;
            }
        }
        else {
            
            if (!isOrdered) {
                self.currentAnswer.answerIsClosed = YES;
                self.nCorrectAnswers--;
                
                if (self.nCorrectAnswers == 0) {
                    self.isCompleted = YES;
                    //                    NSLog(@"isCompleted set to YES 2");
                    [self showCorrectFeedback];
                }
                else {
                    [AUDIO_MANAGER playRightSound];
                }
                
            }
            else {
                
                self.isCompleted = YES;
                //                NSLog(@"isCompleted set to YES 3");
            }
            
            self.isAnsweredCorrect = YES;
            
            if (isOrdered) {
                [self showCorrectFeedback];
                self.nCorrectAnswers = 0;
            }
            
            [self.currentAnswer showFeedbackWithStatus];
            [ANALYTICS_MANAGER sendAnalyticsForEvent:kMCIScreen action:@"MCI_End" labelName:@"Correctness" value:@(1)];

        }
    }
    
    for (ClickImageView *answer in self.viewAnswerContent.subviews) {
        
        if (self.isCompleted) {
            break;
        }
        if ([answer isKindOfClass:[ClickImageView class]] && answer != self.currentAnswer) {
            
            if (!isOrdered && answer.answerIsClosed) {
                
            }
            else {
                [answer hideAnswer];
            }
        }
    }
}


- (void)hideAllFeedbacks {
    
    for (ClickImageView *answer in self.viewAnswerContent.subviews) {
        if (self.isCompleted) {
            break;
        }
        if ([answer isKindOfClass:[ClickImageView class]]) {
            
            if (!isOrdered && answer.answerIsClosed) {
                
            }
            else {
                [answer hideAnswer];
            }
        }
    }
    
    self.currentAnswer = nil;
}

- (void)selectCorrectAnswer {
    
    for (ClickImageView *answer in self.viewAnswerContent.subviews) {
        
        if ([answer isKindOfClass:[ClickImageView class]]) {
            
            if ([[(InstanceAnswersMultipleImageObject *)[answer dataAnswer] correctAnswer] boolValue]) {
                
                [answer showFeedbackWithStatus];
            }
            else {
                
                if (answer != self.currentAnswer) {
                    [answer hideAnswer];
                    
                }
            }
        }
        
    }
}


- (void)answerWasClicked:(id)sender changeImage:(NSString *)image {
    
    if ((self.nAttemptsLeft<=0 && !self.isLearnVehicle) || sender == self.currentAnswer) {
        return;
    }
    
    self.currentAnswer = (ClickImageView *)sender;
    self.currentAnswer.timesClicked++;

    for (ClickImageView *answer in self.viewAnswerContent.subviews) {
        
        if ([answer isKindOfClass:[ClickImageView class]] && answer != self.currentAnswer) {
            
            [answer hideAnswer];
        }
    }
    [imageViewOneImageAnswer setImage:[UIImage imageNamed:image]];
}

#pragma mark - Activate/Deactivate vehicle

- (void)activateVehicle {
    
    [super activateVehicle];
    
    if (self.isLearnVehicle) {
        
        [ANALYTICS_MANAGER sendAnalyticsForEvent:kClickToLearnScreen action:@"C2L_Start" additionalData:nil];
    }
    else {
        
        [ANALYTICS_MANAGER sendAnalyticsForEvent:kMCIScreen action:@"MCI_Start" additionalData:nil];
    }
    
    if ([self.dataClickToLearnInstance objectForKey:@"questionAudio"]) {
        [AUDIO_MANAGER playSoundFromAudioFile:[self.dataClickToLearnInstance objectForKey:@"questionAudio"]];
    }
    
    if ([self.dataClickToLearnInstance objectForKey:@"questionTextVocabs"]) {
        
        if ([[self.dataClickToLearnInstance objectForKey:@"questionTextVocabs"] boolValue]) {
            [self.vehicleDelegate setQuestionBarTextWithVocab:self.dataVehicleObject.questionText];
            
        }
        else {
            [self.vehicleDelegate setQuestionBarText:self.dataVehicleObject.questionText];
            
        }
    }
    else {
        [self.vehicleDelegate setQuestionBarText:self.dataVehicleObject.questionText];
    }
}

- (void)deactivateVehicle {
    
    [super deactivateVehicle];
    
    self.currentAnswer = nil;
    
    for (ClickImageView *answer in self.viewAnswerContent.subviews) {
        
        if ([answer isKindOfClass:[ClickImageView class]]) {
            
            [answer hideAnswer];
            answer.answerIsClosed = NO;
            
            if (self.isLearnVehicle) {
                
                [ANALYTICS_MANAGER sendAnalyticsForEvent:kClickToLearnScreen action:@"C2L_End" labelName:[NSString stringWithFormat:@"Answer%ld_ClicksNumber", answer.tag+1] value:@(answer.timesClicked)];
                
                answer.timesClicked = 0;
            }
        }
    }
    
    if (!self.isLearnVehicle) {
        
        NSSet *set1;

        if ([self.dataVehicleObject isKindOfClass:[VehicleObjectClickToLearn class]]) {
            
            set1 = [(VehicleObjectClickToLearn *)self.dataVehicleObject instanceImage];
        }
        else {
            
            set1 = [(VehicleObjectMultipleImage *)self.dataVehicleObject instanceAnswers];
        }

        //analytics check for skip
        if ((self.nAttemptsLeft == [PARAMETER_MANAGER getParameterAttempts] && !self.isAnsweredCorrect) ||
            ([self.dataVehicleObject.isOneTry integerValue] == 1 && self.nAttemptsLeft == 1 && !self.isAnsweredCorrect)) {
            //skipped
            [ANALYTICS_MANAGER sendAnalyticsForEvent:kMCIScreen action:@"MCI_End" labelName:@"Correctness" value:@(2)];
        }

        NSArray *arrayAnswers = [set1 allObjects];
        if ([arrayAnswers count] >= 3) {

            self.nAttemptsLeft = [PARAMETER_MANAGER getParameterAttempts];
        }
        else {
            self.nAttemptsLeft = 1;
        }
        
        if ([self.dataVehicleObject.isOneTry integerValue] == 1) {
            
            self.nAttemptsLeft = 1;
        }
        
        self.isCompleted = NO;
        //        NSLog(@"isCompleted set to NO 1");
        if (!isOrdered) {
            self.nCorrectAnswers = [arrayCorrectAnswers count];
        }
        else {
            self.nCorrectAnswers = 1;
        }
        
        [self makeNewAnswersLayout:arrayAnswers];
        
        
    }
    else {
        
        if (self.isOneImageLayout) {
            [imageViewOneImageAnswer setImage:[UIImage imageNamed:[self.dataClickToLearnInstance objectForKey:@"bigImage"]]];
        }
    }
    
}

#pragma mark - Make new random

- (void)makeNewAnswersLayout:(NSArray *)arrayAnswers {
    
    NSInteger numberOfAnswers = [arrayAnswers count];;
    CGFloat answerWidth = (viewAnswerContent.frame.size.width-(numberOfAnswers-1)*15)/numberOfAnswers;
    
    NSMutableArray *arryRandomNumbers=[[NSMutableArray alloc]init];
    while (arryRandomNumbers.count<numberOfAnswers) {
        NSInteger randomNumber=arc4random()%numberOfAnswers;
        if (![arryRandomNumbers containsObject:[NSString stringWithFormat:@"%ld",randomNumber]])       {
            [arryRandomNumbers addObject:[NSString stringWithFormat:@"%ld",randomNumber]];
        }
        continue;
    }
    
    
    //Create answers
    NSInteger index = 0;
    NSInteger order = 0;
    
    for (ClickImageView *clickableImage in viewAnswerContent.subviews) {
        
        if ([clickableImage isKindOfClass:[ClickImageView class]]) {
            
            order = [[arryRandomNumbers objectAtIndex:index] integerValue];
            clickableImage.frame = CGRectMake(order*(answerWidth+15), 0, clickableImage.width, clickableImage.height);
            clickableImage.timesClicked = 0;
            clickableImage.tag = order;
            
            index++;
        }
    }
    
}

- (void)finishVehicle{
    [super finishVehicle];
    
    if (self.isAnsweredCorrect) {
        
        if (self.nCorrectAnswers == 0) {
            [self.vehicleDelegate  vehiclePass:YES];
            self.vehiclePlayStatus = 2;
            self.isCompleted = YES;
            //          old way
            //          [self.vehicleDelegate vehicleWasFinished:self];
            
            if (self.afterAudioString) {
                self.vehicleState = KAPlayAfterAudioState;

                [AUDIO_MANAGER setDelegate:self];
                
                if ([AUDIO_MANAGER playRemoteAudioFile:[NSURL URLWithString:self.afterAudioString]]) {
                    
                }
                else {
                    NSLog(@"File not found");
                    self.isCompleted = YES;
                    [self.vehicleDelegate vehicleWasFinished:self];
                    [AUDIO_MANAGER setDelegate:nil];
                }

            
            }else{
                [self.vehicleDelegate  vehiclePass:NO];
                //self.vehiclePlayStatus = 1;
                self.isCompleted = YES;
                [self.vehicleDelegate vehicleWasFinished:self];
                [AUDIO_MANAGER setDelegate:nil];
            }
            
        }
    }
    else if (self.nAttemptsLeft == 0) {
        
        [self selectCorrectAnswer];
        self.isCompleted = YES;
        
        if(self.afterAudioString){
            self.vehicleState = KAPlayAfterAudioState;

            [AUDIO_MANAGER setDelegate:self];
            
            if ([AUDIO_MANAGER playRemoteAudioFile:[NSURL URLWithString:self.afterAudioString]]) {
                
            }
            else {
                NSLog(@"File not found");
                self.isCompleted = YES;
                [self.vehicleDelegate vehicleWasFinished:self];
                [AUDIO_MANAGER setDelegate:nil];
            }

        
        }else{
            self.isCompleted = YES;
            [self performSelector:@selector(callFinishAfterDelay) withObject:nil afterDelay:1.0];
            [AUDIO_MANAGER setDelegate:nil];
        }
        
        //        Old way
        //        [self performSelector:@selector(callFinishAfterDelay) withObject:nil afterDelay:1.0];
        
    }
    
}

- (void)callFinishAfterDelay {
    
    [self.vehicleDelegate vehicleWasFinished:self];
}

#pragma mark - AVWrapperDelegate

- (void)playbackComplete {
    
    if(self.vehicleState == KAPlayBeforeAudioState){
        
    }else if(self.vehicleState == KAPlayAfterAudioState){
        self.vehicleState = KADefaultState;
        self.isCompleted = YES;
        [self.vehicleDelegate vehicleWasFinished:self];
        //[self.vehicleDelegate setQuestionBarText:@""];
        [AUDIO_MANAGER setDelegate:nil];
    }
}

@end
