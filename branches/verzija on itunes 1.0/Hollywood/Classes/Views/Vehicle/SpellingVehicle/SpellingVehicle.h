//
//  SpellingVehicle.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 4/8/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "BaseViewVehicle.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "VehicleObjectSpelling+CoreDataClass.h"

@interface SpellingVehicle : BaseViewVehicle<UITextFieldDelegate, LMAudioManagerDelegate> {
    
    float xPosition;
    float yPosition;
    NSInteger currentTag;
    
    NSInteger numberOfLettersToFill;
    
    NSInteger totalPoints,incorectPoint;
}

@property (nonatomic, strong) id vehicleData;
@property (nonatomic, strong) VehicleObjectSpelling *dataVehicleObject;

@property (nonatomic, strong) TPKeyboardAvoidingScrollView *scrollKeyboardAvoidingContainer;

@property (nonatomic, strong) UIView *viewAnswerContainer;
@property (nonatomic, strong) UIView *viewQuestionContainer;

@property (nonatomic, strong) UIImageView *imageViewQuestion;


@property (nonnull, strong) SpellingTextField *currentTextField;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data;
- (void)goToNextTextField:(id)sender;

- (void)setCorrectFeedbackOnQuestionView:(NSInteger)status;

@end
