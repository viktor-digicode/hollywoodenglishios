//
//  SwipeVehicleView.m
//  NavigationHollywood
//
//  Created by Dimitar Shopovski on 3/21/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import "SwipeVehicleView.h"

@implementation SwipeVehicleView

- (instancetype)initWithFrame:(CGRect)frame data:(id)data {
    
    if (self = [super initWithFrame:frame]) {
        
        _vehicleData = data;
        
    }
    
    return self;
}

- (void)drawRect:(CGRect)rect {
    
    swipeDestinationViewLeft = [[SwipeDestinationView alloc] initWithFrame:CGRectMake(0, 0, 70, 200)];
    [swipeDestinationViewLeft setBackgroundColor:[UIColor redColor]];
    [self addSubview:swipeDestinationViewLeft];
    
    swipeDestinationViewRight = [[SwipeDestinationView alloc] initWithFrame:CGRectMake(rect.size.width-70, 0, 70, 200)];
    [swipeDestinationViewRight setBackgroundColor:[UIColor redColor]];
    [self addSubview:swipeDestinationViewRight];
    
    NSDictionary *dictMovingView = [_vehicleData objectForKey:@"movingViewData"];
    
    swipeMovingView = [[SwipeMovingView alloc] initWithFrame:CGRectMake(0, 0, 70, 200) data:dictMovingView];
    swipeMovingView.center = CGPointMake(self.frame.size.width/2, swipeMovingView.center.y);
    swipeMovingView.swipeViewDelegate = self;
    [swipeMovingView setBackgroundColor:[UIColor greenColor]];
    [self addSubview:swipeMovingView];
    
    [swipeMovingView setNeedsDisplay];


}

#pragma mark - SwipeMovingView Delegate implementation

- (void)leftSwipeDetected:(id)sender {
    
    NSLog(@"Left swipe was detected");
    
    if (self.isCompleted) {
        return;
    }
    
    [UIView animateWithDuration:1.0 animations:^{
       
        swipeMovingView.center = swipeDestinationViewLeft.center;
        
    } completion:^(BOOL finished) {
     
        self.isCompleted = YES;
        [self.vehicleDelegate vehicleWasFinished:self];
        
    }];
    
}

- (void)rightSwipeDetected:(id)sender {
    
    NSLog(@"Right swipe was detected");
    
    if (self.isCompleted) {
        return;
    }
    
    [UIView animateWithDuration:1.0 animations:^{
        
        swipeMovingView.center = swipeDestinationViewRight.center;
        
    } completion:^(BOOL finished) {
        
        self.isCompleted = YES;
        [self.vehicleDelegate vehicleWasFinished:self];
        
    }];
}

@end
