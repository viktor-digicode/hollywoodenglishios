//
//  PlayerView.m
//  X4 Video Player
//
//  Created by Hemkaran Raghav on 10/4/13.
//  Copyright (c) 2013 Mahesh Gera. All rights reserved.
//

#import "KSVideoPlayerView.h"
//#import <KeepLayout/KeepLayout.h>

@implementation KSVideoPlayerView
{
    id playbackObserver;
    AVPlayerLayer *playerLayer;
    BOOL viewIsShowing;
    UIButton *doneButton;
    BOOL playerFailed;
}

-(id)initWithFrame:(CGRect)frame playerItem:(AVPlayerItem*)playerItem
{
    self = [super initWithFrame:frame];
    if (self) {
        self.moviePlayer = [AVPlayer playerWithPlayerItem:playerItem];
        playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.moviePlayer];
        [playerLayer setFrame:frame];
        [self.moviePlayer seekToTime:kCMTimeZero];
        [self.layer addSublayer:playerLayer];
        self.contentURL = nil;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerFinishedPlaying) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        
        
        [playerItem addObserver:self forKeyPath:@"status" options:0 context:nil];
        [playerItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:0 context:nil];
        
        [self initializePlayer:frame];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame contentURL:(NSURL*)contentURL
{
    self = [super initWithFrame:frame];
    if (self) {
        
        AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:contentURL];
        self.moviePlayer = [AVPlayer playerWithPlayerItem:playerItem];
        playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.moviePlayer];
        [playerLayer setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self.moviePlayer seekToTime:kCMTimeZero];
        [self.layer addSublayer:playerLayer];
        self.contentURL = contentURL;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerFinishedPlaying) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        
        //sometime crash if you not remove observers
        [playerItem addObserver:self forKeyPath:@"status" options:0 context:nil];
        [playerItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:0 context:nil];
        
        ///////new
//        CMTime interval = CMTimeMakeWithSeconds(1.0, NSEC_PER_SEC); // 1 second
//        self.playbackTimeObserver =
//        [self.moviePlayer addPeriodicTimeObserverForInterval:interval
//                                                  queue:NULL usingBlock:^(CMTime time) {
//                                                      // update slider value here...
//                                                      
//                                                      NSLog(@"Count timmmemememememe ------ %lld", time.value/time.timescale);
//                                                      if ([self.delegate respondsToSelector:@selector(playerSecondsCount:seconds:)])
//                                                          [self.delegate playerSecondsCount:self seconds:time.value/time.timescale];
//                                                      
//                                                  }];
        
        [self initializePlayer:frame];
    }
    return self;
}

-(void)loadNewUrl:(NSURL*)contentURL {
    [playerLayer removeFromSuperlayer];
    playerLayer = nil;
    
    [self stop];
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:contentURL];
    self.moviePlayer = [AVPlayer playerWithPlayerItem:playerItem];
    playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.moviePlayer];
    [playerLayer setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];

    [self.layer addSublayer:playerLayer];
    self.contentURL = contentURL;
    [self play];
    
}
-(void) setFrame:(CGRect)frame
{
    //CGRect playerLayerFrame =  CGRectMake(0, 0,frame.size.width, frame.size.height);
    [super setFrame:frame];
    [playerLayer setFrame:frame];
}

-(void) setupConstraints
{
    
    [self.playerHudBottom autoSetDimension:ALDimensionHeight toSize:60.0];
    [self.playerHudBottom autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0.0f];
    [self.playerHudBottom autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0.0f];
    [self.playerHudBottom autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0.0f];
    
    [self.playerHudBottomBGView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0.0f];
    [self.playerHudBottomBGView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0.0f];
    [self.playerHudBottomBGView autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0.0f];
    [self.playerHudBottomBGView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0.0f];
    
    [self.progressBar autoSetDimension:ALDimensionHeight toSize:50.0];
    [self.progressBar autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0.0f];
    [self.progressBar autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:50.0f];
    [self.progressBar autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:65.0f];
    
    [self.playPauseButton autoSetDimension:ALDimensionHeight toSize:35.0];
    [self.playPauseButton autoSetDimension:ALDimensionWidth toSize:35.0];
    [self.playPauseButton autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:5.0f];
    [self.playPauseButton autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:5.0f];
    
    [self.playBackTotalTime autoSetDimension:ALDimensionHeight toSize:50.0];
    [self.playBackTotalTime autoSetDimension:ALDimensionWidth toSize:50.0];
    [self.playBackTotalTime autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0.0f];
    [self.playBackTotalTime autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:5.0f];
    
    
}

-(void)initializePlayer:(CGRect)frame
{
    int frameWidth =  frame.size.width;
    int frameHeight = frame.size.height;
    
    self.backgroundColor = [UIColor blackColor];
    viewIsShowing =  NO;
    
    [self.layer setMasksToBounds:YES];
    
    self.playerHudBottom = [[UIView alloc] init];
    self.playerHudBottom.frame = CGRectMake(0, frameHeight-25, frameWidth, 25);
    [self.playerHudBottom setBackgroundColor:[UIColor clearColor]];
    [self addSubview:self.playerHudBottom];
    
    self.playerHudBottomBGView = [[UIView alloc] init];
    self.playerHudBottomBGView.frame = CGRectMake(0, 0, frameWidth, 48*frameHeight/160);
    self.playerHudBottomBGView.backgroundColor = [UIColor blackColor];
    
    // Create the colors for our gradient.
    UIColor *transparent = [UIColor colorWithWhite:1.0f alpha:0.f];
    UIColor *opaque = [UIColor colorWithWhite:1.0f alpha:1.0f];
    
    // Create a masklayer.
    CALayer *maskLayer = [[CALayer alloc]init];
    maskLayer.frame = self.playerHudBottomBGView.bounds;
    CAGradientLayer *gradientLayer = [[CAGradientLayer alloc]init];
    gradientLayer.frame = CGRectMake(0,0,self.playerHudBottomBGView.bounds.size.width, self.playerHudBottomBGView.bounds.size.height);
    gradientLayer.colors = @[(id)transparent.CGColor, (id)transparent.CGColor, (id)opaque.CGColor, (id)opaque.CGColor];
    gradientLayer.locations = @[@0.0f, @0.09f, @0.8f, @1.0f];
    
    // Add the mask.
    [maskLayer addSublayer:gradientLayer];
    self.playerHudBottomBGView.layer.mask = maskLayer;
    [self.playerHudBottom addSubview:self.playerHudBottomBGView];
    
    //DOne Button
    doneButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    doneButton.frame = CGRectMake(frameWidth - 50, 5, 44, 44);
    [doneButton addTarget:self action:@selector(pressDone) forControlEvents:UIControlEventTouchUpInside];
    [doneButton setTitle:@"Done" forState:UIControlStateSelected];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton setTintColor:[UIColor blackColor]];
    doneButton.layer.opacity = 0;
    [self addSubview:doneButton];
    
    //Play Pause Button
    self.playPauseButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    //self.playPauseButton.frame = CGRectMake(5*frameWidth/240, 6*frameHeight/160, 16*frameWidth/240, 16*frameHeight/160);
    self.playPauseButton.frame = CGRectMake(5, frameHeight-45, 35, 35);
    [self.playPauseButton addTarget:self action:@selector(playButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.playPauseButton setSelected:NO];
    [self.playPauseButton setBackgroundImage:[UIImage imageNamed:@"videoPause"] forState:UIControlStateSelected];
    [self.playPauseButton setBackgroundImage:[UIImage imageNamed:@"videoPlay"] forState:UIControlStateNormal];
    [self.playPauseButton setTintColor:[UIColor clearColor]];
    self.playPauseButton.layer.opacity = 0;
    [self addSubview:self.playPauseButton];
    
    //Seek Time Progress Bar
    self.progressBar = [[UISlider alloc] init];
    self.progressBar.tintColor = APP_GOLD_COLOR;
    self.progressBar.frame = CGRectMake(50, -7.5, frameWidth-100, 15);
    [self.progressBar addTarget:self action:@selector(progressBarChanged:) forControlEvents:UIControlEventValueChanged];
    [self.progressBar addTarget:self action:@selector(proressBarChangeEnded:) forControlEvents:UIControlEventTouchUpInside];
    [self.progressBar setThumbImage:[UIImage imageNamed:@"scrollPlayback"] forState:UIControlStateNormal];
    [self.playerHudBottom addSubview:self.progressBar];

    //Current Time Label
    self.playBackTime = [[UILabel alloc] init];
    //[self.playBackTime sizeToFit];
    self.playBackTime.frame = CGRectMake(frameWidth-60, -25, 50, 50);
    self.playBackTime.text = [self getStringFromCMTime:self.moviePlayer.currentTime];
    [self.playBackTime setTextAlignment:NSTextAlignmentCenter];
    [self.playBackTime setTextColor:[UIColor whiteColor]];
    //[self.playBackTime setBackgroundColor:[UIColor redColor]];
    self.playBackTime.font = [UIFont systemFontOfSize:12];
    //self.playBackTime.font = [UIFont systemFontOfSize:12*frameWidth/240];
    [self.playerHudBottom addSubview:self.playBackTime];
    
    //Total Time label
    self.playBackTotalTime = [[UILabel alloc] init];
    //[self.playBackTotalTime sizeToFit];
    self.playBackTotalTime.frame = CGRectMake(frameWidth-60, -25, 50, 50);
    self.playBackTotalTime.text = [self getStringFromCMTime:self.moviePlayer.currentItem.asset.duration];
    [self.playBackTotalTime setTextAlignment:NSTextAlignmentLeft];
    [self.playBackTotalTime setTextColor:[UIColor whiteColor]];
    //[self.playBackTotalTime setBackgroundColor:[UIColor redColor]];
    self.playBackTotalTime.font = [UIFont systemFontOfSize:12];
    //self.playBackTotalTime.font = [UIFont systemFontOfSize:12*frameWidth/240];
    [self.playerHudBottom addSubview:self.playBackTotalTime];
    
    //zoom button
    /*UIImage *image = [UIImage imageNamed:@"zoomin"];
    self.zoomButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.zoomButton.frame = CGRectMake(0,0,image.size.width, image.size.height);
    [self.zoomButton addTarget:self action:@selector(zoomButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.zoomButton setBackgroundImage:image forState:UIControlStateNormal];
    [self.playerHudBottom addSubview:self.zoomButton];
    */
    
    for (UIView *view in [self subviews]) {
        view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    }
    
    CMTime interval = CMTimeMake(33, 1000);
    __weak __typeof(self) weakself = self;
    playbackObserver = [self.moviePlayer addPeriodicTimeObserverForInterval:interval queue:dispatch_get_main_queue() usingBlock: ^(CMTime time) {
        CMTime endTime = CMTimeConvertScale (weakself.moviePlayer.currentItem.asset.duration, weakself.moviePlayer.currentTime.timescale, kCMTimeRoundingMethod_RoundHalfAwayFromZero);
        if (CMTimeCompare(endTime, kCMTimeZero) != 0) {
            double normalizedTime = (double) weakself.moviePlayer.currentTime.value / (double) endTime.value;
            weakself.progressBar.value = normalizedTime;
            [weakself sliderValueChanged:weakself.progressBar];
        }
        weakself.playBackTime.text = [weakself getStringFromCMTime:weakself.moviePlayer.currentTime];
    }];
    
    [self setupConstraints];
    [self showHud:NO];
}

-(void)makeFullScreen{
    [playerLayer setFrame:[UIScreen mainScreen].bounds];
}
-(void)zoomButtonPressed:(UIButton*)sender
{
//    [UIView animateWithDuration:0.5 animations:^{
//        [self setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width)];
//    }];
//    [self.delegate playerViewZoomButtonClicked:self];
}

-(void)layoutSubviews
{
    CGRect playerLayerFrame =  CGRectMake(0, 0,self.frame.size.width, self.frame.size.height);
    [super layoutSubviews];
    [playerLayer setFrame:playerLayerFrame];
}

-(void)setIsFullScreenMode:(BOOL)isFullScreenMode
{
    _isFullScreenMode = isFullScreenMode;
    if (isFullScreenMode) {
        self.backgroundColor = [UIColor blackColor];
    } else {
        self.backgroundColor = [UIColor clearColor];
    }
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isKindOfClass:[AVPlayerItem class]])
    {
        AVPlayerItem *item = (AVPlayerItem *)object;
        //playerItem status value changed?
        if ([keyPath isEqualToString:@"status"])
        {   //yes->check it...
            switch(item.status)
            {
                case AVPlayerItemStatusFailed:
                    NSLog(@"player item status failed");
                    [self removeObserverForPlayer];
                    //[self.delegate videoFinishedPlaying];
                    /*if(!playerFailed){
                        playerFailed = YES;
                        AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:self.contentURL];
                        [self.moviePlayer replaceCurrentItemWithPlayerItem:playerItem];
                        [self play];
                    }else{
                        [self.delegate videoFinishedPlaying];
                    }*/
                    break;
                case AVPlayerItemStatusReadyToPlay:
                    NSLog(@"player item status is ready to play");
                    [self playerLoadVideo];
                    break;
                case AVPlayerItemStatusUnknown:
                    [self removeObserverForPlayer];
                    NSLog(@"player item status is unknown");
                    break;
            }
        }
        else if ([keyPath isEqualToString:@"playbackBufferEmpty"])
        {
            if (item.playbackBufferEmpty)
            {
                NSLog(@"player item playback buffer is empty");
                [self removeObserverForPlayer];
            }
        }
    }
}

-(void)removeObserverForPlayer{
    if(self.moviePlayer){
        AVPlayerItem *playerItem = [self.moviePlayer currentItem];
        [playerItem removeObserver:self forKeyPath:@"status" context:nil];
        [playerItem removeObserver:self forKeyPath:@"playbackBufferEmpty" context:nil];
    }
}
-(void)playerFinishedPlaying
{
    [self.moviePlayer pause];
    [self.moviePlayer seekToTime:kCMTimeZero];
    [self.playPauseButton setSelected:NO];
    self.isPlaying = NO;
    if ([self.delegate respondsToSelector:@selector(playerFinishedPlayback:)]) {
        [self.delegate playerFinishedPlayback:self];
    }
    
    if(self.automaticGoToNextVehicle){
        [self.delegate videoFinishedPlaying];
    }else{
        doneButton.layer.opacity = 1;
    }
}
-(void)playerLoadVideo
{
    [self removeObserverForPlayer];
    [self.delegate playerLoadVideo];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint point = [(UITouch*)[touches anyObject] locationInView:self];
    if (CGRectContainsPoint(playerLayer.frame, point)) {
        [self showHud:!viewIsShowing];
    }
}

-(void) showHud:(BOOL)show
{
    __weak __typeof(self) weakself = self;
    if(show) {
        [self.delegate hideArrows];
        CGRect frame = self.playerHudBottom.frame;
        frame.origin.y = self.bounds.size.height+25;
        
        [UIView animateWithDuration:0.3 animations:^{
            weakself.playerHudBottom.frame = frame;
            weakself.playPauseButton.layer.opacity = 0;
            viewIsShowing = show;
        }];
    } else {
        [self.delegate showArrows];
        CGRect frame = self.playerHudBottom.frame;
        frame.origin.y = self.bounds.size.height-self.playerHudBottom.frame.size.height;
        
        [UIView animateWithDuration:0.3 animations:^{
            weakself.playerHudBottom.frame = frame;
            weakself.playPauseButton.layer.opacity = 1;
            viewIsShowing = show;
        }];
    }
}

-(NSString*)getStringFromCMTime:(CMTime)time
{
    Float64 currentSeconds = CMTimeGetSeconds(time);
    int mins = currentSeconds/60.0;
    int secs = fmodf(currentSeconds, 60.0);
    NSString *minsString = mins < 10 ? [NSString stringWithFormat:@"0%d", mins] : [NSString stringWithFormat:@"%d", mins];
    NSString *secsString = secs < 10 ? [NSString stringWithFormat:@"0%d", secs] : [NSString stringWithFormat:@"%d", secs];
    return [NSString stringWithFormat:@"%@:%@", minsString, secsString];
}

//-(void)volumeButtonPressed:(UIButton*)sender
//{
//    if (sender.isSelected) {
//        [self.moviePlayer setMuted:YES];
//        [sender setSelected:NO];
//    } else {
//        [self.moviePlayer setMuted:NO];
//        [sender setSelected:YES];
//    }
//}

-(void)playButtonAction:(UIButton*)sender
{
    if (self.isPlaying) {
        [self pause];
//        [sender setSelected:NO];
    } else {
        [self play];
//        [sender setSelected:YES];
    }
}

-(void)progressBarChanged:(UISlider*)sender
{
    if (self.isPlaying) {
        [self.moviePlayer pause];
    }
    CMTime seekTime = CMTimeMakeWithSeconds(sender.value * (double)self.moviePlayer.currentItem.asset.duration.value/(double)self.moviePlayer.currentItem.asset.duration.timescale, self.moviePlayer.currentTime.timescale);
    [self.moviePlayer seekToTime:seekTime];
    
    
}

-(void)proressBarChangeEnded:(UISlider*)sender
{
    if (self.isPlaying) {
        [self.moviePlayer play];
    }
}

-(void)volumeBarChanged:(UISlider*)sender
{
    [self.moviePlayer setVolume:sender.value];
}

-(void)play
{
    [self.moviePlayer play];
    self.isPlaying = YES;
    [self.playPauseButton setSelected:YES];
}

-(void)pause
{
    [self.moviePlayer pause];
    self.isPlaying = NO;
    [self.playPauseButton setSelected:NO];
}

-(void)stop
{
    [self.moviePlayer pause];
    [self.moviePlayer seekToTime:kCMTimeZero];
    //[self.moviePlayer setRate:0.0];
    self.isPlaying = NO;
    [self.playPauseButton setSelected:NO];
    if(viewIsShowing){
        [self showHud:!viewIsShowing];
    }
}

- (void) pressDone
{
  [self.delegate videoFinishedPlaying];
}

-(void)dealloc
{
    @try {
        [self removeObserverForPlayer];
    }
    @catch(NSException *exception)
    {
        //MRLogError(@"!!!Not Panic You remove after playerLoadVideo :) Unable to perform removeObserver: %@", (id)[exception userInfo] ?: (id)[exception reason]);
    }
    @finally
    {

    }
    [self.moviePlayer removeTimeObserver:playbackObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    

}

- (void)sliderValueChanged:(UISlider *)sender {
    //NSLog(@"slider value = %f", sender.value);
    self.playBackTime.frame = CGRectMake(30+((self.frame.size.width-125) *sender.value), 25, 50, 50);
    //self.playBackTime.frame = CGRectMake(30+([self.progressBar width] *sender.value), 25, 50, 50);
}
@end
