//
//  BaseViewVehicle.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 3/29/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "BaseViewVehicle.h"

@implementation BaseViewVehicle{
    NSTimer *finishVehicleTimer;
    UIView *backgroundFeedbackView;
    UIImageView *imageViewFeedbackSmall;
    int incorrectAnswers;
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
    }
    
    return self;
}



#pragma mark - Reset instance and completed instance layout


- (void)resetTheInstance {
    incorrectAnswers = 0;
}

- (void)showLayout:(BOOL)forCompleted {
    self.isCompleted = forCompleted;
    self.isCompleted ? [self showLayoutForCompletedInstance] : [self showLayoutForNewVehicle];
}

- (void)showLayoutForCompletedInstance {
    
    
}

- (void)showLayoutForNewVehicle {
    
}

#pragma mark enable/Disable
- (void)activateVehicle{
    //Vehicle is shown on phone screen
    self.isCurrent = YES;
    self.vehicleState = KADefaultState;
    incorrectAnswers = 0;
    
}

- (void)deactivateVehicle{
    
    //Vehicle is dismist from phone screen
    //[AUDIO_MANAGER stopRecording];
    //[AUDIO_MANAGER stopPlaying];
    [self hideAllFeedbackView];
    
    [finishVehicleTimer invalidate];
    finishVehicleTimer = nil;
    self.isCurrent = NO;
}

-(void)hideAllFeedbackView{
    [backgroundFeedbackView removeFromSuperview];
    [imageViewFeedbackSmall removeFromSuperview];
    backgroundFeedbackView = nil;
    imageViewFeedbackSmall = nil;
}
-(BOOL)haveBackgroundFeedback{
    return backgroundFeedbackView;
}

- (void)finishVehicle{
    [finishVehicleTimer invalidate];
    finishVehicleTimer = nil;
}

- (void)disableVehicleInteractions {
    self.isActive = NO;
}

- (void)enableVehicleInteractions {
    self.isActive = YES;
}

- (void)feedSwipe
{
    
}

- (void)updateViewConstraints{
    
}

- (void)showIncorrectFeedback{
    incorrectAnswers ++;
    [AUDIO_MANAGER playWrongSound];
    if(!self.afterAudioString){
        finishVehicleTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(finishVehicle) userInfo:nil repeats:NO];
    }else{
        finishVehicleTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(finishVehicle) userInfo:nil repeats:NO];
    }
}

- (void)showCorrectFeedback {
    [self.vehicleDelegate addPoints:kParam001 - (incorrectAnswers * kParam002)];
    
    if (finishVehicleTimer) {
        [finishVehicleTimer invalidate];
    }
    
    [AUDIO_MANAGER playRightSound];
    if(!self.afterAudioString){
        finishVehicleTimer = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(finishVehicle) userInfo:nil repeats:NO];
    }else{
        finishVehicleTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(finishVehicle) userInfo:nil repeats:NO];
    }
}
- (void)showCorrectFeedbackWithOutPoins {
    
    if (finishVehicleTimer) {
        [finishVehicleTimer invalidate];
    }
    
    [AUDIO_MANAGER playRightSound];
    if(!self.afterAudioString){
        finishVehicleTimer = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(finishVehicle) userInfo:nil repeats:NO];
    }else{
        finishVehicleTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(finishVehicle) userInfo:nil repeats:NO];
    }
}

- (void)showCorrectFeedbackAndAddPoints:(int)points {
    
    [self.vehicleDelegate addPoints:points];
    
    if(!self.afterAudioString){
        finishVehicleTimer = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(finishVehicle) userInfo:nil repeats:NO];
    }else{
        finishVehicleTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(finishVehicle) userInfo:nil repeats:NO];
    }
    
}

- (void)andAddPoints:(int)points {
    
    [self.vehicleDelegate addPoints:points];
}

- (void)showFeedbackWithStatus:(BOOL)status forView:(UIView*)statusView {
    backgroundFeedbackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [statusView width], [statusView height])];
    [backgroundFeedbackView setBackgroundColor:[UIColor clearColor]];
    
    //statusView.layer.cornerRadius = 4.0;
    statusView.layer.borderColor = APP_INCORRECT_COLOR.CGColor;
    statusView.layer.borderWidth = 2.0;
    
    if (status) {
        imageViewFeedbackSmall = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"correct-symbol"]];
        [imageViewFeedbackSmall setImage:[UIImage imageNamed:@"correct-symbol"]];
        [backgroundFeedbackView addSubview:imageViewFeedbackSmall];
        statusView.layer.borderColor = APP_CORRECT_COLOR.CGColor;
        
    }else{
        incorrectAnswers ++;//#
        imageViewFeedbackSmall = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"wrong-symbol"]];
        [imageViewFeedbackSmall setImage:[UIImage imageNamed:@"wrong-symbol"]];
        [backgroundFeedbackView addSubview:imageViewFeedbackSmall];
        statusView.layer.borderColor = APP_INCORRECT_COLOR.CGColor;
    }
    
    [imageViewFeedbackSmall setX:8];
    [imageViewFeedbackSmall setY:[backgroundFeedbackView height]-16-8];
    
    [statusView addSubview:backgroundFeedbackView];
    
    [imageViewFeedbackSmall autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:backgroundFeedbackView withOffset:8.0 relation:NSLayoutRelationEqual];
    [imageViewFeedbackSmall autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:backgroundFeedbackView withOffset:-8.0 relation:NSLayoutRelationEqual];
    
    [imageViewFeedbackSmall autoSetDimensionsToSize:CGSizeMake(16, 16)];
    
    [backgroundFeedbackView autoCenterInSuperview];
    [backgroundFeedbackView autoSetDimensionsToSize:CGSizeMake([backgroundFeedbackView width], [backgroundFeedbackView height])];
    
    /*[backgroundFeedbackView setAlpha:0];
     [imageViewFeedbackSmall setAlpha:0];
     double delayInSeconds = 0.0f;
     dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
     dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
     [backgroundFeedbackView setAlpha:1];
     [imageViewFeedbackSmall setAlpha:1];
     statusView.layer.borderWidth = 2.0;
     });*/
    
}

- (void)showFeedbackWithStatus:(BOOL)status forView:(UIView*)statusView forBorderView:(UIView*)statusBorderView{
    backgroundFeedbackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [statusView width], [statusView height])];
    [backgroundFeedbackView setBackgroundColor:[UIColor clearColor]];
    
    
    //statusView.layer.cornerRadius = 4.0;
    statusBorderView.layer.borderColor = APP_INCORRECT_COLOR.CGColor;
    statusBorderView.layer.borderWidth = 0.0;
    
    if (status) {
        imageViewFeedbackSmall = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"correct-symbol"]];
        [imageViewFeedbackSmall setImage:[UIImage imageNamed:@"correct-symbol"]];
        [backgroundFeedbackView addSubview:imageViewFeedbackSmall];
        statusBorderView.layer.borderColor = APP_CORRECT_COLOR.CGColor;
        
    }else{
        incorrectAnswers ++;
        imageViewFeedbackSmall = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"wrong-symbol"]];
        [imageViewFeedbackSmall setImage:[UIImage imageNamed:@"wrong-symbol"]];
        [backgroundFeedbackView addSubview:imageViewFeedbackSmall];
        statusBorderView.layer.borderColor = APP_INCORRECT_COLOR.CGColor;
    }
    //[imageViewFeedbackSmall setX:8];
    //[imageViewFeedbackSmall setY:[backgroundFeedbackView height]-8];
    
    [statusView addSubview:backgroundFeedbackView];
    
    [imageViewFeedbackSmall autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:backgroundFeedbackView withOffset:8.0 relation:NSLayoutRelationEqual];
    [imageViewFeedbackSmall autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:backgroundFeedbackView withOffset:-8.0 relation:NSLayoutRelationEqual];
    
    [imageViewFeedbackSmall autoSetDimensionsToSize:CGSizeMake(16, 16)];
    
    [backgroundFeedbackView autoCenterInSuperview];
    [backgroundFeedbackView autoSetDimensionsToSize:CGSizeMake([backgroundFeedbackView width], [backgroundFeedbackView height])];
    
    [backgroundFeedbackView setAlpha:0];
    [imageViewFeedbackSmall setAlpha:0];
    double delayInSeconds = 0.4f;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [backgroundFeedbackView setAlpha:1];
        [imageViewFeedbackSmall setAlpha:1];
        statusBorderView.layer.borderWidth = 2.0;
    });
}

@end
