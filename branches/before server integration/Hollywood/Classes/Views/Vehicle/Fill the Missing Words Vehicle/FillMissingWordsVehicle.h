//
//  FillMissingWordsVehicle.h
//  Hollywood
//
//  Created by Kiril Kiroski on 5/12/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "BaseViewVehicle.h"
#import "MissingWordsTextAnswer.h"

@interface FillMissingWordsVehicle : BaseViewVehicle <MissingWordsTextAnswerDelegate>

- (instancetype)initWithFrame:(CGRect)frame data:(id)data;

@end
