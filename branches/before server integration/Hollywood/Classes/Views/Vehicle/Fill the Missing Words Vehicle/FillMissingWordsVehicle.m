//
//  FillMissingWordsVehicle.m
//  Hollywood
//
//  Created by Kiril Kiroski on 5/12/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "FillMissingWordsVehicle.h"

@implementation FillMissingWordsVehicle{
    id vehicleData;
    NSArray *words;
    NSString *question;
    NSString *answer;
    UIView *viewAnswerContent;
    NSMutableArray *questionWordView;
    NSMutableArray *dragWordViewArray;
    NSString *replaceString;
    int hit;
    int wordsForFill;
    UIView *viewQuestionContent;
    UIImageView *imageViewQuestion;
    NSTimer *goToFinishTimer;
    int currentBox;
    UILabel *completeSentenceLabel;
}

- (void)activateVehicle{
    [super activateVehicle];
    
    [ANALYTICS_MANAGER enterTheVehicleEvent:kEnterFillTheMissing];
    currentBox = 1;
    [self showCurrentBox];
    /*hit = 0;
    self.nAttemptsLeft = 2;*/
    
}

- (void)deactivateVehicle{
    
    [super deactivateVehicle];
    hit = 0;
    self.nAttemptsLeft = 2;
    if(goToFinishTimer){
        [goToFinishTimer invalidate];
        goToFinishTimer = nil;
    }
    for (int i= 0; i < [dragWordViewArray count]; i++) {
        MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[dragWordViewArray objectAtIndex:i];
        [tempClickTextAnswer reset];
    }
    for (int i= 0; i < [questionWordView count]; i++) {
        MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[questionWordView objectAtIndex:i];
        [tempClickTextAnswer resetLabel];
        tempClickTextAnswer.alpha = 1;
    }
    completeSentenceLabel.alpha = 0;
    [ANALYTICS_MANAGER leaveTheVehicleEvent:kEnterFillTheMissing otherData:nil];

    
}
- (instancetype)initWithFrame:(CGRect)frame data:(id)data {
    
    if (self = [super initWithFrame:frame]) {
        hit = 0;
        self.nAttemptsLeft = kParam012;
        
        vehicleData = data;
        self.backgroundColor = [UIColor clearColor];
        answer = [[vehicleData objectForKey:@"answer"] stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        question = [[vehicleData objectForKey:@"question"] stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
        words = [question componentsSeparatedByString:@" "];
        
        [self createQuestionImage];
        [self createLayout:[LMHelper shuffleArray:[vehicleData objectForKey:@"answers"]]];

    }
    
    return self;
}


- (void)createLayout:(NSArray *)arrayAnswers {
    
    //Create answer content view
    
    float answerContentWidth = [self width] - [viewQuestionContent width]  - 10 - ([viewQuestionContent x]*2);//(DEVICE_BIGER_SIDE-120-20)/2;
    float answerContentHeight = [self height]-40;//DEVICE_SMALL_SIDE-64-90;
    
    
    if (viewAnswerContent == nil) {
        //viewAnswerContent = [[UIView alloc] initWithFrame:CGRectMake(60+answerContentWidth+20, 30, answerContentWidth, answerContentHeight)];
        viewAnswerContent = [[UIView alloc] initWithFrame:CGRectMake([viewQuestionContent x] + [viewQuestionContent width] + 10, 20, answerContentWidth, answerContentHeight)];
        [viewAnswerContent setBackgroundColor:[UIColor clearColor]];
        [self addSubview:viewAnswerContent];
    }
    
    
    
    
    //Create answers
    MissingWordsTextAnswer *clickTextAnswer;
    CGFloat answerWidth = (answerContentWidth/2)- 5;
    float answerHeight = (answerContentHeight/4)- 5;//15 = 3*5 space betwen box
    
    float startYCordinate2 = [self height]- 40 - answerHeight;
    
    
    questionWordView = [[NSMutableArray alloc]init];
    NSInteger startX = [viewAnswerContent x];
    NSInteger startY = 30;
    
    NSString *tempReplaceString;
    replaceString = @"_";
    
    for (id tempString in words) {
        if([LMHelper isWordMatchingPattern:tempString pattern:kWordPattern]) {
            tempReplaceString = [[tempString componentsSeparatedByCharactersInSet:[[NSCharacterSet letterCharacterSet] invertedSet]] componentsJoinedByString:@"_"];
            tempReplaceString = [[tempReplaceString componentsSeparatedByCharactersInSet:[NSCharacterSet letterCharacterSet]] componentsJoinedByString:@"_"];
            tempReplaceString = [NSString stringWithFormat:@"_%@_",tempReplaceString];
        }
        if([replaceString length]<[tempReplaceString length]){
            replaceString = tempReplaceString;
        }
    }
    UILabel *testLabel = [[UILabel alloc] init];
    [testLabel setFont:[UIFont systemFontOfSize:26.0]];
    testLabel.text = replaceString;
    [testLabel sizeToFit];
    
    
    if(answerWidth < [testLabel width]){
        answerWidth = [testLabel width];
    }
    
    NSString *completeSentence = @"";
    completeSentenceLabel = [[UILabel alloc] initWithFrame:CGRectMake(startX,startY, answerContentWidth, answerContentHeight)];
    [completeSentenceLabel setFont:[UIFont systemFontOfSize:26.0]];
    completeSentenceLabel.text = completeSentence;
    completeSentenceLabel.backgroundColor = [UIColor clearColor];
    completeSentenceLabel.textColor = [UIColor whiteColor];
    [self addSubview:completeSentenceLabel];
    completeSentenceLabel.alpha = 0;
    
    wordsForFill = 0;
    
    
    for (id dataClickImage in words) {
        completeSentence = [NSString stringWithFormat:@"%@ %@",completeSentence,dataClickImage];
        clickTextAnswer = [[MissingWordsTextAnswer alloc] initWithFrame:CGRectMake(startX,startY, answerWidth, answerHeight) withText:dataClickImage andReplaceString:replaceString];
        if(startX + [clickTextAnswer width] >= ([viewAnswerContent x] + answerContentWidth)){
            startX = [viewAnswerContent x];;
            startY = startY+answerHeight;
            clickTextAnswer = [[MissingWordsTextAnswer alloc] initWithFrame:CGRectMake(startX,startY, answerWidth, answerHeight) withText:dataClickImage andReplaceString:replaceString];
        }
        [clickTextAnswer setBackgroundColor:[UIColor clearColor]];
        [self addSubview:clickTextAnswer];
        [questionWordView addObject:clickTextAnswer];
        
        if([[clickTextAnswer getLabelWordText] isEqualToString:replaceString]){
            wordsForFill ++;
        }
        
        
        
        startX += [clickTextAnswer width]+10;
        //NSLog(@"startX %.2ld > %.2f ",(long)startX,[viewAnswerContent x] + answerContentWidth);
        /*if(startX+50 >= ([viewAnswerContent x] + answerContentWidth)){
        //if(startX >= (DEVICE_BIGER_SIDE-120)){
            startX = [viewAnswerContent x];;
            startY = startY+answerHeight;
        }*/
    }
    completeSentenceLabel.numberOfLines = 0;
    completeSentenceLabel.text = answer;//completeSentence;
    [completeSentenceLabel sizeToFit];
    [completeSentenceLabel setWidth:answerContentWidth];
    
    NSInteger order = 0;
    NSInteger row = 0;
    NSInteger column = 0;
    answerWidth = (answerContentWidth-5)/2 ;
    
    dragWordViewArray = [[NSMutableArray alloc]init];
    
    row = 0;//[arrayAnswers count]/2;
   
    UIView *depthView =  [[UIView alloc]init];
     [self addSubview:depthView];
    
    float minFont = 26;
    UIFont *bestFont;
    
    for (id dataClickImage in arrayAnswers) {
        UIView *clickTextBox = [[UIView alloc] initWithFrame:CGRectMake([viewAnswerContent x] + column*(answerWidth+5),[viewAnswerContent y] +startYCordinate2 - ((answerHeight+5)*row), answerWidth, answerHeight)];

        [clickTextBox setBackgroundColor:[UIColor blackColor]];
        clickTextBox.layer.cornerRadius = 4.0;
        clickTextBox.layer.borderColor = [UIColor whiteColor].CGColor;
        clickTextBox.layer.borderWidth = 1.0;
        clickTextBox.layer.masksToBounds = YES;
        clickTextBox.userInteractionEnabled = NO;
        [self insertSubview:clickTextBox belowSubview:depthView];
        
        ///
        if ([dataClickImage objectForKey:@"sound"]) {
            /*UIButton *buttonSound = [UIButton buttonWithType:UIButtonTypeCustom];
            [buttonSound setImage:[UIImage imageNamed:@"soundGoldcopy11"] forState:UIControlStateNormal];
            [buttonSound setFrame:CGRectMake(clickTextBox.width-32, (clickTextBox.height-34)/2, 34, 34)];
            [buttonSound setAlpha:0.64];
            [buttonSound addTarget:self action:@selector(playAnswerSound:) forControlEvents:UIControlEventTouchUpInside];
            [clickTextBox addSubview:buttonSound];
            clickTextBox.userInteractionEnabled = YES;*/
            clickTextAnswer = [[MissingWordsTextAnswer alloc] initWithFrame:CGRectMake([viewAnswerContent x] + column*(answerWidth+5),[viewAnswerContent y] +startYCordinate2 - ((answerHeight+5)*row), answerWidth-25, answerHeight) data:dataClickImage type:YES onView:clickTextBox];

        }else{
            clickTextAnswer = [[MissingWordsTextAnswer alloc] initWithFrame:CGRectMake([viewAnswerContent x] + column*(answerWidth+5),[viewAnswerContent y] +startYCordinate2 - ((answerHeight+5)*row), answerWidth, answerHeight) data:dataClickImage type:YES];

        }
        ///
        
                //clickTextAnswer.delegateClickText = self;
        [clickTextAnswer setBackgroundColor:[UIColor clearColor]];
        clickTextAnswer.vehicleDelegate = self;
        //[self addSubview:clickTextAnswer];
        [self insertSubview:clickTextAnswer aboveSubview:depthView];
        [dragWordViewArray addObject:clickTextAnswer];
        UIFont *testFont = [clickTextAnswer getFont];
        if(testFont.pointSize < minFont){
            minFont = testFont.pointSize;
            bestFont = testFont;
        }
        
        order++;
        row = order/2;//([arrayAnswers count]/2) - order/2;
        column = order%2;
    }
    
    if(bestFont){
        completeSentenceLabel.font = bestFont;
        completeSentenceLabel.numberOfLines = 0;
        [completeSentenceLabel sizeToFit];
        
        for (int i= 0; i < [dragWordViewArray count]; i++) {
            MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[dragWordViewArray objectAtIndex:i];
            [tempClickTextAnswer setNewFont:bestFont];
        }
    }
  
    
}

- (void)playAnswerSound:(id)sender {
    
   //[AUDIO_MANAGER playSoundFromAudioFile:[self.dataAnswer objectForKey:@"sound"]];
}

- (void)createQuestionImage {
    
    UIImage *originalImage = [UIImage imageNamed:[vehicleData objectForKey:@"bigImage"]];
   // UIImage *newImage = [UIImage imageWithImage:originalImage scaledToHeight:DEVICE_SMALL_SIDE-64-90];
     UIImage *newImage = [UIImage imageWithImage:originalImage scaledToHeight:[self height]-40];
    
    CGFloat contentQuestionWidth = newImage.size.width;
    CGFloat contentQuestionHeight = newImage.size.height;
    
    NSLog(@"New width - %f, new height - %f", newImage.size.width, newImage.size.height);
    
    float answerContentWidth = (DEVICE_BIGER_SIDE-100-20)/4;
    if (newImage.size.width > 3*answerContentWidth) {
        
        contentQuestionWidth = 3*answerContentWidth;
    }
    
    if (viewQuestionContent == nil) {
        
        viewQuestionContent = [[UIView alloc] initWithFrame:CGRectMake(50, 20, contentQuestionWidth, contentQuestionHeight)];
        [viewQuestionContent setBackgroundColor:[UIColor blackColor]];
        [self addSubview:viewQuestionContent];
        
        viewQuestionContent.layer.cornerRadius = 4.0;
        viewQuestionContent.layer.borderWidth = 1.0;
        viewQuestionContent.layer.borderColor = [UIColor whiteColor].CGColor;
        viewQuestionContent.layer.masksToBounds = YES;
    }
    
    if (imageViewQuestion == nil) {
        
        imageViewQuestion = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, contentQuestionWidth-6, contentQuestionHeight-6)];
        [imageViewQuestion setImage:newImage];
        [imageViewQuestion setContentMode:UIViewContentModeScaleAspectFit];
        [viewQuestionContent addSubview:imageViewQuestion];
        
    }
}

#pragma mark MissingWordsTextAnswerDelegate
- (void) showCurrentBox{
    MissingWordsTextAnswer* nextTextAnswer = [self takeCurentBox];
    [nextTextAnswer makeCurrentBox];
    if((hit < wordsForFill) && (self.nAttemptsLeft > 0)){
        for (int i= 0; i < [dragWordViewArray count]; i++) {
            MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[dragWordViewArray objectAtIndex:i];
            [tempClickTextAnswer unlock];
        }
    }
}

-(MissingWordsTextAnswer*)takeCurentBox{
    int count = 0;
    for (int i= 0; i < [questionWordView count]; i++) {
        MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[questionWordView objectAtIndex:i];
        if ([[tempClickTextAnswer wordString] rangeOfString:@"<"].location != NSNotFound) {
            count++;
            if(count == currentBox){
                return tempClickTextAnswer;
            }
        }
    }
    return nil;
}

- (CGPoint) chekAnswer:(NSString*)curentText {
    float delayInSeconds = 0.2;
    for (int i= 0; i < [dragWordViewArray count]; i++) {
        MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[dragWordViewArray objectAtIndex:i];
        [tempClickTextAnswer lock];
    }
    MissingWordsTextAnswer* tempClickTextAnswer = [self takeCurentBox];
    if([[NSString stringWithFormat:@"<%@>",curentText] isEqualToString:[tempClickTextAnswer wordString]]) {
        NSLog(@"# yess");
        currentBox ++;//go to next box
        
        [self answerIsCorect:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [tempClickTextAnswer setCorrectAnswer];
        });
    }else{
        [self showIncorrectFeedback];
        self.nAttemptsLeft--;
        [self answerIsCorect:NO];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [tempClickTextAnswer setIncorrectAnswer];
        });
    }
    return [tempClickTextAnswer takeFinalLocation];
}

- (BOOL) isAnswerCorect:(NSString*)curentText {
    MissingWordsTextAnswer* tempClickTextAnswer = [self takeCurentBox];
    if([[NSString stringWithFormat:@"<%@>",curentText] isEqualToString:[tempClickTextAnswer wordString]]) {
        return YES;
    }else{
        return NO;
    }
    return NO;
}

- (CGPoint) chekAnswer:(NSString*)curentText andPoint:(CGPoint)loc{
    
    //CGPoint loc = [tapGesture locationInView:carouselPickerView];
    for (int i= 0; i < [questionWordView count]; i++) {
        MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[questionWordView objectAtIndex:i];
        
        if (CGRectContainsPoint(tempClickTextAnswer.frame, loc)){
            //NSLog(@"###%@ == %@###",[NSString stringWithFormat:@"<%@>",curentText],[tempClickTextAnswer wordString]);
            if([[NSString stringWithFormat:@"<%@>",curentText] isEqualToString:[tempClickTextAnswer wordString]]) {
                NSLog(@"# yess");
                //[tempClickTextAnswer setCorrectAnswer];
                return [tempClickTextAnswer takeFinalLocation];;
            }else{
                [self showIncorrectFeedback];
                self.nAttemptsLeft--;
                [tempClickTextAnswer setIncorrectAnswer];
            }
        }else{
            NSLog(@"# no");
            
            //return NO;
        }
    }
    return CGPointZero;
}

- (void) setCorectAnswer:(NSString*)curentText andPoint:(CGPoint)loc{
    for (int i= 0; i < [questionWordView count]; i++) {
        MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[questionWordView objectAtIndex:i];
        if (CGRectContainsPoint(tempClickTextAnswer.frame, loc)){
            //NSLog(@"###%@ == %@###",[NSString stringWithFormat:@"<%@>",curentText],[tempClickTextAnswer wordString]);
            if([[NSString stringWithFormat:@"<%@>",curentText] isEqualToString:[tempClickTextAnswer wordString]]) {
                NSLog(@"# yess");
                [tempClickTextAnswer setCorrectAnswer];
                return ;
            }
        }
    }
    return;
}

- (void) answerIsCorect:(BOOL)answerFlag{
    if(answerFlag){
        [self showCorrectFeedback];
        hit ++;
        if(hit >= wordsForFill){
            [self.vehicleDelegate  vehiclePass:YES];
            for (int i= 0; i < [questionWordView count]; i++) {
                MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[questionWordView objectAtIndex:i];
                [UIView animateWithDuration:0.2 animations:^{
                    [tempClickTextAnswer fadeOut];
                } completion:^(BOOL finished) {
                    
                }];
            }
        }
    }else{
        //[self showIncorrectFeedback];
        //self.nAttemptsLeft--;
    }
}

- (void) textStartMove{
    [self.vehicleDelegate disableScrolling:0];
}

- (void) textStopMove{
    [self.vehicleDelegate enableScrolling:0];
}

#pragma mark BaseViewVehicle
-(void)showCorectSentence{
    for (int i= 0; i < [dragWordViewArray count]; i++) {
        MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[dragWordViewArray objectAtIndex:i];
        [tempClickTextAnswer lock];
    }
    [UIView animateWithDuration:0.15  delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        completeSentenceLabel.alpha = 1;
        for (int i= 0; i < [questionWordView count]; i++) {
            MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[questionWordView objectAtIndex:i];
            //[tempClickTextAnswer hide];
            tempClickTextAnswer.alpha = 0;
        }
    } completion:^(BOOL finished) {
        
    }];
}
- (void)finishVehicle{
    if((hit < wordsForFill) && (self.nAttemptsLeft > 0) ) {
        return;
    }
    
    
    
    if(self.nAttemptsLeft == 0){
        
        self.nAttemptsLeft = -10;
        [self showCorectSentence];
        /*for (int i= 0; i < [questionWordView count]; i++) {
            MissingWordsTextAnswer* tempClickTextAnswer = (MissingWordsTextAnswer*)[questionWordView objectAtIndex:i];
                    [tempClickTextAnswer showCorrectAnswer];
        }*/
       
        goToFinishTimer = [NSTimer scheduledTimerWithTimeInterval:kParam019 target:self selector:@selector(finishVehicle) userInfo:nil repeats:NO];
        return;
        
    }else if(self.nAttemptsLeft > 0){
        self.nAttemptsLeft = -20;
        [self showCorectSentence];
        goToFinishTimer = [NSTimer scheduledTimerWithTimeInterval:kParam019 target:self selector:@selector(finishVehicle) userInfo:nil repeats:NO];
        return;
    }
    
    [super finishVehicle];
    if(self.afterAudioString){
        self.vehicleState = KAPlayAfterAudioState;
        [AUDIO_MANAGER playAudio:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:self.afterAudioString ofType:@""]]];
        [AUDIO_MANAGER setDelegate:self];
        self.afterAudioString = nil;
    }else{
        self.isCompleted = YES;
        [self.vehicleDelegate vehicleWasFinished:self];
        //[self.vehicleDelegate setQuestionBarText:@""];
        [AUDIO_MANAGER setDelegate:nil];
    }
    
}
@end
