//
//  ClickImageView.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 3/30/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "ClickImageView.h"

@implementation ClickImageView
@synthesize viewContainerTargetWord;
@synthesize labelTargetWord;
@synthesize imageViewContent;
@synthesize imageViewFeedback;
@synthesize imageViewFeedbackSmall, viewFeedback, viewTargetLabelMask;
@synthesize isOneImageLayout;
@synthesize answerWasShown;
@synthesize viewContainerSourceText, labelSourceText;

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame data:(id)data type:(BOOL)isLearn isOneImage:(BOOL)isOneImage withFontSize:(CGFloat)fontSize {
    
    if ((self = [super initWithFrame:frame])) {
        
        if ([[[NSBundle mainBundle] loadNibNamed:@"ClickImageView" owner:self options:nil] objectAtIndex:0] != nil) {
            self = [[[NSBundle mainBundle] loadNibNamed:@"ClickImageView" owner:self options:nil] objectAtIndex:0];
        }
        
        self.frame = frame;
        
        viewFeedback.hidden = YES;
        imageViewFeedback.alpha = 0.7;
        answerWasShown = NO;
        
        self.isOneImageLayout = isOneImage;
        
        imageViewContent.layer.masksToBounds = YES;
        
        labelTargetWord.textAlignment = NSTextAlignmentCenter;
        
        
        NSInteger numberOfLines = 0;
        
        if (![data isKindOfClass:[QuizInstanceAnswer class]]) {
            if (fontSize) {
                
                UIFont *font = [UIFont systemFontOfSize:fontSize];
                
                labelTargetWord.numberOfLines = numberOfLines;
                [labelTargetWord setFont:font];
                labelTargetWord.text = [[data objectForKey:@"targetText"] capitalizedString];
                labelTargetWord.lineBreakMode = NSLineBreakByWordWrapping;
                //
                //            CGRect textRect = [[[data objectForKey:@"text"] capitalizedString] boundingRectWithSize:CGSizeMake(frame.size.width-10.0, 40.0)
                //                                                 options:NSStringDrawingUsesDeviceMetrics
                //                                              attributes:@{NSFontAttributeName:font}
                //                                                 context:nil];
                
                [labelTargetWord setBackgroundColor:[UIColor clearColor]];
                
                labelTargetWord.frame = CGRectMake(5, 0, frame.size.width-10, 40);
                //            labelTargetWord.frame = CGRectMake((frame.size.width-textRect.size.width/2)/2, 0, textRect.size.width/2, textRect.size.height*2);
                
                
                labelSourceText.numberOfLines = 1;
                [labelSourceText setFont:font];
                labelSourceText.lineBreakMode = NSLineBreakByWordWrapping;
                labelSourceText.frame = CGRectMake(5, 0, frame.size.width-10, 30);
                labelSourceText.minimumScaleFactor = 0.6;
                labelSourceText.adjustsFontSizeToFitWidth = YES;
                
                
                labelSourceText.text = [[data objectForKey:@"text"] capitalizedString];
                
                labelSourceText.y = 5;
                
            }
            else {
                
                labelTargetWord.numberOfLines = 1;
                labelTargetWord.text = [[data objectForKey:@"text"] capitalizedString];
                [labelTargetWord sizeToFit];
                
                labelTargetWord.minimumScaleFactor = 0.6;
                labelTargetWord.adjustsFontSizeToFitWidth = YES;
                
                labelTargetWord.frame = CGRectMake((viewContainerTargetWord.frame.size.width-labelTargetWord.width)/2, (viewContainerTargetWord.frame.size.height-labelTargetWord.height)/2, labelTargetWord.width, labelTargetWord.height);
                
            }
        }
        
        UIFont *font = [UIFont systemFontOfSize:fontSize];
        [labelTargetWord setFont:font];
        
        labelTargetWord.numberOfLines = 1;
        labelTargetWord.minimumScaleFactor = 0.6;
        labelTargetWord.adjustsFontSizeToFitWidth = YES;
        [labelTargetWord setPreferredMaxLayoutWidth:frame.size.width-10];
        labelTargetWord.frame = CGRectMake(5, 5, frame.size.width-10, 30);
        
        if (![data isKindOfClass:[QuizInstanceAnswer class]]) {
            labelTargetWord.text = [[data objectForKey:@"targetText"] capitalizedString];
        }
        [labelTargetWord setTextColor:[UIColor blackColor]];
        
        
        if (!isOneImageLayout) {
            if ([data isKindOfClass:[QuizInstanceAnswer class]]) {
                
                imageViewContent.image = [CACHE_MANAGER getImageFromCache:[(QuizInstanceAnswer *)data imageUrl]];
                
            }
            else {
                
                if ([data objectForKey:@"image"]) {
                    imageViewContent.image = [UIImage imageNamed:[data objectForKey:@"image"]];
                }
            }
        }
        
        self.dataAnswer = data;
        self.isLearnOrAnswer = isLearn;
        
        if (!isLearn) {
            
            [viewContainerTargetWord setBackgroundColor:[UIColor blackColor]];
            [labelTargetWord setTextColor:[UIColor whiteColor]];
            viewContainerTargetWord.frame = CGRectMake(0, -40, frame.size.width, 40);
            viewTargetLabelMask.hidden = YES;
            viewContainerTargetWord.hidden = YES;
            
        }
        else {
            
            viewContainerTargetWord.hidden = YES;
            [viewContainerTargetWord setBackgroundColor:[UIColor clearColor]];
            
            viewContainerSourceText.hidden = YES;
            [viewContainerSourceText setBackgroundColor:[UIColor clearColor]];
            
            if (![data isKindOfClass:[QuizInstanceAnswer class]])
                labelSourceText.text = [[data objectForKey:@"text"] capitalizedString];
        }
        
        if (isOneImageLayout) {
            
            self.layer.masksToBounds = YES;
            
        }
        else {
            
            self.layer.cornerRadius = 4.0;
            self.layer.borderWidth = 1.0;
            self.layer.borderColor = [UIColor whiteColor].CGColor;
            self.layer.masksToBounds = YES;
            
            imageViewContent.layer.cornerRadius = 4.0;
            
        }
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
        [self addGestureRecognizer:tapGesture];
        
    }
    
    return self;
}

#pragma mark - Tap Gesture action

- (IBAction)tapGestureAction:(id)sender {
    
    if (self.answerIsClosed) {
        return;
    }
    
    if (self.isLearnOrAnswer) {
        
        if (![self.dataAnswer isKindOfClass:[QuizInstanceAnswer class]])
            [AUDIO_MANAGER playSoundFromAudioFile:[self.dataAnswer objectForKey:@"sound"]];
    }
    
    if (!answerWasShown) {
        
        if (self.isLearnOrAnswer) {
            viewContainerTargetWord.hidden = NO;
            viewContainerSourceText.hidden = NO;
            
        }
        
        if (self.isLearnOrAnswer) {
            
            [self.delegateClickImageView answerWasClicked:self changeImage:[self.dataAnswer objectForKey:@"image"]];
        }
        else {
            
            [self.delegateClickImageView answerWasClicked:self];
        }
        
        [UIView animateWithDuration:0.5 animations:^{
            
            if (!self.isLearnOrAnswer)
            {
                viewContainerTargetWord.frame = CGRectMake(viewContainerTargetWord.x, self.frame.size.height-40, viewContainerTargetWord.width, viewContainerTargetWord.height);
            }
            else {
                
                viewContainerTargetWord.frame = CGRectMake(viewContainerTargetWord.x, 0, viewContainerTargetWord.width, viewContainerTargetWord.height);
                viewContainerSourceText.frame = CGRectMake(0, self.frame.size.height-40, viewContainerSourceText.width, viewContainerSourceText.height);
                
            }
            
            
        } completion:^(BOOL finished) {
            
        }];
        answerWasShown = YES;
    }
    else {
        
        //        [UIView animateWithDuration:0.5 animations:^{
        //            viewContainerTargetWord.frame = CGRectMake(viewContainerTargetWord.x, -40, viewContainerTargetWord.width, viewContainerTargetWord.height);
        //        } completion:^(BOOL finished) {
        //
        //            viewContainerTargetWord.hidden = YES;
        //
        //        }];
        
        
    }
    
    
    
    
}

- (void)showFeedbackWithStatus {
    
    if (!_isLearnOrAnswer) {
        
        BOOL isAnsweredCorrect;
        
        if  ([self.dataAnswer isKindOfClass:[QuizInstanceAnswer class]]) {
            
            isAnsweredCorrect = [(QuizInstanceAnswer *)[self dataAnswer] isCorrect];
        }
        else {
            isAnsweredCorrect = [[self.dataAnswer objectForKey:@"isCorrect"] boolValue];
        }
        
        if (isAnsweredCorrect) {
            
            viewFeedback.hidden = NO;
            [imageViewFeedback setBackgroundColor:APP_CORRECT_COLOR];
            [imageViewFeedbackSmall setImage:[UIImage imageNamed:@"correct-symbol"]];
            
            self.layer.cornerRadius = 4.0;
            self.layer.borderColor = APP_CORRECT_COLOR.CGColor;
            self.layer.borderWidth = 2.0;
            
        }
        else {
            
            viewFeedback.hidden = NO;
            [imageViewFeedback setBackgroundColor:APP_INCORRECT_COLOR];
            [imageViewFeedbackSmall setImage:[UIImage imageNamed:@"wrong-symbol"]];
            
            self.layer.cornerRadius = 4.0;
            self.layer.borderColor = APP_INCORRECT_COLOR.CGColor;
            self.layer.borderWidth = 2.0;
        }
        //        imageViewFeedbackSmall.center = viewFeedback.center;
        
    }
    
}


- (void)hideAnswer {
    
    [UIView animateWithDuration:0.5 animations:^{
        
        if (!self.isLearnOrAnswer) {
            
            //            viewContainerTargetWord.frame = CGRectMake(viewContainerTargetWord.x, self.frame.size.height, viewContainerTargetWord.width, viewContainerTargetWord.height);
            
        }
        else {
            
            viewContainerTargetWord.frame = CGRectMake(viewContainerTargetWord.x, -40, viewContainerTargetWord.width, viewContainerTargetWord.height);
            viewContainerSourceText.frame = CGRectMake(viewContainerSourceText.x, self.frame.size.height, viewContainerSourceText.width, viewContainerSourceText.height);
            
        }
    }
                     completion:^(BOOL finished) {
                         
                         if (self.isLearnOrAnswer) {
                             viewContainerTargetWord.hidden = YES;
                             viewContainerSourceText.hidden = YES;
                             
                         }
                         
                     }];
    
    viewFeedback.hidden = YES;
    answerWasShown = NO;
    
    if (!isOneImageLayout) {
        self.layer.cornerRadius = 4.0;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.borderWidth = 1.0;
    }
    else {
        
        self.layer.borderWidth = 0.0;
        
    }
}

@end
