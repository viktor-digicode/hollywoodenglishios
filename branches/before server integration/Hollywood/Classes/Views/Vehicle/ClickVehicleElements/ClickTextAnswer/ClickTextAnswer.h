//
//  ClickTextAnswer.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 4/7/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ClickTextAnswerDelegate <NSObject>

@optional

- (void)textAnswerWasClicked:(id)sender correctAnswer:(BOOL)status;

@end



@interface ClickTextAnswer : UIView

@property (nonatomic, strong) id<ClickTextAnswerDelegate> delegateClickText;

//data
@property (nonatomic, assign) BOOL answerWasShown;
@property (nonatomic, assign) BOOL isAnswer;

@property (nonatomic, strong) id dataAnswer;


//visual elements
@property (nonatomic, strong) IBOutlet UIImageView *imageViewContent;
@property (nonatomic, strong) IBOutlet UIView *viewContainerLabelWord;
@property (nonatomic, strong) IBOutlet UILabel *labelWord;
@property (nonatomic, strong) IBOutlet UIImageView *imageViewFeedback;
@property (nonatomic, strong) IBOutlet UIButton *buttonSound;


- (instancetype)initWithFrame:(CGRect)frame data:(id)data type:(BOOL)type;
- (void)showFeedbackWithStatus:(BOOL)correct;
- (void)hideAnswer;
- (void)showFeedbackWithStatus;


@end
