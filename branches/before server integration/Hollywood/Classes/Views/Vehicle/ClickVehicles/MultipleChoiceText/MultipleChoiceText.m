//
//  MultipleChoiceText.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 4/7/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "MultipleChoiceText.h"

@implementation MultipleChoiceText
@synthesize imageViewQuestion;
@synthesize viewAnswerContent, viewQuestionContent;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data isOrdered:(BOOL)type {
    
    if (self = [super initWithFrame:frame]) {
        
        //attempts to answer
        self.nAttemptsLeft = 2;
        
        self.dataMultiChoiceTextInstance = data;
        self.isOrdered = type;
        self.afterAudioString = [data objectForKey:@"afterAudio"];
        self.videoUrlString = [data objectForKey:@"videoUrlString"];
        if(self.videoUrlString){
            [self createQuestionVideo];
        }else{
            [self createQuestionImage];
        }
        
        [self createLayout:[data objectForKey:@"answers"]];
        
        
    }
    
    return self;
}

#pragma mark - Create elements
- (void)createQuestionVideo {
    /*self.videoPlayer = [[KSVideoPlayerView alloc] initWithFrame:CGRectMake(0, -25, self.frame.size.width, self.frame.size.height+50)
     contentURL:[NSURL URLWithString:self.videoUrlString]];*/
    self.videoPlayer = [[KSVideoPlayerView alloc] initWithFrame:CGRectMake(50, 20, DEVICE_SMALL_SIDE-64-90, DEVICE_SMALL_SIDE-64-90)
                                                     contentURL:[NSURL URLWithString:self.videoUrlString]];
    self.videoPlayer.delegate = self;
    self.videoPlayer.automaticGoToNextVehicle = YES;
    [self addSubview:self.videoPlayer];
    self.videoPlayer.tintColor = [UIColor redColor];
    //[self.videoPlayer play];
    viewQuestionContent = [[UIView alloc] initWithFrame:self.videoPlayer.frame];
    
}

- (void)createQuestionImage {
    
    UIImage *originalImage = [UIImage imageNamed:[self.dataMultiChoiceTextInstance objectForKey:@"bigImage"]];
    UIImage *newImage = [UIImage imageWithImage:originalImage scaledToHeight:DEVICE_SMALL_SIDE-64-90];
    
    CGFloat contentQuestionWidth = newImage.size.width;
    CGFloat contentQuestionHeight = newImage.size.height;
    
    NSLog(@"New width - %f, new height - %f", newImage.size.width, newImage.size.height);
    
    float answerContentWidth = (DEVICE_BIGER_SIDE-2*INSTANCE_START_POSITION_X-20)/4;
    if (newImage.size.width > 3*answerContentWidth) {
        
        contentQuestionWidth = 3*answerContentWidth;
    }
    
    if (viewQuestionContent == nil) {
        
        viewQuestionContent = [[UIView alloc] initWithFrame:CGRectMake(INSTANCE_START_POSITION_X, 20, contentQuestionWidth, contentQuestionHeight)];
        [viewQuestionContent setBackgroundColor:[UIColor blackColor]];
        [self addSubview:viewQuestionContent];
        
        viewQuestionContent.layer.cornerRadius = 4.0;
        viewQuestionContent.layer.borderWidth = 1.0;
        viewQuestionContent.layer.borderColor = [UIColor whiteColor].CGColor;
        viewQuestionContent.layer.masksToBounds = YES;
    }
    
    if (imageViewQuestion == nil) {
        
        imageViewQuestion = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, contentQuestionWidth-6, contentQuestionHeight-6)];
        [imageViewQuestion setImage:newImage];
        [imageViewQuestion setContentMode:UIViewContentModeScaleAspectFit];
        [viewQuestionContent addSubview:imageViewQuestion];
        
        //        imageViewQuestion.layer.cornerRadius = 4.0;
        //        imageViewQuestion.layer.masksToBounds = YES;
        
    }
}

- (void)createLayout:(NSArray *)arrayAnswers {
    
    //Create answer content view
    
    CGFloat questionContentWidth = viewQuestionContent.size.width;
    CGFloat answerContentWidth = (DEVICE_BIGER_SIDE-2*INSTANCE_START_POSITION_X-SPACE_QUESTIONS_ANSWERS) - questionContentWidth;
    float answerContentHeight = DEVICE_SMALL_SIDE-64-90;
    
    if (viewAnswerContent == nil) {
        
        viewAnswerContent = [[UIView alloc] initWithFrame:CGRectMake(INSTANCE_START_POSITION_X+questionContentWidth+SPACE_QUESTIONS_ANSWERS, 20, answerContentWidth, answerContentHeight)];
        [viewAnswerContent setBackgroundColor:[UIColor clearColor]];
        [self addSubview:viewAnswerContent];
    }
    
    NSInteger numberOfAnswers = [arrayAnswers count];
    
    NSMutableArray *arryRandomNumbers=[[NSMutableArray alloc]init];
    while (arryRandomNumbers.count<numberOfAnswers) {
        NSInteger randomNumber=arc4random()%numberOfAnswers;
        if (![arryRandomNumbers containsObject:[NSString stringWithFormat:@"%ld",randomNumber]]) {
            
            [arryRandomNumbers addObject:[NSString stringWithFormat:@"%ld",randomNumber]];
        }
        continue;
    }
    
    
    //Create answers
    ClickTextAnswer *clickTextAnswer;
    NSInteger order = 0;
    CGFloat answerWidth = answerContentWidth;
    float answerHeight = (answerContentHeight-3*5)/4;
    
    id dataClickImage = nil;
    NSInteger index;
    
    for (int i=0; i<numberOfAnswers; i++) {
        
        index = [[arryRandomNumbers objectAtIndex:i] integerValue];
        dataClickImage = [arrayAnswers objectAtIndex:index];
        
        clickTextAnswer = [[ClickTextAnswer alloc] initWithFrame:CGRectMake(0, order*(answerHeight+5), answerWidth, answerHeight) data:dataClickImage type:YES];
        clickTextAnswer.delegateClickText = self;
        [clickTextAnswer setBackgroundColor:[UIColor clearColor]];
        
        [viewAnswerContent addSubview:clickTextAnswer];
        
        order++;
    }
    
}

#pragma mark - ClickTextAnswer Delegate

- (void)textAnswerWasClicked:(id)sender correctAnswer:(BOOL)status {
    
    if  (!self.isCurrent) {
        return;
    }
    if (self.isCompleted) {
        return;
    }
    
    if (self.nAttemptsLeft<=0 || sender == self.currentAnswer) {
        return;
    }
    
    self.currentAnswer = (ClickTextAnswer *)sender;
    
    if (!status) {
        
        self.isAnsweredCorrect = NO;
        [self showIncorrectFeedback];
        [self.currentAnswer showFeedbackWithStatus];
        self.nAttemptsLeft--;
        
        if (self.nAttemptsLeft == 0) {
            
            self.isCompleted = YES;
            
        }
        else {
            
            //hide after 2 seconds
            NSLog(@"Hide all FEEDBACKS");
            [self performSelector:@selector(hideAllFeedbacks) withObject:nil afterDelay:1.0];
            return;
        }
        
    }
    else {
        
        self.isAnsweredCorrect = YES;
        self.isCompleted = YES;
        [self showCorrectFeedback];
        
        [self.currentAnswer showFeedbackWithStatus];
        
    }
    
    
    for (ClickTextAnswer *answer in self.viewAnswerContent.subviews) {
        
        if (self.isCompleted) {
            break;
        }
        if ([answer isKindOfClass:[ClickTextAnswer class]] && answer != self.currentAnswer) {
            
            [answer hideAnswer];
        }
    }
    
}

- (void)hideAllFeedbacks {
    
    if (self.isAnsweredCorrect) {
        return;
    }
    for (ClickTextAnswer *answer in self.viewAnswerContent.subviews) {
        
        if (self.isCompleted) {
            break;
        }
        if ([answer isKindOfClass:[ClickTextAnswer class]]) {
            
            [answer hideAnswer];
        }
    }
    
    self.currentAnswer = nil;
}

- (void)selectCorrectAnswer {
    
    for (ClickTextAnswer *answer in self.viewAnswerContent.subviews) {
        
        if ([answer isKindOfClass:[ClickTextAnswer class]]) {
            
            if ([[[answer dataAnswer] objectForKey:@"isCorrect"] boolValue])
                
                [answer showFeedbackWithStatus];
        }
    }
    
}


- (void)answerWasClicked:(id)sender changeImage:(NSString *)image {
    
    //    if ((self.nAttemptsLeft<=0 && !self.isLearnVehicle) || sender == self.currentAnswer) {
    //        return;
    //    }
    //
    //    self.currentAnswer = (ClickImageView *)sender;
    //    for (ClickImageView *answer in self.viewAnswerContent.subviews) {
    //
    //        if ([answer isKindOfClass:[ClickImageView class]] && answer != self.currentAnswer) {
    //
    //            [answer hideAnswer];
    //        }
    //    }
    //    [imageViewOneImageAnswer setImage:[UIImage imageNamed:image]];
}

- (void)activateVehicle {
    [super activateVehicle];
    if(self.videoPlayer){
        [self.videoPlayer play];
    }
    [self.vehicleDelegate setQuestionBarText:[self.dataMultiChoiceTextInstance objectForKey:@"questionText"]];
    
}

- (void)deactivateVehicle {
    [super deactivateVehicle];
    if(self.videoPlayer){
        [self.videoPlayer stop];
    }
    
    self.nAttemptsLeft = 2;
    self.isCompleted = NO;
    
    self.currentAnswer = nil;
    
    for (ClickTextAnswer *answer in self.viewAnswerContent.subviews) {
        
        if ([answer isKindOfClass:[ClickTextAnswer class]]) {
            
            [answer hideAnswer];
        }
    }
    
    [self makeNewAnswersLayout];
    
}

#pragma mark - Make new random

- (void)makeNewAnswersLayout {
    
    NSInteger numberOfAnswers = [(NSArray *)[self.dataMultiChoiceTextInstance objectForKey:@"answers"] count];
    
    NSMutableArray *arryRandomNumbers = [[NSMutableArray alloc] init];
    while (arryRandomNumbers.count<numberOfAnswers) {
        NSInteger randomNumber=arc4random()%numberOfAnswers;
        if (![arryRandomNumbers containsObject:[NSString stringWithFormat:@"%ld",randomNumber]]) {
            
            [arryRandomNumbers addObject:[NSString stringWithFormat:@"%ld",randomNumber]];
        }
        continue;
    }
    
    
    //Create answers
    NSInteger index = 0;
    NSInteger order = 0;
    
    for (ClickTextAnswer *clickTextAnswer in viewAnswerContent.subviews) {
        
        if ([clickTextAnswer isKindOfClass:[ClickTextAnswer class]]) {
            
            order = [[arryRandomNumbers objectAtIndex:index] integerValue];
            clickTextAnswer.frame = CGRectMake(0, order*(clickTextAnswer.height+5), clickTextAnswer.width, clickTextAnswer.height);
            
            index++;
        }
    }
}


#pragma mark BaseViewVehicle
- (void)finishVehicle{
    [super finishVehicle];
    
    if (self.isAnsweredCorrect) {
        self.isCompleted = YES;
        [self.vehicleDelegate  vehiclePass:YES];
        // old way
        //        [self.vehicleDelegate vehicleWasFinished:self];
        
        if(self.afterAudioString){
            self.vehicleState = KAPlayAfterAudioState;
            [AUDIO_MANAGER playAudio:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:self.afterAudioString ofType:@""]]];
            [AUDIO_MANAGER setDelegate:self];
        }else{
            self.isCompleted = YES;
            [self.vehicleDelegate vehicleWasFinished:self];
            [AUDIO_MANAGER setDelegate:nil];
        }
    }
    else if (self.nAttemptsLeft == 0) {
        
        [self performSelectorOnMainThread:@selector(selectCorrectAnswer) withObject:nil waitUntilDone:YES];
        
        self.isCompleted = YES;
        
        [self performSelector:@selector(callFinishAfterDelay) withObject:nil afterDelay:1.0];
        //[self.vehicleDelegate setQuestionBarText:@""];
    }
    
}

- (void)callFinishAfterDelay {
    
    if(self.afterAudioString){
        self.vehicleState = KAPlayAfterAudioState;
        [AUDIO_MANAGER playAudio:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:self.afterAudioString ofType:@""]]];
        [AUDIO_MANAGER setDelegate:self];
    }else{
        self.isCompleted = YES;
        [self.vehicleDelegate vehicleWasFinished:self];
        [AUDIO_MANAGER setDelegate:nil];
    }
    
    //old way
    //    [self.vehicleDelegate vehicleWasFinished:self];
}

#pragma mark - AVWrapperDelegate

- (void)playbackComplete {
    
    if(self.vehicleState == KAPlayBeforeAudioState){
        
    }else if(self.vehicleState == KAPlayAfterAudioState){
        self.vehicleState = KADefaultState;
        self.isCompleted = YES;
        [self.vehicleDelegate vehicleWasFinished:self];
        //[self.vehicleDelegate setQuestionBarText:@""];
        [AUDIO_MANAGER setDelegate:nil];
    }
}

#pragma mark playerViewDelegate
-(void)hideArrows{
    NSLog(@"hideArrows");
}

-(void)showArrows{
    NSLog(@"showArrows");
    //[self.videoPlayer loadNewUrl:[NSURL URLWithString:@"http://cdn.kantoo.com/Hollywood/Hollywood%20Demo%20-%20part%205.mp4"]];
}
-(void)videoFinishedPlaying{
    NSLog(@"videoFinishedPlaying");
    /*[self removeActivityIndicator];
     [player removeFromSuperview];
     player = nil;
     [self removeSwipeGesture];
     [self gotoNextPage];*/
    
}
-(void)playerLoadVideo{
    NSLog(@"playerLoadVideo");
    //[self removeActivityIndicator];
}
@end
