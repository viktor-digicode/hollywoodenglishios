//
//  VideoVehicle.m
//  Hollywood
//
//  Created by Kiril Kiroski on 3/24/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "VideoVehicle.h"

@implementation VideoVehicle {
    
    __weak IBOutlet UILabel *infoLabel;
    __weak IBOutlet UIButton *tempButton;
    __weak IBOutlet UIView *tempView;
    UIImageView *videoImage;
    KSVideoPlayerView* player;
    UIButton *playPauseButton;
    BOOL highVideoQuality;
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.videoUrlString = @"http://cdn.kantoo.com/Hollywood/Hollywood%20Demo%20-%20part%205.mp4";
        videoImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"videoBg"]];
        [self addSubview:videoImage];
        [videoImage setContentMode:UIViewContentModeScaleAspectFill];
        [[videoImage layer]setMasksToBounds:TRUE];
        videoImage.clipsToBounds = YES;
        [[videoImage layer] setCornerRadius:2.0f];
        [[videoImage layer] setBorderWidth:0.9f];
        [[videoImage layer] setBorderColor:[UIColor whiteColor].CGColor];
        
        playPauseButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        playPauseButton.frame = CGRectMake(-22+[self width]/2, -22+[self height]/2, 44, 44);
        [playPauseButton addTarget:self action:@selector(playButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [playPauseButton setBackgroundImage:[UIImage imageNamed:@"videoPause"] forState:UIControlStateSelected];
        [playPauseButton setBackgroundImage:[UIImage imageNamed:@"videoPlay"] forState:UIControlStateNormal];
        [playPauseButton setTintColor:[UIColor grayColor]];
        [self addSubview:playPauseButton];
        
    }
    
    [self setupConstraints];
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame withData:(id)vehicleData {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.videoVehicleData = vehicleData;
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame withData:(id)vehicleData isHighQuality:(BOOL)highQuality {
    
    self = [self initWithFrame:frame withData:vehicleData];
    if (self) {
        
        highVideoQuality = highQuality;
    }
    return self;
}

- (void)setupConstraints{
    [videoImage autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:20];
    [videoImage autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:20];
    [videoImage autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:70];
    [videoImage autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:70];
    
    [playPauseButton autoCenterInSuperview];
    [playPauseButton autoSetDimensionsToSize:CGSizeMake([playPauseButton width], [playPauseButton height])];
}
- (void)activateVehicle{
    [self.vehicleDelegate setQuestionBarText:@"Play video"];
    
    [super activateVehicle];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.vehicleDelegate showVideoWithUrl:self.videoUrlString  and:self.goNextAfterVideoFinish];
    });
    
    
    /*if ([_vehicleData objectForKey:@"questionTextVocabs"]) {
     
     if ([[_vehicleData objectForKey:@"questionTextVocabs"] boolValue]) {
     [self.vehicleDelegate setQuestionBarTextWithVocab:[_vehicleData objectForKey:@"questionText"]];
     
     }
     else {
     [self.vehicleDelegate setQuestionBarText:[_vehicleData objectForKey:@"questionText"]];
     
     }
     }
     else {
     [self.vehicleDelegate setQuestionBarText:[_vehicleData objectForKey:@"questionText"]];
     }*/
    //[NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(playVideo) userInfo:nil repeats:NO];
    //[self.vehicleDelegate showVideoWithUrl:@"http://cdn.kantoo.com/ecb/1/1434376626793/22279_lv1_l1_live_jose_v2.mp4" and:self.goNextAfterVideoFinish];
}
- (void)playVideo{
    //[self.vehicleDelegate showVideoWithUrl:@"http://cdn.kantoo.com/Hollywood/Hollywood%20Demo%20-%20part%202.mp4" and:self.goNextAfterVideoFinish];
    [self.vehicleDelegate showVideoWithUrl:self.videoUrlString and:self.goNextAfterVideoFinish];
}

-(void)zoomVideoPlayer{
    player.frame = [UIScreen mainScreen].bounds;
}

-(void)playButtonAction{
    
}
@end
