//
//  SwipeDestinationView.m
//  NavigationHollywood
//
//  Created by Dimitar Shopovski on 3/21/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import "SwipeDestinationView.h"
#import "NSString+Extras.h"

@implementation SwipeDestinationView

/*- (void)drawRect:(CGRect)rect {
 
 if (_isTextElement) {
 
 labelAnswer = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 250)];
 [self addSubview:labelAnswer];
 labelAnswer.textColor = [UIColor whiteColor];
 
 }
 else {
 
 imageViewAnswer = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
 [self addSubview:imageViewAnswer];
 }
 
 }*/


- (void)setDestinationViewData:(id)data {
    
    if (_isTextElement) {
        
        labelAnswer.text = [data objectForKey:@"textAnswer"];
    }
    else {
        
        imageViewAnswer.image = [UIImage imageNamed:[data objectForKey:@"imageAnswer"]];
    }
}
- (int) wordCount:(NSString*)newString
{
    NSArray *words = [newString componentsSeparatedByString:@""];
    return (int)[words count];
}

- (void)setText:(NSString*)newText{
    
    _isTextElement= YES;
    labelAnswer.text = newText;
    NSDictionary *fontDictionary = [FontManager takeFontSize:newText forVehicle:KASwipeVehicleSwipePictureType];
    /*int fontSize = [[fontDictionary objectForKey:@"size"] floatValue];
    if(DEVICE_BIGER_SIDE < 568){
        fontSize -= 8;
    }*
    labelAnswer.font = [UIFont systemFontOfSize:fontSize];*/
    if([newText wordCount] == 1){
        labelAnswer.numberOfLines = 1;
    }
    [labelAnswer fontSizeToFit];
    
    /* labelAnswer.numberOfLines = 4;
     labelAnswer.adjustsFontSizeToFitWidth = YES;
     labelAnswer.lineBreakMode = NSLineBreakByWordWrapping;*/
}

- (void)setImage:(NSString*)imgString{
    
    imageViewAnswer.image = [UIImage imageNamed:imgString];
}

- (void)makeTextLabel:(CGRect)frame{
   /* labelAnswer = [[UILabel alloc] initWithFrame:frame];
    labelAnswer.textAlignment = NSTextAlignmentCenter;
    labelAnswer.backgroundColor = [UIColor clearColor];
    labelAnswer.numberOfLines = 4;
    labelAnswer.textColor = [UIColor whiteColor];
    labelAnswer.lineBreakMode = NSLineBreakByWordWrapping;
    //labelAnswer.font = [UIFont systemFontOfSize:30];
    [self addSubview:labelAnswer];*/
    labelAnswer = [[UILabel alloc] initWithFrame:frame];
    labelAnswer.textAlignment = NSTextAlignmentCenter;
    labelAnswer.lineBreakMode = NSLineBreakByWordWrapping;
    labelAnswer.backgroundColor = [UIColor clearColor];
    labelAnswer.numberOfLines = 0;
    labelAnswer.textColor = [UIColor whiteColor];
    labelAnswer.font = [UIFont systemFontOfSize:30];
    [self addSubview:labelAnswer];
    [labelAnswer fontSizeToFit];
    
    [labelAnswer autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:10.0f];
    [labelAnswer autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:10.0f];
    
    if(DEVICE_BIGER_SIDE < 568){
        [labelAnswer autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:6.0f];
        [labelAnswer autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:6.0f];
    }else{
        [labelAnswer autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:12.0f];
        [labelAnswer autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:12.0f];
    }
    
    
}

- (void)makeImage:(CGRect)frame{
    imageViewAnswer = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    [imageViewAnswer setContentMode:UIViewContentModeScaleAspectFill];
    
    
    [self addSubview:imageViewAnswer];
    [imageViewAnswer autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
    [imageViewAnswer autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
    [imageViewAnswer autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [imageViewAnswer autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
}
@end
