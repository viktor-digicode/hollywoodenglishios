//
//  QuizVehicleMultiText.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 2/2/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "QuizVehicleMultiText.h"

@implementation QuizVehicleMultiText
@synthesize imageViewQuestion;
@synthesize viewAnswerContent, viewQuestionContent;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data position:(NSInteger)position {
    
    if (self = [super initWithFrame:frame]) {
        
        //attempts to answer
        self.nAttemptsLeft = 1;
        self.positionInQuiz = position;
        
        self.dataMultiChoiceTextInstance = data;
        
        [self createQuestionImage];
        
        NSArray *arrAnswers = [[(QuizInstance *)self.dataMultiChoiceTextInstance answersForInstance] allObjects];
        [self createLayout:arrAnswers];
        
        
    }
    
    return self;
}

#pragma mark - Create elements

- (void)createQuestionImage {
    
    UIImage *originalImage = [CACHE_MANAGER getImageFromCache:[self.dataMultiChoiceTextInstance bigImageUrl]];
    
    UIImage *newImage = [UIImage imageWithImage:originalImage scaledToHeight:DEVICE_SMALL_SIDE-64-90];
    
    CGFloat contentQuestionWidth = newImage.size.width;
    CGFloat contentQuestionHeight = newImage.size.height;
    
    NSLog(@"New width - %f, new height - %f", newImage.size.width, newImage.size.height);
    
    float answerContentWidth = (DEVICE_BIGER_SIDE-2*INSTANCE_START_POSITION_X-20)/4;
    if (newImage.size.width > 3*answerContentWidth) {
        
        contentQuestionWidth = 3*answerContentWidth;
    }
    
    if (viewQuestionContent == nil) {
        
        viewQuestionContent = [[UIView alloc] initWithFrame:CGRectMake(INSTANCE_START_POSITION_X, 20, contentQuestionWidth, contentQuestionHeight)];
        [viewQuestionContent setBackgroundColor:[UIColor blackColor]];
        [self addSubview:viewQuestionContent];
        
        viewQuestionContent.layer.cornerRadius = 4.0;
        viewQuestionContent.layer.borderWidth = 1.0;
        viewQuestionContent.layer.borderColor = [UIColor whiteColor].CGColor;
        viewQuestionContent.layer.masksToBounds = YES;
    }
    
    if (imageViewQuestion == nil) {
        
        imageViewQuestion = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, contentQuestionWidth-6, contentQuestionHeight-6)];
        [imageViewQuestion setImage:newImage];
        [imageViewQuestion setContentMode:UIViewContentModeScaleAspectFit];
        [viewQuestionContent addSubview:imageViewQuestion];
        
    }
}

- (void)createLayout:(NSArray *)arrayAnswers {
    
    //Create answer content view
    
    CGFloat questionContentWidth = viewQuestionContent.size.width;
    CGFloat answerContentWidth = (DEVICE_BIGER_SIDE-2*INSTANCE_START_POSITION_X-SPACE_QUESTIONS_ANSWERS) - questionContentWidth;
    float answerContentHeight = DEVICE_SMALL_SIDE-64-90;
    
    if (viewAnswerContent == nil) {
        
        viewAnswerContent = [[UIView alloc] initWithFrame:CGRectMake(INSTANCE_START_POSITION_X+questionContentWidth+SPACE_QUESTIONS_ANSWERS, 20, answerContentWidth, answerContentHeight)];
        [viewAnswerContent setBackgroundColor:[UIColor clearColor]];
        [self addSubview:viewAnswerContent];
    }
    
    NSInteger numberOfAnswers = [arrayAnswers count];
    
    NSMutableArray *arryRandomNumbers=[[NSMutableArray alloc]init];
    while (arryRandomNumbers.count<numberOfAnswers) {
        NSInteger randomNumber=arc4random()%numberOfAnswers;
        if (![arryRandomNumbers containsObject:[NSString stringWithFormat:@"%ld",randomNumber]]) {
            
            [arryRandomNumbers addObject:[NSString stringWithFormat:@"%ld",randomNumber]];
        }
        continue;
    }
    
    
    //Create answers
    ClickTextAnswer *clickTextAnswer;
    NSInteger order = 0;
    CGFloat answerWidth = answerContentWidth;
    float answerHeight = (answerContentHeight-3*5)/4;
    
    id dataClickImage = nil;
    NSInteger index;
    
    for (int i=0; i<numberOfAnswers; i++) {
        
        index = [[arryRandomNumbers objectAtIndex:i] integerValue];
        dataClickImage = [arrayAnswers objectAtIndex:index];
        
        clickTextAnswer = [[ClickTextAnswer alloc] initWithFrame:CGRectMake(0, order*(answerHeight+5), answerWidth, answerHeight) data:dataClickImage type:YES];
        clickTextAnswer.delegateClickText = self;
        [clickTextAnswer setBackgroundColor:[UIColor clearColor]];
        
        [viewAnswerContent addSubview:clickTextAnswer];
        
        order++;
    }
    
}

#pragma mark - ClickTextAnswer Delegate

- (void)textAnswerWasClicked:(id)sender correctAnswer:(BOOL)status {
    
    if  (!self.isCurrent) {
        return;
    }
    if (self.isCompleted) {
        return;
    }
    
    if (self.nAttemptsLeft<=0 || sender == self.currentAnswer) {
        return;
    }
    
    self.currentAnswer = (ClickTextAnswer *)sender;
    
    if (!status) {
        
        self.isAnsweredCorrect = NO;
        [self showIncorrectFeedback];
        [self.currentAnswer showFeedbackWithStatus];
        self.nAttemptsLeft--;
        
        if (self.nAttemptsLeft == 0) {
            
            self.isCompleted = YES;
            
        }
        else {
            
            //hide after 2 seconds
            NSLog(@"Hide all FEEDBACKS");
            [self performSelector:@selector(hideAllFeedbacks) withObject:nil afterDelay:1.0];
            return;
        }
        
    }
    else {
        
        self.isAnsweredCorrect = YES;
        self.isCompleted = YES;
        [self showCorrectFeedback];
        
        [self.currentAnswer showFeedbackWithStatus];
        
    }
    
    
    for (ClickTextAnswer *answer in self.viewAnswerContent.subviews) {
        
        if (self.isCompleted) {
            break;
        }
        if ([answer isKindOfClass:[ClickTextAnswer class]] && answer != self.currentAnswer) {
            
            [answer hideAnswer];
        }
    }
    
}

- (void)hideAllFeedbacks {
    
    if (self.isAnsweredCorrect) {
        return;
    }
    for (ClickTextAnswer *answer in self.viewAnswerContent.subviews) {
        
        if (self.isCompleted) {
            break;
        }
        if ([answer isKindOfClass:[ClickTextAnswer class]]) {
            
            [answer hideAnswer];
        }
    }
    
    self.currentAnswer = nil;
}

- (void)selectCorrectAnswer {
    
    for (ClickTextAnswer *answer in self.viewAnswerContent.subviews) {
        
        if ([answer isKindOfClass:[ClickTextAnswer class]]) {
            
            if ([[answer dataAnswer] isKindOfClass:[QuizInstanceAnswer class]]) {
                
                if ([(QuizInstanceAnswer *)[answer dataAnswer] isCorrect])
                    
                    [answer showFeedbackWithStatus];
                
            }
            else {
                if ([[[answer dataAnswer] objectForKey:@"isCorrect"] boolValue])
                    
                    [answer showFeedbackWithStatus];
                
            }
        }
    }
    
}

- (void)activateVehicle {
    
    [super activateVehicle];
    
    [self.vehicleDelegate setQuestionBarText:[self.dataMultiChoiceTextInstance instanceQuestion]];
    
}

- (void)deactivateVehicle {
    [super deactivateVehicle];
    
    self.nAttemptsLeft = 1;
    self.isCompleted = NO;
    
    self.currentAnswer = nil;
    
    for (ClickTextAnswer *answer in self.viewAnswerContent.subviews) {
        
        if ([answer isKindOfClass:[ClickTextAnswer class]]) {
            
            [answer hideAnswer];
        }
    }
    
    [self makeNewAnswersLayout];
    
}

#pragma mark - Make new random

- (void)makeNewAnswersLayout {
    
    NSArray *arrAnswers = [[(QuizInstance *)self.dataMultiChoiceTextInstance answersForInstance] allObjects];
    NSInteger numberOfAnswers = [arrAnswers count];
    
    NSMutableArray *arryRandomNumbers = [[NSMutableArray alloc] init];
    while (arryRandomNumbers.count<numberOfAnswers) {
        NSInteger randomNumber=arc4random()%numberOfAnswers;
        if (![arryRandomNumbers containsObject:[NSString stringWithFormat:@"%ld",randomNumber]]) {
            
            [arryRandomNumbers addObject:[NSString stringWithFormat:@"%ld",randomNumber]];
        }
        continue;
    }
    
    //Create answers
    NSInteger index = 0;
    NSInteger order = 0;
    
    for (ClickTextAnswer *clickTextAnswer in viewAnswerContent.subviews) {
        
        if ([clickTextAnswer isKindOfClass:[ClickTextAnswer class]]) {
            
            order = [[arryRandomNumbers objectAtIndex:index] integerValue];
            clickTextAnswer.frame = CGRectMake(0, order*(clickTextAnswer.height+5), clickTextAnswer.width, clickTextAnswer.height);
            
            index++;
        }
    }
}


#pragma mark BaseViewVehicle
- (void)finishVehicle{
    [super finishVehicle];
    
    if (self.isAnsweredCorrect) {
        self.isCompleted = YES;
        [self.vehicleDelegate vehicleWasFinished:self];
        //[self.vehicleDelegate setQuestionBarText:@""];
        
    }
    else if (self.nAttemptsLeft == 0) {
        
        [self performSelectorOnMainThread:@selector(selectCorrectAnswer) withObject:nil waitUntilDone:YES];
        
        self.isCompleted = YES;
        
        [self performSelector:@selector(callFinishAfterDelay) withObject:nil afterDelay:1.0];
        //[self.vehicleDelegate setQuestionBarText:@""];
    }
    
}

- (void)callFinishAfterDelay {
    
    [self.vehicleDelegate vehicleWasFinished:self];
}

@end
