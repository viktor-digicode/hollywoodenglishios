//
//  VoiceRecoginitionVehicle.m
//  Hollywood
//
//  Created by Kiril Kiroski on 4/18/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "VoiceRecoginitionVehicle.h"
#import <AFNetworking/AFURLSessionManager.h>
#import <AFNetworking/AFHTTPSessionManager.h>
#import <objective-zip/OZZipFile.h>
#import <objective-zip/OZZipFileMode.h>
#import "Objective-Zip.h"
#import "Objective-Zip+NSError.h"
#import "FLAnimatedImage.h"

@implementation VoiceRecoginitionVehicle{
    id vehicleData;
    UIImageView *vehicleImageView;
    UIImageView *buttonsBackgroundImageView;
    UIImageView *labelBackgroundImageView;
    
    UILabel *textLabel;
    UILabel *signLabel;
    UIView *labelBackground;
    UIView *buttonsBackground;
    UIView *blackBackground;
    UIView *rightView;
    UIButton *recordButton;
    UIButton *playSoundButton;
    UIButton *playRecordetAudio;
    
    BOOL soundPlay;
    NSString *audioDataUrl;
    NSString *audioUrl;
    NSTimer *stopRecordSoundTimer;
    NSString *recordingString;
    
    FLAnimatedImageView *recordetImageView;
    FLAnimatedImageView *playRecordetImageView;
    int removeIndex;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (instancetype)initWithFrame:(CGRect)frame data:(id)data {
    
    if (self = [super initWithFrame:frame]) {
        
        vehicleData = data;
        
        recordingString = [vehicleData objectForKey:@"recordingText"];
        //self.backgroundColor = [UIColor purpleColor];
        audioDataUrl = [vehicleData objectForKey:@"audioDataUrl"];
        blackBackground = [[UIView alloc] initWithFrame:frame];
        blackBackground.backgroundColor = [UIColor blackColor];
        [blackBackground setHidden:YES];
        [self addSubview:blackBackground];
        
        vehicleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 228, 166)];
        vehicleImageView.image = [UIImage imageNamed:[vehicleData objectForKey:@"image"]];
        [vehicleImageView setContentMode:UIViewContentModeScaleAspectFill];
        [[vehicleImageView layer]setBorderWidth:0.9f];
        [[vehicleImageView layer] setBorderColor:[UIColor whiteColor].CGColor];
        [[vehicleImageView layer]setCornerRadius:2.0f];
        vehicleImageView.layer.masksToBounds = YES;
        
        [self addSubview:vehicleImageView];
        
        
        rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 230, 166)];
        rightView.backgroundColor = [UIColor clearColor];
        [self addSubview:rightView];
        
        labelBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 230, 92)];
        labelBackground.backgroundColor = [UIColor blackColor];
        /*[[labelBackground layer]setBorderWidth:0.9f];
         [[labelBackground layer] setBorderColor:[UIColor whiteColor].CGColor];
         [[labelBackground layer]setCornerRadius:2.0f];
         labelBackground.layer.masksToBounds = YES;*/
        [rightView addSubview:labelBackground];
        
        labelBackgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"textBG"]];
        labelBackgroundImageView.frame = labelBackground.frame;
        [[labelBackgroundImageView layer]setBorderWidth:0.9f];
        [[labelBackgroundImageView layer] setBorderColor:[UIColor whiteColor].CGColor];
        [[labelBackgroundImageView layer]setCornerRadius:2.0f];
        labelBackgroundImageView.layer.masksToBounds = YES;
        [labelBackground addSubview:labelBackgroundImageView];
        
        
        textLabel = [[UILabel alloc]initWithFrame:CGRectMake(5.0, 2.0, [self width]/2-50-45 , [labelBackground height]-2)];
        
        NSDictionary *fontDictionary = [FontManager takeFontSize:recordingString forVehicle:KAVoiceRecoginitionVehicleType];
        int fontSize = [[fontDictionary objectForKey:@"size"] floatValue];
        
        textLabel.numberOfLines = [[fontDictionary objectForKey:@"line"] integerValue];
        textLabel.font = [UIFont systemFontOfSize:fontSize];
        textLabel.textColor = [UIColor whiteColor];
        textLabel.text = recordingString;
        textLabel.textAlignment = NSTextAlignmentCenter;
        textLabel.backgroundColor = [UIColor clearColor];
        [labelBackground addSubview:textLabel];
        
        
        signLabel =  [[UILabel alloc]initWithFrame:CGRectMake(5.0, 2.0, [self width]/2-50-45 , [labelBackground height]-2)];
        signLabel.numberOfLines = [[fontDictionary objectForKey:@"line"] integerValue];
        signLabel.font = [UIFont systemFontOfSize:fontSize];
        signLabel.textColor = [UIColor whiteColor];
        signLabel.textAlignment = NSTextAlignmentCenter;
        //signLabel.text = recordingString;
        signLabel.backgroundColor = [UIColor clearColor];
        [labelBackground addSubview:signLabel];
        
        
        UIColor *color = [UIColor whiteColor];
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setAlignment:NSTextAlignmentLeft];
        [style setLineSpacing:5];
        NSDictionary *attributes = @{ NSForegroundColorAttributeName : color , NSParagraphStyleAttributeName:style};
        NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:recordingString attributes:attributes];
        textLabel.attributedText = newAttString;
        //signLabel.attributedText = newAttString;
        
        //[textLabel sizeToFit];
        //[signLabel sizeToFit];
        
        textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        signLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        //textLabel.text = @"Created new main queue context: <NSManagedObjectContext: 0x7bf7c330>";
        textLabel.numberOfLines = 0;
        textLabel.adjustsFontSizeToFitWidth = YES;
        textLabel.lineBreakMode = NSLineBreakByClipping;
        
        signLabel.numberOfLines = 0;
        signLabel.adjustsFontSizeToFitWidth = YES;
        signLabel.lineBreakMode = NSLineBreakByClipping;
        
        recordButton = [UIButton buttonWithType:UIButtonTypeCustom];
        recordButton.frame = CGRectMake(0, 0, 75, 75);
        [recordButton addTarget:self action:@selector(recordSound:) forControlEvents:UIControlEventTouchUpInside];
        [recordButton setImage:[UIImage imageNamed:@"recordImg"] forState:UIControlStateNormal];
        [recordButton setTintColor:[UIColor clearColor]];
        
        
        buttonsBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 242, 68)];
        buttonsBackground.backgroundColor = [UIColor blackColor];
        [rightView addSubview:buttonsBackground];
        
        buttonsBackgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"butonsBG"]];
        [[buttonsBackgroundImageView layer]setBorderWidth:0.9f];
        [[buttonsBackgroundImageView layer] setBorderColor:[UIColor whiteColor].CGColor];
        [[buttonsBackgroundImageView layer]setCornerRadius:2.0f];
        buttonsBackgroundImageView.layer.masksToBounds = YES;
        [buttonsBackground addSubview:buttonsBackgroundImageView];
        
        playSoundButton = [UIButton buttonWithType:UIButtonTypeCustom];
        playSoundButton.frame = CGRectMake(0, 0, 40, 40);
        [playSoundButton setImage:[UIImage imageNamed:@"soundGoldcopy11"] forState:UIControlStateNormal];
        [playSoundButton addTarget:self action:@selector(playLabelText:) forControlEvents:UIControlEventTouchUpInside];
        [labelBackground addSubview:playSoundButton];
        
        [buttonsBackground addSubview:recordButton];
        
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"Voice_Exc07_01-MIC-3" withExtension:@"gif"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:data];
        recordetImageView = [[FLAnimatedImageView alloc] init];
        recordetImageView.animatedImage = image;
        recordetImageView.frame = CGRectMake([buttonsBackground width]/2 - 25, 9.0, 50, 50);
        [buttonsBackground addSubview:recordetImageView];
        
        playRecordetAudio = [UIButton buttonWithType:UIButtonTypeCustom];
        playRecordetAudio.frame = CGRectMake([buttonsBackground width]- 9, 9.0, 50, 50);
        [playRecordetAudio addTarget:self action:@selector(playRecordetAudio:) forControlEvents:UIControlEventTouchUpInside];
        [playRecordetAudio setImage:[UIImage imageNamed:@"playRecordetAudio"] forState:UIControlStateNormal];
        [playRecordetAudio setTintColor:[UIColor clearColor]];
        [playRecordetAudio setHidden:YES];
        [buttonsBackground addSubview:playRecordetAudio];
        
        [self setupConstraints];
        /*
         //za test da ne zboram
         NSArray *testArray = @[@{@"flag":@0,
         @"word":@"They"},
         @{@"flag":@1,
         @"word":@"have"},
         @{@"flag":@1,
         @"word":@"dog."}];
         
         
         [self chekResults:testArray];
         
         NSMutableAttributedString *myStringTest= [[NSMutableAttributedString alloc] initWithString:@"Thea have dog."];
         //textLabel.attributedText = myStringTest;
         */
    }
    
    return self;
}

- (void)setupConstraints{
    [blackBackground autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:-100];
    [blackBackground autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
    [blackBackground autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [blackBackground autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    
    
    [vehicleImageView autoSetDimension:ALDimensionWidth toSize:[self width]/2-50];
    [vehicleImageView autoSetDimension:ALDimensionHeight toSize:[self height]-40];
    [vehicleImageView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:45];
    [vehicleImageView autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    
    [rightView autoSetDimension:ALDimensionWidth toSize:[self width]/2-50];
    [rightView autoSetDimension:ALDimensionHeight toSize:[self height]-40];
    [rightView autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:45];
    [rightView autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    
    
    [buttonsBackground autoSetDimension:ALDimensionWidth toSize:[self width]/2-50];
    [buttonsBackground autoSetDimension:ALDimensionHeight toSize:[buttonsBackground height]];
    [buttonsBackground autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
    [buttonsBackground autoAlignAxisToSuperviewAxis:ALAxisVertical];
    
    float labelBackgroundHeight = [self height]-40-10-[buttonsBackground height];
    [labelBackground autoSetDimension:ALDimensionWidth toSize:[self width]/2-50];
    [labelBackground autoSetDimension:ALDimensionHeight toSize:labelBackgroundHeight];
    [labelBackground autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
    [labelBackground autoAlignAxisToSuperviewAxis:ALAxisVertical];
    
    [labelBackgroundImageView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
    [labelBackgroundImageView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
    [labelBackgroundImageView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [labelBackgroundImageView autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    
    [buttonsBackgroundImageView autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
    [buttonsBackgroundImageView autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
    [buttonsBackgroundImageView autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [buttonsBackgroundImageView autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    
    //[textLabel autoCenterInSuperview];
    
    
    
    
    [textLabel autoSetDimension:ALDimensionWidth toSize:[textLabel width]];
    if(DEVICE_BIGER_SIDE < 568){
        [textLabel autoSetDimension:ALDimensionHeight toSize:labelBackgroundHeight-8];
        [textLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:8.0];
    }else{
        [textLabel autoSetDimension:ALDimensionHeight toSize:labelBackgroundHeight-4];
        [textLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:2.0];
    }
    
    [textLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:5.0];
    
    
    [signLabel autoSetDimension:ALDimensionWidth toSize:[textLabel width]];
    if(DEVICE_BIGER_SIDE < 568){
        [signLabel autoSetDimension:ALDimensionHeight toSize:labelBackgroundHeight-8];
        [signLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:-14.0+6];
    }else{
        [signLabel autoSetDimension:ALDimensionHeight toSize:labelBackgroundHeight-4];
        [signLabel autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:-14.0];
    }
    [signLabel autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0.0];
    
    
    //[textLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
    //[textLabel autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    
    
    //[signLabel autoAlignAxisToSuperviewAxis:ALAxisVertical];
    
    //[signLabel autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    
    [recordButton autoSetDimension:ALDimensionWidth toSize:50.0];
    [recordButton autoSetDimension:ALDimensionHeight toSize:50.0];
    [recordButton autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    [recordButton autoAlignAxisToSuperviewAxis:ALAxisVertical];
    
    [playRecordetAudio autoSetDimension:ALDimensionWidth toSize:50.0];
    [playRecordetAudio autoSetDimension:ALDimensionHeight toSize:50.0];
    [playRecordetAudio autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:9];
    [playRecordetAudio autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:9];
    
    //recordetImageView
    
    [recordetImageView autoSetDimension:ALDimensionWidth toSize:50.0];
    [recordetImageView autoSetDimension:ALDimensionHeight toSize:50.0];
    [recordetImageView autoAlignAxisToSuperviewAxis:ALAxisVertical];
    [recordetImageView autoAlignAxisToSuperviewAxis:ALAxisHorizontal];
    
    /*[playRecordetAudio autoSetDimension:ALDimensionWidth toSize:75.0];
     [playRecordetAudio autoSetDimension:ALDimensionHeight toSize:75.0];
     [playRecordetAudio autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
     [playRecordetAudio autoPinEdge:ALEdgeLeft toEdge:ALEdgeRight ofView:recordButton withOffset:5];*/
    /*if(DEVICE_BIGER_SIDE < 568){
     [playSoundButton autoSetDimension:ALDimensionWidth toSize:35.0];
     [playSoundButton autoSetDimension:ALDimensionHeight toSize:35.0];
     [playSoundButton autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
     [playSoundButton autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:-5];
     }else{*/
    [playSoundButton autoSetDimension:ALDimensionWidth toSize:40.0];
    [playSoundButton autoSetDimension:ALDimensionHeight toSize:40.0];
    [playSoundButton autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [playSoundButton autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
    //}
}

- (void)activateVehicle{
    [super activateVehicle];
    [self.vehicleDelegate setQuestionBarText:@"Record the sentence:"];
    
    [ANALYTICS_MANAGER enterTheVehicleEvent:@"Voice_Start"];
    
    if ([vehicleData objectForKey:@"questionTextVocabs"]) {
        
        if ([[vehicleData objectForKey:@"questionTextVocabs"] boolValue]) {
            [self.vehicleDelegate setQuestionBarTextWithVocab:[vehicleData objectForKey:@"questionText"]];
            
        }
        else {
            [self.vehicleDelegate setQuestionBarText:[vehicleData objectForKey:@"questionText"]];
            
        }
    }
    else {
        [self.vehicleDelegate setQuestionBarText:[vehicleData objectForKey:@"questionText"]];
    }
    //rest
    //signLabel.attributedText = nil;
    //[playRecordetAudio setHidden:YES];
    
}

- (void)deactivateVehicle{
    [super deactivateVehicle];
    
    [ANALYTICS_MANAGER leaveTheVehicleEvent:@"Voice_End" otherData:nil];
    
    if(stopRecordSoundTimer){
        [stopRecordSoundTimer invalidate];
        stopRecordSoundTimer = nil;
    }
    
    [playRecordetAudio setHidden:YES];
    signLabel.attributedText = nil;
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Voice_Exc07_01-MIC-3" withExtension:@"gif"];
    NSData *data = [NSData dataWithContentsOfURL:url];
    FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:data];
    recordetImageView.animatedImage = image;
    
    [recordButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [recordButton addTarget:self action:@selector(recordSound:) forControlEvents:UIControlEventTouchUpInside];
    
    soundPlay = NO;
    [AUDIO_MANAGER setDelegate:nil];
    [AUDIO_MANAGER stopRecording];
    [AUDIO_MANAGER stopPlaying];
    [recordButton setEnabled:YES];
    [blackBackground setHidden:YES];
    [playSoundButton setHidden:NO];
}
#pragma mark Button functions
- (void)playLabelText:(id)sender {
    [AUDIO_MANAGER setDelegate:nil];
    [AUDIO_MANAGER stopPlaying];
    if ([[NSFileManager defaultManager] fileExistsAtPath:[[NSBundle mainBundle] pathForResource:audioDataUrl ofType:@""]]) {
        [AUDIO_MANAGER playAudio:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:audioDataUrl ofType:@""]]];
    }
    // [AUDIO_MANAGER setDelegate:self];
    /*if ([[NSFileManager defaultManager] fileExistsAtPath:audioUrl]) {
     [AUDIO_MANAGER playAudio:[NSURL URLWithString: audioUrl]];
     }else{
     [AUDIO_MANAGER playAudioFromDocumentPath:audioDataUrl];
     }*/
    [AUDIO_MANAGER setDelegate:self];
    
}

#pragma mark record button press

-(void)playRecordetAudio:(id)sender {
    [AUDIO_MANAGER stopPlaying];
    [AUDIO_MANAGER playAudio:[NSURL URLWithString:[self recordingPathForIndex:1 order:1]]];
    
}
-(void)recordSound:(id)sender {
    signLabel.attributedText = nil;
    signLabel.text = @"";
    
    [playRecordetAudio setHidden:YES];
    [self.vehicleDelegate setQuestionBarText:@""];
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Voice_Exc07_02-STOP-1" withExtension:@"gif"];
    NSData *data = [NSData dataWithContentsOfURL:url];
    FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:data];
    recordetImageView.animatedImage = image;
    
    //[AUDIO_MANAGER playSound:kSoundRecord];
    [playSoundButton setHidden:YES];
    [recordButton setEnabled:NO];
    [recordButton setImage:[UIImage imageNamed:@"stop"] forState:UIControlStateNormal];
    [blackBackground setHidden:NO];
    ///
    [AUDIO_MANAGER playAudio:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Tone_5.1_-_Rec_begin_tone.wav" ofType:@""]]];
    [AUDIO_MANAGER setDelegate:nil];
    [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(afterRecordSound) userInfo:nil repeats:NO];
    
}

-(void)afterRecordSound {
    /*if (![ReachabilityHelper reachable]) {
     [UIAlertView alertWithCause:kAlertNoInternetConnectionForFramePresentation];
     return;
     }*/
    //[self.vehicleDelegate disableScrolling:0];
    [self.vehicleDelegate disablePageControlButtons];
    stopRecordSoundTimer =  [NSTimer scheduledTimerWithTimeInterval:kParam027 target:self selector:@selector(stopRecordSound) userInfo:nil repeats:NO];
    [AUDIO_MANAGER setDelegate:self];
    //[AUDIO_MANAGER record:[self recordingPathForIndex:curentFrameDialog order:self.instanceOrder]];
    [AUDIO_MANAGER record:[self recordingPathForIndex:1 order:1]];
    
    [recordButton setEnabled:YES];
    [recordButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [recordButton addTarget:self action:@selector(chekSentence:) forControlEvents:UIControlEventTouchUpInside];
    
}


-(void)afterSendRecord{
    [recordButton.layer removeAllAnimations];
    [playRecordetAudio setHidden:NO];
    [recordButton setImage:[UIImage imageNamed:@"recordImg"] forState:UIControlStateNormal];
    [recordButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [recordButton addTarget:self action:@selector(recordSound:) forControlEvents:UIControlEventTouchUpInside];
    [recordButton setEnabled:YES];
    [blackBackground setHidden:YES];
    [playSoundButton setHidden:NO];
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Voice_Exc07_01-MIC-3" withExtension:@"gif"];
    NSData *data = [NSData dataWithContentsOfURL:url];
    FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:data];
    recordetImageView.animatedImage = image;
}

-(void)stopRecordSound{
    
    [self chekSentence:0];
    
}
- (NSString *)recordingPathForIndex:(int)path order:(NSInteger)order {
    return [[self recordingPath] stringByAppendingPathComponent:
            [NSString stringWithFormat:@"record%ld_%d.lpcm", (long)order,path]];
}
- (NSString *)recordingPath {
    //return [NSString documentsDir];
    NSString *recordingPathUrlString;// =[[NSString documentsDir] stringByAppendingPathComponent:@"GuestRecordings"];
    recordingPathUrlString = [[NSString documentsDir] stringByAppendingPathComponent:@"GuestRecordings"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:recordingPathUrlString]) {
        NSError *error;
        [[NSFileManager defaultManager] createDirectoryAtPath:recordingPathUrlString withIntermediateDirectories:NO attributes:nil error:&error];
    }
    return recordingPathUrlString;
}

#pragma mark red record button press
- (void)chekSentence:(id)sender {
    
    if(stopRecordSoundTimer){
        [stopRecordSoundTimer invalidate];
        stopRecordSoundTimer = nil;
    }
    
    [recordButton setImage:[UIImage imageNamed:@"loading"] forState:UIControlStateNormal];
    [recordButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [recordButton setEnabled:NO];
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Voice_Exc07_03-LOADING-2" withExtension:@"gif"];
    NSData *data = [NSData dataWithContentsOfURL:url];
    FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:data];
    recordetImageView.animatedImage = image;
    
    
    //[self.vehicleDelegate enableScrolling:0];
    [self.vehicleDelegate enablePageControlButtons];
    
    
    [AUDIO_MANAGER stopRecording];
    
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.fromValue = [NSNumber numberWithFloat:0.0f];
    animation.toValue = [NSNumber numberWithFloat: 2*M_PI];
    animation.duration = 1.0f;
    animation.repeatCount = INFINITY;
    [recordButton.layer addAnimation:animation forKey:@"SpinAnimation"];
}

#pragma mark - AVWrapperDelegate

- (void)recordingComplete {
    DLog(@"recordingComplete");
}

- (void)playbackItemComplete {
    //[progressView setProgress:(((progressView.progress * [segmentsArray count]) + 1) / [segmentsArray count])];
}

- (void)playbackComplete {
    soundPlay = NO;
    [AUDIO_MANAGER setDelegate:nil];
}

- (void)playbackPaused {
    // do nothing
}

- (void)recordingCompleteWithRecordFile:(NSURL *)audioUrl {
    DLog(@"recordingCompleteWithRecordFile %@",[audioUrl absoluteString]);
    self.isCurrent = YES;
    [self confirmAudio:audioUrl];
}

#pragma mark send audio to nuance
- (void)confirmAudio:(NSURL *)audioUrl {
    if(!self.isCurrent){
        return;
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        if(!self.isCurrent){
            return;
        }
        __weak VoiceRecoginitionVehicle *instance = self;
        //http://speech-13.la-mark-il.com:8080/SpeechProxy
        //test
        NSString *ECUrl = [NSString stringWithFormat:@"http://speech-13.la-mark-il.com:8080/SpeechProxy/EC/SpeechZip?uid=756178FA-5EB3-454D-AEA3-5E704340D9C7&msisdn=guest&userid=guest&groupID=1&contentID=1&deviceID=756178FA-5EB3-454D-AEA3-5E704340D9C7&words=%@",[NSString encodeToPercentEscapeString:recordingString]];
        
        //real
        /*NSString *ECUrl = [NSString stringWithFormat:@"http://sr-1.kantoo.com:8001/SpeechProxy/EC/SpeechZip?uid=756178FA-5EB3-454D-AEA3-5E704340D9C7&msisdn=guest&userid=guest&groupID=1&contentID=1&deviceID=756178FA-5EB3-454D-AEA3-5E704340D9C7&words=%@",[NSString encodeToPercentEscapeString:recordingString]];*/
        
        ///////// MAke zip
        NSString *documentsDir = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *filePath = [documentsDir stringByAppendingPathComponent:@"test.zip"];
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:NULL];
        
        OZZipFile *zipFile= [[OZZipFile alloc] initWithFileName:filePath
                                                           mode:OZZipFileModeCreate];
        
        
        OZZipWriteStream *stream1 =
        [zipFile writeFileInZipWithName:@"abc.lpcm" fileDate:[NSDate dateWithTimeIntervalSinceNow:-86400.0] compressionLevel:OZZipCompressionLevelBest];
        [stream1 writeData:[[NSData alloc] initWithContentsOfURL:audioUrl]];
        [stream1 finishedWriting];
        [zipFile close];
        
        NSString *fileP = [NSString stringWithFormat:@"file://%@", filePath];
        NSData *zipData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:fileP]];
        int bytes = zipData.length;
        NSMutableData *body = [[NSMutableData alloc] initWithCapacity:bytes];
        [body appendData:[NSData dataWithData:zipData]];
        
        ///////////
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:ECUrl parameters:nil error:nil];
        
        req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
        [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [req setHTTPBody:[body mutableCopy]];
        
        
        [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            if(!self.isCurrent){
                return;
            }
            if (!error) {
                NSLog(@"Reply JSON: %@", responseObject);
                
                if ([responseObject isKindOfClass:[NSDictionary class]]) {
                    NSArray *responseArray = [responseObject objectForKey:kPreferencesCheckedList];
                    [instance chekResults:responseArray];
                }
            } else {
                NSLog(@"Error: %@, %@, %@", error, response, responseObject);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Problem!"
                                                                message:@"We seem to encounter a technical difficulty. Please try again later."
                                                               delegate:nil
                                                      cancelButtonTitle:@"Dismiss"
                                                      otherButtonTitles:nil];
                [alert show];
                [self afterSendRecord];
            }
        }] resume];
        
    });
    
}

- (void)chekResults2:(NSArray *)responseArray {
    
    removeIndex = 0;
    UIColor *color = [UIColor clearColor];
    UIColor *color1 = [UIColor greenColor];
    UIColor *color2 = [UIColor redColor];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setAlignment:NSTextAlignmentLeft];
    [style setLineSpacing:5];
    NSDictionary *attributes = @{ NSForegroundColorAttributeName : color , NSParagraphStyleAttributeName:style};
    NSDictionary *attributes1 = @{ NSForegroundColorAttributeName : color1 , NSParagraphStyleAttributeName:style};
    NSDictionary *attributes2 = @{ NSForegroundColorAttributeName : color2 , NSParagraphStyleAttributeName:style};
    
    
    //NSAttributedString *newAttString = [[NSAttributedString alloc] initWithString:recordingString attributes:attributes];
    //signLabel.attributedText = newAttString;
    
    //NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    //[style setLineSpacing:45];
    //NSDictionary *attributes = @{NSParagraphStyleAttributeName:style};
    NSMutableAttributedString *mutableAttString = [[NSMutableAttributedString alloc] initWithString:@"" attributes:attributes];
    
    for(int i = 0; i < [responseArray count]; i++){
        NSDictionary *object = [responseArray objectAtIndex:i];
        NSString *objectString = [NSString stringWithFormat:@"%@",[object objectForKey:kWord]];
        NSString *blankCharString = @"                                                                                                   ";
        //NSString *blankCharString = @"***************************************************************************************";
        CGFloat halfIndex = ([objectString length])/2;
        CGFloat lastIndex = [objectString length]-1-halfIndex;
        NSString *newString = [objectString substringToIndex:halfIndex];
        NSString *endtring = [objectString substringFromIndex:halfIndex+1];
        //NSString *token = [objectString stringByReplacingCharactersInRange:NSMakeRange(0, [objectString length]) withString:@"*"];
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image = [UIImage imageNamed:@"correctSign"];
        attachment.fileType = @"$";
        if([[object objectForKey:kPreferencesflag] isEqualToNumber:@1]){
            attachment.image = [UIImage imageNamed:@"correctSign"];
            [self andAddPoints:kParam001];
        }else{
            attachment.image = [UIImage imageNamed:@"inCorrectSign"];
        }
        //CGFloat offsetY = +32.0;
        //attachment.bounds = CGRectMake(0, 0, attachment.image.size.width, attachment.image.size.height);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment ];
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",endtring] attributes:attributes];
        NSMutableAttributedString *blankString= [[NSMutableAttributedString alloc] initWithString:newString attributes:attributes];
        [blankString appendAttributedString:attachmentString];
        [blankString  appendAttributedString:myString];
        
        //[mutableAttString appendAttributedString:blankString];
        
        
        /* NSCharacterSet *doNotWant = [[NSCharacterSet characterSetWithCharactersInString:@"123"] invertedSet];
         objectString = [[objectString componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @"  "];//\u00a0
         NSLog(@"%@.", objectString);*/
        int numberOfCharForReplace = 2;
        /*if([objectString length] > 5){
         halfIndex --;
         }*/
        if([objectString length] <= 2){
            //halfIndex = 0;
            numberOfCharForReplace = 1;
            removeIndex ++;
        }
        /*if([objectString length] >= (halfIndex+numberOfCharForReplace+removeIndex)){
         numberOfCharForReplace += removeIndex;
         removeIndex = 0;
         }*/
        NSMutableAttributedString *sampleText;
        if([[object objectForKey:kPreferencesflag] isEqualToNumber:@1]){
            sampleText = [[NSMutableAttributedString alloc] initWithString:objectString attributes:attributes];
            [sampleText replaceCharactersInRange:NSMakeRange(halfIndex, numberOfCharForReplace) withString:@"✓"];
            [sampleText addAttribute:NSForegroundColorAttributeName value:color1 range:NSMakeRange(halfIndex, 1)];
        }else{
            sampleText = [[NSMutableAttributedString alloc] initWithString:objectString attributes:attributes];
            [sampleText replaceCharactersInRange:NSMakeRange(halfIndex, numberOfCharForReplace) withString:@"✕"];
            [sampleText addAttribute:NSForegroundColorAttributeName value:color2 range:NSMakeRange(halfIndex, 1)];
        }
        
        
        
        [mutableAttString insertAttributedString:sampleText atIndex:[mutableAttString length]];
        if([objectString length] > 1){
            [mutableAttString insertAttributedString: [[NSMutableAttributedString alloc] initWithString:@" "] atIndex:[mutableAttString length]];
        }
        
        
    }
    signLabel.attributedText = mutableAttString;
    //textLabel.attributedText = mutableAttString;
    signLabel.numberOfLines = 0;
    signLabel.adjustsFontSizeToFitWidth = YES;
    signLabel.lineBreakMode = NSLineBreakByClipping;
    //signLabel.backgroundColor = [UIColor redColor];
    
    [self afterSendRecord];
}

- (void)chekResults:(NSArray *)responseArray {
    [self.vehicleDelegate  vehiclePass:YES];
    
    removeIndex = 0;
    UIColor *color = [UIColor clearColor];
    UIColor *color1 = [UIColor greenColor];
    UIColor *color2 = [UIColor redColor];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setAlignment:NSTextAlignmentLeft];
    [style setLineSpacing:5];
    NSDictionary *attributes = @{ NSForegroundColorAttributeName : color , NSParagraphStyleAttributeName:style};
    
    NSMutableAttributedString *mutableAttString = [[NSMutableAttributedString alloc] initWithString:@"" attributes:attributes];
    
    for(int i = 0; i < [responseArray count]; i++){
        NSDictionary *object = [responseArray objectAtIndex:i];
        NSString *objectString = [NSString stringWithFormat:@"%@",[object objectForKey:kWord]];
        CGFloat halfIndex = ([objectString length])/2;
        
        int numberOfCharForReplace = 2;
        /*if([objectString length] > 5){
         halfIndex --;
         }*/
        if([objectString length] <= 2){
            //halfIndex = 0;
            numberOfCharForReplace = 1;
            removeIndex ++;
        }
        /*if([objectString length] >= (halfIndex+numberOfCharForReplace+removeIndex)){
         numberOfCharForReplace += removeIndex;
         removeIndex = 0;
         }*/
        NSMutableAttributedString *sampleText;
        if([[object objectForKey:kPreferencesflag] isEqualToNumber:@1]){
            [self andAddPoints:kParam001];
            sampleText = [[NSMutableAttributedString alloc] initWithString:objectString attributes:attributes];
            [sampleText replaceCharactersInRange:NSMakeRange(halfIndex, numberOfCharForReplace) withString:@"✓"];
            [sampleText addAttribute:NSForegroundColorAttributeName value:color1 range:NSMakeRange(halfIndex, 1)];
        }else{
            sampleText = [[NSMutableAttributedString alloc] initWithString:objectString attributes:attributes];
            [sampleText replaceCharactersInRange:NSMakeRange(halfIndex, numberOfCharForReplace) withString:@"✕"];
            [sampleText addAttribute:NSForegroundColorAttributeName value:color2 range:NSMakeRange(halfIndex, 1)];
        }
        
        
        
        [mutableAttString insertAttributedString:sampleText atIndex:[mutableAttString length]];
        if([objectString length] > 1){
            [mutableAttString insertAttributedString: [[NSMutableAttributedString alloc] initWithString:@" "] atIndex:[mutableAttString length]];
        }
        
        
    }
    signLabel.attributedText = mutableAttString;
    
    signLabel.numberOfLines = 0;
    signLabel.adjustsFontSizeToFitWidth = YES;
    signLabel.lineBreakMode = NSLineBreakByClipping;
    
    [self afterSendRecord];
}
#pragma mark helpForString
- (NSString *)encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)string, NULL,
                                                                                 (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ", kCFStringEncodingUTF8));
}
@end
