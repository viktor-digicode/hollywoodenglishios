//
//  QuizVehicle.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 10/28/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "QuizVehicle.h"
#import "LMAudioManager.h"

@implementation QuizVehicle
@synthesize imageViewOneImageAnswer;
@synthesize viewAnswerContent;
@synthesize constraintContainterHeight, constraintContainterWidth;
@synthesize isOrdered;
@synthesize arrayCorrectAnswers;

- (void)awakeFromNib {
    
    [super awakeFromNib];
}

- (instancetype)initWithFrame:(CGRect)frame quizVehicleData:(QuizInstance *)data position:(NSInteger)position {
    
    if (self = [self initWithFrame:frame data:data isLearnVehicle:NO]) {
        
        self.nAttemptsLeft = 1;
        self.positionInQuiz = position;
    }
    
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame data:(QuizInstance *)data isLearnVehicle:(BOOL)type {
    
    if (self = [super initWithFrame:frame]) {
        
        isOrdered = YES;
        
        UIView *vv = [self createContentView:CGRectMake(44, 20, frame.size.width-88, frame.size.height-40)];
        [vv setBackgroundColor:[UIColor clearColor]];
        
        [self addSubview:vv];
        
        instanceData = data;
        
        NSArray *arrayAnswers = (NSArray *)[[data answersForInstance] allObjects];
        self.nAttemptsLeft = 1;
        
        CGFloat minFontSize = [self getMinimumFontSize:arrayAnswers isLearn:NO];
        [self createLayout:arrayAnswers minFont:minFontSize];
        
    }
    
    return self;
}


#pragma mark - Helper method return min font size to use

- (CGFloat)getMinimumFontSize:(NSArray *)arrayAnswers isLearn:(BOOL)isLearn {
    
    NSDictionary *fontDictionary;
    CGFloat minFontSize = 100.0;
    CGFloat currentFontSize = 0.0;
    
    for (QuizInstanceAnswer *instanceAnswer in arrayAnswers) {
        
        if (isLearn) {
            fontDictionary = [FontManager takeFontSize:instanceAnswer.answerText forVehicle:KAClickToLearnVehiclesType];
        }
        else {
            fontDictionary = [FontManager takeFontSize:instanceAnswer.answerText forVehicle:KAMultipleChoiceVehiclesType];
        }
        NSInteger numberOfLines = 0;
        
        if (fontDictionary) {
            
            if ([fontDictionary objectForKey:@"line"]) {
                numberOfLines = [[fontDictionary objectForKey:@"line"] integerValue];
            }
            
            if ([fontDictionary objectForKey:@"size"]) {
                currentFontSize = [[fontDictionary objectForKey:@"size"] floatValue];
            }
            
        }
        
        if (numberOfLines && currentFontSize) {
            
            if (currentFontSize < minFontSize) {
                minFontSize = currentFontSize;
            }
        }
    }
    
    
    return minFontSize;
}

- (NSInteger)getLabelLines:(NSArray *)arrayAnswers isLearn:(BOOL)isLearn {
    
    NSDictionary *fontDictionary;
    CGFloat minFontSize = 100.0;
    CGFloat currentFontSize = 0.0;
    
    NSInteger currentLines = 1;
    
    for (QuizInstanceAnswer *instanceAnswer in arrayAnswers) {
        
        if (isLearn) {
            fontDictionary = [FontManager takeFontSize:instanceAnswer.answerText forVehicle:KAClickToLearnVehiclesType];
        }
        else {
            fontDictionary = [FontManager takeFontSize:instanceAnswer.answerText forVehicle:KAMultipleChoiceVehiclesType];
        }
        NSInteger numberOfLines = 0;
        
        if (fontDictionary) {
            
            if ([fontDictionary objectForKey:@"line"]) {
                numberOfLines = [[fontDictionary objectForKey:@"line"] integerValue];
            }
            
            if ([fontDictionary objectForKey:@"size"]) {
                currentFontSize = [[fontDictionary objectForKey:@"size"] floatValue];
            }
            
        }
        
        if (numberOfLines && currentFontSize) {
            
            if (currentFontSize < minFontSize) {
                minFontSize = currentFontSize;
                currentLines = numberOfLines;
            }
        }
    }
    
    
    return currentLines;
}


- (UIView *)createContentView:(CGRect)frame {
    
    if (viewAnswerContent == nil) {
        
        viewAnswerContent = [[UIView alloc] initWithFrame:frame];
        viewAnswerContent.backgroundColor = [UIColor clearColor];
        
    }
    
    return viewAnswerContent;
}



- (void)createLayout:(NSArray *)arrayAnswers minFont:(CGFloat)minFontSize {
    
    ClickImageView *clickImage;
    NSInteger order = 0;
    
    NSInteger numberOfAnswers = [arrayAnswers count];
    
    CGFloat answerWidth = (viewAnswerContent.frame.size.width-(numberOfAnswers-1)*15)/numberOfAnswers;
    
    NSMutableArray *arryRandomNumbers=[[NSMutableArray alloc]init];
    while (arryRandomNumbers.count<numberOfAnswers) {
        NSInteger randomNumber=arc4random()%numberOfAnswers;
        if (![arryRandomNumbers containsObject:[NSString stringWithFormat:@"%ld",randomNumber]])       {
            [arryRandomNumbers addObject:[NSString stringWithFormat:@"%ld",randomNumber]];
        }
        continue;
    }
    
    
    QuizInstanceAnswer *dataClickImage = nil;
    NSInteger index;
    
    for (int i=0; i<numberOfAnswers; i++) {
        
        index = [[arryRandomNumbers objectAtIndex:i] integerValue];
        dataClickImage = [arrayAnswers objectAtIndex:index];
        
        clickImage = [[ClickImageView alloc] initWithFrame:CGRectMake(order*(answerWidth+15), 0, answerWidth, viewAnswerContent.height) data:dataClickImage type:NO isOneImage:NO withFontSize:minFontSize];
        clickImage.delegateClickImageView = self;
        
        if ([dataClickImage isCorrect]) {
            
            [arrayCorrectAnswers addObject:dataClickImage];
        }
        
        [viewAnswerContent addSubview:clickImage];
        
        order++;
    }
    
    if (!isOrdered) {
        self.nCorrectAnswers = [arrayCorrectAnswers count];
    }
    else {
        self.nCorrectAnswers = 1;
    }
    
}

- (void)createOneImageLayout:(id)data minFont:(CGFloat)minFontSize {
    
    imageViewOneImageAnswer = [[UIImageView alloc] initWithFrame:CGRectMake(3, 3, viewAnswerContent.frame.size.width-6, viewAnswerContent.frame.size.height-6)];
    [imageViewOneImageAnswer setImage:[UIImage imageNamed:[data objectForKey:@"bigImage"]]];
    [imageViewOneImageAnswer setContentMode:UIViewContentModeScaleAspectFill];
    [viewAnswerContent addSubview:imageViewOneImageAnswer];
    
    imageViewOneImageAnswer.layer.cornerRadius = 4.0;
    imageViewOneImageAnswer.layer.masksToBounds = YES;
    
    
    
    ClickImageView *clickImage;
    NSInteger order = 0;
    
    NSArray *arrayAnswers = [data objectForKey:@"answers"];
    
    CGFloat answerWidth = viewAnswerContent.frame.size.width/[arrayAnswers count];
    
    for (id dataClickImage in arrayAnswers) {
        
        clickImage = [[ClickImageView alloc] initWithFrame:CGRectMake(order*answerWidth, 0, answerWidth, viewAnswerContent.height) data:dataClickImage type:NO isOneImage:YES withFontSize:minFontSize];
        clickImage.delegateClickImageView = self;
        [clickImage setBackgroundColor:[UIColor clearColor]];
        [viewAnswerContent addSubview:clickImage];
        
        order++;
    }
    
    self.nCorrectAnswers = 1;
    
}

#pragma mark - ClickImageViewDelegate

- (void)answerWasClicked:(id)sender {
    
    if  (!self.isCurrent) {
        return;
    }
    if (self.isCompleted || self.nCorrectAnswers == 0) {
        return;
    }
    
    if (self.nAttemptsLeft<=0 || sender == self.currentAnswer) {
        return;
    }
    
    if ([self.quizVehicleDelegate respondsToSelector:@selector(answerClickedInQuiz)]) {
        
        [self.quizVehicleDelegate answerClickedInQuiz];
    }
    
    self.currentAnswer = (ClickImageView *)sender;
    
    ClickImageView *clickImage = (ClickImageView *)sender;
    BOOL status;
    
    if  ([clickImage.dataAnswer isKindOfClass:[QuizInstanceAnswer class]]) {
        
        status = [(QuizInstanceAnswer *)[clickImage dataAnswer] isCorrect];
    }
    else {
        status = [[clickImage.dataAnswer objectForKey:@"isCorrect"] boolValue];
    }
    if (!status) {
        
        self.isAnsweredCorrect = NO;
        [self showIncorrectFeedback];
        [self.currentAnswer showFeedbackWithStatus];
        self.nAttemptsLeft--;
        
        if (self.nAttemptsLeft == 0) {
            //finish instance go to next
            //                NSLog(@"isCompleted set to YES 1");
            
            self.isCompleted = YES;
        }
        else {
            //hide after 2 seconds
            [self performSelector:@selector(hideAllFeedbacks) withObject:nil afterDelay:1.0];
            return;
        }
    }
    else {
        
        if (!isOrdered) {
            self.currentAnswer.answerIsClosed = YES;
            self.nCorrectAnswers--;
            
            if (self.nCorrectAnswers == 0) {
                self.isCompleted = YES;
                [self showCorrectFeedback];
            }
            else {
                [AUDIO_MANAGER playRightSound];
            }
        }
        else {
            self.isCompleted = YES;
        }
        
        self.isAnsweredCorrect = YES;
        
        if (isOrdered) {
            [self showCorrectFeedback];
            self.nCorrectAnswers = 0;
        }
        
        [self.currentAnswer showFeedbackWithStatus];
        
    }
    
    for (ClickImageView *answer in self.viewAnswerContent.subviews) {
        
        if (self.isCompleted) {
            break;
        }
        if ([answer isKindOfClass:[ClickImageView class]] && answer != self.currentAnswer) {
            
            if (!isOrdered && answer.answerIsClosed) {
                
            }
            else {
                [answer hideAnswer];
            }
        }
    }
}


- (void)hideAllFeedbacks {
    
    for (ClickImageView *answer in self.viewAnswerContent.subviews) {
        if (self.isCompleted) {
            break;
        }
        if ([answer isKindOfClass:[ClickImageView class]]) {
            
            if (!isOrdered && answer.answerIsClosed) {
                
            }
            else {
                [answer hideAnswer];
            }
        }
    }
    
    self.currentAnswer = nil;
}

- (void)selectCorrectAnswer {
    
    for (ClickImageView *answer in self.viewAnswerContent.subviews) {
        
        if ([answer isKindOfClass:[ClickImageView class]]) {
            
            BOOL isAnsweredCorrect;
            
            if  ([answer.dataAnswer isKindOfClass:[QuizInstanceAnswer class]]) {
                
                isAnsweredCorrect = [(QuizInstanceAnswer *)[answer dataAnswer] isCorrect];
            }
            else {
                isAnsweredCorrect = [[answer.dataAnswer objectForKey:@"isCorrect"] boolValue];
            }
            
            
            if (isAnsweredCorrect) {
                
                [answer showFeedbackWithStatus];
            }
            else {
                
                if (answer != self.currentAnswer) {
                    [answer hideAnswer];
                    
                }
            }
        }
        
    }
    
}


- (void)answerWasClicked:(id)sender changeImage:(NSString *)image {
    
    if (self.nAttemptsLeft<=0 || sender == self.currentAnswer) {
        return;
    }
    
    self.currentAnswer = (ClickImageView *)sender;
    for (ClickImageView *answer in self.viewAnswerContent.subviews) {
        
        if ([answer isKindOfClass:[ClickImageView class]] && answer != self.currentAnswer) {
            
            [answer hideAnswer];
        }
    }
    [imageViewOneImageAnswer setImage:[UIImage imageNamed:image]];
}

#pragma mark - Activate/Deactivate vehicle

- (void)activateVehicle {
    
    [super activateVehicle];
    
    //    if ([self.dataClickToLearnInstance objectForKey:@"questionTextVocabs"]) {
    //
    //        if ([[self.dataClickToLearnInstance objectForKey:@"questionTextVocabs"] boolValue]) {
    //            [self.vehicleDelegate setQuestionBarTextWithVocab:[self.dataClickToLearnInstance objectForKey:@"questionText"]];
    //
    //        }
    //        else {
    //            [self.vehicleDelegate setQuestionBarText:[self.dataClickToLearnInstance objectForKey:@"questionText"]];
    //
    //        }
    //    }
    //    else {
    //        [self.vehicleDelegate setQuestionBarText:[self.dataClickToLearnInstance objectForKey:@"questionText"]];
    //    }
}

- (void)deactivateVehicle {
    
    [super deactivateVehicle];
    
    self.currentAnswer = nil;
    
    for (ClickImageView *answer in self.viewAnswerContent.subviews) {
        
        if ([answer isKindOfClass:[ClickImageView class]]) {
            
            [answer hideAnswer];
            answer.answerIsClosed = NO;
        }
    }
    
    
    self.nAttemptsLeft = 1;
    
    self.isCompleted = NO;
    self.nCorrectAnswers = 1;
    [self makeNewAnswersLayout];
    
}

#pragma mark - Make new random

- (void)makeNewAnswersLayout {
    
    NSInteger numberOfAnswers = [(NSArray *)[[instanceData answersForInstance] allObjects] count];;
    CGFloat answerWidth = (viewAnswerContent.frame.size.width-(numberOfAnswers-1)*15)/numberOfAnswers;
    
    NSMutableArray *arryRandomNumbers=[[NSMutableArray alloc]init];
    while (arryRandomNumbers.count<numberOfAnswers) {
        NSInteger randomNumber=arc4random()%numberOfAnswers;
        if (![arryRandomNumbers containsObject:[NSString stringWithFormat:@"%ld",randomNumber]])       {
            [arryRandomNumbers addObject:[NSString stringWithFormat:@"%ld",randomNumber]];
        }
        continue;
    }
    
    
    //Create answers
    NSInteger index = 0;
    NSInteger order = 0;
    
    for (ClickImageView *clickableImage in viewAnswerContent.subviews) {
        
        if ([clickableImage isKindOfClass:[ClickImageView class]]) {
            
            order = [[arryRandomNumbers objectAtIndex:index] integerValue];
            clickableImage.frame = CGRectMake(order*(answerWidth+15), 0, clickableImage.width, clickableImage.height);
            
            index++;
        }
    }
    
}

- (void)finishVehicle{
    [super finishVehicle];
    
    if (self.isAnsweredCorrect) {
        
        if (self.nCorrectAnswers == 0) {
            
            self.isCompleted = YES;
            //            NSLog(@"isCompleted set to YES 4");
            [self.vehicleDelegate vehicleWasFinished:self];
            
        }
        
    }
    else if (self.nAttemptsLeft == 0) {
        
        [self selectCorrectAnswer];
        
        self.isCompleted = YES;
        
        [self performSelector:@selector(callFinishAfterDelay) withObject:nil afterDelay:0.5];
        //[self.vehicleDelegate setQuestionBarText:@""];
    }
    
}

- (void)callFinishAfterDelay {
    
    [self.vehicleDelegate vehicleWasFinished:self];
}

@end
