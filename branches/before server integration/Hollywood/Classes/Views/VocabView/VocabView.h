//
//  VocabView.h
//  VocabTest
//
//  Created by Dimitar Shopovski on 7/19/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VocabView : UIView {
    
    BOOL isTranslationShown;
    BOOL isFirstRow;
    UILabel *currentSelectedVocabLabel;
}

@property (strong, nonatomic) UIView *translationView;
@property (strong, nonatomic) UILabel *translationWordLabel;

@property (strong, nonatomic) UIImageView *imgBallon;
@property (strong, nonatomic) NSNumber *fontSize;

//@property (strong, nonatomic) UIButton *buttonFavouritres;

@property (nonatomic) NSTimer *vocabTimer;

- (instancetype)initWithFrame:(CGRect)frame withData:(id)data;
- (void)cleanControl;
- (void)buildTextViewFromString:(NSString *)localizedString;
- (void)buildTextViewFromString:(NSString *)localizedString withFont:(NSNumber *)fontSize;

- (void)createUX;

@end
