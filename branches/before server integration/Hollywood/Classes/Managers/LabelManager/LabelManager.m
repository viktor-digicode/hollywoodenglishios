//
//  LMLabelManager.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 6/21/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LabelManager.h"

@implementation LabelManager


#ifdef LABELMANAGER_SINGLETON



SINGLETON_GCD(LabelManager)
#endif



- (NSString *)setLocalizeLabelText:(NSString *)text
{
    return NSLocalizedString(text, nil);
}


@end


