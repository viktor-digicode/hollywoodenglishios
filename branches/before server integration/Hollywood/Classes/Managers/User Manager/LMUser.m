//  LMUser.m
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//


#import "LMUser.h"


@implementation LMUser

- (id)initWithContentsOfFile:(NSString *)filePath {
	self = [self init];
	if (self) {
		// process
	}
	return self;
}

- (id)initWithMSISDN:(NSString *)msisdn {
	self = [self init];
	if (self) {
		self.msisdn = msisdn;
		NSInteger prefixLength = 2; //[[USER_MANAGER defaultPrefix] length];
		self.number = [msisdn substringFromIndex:prefixLength];
		self.areaCode = [self.number substringToIndex:2];
		self.name = @"";
    self.currentLevel = 0;
		//self.countryCode = [COUNTRY_MANAGER countryCodeFromMSISDN:msisdn];
		self.status = ([self.number isEqualToString:kUserManagerDefault]) ? KAUserGuest : KAUserStatusNotRegistered;
	}
	return self;
}

- (id)initNativeUser:(NSString *)msisdn {
  self = [self init];
  if (self) {
    self.msisdn = msisdn;
    self.number = @"";
    self.areaCode = @"";
    self.name = @"";
    self.countryCode = @"";
    self.currentLevel = 0;
    self.status = KAUserStatusNotRegistered;
    self.isNative = 1;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"makeNativeUser"];
    [[NSUserDefaults standardUserDefaults] synchronize];
  }
  return self;
}

- (void)synchronizeToFile {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
	if ([self.number isEqualToString:@""] || [self.number isEqualToString:kUserManagerDefault]) {
		// guest
		// NSString *path = [[NSString documentsDir] stringByAppendingPathComponent:[NSString stringWithFormat:@"guest/%@.plist", self.msisdn]];
		/*[[NSUserDefaults standardUserDefaults] removeObjectForKey:kLastValidMSISDNPath];
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:kLastValidMSISDN];
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:kLastValidUniqueID];
     [[NSUserDefaults standardUserDefaults] setObject:@NO forKey:kLastValidLoginUser];
     [[NSUserDefaults standardUserDefaults] synchronize];*/
	}
	else {
		// user
		NSString *path = [[NSString documentsDir] stringByAppendingPathComponent:[NSString stringWithFormat:@"user/%@.plist", self.msisdn]];
		[userDefaults setObject:path forKey:kLastValidMSISDNPath];
		[userDefaults setObject:self.msisdn forKey:kLastValidMSISDN];
		[userDefaults setInteger:self.status forKey:kLastValidUserStatus];
		[userDefaults setObject:self.billingDate forKey:kLastValidBillingDate];
		[userDefaults setObject:@YES forKey:kLastValidLoginUser];
		[userDefaults synchronize];
	}
}
- (void)synchronizeNativeUserToFile {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *path = [[NSString documentsDir] stringByAppendingPathComponent:[NSString stringWithFormat:@"user/%@.plist", self.msisdn]];
    
    [userDefaults setObject:path forKey:kLastValidMSISDNPath];
    [userDefaults setObject:self.msisdn forKey:kLastValidMSISDN];
    [userDefaults setInteger:self.status forKey:kLastValidUserStatus];
    [userDefaults setObject:self.billingDate forKey:kLastValidBillingDate];
    //[userDefaults setObject:@YES forKey:kLastValidLoginUser];
    [userDefaults setObject:@NO forKey:kLastValidLoginUser];
    [userDefaults synchronize];
  
}

- (void)saveStatusData {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if(self.levelData){
        [userDefaults setObject:self.levelData forKey:kLastValidLevelData];
    }
    if(self.currentLevel){
        [userDefaults setObject:self.currentLevel forKey:kLastValidCurrentLevel];
    }
    if(self.contentVersion>0){
        [userDefaults setObject:self.contentVersion forKey:kLastValidContentVersion];
    }
    [userDefaults synchronize];
}

- (void)loadStatusData {
    self.levelData = [[NSUserDefaults standardUserDefaults] objectForKey:kLastValidLevelData];
    self.currentLevel = [[NSUserDefaults standardUserDefaults] objectForKey:kLastValidCurrentLevel];
    self.contentVersion = [[NSUserDefaults standardUserDefaults] objectForKey:kLastValidContentVersion];
}

@end
