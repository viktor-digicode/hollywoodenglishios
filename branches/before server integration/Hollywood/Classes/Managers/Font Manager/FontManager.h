//
//  FontManager.h
//  Hollywood
//
//  Created by Kiril Kiroski on 4/14/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FontManager : NSObject

+ (NSDictionary*)takeFontSize:(NSString*)currentString forVehicle:(KAVehiclesType)vehicleType;
+ (NSDictionary*)takeFontSizeForQuestionArea:(NSString *)currentString;

-(void)showTempFont;

@end
