//
//  LMAnalyticsManager.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 10/4/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "LMAnalyticsManager.h"

@implementation LMAnalyticsManager

#ifdef ANALYTICS_SINGLETON
SINGLETON_GCD(LMAnalyticsManager)
#endif


#pragma mark - init Analytics

- (id)initWithProductName:(NSString *)productName {
    
    self = [super init];
    if (self) {

        //        [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelError];
        self.productName = productName;
        NSLog(@"#####  productName:%@", self.productName);
        
    }
    return self;
}

#pragma mark - Events

- (void)sendCategorySelectedEvent:(NSInteger)categoryId {
    
    //Args to send
    
    NSDate *currentDate = [NSDate date];
    
    //Device id
    
    NSLog(@"Send event name: %@, TimeStamp: %@", kProductName, currentDate);
    
}

- (void)sendQuizSelectedEvent:(NSInteger)quizId withActionName:(NSString *)actionName {
    
    //Args to send
    
    NSDate *currentDate = [NSDate date];
    
    //Device id
    
    NSLog(@"Quiz selected: %@\nAction name: %@\nTimeStamp: %@", kProductName, actionName, currentDate);
    
}

- (void)sendQuizVehiclePresented:(NSInteger)vehicleId withActionName:(NSString *)actionName {
    
    //Args to send
    
    NSDate *currentDate = [NSDate date];
    
    //Device id
    
    NSLog(@"Quiz selected: %@\nAction name: %@\nTimeStamp: %@\nVehicle ID: %ld", kProductName, actionName, currentDate, (long)vehicleId);
}

- (void)sendSwipeBetweenInstance:(NSString *)actionName {
    
    //Args to send
    
    NSDate *currentDate = [NSDate date];
    
    //Device id
    
    NSLog(@"Swipe gesture: %@\nAction name: %@\nTimeStamp: %@\n", kProductName, actionName, currentDate);

}

- (void)enterTheVehicleEvent:(NSString *)vehicleName {
    
    NSDate *currentDate = [NSDate date];
    
    //Device id
    
    NSLog(@"Swipe gesture: %@\nAction name: %@\nTimeStamp: %@\n", kProductName, vehicleName, currentDate);

}

- (void)leaveTheVehicleEvent:(NSString *)vehicleName otherData:(NSDictionary *)statsDictionary {
    
    NSDate *currentDate = [NSDate date];
    
    //Device id
    
    NSLog(@"Swipe gesture: %@\nAction name: %@\nTimeStamp: %@\n", kProductName, vehicleName, currentDate);

}

@end
