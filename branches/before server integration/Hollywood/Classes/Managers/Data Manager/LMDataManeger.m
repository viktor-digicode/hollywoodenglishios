//
//  LMDataManeger.m
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "LMDataManeger.h"
#import "CourseObject+CoreDataClass.h"
#import "LevelObject+CoreDataClass.h"
#import "RankObject+CoreDataClass.h"
#import "LessonObject+CoreDataClass.h"
#import "BaseVehiclesObject+CoreDataClass.h"



@implementation LMDataManeger{
    NSNumber *currentLesson;
}

SINGLETON_GCD(LMDataManeger)


- (id)init
{
    self = [super init];
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    //[self initializeStartData];
    if (self) {
       /* if ([[self getStartData] count]) {
            
        } else {
            [self initializeStartData];
            [self initProgressData];
        }*/
    }
    return self;
}

- (NSArray*)getStartData{
    NSPredicate *dataPredicate1 = [NSPredicate predicateWithFormat:LEVEL_SEARCH_PREDICATE, @"Beginner"];
    NSArray *sortArray1 = [[LevelObject MR_findAllSortedBy:@"level_name"
                                                 ascending:YES
                                             withPredicate:dataPredicate1
                                                 inContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    if([sortArray1 count]){
        LevelObject *levelObject1 = [sortArray1 objectAtIndex:0];
        NSLog(@"level_name: %@ ",levelObject1.level_name);
        NSSet *set1 = [levelObject1 takeRangs] ;
        //NSArray *myArray = [set1 allObjects];
        //RankObject *object = [myArray objectAtIndex:0];
    }
    
    
    
    NSPredicate *dataPredicate = [NSPredicate predicateWithFormat:SEARCH_PREDICATE, @"Hollywood"];
    NSArray *sortArray = [[CourseObject MR_findAllSortedBy:@"course_name"
                                            ascending:YES
                                        withPredicate:dataPredicate
                                            inContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    if([sortArray count]){
        CourseObject *CourseObject1 = [sortArray objectAtIndex:0];
        @try {
            
        NSSet *set1 = CourseObject1.course_levels ;
        NSArray *myArray = [set1 allObjects];
        LevelObject *rObject = [myArray objectAtIndex:0];
        
        NSSet *set2 = rObject.level_ranks ;
        NSArray *myArray2 = [set2 allObjects];
        RankObject *object2 = [myArray2 objectAtIndex:0];
        
        NSSet *set3 = object2.rank_lessons;
        NSArray *myArray3 = [set3 allObjects];
        LessonObject *object3 = [myArray3 objectAtIndex:0];
        
            
        NSSet *set4 = object3.lesson_vehicles;
        NSArray *myArray4 = [set4 allObjects];
        BaseVehiclesObject *object4 = [myArray4 objectAtIndex:0];
         
        }
        @finally {
            
        }

        return sortArray;
    }
    return @[];
    
}

- (void)initializeStartData:(NSDictionary*)startDictionary{
    [self makeCourseData:startDictionary];
    [self saveData];
    [self getStartData];
    
    [self makeVipCategoryCourseData:[self takeInitialVipCategoryJson]];
    [self saveData];
}

- (void)initializeStartData{
    [self makeCourseData:[self takeInitialJson]];
    [self saveData];
    [self getStartData];
    
    [self makeVipCategoryCourseData:[self takeInitialVipCategoryJson]];
    [self saveData];
}

-(void) saveData{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
            NSLog(@"You successfully saved your context.");
        } else if (error) {
            NSLog(@"Error saving context: %@", error.description);
        }
    }];
}

- (void)makeCourseData:(NSDictionary*)dataDictionary{
    if(!dataDictionary)
        return;
    NSDictionary *responseDictionary = [self objectOrNilForKey:kApiObjectResponse fromDictionary:dataDictionary];
    
    CourseObject *importedObject =[CourseObject MR_createEntity];
    [importedObject setData:responseDictionary];
}



- (NSMutableDictionary *)takeInitialJson
{
    
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"InitialData" ofType:@"bundle"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:bundlePath]) {
        NSBundle *bundle = [[NSBundle alloc] initWithPath:bundlePath];
        if (bundle) {
            [bundle load];
            NSString *jsonPath = [bundle pathForResource:@"course_prototype" ofType:@"json"];
            if (![[NSFileManager defaultManager] fileExistsAtPath:jsonPath])
                return nil;
            
            NSData *data = [NSData dataWithContentsOfFile:jsonPath];
            NSError *error = nil;
            NSDictionary *jsonRoot = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            return [jsonRoot copy];
        }
    }
    return nil;
}

- (NSArray*)takeDataForLevel:(NSNumber*)levelOrder{
    NSArray *dataArray = @[];
    return dataArray;
}

- (NSArray*)takeVehicleForCurrentLesson{
    NSArray *dataArray = @[@1];
    return dataArray;
}
#pragma mark VipCategory Data
- (NSMutableDictionary *)takeInitialVipCategoryJson
{
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"InitialData" ofType:@"bundle"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:bundlePath]) {
        NSBundle *bundle = [[NSBundle alloc] initWithPath:bundlePath];
        if (bundle) {
            [bundle load];
            NSString *jsonPath = [bundle pathForResource:@"vip_categori" ofType:@"json"];
            if (![[NSFileManager defaultManager] fileExistsAtPath:jsonPath])
                return nil;
            
            NSData *data = [NSData dataWithContentsOfFile:jsonPath];
            NSError *error = nil;
            NSDictionary *jsonRoot = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            return [jsonRoot copy];
        }
    }
    return nil;
}

- (void)makeVipCategoryCourseData:(NSDictionary*)dataDictionary{
    if(!dataDictionary)
        return;
    
    VipCourseObject *importedObject =[VipCourseObject MR_createEntity];
    [importedObject setData:dataDictionary];
}

- (VipCourseObject*)getVipCourseObject{
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    NSArray *dataArray = [[VipCourseObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    VipCourseObject *tempVipCourseObject = [dataArray objectAtIndex:0];
    return tempVipCourseObject;
}

- (NSArray*)getAllVipCategory{
    VipCourseObject *tempVipCourseObject = [self getVipCourseObject];
    
    NSSet *set2 = tempVipCourseObject.vipCategories ;
    NSArray *data2Array = [set2 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"displayOrder"
                                                  ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [data2Array sortedArrayUsingDescriptors:sortDescriptors2];
    
    return sorted2Array;
}
#pragma mark Progress Data

- (void)initProgressData
{
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    if ([[self getAllProgressObject] count]) {
        
    } else {
        [self initializeProgressData];
    }
}

- (NSArray*)getAllProgressObject{
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    NSArray *dataArray = [[ProgressUserObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    return dataArray;
}

- (void)initializeProgressData{
    
    
    ProgressUserObject *progresssObject =[ProgressUserObject MR_createEntity];
    [progresssObject setInitialData];
       
    [self saveData];
}

#pragma mark get curent progress

- (ProgressUserObject*)getCurrentUserProgressObject{
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    NSArray *dataArray = [[ProgressUserObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    ProgressUserObject *tempProgressUser = [dataArray objectAtIndex:0];
    return tempProgressUser;
}

- (ProgressLessonObject*)getLessonObjectForIndex:(int)lessonOrder {
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    NSArray *dataArray = [[ProgressUserObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    ProgressUserObject *tempProgressUser = [dataArray objectAtIndex:0];
    
    NSSet *set2 = tempProgressUser.levelsProgres ;
    NSArray *data2Array = [set2 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"levelOrder"
                                                  ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [data2Array sortedArrayUsingDescriptors:sortDescriptors2];
    /////
    
    ProgressLevelObject *tempProgressLevel = [sorted2Array objectAtIndex:tempProgressUser.currentLevel];
    NSSet *set3 = tempProgressLevel.lessonsProgres ;
    NSArray *data3Array = [set3 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor3;
    sortDescriptor3 = [[NSSortDescriptor alloc] initWithKey:@"lessonOrder"
                                                  ascending:YES];
    NSArray *sortDescriptors3 = [NSArray arrayWithObject:sortDescriptor3];
    NSArray *sorted3Array = [data3Array sortedArrayUsingDescriptors:sortDescriptors3];
    ///
    
    ProgressLessonObject *tempProgressLesson = [sorted3Array objectAtIndex:lessonOrder];
    //ProgressLessonObject *tempProgressLesson = [sorted3Array objectAtIndex:tempProgressLevel.currentUnit];
    
    return tempProgressLesson;
}


- (ProgressLessonObject*)getCurrentLessonObject{
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    NSArray *dataArray = [[ProgressUserObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    ProgressUserObject *tempProgressUser = [dataArray objectAtIndex:0];
    
    NSSet *set2 = tempProgressUser.levelsProgres ;
    NSArray *data2Array = [set2 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"levelOrder"
                                                 ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [data2Array sortedArrayUsingDescriptors:sortDescriptors2];
    /////
    
    ProgressLevelObject *tempProgressLevel = [sorted2Array objectAtIndex:tempProgressUser.currentLevel];
    NSSet *set3 = tempProgressLevel.lessonsProgres ;
    NSArray *data3Array = [set3 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor3;
    sortDescriptor3 = [[NSSortDescriptor alloc] initWithKey:@"lessonOrder"
                                                  ascending:YES];
    NSArray *sortDescriptors3 = [NSArray arrayWithObject:sortDescriptor3];
    NSArray *sorted3Array = [data3Array sortedArrayUsingDescriptors:sortDescriptors3];
    ///
    
    ProgressLessonObject *tempProgressLesson = [sorted3Array objectAtIndex:tempProgressLevel.currentUnit];
    
    return tempProgressLesson;
}

- (ProgressLevelObject*)getCurrentProgressLevelObject{
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Hollywood"];
    NSArray *dataArray = [[ProgressUserObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    ProgressUserObject *tempProgressUser = [dataArray objectAtIndex:0];
    
    NSSet *set2 = tempProgressUser.levelsProgres ;
    NSArray *data2Array = [set2 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"levelOrder"
                                                  ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [data2Array sortedArrayUsingDescriptors:sortDescriptors2];
    /////
    
    ProgressLevelObject *tempProgressLevel = [sorted2Array objectAtIndex:tempProgressUser.currentLevel];
    return tempProgressLevel;
}

#pragma mark - Get Categories
- (NSArray *)getVIPCategories {
    
    //get json
    NSArray *jsonCategories = [self getJSONObjectFromFile:@"categories"];
    
    return jsonCategories;
}

#pragma mark - Helper method take JSON data

- (id)getJSONObjectFromFile:(NSString *)fileName {
    
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:jsonPath]) {
        
        NSData *data = [NSData dataWithContentsOfFile:jsonPath];
        NSError *error = nil;
        id jsonRoot = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        return [jsonRoot copy];
        
    }
    return nil;
    
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end



