//
//  LMRequestManager.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/21/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFURLSessionManager.h>
#import <AFNetworking/AFHTTPSessionManager.h>

typedef enum {
    kRequestLoginUserAPICall,
    kRequestFirstLaunchAPICall,
} ELMRequestType;

@interface LMRequestManager : NSObject

@property (nonatomic, strong) NSString *groupID;
@property (nonatomic, strong) NSString *contentID;

- (NSMutableURLRequest *)post:(ELMRequestType)type headers:(NSDictionary *)headers;
- (void)getDataFor:(ELMRequestType)type
           headers:(NSDictionary *)headers
withSuccessCallBack:(void (^)(NSDictionary *responseObject)) successCallback
andWithErrorCallBack:(void (^)(NSString *errorMessage)) errorCallback;


@end

@interface LMRequestManager ()
+ (id)sharedInstance;
@end

#define REQUEST_MANAGER ((LMRequestManager *)[LMRequestManager sharedInstance])
