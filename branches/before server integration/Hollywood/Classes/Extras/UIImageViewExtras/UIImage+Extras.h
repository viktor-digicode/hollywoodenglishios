//
//  UIImage+Extras.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 5/20/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extras)

+ (UIImage *)imageWithImage:(UIImage *)sourceImage scaledToWidth:(float)i_width;
+ (UIImage *)imageWithImage:(UIImage *)sourceImage scaledToHeight:(float)i_height;

@end
