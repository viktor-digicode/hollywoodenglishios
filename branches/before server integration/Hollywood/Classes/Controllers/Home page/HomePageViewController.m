//
//  HomePageViewController.m
//  Hollywood
//
//  Created by Kiril Kiroski on 8/8/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "HomePageViewController.h"
#import "LMRequestManager.h"

@interface HomePageViewController ()<CacheManagerDelegate> {
    
    __weak IBOutlet UILabel *userScoreLabel;
    __weak IBOutlet UILabel *userVipPassesLabel;

    __weak IBOutlet UIImageView *imageViewUserProfile;
    
    __weak IBOutlet UIImageView *imageViewUserRank;
    
    __weak IBOutlet UIImageView *mainTeaserImage;

    __weak IBOutlet UICollectionView *mainCollectionView;
    __weak IBOutlet UIView *rssView;
    
    __strong ProgressUserObject *currentProgressUserObject;
    __strong ProgressLevelObject *currentProgressLevelObject;
    
    MWFeedParser *feedParser;
    NSMutableArray *parsedItems;
    
    __weak IBOutlet UIView *loadingView;
    __weak IBOutlet UIActivityIndicatorView *activityIndicatorLoading;
    __weak IBOutlet UILabel *loadingLabel;
    
    __weak IBOutlet UILabel *vipScoreLabel;
    __weak IBOutlet UILabel *vipStaticTextLabel;

}

@property (nonatomic, strong) NSMutableArray *lessonsInfoArray;

@end

@implementation HomePageViewController
@synthesize currentLesson;
@synthesize maximumLessonNumber;

- (void)configureUI{
    
    [super configureUI];
    DLog(@"no_internet_access %@", Localized(@"no_internet_access"));
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    
    
    currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
    maximumLessonNumber = currentProgressLevelObject.maxUnit;
    
    imageViewUserRank.image = [UIImage imageNamed:[NSString stringWithFormat:@"Awards_%02ld", (maximumLessonNumber%6)+1]];

    
    userScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
    userVipPassesLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];
    
    self.navigationController.navigationBarHidden = YES;
    [mainCollectionView registerNib:[UINib nibWithNibName:@"HomePageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    //[self initialScroll];
    
    imageViewUserProfile.layer.cornerRadius = 4.0;
    imageViewUserProfile.layer.borderWidth = 1.0;
    imageViewUserProfile.layer.borderColor = [UIColor blackColor].CGColor;
    imageViewUserProfile.layer.masksToBounds = YES;

    imageViewUserProfile.image = [USER_MANAGER takeCurrentUserImage];
    
    [mainTeaserImage setHidden:YES];
    
    userScoreLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:30.0];
    userVipPassesLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:15.0];
    vipStaticTextLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:18.0];

}

- (void)loadData {
    
    [super loadData];
    
    //Points, vip passes
    [REQUEST_MANAGER getDataFor:kRequestFirstLaunchAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
        //
    } andWithErrorCallBack:^(NSString *errorMessage) {
        //
    }];
    
    return;
    
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
    
    userScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
    userVipPassesLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];
    
    //TO DO: Get lesson info

    currentLesson = currentProgressLevelObject.currentUnit;
    maximumLessonNumber = currentProgressLevelObject.maxUnit;
    
//    imageViewUserRank.image = [UIImage imageNamed:[NSString stringWithFormat:@"Awards_%02ld", (maximumLessonNumber%6)+1]];

    
    self.lessonsInfoArray = [NSMutableArray new];
                             
    for (NSInteger i=0; i<=maximumLessonNumber; i++) {
        
        if ([[NSString stringWithFormat:@"iOS-Lessons-Strip_%02ld", (i+1)%10] isEqualToString:@"iOS-Lessons-Strip_00"]) {
            
            [self.lessonsInfoArray addObject:@{
                                               @"lessonStatus":@(1),
                                               @"lessonNumber":@(i+1),
                                               @"lessonImage":@"iOS-Lessons-Strip_10",
                                               @"leasonTeaserImage": @"teaser_lesson_10"}];
        }
        else {
            
            [self.lessonsInfoArray addObject:@{
                                          @"lessonStatus":@(1),
                                          @"lessonNumber":@(i+1),
                                          @"lessonImage":[NSString stringWithFormat:@"iOS-Lessons-Strip_%02ld", (i+1)%10],
                                          @"leasonTeaserImage": [NSString stringWithFormat:@"teaser_lesson_%02ld", (i+1)%10]}];
            
        }
        
    }
    
    for (NSInteger i=maximumLessonNumber+1; i<25; i++) {
        
        
        if ([[NSString stringWithFormat:@"iOS-Lessons-Strip_%02ld", (i+1)%10] isEqualToString:@"iOS-Lessons-Strip_00"]) {
            
            [self.lessonsInfoArray addObject:@{
                                               @"lessonStatus":@(3),
                                               @"lessonNumber":@(i+1),
                                               @"lessonImage":@"iOS-Lessons-Strip_10",
                                               @"leasonTeaserImage": @"teaser_lesson_10"}];
        }
        else {
            
            [self.lessonsInfoArray addObject:@{
                                               @"lessonStatus":@(3),
                                               @"lessonNumber":@(i+1),
                                               @"lessonImage":[NSString stringWithFormat:@"iOS-Lessons-Strip_%02ld", (i+1)%10],
                                               @"leasonTeaserImage": [NSString stringWithFormat:@"teaser_lesson_%02ld", (i+1)%10]}];
            
        }

    }
    
    id currentLessonData;
    
    if ([[NSString stringWithFormat:@"iOS-Lessons-Strip_%02ld", (maximumLessonNumber+1)%10] isEqualToString:@"iOS-Lessons-Strip_00"]) {
    
        currentLessonData = @{
                              @"lessonStatus":@(2),
                              @"lessonNumber":@(maximumLessonNumber+1),
                              @"lessonImage":@"iOS-Lessons-Strip_10",
                              @"leasonTeaserImage": @"teaser_lesson_10"};
        
    }
    else {
        
        currentLessonData = @{
                              @"lessonStatus":@(2),
                              @"lessonNumber":@(maximumLessonNumber+1),
                              @"lessonImage":[NSString stringWithFormat:@"iOS-Lessons-Strip_%02ld", (maximumLessonNumber+1)%10],
                              @"leasonTeaserImage": [NSString stringWithFormat:@"teaser_lesson_%02ld", (maximumLessonNumber+1)%10]};
        
    }
    
    [self.lessonsInfoArray replaceObjectAtIndex:maximumLessonNumber withObject:currentLessonData];
    
    NSString *teaserImageUrl = [[self.lessonsInfoArray objectAtIndex:maximumLessonNumber] objectForKey:@"leasonTeaserImage"];
    mainTeaserImage.image = [UIImage imageNamed:teaserImageUrl];
    [mainTeaserImage setHidden:NO];
    
    [mainCollectionView reloadData];
    
    [mainCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:maximumLessonNumber inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
    
    [self createFeedParser];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self.view.layer removeAllAnimations];
    feedParser.delegate = nil;
    [feedParser stopParsing];
    feedParser = nil;
    [super viewDidDisappear:animated];
}
#pragma mark Orientations
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskPortrait;
    
}

#pragma mark UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.lessonsInfoArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    HomePageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    //[cell setDataArray:dataArray[indexPath.row]];
    
    id lessonData = [self.lessonsInfoArray objectAtIndex:indexPath.row];
    [cell setLessonData:lessonData];

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(204, 100);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    id lessonData = [self.lessonsInfoArray objectAtIndex:indexPath.row];
    NSInteger status = [[lessonData objectForKey:@"lessonStatus"] integerValue];
    
    if (status == 3) {
        return;
    }
    
    currentProgressLevelObject.currentUnit = (int)[[lessonData objectForKey:@"lessonNumber"] integerValue]-1;
    
    [APP_DELEGATE buildLessonStack];
}

#pragma mark - Open lesson

- (IBAction)openLessonWithData:(id)sender {
    
    currentProgressLevelObject.currentUnit = (int)maximumLessonNumber;
    [APP_DELEGATE buildLessonStack];
    
}

- (IBAction)openUserZone:(id)sender {
    
    [APP_DELEGATE buildUserZoneStack];
}

- (IBAction)openVipZone:(id)sender {
    
    [self.view bringSubviewToFront:loadingView];
    loadingView.hidden = NO;
    [activityIndicatorLoading startAnimating];
    
    //donwload resoureces
    NSArray *dataArray = [DATA_MANAGER getAllVipCategory];
    
    [CACHE_MANAGER setDelegate:self];
    [CACHE_MANAGER startDownloadingAndCachingResources:dataArray];


}

#pragma mark - Cache Manager Delegate
- (void)cacheManagerFinishedDownloading:(id)sender {
    
    NSLog(@"========= Images were downloaded =========");
    
    [APP_DELEGATE buildVipZoneStack];
    
    [activityIndicatorLoading stopAnimating];
    loadingView.hidden = YES;
}

#pragma mark RSS
-(void)createFeedParser{
    parsedItems = [[NSMutableArray alloc] init];
    // Create feed parser and pass the URL of the feed
    NSURL *feedURL = [NSURL URLWithString:@"http://feeds.feedburner.com/thr/news"];
    feedParser = [[MWFeedParser alloc] initWithFeedURL:feedURL];
    feedParser.delegate = self;
    feedParser.feedParseType = ParseTypeFull;//ParseTypeInfoOnly;//ParseTypeFull;
    feedParser.connectionType = ConnectionTypeAsynchronously;
    [feedParser parse];
}

#pragma mark MWFeedParserDelegate

- (void)feedParserDidStart:(MWFeedParser *)parser {
    //NSLog(@"Started Parsing: %@", parser.url);
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedInfo:(MWFeedInfo *)info {
    //NSLog(@"Parsed Feed Info: “%@”", info.title);
    self.title = info.title;
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedItem:(MWFeedItem *)item {
    //NSLog(@"Parsed Feed Item: “%@”", item.title);
    if (item) [parsedItems addObject:item];
}

- (void)feedParserDidFinish:(MWFeedParser *)parser {
    //NSLog(@"Finished Parsing%@", (parser.stopped ? @" (Stopped)" : @""));
    /*if([parsedItems count]){
        MWFeedItem *tempItem = [parsedItems objectAtIndex:0];
        tempItem.title = @"rwer rwerewr";
    }*/
    [self makeRssTextAnimation];
}

- (void)feedParser:(MWFeedParser *)parser didFailWithError:(NSError *)error {
    //NSLog(@"Finished Parsing With Error: %@", error);
    if (parsedItems.count == 0) {
        self.title = @"Failed"; // Show failed message in title
    } else {
        // Failed but some items parsed, so show and inform of error
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Parsing Incomplete"
                                                        message:@"There was an error during the parsing of this feed. Not all of the feed items could parsed."
                                                       delegate:nil
                                              cancelButtonTitle:@"Dismiss"
                                              otherButtonTitles:nil];
        [alert show];
    }
    //[self updateTableWithParsedItems];
}

- (void) makeRssTextAnimation{
    if([parsedItems count]){
        MWFeedItem *tempItem = [parsedItems objectAtIndex:0];
        [parsedItems removeObject:tempItem];
        UILabel *newLabel = [[UILabel alloc]initWithFrame:rssView.frame];
        newLabel.y = 0;
        newLabel.x = [rssView width];
        newLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
        newLabel.text = tempItem.title;
        [newLabel sizeToFit];
        [rssView addSubview:newLabel];
        [self showAnim:newLabel];
    }else{
        [self createFeedParser];
    }

}

- (void) showAnim:(UILabel*)newLabel {
    
    int lastPoint = -[newLabel width]*2-[rssView width];
    int animationTime = -lastPoint*0.02;
    
    [UIView animateWithDuration:animationTime/2
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         newLabel.x = newLabel.x - [newLabel width]-([rssView width]/2);
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:animationTime/2
                                               delay:0
                                             options:UIViewAnimationOptionCurveLinear
                                          animations:^{
                                              
                                              newLabel.x = newLabel.x - [newLabel width]-([rssView width]/2);
                                              
                                          } completion:^(BOOL finished) {
                                              [newLabel removeFromSuperview];
                                          }];
                         if(feedParser)
                         [self makeRssTextAnimation];
                     }];
    
}

@end
