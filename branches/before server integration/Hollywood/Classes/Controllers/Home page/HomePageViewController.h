//
//  HomePageViewController.h
//  Hollywood
//
//  Created by Kiril Kiroski on 8/8/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "LMBaseViewController.h"
#import "HomePageCollectionViewCell.h"
#import <MWFeedParser/MWFeedParser.h>

@interface HomePageViewController : LMBaseViewController<UICollectionViewDataSource,UICollectionViewDelegate,MWFeedParserDelegate>

@property (nonatomic, assign) NSInteger currentLesson;
@property (nonatomic, assign) NSInteger maximumLessonNumber;


@end
