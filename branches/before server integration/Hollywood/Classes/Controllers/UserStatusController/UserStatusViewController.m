//
//  UserStatusViewController.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 10/20/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "UserStatusViewController.h"
#import "LMDataManeger.h"

@interface UserStatusViewController () {
    
    __weak IBOutlet UIButton *playAsUserButton;
    __weak IBOutlet UIButton *playAsGuestButton;
    
}

@end

@implementation UserStatusViewController

-(void)configureUI{
    
    [super configureUI];
 
}

- (void)loadData {
    
    [super loadData];
    
    //Points, vip passes
    [REQUEST_MANAGER getDataFor:kRequestFirstLaunchAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
        [DATA_MANAGER initializeStartData:responseObject];
    } andWithErrorCallBack:^(NSString *errorMessage) {
        //
    }];
    
    return;
}
#pragma mark - Button actions

- (IBAction)playAsUserAction:(id)sender {
    
    [USER_MANAGER loginDummyUser];
    [APP_DELEGATE buildHomePageStack];

}

- (IBAction)playAsGuestAction:(id)sender {
    
    [USER_MANAGER setGuestStatus];
    [USER_MANAGER initializeWithGuest];

    [APP_DELEGATE buildHomePageStack];
}

#pragma mark Orientations
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskPortrait;
    
}

@end
