//
//  ConversionViewController.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 10/21/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "LMBaseViewController.h"

@interface ConversionViewController : LMBaseViewController
@property (weak, nonatomic) IBOutlet UILabel *conversionScreenLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@end
