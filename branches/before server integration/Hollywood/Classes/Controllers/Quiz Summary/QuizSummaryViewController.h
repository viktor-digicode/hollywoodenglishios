//
//  QuizSummaryViewController.h
//  Hollywood
//
//  Created by Kiril Kiroski on 11/9/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "LMBaseViewController.h"
#import "KNCirclePercentView.h"
#import <TwitterKit/TwitterKit.h>

@interface QuizSummaryViewController : LMBaseViewController<TWTRComposerViewControllerDelegate,UIDocumentInteractionControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *shareLabel;
@property (nonatomic, assign) NSInteger corectAnswersNumber;
@property (nonatomic, assign) NSInteger totalAnswers;
@property (nonatomic, strong) UIDocumentInteractionController *documentController;
@property (nonatomic, assign) VipCategoryObject *currentVipCategoryObject;

@end
