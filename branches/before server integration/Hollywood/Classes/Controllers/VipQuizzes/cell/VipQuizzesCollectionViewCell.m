//
//  VipQuizzesCollectionViewCell.m
//  Hollywood
//
//  Created by Kiril Kiroski on 10/7/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "VipQuizzesCollectionViewCell.h"

@implementation VipQuizzesCollectionViewCell{
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setVipQuizData:(VipQuizObject*)vipQuizData{
    
//    self.cellImageView.image = [UIImage imageNamed:vipQuizData.imageName];
    self.cellImageView.image = [CACHE_MANAGER getImageFromCache:vipQuizData.imageUrl];
    self.vipPointsLabel.text = [NSString stringWithFormat:@"%d ", [vipQuizData.price intValue]];
    self.titleLabel.text = vipQuizData.quizName;
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    
    if(!vipQuizData.availability){
        self.infoLabelWidthConstraint.constant = 80;
        self.yellowImageViewWidthConstraint.constant = 85;
        self.infoLabel.text = @"Proximamente";
        self.infoLabel.adjustsFontSizeToFitWidth = YES;
        [self.elipseImageView setHidden:YES];
        [self.lockImageView setHidden:YES];
        [self.vipPointsLabel setHidden:YES];
    }else{
        
        self.infoLabelWidthConstraint.constant = 41;
        
        self.yellowImageViewWidthConstraint.constant = 51;
        if(DEVICE_BIGER_SIDE > 568.0){
            self.yellowImageViewWidthConstraint.constant = 51*1.175;
        }
        [self.lockImageView setHidden:YES];
        [self.vipPointsLabel setHidden:NO];
        [self.elipseImageView setHidden:NO];
        self.infoLabel.text = @"VIP";
        if( [vipQuizData.price intValue] == 0){
            self.infoLabel.text = @"Free";
        }else if( [vipQuizData.price intValue] < 0){
            [self.lockImageView setHidden:NO];
            [self.vipPointsLabel setHidden:YES];
        }else{
                        
        }
        
    }
    //[self.comingSoonLabel setHidden:([vipQuizData.availability boolValue])];
    //[self.comingSoonImageView setHidden:([vipQuizData.availability boolValue])];
}
@end
