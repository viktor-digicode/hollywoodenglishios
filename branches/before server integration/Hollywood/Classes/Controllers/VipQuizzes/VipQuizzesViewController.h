//
//  VipQuizzesViewController.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 9/29/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VipQuizzesCollectionViewCell.h"
#import "VipCategoryObject+CoreDataProperties.h"
#import "VipQuizObject+CoreDataProperties.h"
#import "VipQuizViewController.h"

@interface VipQuizzesViewController : LMBaseViewController<UICollectionViewDataSource,UICollectionViewDelegate>

- (void)setVipCategoryObject:(VipCategoryObject*)tempVipCategoryObject;

@end
