//
//  LessonSummaryViewController.h
//  Hollywood
//
//  Created by Kiril Kiroski on 5/22/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LMBaseViewController.h"
#import "ProgressLessonObject.h"

@interface LessonSummaryViewController : LMBaseViewController<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic) float vehiclePassCorrect;
@property(nonatomic) float vehicleNumber;

@end
