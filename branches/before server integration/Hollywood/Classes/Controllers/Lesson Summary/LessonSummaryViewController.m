//
//  LessonSummaryViewController.m
//  Hollywood
//
//  Created by Kiril Kiroski on 5/22/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LessonSummaryViewController.h"
#import "AchievementsTableViewCell.h"
#import "HomePageCollectionViewCell.h"

@interface LessonSummaryViewController (){
    
    __weak IBOutlet UIScrollView *leftScrollView;
    IBOutlet UIView *leftOutsideView;
    
    __weak IBOutlet UIView *topBarView;
    __weak IBOutlet UILabel *userScoreLabel;
    __weak IBOutlet UILabel *vipScoreLabel;
    __weak IBOutlet UILabel *vipStaticTextLabel;
    __weak IBOutlet UILabel *performanceTextLabel;
    
    __strong ProgressUserObject *currentProgressUserObject;
    __strong ProgressLevelObject *currentProgressLevelObject;
    __strong ProgressLessonObject *currentProgressLessonObject;
    
    __weak IBOutlet UIImageView *nextLessonImageView;
    __weak IBOutlet UILabel *currentLessonNumberLabel;
    __weak IBOutlet UILabel *currentLessonPointsLabel;
    __weak IBOutlet UILabel *currentLessonProcentLabel;
    
    __weak IBOutlet UIImageView *star1ImageView;
    __weak IBOutlet UIImageView *star2ImageView;
    __weak IBOutlet UIImageView *star3ImageView;
    
    __weak IBOutlet UIView *containerNextLessonImageView;
    __weak IBOutlet UIImageView *bigSummaryScreenImageView;
    
    __weak IBOutlet UITableView *achievementsTableView;
    NSMutableArray *achievementsArray;
    
    __weak IBOutlet UICollectionView *mainCollectionView;
    NSMutableArray *lessonsInfoArray;
    NSInteger currentLesson;
    NSInteger maximumLessonNumber;
}

@end

@implementation LessonSummaryViewController

- (void)configureUI {
    
    [super configureUI];
    
    bigSummaryScreenImageView.layer.cornerRadius = 4.0;
    bigSummaryScreenImageView.layer.borderWidth = 1.0;
    bigSummaryScreenImageView.layer.borderColor = [UIColor colorWithRed:111.0/255.0 green:102.0/255.0 blue:55.0/255.0 alpha:1.0].CGColor;
    bigSummaryScreenImageView.layer.masksToBounds = YES;
    
    
    self.navigationController.navigationBarHidden = YES;
    topBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar_bg_up"]];
    
    vipScoreLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:15.0];
    vipStaticTextLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:18.0];
    userScoreLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:30.0];
    
    //[currentLessonNumberLabel fontSizeToFit];
    currentLessonProcentLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:18.0];
    currentLessonPointsLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:25.0];
    
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    
    [leftScrollView addSubview:leftOutsideView];
    leftScrollView.contentSize = leftOutsideView.frame.size;
    [self updateLabels];
    
    [achievementsTableView registerNib:[UINib nibWithNibName:@"AchievementsTableViewCell" bundle:nil] forCellReuseIdentifier:@"CellAchievements"];
    
    [mainCollectionView registerNib:[UINib nibWithNibName:@"HomePageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];

    [self loadData];
}
- (void)updateLabels{
    vipScoreLabel.text = [NSString stringWithFormat:@"%d",currentProgressUserObject.vipPasses];
    userScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
}

- (void)loadData {
    
    [super loadData];
    
    //Points, vip passes
    
    
    
    
    //userScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
    //userVipPassesLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];
    
    //TO DO: Get lesson info
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
    
    currentLesson = currentProgressLevelObject.currentUnit;
    long tempLesson = currentLesson - 1;
    currentProgressLessonObject = [DATA_MANAGER getLessonObjectForIndex:(int)tempLesson];
    
    
    currentLessonPointsLabel.text = [NSString stringWithFormat:@"%ld pt",(long)currentProgressLessonObject.scoreForLesson];
    currentLessonNumberLabel.text = [NSString stringWithFormat:@"%ld",(long)currentLesson];
    //[currentLessonNumberLabel fontSizeToFit];
    if(DEVICE_BIGER_SIDE > 568){
        currentLessonNumberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:100.0];
    }else  if(DEVICE_BIGER_SIDE == 568){
        currentLessonNumberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:71.0];
    }else if(DEVICE_BIGER_SIDE < 568){
        currentLessonNumberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:71.0];
    }
    float procent = (self.vehiclePassCorrect / self.vehicleNumber)*100;
    currentLessonProcentLabel.text = [NSString stringWithFormat:@"%.0f %%",procent];
    int numberOfStar = 0;
    
    if(procent > kParam005){
        numberOfStar = 3;
        star1ImageView.hidden = NO;
        star2ImageView.hidden = NO;
        star3ImageView.hidden = NO;
    }else if(procent > kParam004){
        numberOfStar = 2;
        star2ImageView.hidden = NO;
        star3ImageView.hidden = NO;
    }else if(procent > kParam003){
        numberOfStar = 1;
        star2ImageView.hidden = NO;
    }
    
    maximumLessonNumber = currentProgressLevelObject.maxUnit;
    
    currentLessonNumberLabel.text = [NSString stringWithFormat:@"%ld",(long)currentLesson];
    
    lessonsInfoArray = [NSMutableArray new];
    
    for (NSInteger i=0; i<=maximumLessonNumber; i++) {
        
        if ([[NSString stringWithFormat:@"iOS-Lessons-Strip_%02ld", (i+1)%10] isEqualToString:@"iOS-Lessons-Strip_00"]) {
            
            [lessonsInfoArray addObject:@{
                                               @"lessonStatus":@(1),
                                               @"lessonNumber":@(i+1),
                                               @"lessonImage":@"iOS-Lessons-Strip_10",
                                               @"leasonTeaserImage": @"teaser_lesson_10"}];
        }
        else {
            
            [lessonsInfoArray addObject:@{
                                               @"lessonStatus":@(1),
                                               @"lessonNumber":@(i+1),
                                               @"lessonImage":[NSString stringWithFormat:@"iOS-Lessons-Strip_%02ld", (i+1)%10],
                                               @"leasonTeaserImage": [NSString stringWithFormat:@"teaser_lesson_%02ld", (i+1)%10]}];
            
        }
        
    }
    
    for (NSInteger i=maximumLessonNumber+1; i<25; i++) {
        
        
        if ([[NSString stringWithFormat:@"iOS-Lessons-Strip_%02ld", (i+1)%10] isEqualToString:@"iOS-Lessons-Strip_00"]) {
            
            [lessonsInfoArray addObject:@{
                                               @"lessonStatus":@(3),
                                               @"lessonNumber":@(i+1),
                                               @"lessonImage":@"iOS-Lessons-Strip_10",
                                               @"leasonTeaserImage": @"teaser_lesson_10"}];
        }
        else {
            
            [lessonsInfoArray addObject:@{
                                               @"lessonStatus":@(3),
                                               @"lessonNumber":@(i+1),
                                               @"lessonImage":[NSString stringWithFormat:@"iOS-Lessons-Strip_%02ld", (i+1)%10],
                                               @"leasonTeaserImage": [NSString stringWithFormat:@"teaser_lesson_%02ld", (i+1)%10]}];
            
        }
        
    }
    
    id currentLessonData;
    
    if ([[NSString stringWithFormat:@"iOS-Lessons-Strip_%02ld", (maximumLessonNumber+1)%10] isEqualToString:@"iOS-Lessons-Strip_00"]) {
        
        currentLessonData = @{
                              @"lessonStatus":@(2),
                              @"lessonNumber":@(maximumLessonNumber+1),
                              @"lessonImage":@"iOS-Lessons-Strip_10",
                              @"leasonTeaserImage": @"teaser_lesson_10"};
        
    }
    else {
        
        currentLessonData = @{
                              @"lessonStatus":@(2),
                              @"lessonNumber":@(maximumLessonNumber+1),
                              @"lessonImage":[NSString stringWithFormat:@"iOS-Lessons-Strip_%02ld", (maximumLessonNumber+1)%10],
                              @"leasonTeaserImage": [NSString stringWithFormat:@"teaser_lesson_%02ld", (maximumLessonNumber+1)%10]};
        
    }
    
    [lessonsInfoArray replaceObjectAtIndex:maximumLessonNumber withObject:currentLessonData];
    
    
    [mainCollectionView reloadData];
    
    [mainCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:maximumLessonNumber inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
   
    NSString *teaserImageUrl = [[lessonsInfoArray objectAtIndex:tempLesson] objectForKey:@"leasonTeaserImage"];
    bigSummaryScreenImageView.image = [UIImage imageNamed:teaserImageUrl];
}

#pragma mark Interface functions
- (IBAction)goToHomePage:(id)sender {
    [AUDIO_MANAGER stopPlaying];
    [APP_DELEGATE buildHomePageStack];
}

- (IBAction)openUserZone:(id)sender {
    
    [APP_DELEGATE buildUserZoneStack];
}

- (IBAction)goToVipCategories:(id)sender {
    [APP_DELEGATE buildVipZoneStack];
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;//[achievementsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AchievementsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellAchievements"];
    //id option = [self.userOptionsArray objectAtIndex:indexPath.row];
    [cell setData];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 84.0;
}

#pragma mark UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [lessonsInfoArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    HomePageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    cell.fromLessonSummary = YES;
    //[cell setDataArray:dataArray[indexPath.row]];
    
    id lessonData = [lessonsInfoArray objectAtIndex:indexPath.row];
    [cell setLessonData:lessonData];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(160, 80);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    id lessonData = [lessonsInfoArray objectAtIndex:indexPath.row];
    NSInteger status = [[lessonData objectForKey:@"lessonStatus"] integerValue];
    
    if (status == 3) {
        return;
    }
    
    currentProgressLevelObject.currentUnit = (int)[[lessonData objectForKey:@"lessonNumber"] integerValue]-1;
    
    [APP_DELEGATE buildLessonStack];
}

@end
