//
//  AchievementsTableViewCell.h
//  Hollywood
//
//  Created by Kiril Kiroski on 5/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AchievementsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *vipAchievementInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *completeEpisodeStarsLabel;

- (void)setData;

@end
