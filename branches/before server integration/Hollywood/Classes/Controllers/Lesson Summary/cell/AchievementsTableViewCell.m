//
//  AchievementsTableViewCell.m
//  Hollywood
//
//  Created by Kiril Kiroski on 5/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "AchievementsTableViewCell.h"

@implementation AchievementsTableViewCell{
    __weak IBOutlet UILabel *currentAchievementsInfoLabel;
    __weak IBOutlet UILabel *vipAchievementsInfoLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setData{
    //currentAchievementsInfoLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:11.0];
    vipAchievementsInfoLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:10.0];
}

@end
