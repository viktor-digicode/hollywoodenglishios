//
//  LessonObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LessonObject+CoreDataProperties.h"

@implementation LessonObject (CoreDataProperties)

+ (NSFetchRequest<LessonObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"LessonObject"];
}

@dynamic initial;
@dynamic lesson_description;
@dynamic lesson_home_page_image;
@dynamic lesson_id;
@dynamic lesson_name;
@dynamic lesson_teaser;
@dynamic lesson_vehicles;

@end
