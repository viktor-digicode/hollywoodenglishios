//
//  InstanceAnswersSwipeObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersSwipeObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface InstanceAnswersSwipeObject (CoreDataProperties)

+ (NSFetchRequest<InstanceAnswersSwipeObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *free;
@property (nullable, nonatomic, copy) NSString *image;
@property (nullable, nonatomic, copy) NSNumber *isCorrect;
@property (nullable, nonatomic, copy) NSNumber *itemPostion;

@end

NS_ASSUME_NONNULL_END
