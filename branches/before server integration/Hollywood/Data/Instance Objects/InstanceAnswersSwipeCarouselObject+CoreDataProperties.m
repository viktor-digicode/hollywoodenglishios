//
//  InstanceAnswersSwipeCarouselObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersSwipeCarouselObject+CoreDataProperties.h"

@implementation InstanceAnswersSwipeCarouselObject (CoreDataProperties)

+ (NSFetchRequest<InstanceAnswersSwipeCarouselObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"InstanceAnswersSwipeCarouselObject"];
}

@dynamic afterClickAudio;
@dynamic afterClickImage;
@dynamic answerImage;
@dynamic carouselSegmentQuestionAudio;
@dynamic carouselSegmentQuestionText;
@dynamic carouselSegmentQuestionType;
@dynamic free;

@end
