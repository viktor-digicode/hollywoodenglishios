//
//  LessonObject+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LessonObject+CoreDataClass.h"
#import "BaseVehiclesObject+CoreDataClass.h"

@implementation LessonObject

- (void) setData:(NSDictionary*)dataDictionary{
    if( [dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.lesson_home_page_image = [self objectOrNilForKey:kRankLessonsLessonHomePageImage fromDictionary:dataDictionary];
        self.lesson_id = [NSNumber numberWithInteger:[[self objectOrNilForKey:kRankLessonsLessonId fromDictionary:dataDictionary]integerValue]];
        self.lesson_teaser = [self objectOrNilForKey:kRankLessonsLessonTeaser fromDictionary:dataDictionary];
        self.lesson_description = [self objectOrNilForKey:kRankLessonsLessonDescription fromDictionary:dataDictionary];
        self.lesson_name = [self objectOrNilForKey:kRankLessonsLessonName fromDictionary:dataDictionary];
        self.initial = [NSNumber numberWithInteger:[[self objectOrNilForKey:kRankLessonsInitial fromDictionary:dataDictionary]integerValue]];
        //self.lesson_vehicles = [self objectOrNilForKey:kRankLessonsLessonVehicles fromDictionary:dataDictionary];
        
        NSObject *receivedLessonsVehicles = [dataDictionary objectForKey:kRankLessonsLessonVehicles];
        //NSMutableArray *parsedLessonsVehicles = [NSMutableArray array];
        if ([receivedLessonsVehicles isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLessonsVehicles) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    BaseVehiclesObject *importedObject = [BaseVehiclesObject MR_createEntity];
                    //importedObject.instanceID  = (NSNumber*)item;
                    [importedObject setData:item];
                    //[parsedLessonsVehicles addObject:importedObject];
                    [self addLesson_vehiclesObject:importedObject];
                }
            }
        } else if ([receivedLessonsVehicles isKindOfClass:[NSNumber class]]) {
            BaseVehiclesObject *importedObject = [BaseVehiclesObject MR_createEntity];
            //[importedObject setData:(NSDictionary *)receivedLessonsVehicles];
            importedObject.instanceID  = (NSNumber*)receivedLessonsVehicles;
            //[parsedLessonsVehicles addObject:importedObject];
            [self addLesson_vehiclesObject:importedObject];
        }
        
        /* NSSet *lessonsVehiclesSet = [[NSSet alloc] init];
         [lessonsVehiclesSet setByAddingObjectsFromArray:[NSArray arrayWithArray:parsedLessonsVehicles]];
         self.lesson_vehicles = lessonsVehiclesSet;*/
        
        
    }
}



#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}
@end
