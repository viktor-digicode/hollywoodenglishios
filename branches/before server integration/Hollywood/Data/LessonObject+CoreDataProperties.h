//
//  LessonObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LessonObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface LessonObject (CoreDataProperties)

+ (NSFetchRequest<LessonObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *initial;
@property (nullable, nonatomic, copy) NSString *lesson_description;
@property (nullable, nonatomic, copy) NSString *lesson_home_page_image;
@property (nullable, nonatomic, copy) NSNumber *lesson_id;
@property (nullable, nonatomic, copy) NSString *lesson_name;
@property (nullable, nonatomic, copy) NSString *lesson_teaser;
@property (nullable, nonatomic, retain) NSSet<BaseVehiclesObject *> *lesson_vehicles;

@end

@interface LessonObject (CoreDataGeneratedAccessors)

- (void)addLesson_vehiclesObject:(BaseVehiclesObject *)value;
- (void)removeLesson_vehiclesObject:(BaseVehiclesObject *)value;
- (void)addLesson_vehicles:(NSSet<BaseVehiclesObject *> *)values;
- (void)removeLesson_vehicles:(NSSet<BaseVehiclesObject *> *)values;

@end

NS_ASSUME_NONNULL_END
