//
//  RankObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "RankObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface RankObject (CoreDataProperties)

+ (NSFetchRequest<RankObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *rank_achieved_audio;
@property (nullable, nonatomic, copy) NSNumber *rank_diploma_design;
@property (nullable, nonatomic, copy) NSString *rank_icon;
@property (nullable, nonatomic, copy) NSNumber *rank_id;
@property (nullable, nonatomic, copy) NSString *rank_lesson_Background;
@property (nullable, nonatomic, copy) NSString *rank_name;
@property (nullable, nonatomic, copy) NSNumber *rank_number;
@property (nullable, nonatomic, copy) NSString *rank_rank_achieved_text;
@property (nullable, nonatomic, copy) NSString *rank_teaser_text;
@property (nullable, nonatomic, retain) NSSet<LessonObject *> *rank_lessons;

@end

@interface RankObject (CoreDataGeneratedAccessors)

- (void)addRank_lessonsObject:(LessonObject *)value;
- (void)removeRank_lessonsObject:(LessonObject *)value;
- (void)addRank_lessons:(NSSet<LessonObject *> *)values;
- (void)removeRank_lessons:(NSSet<LessonObject *> *)values;

@end

NS_ASSUME_NONNULL_END
