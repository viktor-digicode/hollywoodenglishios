//
//  LevelObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LevelObject+CoreDataProperties.h"

@implementation LevelObject (CoreDataProperties)

+ (NSFetchRequest<LevelObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"LevelObject"];
}

@dynamic level_description;
@dynamic level_id;
@dynamic level_name;
@dynamic level_order;
@dynamic level_ranks;

@end
