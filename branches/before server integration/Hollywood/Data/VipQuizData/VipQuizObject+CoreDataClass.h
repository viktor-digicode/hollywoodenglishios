//
//  VipQuizObject+CoreDataClass.h
//  Hollywood
//
//  Created by Kiril Kiroski on 10/3/16.
//  Copyright © 2016 aa. All rights reserved.
//
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BaseVehiclesObject;

NS_ASSUME_NONNULL_BEGIN

@interface VipQuizObject : NSManagedObject

- (void) setData:(NSDictionary*)dataDictionary;

@end

NS_ASSUME_NONNULL_END

#import "VipQuizObject+CoreDataProperties.h"
