//
//  VipQuizObject+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 10/3/16.
//  Copyright © 2016 aa. All rights reserved.
//
#import "Constants.h"
#import "VipQuizObject+CoreDataClass.h"
#import "QuizInstance+CoreDataClass.h"
//#import "BaseVehiclesObject+CoreDataClass.h"

@implementation VipQuizObject

- (void) setData:(NSDictionary*)dataDictionary{
    if(self && [ dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.quizListDescription = [self objectOrNilForKey:kQuizListDescription fromDictionary: dataDictionary];
        self.orderInCategory = [NSNumber numberWithDouble:(double)[[self objectOrNilForKey:kQuizListOrderInCategory fromDictionary: dataDictionary] doubleValue]];
        self.quizId = [NSNumber numberWithDouble:(double)[[self objectOrNilForKey:kQuizListQuizId fromDictionary: dataDictionary] doubleValue]];
        self.quizName = [self objectOrNilForKey:kQuizListQuizName fromDictionary: dataDictionary];
        self.avEndDate = [self objectOrNilForKey:kQuizListAvEndDate fromDictionary: dataDictionary];
        self.imageUrl = [self objectOrNilForKey:kQuizListImageUrl fromDictionary: dataDictionary];
        self.price =  [NSNumber numberWithDouble:(double)[[self objectOrNilForKey:kQuizListPrice fromDictionary: dataDictionary] doubleValue]];
        self.availability = [[self objectOrNilForKey:kQuizListAvailability fromDictionary: dataDictionary] boolValue];
        self.avStartDate = [self objectOrNilForKey:kQuizListAvStartDate fromDictionary: dataDictionary];
        self.isPromo = [NSNumber numberWithDouble:(double)[[self objectOrNilForKey:kQuizListIsPromo fromDictionary: dataDictionary] doubleValue]];        self.teaser = [self objectOrNilForKey:kQuizListTeaser fromDictionary: dataDictionary];
        self.imageName = [self objectOrNilForKey:kQuizListImageName fromDictionary: dataDictionary];
       //self.instanceList = [self objectOrNilForKey:kQuizListInstanceList fromDictionary: dataDictionary];
        
        NSObject *receivedInstanceList = [self objectOrNilForKey:kQuizListInstanceList fromDictionary: dataDictionary];
        
        if ([receivedInstanceList isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedInstanceList) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    QuizInstance *tempQuizInstanceObject = [QuizInstance MR_createEntity];
                    [tempQuizInstanceObject setData:item];
                    [self addInstanceListObject:tempQuizInstanceObject];
                }
            }
        } else if ([receivedInstanceList isKindOfClass:[NSDictionary class]]) {
            QuizInstance *tempQuizInstanceObject = [QuizInstance MR_createEntity];
            [tempQuizInstanceObject setData:(NSDictionary*)receivedInstanceList];
            [self addInstanceListObject:tempQuizInstanceObject];
        }
    }
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}
@end
