//
//  VipCourseObject+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 10/3/16.
//  Copyright © 2016 aa. All rights reserved.
//
//
#import "VipCourseObject+CoreDataClass.h"
#import "VipCategoryObject+CoreDataClass.h"
#import "Constants.h"
//#import "VipCategoryObject.h"

@implementation VipCourseObject

- (void) setData:(NSDictionary*)dataDictionary{
    if([dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.vipCourse_id = 0;
        self.vipCourse_name = @"NEW";
        NSDictionary *vipCourseDictionary = [dataDictionary objectForKey:kBaseClassVipCourse];
        ////////
        NSObject *receivedVipCategories = [vipCourseDictionary objectForKey:kVipCourseVipCategories];
        
        if ([receivedVipCategories isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedVipCategories) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    VipCategoryObject *tempVipCategoryObject = [VipCategoryObject MR_createEntity];
                    [tempVipCategoryObject setData:item];
                    [self addVipCategoriesObject:tempVipCategoryObject];
                }
            }
        } else if ([receivedVipCategories isKindOfClass:[NSDictionary class]]) {
            VipCategoryObject *tempVipCategoryObject = [VipCategoryObject MR_createEntity];
            [tempVipCategoryObject setData:(NSDictionary*)receivedVipCategories];
            [self addVipCategoriesObject:tempVipCategoryObject];
        }
        
      
    }
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
