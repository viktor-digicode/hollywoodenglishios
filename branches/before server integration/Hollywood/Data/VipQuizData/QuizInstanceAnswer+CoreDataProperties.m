//
//  QuizInstanceAnswer+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 2/6/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "QuizInstanceAnswer+CoreDataProperties.h"

@implementation QuizInstanceAnswer (CoreDataProperties)

+ (NSFetchRequest<QuizInstanceAnswer *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"QuizInstanceAnswer"];
}

@dynamic answerId;
@dynamic answerText;
@dynamic imageName;
@dynamic imageUrl;
@dynamic isCorrect;

@end
