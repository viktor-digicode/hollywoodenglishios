//
//  QuizInstance+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 10/28/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "QuizInstance+CoreDataClass.h"

@implementation QuizInstance



- (void) setData:(NSDictionary*)dataDictionary{
    if([dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.quizInstanceId =  [NSNumber numberWithDouble:(double)[[self objectOrNilForKey:@"instance_id" fromDictionary: dataDictionary] doubleValue]];
        self.instanceQuestion = [self objectOrNilForKey:@"questionText" fromDictionary:dataDictionary];
        self.imageUrl = @"";//[self objectOrNilForKey:kVipCategoriesCategoryName fromDictionary:dataDictionary];
        self.imageName = @"";//[NSNumber numberWithDouble:[[self objectOrNilForKey:kVipCategoriesDisplayOrder fromDictionary:dataDictionary] doubleValue]];
        self.bigImageUrl = [self objectOrNilForKey:@"bigImage" fromDictionary:dataDictionary] ;
        //self.answersForInstance = [self objectOrNilForKey:@"answers" fromDictionary:dataDictionary] ;
        self.isOneImageLayout = [[self objectOrNilForKey:@"isOneImageLayout" fromDictionary:dataDictionary] boolValue];
        self.instanceType = [NSNumber numberWithDouble:(double)[[self objectOrNilForKey:@"instanceType" fromDictionary: dataDictionary] doubleValue]];;
        NSObject *receivedAnswersList = [self objectOrNilForKey:@"answers" fromDictionary: dataDictionary];
        if ([receivedAnswersList isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedAnswersList) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    QuizInstanceAnswer *tempQuizInstanceObject = [QuizInstanceAnswer MR_createEntity];
                    [tempQuizInstanceObject setData:item];
                    [self addAnswersForInstanceObject:tempQuizInstanceObject];
                }
            }
        } else if ([receivedAnswersList isKindOfClass:[NSDictionary class]]) {
            QuizInstanceAnswer *tempQuizInstanceObject = [QuizInstanceAnswer MR_createEntity];
            [tempQuizInstanceObject setData:(NSDictionary*)receivedAnswersList];
            [self addAnswersForInstanceObject:tempQuizInstanceObject];
        }
        
    }
}


#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}
@end
