//
//  VehicleObjectVideo+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectVideo+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VehicleObjectVideo (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectVideo *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *textInfo;
@property (nullable, nonatomic, copy) NSString *videoHighQuality;
@property (nullable, nonatomic, copy) NSString *videoLowQuality;
@property (nullable, nonatomic, copy) NSString *videoPreview;
@property (nullable, nonatomic, copy) NSString *videoType;

@end

NS_ASSUME_NONNULL_END
