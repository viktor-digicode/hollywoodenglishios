//
//  VehicleObjectVideo+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectVideo+CoreDataProperties.h"

@implementation VehicleObjectVideo (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectVideo *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"VehicleObjectVideo"];
}

@dynamic textInfo;
@dynamic videoHighQuality;
@dynamic videoLowQuality;
@dynamic videoPreview;
@dynamic videoType;

@end
