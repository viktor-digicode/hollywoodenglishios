//
//  VehicleObjectSwipeCarousel+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectSwipeCarousel+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VehicleObjectSwipeCarousel (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectSwipeCarousel *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSSet<InstanceAnswersSwipeObject *> *instanceAnswers;

@end

@interface VehicleObjectSwipeCarousel (CoreDataGeneratedAccessors)

- (void)addInstanceAnswersObject:(InstanceAnswersSwipeObject *)value;
- (void)removeInstanceAnswersObject:(InstanceAnswersSwipeObject *)value;
- (void)addInstanceAnswers:(NSSet<InstanceAnswersSwipeObject *> *)values;
- (void)removeInstanceAnswers:(NSSet<InstanceAnswersSwipeObject *> *)values;

@end

NS_ASSUME_NONNULL_END
