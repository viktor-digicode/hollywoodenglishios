//
//  VehicleObject_Swipe+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObject_Swipe+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VehicleObject_Swipe (CoreDataProperties)

+ (NSFetchRequest<VehicleObject_Swipe *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *initial;
@property (nullable, nonatomic, copy) NSString *lesson_description;
@property (nullable, nonatomic, copy) NSString *lesson_home_page_image;
@property (nullable, nonatomic, copy) NSNumber *lesson_id;
@property (nullable, nonatomic, copy) NSString *lesson_name;
@property (nullable, nonatomic, copy) NSString *lesson_teaser;
@property (nullable, nonatomic, retain) NSSet<InstanceAnswersSwipeObject *> *instanceAnswers;

@end

@interface VehicleObject_Swipe (CoreDataGeneratedAccessors)

- (void)addInstanceAnswersObject:(InstanceAnswersSwipeObject *)value;
- (void)removeInstanceAnswersObject:(InstanceAnswersSwipeObject *)value;
- (void)addInstanceAnswers:(NSSet<InstanceAnswersSwipeObject *> *)values;
- (void)removeInstanceAnswers:(NSSet<InstanceAnswersSwipeObject *> *)values;

@end

NS_ASSUME_NONNULL_END
