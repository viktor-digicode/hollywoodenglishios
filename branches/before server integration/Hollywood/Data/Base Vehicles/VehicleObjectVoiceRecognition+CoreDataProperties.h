//
//  VehicleObjectVoiceRecognition+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectVoiceRecognition+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VehicleObjectVoiceRecognition (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectVoiceRecognition *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *mainImageId;
@property (nullable, nonatomic, copy) NSString *targetAudio;
@property (nullable, nonatomic, copy) NSString *targetText;

@end

NS_ASSUME_NONNULL_END
