//
//  VehicleObject_Spelling+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObject_Spelling+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VehicleObject_Spelling (CoreDataProperties)

+ (NSFetchRequest<VehicleObject_Spelling *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *sentence;

@end

NS_ASSUME_NONNULL_END
