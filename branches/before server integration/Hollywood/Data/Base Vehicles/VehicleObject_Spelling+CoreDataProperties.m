//
//  VehicleObject_Spelling+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObject_Spelling+CoreDataProperties.h"

@implementation VehicleObject_Spelling (CoreDataProperties)

+ (NSFetchRequest<VehicleObject_Spelling *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"VehicleObject_Spelling"];
}

@dynamic sentence;

@end
