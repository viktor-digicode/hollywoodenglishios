//
//  VehicleObject_Swipe+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObject_Swipe+CoreDataProperties.h"

@implementation VehicleObject_Swipe (CoreDataProperties)

+ (NSFetchRequest<VehicleObject_Swipe *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"VehicleObject_Swipe"];
}

@dynamic initial;
@dynamic lesson_description;
@dynamic lesson_home_page_image;
@dynamic lesson_id;
@dynamic lesson_name;
@dynamic lesson_teaser;
@dynamic instanceAnswers;

@end
