//
//  RankObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "RankObject+CoreDataProperties.h"

@implementation RankObject (CoreDataProperties)

+ (NSFetchRequest<RankObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"RankObject"];
}

@dynamic rank_achieved_audio;
@dynamic rank_diploma_design;
@dynamic rank_icon;
@dynamic rank_id;
@dynamic rank_lesson_Background;
@dynamic rank_name;
@dynamic rank_number;
@dynamic rank_rank_achieved_text;
@dynamic rank_teaser_text;
@dynamic rank_lessons;

@end
