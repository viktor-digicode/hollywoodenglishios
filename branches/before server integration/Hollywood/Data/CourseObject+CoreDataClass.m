//
//  CourseObject+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "CourseObject+CoreDataClass.h"
#import "LevelObject+CoreDataClass.h"

@interface CourseObject ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end


@implementation CourseObject

- (void) setData:(NSDictionary*)dataDictionary{
    if([dataDictionary isKindOfClass:[NSDictionary class]]) {
        //self.course_source_language = [self objectOrNilForKey:kCourseObjectCourseSourceLanguage fromDictionary:dataDictionary];
        self.course_name = @"Hollywood";//[self objectOrNilForKey:kCourseObjectCourseName fromDictionary:dataDictionary];
        self.course_id = [[self objectOrNilForKey:kCourseObjectCourseId fromDictionary:dataDictionary] stringValue];//?chek
        //self.course_description = [self objectOrNilForKey:kCourseObjectCourseDescription fromDictionary:dataDictionary];
        ////////
        NSObject *receivedCourseLevels = [dataDictionary objectForKey:kCourseObjectCourseLevels];
        //NSMutableArray *parsedCourseLevels = [NSMutableArray array];
        if ([receivedCourseLevels isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedCourseLevels) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    LevelObject *importedObject = [LevelObject MR_createEntity];
                    [importedObject setData:item];
                    //[parsedCourseLevels addObject:importedObject];
                    [self addCourse_levelsObject:importedObject];
                }
            }
        } else if ([receivedCourseLevels isKindOfClass:[NSDictionary class]]) {
            LevelObject *importedObject = [LevelObject MR_createEntity];
            [importedObject setData:(NSDictionary *)receivedCourseLevels];
            //[parsedCourseLevels addObject:importedObject];
            [self addCourse_levelsObject:importedObject];
            /*[parsedCourseLevels addObject:[LevelObject modelObjectWithDictionary:(NSDictionary *)receivedCourseLevels]];*/
        }
        
        /*NSSet *courseLevelsSet = [[NSSet alloc] init];
         [courseLevelsSet setByAddingObjectsFromArray:[NSArray arrayWithArray:parsedCourseLevels]];
         //self.course_levels = courseLevelsSet;
         [self addCourse_levels:courseLevelsSet];*/
    }
}

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.course_source_language = [self objectOrNilForKey:kCourseObjectCourseSourceLanguage fromDictionary:dict];
        self.course_name = [self objectOrNilForKey:kCourseObjectCourseName fromDictionary:dict];
        self.course_id = [self objectOrNilForKey:kCourseObjectCourseId fromDictionary:dict];
        NSObject *receivedCourseLevels = [dict objectForKey:kCourseObjectCourseLevels];
        NSMutableArray *parsedCourseLevels = [NSMutableArray array];
        if ([receivedCourseLevels isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedCourseLevels) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    //[parsedCourseLevels addObject:[CourseLevels modelObjectWithDictionary:item]];
                }
            }
        } else if ([receivedCourseLevels isKindOfClass:[NSDictionary class]]) {
            //[parsedCourseLevels addObject:[CourseLevels modelObjectWithDictionary:(NSDictionary *)receivedCourseLevels]];
        }
        
        NSSet *courseLevelsSet = [[NSSet alloc] init];
        [courseLevelsSet setByAddingObjectsFromArray:[NSArray arrayWithArray:parsedCourseLevels]];
        self.course_levels = courseLevelsSet;
        self.course_description = [self objectOrNilForKey:kCourseObjectCourseDescription fromDictionary:dict];
        
    }
    
    return self;
    
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end

