//
//  ProgressVehicleObject.h
//  Hollywood
//
//  Created by Kiril Kiroski on 8/10/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProgressVehicleObject : NSManagedObject

- (void) setData:(NSDictionary*)dataDictionary;
- (void) setInitialData;

@end

NS_ASSUME_NONNULL_END

#import "ProgressVehicleObject+CoreDataProperties.h"
