//
//  ProgressUserObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 8/11/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ProgressUserObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProgressUserObject (CoreDataProperties)

@property (nonatomic) int32_t maxLevel;
@property (nonatomic) int32_t currentLevel;
@property (nonatomic) int32_t score;
@property (nonatomic) int32_t vipPasses;
@property (nonatomic) int32_t rank;
@property (nullable, nonatomic, retain) NSSet<ProgressLevelObject *> *levelsProgres;

@end

@interface ProgressUserObject (CoreDataGeneratedAccessors)

- (void)addLevelsProgresObject:(ProgressLevelObject *)value;
- (void)removeLevelsProgresObject:(ProgressLevelObject *)value;
- (void)addLevelsProgres:(NSSet<ProgressLevelObject *> *)values;
- (void)removeLevelsProgres:(NSSet<ProgressLevelObject *> *)values;

@end

NS_ASSUME_NONNULL_END
