//
//  ProgressUserObject.h
//  Hollywood
//
//  Created by Kiril Kiroski on 8/10/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ProgressLevelObject;

NS_ASSUME_NONNULL_BEGIN

@interface ProgressUserObject : NSManagedObject

- (void) setData:(NSDictionary*)dataDictionary;
- (void) setInitialData;

@end

NS_ASSUME_NONNULL_END

#import "ProgressUserObject+CoreDataProperties.h"
