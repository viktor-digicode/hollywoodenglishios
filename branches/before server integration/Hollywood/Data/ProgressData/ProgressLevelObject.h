//
//  ProgressLevelObject.h
//  Hollywood
//
//  Created by Kiril Kiroski on 8/10/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ProgressLessonObject;

NS_ASSUME_NONNULL_BEGIN

@interface ProgressLevelObject : NSManagedObject

- (void) setData:(NSDictionary*)dataDictionary;
- (void) setInitialData;

@end

NS_ASSUME_NONNULL_END

#import "ProgressLevelObject+CoreDataProperties.h"
