//
//  LessonObject+CoreDataClass.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BaseVehiclesObject;

NS_ASSUME_NONNULL_BEGIN

@interface LessonObject : NSManagedObject

- (void) setData:(NSDictionary*)dataDictionary;

@end

NS_ASSUME_NONNULL_END

#import "LessonObject+CoreDataProperties.h"
