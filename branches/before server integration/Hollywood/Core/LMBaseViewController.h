//
//  LMBaseViewController.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/24/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LMBaseViewController : UIViewController

- (void)configureAppearance;
- (void)configureUI;
- (void)configureObservers;
- (void)configureNavigation;
- (void)configureData;
- (void)loadData;
- (void)layout;
- (void)dismissObservers;

- (void)addVideoVehiclesIndicator;
- (void)removeVideoIndicator;

- (void)addActivityIndicator;
- (void)removeActivityIndicator;
- (void)addLoadingVehiclesIndicator;

@end
