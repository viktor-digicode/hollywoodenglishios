//
//  LMBaseViewController.m
//  Hollywood
//
//  Created by Kiril Kiroski on 3/24/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "LMBaseViewController.h"
#import "UINavigationController+Orientation.h"
#import "FLAnimatedImage.h"

@interface LMBaseViewController ()

@property(strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property(strong, nonatomic) UIView *activityIndicatorBacground;
@property(strong, nonatomic) UILabel *activityIndicatorLabel;
@property(strong, nonatomic) UIView *smallActivityBackground;
@property(strong, nonatomic) UIImageView *bgImageView;
@property(strong, nonatomic) FLAnimatedImageView *gifImageView;

@end

@implementation LMBaseViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureUI];
    [self configureData];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self configureAppearance];
    [self configureNavigation];
    [self configureObservers];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self loadData];
    [self layout];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self dismissObservers];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark - Implementation

- (void)configureAppearance
{

}

- (void)configureUI
{
 
}

- (void)configureObservers
{
}

- (void)configureNavigation
{
}

- (void)configureData
{
}

- (void)loadData
{
    
}

- (void)layout
{
}

- (void)dismissObservers
{
}

#pragma mark - Status Bar Style

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

#pragma mark - done Action
- (void)doneAction
{
}

#pragma mark - show Settings
- (void)showSettings
{
}




#pragma mark - ActivityIndicator
- (void)addVideoVehiclesIndicator{
    dispatch_async(
                   dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       sleep(0.2);
                       dispatch_async(dispatch_get_main_queue(), ^{
                           if (!self.bgImageView) {
                               self.bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                               self.bgImageView.image = [UIImage imageNamed:@"videoBg"];
                               [self.view addSubview:self.bgImageView];
                           }
                           if (!self.gifImageView) {
                               NSURL *url = [[NSBundle mainBundle] URLForResource:@"Preloader_03" withExtension:@"gif"];
                               NSData *data = [NSData dataWithContentsOfURL:url];
                               FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:data];
                               self.gifImageView = [[FLAnimatedImageView alloc] init];
                               self.gifImageView.animatedImage = image;
                               self.gifImageView.frame = CGRectMake(0, 0, 50, 50);
                               self.gifImageView.center = self.bgImageView.center;
                               [self.view addSubview:self.gifImageView];
                           }
                       });
                   }
                   );
    
}


- (void)removeVideoIndicator {
    
    if (self.bgImageView) {
        [self.bgImageView removeFromSuperview];
        self.bgImageView = nil;
    }
    if (self.gifImageView) {
       // [self.gifImageView stopAnimating];
        [self.gifImageView removeFromSuperview];
        [self.gifImageView stopAnimating];
        self.gifImageView = nil;
    }
}

- (void)addLoadingVehiclesIndicator{
    [self addVideoVehiclesIndicator];
    return;
    
    if (!self.activityIndicatorBacground) {
        self.activityIndicatorBacground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.activityIndicatorBacground.userInteractionEnabled = YES;
        self.activityIndicatorBacground.backgroundColor = APP_GOLD_COLOR;
        self.activityIndicatorBacground.alpha = 0.5;
        [self.view addSubview:self.activityIndicatorBacground];
        
        
        if (!self.gifImageView) {
            NSURL *url = [[NSBundle mainBundle] URLForResource:@"Preloader_03" withExtension:@"gif"];
            NSData *data = [NSData dataWithContentsOfURL:url];
            FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:data];
            self.gifImageView = [[FLAnimatedImageView alloc] init];
            self.gifImageView.animatedImage = image;
            self.gifImageView.frame = CGRectMake(0,0, 50, 50);
            self.gifImageView.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
            [self.view addSubview:self.gifImageView];
        }
        /*self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.view addSubview:self.activityIndicator];
        self.activityIndicator.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.activityIndicator.color = [UIColor blackColor];
        self.activityIndicator.center = CGPointMake(self.view.frame.size.width / 2, 30 + self.view.frame.size.height / 2);
        [self.activityIndicator startAnimating];
        
        self.activityIndicatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.activityIndicator.y + 50, DEVICE_BIGER_SIDE, 50)];
        [self.activityIndicatorLabel setTextAlignment:NSTextAlignmentCenter];
        [self.activityIndicatorLabel setFont:[UIFont systemFontOfSize:15.0]];
        [self.activityIndicatorLabel setTextColor:[UIColor blackColor]];
        self.activityIndicatorLabel.text = @"Loading vehicles...";
        [self.view addSubview:self.activityIndicatorLabel];
        [self.activityIndicatorLabel sizeToFit];
        
        
        [self.view bringSubviewToFront:self.activityIndicatorLabel];
        [self.view bringSubviewToFront:self.activityIndicator];
        
        self.activityIndicatorLabel.center = CGPointMake(self.activityIndicatorBacground.center.x, self.activityIndicatorBacground.center.y);
         */
        
    }

}

- (void)addActivityIndicatorForHelpWebView
{
    if (!self.bgImageView) {
        self.bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height)];
        self.bgImageView.image = [UIImage imageNamed:@"splashScreenImage"];
        [self.view addSubview:self.bgImageView];
    }
    if (!self.gifImageView) {
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"Preloader_03" withExtension:@"gif"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:data];
        self.gifImageView = [[FLAnimatedImageView alloc] init];
        self.gifImageView.animatedImage = image;
        self.gifImageView.frame = CGRectMake(0, 0, 50, 50);
        self.gifImageView.center = self.bgImageView.center;
        [self.view addSubview:self.gifImageView];
    }
}


- (void)addActivityIndicator
{
    [self addVideoVehiclesIndicator];
    return;
    
    if (!self.activityIndicatorBacground) {
        self.activityIndicatorBacground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.activityIndicatorBacground.userInteractionEnabled = YES;
        self.activityIndicatorBacground.backgroundColor = [UIColor darkGrayColor];
        self.activityIndicatorBacground.alpha = 0.5;
        [self.view addSubview:self.activityIndicatorBacground];
        
        if (!self.gifImageView) {
            NSURL *url = [[NSBundle mainBundle] URLForResource:@"Preloader_03" withExtension:@"gif"];
            NSData *data = [NSData dataWithContentsOfURL:url];
            FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:data];
            self.gifImageView = [[FLAnimatedImageView alloc] init];
            self.gifImageView.animatedImage = image;
            self.gifImageView.frame = CGRectMake(0,0, 50, 50);
            self.gifImageView.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
            [self.view addSubview:self.gifImageView];
        }
        /*self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.view addSubview:self.activityIndicator];
        self.activityIndicator.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.activityIndicator.color = APP_GOLD_COLOR;
        self.activityIndicator.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
        [self.activityIndicator startAnimating];
        
        
        [self.view bringSubviewToFront:self.activityIndicatorLabel];
        [self.view bringSubviewToFront:self.activityIndicator];
        
        self.activityIndicatorLabel.center = CGPointMake(self.smallActivityBackground.center.x, self.activityIndicatorLabel.center.y);*/
        
    }
}
- (void)addBaseActivityIndicator
{
  
    if (!self.activityIndicatorBacground) {
        self.activityIndicatorBacground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        self.activityIndicatorBacground.userInteractionEnabled = YES;
        self.activityIndicatorBacground.backgroundColor = [UIColor darkGrayColor];
        self.activityIndicatorBacground.alpha = 0.5;
        [self.view addSubview:self.activityIndicatorBacground];
        
        if (!self.gifImageView) {
            NSURL *url = [[NSBundle mainBundle] URLForResource:@"Preloader_03" withExtension:@"gif"];
            NSData *data = [NSData dataWithContentsOfURL:url];
            FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:data];
            self.gifImageView = [[FLAnimatedImageView alloc] init];
            self.gifImageView.animatedImage = image;
            self.gifImageView.frame = CGRectMake(0,0, 50, 50);
            self.gifImageView.center = CGPointMake( [UIScreen mainScreen].bounds.size.width / 2,  [UIScreen mainScreen].bounds.size.height / 2);
            [self.view addSubview:self.gifImageView];
        }
    }
}
- (void)removeActivityIndicator {
    
    if (self.bgImageView) {
        [self.bgImageView removeFromSuperview];
        self.bgImageView = nil;
    }
    if (self.activityIndicator) {
        [self.activityIndicator stopAnimating];
        [self.activityIndicator removeFromSuperview];
    }
    if (self.activityIndicatorBacground) {
        [self.activityIndicatorBacground removeFromSuperview];
    }
    if (self.activityIndicatorLabel) {
        [self.activityIndicatorLabel removeFromSuperview];
    }
    if (self.smallActivityBackground) {
        [self.smallActivityBackground removeFromSuperview];
    }
    self.activityIndicator = nil;
    self.activityIndicatorBacground = nil;
    self.activityIndicatorLabel = nil;
    self.smallActivityBackground = nil;
    
    if (self.gifImageView) {
        // [self.gifImageView stopAnimating];
        [self.gifImageView removeFromSuperview];
        [self.gifImageView stopAnimating];
        self.gifImageView = nil;
    }
}

#pragma mark Orientations
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotate {
    
    return NO;
}

- (BOOL)prefersStatusBarHidden {
    
 return YES;
    
}

-(BOOL)chekInternet{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:
                [ALERT_MANAGER showCustomAlertWithTitle:[LABEL_MANAGER setLocalizeLabelText:@"error_problem"]  andMessage:[LABEL_MANAGER setLocalizeLabelText:@"error_message_1"]];
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                break;
            default:
                NSLog(@"Unkown network status");
                
        }
        
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    if(![AFNetworkReachabilityManager sharedManager].reachable){
        //[ALERT_MANAGER showCustomAlertWithTitle:[LABEL_MANAGER setLocalizeLabelText:@"error_problem"]  andMessage:[LABEL_MANAGER setLocalizeLabelText:@"error_message_1"]];
        return NO;
    }else{
        return YES;
    }
}

@end
