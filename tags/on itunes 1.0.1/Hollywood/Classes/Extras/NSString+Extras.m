//  NSString+Extras.m
//  Created by Dimitar Tasev in February 2014.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


#import "NSString+Extras.h"
#import <CommonCrypto/CommonCrypto.h>


@implementation NSString (Extras)

#pragma mark - Unicode


- (NSString *)punctuationFree {
  static NSCharacterSet *punctuation = 0;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    punctuation = [NSCharacterSet characterSetWithCharactersInString:@",.?!:;"];
  });
  return [self stringByTrimmingCharactersInSet:punctuation];
}

+ (BOOL)containsUnicode:(NSString *)string {
  static NSCharacterSet *_ansi = 0;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{ _ansi = [[NSCharacterSet characterSetWithRange:(NSRange) { 0, 128 }] invertedSet]; });
  return NSNotFound != [string rangeOfCharacterFromSet:_ansi].location;
}

#pragma mark - URL Schemes

+ (NSString *)phoneURL:(NSString *)number {
  assert_valid_null(number) NSString *phoneNumber = number;
  NSString *expression = @"\\s+\\([^()]*\\)";
  while ([phoneNumber rangeOfString:expression options:NSRegularExpressionSearch | NSCaseInsensitiveSearch].location != NSNotFound) {
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:expression
                                                         withString:@""
                                                            options:NSRegularExpressionSearch | NSCaseInsensitiveSearch
                                                              range:NSMakeRange(0, [phoneNumber length])];
  }
  if (!empty_string(phoneNumber)) {
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
  }
  if (![number hasPrefix:@"tel"]) {
    phoneNumber = [@"telprompt:" stringByAppendingString:phoneNumber];
  }
  return phoneNumber;
}

+ (NSString *)emailURL:(NSString *)path {
  return [@"mailto:" stringByAppendingString:path];
}

+ (NSString *)linkURL:(NSString *)path {
  NSString *webPath = path;
  if (![webPath hasPrefix:@"http"]) {
    webPath = [@"https://" stringByAppendingString:webPath];
  }
  return webPath;
}

#pragma mark - Validation

+ (BOOL)validatePassword:(NSString *)pass {
  NSString *passRegex = @"[a-zA-Z0-9]{8,15}";
  NSPredicate *passTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passRegex];
  BOOL isValid = [passTest evaluateWithObject:pass];
  return isValid;
}

+ (BOOL)validateEmail:(NSString *)email {
  NSString *emailRegEx = @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

  NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegEx];
  return [regExPredicate evaluateWithObject:[email lowercaseString]];
}

#pragma mark - Paths

+ (NSString *)documentsDir {
  NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);

  return [documentsPaths objectAtIndex:0];
}

+ (NSString *)libraryDir {
  NSArray *libraryPaths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);

  return [libraryPaths objectAtIndex:0];
}

+ (NSString *)cachesDir {
  NSArray *cachesPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);

  return [cachesPaths objectAtIndex:0];
}

#pragma mark - UUID

+ (NSString *)getUUID {
  CFUUIDRef uuidRef = CFUUIDCreate(NULL);
  CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
  NSString *uuid = [NSString stringWithString:(__bridge NSString *)uuidStringRef];
  CFRelease(uuidRef);
  CFRelease(uuidStringRef);
  return uuid;
}

#pragma mark - Base64

+ (NSString *)md5:(NSString *)input {
  const char *cStr = [input UTF8String];
  unsigned char result[16];
  CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
  return
    [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x", result[0], result[1], result[2], result[3], result[4],
                               result[5], result[6], result[7], result[8], result[9], result[10], result[11], result[12], result[13], result[14], result[15]];
}

+ (NSString *)getSHA1ForString:(NSString *)input {
  const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
  NSData *data = [NSData dataWithBytes:cstr length:input.length];
  uint8_t digest[CC_SHA1_DIGEST_LENGTH];

  CC_SHA1(data.bytes, (CC_LONG)data.length, digest);
  NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
  for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
    [output appendFormat:@"%02x", digest[i]];
  }
  return output;
}

+ (NSData *)dataFromBase64String:(NSString *)input {
  return 0;//[NSData dataFromBase64String:input];
}

+ (NSString *)AES256EncryptedBase64String:(NSString *)input key:(NSString *)key {
  return 0;//[[[input dataUsingEncoding:NSUTF8StringEncoding] AES256EncryptWithKey:key] base64EncodedString];
}

+ (NSString *)AES256DecryptedBase64String:(NSString *)input key:(NSString *)key {
  return 0;//[[[NSData dataFromBase64String:input] AES256DecryptWithKey:key] stringUsingEncoding:NSUTF8StringEncoding];
}

+ (NSString *)encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)string, NULL,
                                                                                 (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ", kCFStringEncodingUTF8));
}


- (int) wordCount
{
    NSArray *words = [self componentsSeparatedByString:@" "];
    return (int)[words count];
}



@end
