//
//  LanguagesSelectionViewController.m
//  Hollywood
//
//  Created by Kiril Kiroski on 7/12/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LanguagesSelectionViewController.h"
#import "LanguagesCollectionViewCell.h"
#import <SSKeychain.h>

@interface LanguagesSelectionViewController (){
    NSArray *languagesArray;
    __weak IBOutlet UIButton *restoreButton;
    __weak IBOutlet UIButton *clearButton;
    __weak IBOutlet UIImageView *yellowImageView;
    __weak IBOutlet UILabel *selectLanguageLabel;
}

@end

@implementation LanguagesSelectionViewController

-(void)configureUI{
    
    [super configureUI];
    self.navigationController.navigationBarHidden = YES;
    [restoreButton setHidden:YES];
    [clearButton setHidden:YES];
    [self addBaseActivityIndicator];
    selectLanguageLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"select_language"];
    
    yellowImageView.layer.cornerRadius = 4.0;
    yellowImageView.layer.borderColor = [UIColor blackColor].CGColor;
    yellowImageView.layer.borderWidth = 1.0;
    
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kLanguageSelection action:@"Select_Language_Screen" additionalData:nil];
}

- (void)loadData {
    
    [super loadData];
    //[self addActivityIndicator];
    [self addBaseActivityIndicator];
    [self chekInternet];
    [REQUEST_MANAGER getDataFor:kRequestGetLanguagesAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
        NSDictionary *responseDictionary = [responseObject objectForKey:kApiObjectResponse];
        languagesArray = [responseDictionary objectForKey:kApiObjectLanguages];
        //[mainCollectionView reloadData];
        [self removeActivityIndicator];
    } andWithErrorCallBack:^(NSString *errorMessage) {
        [self removeActivityIndicator];
    }];
    NSString *appName=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
    NSString *strApplicationUUID = [SSKeychain passwordForService:appName account:@"incoding"];
    if (strApplicationUUID != nil)
    {
        [restoreButton setHidden:NO];
        [clearButton setHidden:NO];
    }else{
        [restoreButton setHidden:YES];
        [clearButton setHidden:YES];
    }
    return;
}


- (IBAction)restoreButtonClick:(id)sender {
    NSString *appName=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
    NSString *courseID = [SSKeychain passwordForService:appName account:@"courseID"];
    REQUEST_MANAGER.courseID = courseID;
    [self addBaseActivityIndicator];
    [self chekInternet];
    [REQUEST_MANAGER getDataFor:kRequestFirstLaunchAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
        [DATA_MANAGER initializeStartData:responseObject];
        NSDictionary *responseDictonary = [responseObject valueForKey:kApiObjectResponse];
        [PARAMETER_MANAGER initializeStartData:responseDictonary];
        NSNumber *courseRevision = [responseDictonary valueForKey:@"revision"];
        NSNumber *courseVersion = [responseDictonary valueForKey:@"version"];
        
        
        NSDictionary *labelsDictionary = [responseDictonary valueForKey: kApiObjectLabels];
        NSNumber *labelVersion = [labelsDictionary valueForKey:kUserLabelVersion];
        
        NSDictionary *appParametersDictionary = [responseDictonary valueForKey:kApiObjectAppParameters];
        NSNumber *parametersVersion = [appParametersDictionary valueForKey:kParameterVersion];
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setObject:courseRevision forKey:kUserCourseRevision];
        [userDefault setObject:courseVersion forKey:kUserCourseVersion];
        [userDefault setObject:labelVersion forKey:kUserLabelVersion];
        [userDefault setObject:parametersVersion forKey:kParameterVersion];
        
        [self removeActivityIndicator];
        [self makeUser];
    } andWithErrorCallBack:^(NSString *errorMessage) {
        [self removeActivityIndicator];
    }];
}

- (void)makeUser{
    NSString *appName=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
    NSString *strApplicationUUID = [SSKeychain passwordForService:appName account:@"incoding"];
    NSString *productIdentifier = [SSKeychain passwordForService:appName account:@"productIdentifier"];
    [USER_MANAGER initProductIdentifier: productIdentifier];
    //NSString *transactionDate = [SSKeychain passwordForService:appName account:@"transactionDate"];
    NSString *userAppId = [SSKeychain passwordForService:appName account:@"userApplicationId"];
    
    if (strApplicationUUID != nil)
    {
        [USER_MANAGER initializeWithGuest];
        [USER_MANAGER setUserApplicationId:userAppId];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@YES forKey:kLastValidLoginUser];
        [APP_DELEGATE buildHomePageStack];
    }

}
- (IBAction)deleteDataButtonClick:(id)sender {
    
    NSString *appName=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
    [SSKeychain deletePasswordForService:appName account:@"incoding"];
    [SSKeychain deletePasswordForService:appName account:@"productIdentifier"];
    [SSKeychain deletePasswordForService:appName account:kTransactionDate];
    [SSKeychain deletePasswordForService:appName account:@"userApplicationId"];
    [SSKeychain deletePasswordForService:appName account:@"courseID"];
    [self loadData];
    
}

#pragma mark
#pragma mark Interface functions
- (IBAction)pressBrazilButton:(id)sender {
    if([languagesArray count] == 0){
        return;
    }
    
    NSDictionary *cellData = [languagesArray objectAtIndex:0];
    NSString *idString = [cellData objectForKey:kLevelRanksRankId];
    NSLog(@"ID: %@", idString);
    REQUEST_MANAGER.courseID = idString;
    [APP_DELEGATE buildUserStatusSelectionStack];
    [[NSUserDefaults standardUserDefaults] setObject:idString forKey:kUserCourseID];
}
#pragma mark Interface Orientation

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskPortrait;
    
}

- (BOOL)shouldAutorotate {
    
    return YES;
}
@end
