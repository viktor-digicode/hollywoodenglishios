//
//  UserZoneViewController.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 8/10/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserZoneViewController : LMBaseViewController<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *vipLabel;

@end
