//
//  SettingsStatusViewController.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/26/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelpViewController.h"
#import <FCAlertView/FCAlertView.h>

@interface SettingsStatusViewController : LMBaseViewController

@property (weak, nonatomic) IBOutlet UILabel *userScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *userVipScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewUserRank;
@property (weak, nonatomic) IBOutlet UIButton *helpButton;

@end
