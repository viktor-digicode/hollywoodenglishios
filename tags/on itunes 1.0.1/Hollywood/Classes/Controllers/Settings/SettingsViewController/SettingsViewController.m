//
//  SettingsViewController.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/26/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "SettingsViewController.h"
#import "UserSettingsTableViewCell.h"
#import "SettingsDiplomasViewController.h"
#import "SettingsStatusViewController.h"

@interface SettingsViewController ()<CacheManagerDelegate>
{
    __strong ProgressUserObject *currentProgressUserObject;
    __strong ProgressLevelObject *currentProgressLevelObject;
}

@end

@implementation SettingsViewController

- (void)configureUI{
    
    [super configureUI];
    
    self.userOptionsArray = [[NSMutableArray alloc] initWithObjects:
                             
                             @{
                               @"optionName": [LABEL_MANAGER setLocalizeLabelText:@"diplomas_title"],
                               @"optionImage": @"diplomas-option",
                               
                               },
                             
                             @{
                               @"optionName": [LABEL_MANAGER setLocalizeLabelText:@"settings_title"],
                               @"optionImage": @"settings-option",
                               
                               }/*,
                                 @{
                                 @"optionName": [LABEL_MANAGER setLocalizeLabelText:@"progress_title"],
                                 @"optionImage": @"progress-option",
                                 
                                 },
                                 
                                 @{
                                 @"optionName": [LABEL_MANAGER setLocalizeLabelText:@"challenges_title"],
                                 @"optionImage": @"challenges-option",
                                 
                                 },
                                 @{
                                 @"optionName": [LABEL_MANAGER setLocalizeLabelText:@"vocab_title"],
                                 @"optionImage": @"vocab-option",
                                 
                                 }*/, nil];
    
    self.navigationController.navigationBarHidden = YES;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"UserSettingsTableViewCell" bundle:nil] forCellReuseIdentifier:@"CellUserSettings"];
    
    /*_userProfileImageView.layer.cornerRadius = 4.0;
    _userProfileImageView.layer.borderWidth = 1.0;
    _userProfileImageView.layer.borderColor = [UIColor blackColor].CGColor;
    _userProfileImageView.layer.masksToBounds = YES;*/
    
    _userProfileImageView.image = [USER_MANAGER takeCurrentUserImage];
    
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
    
    self.userScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
    self.userVipPassesLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];
    
    
    self.imageViewUserRank.image = [UIImage imageNamed:[NSString stringWithFormat:@"Awards_%02d", [[USER_MANAGER takeMaxRank] intValue] + 1]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Orientations

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


#pragma mark - UITableView DataSource & Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.userOptionsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellUserSettings"];
    
    id option = [self.userOptionsArray objectAtIndex:indexPath.row];
    [cell.optionNameLabel setText:[option objectForKey:@"optionName"]];
    
    [cell.backgroundImageView setImage:[UIImage imageNamed:[option objectForKey:@"optionImage"]]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 105.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        SettingsDiplomasViewController *newController = [SettingsDiplomasViewController new];
        [self.navigationController pushViewController:newController animated:YES];
 
    }
    else if(indexPath.row == 1)
    {
        SettingsStatusViewController *newController = [SettingsStatusViewController new];
        [self.navigationController pushViewController:newController animated:YES];
        
    }
}


#pragma mark - Action methods

- (IBAction)homeButtonTapped:(id)sender
{
    [APP_DELEGATE buildHomePageStack];
}
- (IBAction)vipButtonTapped:(id)sender
{    
    id vipCourseObject = [DATA_MANAGER getVipCourseObject];
    if(vipCourseObject){
        //donwload resoureces
        NSArray *dataArray = [DATA_MANAGER getAllVipCategory];
        
        [CACHE_MANAGER setDelegate:self];
        [CACHE_MANAGER startDownloadingAndCachingResources:dataArray];
    }else{
        [self addBaseActivityIndicator];
        [self chekInternet];
        [REQUEST_MANAGER getDataFor:kRequestGetVipDataAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
            [self removeActivityIndicator];
            NSDictionary *responseDictionary = [responseObject objectForKey:kApiObjectResponse];
            [DATA_MANAGER makeVipCategoryCourseData:responseDictionary];
            [DATA_MANAGER saveData];
            [self vipButtonTapped:0];
        } andWithErrorCallBack:^(NSString *errorMessage) {
            [self removeActivityIndicator];
        }];
        //makeVipCategoryCourseData
    }
}




#pragma mark - Cache Manager Delegate
- (void)cacheManagerFinishedDownloading:(id)sender {
    
    NSLog(@"========= Images were downloaded =========");
    
    [APP_DELEGATE buildVipZoneStack];
    
}

@end
