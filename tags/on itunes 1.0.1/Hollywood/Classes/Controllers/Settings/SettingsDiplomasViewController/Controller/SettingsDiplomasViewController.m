//
//  SettingsDiplomasViewController.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/25/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "SettingsDiplomasViewController.h"
#import "SettingsDiplomasCollectionViewCell.h"
#import "RankObject+CoreDataClass.h"
#import <GPUImage/GPUImageSaturationFilter.h>
#import <GPUImage/GPUImagePicture.h>
#import "LabelManager.h"
#import "ParameterManager.h"

@interface SettingsDiplomasViewController ()<CacheManagerDelegate>
{
    __strong ProgressUserObject *currentProgressUserObject;
    __weak IBOutlet UIView *loadingView;
    NSArray *rangsArray;
}

@end

@implementation SettingsDiplomasViewController

- (void)configureUI {
    [super configureUI];
    [self.navigationController.navigationBar setHidden:YES];
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    [_collectionView registerNib:[UINib nibWithNibName:@"SettingsDiplomasCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"SettingsDiplomasCollectionViewCell"];
    self.userScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];    
    self.userVipScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];
    self.imageViewUserRank.image = [UIImage imageNamed:[NSString stringWithFormat:@"Awards_%02d", [[USER_MANAGER takeMaxRank] intValue] + 1]];
}

- (void)loadData{
    [super loadData];
    rangsArray = [DATA_MANAGER takeAllRankObjects];
    [self.collectionView reloadData];
}


#pragma mark - Action methods

- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)vipButtonTapped:(id)sender
{
    id vipCourseObject = [DATA_MANAGER getVipCourseObject];
    if(vipCourseObject){
        //donwload resoureces
        NSArray *dataArray = [DATA_MANAGER getAllVipCategory];
        
        [CACHE_MANAGER setDelegate:self];
        [CACHE_MANAGER startDownloadingAndCachingResources:dataArray];
    }else{
        [self addBaseActivityIndicator];
        [self chekInternet];
        [REQUEST_MANAGER getDataFor:kRequestGetVipDataAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
            [self removeActivityIndicator];
            NSDictionary *responseDictionary = [responseObject objectForKey:kApiObjectResponse];
            [DATA_MANAGER makeVipCategoryCourseData:responseDictionary];
            [DATA_MANAGER saveData];
            [self vipButtonTapped:0];
        } andWithErrorCallBack:^(NSString *errorMessage) {
            [self removeActivityIndicator];
        }];
        //makeVipCategoryCourseData
    }
}

#pragma mark Orientations
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskPortrait;
    
}


#pragma mark - UICollectionView DataSource & Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [rangsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsDiplomasCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SettingsDiplomasCollectionViewCell" forIndexPath:indexPath];
    RankObject *tempRankObject = [rangsArray objectAtIndex:indexPath.row];
    cell.rankLabel.text = tempRankObject.rank_name;
    
    [cell getImageFromURL:[NSURL URLWithString:tempRankObject.rank_diploma_design] withCompletionHandler:^(UIImage *image, NSError *error) {
        if(error == nil)
        {
            if([cell checkMaxRankForRow: indexPath.row])
            {
                cell.diplomaDesignRankImageView.image = image;
            }
            else
            {
                cell.diplomaDesignRankImageView.image = [cell makeSaturationForImage:image];
            }
        }
    }];
    
    if([cell checkMaxRankForRow: indexPath.row])
    {
        [cell.circleRankImageView setImage:[UIImage imageNamed:@"ellipse5"]];
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(80, 180);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}


#pragma mark - Cache Manager Delegate
- (void)cacheManagerFinishedDownloading:(id)sender {
    
    NSLog(@"========= Images were downloaded =========");
    
    [APP_DELEGATE buildVipZoneStack];
    
}

@end
