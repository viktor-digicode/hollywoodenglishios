//
//  VipQuizzesViewController.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 9/29/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "VipQuizzesViewController.h"
#import "GetReadyView.h"
#import "ConversionViewController.h"
#import "VipQuizObject+CoreDataClass.h"


@interface VipQuizzesViewController ()<GetReadyViewDelegate> {
    __weak IBOutlet UIView *topBarView;
    __weak IBOutlet UIView *questionBarView;
    __weak IBOutlet UILabel *questionBarLabel;
    __weak IBOutlet UILabel *vipScoreLabel;
    __weak IBOutlet UILabel *vipStaticTextLabel;

    __weak IBOutlet UILabel *titleLabel;
    
    NSArray *dataArray;
    __weak IBOutlet UICollectionView *mainCollectionView;
    VipCategoryObject *currentVipCategoryObject;
    
    __strong ProgressUserObject *currentProgressUserObject;
    NSInteger selectedIndexRow;
}

@end

@implementation VipQuizzesViewController

- (void)configureUI{
    [super configureUI];
    self.navigationController.navigationBarHidden = YES;
    titleLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"vip_zone_title"];
    questionBarLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"quiz_zone_info"];//vip_zone_info
    vipStaticTextLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"vip_bar_label"];
    topBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar_bg_up"]];
    questionBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar_bg_down"]];
    
    [mainCollectionView registerNib:[UINib nibWithNibName:@"VipQuizzesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
    titleLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:30.0];
    vipScoreLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:15.0];
    vipStaticTextLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:18.0];

    self.imageViewUserRank.image = [UIImage imageNamed:[NSString stringWithFormat:@"Awards_%02d", [[USER_MANAGER takeMaxRank] intValue]+1]];
    
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    [self updateLabels];
    
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    
    NSSet *set2 = currentVipCategoryObject.quizList;
    NSArray *data2Array = [set2 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"orderInCategory"
                                                  ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [data2Array sortedArrayUsingDescriptors:sortDescriptors2];
    
    NSArray *quizesWithingTimeSpan = [self checkQuizes:sorted2Array];
    
    dataArray = quizesWithingTimeSpan;
    [mainCollectionView reloadData];
}

- (void)loadData {
    [super loadData];
    //NSArray *collectionPaths = [mainCollectionView indexPathsForVisibleItems];
    
    /*currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    
    NSSet *set2 = currentVipCategoryObject.quizList;
    NSArray *data2Array = [set2 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"orderInCategory"
                                                  ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [data2Array sortedArrayUsingDescriptors:sortDescriptors2];
    
    NSArray *quizesWithingTimeSpan = [self checkQuizes:sorted2Array];
    
    dataArray = quizesWithingTimeSpan;
    [mainCollectionView reloadData];*/
}


- (NSArray *)checkQuizes:(NSArray *)sourceArray {
    
    NSMutableArray *arrayQuizes = [sourceArray mutableCopy];
    
    NSMutableArray *discardedItems = [NSMutableArray array];
    
    for (VipQuizObject *quiz in arrayQuizes) {
        
        //time span compare
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
        
        NSDate *startDate = [dateFormatter dateFromString:quiz.avStartDate];
        NSDate *endDate = [dateFormatter dateFromString:quiz.avEndDate];
        
        if (![self isQuizInTimeSpan:startDate and:endDate]) {
            
            [discardedItems addObject:quiz];
        }
    }
    
    [arrayQuizes removeObjectsInArray:discardedItems];
    
    return arrayQuizes;
}

#pragma mark - Compare is quiz in time span
- (BOOL)isQuizInTimeSpan:(NSDate *)startDate and:(NSDate *)endDate {
    
    if (!startDate || !endDate) {
        return YES;
    }
    
    NSDate *currentDate = [NSDate date];
    
    if ([currentDate isAfter:startDate] && [currentDate isBefore:endDate]) {
        return YES;
    }
    
    return NO;
}

- (void)setVipCategoryObject:(VipCategoryObject*)tempVipCategoryObject{
    currentVipCategoryObject = tempVipCategoryObject;
}

#pragma mark Interface functions

- (IBAction)goToHomePage:(id)sender {
    [AUDIO_MANAGER stopPlaying];
    [APP_DELEGATE buildHomePageStack];
}

- (IBAction)openUserZone:(id)sender {
    
    [APP_DELEGATE buildUserZoneStack];
}

#pragma mark UICollectionView
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    if(DEVICE_BIGER_SIDE > 568.0){
        if([dataArray count] == 5){
            return CGSizeMake(-10, 0);
        }else{
            return CGSizeMake(35, 0);
        }
    }
    return CGSizeMake(35, 0);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [dataArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    VipQuizzesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    VipQuizObject *tempOBj = [dataArray objectAtIndex:indexPath.row];
    [cell setVipQuizData:tempOBj];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(DEVICE_BIGER_SIDE > 568.0){
        return CGSizeMake(119, 200);
    }
    return CGSizeMake(101, 170);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    // To Do: Make a logic to compare if the quiz is available
    // Check quiz price
    // Reduce the vip points
    // Change the status of quiz as bought...
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kVipQuizzesScreen action:@"Quizzes_Screen" additionalData:nil];

    selectedIndexRow = indexPath.row;
    
    VipQuizObject *tempOBj = (VipQuizObject *)[dataArray objectAtIndex:indexPath.row];
    
    NSInteger quizPrice = [tempOBj.price integerValue];
    BOOL isQuizPromo = [[tempOBj isPromo] boolValue];

    if ((quizPrice>0 && currentProgressUserObject.vipPasses-quizPrice<0) && !isQuizPromo && !tempOBj.purchased) {
        
        [ANALYTICS_MANAGER sendQuizSelectedEvent:selectedIndexRow withActionName:kQuizNSuffActive];
        
        //Go to buy vip passes
        
        [ALERT_MANAGER showCustomAlertWithTitle:nil andMessage:[LABEL_MANAGER setLocalizeLabelText:@"insufficient_vip_passes"]];
        
        return;
    }
    
    NSSet *set2 = tempOBj.instanceList;
    NSArray *data2Array = [set2 allObjects];
    if([data2Array count] == 0){
        NSDictionary *dictHeaders = @{
                                      kUserQuizId : tempOBj.quizId
                                      };
        [self addBaseActivityIndicator];
        [self chekInternet];
        [REQUEST_MANAGER getDataFor:kRequestGetQuizDataAPICall headers:dictHeaders withSuccessCallBack:^(NSDictionary *responseObject) {
            [self removeActivityIndicator];
            NSDictionary *responseDictionary = [responseObject objectForKey:kApiObjectResponse];
            [tempOBj setInstanceData:responseDictionary];
            [DATA_MANAGER saveData];
            id vipCourseObject = [DATA_MANAGER getVipCourseObject];
            if(vipCourseObject){
                //donwload resoureces
                NSArray *vipCategoryArray = [DATA_MANAGER getAllVipCategory];
                [CACHE_MANAGER setDelegate:self];
                [CACHE_MANAGER startDownloadingAndCachingResources:vipCategoryArray];
            }
            
        } andWithErrorCallBack:^(NSString *errorMessage) {
            
        }];
    }else{
        [self removeActivityIndicator];
        [self selectVipQuiz:tempOBj];
    }
 
}

-(void)selectVipQuiz:(VipQuizObject *)tempOBj{

    
    BOOL isQuizPromo = [[tempOBj isPromo] boolValue];
    NSInteger quizPrice = [[tempOBj price] integerValue];
    
    if (!tempOBj.availability) {
        return;
    }
    
    if (isQuizPromo || currentProgressUserObject.vipPasses-quizPrice>=0 || tempOBj.purchased) {
        
        if (!tempOBj.purchased) {
            currentProgressUserObject.vipPasses -= quizPrice;
            tempOBj.purchased = YES;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSMutableArray *arrayPurchasedQuizIds = [[defaults objectForKey:kUserDataQuizList] mutableCopy];
            [arrayPurchasedQuizIds addObject:tempOBj.quizId];
            NSArray *temp = [NSArray arrayWithArray:arrayPurchasedQuizIds];
            [defaults setObject:temp forKey:kUserDataQuizList];
            [defaults synchronize];
            
            [DATA_MANAGER saveData];
            [self updateLabels];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateVipPasses" object:nil];

        }

        //Tracking event logic
        
        if (isQuizPromo) {
            [ANALYTICS_MANAGER sendQuizSelectedEvent:selectedIndexRow withActionName:kQuizFree];

        }
        else if (quizPrice == -1) {
            
            [ANALYTICS_MANAGER sendQuizSelectedEvent:selectedIndexRow withActionName:kQuizAlreadyPurchased];

        }
        else {
            [ANALYTICS_MANAGER sendQuizSelectedEvent:selectedIndexRow withActionName:kQuizPurchase];

        }


        GetReadyView *getReadyView = [GetReadyView loadFromNib];
        getReadyView.delegate = self;
        
        getReadyView.frame = CGRectMake(mainCollectionView.x, mainCollectionView.y, mainCollectionView.width, mainCollectionView.height);
        [self.view addSubview:getReadyView];
        
        questionBarLabel.text = @"";
    }
    else {
        
        DLog(@"IS GUEST ---------->>> %d", [USER_MANAGER isGuest]);
        
        if ((quizPrice>0 && currentProgressUserObject.vipPasses-quizPrice<0) && [USER_MANAGER isGuest]) {
            
            [ANALYTICS_MANAGER sendQuizSelectedEvent:selectedIndexRow withActionName:kQuizNSuffGuest];
            
            ConversionViewController *conversionScreen = [[ConversionViewController alloc] initWithNibName:@"ConversionViewController" bundle:nil];
            [self.navigationController pushViewController:conversionScreen animated:YES];
        }
        else {
            [ANALYTICS_MANAGER sendQuizSelectedEvent:selectedIndexRow withActionName:kQuizNSuffActive];
        
        
            //Go to buy vip passes
            
            UIAlertController *alert=   [UIAlertController
                                         alertControllerWithTitle:@"Hollywod English"
                                         message:@"Not sufficient VIP passes"
                                         preferredStyle:UIAlertControllerStyleAlert];
                            
            [self presentViewController:alert animated:YES completion:nil];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [alert dismissViewControllerAnimated:YES completion:^{
                    // do something ?
                }];
                
            });
        }
    }
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    //return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    // Add inset to the collection view if there are not enough cells to fill the width.
    CGFloat cellSpacing = ((UICollectionViewFlowLayout *) collectionViewLayout).minimumLineSpacing;
    CGSize headerSize = ((UICollectionViewFlowLayout *) collectionViewLayout).headerReferenceSize;
    CGFloat headerSpacing = headerSize.width;
    CGFloat cellWidth = ((UICollectionViewFlowLayout *) collectionViewLayout).itemSize.width;
    NSInteger cellCount = [collectionView numberOfItemsInSection:section];
    CGFloat inset = (collectionView.bounds.size.width - (headerSpacing*2) - (cellCount * (cellWidth + cellSpacing))) * 0.5;
    inset = MAX(inset, 0.0);
    return UIEdgeInsetsMake(0.0, inset, 0.0, 0.0);
}

- (void)updateLabels{
    vipScoreLabel.text = [NSString stringWithFormat:@"%d",currentProgressUserObject.vipPasses];
}

- (IBAction)goBackToCategories:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - GetReadyView delegate

- (void)gotoNewScreen:(id)sender {
    
    VipQuizViewController *viewController = [VipQuizViewController new];
    viewController.currentVipCategoryObject = currentVipCategoryObject;
    VipQuizObject *tempOBj = [dataArray objectAtIndex:selectedIndexRow];
    [viewController setVipQuizObject:tempOBj];
    [self.navigationController pushViewController:viewController animated:NO];

}

#pragma mark - Cache Manager Delegate
- (void)cacheManagerFinishedDownloading:(id)sender {
    
    NSLog(@"========= Images were downloaded =========");
    VipQuizObject *tempOBj = (VipQuizObject *)[dataArray objectAtIndex:selectedIndexRow];
    [self selectVipQuiz:tempOBj];
}
@end
