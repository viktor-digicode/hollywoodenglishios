//
//  VipCategoriCollectionViewCell.m
//  Hollywood
//
//  Created by Kiril Kiroski on 9/29/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "VipCategoriCollectionViewCell.h"

@implementation VipCategoriCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setVipCategoryData:(VipCategoryObject*)categoryData
{
//    self.cellImageView.image = [UIImage imageNamed:categoryData.imageName];
        
    self.cellImageView.image = [CACHE_MANAGER getImageFromCache:categoryData.imageUrl];
    
    self.titleLabel.text = categoryData.categoryName;
    /*self.titleLabel.numberOfLines = 0;
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.titleLabel fontSizeToFit];*/
    [self.comingSoonLabel setHidden:([categoryData.availability boolValue])];
    [self.comingSoonImageView setHidden:([categoryData.availability boolValue])];
    
    self.comingSoonLabel.text =  [LABEL_MANAGER setLocalizeLabelText:@"coming_soon"];
}

@end
