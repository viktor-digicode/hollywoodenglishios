//
//  JSONManager.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 9/22/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#define JSON_MANAGER ((JSONManager *)[JSONManager sharedInstance])

@interface JSONManager : NSObject

+ (id)sharedInstance;

- (void)getLocalJSONFile:(NSString *)fileName;

@end
