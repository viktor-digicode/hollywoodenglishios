//
//  AppDelegate.h
//  TestAsyncImages
//
//  Created by Dimitar Shopovski on 9/29/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import "ImageCacheObject.h"

@implementation ImageCacheObject

@synthesize size;
@synthesize timeStamp;
@synthesize image;

- (id)initWithSize:(NSUInteger)sz Image:(UIImage *)anImage{
    
    if (self = [super init]) {
        size = sz;
        timeStamp = [NSDate date];
        image = anImage;
    }
    return self;
}

- (void)resetTimeStamp {

    timeStamp = [NSDate date];
}



@end
