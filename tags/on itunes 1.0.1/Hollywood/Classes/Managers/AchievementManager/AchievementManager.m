//
//  AchievementManager.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 8/29/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "AchievementManager.h"

@implementation AchievementManager

#ifdef ACHIEVEMENT_SINGLETON



SINGLETON_GCD(AchievementManager)
#endif


- (void)saveData
{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
            NSLog(@"You successfully saved your context.");
        } else if (error) {
            NSLog(@"Error saving context: %@", error.description);
        }
    }];
}

//- (void)initializeStartData:(NSDictionary*)dictionary
//{
//    NSDictionary *vocabDictonary = [dictionary valueForKey: kApiObjectVocabs];
//
//
//    for (NSDictionary *dict in vocabDictonary)
//    {
//        VocabsObject *importObject = [VocabsObject MR_createEntity];
////        [importObject setDataWithKey: dict];
//
//
////        [importObject setDataWithKey: dict.description andValue: [parameterValueDictionary valueForKey:dict.description]];
//
//    }
//
//    [self saveData];
//}

- (void)deleteAchievementObjects
{
    NSArray *dataArray = [[AchievementsObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    
    for(int i = 0; i < [dataArray count]; i++)
    {
        AchievementsObject *achievementObject = [dataArray objectAtIndex: i];
        [achievementObject MR_deleteEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    }
    
    [self saveData];
}

- (AchievementsObject *)getAchievementById:(NSNumber *)achievementID
{
    for(AchievementsObject *achievement in [self getArrayAchievementManager])
    {
        if([[achievement achievement_id] isEqualToNumber: achievementID])
        {
            return achievement;
        }
    }
    return nil;
}

- (NSArray *)getArrayAchievementManager
{
    return [[AchievementsObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
}

- (void)storeAchievementID:(NSNumber *)achievementID
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *array = [defaults objectForKey: kAchievementsArray];
    if(array == nil)
    {
        array = [[NSMutableArray alloc] init];
        [array addObject:achievementID];
        [defaults setObject:array forKey: kAchievementsArray];
    }
    else
    {
        NSMutableArray *newArray = [[NSMutableArray alloc] init];
        for (int i = 0; i < array.count; i++)
        {
            [newArray addObject:array[i]];
        }
        [newArray addObject:achievementID];
        [defaults setObject:newArray forKey: kAchievementsArray];
    }
}

- (NSArray *)getAchievementsListedInSummary
{
    NSArray *array = [[NSArray alloc] init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    array = [defaults objectForKey: kAchievementsArray];
    
    return array;
}

#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
