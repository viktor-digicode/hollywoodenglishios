//
//  AchievementManager.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 8/29/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ACHIEVEMENT_MANAGER ((AchievementManager *)[AchievementManager sharedInstance])

@interface AchievementManager : NSObject

- (AchievementsObject *)getAchievementById:(NSNumber *)achievementID;
- (NSArray *)getArrayAchievementManager;
- (void)deleteAchievementObjects;

- (void)storeAchievementID:(NSNumber *)achievementID;
- (NSArray *)getAchievementsListedInSummary;

@end


#ifdef ACHIEVEMENT_SINGLETON

@interface AchievementManager ()
+ (id)sharedInstance;
@end

#define ACHIEVEMENT_MANAGER ((AchievementManager *)[AchievementManager sharedInstance])
#endif


