//  LMUser.m
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//


#import "LMUser.h"
#import "LessonObject+CoreDataClass.h"

@implementation LMUser

- (id)initWithContentsOfFile:(NSString *)filePath {
	self = [self init];
	if (self) {
		// process
	}
	return self;
}

- (id)initWithMSISDN:(NSString *)msisdn {
	self = [self init];
	if (self) {
		self.msisdn = msisdn;
		NSInteger prefixLength = 2; //[[USER_MANAGER defaultPrefix] length];
		self.number = [msisdn substringFromIndex:prefixLength];
		self.areaCode = [self.number substringToIndex:2];
		self.name = @"";
    self.currentLevel = 0;
		//self.countryCode = [COUNTRY_MANAGER countryCodeFromMSISDN:msisdn];
		self.status = ([self.number isEqualToString:kUserManagerDefault]) ? KAUserGuest : KAUserStatusNotRegistered;
	}
	return self;
}

- (id)initNativeUser:(NSString *)msisdn {
  self = [self init];
  if (self) {
    self.msisdn = msisdn;
    self.number = @"";
    self.areaCode = @"";
    self.name = @"";
    self.countryCode = @"";
    self.currentLevel = 0;
    self.status = KAUserStatusNotRegistered;
    self.isNative = 1;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"makeNativeUser"];
    [[NSUserDefaults standardUserDefaults] synchronize];
  }
  return self;
}

- (void)setUserData:(NSDictionary*)tempData{
    if([self objectOrNilForKey: @"customerPaymentMethod" fromDictionary: tempData]){
        self.customerPaymentMethod = [self objectOrNilForKey: @"customerPaymentMethod" fromDictionary: tempData];
    }
    if([self objectOrNilForKey: @"email" fromDictionary: tempData]){
        self.email = [self objectOrNilForKey: @"email" fromDictionary: tempData];
    }
    if([self objectOrNilForKey: @"surName" fromDictionary: tempData]){
        self.surName = [self objectOrNilForKey: @"surName" fromDictionary: tempData];
    }
    if([self objectOrNilForKey: @"status" fromDictionary: tempData]){
        self.statusString = [self objectOrNilForKey: @"status" fromDictionary: tempData];
    }
    if([self objectOrNilForKey: @"maxOrderLevel" fromDictionary: tempData]){
        self.maxOrderLevel = [self objectOrNilForKey: @"maxOrderLevel" fromDictionary: tempData];
    }
    /*if([self objectOrNilForKey: @"maxOrderRank" fromDictionary: tempData]){
        self.maxOrderRank = [self objectOrNilForKey: @"maxOrderRank" fromDictionary: tempData];
    }*/
    if([self objectOrNilForKey: @"maxOrderedLesson" fromDictionary: tempData]){
        self.maxOrderedLesson = [self objectOrNilForKey: @"maxOrderedLesson" fromDictionary: tempData];
    }
    if([self objectOrNilForKey: @"userCredits" fromDictionary: tempData]){
        self.userCredits = [self objectOrNilForKey: @"userCredits" fromDictionary: tempData];
    }
    if([self objectOrNilForKey: @"vipPasses" fromDictionary: tempData]){
        self.vipPasses = [self objectOrNilForKey: @"vipPasses" fromDictionary: tempData];
    }
    if([self objectOrNilForKey: @"score" fromDictionary: tempData]){
        self.userScore = [self objectOrNilForKey: @"score" fromDictionary: tempData];
    }
    
     if([self objectOrNilForKey: @"isLastLessonCompleted" fromDictionary: tempData]){
         if(  [[self objectOrNilForKey: @"isLastLessonCompleted" fromDictionary: tempData] boolValue]){
             self.maxOrderedLesson = [NSNumber numberWithInt:[self.maxOrderedLesson intValue]+1];
         }
     }
    
    
    if([self.statusString isEqualToString:@"active"]){
        self.status = KAUserStatusActive;
    }else  if([self.statusString isEqualToString:@"cancelled"]){
        self.status = KAUserStatusCanceled;
    }else  if([self.statusString isEqualToString:@"disabled"]){
        self.status = KAUserStatusDisabled;
    }/*else{
        self.status = KAUserStatusDisabled;
    }*/
    LessonObject *currentLessonObject = [DATA_MANAGER takeLessonObjectObject:[self.maxOrderedLesson intValue]];
    self.maxOrderRank = currentLessonObject.rankParentNumber;
}

- (void)synchronizeToFile {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
	if ([self.number isEqualToString:@""] || [self.number isEqualToString:kUserManagerDefault]) {
		// guest
		// NSString *path = [[NSString documentsDir] stringByAppendingPathComponent:[NSString stringWithFormat:@"guest/%@.plist", self.msisdn]];
		/*[[NSUserDefaults standardUserDefaults] removeObjectForKey:kLastValidMSISDNPath];
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:kLastValidMSISDN];
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:kLastValidUniqueID];
     [[NSUserDefaults standardUserDefaults] setObject:@NO forKey:kLastValidLoginUser];
     [[NSUserDefaults standardUserDefaults] synchronize];*/
	}
	else {
		// user
		NSString *path = [[NSString documentsDir] stringByAppendingPathComponent:[NSString stringWithFormat:@"user/%@.plist", self.msisdn]];
		[userDefaults setObject:path forKey:kLastValidMSISDNPath];
		[userDefaults setObject:self.msisdn forKey:kLastValidMSISDN];
		[userDefaults setInteger:self.status forKey:kLastValidUserStatus];
		[userDefaults setObject:self.billingDate forKey:kLastValidBillingDate];
		[userDefaults setObject:@YES forKey:kLastValidLoginUser];
		[userDefaults synchronize];
	}
}
- (void)synchronizeNativeUserToFile {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *path = [[NSString documentsDir] stringByAppendingPathComponent:[NSString stringWithFormat:@"user/%@.plist", self.msisdn]];
    
    [userDefaults setObject:path forKey:kLastValidMSISDNPath];
    [userDefaults setObject:self.msisdn forKey:kLastValidMSISDN];
    [userDefaults setInteger:self.status forKey:kLastValidUserStatus];
    [userDefaults setObject:self.billingDate forKey:kLastValidBillingDate];
    //[userDefaults setObject:@YES forKey:kLastValidLoginUser];
    [userDefaults setObject:@NO forKey:kLastValidLoginUser];
    [userDefaults synchronize];
  
}

- (void)saveStatusData {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if(self.levelData){
        [userDefaults setObject:self.levelData forKey:kLastValidLevelData];
    }
    if(self.currentLevel){
        [userDefaults setObject:self.currentLevel forKey:kLastValidCurrentLevel];
    }
    if(self.contentVersion>0){
        [userDefaults setObject:self.contentVersion forKey:kLastValidContentVersion];
    }
    [userDefaults synchronize];
}

- (void)loadStatusData {
    self.levelData = [[NSUserDefaults standardUserDefaults] objectForKey:kLastValidLevelData];
    self.currentLevel = [[NSUserDefaults standardUserDefaults] objectForKey:kLastValidCurrentLevel];
    self.contentVersion = [[NSUserDefaults standardUserDefaults] objectForKey:kLastValidContentVersion];
}

#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}
@end
