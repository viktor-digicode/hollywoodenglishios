//  LMUserManager.h
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//


#import "LMUser.h"
//#import "PurchaseManager.h"
#import <StoreKit/StoreKit.h>


@interface LMUserManager : NSObject


typedef void (^ManagerResultBlock)(NSError *error);
typedef void (^UserStatusResultBlock)(NSNumber *userStatus);
typedef void (^IsLoginResultBlock)(BOOL isUserLogin);


@property (nonatomic) BOOL isUserRegister;

- (BOOL)processAutologin;
- (BOOL)loginWithMSISDN:(NSString *)msisdn code:(NSString *)code;
- (void)loginWithMSISDN:(NSString *)msisdn code:(NSString *)code WithCompletitionBlock:(IsLoginResultBlock)completitionBlock;

- (void)makeLoginTokenWithMSISDN:(NSString *)msisdn WithCompletitionBlock:(UserStatusResultBlock)completitionBlock;

- (void)loginDummyUser;

- (BOOL)takeLoginTokenWithMSISDN:(NSString *)msisdn;
- (NSNumber *)showMeTokenForMSISDN:(NSString *)msisdn;
- (void)registerNativeUserValuesWithCompletitionBlock:(ManagerResultBlock)completitionBlock;
- (void)loadUserValuesWithCompletitionBlock:(ManagerResultBlock)completitionBlock;
- (void)loadNativeUserValuesWithCompletitionBlock:(ManagerResultBlock)completitionBlock;
- (void)buyPurchases;
- (void)logOutUser;


- (void)makeBillingForNativeUserWithCompletitionBlock:(ManagerResultBlock)completitionBlock;

- (NSString *)isNextBillingDateDay;
- (BOOL)isAuthenticated;
- (BOOL)isGuest;
- (BOOL)isActive;
- (BOOL)isInitialized;
- (BOOL)isNative;
- (BOOL)chekUserStatus:(int)currentUserStatus;

- (BOOL)canOpenLesson;
- (void)UserStatus;
- (KAUserStatus)getUserStatus;
- (NSString *)userMsisdn;
- (NSString *)userPhone;
- (NSString *)getUserApplicationId;
- (void)setUserApplicationId:(NSString *)userAppId;

- (void)initializeWithGuest;
- (void)initializeWithUser;
- (void)initializeWithUser:(LMUser *)user;
- (void)initializeWithNativeUser;
- (void)makeToBENativeUser;

- (void)setUserData:(NSDictionary*)tempData;
- (void)setUserMSISD:(NSString*)msisdn;

- (NSString *)statusDescription:(KAUserStatus)status;
- (NSString *)loginConfigPath:(LMUser *)user;
- (NSString *)defaultPrefix;
- (NSString *)defaultMSISDN;
- (NSString *)userMsisdnString;

- (NSInteger)numberOfCredits;
- (void)setNumberOfCredits:(NSInteger)tempCredits;

- (void)takeCredit;

- (NSString *)deviceIdentifier;
- (NSString *)takeStringFromDate;
- (NSString *)takeNativeDateStringFromDate;
- (NSString*)nextBillingDateString;
- (NSString *)takeNativeDateStringForSettings;

-(void)setLastLoginTime;
-(BOOL)loginBefore3Days;
-(NSInteger)userWeeklyUsage;

-(NSArray *)takeLevelData;
-(NSNumber *)takeCurrentLevel;
-(NSNumber *)takeMaxLevel;
-(NSNumber *)takeMaxRank;
-(NSString *)takeContentVersion;
-(UIImage*)takeCurrentUserImage;//NEW H

@property (nonatomic, strong) NSString *userOperatorPrefix;
-(void)setOperatorPrefix:(NSString *)prefix;

@property (nonatomic, assign) NSInteger userOperatorSiteID;
-(void)setOperatorSiteID:(NSInteger)siteID;

@property (nonatomic, assign) NSInteger userOperatorProductID;
-(void)setOperatorProductID:(NSInteger)productID;

@property (nonatomic, strong) NSString *stringOpertorSplashIos;
- (void)saveOpertorSplash:(NSString *)stringSplashIos;

@property (nonatomic, strong) NSString *stringOpertorLogoIos;
- (void)saveOperatorLogoIos:(NSString *)stringLogoIos;

- (NSInteger)getUserSiteID;
- (NSInteger)getUserProductID;

- (NSString *)getUserSplashIos;
- (NSString *)getUserLogoIos;
- (void)initProductIdentifier:(NSString *)productIdentifier;

@property (nonatomic, strong) NSString *operatorName;
@property (nonatomic, strong) NSString *productIdentifier;
- (NSString *)getPurchasedPackage;
- (void)setValidationForPackageOption:(NSString *)packageOption andDate:(NSDate *)onDatePurchased;
- (NSString *)getPackagePurchasedDescription;
- (NSDate *)getDateExpirationForActiveUser;
@end


#ifdef USER_SINGLETON

@interface LMUserManager ()
+ (id)sharedInstance;
@end

#define USER_MANAGER ((LMUserManager *)[LMUserManager sharedInstance])
#endif
