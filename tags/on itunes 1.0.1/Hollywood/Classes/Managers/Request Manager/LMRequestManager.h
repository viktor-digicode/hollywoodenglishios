//
//  LMRequestManager.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/21/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFURLSessionManager.h>
#import <AFNetworking/AFHTTPSessionManager.h>

typedef enum {
    kRequestLoginUserAPICall,
    kRequestSignUpAPICall,
    kRequestFirstLaunchAPICall,
    kRequestGetLanguagesAPICall,
    kRequestGetVipDataAPICall,
    kRequestGetQuizDataAPICall,
    kRequestIsMsisdnExistAPICall,
    kRequestSaveUserProgressionAPICall,
    kRequestGetUserDataAPICall,
    kRequestCreateUserAPICall,
    kRequestGetLessonDataAPICall,
    kRequestCheckCourseVersionAPICall,
    kRequestGetParametersForApplicationAPICall,
    kRequestGetLablesForCourseAPICall,
    kRequestGetRSSAPICall,
    kCreateGuestUser
} ELMRequestType;

@interface LMRequestManager : NSObject

@property (nonatomic, strong) NSString *groupID;
@property (nonatomic, strong) NSString *contentID;
@property (nonatomic, strong) NSString *courseID;
@property (nonatomic, strong) NSString *courseMode;
@property (nonatomic, strong) NSString *applicationID;

- (NSMutableURLRequest *)post:(ELMRequestType)type headers:(NSDictionary *)headers;
- (void)getDataFor:(ELMRequestType)type
           headers:(NSDictionary *)headers
withSuccessCallBack:(void (^)(NSDictionary *responseObject)) successCallback
andWithErrorCallBack:(void (^)(NSString *errorMessage)) errorCallback;


@end

@interface LMRequestManager ()
+ (id)sharedInstance;
@end

#define REQUEST_MANAGER ((LMRequestManager *)[LMRequestManager sharedInstance])
