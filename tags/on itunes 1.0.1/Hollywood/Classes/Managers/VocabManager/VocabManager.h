//
//  VocabManager.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 8/29/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>

#define VOCAB_MANAGER ((VocabManager *)[VocabManager sharedInstance])

@interface VocabManager : NSObject

//
//- (void)saveData;
//- (void)deleteParameterObjects;
//- (int)getParameterAttempts;
//- (void)initializeStartData:(NSDictionary*)dictionary;
//- (NSString *)getLocalizeParameterText:(NSString *)text;
- (VocabsObject *)getVocabById:(NSNumber *)vocabID;
- (NSArray *)getArrayVocabManager;
- (void)deleteVocabObjects;
- (VocabsObject *)getVocabForString:(NSString *)vocabText;

@end


#ifdef VOCABMANAGER_SINGLETON

@interface VocabManager ()
+ (id)sharedInstance;
@end

#define VOCAB_MANAGER ((VocabManager *)[VocabManager sharedInstance])
#endif


