//
//  VocabLabelView.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 9/7/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomDataLabel.h"

@interface VocabLabelView : UIView {
    
    BOOL isTranslationShown;
    BOOL isFirstRow;
    UILabel *currentSelectedVocabLabel;
    CustomDataLabel *labelOriginalWord;

}

@property (strong, nonatomic) UIView *translationView;
@property (strong, nonatomic) UILabel *translationWordLabel;

@property (strong, nonatomic) UIImageView *imgBallon;
@property (strong, nonatomic) NSNumber *fontSize;

@property (strong, nonatomic) UIButton *buttonSound;
@property (strong, nonatomic) WordsObject *currentVocabObject;

@property (nonatomic) NSTimer *vocabTimer;

- (instancetype)initWithFrame:(CGRect)frame withData:(id)data;
- (void)cleanControl;
- (void)buildTextViewFromString:(NSString *)localizedString;
- (void)buildTextViewFromString:(NSString *)localizedString withFont:(NSNumber *)fontSize;

- (void)createUX;

@end
