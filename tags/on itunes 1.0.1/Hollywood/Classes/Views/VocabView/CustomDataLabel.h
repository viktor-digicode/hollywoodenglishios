//
//  CustomDataLabel.h
//  VocabTest
//
//  Created by Dimitar Shopovski on 7/19/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomDataLabel : UILabel

@property (nonatomic, assign) BOOL isLink;
@property (nonatomic, strong) id data;

@end
