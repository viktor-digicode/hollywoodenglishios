//
//  VocabLabelView.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 9/7/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VocabLabelView.h"
#import "WordsObject+CoreDataClass.h"

@implementation VocabLabelView
@synthesize imgBallon;

- (instancetype)initWithFrame:(CGRect)frame withData:(id)data {
    
    if (self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[UIColor clearColor]];
        NSString *textString = [data objectForKey:@"word"];
        [self buildTextViewFromString:textString];
        
        [self createUX];
    }
    
    return self;
}

- (void)buildTextViewFromString:(NSString *)localizedString withFont:(NSNumber *)fontSize {
    
    self.fontSize = fontSize;
    [self buildTextViewFromString:localizedString];
    
}

- (void)buildTextViewFromString:(NSString *)localizedString {
    
    CGFloat fontSizeValue = [self.fontSize floatValue];
    
    if (!fontSizeValue) {
        fontSizeValue = 26.0;
    }
    
    labelOriginalWord = [[CustomDataLabel alloc] init];
    labelOriginalWord.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSizeValue];
    labelOriginalWord.text = localizedString;
    labelOriginalWord.userInteractionEnabled = YES;

    SEL selectorAction = @selector(tapOnVocabWord:);
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:selectorAction];
    [labelOriginalWord addGestureRecognizer:tapGesture];

    
    NSString *originalString = localizedString;
    
    labelOriginalWord.textColor = [UIColor whiteColor];
    labelOriginalWord.highlightedTextColor = [UIColor yellowColor];
    
    
    labelOriginalWord.text = originalString;
    
    originalString = [originalString stringByReplacingOccurrencesOfString:@"[" withString:@""];
    originalString = [originalString stringByReplacingOccurrencesOfString:@"]" withString:@""];
    if([originalString containsString:@"."])
    {
        originalString = [originalString stringByReplacingOccurrencesOfString:@"." withString:@""];
    }
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:originalString];

    NSString *vocabOriginalText = originalString;
    if ([originalString length] > 0) {
        vocabOriginalText = [originalString substringWithRange:NSMakeRange(0, [originalString length])];
        [attributedString addAttributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                          NSForegroundColorAttributeName: APP_VOCAB_COLOR,
                                          NSFontAttributeName: [UIFont italicSystemFontOfSize:fontSizeValue]
                                          } range:NSMakeRange(0, [originalString length])];

    }
    else {
        
        [attributedString addAttributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                          NSForegroundColorAttributeName: APP_VOCAB_COLOR,
                                          NSFontAttributeName: [UIFont italicSystemFontOfSize:fontSizeValue]
                                          } range:NSMakeRange(0, originalString.length)];

    }
    
    
    [labelOriginalWord setAttributedText:attributedString];
    
    VocabsObject *vocab = [VOCAB_MANAGER getVocabForString:vocabOriginalText];
    NSSet *translateOptions = vocab.nativeVocabs;
    if (translateOptions) {
        
        WordsObject *wordObject = (WordsObject *)[[translateOptions allObjects] objectAtIndex:0];
        [labelOriginalWord setData:wordObject];
        self.currentVocabObject = wordObject;
        
    }
    
    [labelOriginalWord sizeToFit];

    // Show this label:
    [self addSubview:labelOriginalWord];
    
    self.frame = CGRectMake(self.x, self.y, labelOriginalWord.width, self.height);
}

- (void)tapOnVocabWord:(UITapGestureRecognizer *)tapGesture
{
    CustomDataLabel *selectedLabel = (CustomDataLabel *)tapGesture.view;
    
    if (tapGesture.state == UIGestureRecognizerStateEnded)
    {
        if (selectedLabel == currentSelectedVocabLabel && isTranslationShown) {
            
            if (_vocabTimer != nil) {
                
                [_vocabTimer invalidate];
                _vocabTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(stopTheTimer:) userInfo:nil repeats:NO];
                
            }
        }
        else {
            
            CGFloat xPos = tapGesture.view.center.x - self.translationView.frame.size.width/2;
            CGFloat yPos = tapGesture.view.frame.origin.y + tapGesture.view.frame.size.height;
            
            CGFloat widthTranslation = 10.0;
            CGFloat offset = 0.0;

            self.translationView.hidden = NO;
            
            _translationWordLabel.frame = CGRectMake(_translationWordLabel.x, _translationWordLabel.y, 200, _translationWordLabel.height);
            _translationWordLabel.text = [selectedLabel.data word];
            [_translationWordLabel sizeToFit];
            
            widthTranslation += _translationWordLabel.width;
            
            if (![selectedLabel.data wordAudio] || [[selectedLabel.data wordAudio] isEqualToString:@""]) {
                
                _buttonSound.hidden = YES;
                imgBallon.frame = CGRectMake(0, 0, _translationWordLabel.width + 28, 36);
            }
            else {
                
                widthTranslation += 28 + 5;
                _buttonSound.hidden = NO;
                imgBallon.frame = CGRectMake(0, 0, _translationWordLabel.width + 28 + 24, 36);
            }
            
            if (widthTranslation > self.frame.size.width) {
                
                offset = (widthTranslation-self.frame.size.width)/2;
            }
            
//            if (xPos-30 < 0) {
//                
//                self.translationView.frame = CGRectMake(-30, yPos, _translationWordLabel.width + 29 + 4 + 10, 36);
//            }
//            else {
//                
//                self.translationView.frame = CGRectMake(xPos-30, yPos, _translationWordLabel.width + 29 + 4 + 10, 36);
//            }
            
            
            if (offset != 0.0) {
                
                self.frame = CGRectMake(self.x - offset, self.y, self.width + 2*offset, self.translationView.height + _translationWordLabel.height);
                self.translationView.frame = CGRectMake(0, yPos, _translationWordLabel.width + 29 + 4 + 10, 36);
                labelOriginalWord.frame = CGRectMake(offset, labelOriginalWord.y, labelOriginalWord.width, labelOriginalWord.height);

            }
            
            _buttonSound.frame = CGRectMake(_translationView.width-22, 10, 20, 20);
            
            //new code label alignment
            _translationWordLabel.frame = CGRectMake(10, _translationWordLabel.y, _translationWordLabel.width, _translationWordLabel.height);
            
            isTranslationShown = YES;
            
            if (selectedLabel != currentSelectedVocabLabel && _vocabTimer != nil) {
                [_vocabTimer invalidate];
            }
            currentSelectedVocabLabel = selectedLabel;
            
            _vocabTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(stopTheTimer:) userInfo:nil repeats:NO];
        }
        
//        [self bringSubviewToFront:self.translationView];
    }
}

- (void)stopTheTimer:(id)sender {
    
    self.translationView.hidden = YES;
    isTranslationShown = NO;
    
}

#pragma mark - createUX

- (void)createUX {
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.translationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 110, 36)];
    self.translationView.hidden = YES;
    
    //image
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(6, 6, 6, 12);
    UIImage *backgroundButtonImage = [[UIImage imageNamed:@"ballon-img-v2"] resizableImageWithCapInsets:edgeInsets];
    
    
    //image
    imgBallon = [[UIImageView alloc] initWithImage:backgroundButtonImage];
    imgBallon.frame = CGRectMake(0, 0, 72, 36);
    [imgBallon setContentMode:UIViewContentModeScaleToFill];
    [self.translationView addSubview:imgBallon];
    
    _translationWordLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, 72-32, 18)];
    _translationWordLabel.text = @"translation";
    [_translationWordLabel setTextColor:APP_VOCAB_TRANSLATION_COLOR];
    [_translationWordLabel setFont:[UIFont systemFontOfSize:20.0]];
    [self.translationView addSubview:_translationWordLabel];
    
    _buttonSound = [UIButton buttonWithType:UIButtonTypeCustom];
    _buttonSound.frame = CGRectMake(self.translationView.frame.size.width-37, 10, 20, 20);
    [_buttonSound setImage:[UIImage imageNamed:@"sound-mct"] forState:UIControlStateNormal];
    [_buttonSound addTarget:self action:@selector(playVocabSound:) forControlEvents:UIControlEventTouchUpInside];
    [self.translationView addSubview:_buttonSound];
    
    [self addSubview:self.translationView];
}

#pragma mark - Remove subviews

- (void)cleanControl {
    
    for (UIView *vv in self.subviews) {
        [vv removeFromSuperview];
    }
}

#pragma mark - Helper

- (NSString *)findTranslateWord:(NSString *)sourceText betweenTag:(NSString *)tag {
    
    NSString *s = sourceText;
    
    NSRange r1 = [s rangeOfString:[NSString stringWithFormat:@"<%@>", tag]];
    NSRange r2 = [s rangeOfString:[NSString stringWithFormat:@"</%@>", tag]];
    NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
    NSString *sub = [s substringWithRange:rSub];
    
    return sub;
}

#pragma mark - Set the word in favourites

- (IBAction)addToFavourites:(id)sender {
    
    if (_vocabTimer != nil && isTranslationShown) {
        
        [_vocabTimer invalidate];
        _vocabTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(stopTheTimer:) userInfo:nil repeats:NO];
        
    }
    
    UIButton *buttonFav = (UIButton *)sender;
    
    NSData *data1 = UIImagePNGRepresentation(buttonFav.currentImage);
    NSData *data2 = UIImagePNGRepresentation([UIImage imageNamed:@"star-normal"]);
    
    if (![data1 isEqual:data2]) {
        
        [buttonFav setImage:[UIImage imageNamed:@"star-normal"] forState:UIControlStateNormal];
        
    }
    else {
        [buttonFav setImage:[UIImage imageNamed:@"star-selected"] forState:UIControlStateNormal];
        
    }
    
}

- (IBAction)playVocabSound:(id)sender {
    
    if (self.currentVocabObject) {
        
        [AUDIO_MANAGER playRemoteAudioFile:[NSURL URLWithString:[self.currentVocabObject wordAudio]]];
        NSLog(@"Sound playing - %@", [self.currentVocabObject wordAudio]);
        
    }
}

@end
