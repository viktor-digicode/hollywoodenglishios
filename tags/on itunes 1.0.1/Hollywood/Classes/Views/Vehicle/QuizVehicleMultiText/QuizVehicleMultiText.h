//
//  QuizVehicleMultiText.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 2/2/17.
//  Copyright © 2017 aa. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "BaseViewVehicle.h"
#import "ClickTextAnswer.h"

@interface QuizVehicleMultiText : BaseViewVehicle<ClickTextAnswerDelegate>

@property (nonatomic, strong) QuizInstance *dataMultiChoiceTextInstance;
@property (nonatomic, assign) BOOL isOrdered;
@property (nonatomic, assign) NSInteger positionInQuiz;

@property (nonatomic, strong) UIView *viewAnswerContent;

@property (nonatomic, strong) UIView *viewQuestionContent;
@property (nonatomic, strong) UIImageView *imageViewQuestion;

@property (nonatomic, assign) ClickTextAnswer *currentAnswer;
@property (assign, nonatomic) NSString *videoUrlString;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data position:(NSInteger)position;

@property (nonatomic, assign) BOOL isAnsweredCorrect;

@end
