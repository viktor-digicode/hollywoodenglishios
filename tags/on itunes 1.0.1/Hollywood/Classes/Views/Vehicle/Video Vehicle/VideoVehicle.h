//
//  VideoVehicle.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/24/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "BaseViewVehicle.h"
#import "KSVideoPlayerView.h"

@interface VideoVehicle : BaseViewVehicle<AVAudioPlayerDelegate>

@property (strong, nonatomic) UIView *nibView;
@property (strong, nonatomic) IBOutlet UIView *sView;
@property (assign, nonatomic) BOOL goNextAfterVideoFinish;
@property (strong, nonatomic) NSString *videoUrlString;
@property (nonatomic, assign) BOOL isFreeVideo;
@property (nonatomic, assign) NSInteger freeVideoLength;

-(void)zoomVideoPlayer;

- (instancetype)initWithFrame:(CGRect)frame withData:(id)vehicleData;
- (instancetype)initWithFrame:(CGRect)frame withData:(id)vehicleData isHighQuality:(BOOL)highQuality;

- (void)setDataForVehicle:(BaseVehiclesObject *)tempVehiclesObject isHighQuality:(BOOL)highQuality;

@end
