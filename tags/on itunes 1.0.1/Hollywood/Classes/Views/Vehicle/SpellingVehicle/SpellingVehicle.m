//
//  SpellingVehicle.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 4/8/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "SpellingVehicle.h"

@implementation SpellingVehicle
@synthesize vehicleData, viewAnswerContainer, viewQuestionContainer;
@synthesize scrollKeyboardAvoidingContainer;
@synthesize currentTextField;
@synthesize imageViewQuestion;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data {
    
    if (self = [super initWithFrame:frame]) {
        
        self.nAttemptsLeft = 3;
        totalPoints = kParam001;
        
        vehicleData = data;
        self.afterAudioString = [data objectForKey:@"afterAudio"];
        
        currentTag = 1;
        
        numberOfLettersToFill = 0;
        
        [self createAvoidingKeyboardContainer];
        [self createQuestionView];
        [self createAnswerContainer];
        [self createWordPart:[data objectForKey:@"answer"]];
        [self selectFirstUnasweredTextField];
        
        //        [self createComplexWord:@"A#CT*O#R*"];
        
    }
    
    return self;
}

- (void)setDataForVehicle:(BaseVehiclesObject*)tempVehiclesObject {
    
    [super setDataForVehicle:tempVehiclesObject];
 
    self.nAttemptsLeft = 3;
    totalPoints = kParam001;
    
    self.dataVehicleObject = (VehicleObjectSpelling *)tempVehiclesObject;
    self.afterAudioString = [self.dataVehicleObject questionAudioId];

    currentTag = 1;
    
    numberOfLettersToFill = 0;
    
    [self createAvoidingKeyboardContainer];
    [self createQuestionView];
    [self createAnswerContainer];
    
    
    [self createWordPart:[self.dataVehicleObject sentence]];
    [self selectFirstUnasweredTextField];


}

- (void)createAvoidingKeyboardContainer {
    
    if (scrollKeyboardAvoidingContainer == nil) {
        
        scrollKeyboardAvoidingContainer = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, LM_WIDTH, LM_HEIGHT)];
        [scrollKeyboardAvoidingContainer setBackgroundColor:[UIColor clearColor]];
        [self addSubview:scrollKeyboardAvoidingContainer];
    }
}

- (void)createQuestionView {
    
//    float containerHeight = DEVICE_SMALL_SIDE-64-84;
//    float containerWidth = (DEVICE_BIGER_SIDE-2*INSTANCE_START_POSITION_X - INSTANCE_START_POSITION_X/2)/2;
    
//    UIImage *originalImage = [UIImage imageNamed:[self.dataVehicleObject image]];
//    UIImage *newImage = [UIImage imageWithImage:originalImage scaledToHeight:containerHeight];
//    
//    CGFloat contentQuestionWidth = newImage.size.width;
//    CGFloat contentQuestionHeight = newImage.size.height;

    float containerWidth = (DEVICE_BIGER_SIDE-2*INSTANCE_START_POSITION_X - INSTANCE_START_POSITION_X/2)/2;
    float containerHeight = DEVICE_SMALL_SIDE-64-64-30;

    if (!viewQuestionContainer) {
        
        viewQuestionContainer = [[UIView alloc] initWithFrame:CGRectMake(INSTANCE_START_POSITION_X, 20, containerWidth, containerHeight)];
        [viewQuestionContainer setBackgroundColor:[UIColor blackColor]];
        [scrollKeyboardAvoidingContainer addSubview:viewQuestionContainer];
        
        if (!imageViewQuestion) {
            
            imageViewQuestion = [[UIImageView alloc] initWithFrame:CGRectMake(0, 3, containerWidth, containerHeight-6)];
            [imageViewQuestion setImageWithURL:[NSURL URLWithString:[self.dataVehicleObject image]] placeholderImage:nil];

            [viewQuestionContainer addSubview:imageViewQuestion];
            imageViewQuestion.contentMode = UIViewContentModeScaleAspectFit;
            
        }
        
        viewQuestionContainer.layer.cornerRadius = 4.0;
        viewQuestionContainer.layer.borderWidth = 1.0;
        viewQuestionContainer.layer.borderColor = [UIColor whiteColor].CGColor;
        viewQuestionContainer.layer.masksToBounds = YES;
        
    }
}

- (void)createAnswerContainer {
    
    float containerWidth = (DEVICE_BIGER_SIDE-2*INSTANCE_START_POSITION_X - INSTANCE_START_POSITION_X/2)/2;
    float containerHeight = DEVICE_SMALL_SIDE-64-64-30;
    
    
    //    UIImage *originalImage = [UIImage imageNamed:[self.vehicleData objectForKey:@"image"]];
    //    UIImage *newImage = [UIImage imageWithImage:originalImage scaledToHeight:containerHeight];
    
    //    CGFloat contentQuestionWidth = newImage.size.width;
    //    CGFloat answerContainerWidth = (DEVICE_BIGER_SIDE-3*INSTANCE_START_POSITION_X - contentQuestionWidth);
    
    if (!viewAnswerContainer) {
        
        viewAnswerContainer = [[UIView alloc] initWithFrame:CGRectMake(INSTANCE_START_POSITION_X + containerWidth + INSTANCE_START_POSITION_X/2, 0, containerWidth, containerHeight)];
        [viewAnswerContainer setBackgroundColor:[UIColor clearColor]];
        viewAnswerContainer.hidden = NO;
        [scrollKeyboardAvoidingContainer addSubview:viewAnswerContainer];
    }
}

- (void)createWordPart:(NSString *)word {
    
    NSLog(@"The word is - %@", word);
    xPosition = viewAnswerContainer.x+2;
    yPosition = 20.0;
    
    NSMutableArray *arrayTemporaryWords = [NSMutableArray new];
    
    NSString *currentWord = word;
    
    NSRange r1;
    NSRange r2;
    do {
        
        r1 = [currentWord rangeOfString:@"{"];
        r2 = [currentWord rangeOfString:@"}"];
        
        if (r1.location != NSNotFound && r2.location != NSNotFound) {
            
            NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
            NSString *sub = [currentWord substringWithRange:rSub];
            
            //            NSLog(@"Sub - %@", sub);
            
            if (r1.location >= 0) {
                
                NSString *word1 = [currentWord substringToIndex:r1.location];
                //                NSLog(@"Word pre - %@", word1);
                
                if (word1.length>0 && ![word1 isEqualToString:@" "]) {
//                    [arrayTemporaryWords addObject:@{@"word": [word1 stringByReplacingOccurrencesOfString:@" " withString:@""], @"type": @"label"}];
                    [arrayTemporaryWords addObject:@{@"word": [word1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]],
                                                     @"type": @"label"}];
                    
                }
                if (sub.length>0) {
                    
                    sub = [sub stringByReplacingOccurrencesOfString:@" " withString:@""];
                    [arrayTemporaryWords addObject:@{@"word": sub, @"type": @"textFields"}];
                    
                }
                
            }
            
            currentWord = [currentWord substringFromIndex:r2.location+1];
            
        }
        
        
    } while (r1.location != NSNotFound && r2.location != NSNotFound);
    
    [arrayTemporaryWords addObject:@{@"word": currentWord, @"type": @"label"}];
    
    float spaceBetweenLines = 5.0;
    float spaceBetweenWords = 10.0;
    
    UILabel *label;
    
    for (id element in arrayTemporaryWords) {
        
        if ([[element objectForKey:@"type"] isEqualToString:@"label"]) {
            
            label = [[UILabel alloc] initWithFrame:CGRectMake(xPosition, yPosition-2, 500, 21)];
            [label setFont:[UIFont systemFontOfSize:20.0]];
            label.numberOfLines = 0;
            label.text = [element objectForKey:@"word"];
            label.textColor = [UIColor whiteColor];
            [label sizeToFit];
            [scrollKeyboardAvoidingContainer addSubview:label];
            
            if (xPosition+label.frame.size.width > viewAnswerContainer.x+viewAnswerContainer.frame.size.width-5.0) {
                
                yPosition+=25.0;
                xPosition = viewAnswerContainer.x;
                
                label.frame = CGRectMake(xPosition, yPosition, label.frame.size.width, label.frame.size.height);
                
            }
            
            if (xPosition != viewAnswerContainer.x) {
                
                label.frame = CGRectMake(xPosition+6.0, yPosition, label.frame.size.width, label.frame.size.height);
                
            }
            
            NSLog(@"Labela xPosition - %f : width - %f", xPosition, label.width);
            
            xPosition+=label.frame.size.width + spaceBetweenWords;
            
            NSLog(@"Labela xPosition after - %f", xPosition);
        }
        else {
            
            float letterWidth = 34.0;
            float letterHeight = 21.0;
            float letterBgWhiteSpace = 10.0;
            
            float spaceBetweenLetters = letterWidth - letterBgWhiteSpace;
            
            
            NSString *wordTextField = [element objectForKey:@"word"];
            
            if (xPosition+(wordTextField.length*letterWidth - letterBgWhiteSpace) > viewAnswerContainer.x+viewAnswerContainer.frame.size.width-5.0) {
                
                yPosition+=letterHeight+spaceBetweenLines;
                xPosition = viewAnswerContainer.x+2;
            }
            
            for (int i=0; i<wordTextField.length; i++) {
                
                NSString *substring = [wordTextField substringWithRange:NSMakeRange(i, 1)];
                SpellingTextField *textFieldLetter = [[SpellingTextField alloc] initWithFrame:CGRectMake(xPosition, yPosition, letterWidth, letterHeight) expectedLetter:substring];
                [textFieldLetter setFont:[UIFont systemFontOfSize:20.0]];
                [textFieldLetter setBorderStyle:UITextBorderStyleNone];
                textFieldLetter.autocorrectionType = UITextAutocorrectionTypeNo;
                //                [textFieldLetter setBackground:[UIImage imageNamed:@"tf-normal"]];
                
                NSLog(@"TextField xPosition - %f : width - %f", xPosition, textFieldLetter.width);
                
                textFieldLetter.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tf-normal"]];
                
                textFieldLetter.textColor = [UIColor whiteColor];
                textFieldLetter.textAlignment = NSTextAlignmentCenter;
                textFieldLetter.delegate = self;
                if (currentTag != 1) {
                    textFieldLetter.userInteractionEnabled = NO;
                }
                else {
                    //                    [textFieldLetter setBackground:[UIImage imageNamed:@"tf-active"]];
                    textFieldLetter.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tf-active"]];
                    
                }
                textFieldLetter.tag = currentTag++;
                textFieldLetter.autocapitalizationType = UITextAutocapitalizationTypeNone;
                [scrollKeyboardAvoidingContainer addSubview:textFieldLetter];
                
                if (xPosition+textFieldLetter.frame.size.width > viewAnswerContainer.x+viewAnswerContainer.frame.size.width-5.0) {
                    
                    yPosition+=letterHeight+spaceBetweenLines;
                    xPosition = viewAnswerContainer.x+2;
                    
                    textFieldLetter.frame = CGRectMake(xPosition, yPosition, textFieldLetter.frame.size.width, textFieldLetter.frame.size.height);
                    
                }
                
                xPosition+=spaceBetweenLetters;
                NSLog(@"TextField xPosition after - %f", xPosition);
                
                numberOfLettersToFill++;
            }
            
            xPosition+=spaceBetweenWords;
            NSLog(@"TextField xPosition kraj zbor - %f", xPosition);
            
        }
    }
}

#pragma mark - Select first unaswered

- (void)selectFirstUnasweredTextField {
    
    
}

- (void)createComplexWord:(NSString *)word {
    
    
    NSMutableArray *arrayTemporaryWords = [NSMutableArray new];
    
    NSString *currentWord = word;
    
    NSRange r1;
    NSRange r2;
    do {
        
        r1 = [currentWord rangeOfString:@"#"];
        r2 = [currentWord rangeOfString:@"*"];
        
        if (r1.location != NSNotFound && r2.location != NSNotFound) {
            
            NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
            NSString *sub = [currentWord substringWithRange:rSub];
            
            //            NSLog(@"Sub - %@", sub);
            
            if (r1.location >= 0) {
                
                NSString *word1 = [currentWord substringToIndex:r1.location];
                //                NSLog(@"Word pre - %@", word1);
                
                if (word1.length>0) {
                    [arrayTemporaryWords addObject:@{@"word": word1, @"type": @"label"}];
                    
                }
                if (sub.length>0) {
                    [arrayTemporaryWords addObject:@{@"word": sub, @"type": @"textFields"}];
                    
                }
                
            }
            
            currentWord = [currentWord substringFromIndex:r2.location+1];
            
        }
        
        
    } while (r1.location != NSNotFound && r2.location != NSNotFound);
    
    [arrayTemporaryWords addObject:@{@"word": currentWord, @"type": @"label"}];
    
    for (id element in arrayTemporaryWords) {
        
        if ([[element objectForKey:@"type"] isEqualToString:@"label"]) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(xPosition, yPosition, 500, 21)];
            label.numberOfLines = 0;
            label.text = [element objectForKey:@"word"];
            label.textColor = [UIColor whiteColor];
            [label sizeToFit];
            [scrollKeyboardAvoidingContainer addSubview:label];
            
            if (xPosition+label.frame.size.width > viewAnswerContainer.x+viewAnswerContainer.frame.size.width-5.0) {
                
                yPosition+=25.0;
                xPosition = viewAnswerContainer.x;
                
                label.frame = CGRectMake(xPosition, yPosition, label.frame.size.width, label.frame.size.height);
                
            }
            
            xPosition+=label.frame.size.width+5;
        }
        else {
            
            NSString *wordTextField = [element objectForKey:@"word"];
            
            if (xPosition+wordTextField.length*32 > viewAnswerContainer.x+viewAnswerContainer.frame.size.width-5.0) {
                
                yPosition+=25;
                xPosition = viewAnswerContainer.x+2;
            }
            
            for (int i=0; i<wordTextField.length; i++) {
                
                NSString *substring = [wordTextField substringWithRange:NSMakeRange(i, 1)];
                SpellingTextField *textFieldLetter = [[SpellingTextField alloc] initWithFrame:CGRectMake(xPosition, yPosition, 30, 21) expectedLetter:substring];
                [textFieldLetter setBorderStyle:UITextBorderStyleNone];
                textFieldLetter.autocorrectionType = UITextAutocorrectionTypeNo;
                [textFieldLetter setBackground:[UIImage imageNamed:@"tf-normal"]];
                textFieldLetter.textColor = [UIColor whiteColor];
                textFieldLetter.textAlignment = NSTextAlignmentCenter;
                textFieldLetter.delegate = self;
                if (currentTag != 1) {
                    textFieldLetter.userInteractionEnabled = NO;
                }
                textFieldLetter.tag = currentTag++;
                textFieldLetter.autocapitalizationType = UITextAutocapitalizationTypeNone;
                [scrollKeyboardAvoidingContainer  addSubview:textFieldLetter];
                
                if (xPosition+textFieldLetter.frame.size.width > viewAnswerContainer.x+viewAnswerContainer.frame.size.width-5.0) {
                    
                    yPosition+=25.0;
                    xPosition = viewAnswerContainer.x+2;
                    
                    textFieldLetter.frame = CGRectMake(xPosition, yPosition, textFieldLetter.frame.size.width, textFieldLetter.frame.size.height);
                    
                }
                
                xPosition+=30+2;
                numberOfLettersToFill++;
            }
            
            //        xPosition+=5;
        }
    }
    
}


#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (self.isCompleted) {
        return NO;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    if (newLength>1) {
        return NO;
    }
    
    if (newLength <= 1) {
        
        //NSInteger tag = textField.tag;
        NSString *expectedLetter;
        
        if ([textField isKindOfClass:[SpellingTextField class]]) {
            
            currentTextField = (SpellingTextField *)textField;
            
            expectedLetter = [(SpellingTextField *)textField stringExpectedLetter];
            
            if ([[string lowercaseString] isEqualToString:[expectedLetter lowercaseString]]) {
                
                NSLog(@"Correct");
                
                textField.text = expectedLetter;
                
                //            [textField setBackground:[UIImage imageNamed:@"tf-correct"]];
                
                textField.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tf-correct"]];
                
                //            totalPoints+=kParam002;
                [AUDIO_MANAGER playRightSound];
                
                if (textField.tag+1 == numberOfLettersToFill+1) {
                    textField.text = expectedLetter;
                    [self.vehicleDelegate  vehiclePass:YES];
                    [ANALYTICS_MANAGER sendAnalyticsForEvent:kSpellingScreen action:@"SPL_End" labelName:@"Correctness" value:@(1)];
                    self.vehiclePlayStatus = 2;
                }
                [self goToNextTextField:@(textField.tag+1)];
                return NO;
            }
            else {
                [self.vehicleDelegate  vehiclePass:NO];
                //self.vehiclePlayStatus = 1;
                NSLog(@"INNNNNCorrect");
                
                textField.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tf-wrong"]];
                
                [AUDIO_MANAGER playWrongSound];
                
                self.nAttemptsLeft--;
                if (self.nAttemptsLeft == 0) {
                    
                    self.isCompleted = YES;
                    [self setCorrectFeedbackOnQuestionView:2];
                    [self showIncorrectFeedback];
                    [ANALYTICS_MANAGER sendAnalyticsForEvent:kSpellingScreen action:@"SPL_End" labelName:@"Correctness" value:@(0)];
                    
                    [textField resignFirstResponder];
                    
                }
                else {
                    incorectPoint += kParam002;
                    totalPoints-=kParam002;
                }
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    
                    [self deleteLetter:textField];
                });
                
                if (self.isCompleted) {
                    
                    [self showCorrectFeedback];
                }
            }
        }
        return YES;
    }
    
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    textField.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tf-active"]];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
    }];
    return [super canPerformAction:action withSender:sender];
}

- (void)deleteLetter:(id)sender {
    
    SpellingTextField *tf = (SpellingTextField *)sender;
    [tf setText:@""];
    tf.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tf-active"]];
}

- (void)goToNextTextField:(id)sender {
    
    NSInteger newTag = [sender integerValue];
    //    SpellingTextField *nextTextField = (SpellingTextField *)[scrollKeyboardAvoidingContainer viewWithTag:newTag];
    
    UIResponder *nextResponder = [scrollKeyboardAvoidingContainer viewWithTag:newTag];
    
    if (newTag == numberOfLettersToFill+1) {
        
        [self setCorrectFeedbackOnQuestionView:1];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self showCorrectFeedbackAndAddPoints:numberOfLettersToFill*kParam001 - incorectPoint];
            
        });
    }
    
    if (nextResponder!=nil) {
        [(SpellingTextField *)nextResponder setUserInteractionEnabled:YES];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            
            [nextResponder becomeFirstResponder];
        });
    }
    else {
        
        [currentTextField resignFirstResponder];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (void)setCorrectFeedbackOnQuestionView:(NSInteger)status {
    
    //0 - normal = white
    //1 - correct = greeen
    //2 - incorrect = red
    
    if (status == 0) {
        viewQuestionContainer.layer.borderColor = [UIColor whiteColor].CGColor;
        viewQuestionContainer.layer.borderWidth = 1.0;
        
        
    }
    else if (status == 1) {
        viewQuestionContainer.layer.borderColor = APP_CORRECT_COLOR.CGColor;
        viewQuestionContainer.layer.borderWidth = 2.0;
        //make all text fields with white
        [self makeAllFieldsInNormalState];
        
        
    }
    else if (status == 2) {
        viewQuestionContainer.layer.borderColor = APP_INCORRECT_COLOR.CGColor;
        viewQuestionContainer.layer.borderWidth = 2.0;
    }
    
}

- (void)makeAllFieldsInNormalState {
    
    for (SpellingTextField *textField in self.scrollKeyboardAvoidingContainer.subviews) {
        
        if ([textField isKindOfClass:[SpellingTextField class]]) {
            
            //            [textField setBackground:[UIImage imageNamed:@"tf-normal"]];
            textField.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tf-normal"]];
        }
    }
    
}

#pragma mark - Activate/deactivate vehicle

- (void)activateVehicle {
    [super activateVehicle];
    
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kSpellingScreen action:@"SPL_Start" additionalData:nil];
    [self.vehicleDelegate setQuestionBarTextWithVocab:[self.dataVehicleObject questionText]];
    if ([self.dataVehicleObject audio]) {
        [AUDIO_MANAGER playSoundFromAudioFile:[self.dataVehicleObject audio]];
    }
}

- (void)deactivateVehicle {
    
    [super deactivateVehicle];
    
    self.nAttemptsLeft = 3;
    totalPoints = kParam001;
    incorectPoint = 0;
    numberOfLettersToFill = 0;
    currentTag = 1;
    [self setCorrectFeedbackOnQuestionView:0];
    
    self.isCompleted = NO;
    
    for (SpellingTextField *textField in self.scrollKeyboardAvoidingContainer.subviews) {
        
        if ([textField isKindOfClass:[SpellingTextField class]]) {
            
            textField.text = @"";
            if (textField.tag != 1) {
                textField.userInteractionEnabled = NO;
                //                [textField setBackground:[UIImage imageNamed:@"tf-normal"]];
                textField.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tf-normal"]];
                
            }
            else {
                textField.userInteractionEnabled = YES;
                //                [textField setBackground:[UIImage imageNamed:@"tf-active"]];
                textField.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tf-active"]];
                
            }
            
            [textField resignFirstResponder];
            numberOfLettersToFill++;
        }
    }
}

- (void)finishVehicle {
    
    [super finishVehicle];
    
    if(self.afterAudioString){
        self.vehicleState = KAPlayAfterAudioState;
        [AUDIO_MANAGER playAudio:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:self.afterAudioString ofType:@""]]];
        [AUDIO_MANAGER setDelegate:self];
    }else{
        self.isCompleted = YES;
        [self.vehicleDelegate vehicleWasFinished:self];

        [AUDIO_MANAGER setDelegate:nil];
    }
}

#pragma mark - AVWrapperDelegate
- (void)playbackComplete {
    
    if(self.vehicleState == KAPlayBeforeAudioState){
        
    }else if(self.vehicleState == KAPlayAfterAudioState){
        self.vehicleState = KADefaultState;
        self.isCompleted = YES;
        [self.vehicleDelegate vehicleWasFinished:self];
        //[self.vehicleDelegate setQuestionBarText:@""];
        [AUDIO_MANAGER setDelegate:nil];
    }
}

@end
