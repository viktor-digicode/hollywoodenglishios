//
//  GetReadyView.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 10/21/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GetReadyViewDelegate <NSObject>

@required
- (void)gotoNewScreen:(id)sender;

@end

@interface GetReadyView : UIView {
    
    NSInteger currentNumber;
    NSTimer *counterTimer;
}

@property (nonatomic, weak) IBOutlet UIButton *skipButton;
@property (nonatomic, weak) IBOutlet UILabel *numberCounterLabel;
@property (nonatomic, weak) IBOutlet UIImageView *backgroundImageView;

@property (nonatomic, strong) id<GetReadyViewDelegate> delegate;

@end
