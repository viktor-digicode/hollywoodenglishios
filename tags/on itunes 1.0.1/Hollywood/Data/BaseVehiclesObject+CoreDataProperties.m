//
//  BaseVehiclesObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "BaseVehiclesObject+CoreDataProperties.h"

@implementation BaseVehiclesObject (CoreDataProperties)

+ (NSFetchRequest<BaseVehiclesObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"BaseVehiclesObject"];
}

@dynamic audio;
@dynamic free;
@dynamic image;
@dynamic instanceID;
@dynamic name;
@dynamic order;
@dynamic questionAudioId;
@dynamic questionText;
@dynamic questionType;
@dynamic showInstruction;
@dynamic title;
@dynamic type;
@dynamic unitID;

@end
