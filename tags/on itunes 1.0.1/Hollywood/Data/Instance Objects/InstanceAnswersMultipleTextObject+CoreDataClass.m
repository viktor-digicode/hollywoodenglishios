//
//  InstanceAnswersMultipleTextObject+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersMultipleTextObject+CoreDataClass.h"

@implementation InstanceAnswersMultipleTextObject

- (void) setData:(NSDictionary*)dataDictionary{
    [super setData:dataDictionary];
    //DLog(@"## %@",dataDictionary);
    
    if( [dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.afterClickImage = [self objectOrNilForKey:kInstanceObjectAfterClickImage fromDictionary:dataDictionary];
        self.afterClickText = [self objectOrNilForKey:kInstanceObjectAfterClickText fromDictionary:dataDictionary];
        self.answerClickAudio = [self objectOrNilForKey:kInstanceObjectAnswerClickAudio fromDictionary:dataDictionary];
        self.answerText = [self objectOrNilForKey:kInstanceObjectAnswerText fromDictionary:dataDictionary];
        self.correctAnswer = [NSNumber numberWithInteger:[[self objectOrNilForKey:kInstanceObjectCorrectAnswer fromDictionary:dataDictionary]integerValue]];
    }
    
}


#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
