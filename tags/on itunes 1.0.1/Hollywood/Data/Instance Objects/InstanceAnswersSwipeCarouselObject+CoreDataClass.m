//
//  InstanceAnswersSwipeCarouselObject+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersSwipeCarouselObject+CoreDataClass.h"

@implementation InstanceAnswersSwipeCarouselObject

- (void) setData:(NSDictionary*)dataDictionary{
    [super setData:dataDictionary];
    //DLog(@"## %@",dataDictionary);
    
    if( [dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.afterClickAudio = [self objectOrNilForKey:kInstanceObjectAfterClickAudio fromDictionary:dataDictionary];
        self.afterClickImage = [self objectOrNilForKey:kInstanceObjectAfterClickImage fromDictionary:dataDictionary];
        self.answerImage = [self objectOrNilForKey:kInstanceObjectAnswerImage fromDictionary:dataDictionary];
        self.carouselSegmentQuestionAudio = [self objectOrNilForKey:kInstanceObjectCarouselSegmentQuestionAudio fromDictionary:dataDictionary];
        self.carouselSegmentQuestionText = [self objectOrNilForKey:kInstanceObjectCarouselSegmentQuestionText fromDictionary:dataDictionary];
        self.carouselSegmentQuestionType = [self objectOrNilForKey:kInstanceObjectCarouselSegmentQuestionType fromDictionary:dataDictionary];
        self.free = [NSNumber numberWithInteger:[[self objectOrNilForKey:kFree fromDictionary:dataDictionary]integerValue]];
    }
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
