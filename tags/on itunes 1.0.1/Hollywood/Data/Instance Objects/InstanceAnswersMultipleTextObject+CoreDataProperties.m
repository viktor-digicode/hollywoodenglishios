//
//  InstanceAnswersMultipleTextObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersMultipleTextObject+CoreDataProperties.h"

@implementation InstanceAnswersMultipleTextObject (CoreDataProperties)

+ (NSFetchRequest<InstanceAnswersMultipleTextObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"InstanceAnswersMultipleTextObject"];
}

@dynamic afterClickImage;
@dynamic afterClickText;
@dynamic answerClickAudio;
@dynamic answerText;
@dynamic correctAnswer;

@end
