//
//  InstanceAnswersImageObject+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersImageObject+CoreDataClass.h"

@implementation InstanceAnswersImageObject

- (void) setData:(NSDictionary*)dataDictionary{
    [super setData:dataDictionary];
    //DLog(@"## %@",dataDictionary);
    
    if( [dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.defaultImage = [self objectOrNilForKey:kInstanceObjectDefaultImage fromDictionary:dataDictionary];
        self.afterAudio = [self objectOrNilForKey:kInstanceObjectAfterAudio fromDictionary:dataDictionary];
        self.sourceLanguageText = [self objectOrNilForKey:kInstanceObjectSourceLanguageText fromDictionary:dataDictionary];
        self.targetLanguageText = [self objectOrNilForKey:kInstanceObjectTargetLanguageText fromDictionary:dataDictionary];
        self.free = [NSNumber numberWithInteger:[[self objectOrNilForKey:kFree fromDictionary:dataDictionary]integerValue]];
    }
}

#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
