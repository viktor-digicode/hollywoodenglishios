//
//  InstanceAnswersWordsObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersWordsObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface InstanceAnswersWordsObject (CoreDataProperties)

+ (NSFetchRequest<InstanceAnswersWordsObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *audio;
@property (nullable, nonatomic, copy) NSNumber *free;
@property (nullable, nonatomic, copy) NSString *word;

@end

NS_ASSUME_NONNULL_END
