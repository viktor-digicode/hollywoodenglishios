//
//  VehicleObjectVoiceRecognition+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectVoiceRecognition+CoreDataProperties.h"

@implementation VehicleObjectVoiceRecognition (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectVoiceRecognition *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"VehicleObjectVoiceRecognition"];
}

@dynamic mainImageId;
@dynamic targetAudio;
@dynamic targetText;

@end
