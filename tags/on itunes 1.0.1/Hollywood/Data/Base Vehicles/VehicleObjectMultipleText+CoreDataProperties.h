//
//  VehicleObjectMultipleText+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectMultipleText+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VehicleObjectMultipleText (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectMultipleText *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSSet<InstanceAnswersMultipleTextObject *> *instanceAnswers;

@end

@interface VehicleObjectMultipleText (CoreDataGeneratedAccessors)

- (void)addInstanceAnswersObject:(InstanceAnswersMultipleTextObject *)value;
- (void)removeInstanceAnswersObject:(InstanceAnswersMultipleTextObject *)value;
- (void)addInstanceAnswers:(NSSet<InstanceAnswersMultipleTextObject *> *)values;
- (void)removeInstanceAnswers:(NSSet<InstanceAnswersMultipleTextObject *> *)values;

@end

NS_ASSUME_NONNULL_END
