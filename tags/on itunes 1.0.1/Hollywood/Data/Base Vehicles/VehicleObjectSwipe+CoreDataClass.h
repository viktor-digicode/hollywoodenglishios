//
//  VehicleObjectSwipe+CoreDataClass.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 6/30/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseVehiclesObject+CoreDataClass.h"

@class InstanceAnswersSwipeObject;

NS_ASSUME_NONNULL_BEGIN

@interface VehicleObjectSwipe : BaseVehiclesObject

- (void) setData:(NSDictionary*)dataDictionary;

@end

NS_ASSUME_NONNULL_END

#import "VehicleObjectSwipe+CoreDataProperties.h"
