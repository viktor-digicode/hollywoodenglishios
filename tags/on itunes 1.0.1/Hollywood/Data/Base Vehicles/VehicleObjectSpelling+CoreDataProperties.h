//
//  VehicleObjectSpelling+CoreDataProperties.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 6/30/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectSpelling+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VehicleObjectSpelling (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectSpelling *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *sentence;

@end

NS_ASSUME_NONNULL_END
