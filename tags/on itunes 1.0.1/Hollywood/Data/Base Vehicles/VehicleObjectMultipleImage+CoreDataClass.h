//
//  VehicleObjectMultipleImage+CoreDataClass.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseVehiclesObject+CoreDataClass.h"

@class InstanceAnswersMultipleImageObject;

NS_ASSUME_NONNULL_BEGIN

@interface VehicleObjectMultipleImage : BaseVehiclesObject

- (void) setData:(NSDictionary*)dataDictionary;

@end

NS_ASSUME_NONNULL_END

#import "VehicleObjectMultipleImage+CoreDataProperties.h"
