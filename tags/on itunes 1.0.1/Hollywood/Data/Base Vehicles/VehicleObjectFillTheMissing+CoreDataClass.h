//
//  VehicleObjectFillTheMissing+CoreDataClass.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseVehiclesObject+CoreDataClass.h"

@class InstanceAnswersWordsObject;

NS_ASSUME_NONNULL_BEGIN

@interface VehicleObjectFillTheMissing : BaseVehiclesObject

- (void) setData:(NSDictionary*)dataDictionary;

@end

NS_ASSUME_NONNULL_END

#import "VehicleObjectFillTheMissing+CoreDataProperties.h"
