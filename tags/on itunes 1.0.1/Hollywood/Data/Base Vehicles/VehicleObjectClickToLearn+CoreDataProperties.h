//
//  VehicleObjectClickToLearn+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectClickToLearn+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VehicleObjectClickToLearn (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectClickToLearn *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSSet<InstanceAnswersImageObject *> *instanceImage;

@end

@interface VehicleObjectClickToLearn (CoreDataGeneratedAccessors)

- (void)addInstanceImageObject:(InstanceAnswersImageObject *)value;
- (void)removeInstanceImageObject:(InstanceAnswersImageObject *)value;
- (void)addInstanceImage:(NSSet<InstanceAnswersImageObject *> *)values;
- (void)removeInstanceImage:(NSSet<InstanceAnswersImageObject *> *)values;

@end

NS_ASSUME_NONNULL_END
