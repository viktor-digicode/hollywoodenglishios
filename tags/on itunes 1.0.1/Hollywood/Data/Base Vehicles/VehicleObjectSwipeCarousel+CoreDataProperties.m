//
//  VehicleObjectSwipeCarousel+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 7/4/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectSwipeCarousel+CoreDataProperties.h"

@implementation VehicleObjectSwipeCarousel (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectSwipeCarousel *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"VehicleObjectSwipeCarousel"];
}

@dynamic instanceAnswers;

@end
