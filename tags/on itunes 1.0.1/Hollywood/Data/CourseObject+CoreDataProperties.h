//
//  CourseObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "CourseObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CourseObject (CoreDataProperties)

+ (NSFetchRequest<CourseObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *course_description;
@property (nullable, nonatomic, copy) NSString *course_id;
@property (nullable, nonatomic, copy) NSString *course_name;
@property (nullable, nonatomic, copy) NSString *course_source_language;
@property (nullable, nonatomic, retain) NSSet<LevelObject *> *course_levels;

@end

@interface CourseObject (CoreDataGeneratedAccessors)

- (void)addCourse_levelsObject:(LevelObject *)value;
- (void)removeCourse_levelsObject:(LevelObject *)value;
- (void)addCourse_levels:(NSSet<LevelObject *> *)values;
- (void)removeCourse_levels:(NSSet<LevelObject *> *)values;

@end

NS_ASSUME_NONNULL_END
