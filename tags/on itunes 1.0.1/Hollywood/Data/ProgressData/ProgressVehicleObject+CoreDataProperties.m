//
//  ProgressVehicleObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 8/11/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ProgressVehicleObject+CoreDataProperties.h"

@implementation ProgressVehicleObject (CoreDataProperties)

@dynamic achived;
@dynamic progres;
@dynamic vehicleOrder;

@end
