//
//  ProgressLessonObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 8/11/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ProgressLessonObject+CoreDataProperties.h"

@implementation ProgressLessonObject (CoreDataProperties)

@dynamic maxVehicle;
@dynamic currentVehicle;
@dynamic scoreForLesson;
@dynamic starsNumber;
@dynamic lessonOrder;
@dynamic vehicleProgres;

@end
