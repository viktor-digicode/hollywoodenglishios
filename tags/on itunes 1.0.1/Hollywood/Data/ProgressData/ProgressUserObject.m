//
//  ProgressUserObject.m
//  Hollywood
//
//  Created by Kiril Kiroski on 8/10/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "ProgressUserObject.h"
#import "ProgressLevelObject.h"

@implementation ProgressUserObject



- (void) setData:(NSDictionary*)dataDictionary{
    if([dataDictionary isKindOfClass:[NSDictionary class]]) {
        
        self.maxLevel =  [[self objectOrNilForKey:kProgressMaxLevel fromDictionary:dataDictionary]unsignedIntValue];
        self.currentLevel =  [[self objectOrNilForKey:kProgressCurrentLevel fromDictionary:dataDictionary]unsignedIntValue];
        self.score = [[self objectOrNilForKey:kProgressScore fromDictionary:dataDictionary]unsignedIntValue];
        self.vipPasses =  [[self objectOrNilForKey:kProgressVipPasses fromDictionary:dataDictionary]unsignedIntValue];
        self.rank =  [[self objectOrNilForKey:kProgressRank fromDictionary:dataDictionary]unsignedIntValue];
        //for (int i= 0; i < 5; i++) {
        for (int i= 0; i < 1; i++) {
            ProgressLevelObject *newObject = [ProgressLevelObject MR_createEntity];
            newObject.levelOrder = i;
            [newObject setInitialData];
            [self addLevelsProgresObject:newObject];
        }
    }
}

- (void) setInitialData{
    int startVip = (int)kParam014;
    NSDictionary *dataDictionary = @{
                                     @"maxLevel":@0,
                                     @"currentLevel":@0,
                                     @"score":@0,
                                     @"vipPasses":[NSNumber numberWithInt:startVip],
                                     @"rank":@0,
                                     };
    [self setData:dataDictionary];
    
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}
@end
