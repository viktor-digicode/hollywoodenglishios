//
//  AchievementsObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/11/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "AchievementsObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AchievementsObject (CoreDataProperties)

+ (NSFetchRequest<AchievementsObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *achievement_id;
@property (nullable, nonatomic, copy) NSString *achievementType;
@property (nullable, nonatomic, copy) NSString *image;
@property (nullable, nonatomic, copy) NSString *afterImage;
@property (nullable, nonatomic, copy) NSString *achivmentText;
@property (nullable, nonatomic, copy) NSString *teaserText;
@property (nullable, nonatomic, copy) NSNumber *award;
@property (nullable, nonatomic, copy) NSNumber *criteria;


@end

NS_ASSUME_NONNULL_END
