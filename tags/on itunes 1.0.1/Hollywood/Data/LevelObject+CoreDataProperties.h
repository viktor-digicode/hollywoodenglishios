//
//  LevelObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LevelObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface LevelObject (CoreDataProperties)

+ (NSFetchRequest<LevelObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *level_description;
@property (nullable, nonatomic, copy) NSNumber *level_id;
@property (nullable, nonatomic, copy) NSString *level_name;
@property (nullable, nonatomic, copy) NSNumber *level_order;
@property (nullable, nonatomic, retain) NSSet<RankObject *> *level_ranks;

@end

@interface LevelObject (CoreDataGeneratedAccessors)

- (void)addLevel_ranksObject:(RankObject *)value;
- (void)removeLevel_ranksObject:(RankObject *)value;
- (void)addLevel_ranks:(NSSet<RankObject *> *)values;
- (void)removeLevel_ranks:(NSSet<RankObject *> *)values;

@end

NS_ASSUME_NONNULL_END
