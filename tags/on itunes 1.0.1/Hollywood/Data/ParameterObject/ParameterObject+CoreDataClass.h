//
//  ParameterObject+CoreDataClass.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 8/21/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface ParameterObject : NSManagedObject

- (void)setDataWithKey:(NSString *)key andValue:(NSNumber *)value;

@end

NS_ASSUME_NONNULL_END

#import "ParameterObject+CoreDataProperties.h"
