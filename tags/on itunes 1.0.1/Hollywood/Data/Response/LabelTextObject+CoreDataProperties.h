//
//  LabelTextObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/5/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LabelTextObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface LabelTextObject (CoreDataProperties)

+ (NSFetchRequest<LabelTextObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *key;
@property (nullable, nonatomic, copy) NSString *value;

@end

NS_ASSUME_NONNULL_END
