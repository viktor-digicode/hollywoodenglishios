//
//  BaseViewVehicle.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 3/29/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PureLayout/PureLayout.h>
#import "BaseVehiclesObject+CoreDataClass.h"
//#import "FeedbackView.h"
//#import "CustomTitleView.h"



@protocol BaseViewVehicleDelegate <NSObject>

- (void)vehicleWasFinished:(id)sender;

@optional
- (void)showIncorrectSpellingFeedback:(id)sender;
- (void)showCorrectSpellingFeedback:(id)sender;
- (void)vehiclePass:(BOOL)correct;
- (void)vehiclesendStatus:(int)vehiclePlayStatus forID:(int)idForVehicle;

@required

- (void)addPoints:(int)points;
- (void)gotoNextPage;
- (void)gotoPreviousPage;

@optional
- (void)addActivityIndicator;
- (void)removeActivityIndicator;

- (void)enableScrolling:(id)sender;
- (void)disableScrolling:(id)sender;

- (void)enablePageControlButtons;
- (void)disablePageControlButtons;

- (void)showVideoWithUrl:(NSString *)videoUrl and:(BOOL)automaticGoNext isFree:(BOOL)isFree freeLength:(NSInteger)length;

- (void)setQuestionBarText:(NSString*)newText;
- (void)setQuestionBarTextWithVocab:(NSString *)newText;

@end

@interface BaseViewVehicle : UIView

//@property (nonatomic, strong) BaseInstance *dictVehicleInfo;
@property (nonatomic, weak) id<BaseViewVehicleDelegate> vehicleDelegate;

//positions of question, answers
@property float yPosQuestionArea;
@property float yPosAnswersArea;
@property int vehiclePlayStatus;//0-notTouch //1=wron //2right

@property (nonatomic, readwrite) BOOL isCompleted;
@property (nonatomic, assign, readwrite) BOOL isCurrent;
@property (nonatomic, readwrite) BOOL isActive;
@property (nonatomic, readwrite) KAVehicleState vehicleState;


@property (nonatomic, assign) NSInteger vehicleOrder;
@property (nonatomic, assign) NSInteger vehicleNumber;
@property (nonatomic, assign) NSInteger pageNumber;
@property (nonatomic, assign) NSInteger nAttemptsLeft;

@property (nonatomic, assign) NSString *afterAudioString;
@property (nonatomic, assign) NSString *beforeAudioString;
@property (nonatomic, assign) BaseVehiclesObject *currentVehiclesObject;

- (void)setDataForVehicle:(BaseVehiclesObject*)tempVehiclesObject;

- (void)resetTheInstance;
- (void)showLayout:(BOOL)forCompleted;

- (void)activateVehicle;
- (void)deactivateVehicle;

-(BOOL)vehicleIsAvalible;

- (void)disableVehicleInteractions;
- (void)enableVehicleInteractions;

- (void)finishVehicle;

- (void)showIncorrectFeedback;
- (void)showCorrectFeedback;
- (void)showCorrectFeedbackAndAddPoints:(int)points;
- (void)showCorrectFeedbackWithOutPoins;

- (void)andAddPoints:(int)points;

- (void)showFeedbackWithStatus:(BOOL)status forView:(UIView*)statusView;
- (void)showFeedbackWithStatus:(BOOL)status forView:(UIView*)statusView forBorderView:(UIView*)statusBorderView;

-(void)hideAllFeedbackView;
-(BOOL)haveBackgroundFeedback;

- (void)recordVehicleData;

@end
