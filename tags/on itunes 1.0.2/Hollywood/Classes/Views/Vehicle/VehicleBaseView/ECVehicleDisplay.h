//
//  ECVehicleDisplay.h
//  TransitionsTest
//
//  Created by Dimitar Shopovski on 11/13/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PureLayout/PureLayout.h>
//#import "FeedbackView.h"
//#import "CustomTitleView.h"



@protocol ECVehicleDisplayDelegate <NSObject>

@required
- (void)addPoints:(int)points;
- (void)gotoNextPage;
- (void)gotoPreviousPage;

@optional
- (void)addActivityIndicator;
- (void)removeActivityIndicator;


@optional
- (void)enableScrolling:(id)sender;
- (void)disableScrolling:(id)sender;

- (void)enablePageControlButtons;
- (void)disablePageControlButtons;


@end

@interface ECVehicleDisplay : UIScrollView

//@property (nonatomic, strong) BaseInstance *dictVehicleInfo;
@property (nonatomic, weak) id<ECVehicleDisplayDelegate> vehicleDelegate;

//positions of question, answers
@property float yPosQuestionArea;
@property float yPosAnswersArea;


@property (nonatomic, readwrite) BOOL isCompleted;
@property (nonatomic, readwrite) BOOL isCurrent;
@property (nonatomic, readwrite) BOOL isActive;

@property (nonatomic, assign) NSInteger vehicleOrder;
@property (nonatomic, assign) NSInteger vehicleNumber;
@property (nonatomic, assign) NSInteger pageNumber;


- (void)resetTheInstance;
- (void)showLayout:(BOOL)forCompleted;

- (void) activeteVehicle;
- (void) deactiveteVehicle;

- (void)disableVehicleInteractions;
- (void)enableVehicleInteractions;



@end
