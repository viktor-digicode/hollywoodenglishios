//
//  SwipeCarouselVehicle.m
//  Hollywood
//
//  Created by Kiril Kiroski on 4/1/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "SwipeCarouselVehicle.h"
#import "InstanceAnswersSwipeCarouselObject+CoreDataClass.h"

#define     CarouselHeight 169
#define     CarouselWidth 298


@implementation SwipeCarouselVehicle{
    UIPickerView *carouselPickerView;
    NSArray *arrayAnswers;
    UITapGestureRecognizer *tapGesture;
    UIView *selectView;
    UIView *newFeedbackView;
    int incorrectNumbersLeft;
}

- (instancetype)initWithFrame:(CGRect)frame data:(id)data {
    
    self.vehicleData = data;
    arrayAnswers = [data objectForKey:@"answers"];
    
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor clearColor];
        //[self setY:10];
        
        if(IS_IOS8){
            carouselPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,  216.0, [self width], [self height])];
            if(DEVICE_BIGER_SIDE < 568){
                [carouselPickerView setY:([self height]-(224*0.8))/2];
            }
        }else{
            carouselPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, -50, [self width], [self height]+100)];
        }
       /* CGAffineTransform t0 = CGAffineTransformMakeTranslation (0, carouselPickerView.bounds.size.height/2);
        CGAffineTransform s0 = CGAffineTransformMakeScale       (1.2, 1.2);
        CGAffineTransform t1 = CGAffineTransformMakeTranslation (0, -carouselPickerView.bounds.size.height/2);
        carouselPickerView.transform = CGAffineTransformConcat          (t0, CGAffineTransformConcat(s0, t1));*/
        
        carouselPickerView.backgroundColor = [UIColor clearColor];
        [carouselPickerView setDataSource: self];
        [carouselPickerView setDelegate: self];
        carouselPickerView.showsSelectionIndicator = NO;
        carouselPickerView.userInteractionEnabled = YES;
        
        [self addSubview:carouselPickerView];
        
//        NSUInteger max = INT16_MAX;
        NSUInteger max = 100;
        NSUInteger base10 = (max/2)-(max/2)%10;
        [carouselPickerView selectRow:[carouselPickerView selectedRowInComponent:0]%10+base10 inComponent:0 animated:false];
        //[carouselPickerView selectRow:55 inComponent:0 animated:NO];
        //[carouselPickerView autoCenterInSuperview];
        
        tapGesture =[[UITapGestureRecognizer alloc]  initWithTarget:self action:@selector(pickerTapped:)];
        [tapGesture setNumberOfTapsRequired:1];
        [carouselPickerView addGestureRecognizer:tapGesture];
        tapGesture.delegate = self;
        
        
        
       /* UIImageView *leftLineImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"lineForCarousel"]];
        [leftLineImage setX:[self width]/2 - 150];
        [leftLineImage setY:[self height]/2 - [leftLineImage height]/2];
        [self addSubview:leftLineImage];
        
        UIImageView *rightLineImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"lineForCarousel"]];
        [rightLineImage setX:[self width]/2 + 140];
        [rightLineImage setY:[self height]/2 - [leftLineImage height]/2];
        [self addSubview:rightLineImage];
       */
        
        UIImageView *exampleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Carousel1"]];
        [exampleImageView setHeight:CarouselHeight];
        [exampleImageView setWidth:CarouselWidth];
        
        if(DEVICE_BIGER_SIDE < 568){
            [exampleImageView setHeight: [exampleImageView height]*0.8];
            [exampleImageView setWidth: [exampleImageView width]*0.8];
        }
        // 667.0
        selectView = [[UIView alloc]initWithFrame:CGRectMake(([self width]- exampleImageView.frame.size.width)/2,
                                                             exampleImageView.frame.size.height-10+30,
                                                             exampleImageView.frame.size.width,
                                                             30)];
        if(DEVICE_BIGER_SIDE == 568){
            selectView = [[UIView alloc]initWithFrame:CGRectMake(([self width]- exampleImageView.frame.size.width)/2,
                                                                 exampleImageView.frame.size.height-10,
                                                                 exampleImageView.frame.size.width,
                                                                 30)];
        }else if(DEVICE_BIGER_SIDE <= 568){
            selectView = [[UIView alloc]initWithFrame:CGRectMake(([self width]- exampleImageView.frame.size.width)/2,
                                                                 exampleImageView.frame.size.height+7,
                                                                 exampleImageView.frame.size.width,
                                                                 30)];
        }
        
        [self addSubview:selectView];
        selectView.backgroundColor = [UIColor clearColor];
        selectView.userInteractionEnabled = NO;
        
    }
    
    return self;
}

- (void)setDataForVehicle:(BaseVehiclesObject*)tempVehiclesObject{
    [super setDataForVehicle:tempVehiclesObject];
    self.dataVehicleObject = (VehicleObjectSwipeCarousel*)tempVehiclesObject;
    
    arrayAnswers = [[NSMutableArray alloc]init];;
    NSSet *set1 = self.dataVehicleObject.instanceAnswers;
    NSArray *myArray2 = [set1 allObjects];
    arrayAnswers = myArray2 ;
   /* for(InstanceAnswersSwipeCarouselObject *swipeObject in myArray2){
        [arrayAnswers addObject:swipeObject.answerImage];
    }*/
    
    self.backgroundColor = [UIColor clearColor];
    
    if(IS_IOS8){
        carouselPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,  216.0, [self width], [self height])];
        if(DEVICE_BIGER_SIDE < 568){
            [carouselPickerView setY:([self height]-(224*0.8))/2];
        }
    }else{
        carouselPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, -50, [self width], [self height]+100)];
    }
    
    carouselPickerView.backgroundColor = [UIColor clearColor];
    [carouselPickerView setDataSource: self];
    [carouselPickerView setDelegate: self];
    carouselPickerView.showsSelectionIndicator = NO;
    carouselPickerView.userInteractionEnabled = YES;
    
    [self addSubview:carouselPickerView];
    
//    NSUInteger max = INT16_MAX;
    NSUInteger max = 100;
    NSUInteger base10 = (max/2)-(max/2)%10;
    [carouselPickerView selectRow:[carouselPickerView selectedRowInComponent:0]%10+base10 inComponent:0 animated:false];
    
    tapGesture =[[UITapGestureRecognizer alloc]  initWithTarget:self action:@selector(pickerTapped:)];
    [tapGesture setNumberOfTapsRequired:1];
    [carouselPickerView addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;
    
    
    UIImageView *exampleImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Carousel1"]];
    [exampleImageView setHeight:CarouselHeight];
    [exampleImageView setWidth:CarouselWidth];
    
    if(DEVICE_BIGER_SIDE < 568){
        [exampleImageView setHeight: [exampleImageView height]*0.8];
        [exampleImageView setWidth: [exampleImageView width]*0.8];
    }
    // 667.0
    selectView = [[UIView alloc]initWithFrame:CGRectMake(([self width]- exampleImageView.frame.size.width)/2,
                                                         exampleImageView.frame.size.height-10+30,
                                                         exampleImageView.frame.size.width,
                                                         30)];
    if(DEVICE_BIGER_SIDE == 568){
        selectView = [[UIView alloc]initWithFrame:CGRectMake(([self width]- exampleImageView.frame.size.width)/2,
                                                             exampleImageView.frame.size.height-10,
                                                             exampleImageView.frame.size.width,
                                                             30)];
    }else if(DEVICE_BIGER_SIDE <= 568){
        selectView = [[UIView alloc]initWithFrame:CGRectMake(([self width]- exampleImageView.frame.size.width)/2,
                                                             exampleImageView.frame.size.height+7,
                                                             exampleImageView.frame.size.width,
                                                             30)];
    }
    
    [self addSubview:selectView];
    selectView.backgroundColor = [UIColor clearColor];
    selectView.userInteractionEnabled = NO;
}

- (void)activateVehicle{
    [super activateVehicle];
    
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kSwipeCarouselScreen action:@"Carousel_Start" additionalData:nil];
    self.nAttemptsLeft = [arrayAnswers count];
    
    [self setQuestionText];
    /* NSUInteger max = INT16_MAX;
     NSUInteger base10 = (max/2)-(max/2)%10;
     [carouselPickerView selectRow:[carouselPickerView selectedRowInComponent:0]%10+base10 inComponent:0 animated:false];*/
    
    [self randomSwipeCarouselPickerView];
    
    carouselPickerView.userInteractionEnabled = YES;
}

- (void)selectRowInPicker:(NSNumber *)randomRow
{
    [carouselPickerView selectRow:[randomRow integerValue] inComponent:0 animated:YES];
}

#pragma mark BaseViewVehicle

- (void)finishVehicle{
    [super finishVehicle];
    self.isCompleted = YES;
    [self.vehicleDelegate vehicleWasFinished:self];
    //[self.vehicleDelegate setQuestionBarText:@""];
}

#pragma mark UIPickerView

-(void)pickerView:(UIPickerView *)pV didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSLog(@"TEST");
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
//    return INT16_MAX;
    return 100;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return @"KIRCA";
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    //row = row % 10;
    if (!view)
    {
        //NSLog(@"MakeView");
        [[carouselPickerView.subviews objectAtIndex:1] setHidden:TRUE];
        [[carouselPickerView.subviews objectAtIndex:2] setHidden:TRUE];
        
        UIImageView *flagView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Carousel1"]];
        flagView.contentMode = UIViewContentModeScaleAspectFit;
        flagView.userInteractionEnabled = NO;
        [flagView setHeight:CarouselHeight];
        [flagView setWidth:CarouselWidth];
        flagView.tag = 2;
        if(DEVICE_BIGER_SIDE < 568){
            [flagView setHeight: [flagView height]*0.8];
            [flagView setWidth: [flagView width]*0.8];
        }
        
        view = [[UIView alloc] initWithFrame:flagView.frame];
        //[view setHeight:CarouselHeight-80];
        view.backgroundColor = [UIColor blackColor];
        
        [[view layer]setMasksToBounds:TRUE];
        view.clipsToBounds = YES;
        [[view layer]setCornerRadius:2.0f];
        [[view layer]setBorderWidth:0.9f];
        [[view layer] setBorderColor:[UIColor whiteColor].CGColor];
        [view addSubview:flagView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, flagView.frame.size.height-30, flagView.frame.size.width, 30)];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.userInteractionEnabled = NO;
        label.tag = 1;
        label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0];
        //[UIFont fontWithName:@"ChalkboardSE-Regular" size:20];
        label.alpha = 0;
        
        UIView *labelView = [[UIView alloc] initWithFrame:label.frame];
        labelView.backgroundColor = [UIColor whiteColor];
        labelView.alpha = 0;//0.4;
        [view addSubview:labelView];
        [view addSubview:label];
        
        
        /*UIButton *buttonView = [UIButton buttonWithType:UIButtonTypeCustom];
         [buttonView addTarget:self
         action:@selector(pressSelect)
         forControlEvents:UIControlEventTouchUpInside];
         
         buttonView.frame = label.frame;// CGRectMake(0, label.frame.size.height-240, frame.size.width, 40);
         buttonView.backgroundColor = [UIColor redColor];
         [view addSubview:buttonView];*/
        
        /*UIButton *buttonView = [UIButton buttonWithType:UIButtonTypeRoundedRect];
         buttonView.userInteractionEnabled = YES;
         buttonView.frame = label.frame;
         [buttonView addTarget:self action:@selector(pressSelect) forControlEvents:UIControlEventTouchUpInside];
         buttonView.backgroundColor = [UIColor redColor];
         [view addSubview:buttonView];*/
        
    }
    
    InstanceAnswersSwipeCarouselObject *swipeObject = [arrayAnswers objectAtIndex:(row % ([arrayAnswers count]))];
    
    //NSDictionary *tempData = [arrayAnswers objectAtIndex:(row % ([arrayAnswers count]))];
    NSString *imageString =  swipeObject.answerImage;//[tempData objectForKey:@"image"];
    NSString *infoString =  @"Selecionar";//[tempData objectForKey:@"text"];
    
    ((UILabel *)[view viewWithTag:1]).text = infoString;
    UIImageView *imageViewQuestion = ((UIImageView *)[view viewWithTag:2]);    [imageViewQuestion setImageWithURL:[NSURL URLWithString:imageString] placeholderImage:[UIImage imageNamed:@"Carousel1"]];
    return view;
}
/*- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
 {
 return 150;
 }
 */
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    if(DEVICE_BIGER_SIDE < 568){
        return (CarouselHeight+5) * 0.8;
    }
    return CarouselHeight+5;
}


#pragma mark Picker Tapped

-(void)pickerTapped:(id)sender {
    //NSLog(@"Picker tapped");
    //NSLog(@"Selected  %li", (long)[carouselPickerView selectedRowInComponent:0]);
    if([self haveBackgroundFeedback]){
        return;
    }
    
    //CGPoint loc = [tapGesture locationInView:carouselPickerView];
    //CGPoint loc2 = [tapGesture locationInView:self];
    
    
//    if (CGRectContainsPoint(selectView.frame, loc2)){
        //NSLog(@"CGRectContainsPoint");
       // NSDictionary *tempData = [arrayAnswers objectAtIndex:( (long)[carouselPickerView selectedRowInComponent:0] % ([arrayAnswers count]))];
        
        InstanceAnswersSwipeCarouselObject *swipeAnswerObject = [arrayAnswers objectAtIndex:( (long)[carouselPickerView selectedRowInComponent:0] % ([arrayAnswers count]))];

        
        InstanceAnswersSwipeCarouselObject *swipeQuestionObject = [arrayAnswers objectAtIndex: (self.nAttemptsLeft - 1) ];
        
        
        
        if([self checkSwipeCarouselQuestion: [swipeQuestionObject carouselSegmentQuestionText] answer: [swipeAnswerObject carouselSegmentQuestionText]])
        {
//            [self.vehicleDelegate  vehiclePass:YES];
            [self showCorrectFeedback:swipeQuestionObject];
        }
        else
        {
            [self showIncorrectFeedback];
        }
//    }
}

#pragma mark UIGestureRecognizerDelegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return true;
}

#pragma mark Feedback


-(UIView*)makeFeedbackView{
    if(!newFeedbackView){
        UIImageView *flagView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Carousel1"]];
        [flagView setHeight:170];
        [flagView setWidth:CarouselWidth+1];
        if(DEVICE_BIGER_SIDE < 568){
            [flagView setHeight: [flagView height]*0.8];
            [flagView setWidth: [flagView width]*0.8];
        }
        newFeedbackView = [[UIView alloc] initWithFrame:flagView.frame];
        newFeedbackView.backgroundColor = [UIColor clearColor];
        
        [[newFeedbackView layer]setMasksToBounds:TRUE];
        newFeedbackView.clipsToBounds = YES;
        [[newFeedbackView layer]setCornerRadius:2.0f];
        [[newFeedbackView layer]setBorderWidth:0.9f];
        [[newFeedbackView layer] setBorderColor:[UIColor whiteColor].CGColor];
        [self addSubview:newFeedbackView];
        
        [newFeedbackView autoSetDimensionsToSize:CGSizeMake([flagView width], [flagView height])];
        [newFeedbackView autoCenterInSuperview];
    }
    return newFeedbackView;
}

- (void)showCorrectFeedback:(InstanceAnswersSwipeCarouselObject *)swipeObject
{
//    carouselPickerView.userInteractionEnabled = NO;
    self.nAttemptsLeft--;
    [self.vehicleDelegate addPoints:kParam001 - (incorrectNumbersLeft * kParam002)];
    [self showFeedbackWithStatus:YES forView:[self makeFeedbackView]];
    NSLog(@"M");
    [AUDIO_MANAGER playRightSound];
    [AUDIO_MANAGER playRemoteAudioFile:[NSURL URLWithString:[swipeObject afterClickAudio]]];
    self.vehiclePlayStatus = 2;
    
    if (self.nAttemptsLeft == 0)
    {
        carouselPickerView.userInteractionEnabled = NO;
        [self performSelector:@selector(finishVehicle) withObject:nil afterDelay: 1.0];
        [super showCorrectFeedback];
        [self.vehicleDelegate vehiclePass:YES];
    }
    else
    {
        [self performSelector:@selector(hideAllFeedbackView) withObject:nil afterDelay:3];
        [self performSelector:@selector(setQuestionText) withObject:nil afterDelay:3];
        [self performSelector:@selector(randomSwipeCarouselPickerView) withObject:nil afterDelay:1.5];
    }
}

- (void)showIncorrectFeedback{
    //carouselPickerView.userInteractionEnabled = NO;
    [self showFeedbackWithStatus:NO forView:[self makeFeedbackView]];
    [AUDIO_MANAGER playWrongSound];
    if (self.nAttemptsLeft == 0)
    {
        //self.vehiclePlayStatus = 1;
        [self performSelector:@selector(showRightPickerImage) withObject:nil afterDelay:1.5];
    }
    else
    {
        [self performSelector:@selector(hideAllFeedbackView) withObject:nil afterDelay:1.5];
        incorrectNumbersLeft++;
        if(incorrectNumbersLeft == [PARAMETER_MANAGER getParameterAttempts])
        {
            self.nAttemptsLeft--;
            [self showCorrectImagePickerView];
            
            if(self.nAttemptsLeft != 0)
            {
                [self performSelector:@selector(setQuestionText) withObject:nil afterDelay:3];
                [self performSelector:@selector(randomSwipeCarouselPickerView) withObject:nil afterDelay:1.5];
            }
            else
            {
                [self performSelector:@selector(finishVehicle) withObject:nil afterDelay:1.5];
            }
            
        }
    }
}

-(void)showRightPickerImage{
//    [self hideAllFeedbackView];
//    [self showFeedbackWithStatus:YES forView:[self makeFeedbackView]];
//    [carouselPickerView selectRow:[self findCorrectCountInPickerView] inComponent:0 animated:YES];
//    carouselPickerView.userInteractionEnabled = NO;
    
    [self showCorrectImagePickerView];
    
    
    [self performSelector:@selector(endLastRoll) withObject:nil afterDelay:1.5];
    
}

-(void)endLastRoll{
    //[super showIncorrectFeedback];
    [self finishVehicle];
}
-(void)hideAllFeedbackView{
    [super hideAllFeedbackView];
    if(newFeedbackView){
        [newFeedbackView removeFromSuperview];
        newFeedbackView = nil;
    }
}


#pragma mark - Support methods

- (int)findCorrectCountInPickerView
{
    BOOL isCorrect = FALSE;
    int correctCount = (int)[carouselPickerView selectedRowInComponent: 0];
    InstanceAnswersSwipeCarouselObject *swipeCorrectObject = [arrayAnswers objectAtIndex: self.nAttemptsLeft];
    
    while(!isCorrect)
    {
        correctCount ++;
        int realCount = correctCount % [arrayAnswers count];
        
        //        if(self.nAttemptsLeft)
        
        InstanceAnswersSwipeCarouselObject *swipeSearchObject = [arrayAnswers objectAtIndex: realCount];
        
        
        
        if([swipeSearchObject.carouselSegmentQuestionText isEqualToString: swipeCorrectObject.carouselSegmentQuestionText])
        {
            NSLog(@"TRUE");
            isCorrect = YES;
        }
    }
    
    return correctCount;
}

- (void)showCorrectImagePickerView
{
    [self hideAllFeedbackView];
    [self showFeedbackWithStatus:YES forView:[self makeFeedbackView]];
    [carouselPickerView selectRow:[self findCorrectCountInPickerView] inComponent:0 animated:YES];
    [self performSelector:@selector(hideAllFeedbackView) withObject:nil afterDelay:2];
}

- (BOOL)checkSwipeCarouselQuestion:(NSString *)question answer:(NSString *)answer
{
    if([question isEqualToString:answer])
    {
        return YES;
    }
    return NO;
}

- (void)randomSwipeCarouselPickerView
{
    for (int i = 0; i < 3; i++)
    {
//        NSUInteger randomRow = arc4random_uniform(INT16_MAX);
        NSUInteger randomRow = arc4random_uniform(100);
        [self performSelector:@selector(selectRowInPicker:) withObject:@(randomRow) afterDelay:i*0.3];
    }
}

- (void)setQuestionText
{
    incorrectNumbersLeft = 0;
    InstanceAnswersSwipeCarouselObject *swipeObject = [arrayAnswers objectAtIndex: (self.nAttemptsLeft - 1) ];
    [self.vehicleDelegate setQuestionBarTextWithVocab:swipeObject.carouselSegmentQuestionText];
}


@end
