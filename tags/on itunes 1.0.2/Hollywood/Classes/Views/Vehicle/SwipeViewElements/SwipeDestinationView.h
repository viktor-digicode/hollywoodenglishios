//
//  SwipeDestinationView.h
//  NavigationHollywood
//
//  Created by Dimitar Shopovski on 3/21/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwipeDestinationView : UIView {
    
    UILabel *labelAnswer;
    UIImageView *imageViewAnswer;
    
}

@property (nonatomic, assign) BOOL isTextElement;
@property (nonatomic, assign) BOOL isLeftView;

- (void)setDestinationViewData:(id)data;
- (void)setText:(NSString*)newText;
- (void)makeTextLabel:(CGRect)frame;
- (void)setImage:(NSString*)imgString;
- (void)makeImage:(CGRect)frame;

@end
