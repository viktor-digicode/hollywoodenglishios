//
//  SwipeMovingView.h
//  NavigationHollywood
//
//  Created by Dimitar Shopovski on 3/21/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SwipeMovingViewDelegate <NSObject>

- (void)leftSwipeDetected:(id)sender;
- (void)rightSwipeDetected:(id)sender;

@end

@interface SwipeMovingView : UIView <UIGestureRecognizerDelegate> {
    
    UILabel *labelAnswer;
    UIImageView *imageViewAnswer;
    
}

@property (nonatomic, assign) BOOL isTextElement;
@property (nonatomic, strong) id<SwipeMovingViewDelegate> swipeViewDelegate;
@property (nonatomic, strong) id movingViewData;

@property (nonatomic) UISwipeGestureRecognizer *swipeGestureLeft;
@property (nonatomic) UISwipeGestureRecognizer *swipeGestureRight;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data;
- (void)setText:(NSString*)newText;
- (void)makeTextLabel:(CGRect)frame;
- (void)setImage:(NSString*)imgString;
- (void)makeImage:(CGRect)frame;

@end
