//
//  ClickToLearnVehicle.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 3/30/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "BaseViewVehicle.h"
#import "ClickImageView.h"
#import "VehicleObjectClickToLearn+CoreDataClass.h"
#import "VehicleObjectMultipleImage+CoreDataClass.h"

@interface ClickToLearnVehicle : BaseViewVehicle<ClickImageViewDelegate, LMAudioManagerDelegate>

@property (nonatomic, strong) id dataClickToLearnInstance;
@property (nonatomic, strong) BaseVehiclesObject *dataVehicleObject;
@property (nonatomic, assign) BOOL isLearnVehicle;
@property (nonatomic, assign) BOOL isOneImageLayout;
@property (nonatomic, assign) BOOL isOrdered;
@property (nonatomic, assign) BOOL isQuizVehicle;
@property (nonatomic, assign) NSInteger positionInQuiz;

@property (nonatomic, strong) UIView *viewAnswerContent;
@property (nonatomic, strong) UIImageView *imageViewOneImageAnswer;

@property (nonatomic, assign) ClickImageView *currentAnswer;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *constraintContainterWidth;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *constraintContainterHeight;

@property (nonatomic, strong) NSMutableArray *arrayCorrectAnswers;
@property (nonatomic, assign) NSInteger nCorrectAnswers;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data isLearnVehicle:(BOOL)type;
- (instancetype)initWithFrame:(CGRect)frame data:(id)data isLearnVehicle:(BOOL)type isOrdered:(BOOL)ordered;
- (instancetype)initWithFrame:(CGRect)frame quizVehicleData:(id)data position:(NSInteger)position;

//help methods
- (UIView *)createContentView:(CGRect)frame;
- (void)createLayout:(NSArray *)arrayAnswers minFont:(CGFloat)minFontSize;
- (CGFloat)getMinimumFontSize:(NSArray *)arrayAnswers isLearn:(BOOL)isLearn;

@property (nonatomic, assign) BOOL isAnsweredCorrect;


@end
