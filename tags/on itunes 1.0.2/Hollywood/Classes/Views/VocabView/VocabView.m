//
//  VocabView.m
//  VocabTest
//
//  Created by Dimitar Shopovski on 7/19/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import "VocabView.h"
#import "CustomDataLabel.h"
#import "WordsObject+CoreDataClass.h"

@implementation VocabView
@synthesize imgBallon;

- (instancetype)initWithFrame:(CGRect)frame withData:(id)data {
    
    if (self = [super initWithFrame:frame]) {
        [self setBackgroundColor:[UIColor clearColor]];
        NSString *textString = [data objectForKey:@"title"];
        [self buildTextViewFromString:textString];
        
        [self createUX];
    }
    
    return self;
}

- (void)buildTextViewFromString:(NSString *)localizedString withFont:(NSNumber *)fontSize {
    
    self.fontSize = fontSize;
    [self buildTextViewFromString:localizedString];
    
}

- (void)buildTextViewFromString:(NSString *)localizedString
{
    
    CGFloat fontSizeValue = [self.fontSize floatValue];
    
    if (!fontSizeValue) {
        fontSizeValue = 20.0;
    }
    
    isFirstRow = YES;

    // 1. Split the localized string on the # sign:
    NSArray *localizedStringPieces = [localizedString componentsSeparatedByString:@" "];
    
    // 2. Loop through all the pieces:
    NSUInteger msgChunkCount = localizedStringPieces ? localizedStringPieces.count : 0;
    CGPoint wordLocation = CGPointMake(0.0, 0.0);
    for (NSUInteger i = 0; i < msgChunkCount; i++)
    {
        NSString *chunk = [localizedStringPieces objectAtIndex:i];
        if ([chunk isEqualToString:@""])
        {
            continue;     // skip this loop if the chunk is empty
        }
        
        // 3. Determine what type of word this is:
        BOOL isVocabText = ([chunk containsString:@"["] && [chunk containsString:@"]"]);
//        BOOL isPrivacyPolicyLink  = [chunk hasPrefix:@"<pp>"];
//        BOOL isLink = (BOOL)(isTranslationText || isPrivacyPolicyLink);
        
        BOOL isLink = isVocabText;
        
        // 4. Create label, styling dependent on whether it's a link:
        CustomDataLabel *label = [[CustomDataLabel alloc] init];
        label.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSizeValue];
        label.text = chunk;
        label.userInteractionEnabled = isLink;
        
        if (isLink)
        {
            label.textColor = [UIColor whiteColor];
            label.highlightedTextColor = [UIColor yellowColor];
            
            // 5. Set tap gesture for this clickable text:
            SEL selectorAction = @selector(tapOnVocabWord:);
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                         action:selectorAction];
            [label addGestureRecognizer:tapGesture];
            
            label.text = chunk;
            
            NSRange rangeOfStart = [chunk rangeOfString:@"["];
            NSRange rangeOfLast = [chunk rangeOfString:@"]"];
            chunk = [chunk stringByReplacingOccurrencesOfString:@"[" withString:@""];
            chunk = [chunk stringByReplacingOccurrencesOfString:@"]" withString:@""];
        
            NSInteger linkWordLength = rangeOfLast.location - rangeOfStart.location - 1;
            
            NSString *vocabOriginalText = [chunk substringWithRange:NSMakeRange(rangeOfStart.location, linkWordLength)];
            
            VocabsObject *vocab = [VOCAB_MANAGER getVocabForString:vocabOriginalText];
            NSSet *translateOptions = vocab.nativeVocabs;
            if (translateOptions) {
                
                WordsObject *wordObject = (WordsObject *)[[translateOptions allObjects] objectAtIndex:0];
                [label setData:wordObject];
                self.currentVocabObject = wordObject;
            }
            

            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:chunk];
            [attributedString addAttributes:@{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle),
                                              NSForegroundColorAttributeName: APP_VOCAB_COLOR,
                                              NSFontAttributeName: [UIFont italicSystemFontOfSize:fontSizeValue]
                                              } range:NSMakeRange(rangeOfStart.location, linkWordLength)];
            
            
            [label setAttributedText:attributedString];
        }
        else
        {
            label.textColor = [UIColor whiteColor];
        }
        
        // 6. Lay out the labels so it forms a complete sentence again:
        
        // If this word doesn't fit at end of this line, then move it to the next
        // line and make sure any leading spaces are stripped off so it aligns nicely:
        
        [label sizeToFit];
        
        if (self.frame.size.width < wordLocation.x + label.bounds.size.width + 4.0)
        {
            wordLocation.x = 0.0;                       // move this word all the way to the left...
            wordLocation.y += label.frame.size.height;  // ...on the next line
            
            // And trim of any leading white space:
            NSRange startingWhiteSpaceRange = [label.text rangeOfString:@"^\\s*"
                                                                options:NSRegularExpressionSearch];
            if (startingWhiteSpaceRange.location == 0)
            {
                label.text = [label.text stringByReplacingCharactersInRange:startingWhiteSpaceRange
                                                                 withString:@""];
                [label sizeToFit];
            }
        }
        else {
            
            if (!isFirstRow) {
                wordLocation.x = wordLocation.x + 4.0;
            }
            else {
                isFirstRow = NO;
            }
        }
        
        // Set the location for this label:
        label.frame = CGRectMake(wordLocation.x,
                                 wordLocation.y,
                                 label.frame.size.width,
                                 label.frame.size.height);
        // Show this label:
        [self addSubview:label];
        
        // Update the horizontal position for the next word:
        wordLocation.x += label.frame.size.width;
    }
}

- (void)tapOnVocabWord:(UITapGestureRecognizer *)tapGesture
{
    CustomDataLabel *selectedLabel = (CustomDataLabel *)tapGesture.view;
    
    if (tapGesture.state == UIGestureRecognizerStateEnded)
    {
        if (selectedLabel == currentSelectedVocabLabel && isTranslationShown) {
            
            if (_vocabTimer != nil) {
                
                [_vocabTimer invalidate];
                _vocabTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(stopTheTimer:) userInfo:nil repeats:NO];
            }
        }
        else {
            
            CGFloat xPos = tapGesture.view.center.x - self.translationView.frame.size.width/2;
            CGFloat yPos = tapGesture.view.frame.origin.y + tapGesture.view.frame.size.height;
            
            self.translationView.hidden = NO;
            
            _translationWordLabel.frame = CGRectMake(_translationWordLabel.x, _translationWordLabel.y, 200, _translationWordLabel.height);
            _translationWordLabel.text = [selectedLabel.data word];
            [_translationWordLabel sizeToFit];
            
            
            if (![selectedLabel.data wordAudio] || [[selectedLabel.data wordAudio] isEqualToString:@""]) {
                
                _buttonSound.hidden = YES;
                imgBallon.frame = CGRectMake(0, 0, _translationWordLabel.width + 28, 36);

            }
            else {
                
                _buttonSound.hidden = NO;
                imgBallon.frame = CGRectMake(0, 0, _translationWordLabel.width + 28 + 24, 36);

            }
            
            if (xPos-30 < 0) {
                
                self.translationView.frame = CGRectMake(-30, yPos, _translationWordLabel.width + 29 + 4 + 10, 36);

            }
            else {
                
                self.translationView.frame = CGRectMake(xPos-30, yPos, _translationWordLabel.width + 29 + 4 + 10, 36);

            }
            _buttonSound.frame = CGRectMake(_translationView.width-22, 10, 20, 20);

            //new code label alignment
            _translationWordLabel.frame = CGRectMake(10, _translationWordLabel.y, _translationWordLabel.width, _translationWordLabel.height);
            
            isTranslationShown = YES;
            
            if (selectedLabel != currentSelectedVocabLabel && _vocabTimer != nil) {
                [_vocabTimer invalidate];
            }
            currentSelectedVocabLabel = selectedLabel;

            _vocabTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(stopTheTimer:) userInfo:nil repeats:NO];
        }
    }
}

- (void)stopTheTimer:(id)sender {
    
    self.translationView.hidden = YES;
    isTranslationShown = NO;

}

#pragma mark - createUX

- (void)createUX {
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.translationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 110, 36)];
    self.translationView.hidden = YES;
    
    //image
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(6, 6, 6, 12);
    UIImage *backgroundButtonImage = [[UIImage imageNamed:@"ballon-img-v2"]
                                      resizableImageWithCapInsets:edgeInsets];
    
    
    //image
    imgBallon = [[UIImageView alloc] initWithImage:backgroundButtonImage];
    imgBallon.frame = CGRectMake(0, 0, 72, 36);
    [imgBallon setContentMode:UIViewContentModeScaleToFill];
    [self.translationView addSubview:imgBallon];
    
    _translationWordLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, 72-32, 18)];
    _translationWordLabel.text = @"translation";
    [_translationWordLabel setTextColor:APP_VOCAB_TRANSLATION_COLOR];
    [_translationWordLabel setFont:[UIFont systemFontOfSize:20.0]];
    [self.translationView addSubview:_translationWordLabel];
    
    _buttonSound = [UIButton buttonWithType:UIButtonTypeCustom];
    _buttonSound.frame = CGRectMake(self.translationView.frame.size.width-37, 10, 20, 20);
    [_buttonSound setImage:[UIImage imageNamed:@"sound-mct"] forState:UIControlStateNormal];
    [_buttonSound addTarget:self action:@selector(playVocabSound:) forControlEvents:UIControlEventTouchUpInside];
    [self.translationView addSubview:_buttonSound];
    
    [self addSubview:self.translationView];
    
}

#pragma mark - Remove subviews

- (void)cleanControl {
    
    for (UIView *vv in self.subviews) {
        [vv removeFromSuperview];
    }
}

#pragma mark - Helper

- (NSString *)findTranslateWord:(NSString *)sourceText betweenTag:(NSString *)tag {

    NSString *s = sourceText;
    
    NSRange r1 = [s rangeOfString:[NSString stringWithFormat:@"<%@>", tag]];
    NSRange r2 = [s rangeOfString:[NSString stringWithFormat:@"</%@>", tag]];
    NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
    NSString *sub = [s substringWithRange:rSub];
    
    return sub;
}

#pragma mark - Set the word in favourites

- (IBAction)addToFavourites:(id)sender {
    
    if (_vocabTimer != nil && isTranslationShown) {
            
        [_vocabTimer invalidate];
        _vocabTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(stopTheTimer:) userInfo:nil repeats:NO];
            
    }
    
    UIButton *buttonFav = (UIButton *)sender;
    
    NSData *data1 = UIImagePNGRepresentation(buttonFav.currentImage);
    NSData *data2 = UIImagePNGRepresentation([UIImage imageNamed:@"star-normal"]);
    
    if (![data1 isEqual:data2]) {
        
        [buttonFav setImage:[UIImage imageNamed:@"star-normal"] forState:UIControlStateNormal];

    }
    else {
        [buttonFav setImage:[UIImage imageNamed:@"star-selected"] forState:UIControlStateNormal];

    }
    
}

#pragma mark - Play sound

- (IBAction)playVocabSound:(id)sender {
    
    if (self.currentVocabObject) {
        
        [AUDIO_MANAGER playRemoteAudioFile:[NSURL URLWithString:[self.currentVocabObject wordAudio]]];
        NSLog(@"Sound playing - %@", [self.currentVocabObject wordAudio]);

    }
    
}


@end
