//
//  AnalyticConstants.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 10/4/16.
//  Copyright © 2016 aa. All rights reserved.
//

#ifndef AnalyticConstants_h
#define AnalyticConstants_h

#pragma mark - Constants
#pragma mark - Tracking id

#define  kTrackingId  @"UA-93846781-2"

#pragma mark - Product name

#define  kProductName  @"Hollywood English Course"

#pragma mark - Event action

#define kEventLaunch                @"Launch"

#define kQuizNSuffGuest @"QuizClick_NSuff_Guest"
#define kQuizNSuffActive @"QuizClick_NSuff_Active"
#define kQuizPurchase @"QuizClick_Purchase"
#define kQuizAlreadyPurchased @"QuizClick_Already_Purchase"
#define kQuizFree @"QuizClick_Free"

#pragma mark - Screens

#define kVipScreen              @"VIP section"
#define kVipQuizzesScreen       @"VIP Quizzes selection"
#define kVipQuizScreen          @"VIP Quiz"
#define kVipSummaryScreen       @"VIP Summary"
#define kLanguageSelection      @"Initial language selection"
#define kConversionScreen       @"Conversion screen"
#define kHomePageScreen         @"Home Page"
#define kLoginScreen            @"Login Screen"
#define kFillConversionScreen   @"Full conversion screen"
#define kPackagesScreen         @"Packages Screen"
#define kSmallConversionScreen  @"Small  conversion screen"
#define kUserCreationScreen     @"User Creation"
#define kSwipeScreen            @"Swipe"
#define kVideoScreen            @"Video"
#define kClickToLearnScreen     @"Click To Learn"
#define kMCIScreen              @"Multiple Choice Image"
#define kMCTScreen              @"Multiple Choice Text"
#define kSwipeCarouselScreen    @"Swipe Carousel"
#define kFillMissingScreen      @"Fill In The Missing Words"
#define kSpellingScreen         @"Spelling"
#define kVoiceScreen            @"Voice"
#define kSmallOrFullScreen      @"small or full conversion screen"
#define kNoInternetScreen       @"No Internet"
#define kGuestUserScreen        @"Call Create Guest user"
#define kUserResponse           @"Create user response"

#define kQuizScreen @"Quizzes_Screen"


#define kEnterFillTheMissing @"FTMW_Start"

#define kQuizVehiclePresented @"Quiz_Vehicle_Presented"
#define kQuizSwipeInstance @"Quiz_Vehicle_Swiped"

enum LMDimensionType : NSUInteger {
    
    LMDIMENSION_PRODUCT_NAME        = 1,
    LMDIMENSION_USER_STATUS         = 2,
    LMDIMENSION_REVISION            = 3,
    LMDIMENSION_VERSION             = 4,
    LMDIMENSION_TIMESTAMP           = 5,
    LMDIMENSION_DEVICEID            = 6,
    LMDIMENSION_USERID              = 7,
};

#endif /* AnalyticConstants_h */
