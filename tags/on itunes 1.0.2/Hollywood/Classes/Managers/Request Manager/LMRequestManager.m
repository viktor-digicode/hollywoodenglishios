//
//  LMRequestManager.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/21/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LMRequestManager.h"

@implementation LMRequestManager

SINGLETON_GCD(LMRequestManager)

+ (AFHTTPSessionManager*) manager
{
    static dispatch_once_t onceToken;
    static AFHTTPSessionManager *manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [AFHTTPSessionManager manager];
    });
    
    return manager;
}

- (void)getDataFor:(ELMRequestType)type
           headers:(NSDictionary *)headers
withSuccessCallBack:(void (^)(NSDictionary *responseObject)) successCallback
andWithErrorCallBack:(void (^)(NSString *errorMessage)) errorCallback
{
    
    AFURLSessionManager *manager =  [LMRequestManager manager];
    //[[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *req = [self post:type headers:headers];
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (!error) {
            NSLog(@"Reply JSON: %@", responseObject);
            successCallback ((NSDictionary *)responseObject);
        } else {
            
            errorCallback([error localizedDescription]);
        }
    }] resume];
}

- (NSMutableURLRequest *)post:(ELMRequestType)type headers:(NSDictionary *)headers{
    NSString *urlString = [self urlForType:type headers:headers];
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlString parameters:nil error:nil];
    
    request.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    return request;
}


- (NSString *)urlForType:(ELMRequestType)type headers:(NSDictionary *)headers {
    NSString *path = 0;
    NSString *deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    switch (type) {
            
        case kRequestFirstLaunchAPICall:
            //self.courseID = @"62";
            self.courseMode = kApiBaseCourseMode;
            //path = @"http://hollywood.la-mark-il.com:8080/hollywoodenglish/api/firstLaunch?courseId=62&userId=45&mode=stage";
            path = [NSString stringWithFormat:@"%@firstLaunch?courseId=%@&mode=%@", kApiBaseUrl, self.courseID,self.courseMode];
            break;
        case kRequestGetLanguagesAPICall:
            self.applicationID = kApiBaseaAplicationId;
            self.courseMode = kApiBaseCourseMode;
            //path = @"http://hollywood.la-mark-il.com:8080/hollywoodenglish/api/getLanguagesForApplication?applicationId=%@&mode=stage";
            path = [NSString stringWithFormat:@"%@getLanguagesForApplication?applicationId=%@&mode=%@", kApiBaseUrl, self.applicationID,self.courseMode];
            break;
        case kCreateGuestUser:
            //self.courseID = @"62";
            //path = [NSString stringWithFormat:@"http://hollywood.la-mark-il.com:8080/hollywoodenglish/api/createGuestUser?courseId=%@&deviceId=%@", @"62", deviceIdentifier];
            path = [NSString stringWithFormat:@"%@createGuestUser?courseId=%@&deviceId=%@",kApiBaseUrl, self.courseID, deviceIdentifier];
            break;
        case kRequestLoginUserAPICall:
            //self.courseID = @"62";
            path = [NSString stringWithFormat:@"%@usernameLogin?courseId=%@&deviceId=%@&email=%@&password=%@&appUserId=%@&mode=%@&score=%@&vipPasses=%@",kApiBaseUrl, self.courseID, deviceIdentifier, headers[kUserEmail], headers[kUserPassword], [USER_MANAGER getUserApplicationId], kApiBaseCourseMode, @"0", @"0"];
            break;
        case kRequestGetVipDataAPICall:
            path = [NSString stringWithFormat:@"%@getVipData?appUserId=%@&mode=%@",kApiBaseUrl,[USER_MANAGER getUserApplicationId],self.courseMode];
            break;
        case kRequestGetQuizDataAPICall:
            path = [NSString stringWithFormat:@"%@getQuizData?appUserId=%@&mode=%@&quizId=%@&courseId=%@",kApiBaseUrl,[USER_MANAGER getUserApplicationId],self.courseMode,headers[kUserQuizId], self.courseID];
            break;
        case kRequestIsMsisdnExistAPICall:
            path = [NSString stringWithFormat:@"%@isMsisdnExist?msisdn=%@&appUserId=%@&courseId=%@&deviceId=%@&mode=%@&score=%@&vipPasses=%@",kApiBaseUrl, headers[kUserMSISDN],[USER_MANAGER getUserApplicationId],self.courseID,deviceIdentifier,self.courseMode,headers[kProgressScore],headers[kProgressVipPasses] ];
            break;
        case kRequestSaveUserProgressionAPICall:
            path = [NSString stringWithFormat:@"%@saveUserProgression?appUserId=%@&lessonOrder=%@&rankOrder=%@&levelOrder=%@&isLessonCompleted=%@&stars=%@&score=%@&mode=%@",
                    kApiBaseUrl,
                    [USER_MANAGER getUserApplicationId],
                    headers[kProgressLessonOrder],
                    headers[kProgressRankOrder],
                    headers[kProgressLevelOrder],
                    headers[kProgressLessonCompleted],
                    headers[kProgressStarNumber],
                    headers[kProgressScore],
                    self.courseMode];
            break;
        case kRequestGetUserDataAPICall:
            path = [NSString stringWithFormat:@"%@getUserData?appUserId=%@&mode=%@",
                    kApiBaseUrl,
                    [USER_MANAGER getUserApplicationId],
                    self.courseMode];
            break;
        case kRequestCreateUserAPICall:          
            path = [NSString stringWithFormat:@"%@createUser?courseId=%@&deviceId=%@&appUserId=%@&paymentMethod=apple&mode=%@",
                    kApiBaseUrl,
                    self.courseID,
                    deviceIdentifier,
                    [USER_MANAGER getUserApplicationId],
                    self.courseMode];
            break;
        case kRequestGetLessonDataAPICall:
            path = [NSString stringWithFormat:@"%@getLessonData?appUserId=%@&mode=%@&courseId=%@&lessonId=%@",
                    kApiBaseUrl,
                    [USER_MANAGER getUserApplicationId],
                    self.courseMode,
                    self.courseID,
                    headers[kLessonId]
                    ];
                break;
            
            
        case kRequestGetRSSAPICall:
            self.applicationID = kApiBaseaAplicationId;
            path = [NSString stringWithFormat:@"%@getRssNews?applicationId=%@&courseId=%@&mode=%@",
                    kApiBaseUrl,
                    self.applicationID,
                    self.courseID,
                    self.courseMode
                    ];
            break;
        case kRequestCheckCourseVersionAPICall:
            self.applicationID = kApiBaseaAplicationId;
            path = [NSString stringWithFormat:@"%@checkCourseVersion?applicationId=%@&courseId=%@&mode=%@",
                    kApiBaseUrl,
                    self.applicationID,
                    self.courseID,
                    self.courseMode
                    ];
            break;
        case kRequestGetLablesForCourseAPICall:
            self.applicationID = kApiBaseaAplicationId;
            path = [NSString stringWithFormat:@"%@getLablesForCourse?courseId=%@&mode=%@",
                    kApiBaseUrl,
                    self.courseID,
                    self.courseMode
                    ];
           break;
        case kRequestGetParametersForApplicationAPICall:
            self.applicationID = kApiBaseaAplicationId;
            path = [NSString stringWithFormat:@"%@getParametersForApplication?applicationId=%@&mode=%@",
                    kApiBaseUrl,
                    self.applicationID,
                    self.courseMode
                    ];
            break;
        case kRequestSignUpAPICall:
            //self.courseID = @"62";
//            path = [NSString stringWithFormat:@"http://hollywood.la-mark-il.com:8080/hollywoodenglish/api/updateUserData?appUserId=1&deviceId=blabla&email=changeonlyemail2673@gmail.com&password=vasko1234&name=something&sureName=again&msisdn=34567890", deviceIdentifier, headers[kUserEmail], headers[kUserPassword], [USER_MANAGER getUserApplicationId]];
            break;
        default:
            break;
    }
    
    
    if (path) {
        NSLog(@"URL: %@",path);
        return path;
    }
    return 0;
}

@end
