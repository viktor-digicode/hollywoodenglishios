//
//  AppDelegate.h
//  TestAsyncImages
//
//  Created by Dimitar Shopovski on 9/29/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ImageCache.h"
#import <UIKit/UIKit.h>

@class ImageCacheObject;

@interface ImageCache : NSObject {
    
    NSUInteger totalSize;  // total number of bytes
    NSUInteger maxSize;    // maximum capacity
    NSMutableDictionary *myDictionary;
}

@property (nonatomic, readonly) NSUInteger totalSize;

- (id)initWithMaxSize:(NSUInteger)max;
- (void)insertImage:(UIImage *)image withSize:(NSUInteger)sz forKey:(NSString *)key;
- (UIImage *)imageForKey:(NSString*)key;

@end
