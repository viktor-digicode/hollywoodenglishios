//
//  PackageCollectionViewCell.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/14/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackageCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *badgeImageView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UIView *titleHolderView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@end
