//
//  PackageCollectionViewCell.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/14/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "PackageCollectionViewCell.h"

@implementation PackageCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 3;
    [self.layer setMasksToBounds:YES];
    self.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.layer.borderWidth = 1.0f;
}

@end
