//
//  QuizSummaryViewController.m
//  Hollywood
//
//  Created by Kiril Kiroski on 11/9/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "QuizSummaryViewController.h"
#import "ConversionViewController.h"
#import "ProgressLessonObject.h"
#import <FBSDKShareKit/FBSDKShareLinkContent.h>
#import <FBSDKShareKit/FBSDKShareOpenGraphObject.h>
#import <FBSDKShareKit/FBSDKShareOpenGraphContent.h>
#import <FBSDKShareKit/FBSDKShareOpenGraphAction.h>
#import <FBSDKShareKit/FBSDKShareDialog.h>
#import <FBSDKShareKit/FBSDKSharePhoto.h>
#import <FBSDKShareKit/FBSDKSharePhotoContent.h>
#import <TwitterKit/TwitterKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/PHPhotoLibrary.h>
#import <Photos/PHAssetChangeRequest.h>
#import <Photos/PHImageManager.h>
#import "UILabel+FontSizeToFit.h"

@interface QuizSummaryViewController (){
    
    __weak IBOutlet UIScrollView *leftScrollView;
    IBOutlet UIView *leftOutsideView;
    
    __weak IBOutlet UIView *topBarView;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *backToLabel;
    __weak IBOutlet UILabel *vipScoreLabel;
    __weak IBOutlet UILabel *vipStaticTextLabel;
    __weak IBOutlet UILabel *performanceTextLabel;
    
    __strong ProgressUserObject *currentProgressUserObject;
    
    __weak IBOutlet KNCirclePercentView *circleProgress;
    
    __strong ProgressLevelObject *currentProgressLevelObject;
    
    __weak IBOutlet UIImageView *nextLessonImageView;
    __weak IBOutlet UILabel *nextLessonNumberLabel;
    __weak IBOutlet UILabel *nextLessonTextLabel;
    
    __weak IBOutlet UIView *containerNextLessonImageView;
    __weak IBOutlet UIImageView *bigSummaryScreenImageView;
    
    VipQuizObject *currentVipQuizObject;
    
    __weak IBOutlet UIView *loadingView;
    __weak IBOutlet UIActivityIndicatorView *activityIndicatorLoading;
    __weak IBOutlet UILabel *loadingLabel;

}

@end

@implementation QuizSummaryViewController

- (void)configureUI {
    
    [super configureUI];
    
    NSLog(@"--- %@ ---", self.currentVipCategoryObject);
    
    performanceTextLabel.text = [self performanceTextForScore];
    [performanceTextLabel fontSizeToFit];
    CGFloat percent = (float)_corectAnswersNumber/(float)self.totalAnswers*100.0;
    [circleProgress drawCircleWithPercent:percent
                                 duration:0
                                lineWidth:2
                                clockwise:YES
                                  lineCap:kCALineCapSquare
                                fillColor:[UIColor colorWithRed:136.0/255.0 green:148.0/255.0 blue:140.0/255.0 alpha:1.0]
                              strokeColor:[UIColor whiteColor]
                           animatedColors:nil];
    
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kVipSummaryScreen action:@"Quiz_Summary_Presented" additionalData:[NSString stringWithFormat:@"Percent %f", percent]];

    
    NSMutableAttributedString *decimalRepresentation = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%ld/%ld", _corectAnswersNumber, self.totalAnswers]];
    
    int rangeStartIndex = 0;
    if (_corectAnswersNumber<10) {
        [decimalRepresentation addAttribute:NSFontAttributeName
                                      value:[UIFont systemFontOfSize:26.0]
                                      range:NSMakeRange(0, 1)];
        rangeStartIndex = 1;
        
    }
    else {
        [decimalRepresentation addAttribute:NSFontAttributeName
                                      value:[UIFont systemFontOfSize:26.0]
                                      range:NSMakeRange(0, 2)];
        rangeStartIndex = 2;
        
    }
    
    if (self.totalAnswers<10) {
        
        [decimalRepresentation addAttribute:NSFontAttributeName
                                      value:[UIFont systemFontOfSize:16.0]
                                      range:NSMakeRange(rangeStartIndex, 2)];
        
    }
    else {
        
        [decimalRepresentation addAttribute:NSFontAttributeName
                                      value:[UIFont systemFontOfSize:16.0]
                                      range:NSMakeRange(rangeStartIndex, 3)];
        
    }
    [circleProgress.percentLabel setAttributedText:decimalRepresentation];
    
    circleProgress.percentLabel.textColor = [UIColor whiteColor];
    
    
    //next lesson logic
    currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
    
    //long currentLesson = currentProgressLevelObject.currentUnit;
    NSInteger maximumLessonNumber = currentProgressLevelObject.maxUnit;
    
    LessonObject *currentLessonObject = [DATA_MANAGER takeLessonObjectObject:(int)maximumLessonNumber];
    
    id currentLessonData;
    currentLessonData = @{
                          @"lessonStatus":@(2),
                          @"lessonNumber":@([currentLessonObject.lesson_order intValue]+1),
                          @"lessonImage":currentLessonObject.lesson_home_page_image,
                          @"leasonTeaserImage":currentLessonObject.lesson_teaser};
    
    /*if(maximumLessonNumber+1 > [[DATA_MANAGER takeAllLessonObject] count]){
        currentLessonData = @{
                              @"lessonStatus":@(2),
                              @"lessonNumber":@(maximumLessonNumber),
                              @"lessonImage":currentLessonObject.lesson_home_page_image,
                              @"leasonTeaserImage":currentLessonObject.lesson_teaser};
    }*/
    
    containerNextLessonImageView.layer.cornerRadius = 4.0;
    containerNextLessonImageView.layer.borderWidth = 1.0;
    containerNextLessonImageView.layer.borderColor = [UIColor colorWithRed:111.0/255.0 green:102.0/255.0 blue:55.0/255.0 alpha:1.0].CGColor;
    containerNextLessonImageView.layer.masksToBounds = YES;
    
    
    nextLessonImageView.image = [UIImage imageNamed:[currentLessonData objectForKey:@"lessonImage"]];
    nextLessonNumberLabel.text = [NSString stringWithFormat:@"%ld", [[currentLessonData objectForKey:@"lessonNumber"] longValue]];
    
    NSNumber *categoryId = self.currentVipCategoryObject.categoryId;
    if ([categoryId integerValue] == 1) {
        
        bigSummaryScreenImageView.image = [UIImage imageNamed:@"iOS-TVQuizSummaryImage"];
    }
    else if ([categoryId integerValue] == 2) {
        
        bigSummaryScreenImageView.image = [UIImage imageNamed:@"iOS-MoviesQuizSummaryImage"];
        
    }
    
    bigSummaryScreenImageView.layer.cornerRadius = 4.0;
    bigSummaryScreenImageView.layer.borderWidth = 1.0;
    bigSummaryScreenImageView.layer.borderColor = [UIColor colorWithRed:111.0/255.0 green:102.0/255.0 blue:55.0/255.0 alpha:1.0].CGColor;
    bigSummaryScreenImageView.layer.masksToBounds = YES;
    
    
    self.navigationController.navigationBarHidden = YES;
    topBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar_bg_up"]];
    
    titleLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:30.0];
    vipScoreLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:15.0];
    vipStaticTextLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:18.0];
    
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    
    [leftScrollView addSubview:leftOutsideView];
    leftScrollView.contentSize = leftOutsideView.frame.size;
    [self updateLabels];
    /*UIImage *originalImage = [CACHE_MANAGER getImageFromCache:currentVipQuizObject.imageName];
    bigSummaryScreenImageView.image = originalImage;*/
    //bigSummaryScreenImageView.image = [UIImage imageNamed:currentVipQuizObject.quizSummaryImage];
    [bigSummaryScreenImageView setImageWithURL:[NSURL URLWithString:currentVipQuizObject.quizSummaryImage] placeholderImage:nil];
    [nextLessonImageView setImageWithURL:[NSURL URLWithString:[currentLessonData objectForKey:@"lessonImage"]] placeholderImage:nil];
    
    _shareLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"share_info"];
    backToLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"vip_sum_back_to"];
}

- (void)setVipQuizObject:(VipQuizObject*)tempVipQuizObject{
    currentVipQuizObject = tempVipQuizObject;
   
}

- (NSString*)performanceTextForScore{
    /*NSArray *stringArray = @[@[@"Talvez este quiz não seja para você. Tente um tema diferente!",
                               @"Esse não é o seu forte...",
                               @"O que vale é a intenção..."
                               ],
                             @[@"Você pode fazer muito melhor!",
                               @"Por que você não tenta novamente?",
                               @"Mais sorte da próxima vez!"
                               ],
                             @[@"Nada mal! Você pode fazer ainda melhor!",
                               @"Bem, você acertou quase todas as respostas, siga tentando até chegar à perfeição!",
                               @"Nada mal! Você pode fazer ainda melhor!"
                               ],
                             @[@"Você é um expert sobre Hollywood!",
                               @"Este realmente é o quiz certo para você!",
                               @"Você provou suas habilidades!"
                               ],
                             @[@"Uau, você é mesmo nota 10!",
                               @"Você tirou este quiz de letra!",
                               @"Você é um verdadeiro expert sobre Hollywood!"]
                             ];*/
    NSArray *stringArray = @[@[[LABEL_MANAGER setLocalizeLabelText:@"reward_info_text_1_1"],
                               [LABEL_MANAGER setLocalizeLabelText:@"reward_info_text_1_2"],
                               [LABEL_MANAGER setLocalizeLabelText:@"reward_info_text_1_3"]
                               ],
                             @[[LABEL_MANAGER setLocalizeLabelText:@"reward_info_text_2_1"],
                               [LABEL_MANAGER setLocalizeLabelText:@"reward_info_text_2_2"],
                               [LABEL_MANAGER setLocalizeLabelText:@"reward_info_text_2_3"]
                               ],
                             @[[LABEL_MANAGER setLocalizeLabelText:@"reward_info_text_3_1"],
                               [LABEL_MANAGER setLocalizeLabelText:@"reward_info_text_3_2"],
                               [LABEL_MANAGER setLocalizeLabelText:@"reward_info_text_3_3"]
                               ],
                             @[[LABEL_MANAGER setLocalizeLabelText:@"reward_info_text_4_1"],
                               [LABEL_MANAGER setLocalizeLabelText:@"reward_info_text_4_2"],
                               [LABEL_MANAGER setLocalizeLabelText:@"reward_info_text_4_3"]
                               ],
                             @[[LABEL_MANAGER setLocalizeLabelText:@"reward_info_text_5_1"],
                               [LABEL_MANAGER setLocalizeLabelText:@"reward_info_text_5_2"],
                               [LABEL_MANAGER setLocalizeLabelText:@"reward_info_text_5_3"]
                             ]];

    NSArray *performaceTextArray;
    if (_corectAnswersNumber<=kParam022) {
        performaceTextArray = stringArray[0];
    }else if (_corectAnswersNumber<=kParam023) {
        performaceTextArray = stringArray[1];
    }else if (_corectAnswersNumber<=kParam024) {
        performaceTextArray = stringArray[2];
    }else if (_corectAnswersNumber<=kParam025) {
        performaceTextArray = stringArray[3];
    }else{
        performaceTextArray = stringArray[4];
    }
    uint32_t randomValue = arc4random_uniform([performaceTextArray count]);
    NSString *randomObject = [performaceTextArray objectAtIndex:randomValue];
    return randomObject;
}

- (void)updateLabels{
    vipScoreLabel.text = [NSString stringWithFormat:@"%d",currentProgressUserObject.vipPasses];
}

#pragma mark Interface functions

- (IBAction)gotoLesson:(id)sender {
    
    //long currentLesson = currentProgressLevelObject.currentUnit;
    currentProgressLevelObject.currentUnit = currentProgressLevelObject.maxUnit;
    [APP_DELEGATE buildLessonStack];
}

- (IBAction)goToHomePage:(id)sender {
    [AUDIO_MANAGER stopPlaying];
    [APP_DELEGATE buildHomePageStack];
}

- (IBAction)openUserZone:(id)sender {
    
    [APP_DELEGATE buildUserZoneStack];
}
- (IBAction)goToVipCategories:(id)sender {
    [APP_DELEGATE buildVipZoneStack];
    return;
    ///
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kVipScreen action:@"Click_on_VIP" additionalData:nil];
    
    [self.view bringSubviewToFront:loadingView];
    loadingView.hidden = NO;
    [activityIndicatorLoading startAnimating];
    
    [self addBaseActivityIndicator];
    id vipCourseObject = [DATA_MANAGER getVipCourseObject];
    if(vipCourseObject){
        //donwload resoureces
        NSArray *dataArray = [DATA_MANAGER getAllVipCategory];
        
        [CACHE_MANAGER setDelegate:self];
        [CACHE_MANAGER startDownloadingAndCachingResources:dataArray];
    }else{
        [self addBaseActivityIndicator];
        [self chekInternet];
        [REQUEST_MANAGER getDataFor:kRequestGetVipDataAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
            [self removeActivityIndicator];
            NSDictionary *responseDictionary = [responseObject objectForKey:kApiObjectResponse];
            [DATA_MANAGER makeVipCategoryCourseData:responseDictionary];
            [DATA_MANAGER saveData];
            [self goToVipCategories:0];
        } andWithErrorCallBack:^(NSString *errorMessage) {
            [self removeActivityIndicator];
        }];
        //makeVipCategoryCourseData
    }
    
}

#pragma mark - Cache Manager Delegate
- (void)cacheManagerFinishedDownloading:(id)sender {
    
    NSLog(@"========= Images were downloaded =========");
    
    [APP_DELEGATE buildVipZoneStack];
    
    [self removeActivityIndicator];
    loadingView.hidden = YES;
}


#pragma mark share FB
- (IBAction)shareFbPress1:(id)sender {
    
    /*FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
     content.contentURL = [NSURL URLWithString:@"https://developers.facebook.com"];
     */
    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    photo.image = nextLessonImageView.image;
    photo.userGenerated = YES;
    
    NSDictionary *properties = @{
                                 @"og:type": @"books.book",
                                 @"og:title": @"A Game of Thrones",
                                 @"og:description": @"In the frozen wastes to the north of Winterfell, sinister and supernatural forces are mustering.",
                                 @"books:isbn": @"0-553-57340-3",
                                 };
    FBSDKShareOpenGraphObject *object = [FBSDKShareOpenGraphObject objectWithProperties:properties];
    
    
    FBSDKShareOpenGraphAction *action = [[FBSDKShareOpenGraphAction alloc] init];
    action.actionType = @"books.reads";
    [action setObject:object forKey:@"books:book"];
    //[action setArray:@[photo] forKey:@"image"];
    
    FBSDKShareOpenGraphContent *content = [[FBSDKShareOpenGraphContent alloc] init];
    content.action = action;
    content.previewPropertyName = @"books:book";
    
    [FBSDKShareDialog showFromViewController:self
                                 withContent:content
                                    delegate:nil];
}

- (IBAction)shareFbPress:(id)sender {
    //from web example
    // Construct an FBSDKSharePhoto
    //https://developers.facebook.com/docs/sharing/opengraph/ios
    //FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    //photo.image = [UIImage imageNamed:@"07BokehBackground"];//nextLessonImageView.image;
    // Optionally set user generated to YES only if this image was created by the user
    // You must get approval for this capability in your app's Open Graph configuration
    // photo.userGenerated = YES;
    
    // Create an object
    NSDictionary *properties = @{
                                 @"og:type": @"books.book",
                                 @"og:title": @"A Game of Thrones",
                                 @"og:description": @"In the frozen wastes to the north of Winterfell, sinister and supernatural forces are mustering.",
                                 @"books:isbn": @"0-553-57340-3",
                                 @"og:image":@"http://i.imgur.com/XHhZxmB.png",
                                 };
    FBSDKShareOpenGraphObject *object = [FBSDKShareOpenGraphObject objectWithProperties:properties];
    
    // Create an action
    FBSDKShareOpenGraphAction *action = [[FBSDKShareOpenGraphAction alloc] init];
    action.actionType = @"books.reads";
    [action setObject:object forKey:@"books:book"];
    
    // Add the photo to the action. Actions
    // can take an array of images.
    //[action setArray:@[photo] forKey:@"image"];
    
    // Create the content
    FBSDKShareOpenGraphContent *content = [[FBSDKShareOpenGraphContent alloc] init];
    content.action = action;
    content.previewPropertyName = @"books:book";
    
    [FBSDKShareDialog showFromViewController:self
                                 withContent:content
                                    delegate:nil];
}
- (IBAction)shareFbPress3:(id)sender {
    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    photo.image = [UIImage imageNamed:@"07BokehBackground"];
    photo.userGenerated = YES;
    photo.caption = @"Your application must first be ....";
    FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
    content.photos = @[photo];
    [FBSDKShareDialog showFromViewController:self
                                 withContent:content
                                    delegate:nil];
}
- (IBAction)shareTwitterPress1:(id)sender {
    
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            NSLog(@"signed in as %@", [session userName]);
            [self addTwit];
        } else {
            NSLog(@"error: %@", [error localizedDescription]);
        }
    }];
}
-(void)addTwit{
    //Your application must first be whitelisted by Twitter in order to post a Tweet with an App Card.
    // Objective-C
    // Users must be logged-in to compose Tweets
    TWTRSession *session = [Twitter sharedInstance].sessionStore.session;
    
    // User generated image
    UIImage *image = [UIImage imageNamed:@"twitterimgEXMP.png"];
    
    // Create the card and composer
    TWTRCardConfiguration *card = [TWTRCardConfiguration appCardConfigurationWithPromoImage:image iPhoneAppID:@"976530925" iPadAppID:nil googlePlayAppID:nil];
    
    TWTRComposerViewController *composer = [[TWTRComposerViewController alloc] initWithUserID:session.userID cardConfiguration:card];
    
    // Optionally set yourself as the delegate
    composer.delegate = self;
    
    // Show the view controller
    [self presentViewController:composer animated:YES completion:nil];
    
}
- (void)composerDidSucceed:(TWTRComposerViewController *)controller withTweet:(TWTRTweet *)tweet{
    NSLog(@"composerDidSucceed");
}

- (void)composerDidFail:(TWTRComposerViewController *)controller withError:(NSError *)error{
    NSLog(@"composerDidFail");
}
- (IBAction)shareTwitterPress:(id)sender {
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    
    [composer setText:@"Your application must first be whitelisted by Twitter in order to post a Tweet with Card."];// Card.Now this is ok
    [composer setImage:[UIImage imageNamed:@"07BokehBackground"]];
    [composer setURL:[NSURL URLWithString:@"http://www.kantoo.com/KantooEnglish/en-index.html"]];
    // Called from a UIViewController
    [composer showFromViewController:self completion:^(TWTRComposerResult result) {
        if (result == TWTRComposerResultCancelled) {
            NSLog(@"Tweet composition cancelled");
        }
        else {
            NSLog(@"Sending Tweet!");
        }
    }];
}

- (IBAction)shareInstagramPress:(id)sender {
    [self instaGramWallPost];
    return;
    
    /*NSURL *instagramURL = [NSURL URLWithString:@"instagram://location?id=1"];
     if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
     [[UIApplication sharedApplication] openURL:instagramURL];
     }*/
}

-(void)instaGramWallPost
{
    //https://www.youtube.com/watch?v=C-spxq6ZmMk
    NSURL *myURL = [NSURL URLWithString:@"http://i.imgur.com/XHhZxmB.png"];
    NSData * imageData = [[NSData alloc] initWithContentsOfURL:myURL];
    UIImage *imgShare = [[UIImage alloc] initWithData:imageData];
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    
    if([[UIApplication sharedApplication] canOpenURL:instagramURL]) //check for App is install or not
    {
        UIImage *imageToUse = imgShare;
        NSString *documentDirectory=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *saveImagePath=[documentDirectory stringByAppendingPathComponent:@"Image.igo"];
        NSData *imageData=UIImagePNGRepresentation(imageToUse);
        [imageData writeToFile:saveImagePath atomically:YES];
        NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];
        self.documentController= [[UIDocumentInteractionController alloc]init];
        self.documentController = [UIDocumentInteractionController interactionControllerWithURL:imageURL];
        self.documentController.delegate = self;
        self.documentController.annotation = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"Testing for hollywood"], @"InstagramCaption", nil];
        self.documentController.UTI = @"com.instagram.exclusivegram";
        UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
        [self.documentController presentOpenInMenuFromRect:CGRectMake(1, 1, 1, 1) inView:vc.view animated:YES];
        
    }
    else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"Instagram not installed in this device!\nTo share image please install instagram." delegate:nil cancelButtonTitle:nil otherButtonTitles:[LABEL_MANAGER setLocalizeLabelText:@"ok"], nil];
        [alert show];
    }
}

-(void)instaGramWallPost2
{
    NSURL *myURL = [NSURL URLWithString:@"http://i.imgur.com/XHhZxmB.png"];
    NSData * imageData = [[NSData alloc] initWithContentsOfURL:myURL];
    UIImage *imgShare = [[UIImage alloc] initWithData:imageData];
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    
    if([[UIApplication sharedApplication] canOpenURL:instagramURL]) //check for App is install or not
    {
        UIImage *imageToUse = imgShare;
        NSString *documentDirectory=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *saveImagePath=[documentDirectory stringByAppendingPathComponent:@"slika.png"];
        NSData *imageData=UIImagePNGRepresentation(imageToUse);
        [imageData writeToFile:saveImagePath atomically:YES];
        NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];
        
        NSString *caption = @"Some Preloaded Caption";
        NSString *escapedString   = [self encodeToPercentEscapeString:[imageURL absoluteString]];
        NSString *escapedCaption  = [self encodeToPercentEscapeString:caption];
        NSURL *instagramURL2 = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?AssetPath=%@&InstagramCaption=%@",escapedString,escapedCaption]];
        if ([[UIApplication sharedApplication] canOpenURL:instagramURL2]) {
            [[UIApplication sharedApplication] openURL:instagramURL2];
        }
        
    }
    else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"Instagram not installed in this device!\nTo share image please install instagram." delegate:nil cancelButtonTitle:nil otherButtonTitles:[LABEL_MANAGER setLocalizeLabelText:@"ok"], nil];
        [alert show];
    }
}

-(void)instaGramWallPost1
{
    NSURL *myURL = [NSURL URLWithString:@"http://i.imgur.com/XHhZxmB.png"];
    NSData * imageData = [[NSData alloc] initWithContentsOfURL:myURL];
    UIImage *imgShare = [[UIImage alloc] initWithData:imageData];
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    
    if([[UIApplication sharedApplication] canOpenURL:instagramURL]) //check for App is install or not
    {
        __block NSString* localId;
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:imgShare];
            
            localId = [[assetChangeRequest placeholderForCreatedAsset] localIdentifier];
            
        } completionHandler:^(BOOL success, NSError *error) {
            if (success) {
                NSLog(@"Success %@",localId);
                //PHFetchResult* assetResult = [PHAsset fetchAssetsWithLocalIdentifiers:@[localId] options:nil];
                //PHAsset *asset = [assetResult firstObject];
                //[[PHImageManager defaultManager] requestImageDataForAsset:asset options:nil resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                
                NSString *caption = @"Some Preloaded Caption";
                //NSURL *newURL = [info valueForKey:@"PHImageFileURLKey"];
                //NSString *escapedString   = [self encodeToPercentEscapeString:[newURL absoluteString]];
                //NSString *escapedString   = [self encodeToPercentEscapeString:@"/private/var/mobile/Media/DCIM/101APPLE/IMG_1078.PNG"];
                
                //NSString *escapedCaption  = [self encodeToPercentEscapeString:caption];
                //
                //NSURL *instagramURL2 = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://media?id=%@",localId]];
                
                NSURL *instagramURL2 = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?LocalIdentifier=%@?name=Kirca",localId]];
                //NSURL *instagramURL2 = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?AssetPath=%@&InstagramCaption=%@",escapedString,escapedCaption]];
                if ([[UIApplication sharedApplication] canOpenURL:instagramURL2]) {
                    [[UIApplication sharedApplication] openURL:instagramURL2];
                }
                
                //}
                //];
            }
            else {
                NSLog(@"write error : %@",error);
            }
        }];
        
    }
    else {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"Instagram not installed in this device!\nTo share image please install instagram." delegate:nil cancelButtonTitle:nil otherButtonTitles:[LABEL_MANAGER setLocalizeLabelText:@"ok"], nil];
        [alert show];
    }
}
#pragma mark helpForString
- (NSString *)encodeToPercentEscapeString:(NSString *)string {
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)string, NULL,
                                                                                 (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ", kCFStringEncodingUTF8));
}
@end
