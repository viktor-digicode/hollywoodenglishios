//
//  SettingsDiplomasCollectionViewCell.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/25/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsDiplomasCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *circleRankImageView;
@property (weak, nonatomic) IBOutlet UILabel *rankLabel;
@property (weak, nonatomic) IBOutlet UIImageView *diplomaDesignRankImageView;

typedef void (^ResponseBlock)(UIImage *image, NSError *error);
- (void)getImageFromURL:(NSURL *)url withCompletionHandler:(ResponseBlock)completionBlock;
- (BOOL)checkMaxRankForRow:(NSInteger)row;
- (UIImage *)makeSaturationForImage:(UIImage *)image;

@end
