//
//  SettingsDiplomasViewController.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/25/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsDiplomasViewController : LMBaseViewController
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *userVipPassesLabel;
@property (weak, nonatomic) IBOutlet UILabel *userVipScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *userScoreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewUserRank;

@end
