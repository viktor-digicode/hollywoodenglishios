//
//  VipQuizzesCollectionViewCell.h
//  Hollywood
//
//  Created by Kiril Kiroski on 10/7/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VipQuizObject+CoreDataProperties.h"

@interface VipQuizzesCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cellImageView;
@property (weak, nonatomic) IBOutlet UIImageView *lockImageView;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *vipPointsLabel;

@property (weak, nonatomic) IBOutlet UIImageView *elipseImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoLabelWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *yellowImageViewWidthConstraint;

- (void)setVipQuizData:(VipQuizObject*)vipQuizData;

@end
