//
//  SelectCountryViewController.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/12/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "SelectCountryViewController.h"
#import "LMRequestManager.h"
#import "PackageViewController.h"


@interface SelectCountryViewController ()<CacheManagerDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate> {

    __strong ProgressUserObject *currentProgressUserObject;
    __weak IBOutlet UIView *loadingView;
    UIPickerView *pickerView;
}

@end

@implementation SelectCountryViewController

- (void)configureUI{
    
    [super configureUI];
    DLog(@"no_internet_access %@", Localized(@"no_internet_access"));
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    
    
    self.userVipPassesLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];
    
    self.navigationController.navigationBarHidden = YES;
    
    /*self.imageViewUserProfile.layer.cornerRadius = 4.0;
    self.imageViewUserProfile.layer.borderWidth = 1.0;
    self.imageViewUserProfile.layer.borderColor = [UIColor blackColor].CGColor;
    self.imageViewUserProfile.layer.masksToBounds = YES;*/
    
    self.imageViewUserProfile.image = [USER_MANAGER takeCurrentUserImage];
    
    self.userVipPassesLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:15.0];
    self.vipStaticTextLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:18.0];
    [self setupCountryTextField];
    
    self.selectCountryTextField.placeholder = [LABEL_MANAGER setLocalizeLabelText:@"select_country_label"];
    self.msisdnTextField.placeholder = [LABEL_MANAGER setLocalizeLabelText:@"your_mobile_number_label"];
    self.titleTextLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"select_country_info"];
    self.vipStaticTextLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"VIP"];
    
    if(STAGING == 1){
        self.msisdnTextField.text = @"";
        self.prefixTextField.text = @"";
    }
    
    self.selectCountryTextField.text = self.countryPickerArray[0];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self.view.layer removeAllAnimations];
    [super viewDidDisappear:animated];
}


//#pragma mark Orientations
//
//- (UIInterfaceOrientationMask)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskLandscape;
//}


#pragma mark - Open lesson

- (IBAction)openUserZone:(id)sender
{
    [APP_DELEGATE buildUserZoneStack];
}

- (IBAction)homeButtonTapped:(id)sender
{
    [APP_DELEGATE buildHomePageStack];
}

- (IBAction)vipButtonTapped:(id)sender
{
    id vipCourseObject = [DATA_MANAGER getVipCourseObject];
    if(vipCourseObject){
        //donwload resoureces
        NSArray *dataArray = [DATA_MANAGER getAllVipCategory];
        
        [CACHE_MANAGER setDelegate:self];
        [CACHE_MANAGER startDownloadingAndCachingResources:dataArray];
    }else{
        [self addBaseActivityIndicator];
        [self chekInternet];
        [REQUEST_MANAGER getDataFor:kRequestGetVipDataAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
            [self removeActivityIndicator];
            NSDictionary *responseDictionary = [responseObject objectForKey:kApiObjectResponse];
            [DATA_MANAGER makeVipCategoryCourseData:responseDictionary];
            [DATA_MANAGER saveData];
            [self vipButtonTapped:0];
        } andWithErrorCallBack:^(NSString *errorMessage) {
            [self removeActivityIndicator];
        }];
    }
}


#pragma mark - Cache Manager Delegate
- (void)cacheManagerFinishedDownloading:(id)sender {
    
    NSLog(@"========= Images were downloaded =========");
    
    [APP_DELEGATE buildVipZoneStack];
    
}




#pragma mark - Setup methods

- (void)setupCountryTextField
{
    self.countryPickerArray = @[@"Brasil"];
    self.msisdnTextField.delegate = self;
    self.prefixTextField.delegate = self;
    self.selectCountryTextField.rightViewMode = UITextFieldViewModeAlways;
    UIImageView *rightImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"noun796798CcCopy2"]];
    self.selectCountryTextField.rightView = rightImageView;
    
    pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 50, 100, 150)];
    [pickerView setDataSource: self];
    [pickerView setDelegate: self];
    pickerView.showsSelectionIndicator = YES;
    
    UIToolbar *toolBar = [[UIToolbar alloc] init];
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.translucent = YES;
    [toolBar sizeToFit];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePicker)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action: @selector(cancelPicker)];
    
    [toolBar setItems:@[cancelButton, space, doneButton] animated:NO];
    toolBar.userInteractionEnabled = YES;
    toolBar.tintColor = [UIColor grayColor];

    self.selectCountryTextField.inputView = pickerView;
    self.selectCountryTextField.inputAccessoryView = toolBar;
    
    UITapGestureRecognizer *tapToSelect = [[UITapGestureRecognizer alloc]initWithTarget: self
                                                                                 action: @selector(tappedToSelectRow:)];
    [pickerView addGestureRecognizer:tapToSelect];
    tapToSelect.delegate = self;
}


#pragma mark - Action methods

- (IBAction)tappedToSelectRow:(UITapGestureRecognizer *)tapRecognizer
{
    NSInteger selectedRow = [pickerView selectedRowInComponent:0];
    [self pickerView: pickerView didSelectRow:selectedRow inComponent:0];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return true;
}

#pragma mark - UIPickerView DataSource & Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.countryPickerArray.count;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component __TVOS_PROHIBITED
{
    return self.countryPickerArray[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component __TVOS_PROHIBITED
{
    self.selectCountryTextField.text = self.countryPickerArray[row];
    [self cancelPicker];
}


#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.msisdnTextField)
    {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:nil];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        if (numberOfMatches == 0)
        {
            return NO;
        }
        
        if (self.msisdnTextField.text.length >= 9 && [string length] > 0)
            return NO;
        
        return YES;
    }
    else if (textField == self.prefixTextField)
    {
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,2})?)?$";
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive error:nil];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString options:0 range:NSMakeRange(0, [newString length])];
        if (numberOfMatches == 0)
        {
            return NO;
        }

        if (self.prefixTextField.text.length + [string length] >= 2 && [string length] > 0) {
         
            if (self.prefixTextField.text.length < 2)
                self.prefixTextField.text = [self.prefixTextField.text stringByAppendingString:string];
            [self.msisdnTextField becomeFirstResponder];
            return NO;
        }
        return YES;
    }

    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.prefixTextField) {
        [self.msisdnTextField becomeFirstResponder];
    }
    return YES;
}

- (BOOL)chekString:(NSString*)stringForValidation{
    if(stringForValidation.length == 11){
        return YES;
    }
    return NO;
}

- (void)showAlert:(NSString*)stringForSubtitle{
    FCAlertView *alert = [[FCAlertView alloc] init];
    alert.alertBackgroundColor = [UIColor colorWithRed:112.0f/255.0f green:102.0f/255.0f blue:55.0f/255.0f alpha:1.0];
    
    [alert showAlertInView:self
                 withTitle:@""
              withSubtitle:stringForSubtitle
           withCustomImage:nil
       withDoneButtonTitle:nil
                andButtons:nil];
    
    alert.backgroundColor = [UIColor clearColor];
    alert.colorScheme = [UIColor blackColor];
    alert.doneButtonTitleColor = [UIColor whiteColor];
    alert.hideSeparatorLineView = YES;
    alert.blurBackground = NO;
    if([stringForSubtitle isEqualToString:[LABEL_MANAGER setLocalizeLabelText:@"error_message_3"]]){
        [alert doneActionBlock:^{
            [self openPackageScreen];
        }];
    }
    if([stringForSubtitle isEqualToString:[LABEL_MANAGER setLocalizeLabelText:@"error_message_2"]]){
        [alert doneActionBlock:^{
            [APP_DELEGATE buildHomePageStack];
        }];
    }
    
}

- (void)cancelPicker
{
    [self.selectCountryTextField resignFirstResponder];
}

- (void)donePicker
{
    NSInteger selectedRow = [pickerView selectedRowInComponent:0];
    [self pickerView: pickerView didSelectRow:selectedRow inComponent:0];
    [self cancelPicker];
}


#pragma mark - Interface functions

- (IBAction)submitButtonTapped:(id)sender
{
    if(![self chekString:[NSString stringWithFormat:@"%@%@", self.prefixTextField.text, self.msisdnTextField.text]]){
        [self.view endEditing:YES];
        // [self showAlert:[LABEL_MANAGER setLocalizeLabelText:@"error_message_3"]];
        //[self showAlert:@"O número de telefone inserido não é um número válido"];
        [self showAlert:@"Por favor, insira um número de celular válido"];
        return;
    }
    
        NSDictionary *dictHeaders = @{
                                      kUserMSISDN : [NSString stringWithFormat:@"55%@%@", self.prefixTextField.text, self.msisdnTextField.text],
                                      kProgressVipPasses : [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses],
                                      kProgressScore : [NSString stringWithFormat:@"%d", currentProgressUserObject.score]
                                      };
        [self addBaseActivityIndicator];
    [self chekInternet];
        [REQUEST_MANAGER getDataFor:kRequestIsMsisdnExistAPICall headers:dictHeaders withSuccessCallBack:^(NSDictionary *responseObject) {
            [self.view endEditing:YES];
            [self removeActivityIndicator];
            NSDictionary *responseData = [responseObject objectForKey:kApiObjectResponse];
            NSDictionary *userDataView = [responseData objectForKey:kUserDataView];
            [USER_MANAGER setUserMSISD:[NSString stringWithFormat:@"55%@%@", self.prefixTextField.text, self.msisdnTextField.text]];
            if(userDataView){
                [USER_MANAGER setUserData:userDataView];
            }
            [USER_MANAGER setUserApplicationId:[[responseObject objectForKey:@"response"] objectForKey:kUserApplicationId]];
            if ([USER_MANAGER getUserStatus] == KAUserStatusActive) {
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setBool:NO forKey:@"openLessonSession"];
                [defaults synchronize];
                [self.navigationController popViewControllerAnimated:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"backFromConversionScreen" object:nil];
//                [APP_DELEGATE buildHomePageStack];
            }
            else if ([USER_MANAGER getUserStatus] == KAUserStatusCanceled){
                //[self openPackageScreen];
                [self showAlert:[LABEL_MANAGER setLocalizeLabelText:@"error_message_3"]];
            }
            else if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled){
                //[self openPackageScreen];
                [self showAlert:[LABEL_MANAGER setLocalizeLabelText:@"error_message_2"]];
            }
            else {
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setBool:NO forKey:@"openLessonSession"];
                [defaults synchronize];
                [self.navigationController popViewControllerAnimated:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"activateCurrentVehicle" object:nil];


//                [APP_DELEGATE buildHomePageStack];
            }
        } andWithErrorCallBack:^(NSString *errorMessage) {
            [self removeActivityIndicator];
            [self openPackageScreen];
        }];
}

-(void)openPackageScreen{
    PackageViewController *controller = [PackageViewController new];
    [self.navigationController pushViewController:controller animated:YES];
}
@end
