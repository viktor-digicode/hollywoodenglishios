//
//  UserStatusViewController.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 10/20/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "UserStatusViewController.h"
#import "LMDataManeger.h"

@interface UserStatusViewController () {
    
    __weak IBOutlet UIButton *playAsUserButton;
    __weak IBOutlet UIButton *playAsGuestButton;
    
}

@end

@implementation UserStatusViewController

-(void)configureUI{
    
    [super configureUI];
    self.navigationController.navigationBarHidden = YES;
}

- (void)loadData {
    //NSLog(@"UserStatusViewController >>> loadData");
    [super loadData];
    //[self addActivityIndicator];
    //Points, vip passes
    [self addBaseActivityIndicator];
    [self chekInternet];
    [REQUEST_MANAGER getDataFor:kRequestFirstLaunchAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
        //NSLog(@"getDataFor:kRequestFirstLaunchAPICall >>> loadData");
        [DATA_MANAGER initializeStartData:responseObject];
        NSDictionary *responseDictonary = [responseObject valueForKey:kApiObjectResponse];
        [PARAMETER_MANAGER initializeStartData:responseDictonary];
        NSNumber *courseRevision = [responseDictonary valueForKey:@"revision"];
        NSNumber *courseVersion = [responseDictonary valueForKey:@"version"];
        
        
        NSDictionary *labelsDictionary = [responseDictonary valueForKey: kApiObjectLabels];
        NSNumber *labelVersion = [labelsDictionary valueForKey:kUserLabelVersion];
        
        NSDictionary *appParametersDictionary = [responseDictonary valueForKey:kApiObjectAppParameters];
        NSNumber *parametersVersion = [appParametersDictionary valueForKey:kParameterVersion];
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        [userDefault setObject:courseRevision forKey:kUserCourseRevision];
        [userDefault setObject:courseVersion forKey:kUserCourseVersion];
        [userDefault setObject:labelVersion forKey:kUserLabelVersion];
        [userDefault setObject:parametersVersion forKey:kParameterVersion];
        
        [self removeActivityIndicator];
        [self playAsGuestAction:0];
    } andWithErrorCallBack:^(NSString *errorMessage) {
        [self removeActivityIndicator];
    }];
    
    return;
}
#pragma mark - Button actions

- (IBAction)playAsUserAction:(id)sender {
    
    [USER_MANAGER loginDummyUser];
    [APP_DELEGATE buildHomePageStack];
    
}

- (IBAction)playAsGuestAction:(id)sender {
    [self addBaseActivityIndicator];
    [self chekInternet];
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kGuestUserScreen action:@"Call_Create_Guest_User" additionalData:nil];

    [REQUEST_MANAGER getDataFor:kCreateGuestUser headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
        [self removeActivityIndicator];
        NSLog(@"Response: Application user id - %@", [[responseObject objectForKey:@"response"] objectForKey:kUserApplicationId]);
        
        //[USER_MANAGER setGuestStatus];
        [USER_MANAGER initializeWithGuest];
        [USER_MANAGER setUserApplicationId:[[responseObject objectForKey:@"response"] objectForKey:kUserApplicationId]];
        
        [APP_DELEGATE buildHomePageStack];
        
        [ANALYTICS_MANAGER sendAnalyticsForEvent:kGuestUserScreen action:@"Create_Guest_User" additionalData:@"Success"];
        
    } andWithErrorCallBack:^(NSString *errorMessage) {
        
        [ANALYTICS_MANAGER sendAnalyticsForEvent:kGuestUserScreen action:@"Create_Guest_User" additionalData:@"Failure"];

        [self removeActivityIndicator];
        
    }];
    
    
}

#pragma mark Orientations
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskPortrait;
    
}

@end
