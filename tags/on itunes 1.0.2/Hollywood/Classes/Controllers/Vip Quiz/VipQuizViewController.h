//
//  VipQuizViewController.h
//  Hollywood
//
//  Created by Kiril Kiroski on 10/12/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "LMBaseViewController.h"
#import "VipQuizObject+CoreDataClass.h"

@interface VipQuizViewController : LMBaseViewController

@property (nonatomic, assign) VipCategoryObject *currentVipCategoryObject;

- (void)setVipQuizObject:(VipQuizObject*)tempVipQuizObject;

@end
