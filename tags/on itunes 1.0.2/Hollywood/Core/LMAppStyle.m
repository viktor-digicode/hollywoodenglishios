//  AppStyle.m


#define BUTTONS_STYLE_PLIST @"Style_Buttons"
#define FONTS_STYLE_PLIST @"Style_Fonts"
#define COLORS_STYLE_PLIST @"Style_Colors"
#define __SELECTED__ @"_Selected"

@implementation AppStyle

SINGLETON_GCD(AppStyle)

- (id)init
{
  self = [super init];
  if (self) {
  }
  return self;
}

- (NSMutableDictionary *)buttonsDictionary
{
  if (!_buttonsDictionary) {
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:BUTTONS_STYLE_PLIST ofType:@"plist"];
    _buttonsDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
  }
  return _buttonsDictionary;
}

- (NSMutableDictionary *)fontsDictionary
{
  if (!_fontsDictionary) {
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:FONTS_STYLE_PLIST ofType:@"plist"];
    _fontsDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
  }
  return _fontsDictionary;
}

- (NSMutableDictionary *)colorsDictionary
{
  if (!_colorsDictionary) {
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:COLORS_STYLE_PLIST ofType:@"plist"];
    _colorsDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
  }
  return _colorsDictionary;
}

- (NSMutableDictionary *)buttonStyleDictionaryForType:(NSString *)buttonType
{
  NSMutableDictionary *dict = [self.buttonsDictionary objectForKey:buttonType];
  
  return dict;
}

- (NSMutableDictionary *)fontStyleDictionaryForType:(NSString *)textType
{
  NSMutableDictionary *dict = [self.fontsDictionary objectForKey:textType];
  
  return dict;
}

- (NSMutableDictionary *)colorStyleDictionaryForType:(NSString *)colorType
{
  NSMutableDictionary *dict = [self.colorsDictionary objectForKey:colorType];
  if (!dict) {
    dict = [self.fontsDictionary objectForKey:colorType];
  }
  if (!dict) {
    dict = [self.buttonsDictionary objectForKey:colorType];
  }
  return dict;
}

- (UIFont *)fontForType:(NSString *)textType
{
  NSDictionary *styleDict = [self fontStyleDictionaryForType:textType];
  return [UIFont fontWithName:styleDict[FONT_KEY] size:[styleDict[FONT_SIZE_KEY] floatValue]];
}

- (UIColor *)colorForType:(NSString *)textType
{
  // changed from pictoinary
  NSDictionary *styleDict = [self colorStyleDictionaryForType:textType];
  float_t opacity = [styleDict[OPACITY_KEY] floatValue];
  if (0 == opacity) {
    return [UIColor blackColor];
  } else {
    float_t red = [styleDict[COLOR_RED_KEY] floatValue] / 255.;
    float_t green = [styleDict[COLOR_GREEN_KEY] floatValue] / 255.;
    float_t blue = [styleDict[COLOR_BLUE_KEY] floatValue] / 255.0;
    return [UIColor colorWithRed:red green:green blue:blue alpha:opacity];
  }
}

- (UIColor *)shadowColorForType:(NSString *)textType
{
  NSDictionary *styleDict = [self fontStyleDictionaryForType:textType];
  float_t opacity = [styleDict[SHADOW_OPACITY_KEY] floatValue];
  if (0 == opacity) {
    return [UIColor clearColor];
  } else {
    float_t red = [styleDict[SHADOW_COLOR_RED_KEY] floatValue] / 255.;
    float_t green = [styleDict[SHADOW_COLOR_GREEN_KEY] floatValue] / 255.;
    float_t blue = [styleDict[SHADOW_COLOR_BLUE_KEY] floatValue] / 255.0;
    return [UIColor colorWithRed:red green:green blue:blue alpha:opacity];
  }
}

- (CGSize)shadowOffsetForType:(NSString *)textType
{
  NSDictionary *styleDict = [self fontStyleDictionaryForType:textType];
  float_t opacity = [styleDict[SHADOW_OPACITY_KEY] floatValue];
  if (0 == opacity) {
    return CGSizeZero;
  } else {
    return CGSizeMake([styleDict[SHADOW_OFFSET_WIDTH_KEY] floatValue], [styleDict[SHADOW_OFFSET_HEIGHT_KEY] floatValue]);
  }
}

#pragma mark - Control Styles

- (void)applyStyle:(NSString *)style toLabel:(UILabel *)label
{
  __strong NSMutableDictionary *dict = [APPSTYLE fontStyleDictionaryForType:style];
  label.font = style_font(FONT_KEY, FONT_SIZE_KEY);
  label.textColor = style_rgba(FONT_COLOR_RED_KEY, FONT_COLOR_GREEN_KEY, FONT_COLOR_BLUE_KEY, FONT_OPACITY_KEY);
  label.shadowColor = style_rgba(SHADOW_COLOR_RED_KEY, SHADOW_COLOR_GREEN_KEY, SHADOW_COLOR_BLUE_KEY, SHADOW_OPACITY_KEY);
  label.shadowOffset = style_size(SHADOW_OFFSET_WIDTH_KEY, SHADOW_OFFSET_HEIGHT_KEY);
}

- (void)applyBorderColor:(UIColor *)color toButton:(UIButton *)button
{
  button.backgroundColor = [UIColor clearColor];
  button.layer.cornerRadius = 4.0;
  [button.layer setMasksToBounds:YES];
  button.layer.borderWidth = 1;
  button.layer.borderColor = [color CGColor];
}

- (void)applyStyle:(NSString *)style toButton:(UIButton *)button
{
  __strong NSMutableDictionary *buttonDict = [APPSTYLE buttonStyleDictionaryForType:style];
  __strong NSDictionary *dict = [APPSTYLE fontsDictionary][buttonDict[@"style_fonts"]];
  
  button.titleLabel.font = style_font(FONT_KEY, FONT_SIZE_KEY);
  button.titleEdgeInsets =
  style_inset_button(BUTTON_TITLE_INSET_TOP_KEY, BUTTON_TITLE_INSET_LEFT_KEY, BUTTON_TITLE_INSET_BOTTOM_KEY, BUTTON_TITLE_INSET_RIGHT_KEY);
  [button setTitleColor:style_rgba(FONT_COLOR_RED_KEY, FONT_COLOR_GREEN_KEY, FONT_COLOR_BLUE_KEY, FONT_OPACITY_KEY) forState:UIControlStateNormal];
  [button setTitleShadowColor:style_rgba(SHADOW_COLOR_RED_KEY, SHADOW_COLOR_GREEN_KEY, SHADOW_COLOR_BLUE_KEY, SHADOW_OPACITY_KEY)
                     forState:UIControlStateNormal];
  button.titleLabel.shadowOffset = style_size(SHADOW_OFFSET_WIDTH_KEY, SHADOW_OFFSET_HEIGHT_KEY);
  
  if (buttonDict[BUTTON_BG]) {
    UIImage *imageNormal = [[UIImage imageNamed:buttonDict[BUTTON_BG]] resizableImageWithCapInsets:(UIEdgeInsets){ 8, 8, 8, 8 }];
    UIImage *imageSelected = [UIImage imageNamed:[buttonDict[BUTTON_BG] stringByAppendingString:BUTTON_SELECTED_SUFFIX]];
    if (imageSelected) {
      imageSelected = [imageSelected resizableImageWithCapInsets:(UIEdgeInsets){ 8, 8, 8, 8 }];
    }
    
    if (imageSelected) {
      [button setBackgroundImage:imageNormal forState:UIControlStateNormal];
      [button setBackgroundImage:imageSelected forState:UIControlStateHighlighted];
      [button setBackgroundImage:imageSelected forState:UIControlStateSelected];
    } else {
      [button setBackgroundImage:imageNormal forState:UIControlStateNormal];
      [button setBackgroundImage:imageNormal forState:UIControlStateHighlighted];
      [button setBackgroundImage:imageNormal forState:UIControlStateSelected];
    }
  }
}

- (void)applyStyle:(NSString *)style toTextView:(UITextView *)textView
{
  __strong NSMutableDictionary *dict = [APPSTYLE fontStyleDictionaryForType:style];
  textView.font = style_font(FONT_KEY, FONT_SIZE_KEY);
  textView.textColor = style_rgba(FONT_COLOR_RED_KEY, FONT_COLOR_GREEN_KEY, FONT_COLOR_BLUE_KEY, FONT_OPACITY_KEY);
  textView.layer.shadowColor = [style_rgba(SHADOW_COLOR_RED_KEY, SHADOW_COLOR_GREEN_KEY, SHADOW_COLOR_BLUE_KEY, SHADOW_OPACITY_KEY) CGColor];
  textView.layer.shadowOffset = style_size(SHADOW_OFFSET_WIDTH_KEY, SHADOW_OFFSET_HEIGHT_KEY);
}

- (void)applyStyle:(NSString *)style toTextField:(UITextField *)textField
{
  __strong NSMutableDictionary *dict = [APPSTYLE fontStyleDictionaryForType:style];
  textField.font = style_font(FONT_KEY, FONT_SIZE_KEY);
  textField.textColor = style_rgba(FONT_COLOR_RED_KEY, FONT_COLOR_GREEN_KEY, FONT_COLOR_BLUE_KEY, FONT_OPACITY_KEY);
  textField.layer.shadowColor = [style_rgba(SHADOW_COLOR_RED_KEY, SHADOW_COLOR_GREEN_KEY, SHADOW_COLOR_BLUE_KEY, SHADOW_OPACITY_KEY) CGColor];
  textField.layer.shadowOffset = style_size(SHADOW_OFFSET_WIDTH_KEY, SHADOW_OFFSET_HEIGHT_KEY);
}

- (void)applyBackground:(NSString *)background toButton:(UIButton *)button
{
  UIImage *normalImage = [UIImage imageNamed:background];
  UIImage *highlightedImage = [UIImage imageNamed:[background stringByAppendingString:__SELECTED__]];
  if (normalImage) {
    [button setBackgroundImage:normalImage forState:UIControlStateNormal];
  }
  if (highlightedImage) {
    [button setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
    [button setBackgroundImage:highlightedImage forState:UIControlStateSelected];
  } else if (normalImage) {
    [button setBackgroundImage:normalImage forState:UIControlStateHighlighted];
    [button setBackgroundImage:normalImage forState:UIControlStateSelected];
  }
}

- (void)applyIcon:(NSString *)icon toButton:(UIButton *)button
{
  UIImage *normalImage = [UIImage imageNamed:icon];
  UIImage *highlightedImage = [UIImage imageNamed:[icon stringByAppendingString:__SELECTED__]];
  if (normalImage) {
    [button setImage:normalImage forState:UIControlStateNormal];
  }
  if (highlightedImage) {
    [button setImage:highlightedImage forState:UIControlStateHighlighted];
    [button setImage:highlightedImage forState:UIControlStateSelected];
  } else if (normalImage) {
    [button setImage:normalImage forState:UIControlStateHighlighted];
    [button setImage:normalImage forState:UIControlStateSelected];
  }
}

- (void)applyTitle:(NSString *)title toButton:(UIButton *)button
{
  [button setTitle:title forState:UIControlStateNormal];
  [button setTitle:title forState:UIControlStateHighlighted];
  [button setTitle:title forState:UIControlStateSelected];
}

- (UIColor *)otherViewColorForKey:(NSString *)key
{
  NSDictionary *styleDict = self.colorsDictionary;
  
  if (!styleDict) {
    return [UIColor whiteColor];
  }
  
  NSDictionary *dictionary = [styleDict valueForKey:key];
  
  if (!dictionary) {
    NSLog(@"%@: style %@ not found", [self.class description], key);
    return [UIColor whiteColor];
  }
  
  return [UIColor colorWithRed:[(NSNumber *)[dictionary valueForKey:@"color_red"] floatValue] / 255.0
                         green:[(NSNumber *)[dictionary valueForKey:@"color_green"] floatValue] / 255.0
                          blue:[(NSNumber *)[dictionary valueForKey:@"color_blue"] floatValue] / 255.0
                         alpha:[(NSNumber *)[dictionary valueForKey:@"color_opacity"] floatValue]];
}

- (NSString *)htmlStyleForKey:(NSString *)key fontSizePlaceholder:(NSString *)fontSizePlaceholder
{
  NSDictionary *styleDict = [self fontsDictionary];
  
  if (!styleDict) {
    return @"";
  }
  
  NSDictionary *dictionary = [styleDict valueForKey:key];
  
  if (!dictionary) {
    NSLog(@"%@: style %@ not found", [self.class description], key);
    return @"";
  }
  
  NSString *fontFamily = [dictionary valueForKey:@"font"];
  NSString *fontColor =
  [NSString stringWithFormat:@"rgba(%@,%@,%@,%@)", [dictionary valueForKey:@"font_color_red"], [dictionary valueForKey:@"font_color_green"],
   [dictionary valueForKey:@"font_color_blue"], [dictionary valueForKey:@"font_color_opacity"]];
  
  if (!fontSizePlaceholder) {
    fontSizePlaceholder = [dictionary valueForKey:@"font_size"];
  }
  
  NSString *shadowOffsetX = [dictionary valueForKey:@"shadow_offset_width"];
  NSString *shadowOffsetY = [dictionary valueForKey:@"shadow_offset_height"];
  NSString *shadowColor =
  [NSString stringWithFormat:@"rgba(%@,%@,%@,%@)", [dictionary valueForKey:@"shadow_color_red"], [dictionary valueForKey:@"shadow_color_green"],
   [dictionary valueForKey:@"shadow_color_blue"], [dictionary valueForKey:@"shadow_color_opacity"]];
  
  return [NSString stringWithFormat:@"font-family:%@ !important;color:%@;font-size:%@px;text-shadow:%@px %@px %@", fontFamily, fontColor, fontSizePlaceholder,
          shadowOffsetX, shadowOffsetY, shadowColor];
}

@end
