//
//  LMParameterList.h
//  Hollywood
//
//  Created by Kiril Kiroski on 7/20/16.
//  Copyright © 2016 aa. All rights reserved.
//


//#ifndef EnglishCourse_LMParameterList_h
//#define EnglishCourse_LMParameterList_h

//Scoring
//Correct answer	Param-001
//Incorrect answer	Param-002

#define kParam001 [[PARAMETER_MANAGER  getLocalizeParameterText:@"correctAnswerScore"] intValue]// 100
#define kParam002 [[PARAMETER_MANAGER  getLocalizeParameterText:@"incorrectAnswerScore"] intValue]//20
#define kParam003 [[PARAMETER_MANAGER  getLocalizeParameterText:@"noStarStepPercent"] intValue]//19
#define kParam004 [[PARAMETER_MANAGER  getLocalizeParameterText:@"oneStarRequiredPercent"] intValue]//50
#define kParam005 [[PARAMETER_MANAGER  getLocalizeParameterText:@"twoStarRequiredPercent"] intValue]//89
#define kParam006 [[PARAMETER_MANAGER  getLocalizeParameterText:@"threeStarRequiredPercent"] intValue]
#define kParam007 [[PARAMETER_MANAGER  getLocalizeParameterText:@"earnVipPassesPointsTarget"] intValue]//150
#define kParam008 [[PARAMETER_MANAGER  getLocalizeParameterText:@"correctHighlightSeconds"] intValue]//1.5
#define kParam009 [[PARAMETER_MANAGER  getLocalizeParameterText:@"incorrectHighlightSeconds"] intValue]//1
#define kParam010 [[PARAMETER_MANAGER  getLocalizeParameterText:@"maxIncorrectTries"] intValue]
#define kParam011 [[PARAMETER_MANAGER  getLocalizeParameterText:@"spellingMaxIncorrectTries"] intValue]
#define kParam012 [[PARAMETER_MANAGER  getLocalizeParameterText:@"fillMissingWordsMaxIncorrectTries"] intValue]//2
#define kParam013 [[PARAMETER_MANAGER  getLocalizeParameterText:@"vocabPopupDisplaySeconds"] intValue]
//The user will be assigned with Param-014 VIP Passes
//Param-007 points, the application will raise the VIP Passes count by Param-015.
#define kParam014 [[PARAMETER_MANAGER  getLocalizeParameterText:@"firstLaunchVipPasses"] intValue]//7
#define kParam015 [[PARAMETER_MANAGER  getLocalizeParameterText:@"vipPassesEarnedOnPointsTarget"] intValue]//5

#define kParam016 [[PARAMETER_MANAGER  getLocalizeParameterText:@"getReadyCountdownSeconds"] intValue]
#define kParam017 [[PARAMETER_MANAGER  getLocalizeParameterText:@"maxUnsubscribedUsers"] intValue]
#define kParam018 [[PARAMETER_MANAGER  getLocalizeParameterText:@"hideVideoProgressBarAfterSeconds"] intValue]
#define kParam019 [[PARAMETER_MANAGER  getLocalizeParameterText:@"fillMissingWordsShowCorrectAnswerSeconds"] intValue]//2
#define kParam020 [[PARAMETER_MANAGER  getLocalizeParameterText:@""] intValue]
#define kParam021 [[PARAMETER_MANAGER  getLocalizeParameterText:@""] intValue]

#define kParam022 [[PARAMETER_MANAGER  getLocalizeParameterText:@"quizSummaryLowestPerformance"] intValue]//1
#define kParam023 [[PARAMETER_MANAGER  getLocalizeParameterText:@"quizSummaryLowPerformance"] intValue]//5
#define kParam024 [[PARAMETER_MANAGER  getLocalizeParameterText:@"quizSummaryMediumPerformance"] intValue]//7
#define kParam025 [[PARAMETER_MANAGER  getLocalizeParameterText:@"quizSummaryGoodPerformance"] intValue]//9
#define kParam026 [[PARAMETER_MANAGER  getLocalizeParameterText:@"quizSummaryExcellentPerformance"] intValue]//10
#define kParam027 [[PARAMETER_MANAGER  getLocalizeParameterText:@"voiceVehicleRecordingTimeout"] intValue]//10

#define kParam028 [[PARAMETER_MANAGER  getLocalizeParameterText:@"GooglePlayBasicPackagePrice"] intValue]
#define kParam029 [[PARAMETER_MANAGER  getLocalizeParameterText:@"GooglePlayBestPackagePrice"] intValue]
#define kParam030 [[PARAMETER_MANAGER  getLocalizeParameterText:@"GooglePlayMostProfitablePackagePrice"] intValue]

//#endif
