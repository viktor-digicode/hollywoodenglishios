//  AppMacros.h
//  Created by Dimitar Tasev in May 2014.
//  Copyright (c) 2012 Dimitar Tasev. All rights reserved.


#import "AppDelegate.h"


#ifdef __OBJC__


/* Angles */
#define degreesToRadians(x) (M_PI * x / 180.0)
#define radiansToDegrees(x) (x * (180.0 / M_PI))


/* Random */
#define randomInRange(a, b) MIN(a, b) + arc4random() % (MAX(a, b) - MIN(a, b) + 1)


/* Singleton */
#ifndef SINGLETON_GCD
#define SINGLETON_GCD(classname)                                                                                                                               \
+(classname *)sharedInstance {                                                                                                                               \
static dispatch_once_t pred;                                                                                                                               \
__strong static classname *shared##classname = nil;                                                                                                        \
dispatch_once(&pred, ^{ shared##classname = [self new]; });                                                                                                \
return shared##classname;                                                                                                                                  \
}
#endif


/* Devices */
#define IS_WIDESCREEN (fabs((double)[[UIScreen mainScreen] bounds].size.height - (double)568) < DBL_EPSILON)
#define IS_IPHONE ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone"])
#define IS_IPHONE4 ([[UIScreen mainScreen] bounds].size.height < 568)
#define IS_IPHONE5 ([[UIScreen mainScreen] bounds].size.height == 568)
#define IS_IPOD ([[[UIDevice currentDevice] model] isEqualToString:@"iPod touch"])
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)


/* Color */
#define color(_red, _green, _blue, _alpha) ((UIColor *)[UIColor colorWithRed:(_red / 255.0)green:(_green / 255.0)blue:(_blue / 255.0)alpha:_alpha])


/* OS */
#define SYSTEM_VERSION_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define IS_IOS6 ([[[UIDevice currentDevice] systemVersion] floatValue] < 7)
#define IS_IOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7 && [[[UIDevice currentDevice] systemVersion] floatValue] < 8)
#define IS_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8 && [[[UIDevice currentDevice] systemVersion] floatValue] < 9)
#define IS_IOS9 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9 && [[[UIDevice currentDevice] systemVersion] floatValue] < 10)

/* DLog and DxLog */
#ifdef DEBUG
#define DxLog(fmt, ...) NSLog((@"[%s:%d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#define DLog(fmt, ...) NSLog(fmt, ##__VA_ARGS__);
#else
#define DxLog(...)
#define DLog(...)
#endif
/*
#define DxLog(...)
#define DLog(...)
*/
/* ALog and AxLog */
#define AxLog(fmt, ...) NSLog((@"[%s:%d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#define ALog(fmt, ...) NSLog(fmt, ##__VA_ARGS__);


/* Informal Assert */
#define assert_valid(obj)                                                                                                                                      \
if (!obj || [obj isKindOfClass:[NSNull class]])                                                                                                              \
return;
#define assert_valid_null(obj)                                                                                                                                 \
if (!obj || [obj isKindOfClass:[NSNull class]])                                                                                                              \
return 0;
#define assert_valid_zero(obj)                                                                                                                                 \
if (0 == obj)                                                                                                                                                \
return 0;


/* Empty */
#define empty_string(obj)                                                                                                                                      \
(!obj || ![obj isKindOfClass:[NSString class]] ||                                                                                                            \
[[obj stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""])
#define empty_collection(obj) (!obj || [obj isKindOfClass:[NSNull class]] || [obj count] == 0)


/* Failsafe */
#define uuid(obj) ((NSString *)(empty_string(obj) ? [[NSUUID UUID] UUIDString] : obj))
#define string(obj) ((NSString *)(empty_string(obj) ? @"" : obj))
#define array(obj) ((NSArray *)(empty_collection(obj) ? @[] : obj))
#define dictionary(obj) ((NSDictionary *)(empty_collection(obj) ? @{} : obj))


/* Set Shorthand */
#define $(...) [NSSet setWithObjects:__VA_ARGS__, nil]


/* Style processing */
#define style_font(_name_, _size_) [UIFont fontWithName:dict[_name_] size:[dict[_size_] floatValue]]
#define style_inset(_a_, _b_, _c_, _d_)                                                                                                                        \
(UIEdgeInsets) { [dict[_a_] floatValue], [dict[_b_] floatValue], [dict[_c_] floatValue], [dict[_d_] floatValue] }
#define style_inset_button(_a_, _b_, _c_, _d_)                                                                                                                 \
(UIEdgeInsets) { [buttonDict[_a_] floatValue], [buttonDict[_b_] floatValue], [buttonDict[_c_] floatValue], [buttonDict[_d_] floatValue] }
#define style_size(_width_, _height_)                                                                                                                          \
(CGSize) { [dict[_width_] floatValue], [dict[_height_] floatValue] }
#define style_rgb(_red_, _green_, _blue_)                                                                                                                      \
[UIColor colorWithRed:([dict[_red_] floatValue] / 255.)green:([dict[_green_] floatValue] / 255.)blue:([dict[_blue_] floatValue] / 255.)alpha:1]
#define style_rgba(_red_, _green_, _blue_, _alpha_)                                                                                                            \
[UIColor colorWithRed:([dict[_red_] floatValue] / 255.)                                                                                                      \
green:([dict[_green_] floatValue] / 255.)                                                                                                    \
blue:([dict[_blue_] floatValue] / 255.)                                                                                                     \
alpha:[dict[_alpha_] floatValue]]


#endif
