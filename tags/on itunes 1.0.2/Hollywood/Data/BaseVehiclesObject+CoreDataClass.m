//
//  BaseVehiclesObject+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "BaseVehiclesObject+CoreDataClass.h"

@implementation BaseVehiclesObject

- (void) setData:(NSDictionary*)dataDictionary{
    //DLog(@"## %@",dataDictionary);
    
    self.audio = [self objectOrNilForKey:@"audio" fromDictionary: dataDictionary];
    self.free = [self objectOrNilForKey:@"free" fromDictionary: dataDictionary];
    self.image = [self objectOrNilForKey:@"image" fromDictionary: dataDictionary];
    self.instanceID = [self objectOrNilForKey:@"id" fromDictionary: dataDictionary];//instanceID
    self.name = [self objectOrNilForKey:@"instanceType" fromDictionary: dataDictionary];//name
    //self.order = [self objectOrNilForKey:@"order" fromDictionary: dataDictionary];
    self.questionAudioId = [self objectOrNilForKey:@"questionAudioId" fromDictionary: dataDictionary];
    self.questionText = [self objectOrNilForKey:@"questionText" fromDictionary: dataDictionary];
    self.questionType = [self objectOrNilForKey:@"questionType" fromDictionary: dataDictionary];
    self.showInstruction = [self objectOrNilForKey:@"showInstruction" fromDictionary: dataDictionary];
    self.title = [self objectOrNilForKey:@"title" fromDictionary: dataDictionary];
    self.type = [self objectOrNilForKey:@"type" fromDictionary: dataDictionary];
    self.unitID = [self objectOrNilForKey:@"unitID" fromDictionary: dataDictionary];
    self.isOneTry = [self objectOrNilForKey:@"isOneTry" fromDictionary:dataDictionary];
    
    if(!self.questionAudioId){
        self.questionAudioId = [self objectOrNilForKey:@"afterAudio" fromDictionary: dataDictionary];
    }else{
        NSLog(@"self.questionAudioId %@",self.questionAudioId);
    }
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
