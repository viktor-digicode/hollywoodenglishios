//
//  LabelTextObject+CoreDataClass.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/5/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface LabelTextObject : NSManagedObject

- (void) setDataWithKey:(NSString *)key andValue:(NSString *)value;

@end

NS_ASSUME_NONNULL_END

#import "LabelTextObject+CoreDataProperties.h"
