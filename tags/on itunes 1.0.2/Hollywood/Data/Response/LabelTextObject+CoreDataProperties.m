//
//  LabelTextObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/5/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LabelTextObject+CoreDataProperties.h"

@implementation LabelTextObject (CoreDataProperties)

+ (NSFetchRequest<LabelTextObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"LabelTextObject"];
}

@dynamic key;
@dynamic value;

@end
