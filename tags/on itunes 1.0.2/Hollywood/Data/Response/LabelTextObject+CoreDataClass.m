//
//  LabelTextObject+CoreDataClass.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/5/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LabelTextObject+CoreDataClass.h"

@implementation LabelTextObject

- (void) setDataWithKey:(NSString *)key andValue:(NSString *)value
{
    self.key = key;
    self.value = value;
    
    LabelTextObject *object = [LabelTextObject MR_createEntity];
    object.key = key;
    object.value = value;
}

@end
