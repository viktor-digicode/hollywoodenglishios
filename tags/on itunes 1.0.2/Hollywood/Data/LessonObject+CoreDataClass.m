//
//  LessonObject+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LessonObject+CoreDataClass.h"
#import "BaseVehiclesObject+CoreDataClass.h"

#import "VehicleObjectVideo+CoreDataClass.h"
#import "VehicleObjectVoiceRecognition+CoreDataClass.h"
#import "VehicleObjectSwipeCarousel+CoreDataClass.h"
#import "VehicleObjectClickToLearn+CoreDataClass.h"
#import "VehicleObjectFillTheMissing+CoreDataClass.h"
#import "VehicleObjectSpelling+CoreDataClass.h"
#import "VehicleObjectSwipe+CoreDataClass.h"
#import "VehicleObjectMultipleText+CoreDataClass.h"
#import "VehicleObjectMultipleImage+CoreDataClass.h"

@implementation LessonObject

- (void) setData:(NSDictionary*)dataDictionary{
    if( [dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.lesson_home_page_image = [self objectOrNilForKey:kRankLessonsLessonHomePageImage fromDictionary:dataDictionary];
        self.lesson_id = [NSNumber numberWithInteger:[[self objectOrNilForKey:kRankLessonsLessonId fromDictionary:dataDictionary]integerValue]];
        self.lesson_teaser = [self objectOrNilForKey:kRankLessonsLessonTeaser fromDictionary:dataDictionary];
        self.lesson_description = [self objectOrNilForKey:kRankLessonsLessonDescription fromDictionary:dataDictionary];
        self.lesson_name = [self objectOrNilForKey:kRankLessonsLessonName fromDictionary:dataDictionary];
        self.initial = [NSNumber numberWithInteger:[[self objectOrNilForKey:kRankLessonsInitial fromDictionary:dataDictionary]integerValue]];
        //self.lesson_vehicles = [self objectOrNilForKey:kRankLessonsLessonVehicles fromDictionary:dataDictionary];
        self.summary_Image = [self objectOrNilForKey:kLessonSummaryImageInitial fromDictionary:dataDictionary];
        NSObject *receivedLessonsVehicles = [dataDictionary objectForKey:kRankLessonsLessonVehicles];
        //NSMutableArray *parsedLessonsVehicles = [NSMutableArray array];
        int orderNumber = 0;
        if ([receivedLessonsVehicles isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLessonsVehicles) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    NSString *instanceTypeString = [self objectOrNilForKey:@"instanceType" fromDictionary:item];
                    BaseVehiclesObject *importedObject;
                    if ([instanceTypeString isEqualToString:@"video"]){
                        importedObject  = [VehicleObjectVideo MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"VoiceRecognitionInstance"]){
                        importedObject  = [VehicleObjectVoiceRecognition MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"SwipeCarouselInstance"]){
                        importedObject  = [VehicleObjectSwipeCarousel MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"ClickToLearnInstance"]){
                        importedObject  = [VehicleObjectClickToLearn MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"FillTheMissingWords"]){
                        importedObject  = [VehicleObjectFillTheMissing MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"SpellingInstance"]){
                        importedObject  = [VehicleObjectSpelling MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"Swipe"]){
                        importedObject  = [VehicleObjectSwipe MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"MultipleTextInstance"]){
                        importedObject  = [VehicleObjectMultipleText MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"MultipleImageInstance"]){
                        importedObject  = [VehicleObjectMultipleImage MR_createEntity];
                    }else{
                        importedObject = [BaseVehiclesObject MR_createEntity];
                    }
                    
                    //BaseVehiclesObject *importedObject = [BaseVehiclesObject MR_createEntity];
                    
                    importedObject.order  = [NSNumber numberWithInt:orderNumber];
                    orderNumber++;
                    [importedObject setData:item];
                    //[parsedLessonsVehicles addObject:importedObject];
                    [self addLesson_vehiclesObject:importedObject];
                }
            }
        } else if ([receivedLessonsVehicles isKindOfClass:[NSNumber class]]) {
            BaseVehiclesObject *importedObject = [BaseVehiclesObject MR_createEntity];
            //[importedObject setData:(NSDictionary *)receivedLessonsVehicles];
            importedObject.instanceID  = (NSNumber*)receivedLessonsVehicles;
            //[parsedLessonsVehicles addObject:importedObject];
            [self addLesson_vehiclesObject:importedObject];
        }
        
        /* NSSet *lessonsVehiclesSet = [[NSSet alloc] init];
         [lessonsVehiclesSet setByAddingObjectsFromArray:[NSArray arrayWithArray:parsedLessonsVehicles]];
         self.lesson_vehicles = lessonsVehiclesSet;*/
        
        
    }
}

- (void) setVehiclesData:(NSDictionary*)dataDictionary{
    if( [dataDictionary isKindOfClass:[NSDictionary class]]) {
        NSObject *receivedLessonsVehicles = [dataDictionary objectForKey:kRankLessonsLessonVehicles];
        int orderNumber = 0;
        if ([receivedLessonsVehicles isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLessonsVehicles) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    NSString *instanceTypeString = [self objectOrNilForKey:@"instanceType" fromDictionary:item];
                    BaseVehiclesObject *importedObject;
                    if ([instanceTypeString isEqualToString:@"video"]){
                        importedObject  = [VehicleObjectVideo MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"VoiceRecognitionInstance"]){
                        importedObject  = [VehicleObjectVoiceRecognition MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"SwipeCarouselInstance"]){
                        importedObject  = [VehicleObjectSwipeCarousel MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"ClickToLearnInstance"]){
                        importedObject  = [VehicleObjectClickToLearn MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"FillTheMissingWords"]){
                        importedObject  = [VehicleObjectFillTheMissing MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"SpellingInstance"]){
                        importedObject  = [VehicleObjectSpelling MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"Swipe"]){
                        importedObject  = [VehicleObjectSwipe MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"MultipleTextInstance"]){
                        importedObject  = [VehicleObjectMultipleText MR_createEntity];
                    }else if ([instanceTypeString isEqualToString:@"MultipleImageInstance"]){
                        importedObject  = [VehicleObjectMultipleImage MR_createEntity];
                    }else{
                        importedObject = [BaseVehiclesObject MR_createEntity];
                    }
                    
                    //BaseVehiclesObject *importedObject = [BaseVehiclesObject MR_createEntity];
                    
                    importedObject.order  = [NSNumber numberWithInt:orderNumber];
                    orderNumber++;
                    [importedObject setData:item];
                    //[parsedLessonsVehicles addObject:importedObject];
                    [self addLesson_vehiclesObject:importedObject];
                }
            }
        } else if ([receivedLessonsVehicles isKindOfClass:[NSNumber class]]) {
            BaseVehiclesObject *importedObject = [BaseVehiclesObject MR_createEntity];
            importedObject.instanceID  = (NSNumber*)receivedLessonsVehicles;
            [self addLesson_vehiclesObject:importedObject];
        }
        
    }
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}
@end
