//
//  VehicleObjectMultipleText+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectMultipleText+CoreDataProperties.h"

@implementation VehicleObjectMultipleText (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectMultipleText *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"VehicleObjectMultipleText"];
}

@dynamic instanceAnswers;

@end
