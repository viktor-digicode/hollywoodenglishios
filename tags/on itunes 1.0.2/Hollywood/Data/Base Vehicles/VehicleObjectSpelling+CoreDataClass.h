//
//  VehicleObjectSpelling+CoreDataClass.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 6/30/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseVehiclesObject+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface VehicleObjectSpelling : BaseVehiclesObject

- (void) setData:(NSDictionary*)dataDictionary;

@end

NS_ASSUME_NONNULL_END

#import "VehicleObjectSpelling+CoreDataProperties.h"
