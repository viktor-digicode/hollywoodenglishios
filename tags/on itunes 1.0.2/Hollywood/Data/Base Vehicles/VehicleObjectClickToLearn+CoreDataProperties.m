//
//  VehicleObjectClickToLearn+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectClickToLearn+CoreDataProperties.h"

@implementation VehicleObjectClickToLearn (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectClickToLearn *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"VehicleObjectClickToLearn"];
}

@dynamic instanceImage;

@end
