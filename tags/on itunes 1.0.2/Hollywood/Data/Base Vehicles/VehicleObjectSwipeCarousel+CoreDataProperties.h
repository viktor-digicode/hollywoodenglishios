//
//  VehicleObjectSwipeCarousel+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 7/4/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectSwipeCarousel+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VehicleObjectSwipeCarousel (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectSwipeCarousel *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSSet<InstanceAnswersSwipeCarouselObject *> *instanceAnswers;

@end

@interface VehicleObjectSwipeCarousel (CoreDataGeneratedAccessors)

- (void)addInstanceAnswersObject:(InstanceAnswersSwipeCarouselObject *)value;
- (void)removeInstanceAnswersObject:(InstanceAnswersSwipeCarouselObject *)value;
- (void)addInstanceAnswers:(NSSet<InstanceAnswersSwipeCarouselObject *> *)values;
- (void)removeInstanceAnswers:(NSSet<InstanceAnswersSwipeCarouselObject *> *)values;

@end

NS_ASSUME_NONNULL_END
