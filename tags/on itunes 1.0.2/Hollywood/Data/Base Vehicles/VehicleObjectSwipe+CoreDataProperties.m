//
//  VehicleObjectSwipe+CoreDataProperties.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 6/30/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectSwipe+CoreDataProperties.h"

@implementation VehicleObjectSwipe (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectSwipe *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"VehicleObjectSwipe"];
}

@dynamic initial;
@dynamic lesson_description;
@dynamic lesson_home_page_image;
@dynamic lesson_id;
@dynamic lesson_name;
@dynamic lesson_teaser;
@dynamic instanceAnswers;

@end
