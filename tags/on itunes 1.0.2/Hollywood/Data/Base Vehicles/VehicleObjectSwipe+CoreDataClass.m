//
//  VehicleObjectSwipe+CoreDataClass.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 6/30/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectSwipe+CoreDataClass.h"
#import "InstanceAnswersSwipeObject+CoreDataClass.h"

@implementation VehicleObjectSwipe

- (void) setData:(NSDictionary*)dataDictionary{
    [super setData:dataDictionary];
    //DLog(@"## %@",dataDictionary);
    
    
    if( [dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.lesson_id = [NSNumber numberWithInteger:[[self objectOrNilForKey:kRankLessonsLessonId fromDictionary:dataDictionary]integerValue]];
        self.lesson_teaser = [self objectOrNilForKey:kRankLessonsLessonTeaser fromDictionary:dataDictionary];
        self.lesson_description = [self objectOrNilForKey:kRankLessonsLessonDescription fromDictionary:dataDictionary];
        self.lesson_name = [self objectOrNilForKey:kRankLessonsLessonName fromDictionary:dataDictionary];
        self.initial = [NSNumber numberWithInteger:[[self objectOrNilForKey:kRankLessonsInitial fromDictionary:dataDictionary]integerValue]];
        self.lesson_home_page_image = [self objectOrNilForKey:kRankLessonsLessonHomePageImage fromDictionary:dataDictionary];
        
        NSObject *receivedLessonsVehicles = [dataDictionary objectForKey:kVehicleObjectInstanceAnswer];
        //NSMutableArray *parsedLessonsVehicles = [NSMutableArray array];
        if ([receivedLessonsVehicles isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLessonsVehicles) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    InstanceAnswersSwipeObject *importedObject = [InstanceAnswersSwipeObject MR_createEntity];
                    //importedObject.instanceID  = (NSNumber*)item;
                    [importedObject setData:item];
                    //[parsedLessonsVehicles addObject:importedObject];
                    [self addInstanceAnswersObject:importedObject];
                }
            }
        }
    }
    
    
}


#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


@end
