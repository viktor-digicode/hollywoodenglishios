//
//  WordsObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 9/5/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "WordsObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WordsObject (CoreDataProperties)

+ (NSFetchRequest<WordsObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *word;
@property (nullable, nonatomic, copy) NSString *wordAudio;

@end

NS_ASSUME_NONNULL_END
