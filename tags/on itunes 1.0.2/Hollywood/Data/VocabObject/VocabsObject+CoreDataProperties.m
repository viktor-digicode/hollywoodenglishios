//
//  VocabsObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 9/5/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VocabsObject+CoreDataProperties.h"

@implementation VocabsObject (CoreDataProperties)

+ (NSFetchRequest<VocabsObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"VocabsObject"];
}

@dynamic vocab_id;
@dynamic word;
@dynamic wordAudio;
@dynamic nativeVocabs;

@end
