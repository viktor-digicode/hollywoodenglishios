//
//  VipQuizObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 7/20/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VipQuizObject+CoreDataClass.h"
#import "QuizInstance+CoreDataProperties.h"

NS_ASSUME_NONNULL_BEGIN

@interface VipQuizObject (CoreDataProperties)

+ (NSFetchRequest<VipQuizObject *> *)fetchRequest;

@property (nonatomic) BOOL availability;
@property (nullable, nonatomic, copy) NSString *avEndDate;
@property (nullable, nonatomic, copy) NSString *avStartDate;
@property (nullable, nonatomic, copy) NSString *imageName;
@property (nullable, nonatomic, copy) NSString *imageUrl;
@property (nullable, nonatomic, copy) NSNumber *isPromo;
@property (nullable, nonatomic, copy) NSNumber *orderInCategory;
@property (nullable, nonatomic, copy) NSNumber *price;
@property (nullable, nonatomic, copy) NSNumber *quizId;
@property (nullable, nonatomic, copy) NSString *quizListDescription;
@property (nullable, nonatomic, copy) NSString *quizName;
@property (nullable, nonatomic, copy) NSString *teaser;
@property (nullable, nonatomic, copy) NSString *quizSummaryImage;
@property (nonatomic) BOOL purchased;
@property (nullable, nonatomic, retain) NSSet<QuizInstance *> *instanceList;

@end

@interface VipQuizObject (CoreDataGeneratedAccessors)

- (void)addInstanceListObject:(QuizInstance *)value;
- (void)removeInstanceListObject:(QuizInstance *)value;
- (void)addInstanceList:(NSSet<QuizInstance *> *)values;
- (void)removeInstanceList:(NSSet<QuizInstance *> *)values;

@end

NS_ASSUME_NONNULL_END
