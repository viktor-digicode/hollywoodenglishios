//
//  VipCourseObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 10/3/16.
//  Copyright © 2016 aa. All rights reserved.
//
//
#import "VipCourseObject+CoreDataProperties.h"

@implementation VipCourseObject (CoreDataProperties)

+ (NSFetchRequest<VipCourseObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"VipCourseObject"];
}

@dynamic vipCourse_id;
@dynamic vipCourse_name;
@dynamic vipCategories;

@end
