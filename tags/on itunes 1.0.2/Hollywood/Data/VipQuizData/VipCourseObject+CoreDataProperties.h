//
//  VipCourseObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 10/3/16.
//  Copyright © 2016 aa. All rights reserved.
//
//
#import "VipCourseObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VipCourseObject (CoreDataProperties)

+ (NSFetchRequest<VipCourseObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *vipCourse_id;
@property (nullable, nonatomic, copy) NSString *vipCourse_name;
@property (nullable, nonatomic, retain) NSSet<VipCategoryObject *> *vipCategories;

@end

@interface VipCourseObject (CoreDataGeneratedAccessors)

- (void)addVipCategoriesObject:(VipCategoryObject *)value;
- (void)removeVipCategoriesObject:(VipCategoryObject *)value;
- (void)addVipCategories:(NSSet<VipCategoryObject *> *)values;
- (void)removeVipCategories:(NSSet<VipCategoryObject *> *)values;

@end

NS_ASSUME_NONNULL_END
