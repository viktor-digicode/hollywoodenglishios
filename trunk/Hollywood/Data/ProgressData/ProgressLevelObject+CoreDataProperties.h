//
//  ProgressLevelObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 8/11/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ProgressLevelObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProgressLevelObject (CoreDataProperties)

@property (nonatomic) int32_t maxUnit;
@property (nonatomic) int32_t currentUnit;
@property (nonatomic) int32_t scoreForLevel;
@property (nonatomic) BOOL isOpen;
@property (nonatomic) BOOL diploma;
@property (nonatomic) int32_t levelOrder;
@property (nullable, nonatomic, retain) NSSet<ProgressLessonObject *> *lessonsProgres;

@end

@interface ProgressLevelObject (CoreDataGeneratedAccessors)

- (void)addLessonsProgresObject:(ProgressLessonObject *)value;
- (void)removeLessonsProgresObject:(ProgressLessonObject *)value;
- (void)addLessonsProgres:(NSSet<ProgressLessonObject *> *)values;
- (void)removeLessonsProgres:(NSSet<ProgressLessonObject *> *)values;

@end

NS_ASSUME_NONNULL_END
