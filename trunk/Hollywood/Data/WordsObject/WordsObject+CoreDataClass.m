//
//  WordsObject+CoreDataClass.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 9/5/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "WordsObject+CoreDataClass.h"

@implementation WordsObject

- (void) setData:(NSDictionary*)dataDictionary
{
    if([dataDictionary isKindOfClass:[NSDictionary class]])
    {
        self.word = [self objectOrNilForKey: kWord fromDictionary: dataDictionary];
        self.wordAudio = [self objectOrNilForKey: kVocabWordAudio fromDictionary: dataDictionary];
    }
}

#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
