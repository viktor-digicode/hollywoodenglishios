//
//  ParameterObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 8/21/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "ParameterObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ParameterObject (CoreDataProperties)

+ (NSFetchRequest<ParameterObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *key;
@property (nullable, nonatomic, copy) NSNumber *value;

@end

NS_ASSUME_NONNULL_END
