//
//  ParameterObject+CoreDataClass.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 8/21/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "ParameterObject+CoreDataClass.h"

@implementation ParameterObject

- (void)setDataWithKey:(NSString *)key andValue:(NSNumber *)value
{
    self.key = key;
    self.value = value;
    
    ParameterObject *object = [ParameterObject MR_createEntity];
    object.key = key;
    object.value = value;
}

@end
