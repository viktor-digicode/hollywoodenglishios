//
//  InstanceAnswersSwipeCarouselObject+CoreDataClass.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InstanceObject+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface InstanceAnswersSwipeCarouselObject : InstanceObject

@end

NS_ASSUME_NONNULL_END

#import "InstanceAnswersSwipeCarouselObject+CoreDataProperties.h"
