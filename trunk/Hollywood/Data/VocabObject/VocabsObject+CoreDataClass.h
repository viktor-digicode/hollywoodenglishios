//
//  VocabsObject+CoreDataClass.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 9/5/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class WordsObject;

NS_ASSUME_NONNULL_BEGIN

@interface VocabsObject : NSManagedObject

- (void)setData:(NSDictionary*)dataDictionary;

@end

NS_ASSUME_NONNULL_END

#import "VocabsObject+CoreDataProperties.h"
