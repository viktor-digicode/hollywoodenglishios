//
//  RankObject+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "RankObject+CoreDataClass.h"
#import "LessonObject+CoreDataClass.h"

@implementation RankObject

- (void) setData:(NSDictionary*)dataDictionary{
    if([dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.rank_name = [self objectOrNilForKey:kLevelRanksRankName fromDictionary:dataDictionary];
        self.rank_lesson_Background = [self objectOrNilForKey:kLevelRanksRankLessonBackground fromDictionary: dataDictionary];
        self.rank_icon = [self objectOrNilForKey:kLevelRanksRankIcon fromDictionary: dataDictionary];
        //self.rank_number = [NSNumber numberWithInteger:[[self objectOrNilForKey:kLevelRanksRankNumber fromDictionary:dataDictionary]integerValue]];
        self.rank_teaser_text = [self objectOrNilForKey:kLevelRanksRankTeaserText fromDictionary: dataDictionary];
        self.rank_diploma_design = [self objectOrNilForKey:kLevelRanksRankDiplomaDesign fromDictionary:dataDictionary];
        self.rank_id = [NSNumber numberWithInteger:[[self objectOrNilForKey:kLevelRanksRankId fromDictionary:dataDictionary]integerValue]];
        self.rank_achieved_audio = [self objectOrNilForKey:kLevelRanksRankAchievedAudio fromDictionary: dataDictionary];
        self.rank_rank_achieved_text = [self objectOrNilForKey:kLevelRanksRankRankAchievedText fromDictionary: dataDictionary];
        
        NSObject *receivedRankLessons = [ dataDictionary  objectForKey:kLevelRanksRankLessons];
        //NSMutableArray *parsedRankLessons = [NSMutableArray array];
        int countNumber = 0;
        if ([receivedRankLessons isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedRankLessons) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    LessonObject *importedObject = [LessonObject MR_createEntity];
                    importedObject.lesson_order = [DATA_MANAGER getLessonOrder];//
                    //[NSNumber numberWithInteger: ([self.rank_number intValue] * 10) +countNumber];
                    importedObject.rankParentNumber = self.rank_number;
                    countNumber++;
                    [importedObject setData:item];
                    //[parsedRankLessons addObject:importedObject];
                    [self addRank_lessonsObject:importedObject];
                }
            }
        } else if ([receivedRankLessons isKindOfClass:[NSDictionary class]]) {
            LessonObject *importedObject = [LessonObject MR_createEntity];
            [importedObject setData:(NSDictionary *)receivedRankLessons];
            [self addRank_lessonsObject:importedObject];
            //[parsedRankLessons addObject:importedObject];
        }
        
        /* NSSet *rankLessonsSet = [[NSSet alloc] init];
         [rankLessonsSet setByAddingObjectsFromArray:[NSArray arrayWithArray:parsedRankLessons]];
         self.rank_lessons = rankLessonsSet;
         */
        
        
    }
    
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}
@end
