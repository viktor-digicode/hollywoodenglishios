//
//  InstanceAnswersSwipeCarouselObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersSwipeCarouselObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface InstanceAnswersSwipeCarouselObject (CoreDataProperties)

+ (NSFetchRequest<InstanceAnswersSwipeCarouselObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *afterClickAudio;
@property (nullable, nonatomic, copy) NSString *afterClickImage;
@property (nullable, nonatomic, copy) NSString *answerImage;
@property (nullable, nonatomic, copy) NSString *carouselSegmentQuestionAudio;
@property (nullable, nonatomic, copy) NSString *carouselSegmentQuestionText;
@property (nullable, nonatomic, copy) NSString *carouselSegmentQuestionType;
@property (nullable, nonatomic, copy) NSNumber *free;

@end

NS_ASSUME_NONNULL_END
