//
//  InstanceAnswersMultipleImageObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersMultipleImageObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface InstanceAnswersMultipleImageObject (CoreDataProperties)

+ (NSFetchRequest<InstanceAnswersMultipleImageObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *afterClickImage;
@property (nullable, nonatomic, copy) NSString *afterClickText;
@property (nullable, nonatomic, copy) NSString *answerClickAudio;
@property (nullable, nonatomic, copy) NSString *answerImage;
@property (nullable, nonatomic, copy) NSNumber *correctAnswer;

@end

NS_ASSUME_NONNULL_END
