//
//  InstanceAnswersImageObject+CoreDataClass.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InstanceObject+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface InstanceAnswersImageObject : InstanceObject

- (void) setData:(NSDictionary*)dataDictionary;

@end

NS_ASSUME_NONNULL_END

#import "InstanceAnswersImageObject+CoreDataProperties.h"
