//
//  VehicleObjectClickToLearn+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectClickToLearn+CoreDataClass.h"
#import "InstanceAnswersImageObject+CoreDataClass.h"

@implementation VehicleObjectClickToLearn

- (void) setData:(NSDictionary*)dataDictionary{
    [super setData:dataDictionary];
    //DLog(@"## %@",dataDictionary);
    
    NSObject *receivedLessonsVehicles = [dataDictionary objectForKey:kVehicleObjectInstanceImage];
    if ([receivedLessonsVehicles isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedLessonsVehicles) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                InstanceAnswersImageObject *importedObject = [InstanceAnswersImageObject MR_createEntity];
                
                [importedObject setData:item];
                [self addInstanceImageObject:importedObject];
                
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
}

@end
