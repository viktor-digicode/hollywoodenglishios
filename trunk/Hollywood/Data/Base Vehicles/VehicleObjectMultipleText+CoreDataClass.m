//
//  VehicleObjectMultipleText+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectMultipleText+CoreDataClass.h"
#import "InstanceAnswersMultipleTextObject+CoreDataClass.h"

@implementation VehicleObjectMultipleText

- (void) setData:(NSDictionary*)dataDictionary{
    [super setData:dataDictionary];
    //DLog(@"## %@",dataDictionary);
    
    
        NSObject *receivedLessonsVehicles = [dataDictionary objectForKey:kVehicleObjectInstanceAnswer];
        
        if ([receivedLessonsVehicles isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedLessonsVehicles) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    InstanceAnswersMultipleTextObject *importedObject = [InstanceAnswersMultipleTextObject MR_createEntity];
                    //importedObject.instanceID  = (NSNumber*)item;
                    [importedObject setData:item];
                    //[parsedLessonsVehicles addObject:importedObject];
                    [self addInstanceAnswersObject:importedObject];
                }
            }
        }
    
    
    
    
    
    
    
    
    
    
    
}

@end
