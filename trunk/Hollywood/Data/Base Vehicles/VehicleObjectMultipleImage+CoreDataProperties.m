//
//  VehicleObjectMultipleImage+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectMultipleImage+CoreDataProperties.h"

@implementation VehicleObjectMultipleImage (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectMultipleImage *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"VehicleObjectMultipleImage"];
}

@dynamic instanceAnswers;

@end
