//
//  VehicleObjectSpelling+CoreDataProperties.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 6/30/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectSpelling+CoreDataProperties.h"

@implementation VehicleObjectSpelling (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectSpelling *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"VehicleObjectSpelling"];
}

@dynamic sentence;

@end
