//
//  VehicleObjectMultipleImage+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectMultipleImage+CoreDataClass.h"
#import "InstanceAnswersMultipleImageObject+CoreDataClass.h"

@implementation VehicleObjectMultipleImage

- (void) setData:(NSDictionary*)dataDictionary{
    [super setData:dataDictionary];
    //DLog(@"## %@",dataDictionary);
    
    
    NSObject *receivedLessonsVehicles = [dataDictionary objectForKey:kVehicleObjectInstanceAnswer];
    if ([receivedLessonsVehicles isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedLessonsVehicles) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                InstanceAnswersMultipleImageObject *importedObject = [InstanceAnswersMultipleImageObject MR_createEntity];
                
                [importedObject setData:item];
                [self addInstanceAnswersObject:importedObject];
                
            }
        }
    }
    
    
    
}

@end
