//
//  VipCategoryObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 10/4/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "VipCategoryObject+CoreDataProperties.h"

@implementation VipCategoryObject (CoreDataProperties)

+ (NSFetchRequest<VipCategoryObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"VipCategoryObject"];
}

@dynamic categoryId;
@dynamic categoryName;
@dynamic displayOrder;
@dynamic imageName;
@dynamic imageUrl;
@dynamic availability;
@dynamic firstQuizInstanceList;
@dynamic quizList;

@end
