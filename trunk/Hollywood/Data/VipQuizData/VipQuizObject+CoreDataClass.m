//
//  VipQuizObject+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 10/3/16.
//  Copyright © 2016 aa. All rights reserved.
//
#import "Constants.h"
#import "VipQuizObject+CoreDataClass.h"
#import "QuizInstance+CoreDataClass.h"
//#import "BaseVehiclesObject+CoreDataClass.h"

@implementation VipQuizObject

- (void) setData:(NSDictionary*)dataDictionary{
    if(self && [ dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.quizListDescription = [self objectOrNilForKey:kQuizListDescription fromDictionary: dataDictionary];
        //self.orderInCategory = [NSNumber numberWithDouble:(double)[[self objectOrNilForKey:kQuizListOrderInCategory fromDictionary: dataDictionary] doubleValue]];
        self.quizId = [NSNumber numberWithDouble:(double)[[self objectOrNilForKey:kQuizListQuizId fromDictionary: dataDictionary] doubleValue]];
        self.quizName = [self objectOrNilForKey:kQuizListQuizName fromDictionary: dataDictionary];
        self.avEndDate = [self objectOrNilForKey:kQuizListAvEndDate fromDictionary: dataDictionary];
        self.imageUrl = [self objectOrNilForKey:kQuizListImageUrl fromDictionary: dataDictionary];
        self.price =  [NSNumber numberWithDouble:(double)[[self objectOrNilForKey:kQuizListPrice fromDictionary: dataDictionary] doubleValue]];
        NSString *availabilityString = [self objectOrNilForKey:kQuizListAvailability fromDictionary: dataDictionary];
        
        if([availabilityString isEqualToString:@"always"]){
            self.availability = YES;
        }else  if([availabilityString isEqualToString:@"date_span"]){
            self.availability = YES;
        }else{
            self.availability = NO;
        }
            
        self.avStartDate = [self objectOrNilForKey:kQuizListAvStartDate fromDictionary: dataDictionary];
        self.isPromo = [NSNumber numberWithDouble:(double)[[self objectOrNilForKey:kQuizListIsPromo fromDictionary: dataDictionary] doubleValue]];        self.teaser = [self objectOrNilForKey:kQuizListTeaser fromDictionary: dataDictionary];
        self.imageName = [self objectOrNilForKey:kQuizListImageName fromDictionary: dataDictionary];
       //self.instanceList = [self objectOrNilForKey:kQuizListInstanceList fromDictionary: dataDictionary];
        self.quizSummaryImage = [self objectOrNilForKey:kQuizListImageName fromDictionary: dataDictionary];
        self.purchased = [[self objectOrNilForKey:kQuizPurchased fromDictionary: dataDictionary] boolValue];

        NSArray *arrayPurchasedQuizes = [[NSUserDefaults standardUserDefaults] objectForKey:kUserDataQuizList];
        BOOL isQuizPurchased = [self theQuizIsPurchased:arrayPurchasedQuizes quizToCompare:self.quizId];
        
        self.purchased = isQuizPurchased;
        
        NSObject *receivedInstanceList = [self objectOrNilForKey:kQuizListInstanceList fromDictionary: dataDictionary];
        
        if ([receivedInstanceList isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedInstanceList) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    QuizInstance *tempQuizInstanceObject = [QuizInstance MR_createEntity];
                    [tempQuizInstanceObject setData:item];
                    [self addInstanceListObject:tempQuizInstanceObject];
                }
            }
        } else if ([receivedInstanceList isKindOfClass:[NSDictionary class]]) {
            QuizInstance *tempQuizInstanceObject = [QuizInstance MR_createEntity];
            [tempQuizInstanceObject setData:(NSDictionary*)receivedInstanceList];
            [self addInstanceListObject:tempQuizInstanceObject];
        }
    }
}

- (void) setInstanceData:(NSDictionary*)dataDictionary{
    if(self && [ dataDictionary isKindOfClass:[NSDictionary class]]) {
        
        NSObject *receivedInstanceList = [self objectOrNilForKey:kQuizListInstanceList fromDictionary: dataDictionary];
        
        if ([receivedInstanceList isKindOfClass:[NSArray class]]) {
            for (NSDictionary *item in (NSArray *)receivedInstanceList) {
                if ([item isKindOfClass:[NSDictionary class]]) {
                    QuizInstance *tempQuizInstanceObject = [QuizInstance MR_createEntity];
                    [tempQuizInstanceObject setData:item];
                    [self addInstanceListObject:tempQuizInstanceObject];
                }
            }
        } else if ([receivedInstanceList isKindOfClass:[NSDictionary class]]) {
            QuizInstance *tempQuizInstanceObject = [QuizInstance MR_createEntity];
            [tempQuizInstanceObject setData:(NSDictionary*)receivedInstanceList];
            [self addInstanceListObject:tempQuizInstanceObject];
        }
    }
}
#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

- (BOOL)theQuizIsPurchased:(NSArray *)arrayQizzesId quizToCompare:(NSNumber *)quizId {
    
    for (NSNumber *currentQuizId in arrayQizzesId) {
        
        if (currentQuizId == quizId) {
            
            return YES;
        }
    }
    
    return NO;
}

@end
