//
//  QuizInstance+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 2/6/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "QuizInstance+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface QuizInstance (CoreDataProperties)

+ (NSFetchRequest<QuizInstance *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *bigImageUrl;
@property (nullable, nonatomic, copy) NSString *imageName;
@property (nullable, nonatomic, copy) NSString *imageUrl;
@property (nullable, nonatomic, copy) NSString *instanceQuestion;
@property (nonatomic) BOOL isOneImageLayout;
@property (nullable, nonatomic, copy) NSNumber *quizInstanceId;
@property (nullable, nonatomic, copy) NSNumber *instanceType;
@property (nullable, nonatomic, retain) NSSet<QuizInstanceAnswer *> *answersForInstance;

@end

@interface QuizInstance (CoreDataGeneratedAccessors)

- (void)addAnswersForInstanceObject:(QuizInstanceAnswer *)value;
- (void)removeAnswersForInstanceObject:(QuizInstanceAnswer *)value;
- (void)addAnswersForInstance:(NSSet<QuizInstanceAnswer *> *)values;
- (void)removeAnswersForInstance:(NSSet<QuizInstanceAnswer *> *)values;

@end

NS_ASSUME_NONNULL_END
