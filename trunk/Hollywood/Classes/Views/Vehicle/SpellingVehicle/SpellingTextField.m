//
//  SpellingTextField.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 4/11/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "SpellingTextField.h"

@implementation SpellingTextField

- (instancetype)initWithFrame:(CGRect)frame expectedLetter:(NSString *)letter {
    
    if (self = [super initWithFrame:frame]) {
        
        self.stringExpectedLetter = letter;
        self.keyboardType = UIKeyboardTypeEmailAddress;
    }
    
    return self;
}

@end
