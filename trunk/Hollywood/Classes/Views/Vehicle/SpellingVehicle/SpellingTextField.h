//
//  SpellingTextField.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 4/11/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpellingTextField : UITextField

@property (nonatomic, strong) NSString *stringExpectedLetter;

- (instancetype)initWithFrame:(CGRect)frame expectedLetter:(NSString *)letter;

@end
