//
//  MissingWordsTextAnswer.m
//  Hollywood
//
//  Created by Kiril Kiroski on 5/13/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "MissingWordsTextAnswer.h"
#import "Constants.h"
#import "InstanceAnswersWordsObject+CoreDataClass.h"

@implementation MissingWordsTextAnswer{
    UIView *viewContainerLabelWord;
    UILabel *labelWord;
    UIImageView *imageViewFeedback;
    CGPoint lastLocation;
    CGPoint startLocation;
    BOOL buttonTouched;
    NSString *initialString;
    UIButton *buttonSound;
    NSString *soundString;
    UILabel *duplicateLabelWord;
    BOOL isLock;
}

- (instancetype)initWithFrame:(CGRect)frame data:(id)data type:(BOOL)type onView:(UIView*)tempView{
    InstanceAnswersWordsObject *dataClickImage = (InstanceAnswersWordsObject*)data;
    if(dataClickImage.audio){
        buttonSound = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonSound setImage:[UIImage imageNamed:@"soundGoldcopy11"] forState:UIControlStateNormal];
        [buttonSound setFrame:CGRectMake(frame.size.width-8, (frame.size.height-34)/2, 34, 34)];
        [buttonSound setAlpha:0.64];
        [buttonSound addTarget:self action:@selector(playAnswerSound:) forControlEvents:UIControlEventTouchUpInside];
        [tempView addSubview:buttonSound];
        tempView.userInteractionEnabled = YES;
        soundString = dataClickImage.audio;
    }
    return [self initWithFrame:frame data:data type:type];
}
//dolu tie so se mrdaat
- (instancetype)initWithFrame:(CGRect)frame data:(id)data type:(BOOL)type {
    
    InstanceAnswersWordsObject *dataClickImage = (InstanceAnswersWordsObject*)data;
    
    if (self = [super initWithFrame:frame]) {
        
        viewContainerLabelWord = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        /*[viewContainerLabelWord setBackgroundColor:[UIColor blackColor]];
        viewContainerLabelWord.layer.cornerRadius = 4.0;
        viewContainerLabelWord.layer.borderColor = [UIColor whiteColor].CGColor;
        viewContainerLabelWord.layer.borderWidth = 1.0;
        viewContainerLabelWord.layer.masksToBounds = YES;*/
        [self addSubview:viewContainerLabelWord];
        
        labelWord = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, frame.size.width-10, frame.size.height)];
        [labelWord setFont:[UIFont systemFontOfSize:26.0]];
        [labelWord setTextColor:[UIColor whiteColor]];
        [labelWord setBackgroundColor:[UIColor clearColor]];
        labelWord.numberOfLines = 1;
        labelWord.text = [dataClickImage.word capitalizedString];
        initialString = dataClickImage.word;
        [labelWord fontSizeToFit];
        [labelWord sizeToFit];
        
        [self addSubview:labelWord];
        
        //labelWord.frame = CGRectMake((frame.size.width-labelWord.width)/2, (frame.size.height-labelWord.height)/2, labelWord.width, labelWord.height);
        labelWord.frame = CGRectMake(5, (frame.size.height-labelWord.height)/2, labelWord.width, labelWord.height);
        self.backgroundColor = [UIColor redColor];
        startLocation = frame.origin;
        /*UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(detectPan:)];
        self.gestureRecognizers = @[panRecognizer];*/
    }
    
    return self;
}
- (void)setNewFont:(UIFont*)newFont{
    [labelWord setFont:newFont];
}
- (UIFont*)getFont{
    return labelWord.font;
}
//gore tie so se
- (instancetype)initWithFrame:(CGRect)frame withText:(NSString*)infoText andReplaceString:(NSString*)newReplaceString{
    if (self = [super initWithFrame:frame]) {
        self.userInteractionEnabled = NO;
        viewContainerLabelWord = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [viewContainerLabelWord setBackgroundColor:[UIColor clearColor]];
        [self addSubview:viewContainerLabelWord];
        
        labelWord = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [labelWord setFont:[UIFont systemFontOfSize:26.0]];
        [labelWord setTextColor:[UIColor whiteColor]];
        [labelWord setBackgroundColor:[UIColor clearColor]];
        labelWord.numberOfLines = 0;
        labelWord.text = infoText;
        
        initialString = infoText;
        if([LMHelper isWordMatchingPattern:infoText pattern:kWordPattern]) {
            labelWord.text = newReplaceString;//kUnderlineWord;
        }
        
        [labelWord sizeToFit];
        
        viewContainerLabelWord.frame = labelWord.frame;
        [self addSubview:labelWord];
        [self setHeight: labelWord.height];
        [self setWidth:labelWord.width];
        
        UILabel *tempLabelWord = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [tempLabelWord setFont:[UIFont systemFontOfSize:26.0]];
        tempLabelWord.text = infoText;
        [tempLabelWord sizeToFit];
        
        //iskomentiranovo ako e vo sredina dolu zborot so se vlece
        //startLocation = CGPointMake(frame.origin.x-(frame.size.width-labelWord.width)/2,frame.origin.y-(frame.size.height-labelWord.height)/2);;
        //koga ke go ostavi ako e tocno
        startLocation = CGPointMake(-(tempLabelWord.width/2)+frame.origin.x+(labelWord.width/2)+10,frame.origin.y-(frame.size.height-labelWord.height)/2);;
        //+10 go staviv za 4ka 
    }
    
    return self;
}

- (void)setInitialString{
    
    labelWord.text = initialString ;
}

- (void)setIncorrectAnswer{
    if(duplicateLabelWord){
        return ;
    }
    [UIView animateWithDuration:0.2 animations:^{
        labelWord.textColor = [UIColor redColor];
    } completion:^(BOOL finished) {
        if(duplicateLabelWord){
            return ;
        }
         [UIView animateWithDuration:1 animations:^{
            labelWord.alpha = 0.95;
        } completion:^(BOOL finished) {
            labelWord.textColor = [UIColor whiteColor];
            labelWord.alpha = 1;
        }];
    }];
}

- (void)setCorrectAnswer{
    [UIView animateWithDuration:0.2 animations:^{
        labelWord.textColor = [UIColor greenColor];
    } completion:^(BOOL finished) {
        duplicateLabelWord = [[UILabel alloc]initWithFrame:labelWord.frame];
        [duplicateLabelWord setFont:labelWord.font];
        [duplicateLabelWord setTextColor:[UIColor whiteColor]];
        [duplicateLabelWord setBackgroundColor:[UIColor clearColor]];
        duplicateLabelWord.numberOfLines = 1;
        duplicateLabelWord.textAlignment = NSTextAlignmentCenter;
        duplicateLabelWord.text = [initialString substringWithRange:NSMakeRange(1, [initialString length]-2)]; ;
        [self addSubview:duplicateLabelWord];
        
        if(labelWord.alpha == 1)
            labelWord.alpha = 0.5;
    }];
    /*if(labelWord.alpha == 1)
    labelWord.alpha = 0.5;*/
}
- (void)showCorrectAnswer{
    if(duplicateLabelWord){
        [UIView animateWithDuration:0.2 animations:^{
            labelWord.alpha = 0.0;
        } completion:^(BOOL finished) {
            
        }];   
        return;
    }
    
    if ([initialString rangeOfString:@"<"].location != NSNotFound) {
        labelWord.alpha = 1.0;
        [UIView animateWithDuration:0.2 animations:^{
            labelWord.alpha = 0.0;
        } completion:^(BOOL finished) {
            
            [labelWord.layer removeAllAnimations];
            [self.layer removeAllAnimations];
            duplicateLabelWord = [[UILabel alloc]initWithFrame:labelWord.frame];
            [duplicateLabelWord setFont:labelWord.font];
            [duplicateLabelWord setTextColor:[UIColor whiteColor]];
            [duplicateLabelWord setBackgroundColor:[UIColor clearColor]];
            duplicateLabelWord.numberOfLines = 1;
            duplicateLabelWord.textAlignment = NSTextAlignmentCenter;
            duplicateLabelWord.text = [initialString substringWithRange:NSMakeRange(1, [initialString length]-2)]; ;
            [self addSubview:duplicateLabelWord];
            labelWord.alpha = 0.0;
        }];
    }
}

- (void)resetLabel{
    labelWord.textColor = [UIColor whiteColor];
    labelWord.alpha = 1;
    if(duplicateLabelWord){
        [duplicateLabelWord removeFromSuperview];
        duplicateLabelWord = nil;
    }
}

- (void)fadeOut{
    if ([labelWord.text rangeOfString:@"_"].location != NSNotFound) {
         [self performSelector:@selector(hide) withObject:nil afterDelay:1.0];
    }
}
- (void)hide{
    labelWord.alpha = 0;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSLog(@"touchesBegan");
    [self userClick];
    return;
    [_vehicleDelegate textStartMove];
    // test wether the button is touched
    UITouch *touch = [touches anyObject];
    CGPoint touchBegan = [touch locationInView:self.superview.superview];
     NSLog(@"touchBegan %.2f %.2f",touchBegan.x,touchBegan.y);
        buttonTouched = YES;
    lastLocation = self.center;
    
}

- (CGPoint)takeFinalLocation{
    return startLocation;
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    //NSLog(@"touchesMoved");
    
    return;
    if(buttonTouched) {
        // do it here
        UITouch *touch = [touches anyObject];
        CGPoint touchMoved = [touch locationInView:self.superview];
        // NSLog(@"touchMoved %.2f %.2f",touchMoved.x,touchMoved.y);
        /*CGRect newFrame = CGRectMake(touchMoved.x-([self width]/2),
                                     touchMoved.y-([self height]/2),
                                     [self width],
                                     [self height]);*/
        CGRect newFrame = CGRectMake(touchMoved.x-(labelWord.width/2),
                                     touchMoved.y-([self height]/2),
                                     [self width],
                                     [self height]);
        self.frame = newFrame;
     
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    return;
    NSLog(@"touchesEnded");
    [_vehicleDelegate textStopMove];
    buttonTouched = NO;
    UITouch *touch = [touches anyObject];
    CGPoint touchMoved = [touch locationInView:self.superview];
    CGPoint nextPoint = [self.vehicleDelegate chekAnswer:labelWord.text andPoint:touchMoved];
    BOOL isCorrect = NO;
    
    if (CGPointEqualToPoint(nextPoint, CGPointZero)){
        nextPoint = startLocation;
        [_vehicleDelegate answerIsCorect:NO];
    }else{
        nextPoint = CGPointMake(nextPoint.x,nextPoint.y);
        [_vehicleDelegate answerIsCorect:YES];
        isCorrect = YES;
       
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        [self setOrigin:nextPoint];
    } completion:^(BOOL finished) {
        [self.vehicleDelegate setCorectAnswer:labelWord.text andPoint:touchMoved];
        if(isCorrect){
            labelWord.alpha = 0;
            if(labelWord.font.pointSize < 26){
                
            }
        }
    }];
    
}

- (void)userClick{
    
    if(isLock){
        return;
    }
    BOOL isCorrect = [self.vehicleDelegate isAnswerCorect:labelWord.text];
    CGPoint nextPoint = [self.vehicleDelegate chekAnswer:labelWord.text];
    
    [UIView animateWithDuration:0.2 animations:^{
        [self setOrigin:nextPoint];
        
    } completion:^(BOOL finished) {
        if(!isCorrect){
            [UIView animateWithDuration:0.4  delay:1.0 options:UIViewAnimationOptionTransitionNone animations:^{
                [self setOrigin:startLocation];
            } completion:^(BOOL finished) {
                 [self.vehicleDelegate showCurrentBox];
            }];
        }else{
            labelWord.alpha = 0;
            [self setOrigin:startLocation];
            labelWord.textColor = APP_GOLD_COLOR_NEW;
            self.userInteractionEnabled = NO;
            labelWord.alpha = 1;
            [self.vehicleDelegate showCurrentBox];
        }
        
    }];
    
    
}
- (void)lock{
    isLock = YES;
}
- (void)unlock{
    isLock = NO;
}

- (void)makeCurrentBox{
    labelWord.alpha = 1;
    labelWord.textColor = [UIColor colorWithRed:40.0/255.0 green:174.0/255.0 blue:229.0/255.0 alpha:1.0];//[UIColor blueColor];
}

- (NSString*)wordString{
    return [initialString  capitalizedString];
}

-(NSString*)getLabelWordText{
    return labelWord.text;
}

- (void)reset{
    labelWord.alpha = 1;
    labelWord.textColor = [UIColor whiteColor];
    [self setOrigin:startLocation];
    self.userInteractionEnabled = YES;
}

- (void)playAnswerSound:(id)sender {
    [AUDIO_MANAGER playRemoteAudioFile:[NSURL URLWithString:soundString]];
    //[AUDIO_MANAGER playSoundFromAudioFile:soundString];
}
@end
