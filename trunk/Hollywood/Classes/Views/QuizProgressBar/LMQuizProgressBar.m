//
//  LMQuizProgressBar.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 10/19/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "LMQuizProgressBar.h"

@implementation LMQuizProgressBar


- (instancetype)initWithFrame:(CGRect)frame withNumberOfDots:(NSInteger)dotsNumber {
    
    if (self = [super initWithFrame:frame]) {
        
        self.numberOfDots = dotsNumber;
        
        float spaceForLabels = 50.0;
        float spaceCorrectLabel = 30.0;
        float spaceAnswerNumbLabel = 20.0;
        
        UIImageView *progressLineImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 12, frame.size.width-spaceForLabels, 1)];
        [progressLineImage setImage:[UIImage imageNamed:@""]];
        [progressLineImage setBackgroundColor:[UIColor colorWithRed:112.0/255.0 green:102.0/255.0 blue:55.0/255.0 alpha:1.0]];
        [self addSubview:progressLineImage];
        
        //draw the dots
        UIImageView *dotImageView;
        
        float dotWidth = 9.0;
        float spaceBetweenDots = ((frame.size.width-spaceForLabels)-(dotsNumber*dotWidth))/(dotsNumber-1);
        
        for (NSInteger i=0; i<dotsNumber; i++) {
            dotImageView = [[UIImageView alloc] initWithFrame:CGRectMake(i*(spaceBetweenDots+dotWidth), 8, dotWidth, dotWidth)];
            
            if (i == 0)
                [dotImageView setImage:[UIImage imageNamed:@"current-dot"]];
            else
                [dotImageView setImage:[UIImage imageNamed:@"skipped-dot"]];
            dotImageView.tag = i+1;
            [self addSubview:dotImageView];
        }
        
        _correctAnswers = 0;
        self.correctAnswersLabel = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width-spaceForLabels, -2, spaceCorrectLabel, 25.0)];
        [self.correctAnswersLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:21.0]];
        [self.correctAnswersLabel setTextColor:[UIColor colorWithRed:5.0/255.0 green:154.0/255.0 blue:5.0/255.0 alpha:1.0]];
        self.correctAnswersLabel.text = @"0";
        [self.correctAnswersLabel setTextAlignment:NSTextAlignmentRight];
        [self addSubview:self.correctAnswersLabel];
        
        self.totalQuestionsLabel = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width-spaceAnswerNumbLabel, 0, spaceAnswerNumbLabel, 25.0)];
        [self.totalQuestionsLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12.0]];
        [self.totalQuestionsLabel setTextColor:[UIColor colorWithRed:241.0/255.0 green:227.0/255.0 blue:207.0/255.0 alpha:1.0]];
        self.totalQuestionsLabel.text = [NSString stringWithFormat:@"/%ld", dotsNumber];
        [self addSubview:self.totalQuestionsLabel];
    }
    
    return self;
}

#pragma mark - Change dot status

- (void)changeStatus:(NSInteger)status forDotWithIndex:(NSInteger)dotIndex {
    
//    current-dot
    
    UIImageView *currentDot = (UIImageView *)[self viewWithTag:dotIndex];
    
    if (currentDot && [currentDot isKindOfClass:[UIImageView class]]) {
        
        if (status == 1) {
            
            _correctAnswers++;
            self.correctAnswersLabel.text = [NSString stringWithFormat:@"%ld", _correctAnswers];
            [currentDot setImage:[UIImage imageNamed:@"correct-dot"]];
            
        }
        else if (status == 2) {
            
            [currentDot setImage:[UIImage imageNamed:@"skipped-dot"]];

        }
    }
    
    //next dot
    UIImageView *nextDot = (UIImageView *)[self viewWithTag:dotIndex+1];
    
    if (nextDot && [nextDot isKindOfClass:[UIImageView class]]) {
        
        [nextDot setImage:[UIImage imageNamed:@"current-dot"]];

    }
    
    self.currentDotIndex++;
}

@end
