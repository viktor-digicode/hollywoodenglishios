//
//  LessonProgressView.m
//  Hollywood
//
//  Created by Kiril Kiroski on 4/14/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "LessonProgressView.h"

@implementation LessonProgressView{
    __weak IBOutlet JEProgressView *currentProgresView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)configureUI{
    currentProgresView.progress = 0.0;
    self.backgroundColor = [UIColor clearColor];
    currentProgresView.trackTintColor = [UIColor clearColor];
    currentProgresView.tintColor = APP_GOLD_COLOR_NEW;
    //currentProgresView.trackTintColor = [UIColor whiteColor];
    
    
    UIImage *img = [UIImage imageNamed:@"progresBigLine"];
    UIImage *imgForTrac = [UIImage imageNamed:@"progresLine"];
    //img = [img resizableImageWithCapInsets:UIEdgeInsetsMake(0, 4, 0, 4)];
    currentProgresView.progressImage = img;
    //img = [img resizableImageWithCapInsets:UIEdgeInsetsMake(0, 4, 0, 4)];
    currentProgresView.trackImage = imgForTrac;
    
    
    int numOfDots = 5;
    float partDistance = (DEVICE_BIGER_SIDE - 2*INSTANCE_START_POSITION_X)/numOfDots;
    for (int i = 0; i <= numOfDots; i++) {
        UIView *tempDot = [[UIView alloc]initWithFrame:CGRectMake(-5+[currentProgresView x] + partDistance*i, [currentProgresView y]-5+4, 10, 10)];
        tempDot.backgroundColor = APP_GOLD_COLOR_NEW;
        tempDot.layer.cornerRadius = [tempDot height]/2;
        tempDot.tag = i;
        [self addSubview:tempDot];
    }
}

- (void)addProgres:(float)addValue{
    currentProgresView.progress += addValue;
}

- (void)changeProgressWithValue:(float)value {
    
    currentProgresView.progress = value;
    

}

@end
