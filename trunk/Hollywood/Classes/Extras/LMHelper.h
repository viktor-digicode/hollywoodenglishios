//
//  LMHelper.h
//  MissingWord
//
//  Created by Darko Trpevski on 12/11/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LMHelper : NSObject

+ (BOOL)isWordMatchingPattern:(NSString *)word
                      pattern:(NSString *)pattern;

+ (NSArray *)getSubstringsThatMatchPattern:(NSString *)pattern
                            originalString:(NSString *)originalString;

+ (NSArray *)splitQuestionIntoWords:(NSString *)question;
+ (NSArray *)placeholdersWords:(NSArray *)words;
+ (NSString *)removeCharsFromString:(NSString *)word;
+ (NSString *)removeSpecialChars:(NSString *)string;


+ (NSMutableArray *)shuffleArray:(NSMutableArray *)array;
+ (void)saveUserInfo:(NSString *)firstName
          secondName:(NSString *)secondName
               email:(NSString *)email;
+ (NSDictionary *)getUserInfo:(NSString *)msisdn;
+ (BOOL)isMailValid:(NSString *)email;

+ (void)saveModulCertificate:(NSInteger)modul
                       level:(NSInteger)level
                        user:(NSString *)user;

+ (BOOL)isModuleSaved:(NSString *)user
                modul:(NSInteger)modul
                level:(NSInteger)level;

+ (void)saveLevelCompletition:(NSString *)user
                         type:(NSInteger)type
                        level:(NSInteger)level;

+ (BOOL)isLevelCompletitionSaved:(NSString *)user
                            type:(NSInteger)type
                           level:(NSInteger)level;

+ (NSInteger)getNumberOfGestCredits;
+ (NSString *)getDeviceId;
#pragma mark - Save to NSUserDefaults

+ (void)saveValue:(NSString *)value forKey:(NSString *)key;
+ (id)getValueForKey:(NSString *)key;
@end
