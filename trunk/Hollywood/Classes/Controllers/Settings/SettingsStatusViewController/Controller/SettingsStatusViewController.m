//
//  SettingsStatusViewController.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/26/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "SettingsStatusViewController.h"

@interface SettingsStatusViewController ()<CacheManagerDelegate>
{
    __strong ProgressUserObject *currentProgressUserObject;
    __strong ProgressLevelObject *currentProgressLevelObject;
    __weak IBOutlet UIView *loadingView;
    __weak IBOutlet UIActivityIndicatorView *activityIndicatorLoading;
}

@end

@implementation SettingsStatusViewController

- (void)configureUI{
    
    [super configureUI];
    [self.navigationController.navigationBar setHidden:YES];
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
    
    [self.helpButton setTitle: Localized(@"setting_help") forState: UIControlStateNormal];
    [self.logoutButton setTitle:Localized(@"setting_logout") forState: UIControlStateNormal];
    
    self.userScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
    self.userVipScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];
    self.imageViewUserRank.image = [UIImage imageNamed:[NSString stringWithFormat:@"Awards_%02d", [[USER_MANAGER takeMaxRank] intValue] + 1]];
    [self setupUserStatus];
    
}

- (void)setupUserStatus
{
    if([USER_MANAGER isActive])
    {
        [self checkUserActivationKindOf];
    }
    else
    {
        self.statusLabel.text = [NSString stringWithFormat:@"Status: \n %@",[LABEL_MANAGER setLocalizeLabelText:@"setting_guest"] ];//@"Status: \n Guest";
        [self.logoutButton setHidden:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Orientations

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


#pragma mark - Action methods

- (IBAction)backButtonTapped:(id)sender
{
   [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)logoutButtonTapped:(id)sender
{
    FCAlertView *alertView = [[FCAlertView alloc] init];
    alertView.alertBackgroundColor = [UIColor colorWithRed:112.0f/255.0f green:102.0f/255.0f blue:55.0f/255.0f alpha:1.0];
    alertView.backgroundColor = [UIColor clearColor];
    alertView.colorScheme = [UIColor blackColor];
    alertView.doneButtonTitleColor = [UIColor whiteColor];
    alertView.hideSeparatorLineView = NO;
    alertView.blurBackground = NO;
    [alertView setSubTitle:Localized(@"logout_alert")];
    [alertView setTitle:@""];
    
    alertView.secondButtonTitleColor  = [UIColor whiteColor];
    alertView.secondButtonBackgroundColor = [UIColor blackColor];
    alertView.firstButtonTitleColor  = [UIColor whiteColor];
    alertView.firstButtonBackgroundColor = [UIColor blackColor];
    
    [alertView addButton:Localized(@"no_label") withActionBlock:^{
        
    }];
    
    [alertView doneActionBlock:^{
        activityIndicatorLoading.alpha = 1;
        [activityIndicatorLoading startAnimating];
        NSDictionary *dictHeaders = @{
                                      kProgressLessonOrder : @"0",
                                      kProgressRankOrder : @"0",
                                      kProgressLevelOrder : @"0",
                                      kProgressLessonCompleted :@"false",
                                      kProgressStarNumber : @"0",
                                      kProgressScore : [NSString stringWithFormat:@"%d", currentProgressUserObject.score]
                                      
                                      };
        [self addBaseActivityIndicator];
        [self chekInternet];
        
        __weak SettingsStatusViewController *weakSelf = self;
        [REQUEST_MANAGER getDataFor:kRequestSaveUserProgressionAPICall headers:dictHeaders withSuccessCallBack:^(NSDictionary *responseObject) {
            NSDictionary *responseData = [responseObject objectForKey:kApiObjectResponse];
            [weakSelf removeActivityIndicator];
            [DATA_MANAGER deleteLevelObject:0];
            [DATA_MANAGER deleteProgressData];
            [LABEL_MANAGER deleteLabellObjects:0];
            [USER_MANAGER logOutUser];
            [APP_DELEGATE buildLanguagesSelection];
            
        } andWithErrorCallBack:^(NSString *errorMessage) {
            [weakSelf removeActivityIndicator];
            //[activityIndicatorLoading stopAnimating];
        }];
        
        /*[DATA_MANAGER deleteLevelObject:0];
        [DATA_MANAGER deleteProgressData];
        [LABEL_MANAGER deleteLabellObjects:0];
        [USER_MANAGER logOutUser];
        [APP_DELEGATE buildLanguagesSelection];*/
    }];
    
    [alertView showAlertWithTitle:@"" withSubtitle:Localized(@"logout_alert") withCustomImage:nil withDoneButtonTitle:Localized(@"yes_label") andButtons:nil];
}

- (IBAction)helpButtonTapped:(id)sender
{
    HelpViewController *viewController = [HelpViewController new];
    self.navigationItem.title = @" ";
    [self.navigationController pushViewController:viewController animated:YES];
}
- (IBAction)vipButtonTapped:(id)sender
{
    id vipCourseObject = [DATA_MANAGER getVipCourseObject];
    if(vipCourseObject){
        //donwload resoureces
        NSArray *dataArray = [DATA_MANAGER getAllVipCategory];
        
        [CACHE_MANAGER setDelegate:self];
        [CACHE_MANAGER startDownloadingAndCachingResources:dataArray];
    }else{
        [self addBaseActivityIndicator];
        [self chekInternet];
        [REQUEST_MANAGER getDataFor:kRequestGetVipDataAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
            [self removeActivityIndicator];
            NSDictionary *responseDictionary = [responseObject objectForKey:kApiObjectResponse];
            [DATA_MANAGER makeVipCategoryCourseData:responseDictionary];
            [DATA_MANAGER saveData];
            [self vipButtonTapped:0];
        } andWithErrorCallBack:^(NSString *errorMessage) {
           [self removeActivityIndicator]; 
        }];
        //makeVipCategoryCourseData
    }

}


#pragma mark - Cache Manager Delegate

- (void)cacheManagerFinishedDownloading:(id)sender {
    
    NSLog(@"========= Images were downloaded =========");
    
    [APP_DELEGATE buildVipZoneStack];
    
}


#pragma mark - Support methods

- (void)checkUserActivationKindOf
{
    if([[USER_MANAGER userPhone] isEqualToString: kUserManagerDefault] || [USER_MANAGER userPhone] == nil)
    {
        self.statusLabel.text = [NSString stringWithFormat:@"Status: \n %@ \n \n %@",[LABEL_MANAGER setLocalizeLabelText:@"setting_active"] , [USER_MANAGER getPackagePurchasedDescription]];
        [self.logoutButton setHidden:YES];
    }
    else if(![[USER_MANAGER userPhone] isEqualToString: kUserManagerDefault] || [USER_MANAGER userPhone] != nil)
    {
        self.statusLabel.text = [NSString stringWithFormat:@"Status: \n %@ \n Phone no \n %@:",[LABEL_MANAGER setLocalizeLabelText:@"setting_active"] , [USER_MANAGER userPhone]];
        [self.logoutButton setHidden:NO];
    }
    else
    {
        self.statusLabel.text = [NSString stringWithFormat:@"Status: \n %@",[LABEL_MANAGER setLocalizeLabelText:@"setting_guest"] ];
        [self.logoutButton setHidden:YES];
    }
}

@end
