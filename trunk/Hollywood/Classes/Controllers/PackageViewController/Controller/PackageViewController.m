//
//  PackageViewController.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/14/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "PackageViewController.h"
#import <SSKeychain.h>

@interface PackageViewController ()<UICollectionViewDelegate, UICollectionViewDataSource,SKProductsRequestDelegate,SKPaymentTransactionObserver,CacheManagerDelegate>{
        SKProductsRequest *productsRequest;
        NSArray *validProducts;
    __strong ProgressUserObject *currentProgressUserObject;
    
}

@end

@implementation PackageViewController

- (void)configureUI{
    
    [super configureUI];
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    [self.backButton setHidden: self.isBackButtonHidden];
    
    [_collectionView registerNib:[UINib nibWithNibName:@"PackageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"PackageCollectionViewCell"];
    
    DLog(@"no_internet_access %@", Localized(@"no_internet_access"));
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    
    self.navigationController.navigationBarHidden = YES;

//    self.userVipPassesLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:15.0];
//    self.vipStaticTextLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:18.0];
    self.titleTextLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"select_package_title"];
    self.userVipScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];

    self.vipStaticTextLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"VIP"];
    
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kPackagesScreen action:@"Packages_Screen_Presented" additionalData:nil];
}

- (void)loadData{
    [self fetchAvailableProducts];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self.view.layer removeAllAnimations];
    [super viewDidDisappear:animated];
}


#pragma mark - UICollectionView DataSource & Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PackageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PackageCollectionViewCell" forIndexPath:indexPath];
//    if(indexPath.row == 0)
//    {
//        cell.titleLabel.text = [LABEL_MANAGER setLocalizeLabelText: kBasicLabel];
//        cell.timeLabel.text = [LABEL_MANAGER setLocalizeLabelText: kWeekLabel];
//        cell.priceLabel.text = [NSString stringWithFormat:@"%@ \n %@", [LABEL_MANAGER setLocalizeLabelText: kIAPweekPriceLabel] ,[LABEL_MANAGER setLocalizeLabelText: kPerWeekLabel]] ;
//        cell.backgroundImageView.image = [UIImage imageNamed:@"vinCopy3"];
//        [cell.badgeImageView setHidden:YES];
//    }
//    else if(indexPath.row == 1)
//    {
    cell.titleLabel.text = [LABEL_MANAGER setLocalizeLabelText: kBestLabel];
    cell.timeLabel.text = @"1";
    NSString *timeLabelText = [LABEL_MANAGER setLocalizeLabelText: kMonthLabel];
    cell.secondLineTime.text = [timeLabelText substringFromIndex:2];
    cell.priceLabel.text = [NSString stringWithFormat:@"%@", [LABEL_MANAGER setLocalizeLabelText:kIAPmonthPriceLabel]] ;  //[NSString stringWithFormat:@"$6.00 \n %@",[LABEL_MANAGER setLocalizeLabelText:@"per_week_label"]] ;//@"$00.00 per day";
    cell.badgeTitleImageView.image = [UIImage imageNamed:@"recomendado"];
    cell.backgroundImageView.image = [UIImage imageNamed:@"vinCopy2"];
//    }
//    else
//    {
//        cell.titleLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"most_profitable_label"];
//        cell.timeLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"months_label"];//@"3\n Months";
////        cell.priceLabel.text = [NSString stringWithFormat:@"%d", kParam028];
//        cell.priceLabel.text = [NSString stringWithFormat:@"$66.50 \n %@",[LABEL_MANAGER setLocalizeLabelText:@"per_week_label"]] ;//@"$00.00 per day";
//        cell.backgroundImageView.image = [UIImage imageNamed:@"vinCopy3"];
//    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake((SCREEN_HEIGHT * 0.44), (SCREEN_HEIGHT * 0.63));
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kPackagesScreen action:@"Package_8_Selected" additionalData:nil];
    [self buyInApp:indexPath.row];
    //[self makeUserWithOutInApp:indexPath.row];
}

- (void)makeUserWithOutInApp:(NSInteger)indexPackage {
    
    NSString *appName=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
    NSString *strApplicationUUID = [SSKeychain passwordForService:appName account:@"incoding"];
    strApplicationUUID  = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [SSKeychain setPassword:strApplicationUUID forService:appName account:@"incoding"];
    
    NSString *password = [validProducts[indexPackage] productIdentifier];
    NSString *packagePurchasedTitle = [validProducts[indexPackage] localizedTitle];
    [SSKeychain setPassword: password forService:appName account: kProductIdentifier];
    NSString *userAppId = [USER_MANAGER getUserApplicationId];
    NSString *courseID = [NSString stringWithFormat:@"%@",REQUEST_MANAGER.courseID];
    [SSKeychain setPassword:userAppId forService:appName account:@"userApplicationId"];
    [SSKeychain setPassword:courseID forService:appName account:@"courseID"];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *transactionDateString = [dateFormatter stringFromDate:[NSDate date]];
    [SSKeychain setPassword:transactionDateString forService:appName account: kTransactionDate];
    
    [[NSUserDefaults standardUserDefaults] setObject:packagePurchasedTitle forKey: kPackagePurchased];
    [USER_MANAGER setValidationForPackageOption:password andDate:[NSDate date]];
    
    [self makeUser];
}

#pragma mark - Action methods

- (IBAction)homeButtonTapped:(id)sender
{
    [APP_DELEGATE buildHomePageStack];
}

- (IBAction)vipButtonTapped:(id)sender
{
    id vipCourseObject = [DATA_MANAGER getVipCourseObject];
    if(vipCourseObject){
        //donwload resoureces
        NSArray *dataArray = [DATA_MANAGER getAllVipCategory];
        
        [CACHE_MANAGER setDelegate:self];
        [CACHE_MANAGER startDownloadingAndCachingResources:dataArray];
    }else{
        [self addBaseActivityIndicator];
        [self chekInternet];
        [REQUEST_MANAGER getDataFor:kRequestGetVipDataAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
            [self removeActivityIndicator];
            NSDictionary *responseDictionary = [responseObject objectForKey:kApiObjectResponse];
            [DATA_MANAGER makeVipCategoryCourseData:responseDictionary];
            [DATA_MANAGER saveData];
            [self vipButtonTapped:0];
        } andWithErrorCallBack:^(NSString *errorMessage) {
            [self removeActivityIndicator];
        }];
        //makeVipCategoryCourseData
    }
}

- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Make User
- (void)makeUser
{
    [self addBaseActivityIndicator];
    [self chekInternet];
    [REQUEST_MANAGER getDataFor:kRequestCreateUserAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
        [self removeActivityIndicator];
        //NSDictionary *responseDictionary = [responseObject objectForKey:kApiObjectResponse];
        [USER_MANAGER makeToBENativeUser];
        // [USER_MANAGER setUserApplicationId:[responseObject objectForKey:kUserApplicationId]];
        // NSString *appName=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
        //[SSKeychain setPassword:[USER_MANAGER getUserApplicationId] forService:appName account:@"userApplicationId"];
        [APP_DELEGATE buildHomePageStack];
    } andWithErrorCallBack:^(NSString *errorMessage) {
        [self removeActivityIndicator];
    }];
}

#pragma mark - Chek purchase

-(void)fetchAvailableProducts{
    
    [self addBaseActivityIndicator];
    NSSet *productIdentifiers = [NSSet
                                 setWithObjects: kProductName_2, nil];
    productsRequest = [[SKProductsRequest alloc]
                       initWithProductIdentifiers:productIdentifiers];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (BOOL)canMakePurchases
{
    return [SKPaymentQueue canMakePayments];
}
- (void)purchaseMyProduct:(SKProduct*)product{
    if ([self canMakePurchases]) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
    else{
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Purchases are disabled in your device"
                                      message:@""
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
   
    }
}

-(void)buyInApp:(int)order{
    if([validProducts count] > order)
    {
        [self addBaseActivityIndicator];
        [self purchaseMyProduct:[validProducts objectAtIndex:order]];
    }
}



-(IBAction)restore:(id)sender{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];

}

#pragma mark StoreKit Delegate

-(void)paymentQueue:(SKPaymentQueue *)queue
updatedTransactions:(NSArray *)transactions {

    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"Purchasing");
                break;
            case SKPaymentTransactionStatePurchased:
                [self sendServerSecsesfulTransaction:transaction.payment.productIdentifier];
                NSLog(@"Purchased ");
                if ([transaction.payment.productIdentifier isEqualToString:kProductName_2]) {
                    NSLog(@"Purchased ");
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"proverka Restored %@",transaction.payment.productIdentifier);
                if ([transaction.payment.productIdentifier isEqualToString:kProductName_2]) {
                    NSLog(@"Restored %@",transaction.payment.productIdentifier);
                    [self restoreTransaction:transaction];
                }
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"Purchase failed ");
                break;
            default:
                break;
        }
    }
}

-(void)productsRequest:(SKProductsRequest *)request
    didReceiveResponse:(SKProductsResponse *)response
{
     [self removeActivityIndicator];
    SKProduct *validProduct = nil;
    int count = (int)[response.products count];
    if (count>0) {
        
        
        //for(int i = 0; i < count; i++){
            //validProduct = [response.products objectAtIndex:i];
            //NSString *productIdentifier = validProduct.productIdentifier;
            //BOOL haveThisProduct = NO;
            /*for (NSDictionary *tag in priceListArray) {
                if([productIdentifier isEqualToString:[tag objectForKey:@"appleId"]]){
                    NSLog(@"Product productIdentifier: %@",productIdentifier);
                    haveThisProduct = YES;
                    break;
                }
            }*/
            /*if(!haveThisProduct){
             [priceListArray r
             }*/
        //}
        validProducts = response.products;
        validProduct = [response.products objectAtIndex:0];

        if ([validProduct.productIdentifier isEqualToString: kProductName_2]) {
            NSLog(@"Product Title: %@",validProduct.localizedTitle);
            NSLog(@"Product productIdentifier: %@",validProduct.productIdentifier);
            NSLog(@"Product price: %@",validProduct.price);
        }
        
           } else {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Not Available"
                                      message:@"No products to purchase"
                                      preferredStyle:UIAlertControllerStyleAlert];

        [self presentViewController:alert animated:YES completion:nil];
    }

}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"restoreTransaction...");
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];

}
///////
-(void)sendServerSecsesfulTransaction:(NSString*)productIdentifier{
    NSString *appName=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
    
    NSString *strApplicationUUID = [SSKeychain passwordForService:appName account:@"incoding"];
    if (strApplicationUUID == nil)
    {
        strApplicationUUID  = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [SSKeychain setPassword:strApplicationUUID forService:appName account:@"incoding"];
        [SSKeychain setPassword:productIdentifier forService:appName account:@"productIdentifier"];
        NSString *userAppId = [USER_MANAGER getUserApplicationId];
        NSString *courseID = [NSString stringWithFormat:@"%@",REQUEST_MANAGER.courseID];
        [SSKeychain setPassword:userAppId forService:appName account:@"userApplicationId"];
        [SSKeychain setPassword:courseID forService:appName account:@"courseID"];
        
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *transactionDateString = [dateFormatter stringFromDate:[NSDate date]];
        [SSKeychain setPassword:transactionDateString forService:appName account:kTransactionDate];
        [self makeUser];
    }
    
//    if([productIdentifier isEqualToString:kProductName_1]){
    
    if([productIdentifier isEqualToString:kProductName_2]){
        
//    }else if([productIdentifier isEqualToString:kProductName_3]){
        
    }
    
}


#pragma mark - Cache Manager Delegate
- (void)cacheManagerFinishedDownloading:(id)sender {
    
    NSLog(@"========= Images were downloaded =========");
    
    [APP_DELEGATE buildVipZoneStack];
    
}


@end
