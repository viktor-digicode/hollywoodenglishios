//
//  SettingsStatusViewController.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/26/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()<UIWebViewDelegate, CacheManagerDelegate>
{
    __strong ProgressUserObject *currentProgressUserObject;
    __strong ProgressLevelObject *currentProgressLevelObject;
    __weak IBOutlet UIView *loadingView;
}

@end

@implementation HelpViewController

- (void)configureUI
{
    [super configureUI];
    
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
    
    self.userScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
    self.userVipScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];
    
    
    self.imageViewUserRank.image = [UIImage imageNamed:[NSString stringWithFormat:@"Awards_%02d", [[USER_MANAGER takeMaxRank] intValue] + 1]];
    
    self.webView.backgroundColor = [APPSTYLE colorForType:@"New_Vehicle_Background"];
    [self performSelector:@selector(loadWebView) withObject:nil afterDelay:0.25];
}

- (void)loadWebView {
    
    [self addActivityIndicatorForHelpWebView];
    
    NSURL *url = [[NSURL alloc] initWithString: kHelpPageURL];
    
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:60.0];
    
    [self.webView loadRequest:urlRequest];
}


#pragma mark - UIWebViewDelegate Methods

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [self removeActivityIndicator];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [self removeActivityIndicator];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Orientations

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


#pragma mark - Action methods

- (IBAction)backButtonTapped:(id)sender
{
   [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)vipButtonTapped:(id)sender
{
    id vipCourseObject = [DATA_MANAGER getVipCourseObject];
    if(vipCourseObject){
        //donwload resoureces
        NSArray *dataArray = [DATA_MANAGER getAllVipCategory];
        
        [CACHE_MANAGER setDelegate:self];
        [CACHE_MANAGER startDownloadingAndCachingResources:dataArray];
    }else{
        [self addBaseActivityIndicator];
        [self chekInternet];
        [REQUEST_MANAGER getDataFor:kRequestGetVipDataAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
            [self removeActivityIndicator];
            NSDictionary *responseDictionary = [responseObject objectForKey:kApiObjectResponse];
            [DATA_MANAGER makeVipCategoryCourseData:responseDictionary];
            [DATA_MANAGER saveData];
            [self vipButtonTapped:0];
        } andWithErrorCallBack:^(NSString *errorMessage) {
           [self removeActivityIndicator]; 
        }];
        //makeVipCategoryCourseData
    }

}


#pragma mark - Cache Manager Delegate

- (void)cacheManagerFinishedDownloading:(id)sender {
    
    NSLog(@"========= Images were downloaded =========");
    
    [APP_DELEGATE buildVipZoneStack];
    
}

@end
