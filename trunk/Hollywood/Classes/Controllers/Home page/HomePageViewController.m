//
//  HomePageViewController.m
//  Hollywood
//
//  Created by Kiril Kiroski on 8/8/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "HomePageViewController.h"
#import "LMRequestManager.h"
#import "LessonObject+CoreDataClass.h"
#import "LoginViewController.h"
#import "PackageViewController.h"

@interface HomePageViewController ()<CacheManagerDelegate> {
    
    __weak IBOutlet UILabel *userScoreLabel;
    __weak IBOutlet UILabel *userVipPassesLabel;
    
    __weak IBOutlet UIImageView *imageViewUserProfile;
    
    __weak IBOutlet UIImageView *imageViewUserRank;
    
    __weak IBOutlet UIImageView *mainTeaserImage;
    
    __weak IBOutlet UICollectionView *mainCollectionView;
    __weak IBOutlet UIView *rssView;
    
    __strong ProgressUserObject *currentProgressUserObject;
    __strong ProgressLevelObject *currentProgressLevelObject;
    
    MWFeedParser *feedParser;
    NSMutableArray *parsedItems;
    
    __weak IBOutlet UIView *loadingView;
    __weak IBOutlet UIActivityIndicatorView *activityIndicatorLoading;
    __weak IBOutlet UILabel *loadingLabel;
    
    __weak IBOutlet UILabel *vipScoreLabel;
    __weak IBOutlet UILabel *vipStaticTextLabel;
    
    __weak IBOutlet UIButton *loginButton;
    BOOL isActivScreen;
}

@property (nonatomic, strong) NSMutableArray *lessonsInfoArray;

@end

@implementation HomePageViewController
@synthesize currentLesson;
@synthesize maximumLessonNumber;

- (void)configureUI{
    
    [super configureUI];
    DLog(@"no_internet_access %@", Localized(@"no_internet_access"));
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    
    
    currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
    maximumLessonNumber = currentProgressLevelObject.maxUnit;
    
    userScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
    userVipPassesLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];
    
    self.navigationController.navigationBarHidden = YES;
    [mainCollectionView registerNib:[UINib nibWithNibName:@"HomePageCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    //[self initialScroll];
    
    /*imageViewUserProfile.layer.cornerRadius = 4.0;
    imageViewUserProfile.layer.borderWidth = 1.0;
    imageViewUserProfile.layer.borderColor = [UIColor blackColor].CGColor;
    imageViewUserProfile.layer.masksToBounds = YES;*/
    
    imageViewUserProfile.image = [USER_MANAGER takeCurrentUserImage];
    
    [mainTeaserImage setHidden:YES];
    
    userScoreLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:30.0];
    userVipPassesLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:15.0];
    vipStaticTextLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:18.0];
    loginButton.hidden = YES;
    /*if ([USER_MANAGER isGuest]) {
     
     loginButton.hidden = NO;
     }
     else {
     
     loginButton.hidden = YES;
     }*/
    
}

- (void)loadData {

    
    isActivScreen = YES;
    [super loadData];
    [self updateUserData];
   /* if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled){
       // [self showAlert:@"You are subscribe, please charge you prepaid card and all content will be open."];
    }else if ([USER_MANAGER getUserStatus] == KAUserStatusCanceled){
        PackageViewController *controller = [PackageViewController new];
        [self.navigationController pushViewController:controller animated:YES];
    }*/
    [self addBaseActivityIndicator];
   
    [REQUEST_MANAGER getDataFor:kRequestCheckCourseVersionAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
        NSLog(@"#");
        [self removeActivityIndicator];
        NSDictionary *responseDictonary = [responseObject valueForKey:kApiObjectResponse];
        NSNumber *courseRevision = [responseDictonary valueForKey:kUserCourseRevision];
        NSNumber *courseVersion = [responseDictonary valueForKey:kUserCourseVersion];
        NSNumber *serverLabelVersion = [responseDictonary valueForKey:kUserLabelVersion];
        NSNumber *serverParameterVersion = [responseDictonary valueForKey:@"parameterVersion"];
        
        
        //        serverLabelVersion = 5;
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSNumber *userCourseRevision = [userDefault objectForKey:kUserCourseRevision];
        NSNumber *userCourseVersion= [userDefault objectForKey:kUserCourseVersion];
        NSNumber *userDefaultsLabelVersion = [userDefault objectForKey:kUserLabelVersion];
        NSNumber *userDefaultsParamVersion = [userDefault objectForKey:kParameterVersion];
        
        if( ([courseRevision longValue] != [userCourseRevision longValue]) ||
           ([courseVersion longValue] != [userCourseVersion longValue]) )
        {
            [userDefault setObject:courseRevision forKey:kUserCourseRevision];
            [userDefault setObject:courseVersion forKey:kUserCourseVersion];
            [userDefault setObject:serverLabelVersion forKey:kUserLabelVersion];
            [userDefault setObject:serverParameterVersion forKey:kParameterVersion];
            [self loadFirstLaunchAPICall];
        }
        if(userDefaultsLabelVersion != serverLabelVersion){
            [userDefault setObject:serverLabelVersion forKey:kUserLabelVersion];
            [self loadLabels];
        }
        if(userDefaultsParamVersion != serverParameterVersion){
           [userDefault setObject:serverParameterVersion forKey:kParameterVersion];
            [self loadParams];
        }
    } andWithErrorCallBack:^(NSString *errorMessage) {
        [self removeActivityIndicator];
    }];
    [self chekInternet];
}

-(void)loadLabels
{
    [self addBaseActivityIndicator];
    [REQUEST_MANAGER getDataFor:kRequestGetLablesForCourseAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
        [self removeActivityIndicator];
        [LABEL_MANAGER deleteLabellObjects:0];
        [DATA_MANAGER makeNewLabelsData:responseObject];
        [APP_DELEGATE buildHomePageStack];
    } andWithErrorCallBack:^(NSString *errorMessage) {
        [self removeActivityIndicator];
    }];
}

-(void)loadParams
{
    [self addBaseActivityIndicator];
    [REQUEST_MANAGER getDataFor:kRequestGetParametersForApplicationAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
        [self removeActivityIndicator];
        [PARAMETER_MANAGER deleteParameterObjects];
        [PARAMETER_MANAGER initializeStartData:responseObject];
        [APP_DELEGATE buildHomePageStack];
    } andWithErrorCallBack:^(NSString *errorMessage) {
        [self removeActivityIndicator];
    }];
}

-(void)loadFirstLaunchAPICall{
    [DATA_MANAGER deleteLevelObject:0];
    
    [LABEL_MANAGER deleteLabellObjects:0];//this is ok, Ace not remove!!!
    
    [self addBaseActivityIndicator];
    [self chekInternet];
    [REQUEST_MANAGER getDataFor:kRequestFirstLaunchAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
        [self removeActivityIndicator];
        [PARAMETER_MANAGER deleteParameterObjects];
        [PARAMETER_MANAGER initializeStartData: responseObject];
        [DATA_MANAGER initializeStartData:responseObject];
        [APP_DELEGATE buildHomePageStack];
    } andWithErrorCallBack:^(NSString *errorMessage) {
        [self removeActivityIndicator];
    }];
}
- (void)showAlert:(NSString*)stringForSubtitle{
    FCAlertView *alert = [[FCAlertView alloc] init];
    alert.alertBackgroundColor = [UIColor colorWithRed:112.0f/255.0f green:102.0f/255.0f blue:55.0f/255.0f alpha:1.0];
    
    [alert showAlertInView:self
                 withTitle:@""
              withSubtitle:stringForSubtitle
           withCustomImage:nil
       withDoneButtonTitle:nil
                andButtons:nil];
    
    alert.backgroundColor = [UIColor clearColor];
    alert.colorScheme = [UIColor blackColor];
    alert.doneButtonTitleColor = [UIColor whiteColor];
    alert.hideSeparatorLineView = YES;
    alert.blurBackground = NO;
}

- (void)updateUserData{
    //[activityIndicatorLoading startAnimating];
    [self addBaseActivityIndicator];
    [REQUEST_MANAGER getDataFor:kRequestGetUserDataAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
        [self removeActivityIndicator];
        NSDictionary *responseData = [responseObject objectForKey:kApiObjectResponse];
        NSDictionary *userDataView = [responseData objectForKey:kUserDataView];
        
        //To do: Save array of quizzes purchased befor
        NSArray *arrayPurchasedQuizes = [responseData objectForKey:kUserDataQuizList];
        if (arrayPurchasedQuizes) {
            
            [[NSUserDefaults standardUserDefaults] setObject:arrayPurchasedQuizes forKey:kUserDataQuizList];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        
        if(userDataView){
            [USER_MANAGER setUserData:userDataView];
        }
        currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
        currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
        
        currentProgressLevelObject.maxUnit = [[USER_MANAGER takeMaxLevel] intValue];
        if([userDataView objectForKey: kProgressScore]){
            currentProgressUserObject.score = [[userDataView objectForKey: kProgressScore] intValue];
        }
        if([userDataView objectForKey: kProgressVipPasses]){
            currentProgressUserObject.vipPasses =  [[userDataView objectForKey: kProgressVipPasses] intValue];
        }
        [DATA_MANAGER saveData];
        [self setScreenData];
        imageViewUserRank.image = [UIImage imageNamed:[NSString stringWithFormat:@"Awards_%02d", [[USER_MANAGER takeMaxRank] intValue]+1]];
        
        if ([USER_MANAGER getUserStatus] == KAUserStatusDisabled){
             [self showAlert:[LABEL_MANAGER setLocalizeLabelText:@"error_message_2"]];
            // [self showAlert:@"You are subscribe, please charge you prepaid card and all content will be open."];
        }
        else if ([USER_MANAGER getUserStatus] == KAUserStatusCanceled)
        {
            FCAlertView *alert = [[FCAlertView alloc] init];
            alert.alertBackgroundColor = [UIColor colorWithRed:112.0f/255.0f green:102.0f/255.0f blue:55.0f/255.0f alpha:1.0];
            
            [alert showAlertInView:self
                         withTitle:@""
                      withSubtitle:[LABEL_MANAGER setLocalizeLabelText:@"error_message_3"]
                   withCustomImage:nil
               withDoneButtonTitle:nil
                        andButtons:nil];
            
            alert.backgroundColor = [UIColor clearColor];
            alert.colorScheme = [UIColor blackColor];
            alert.doneButtonTitleColor = [UIColor whiteColor];
            alert.hideSeparatorLineView = YES;
            alert.blurBackground = NO;
            [alert doneActionBlock:^{
                [APP_DELEGATE buildPackageScreen];
//                PackageViewController *controller = [PackageViewController new];
//                [self.navigationController pushViewController:controller animated:YES];
            }];
        }
        
        
    } andWithErrorCallBack:^(NSString *errorMessage) {
        
        [self removeActivityIndicator];
        
    }];
}

- (void)setScreenData{
    NSArray *allLessonObject = [DATA_MANAGER takeAllLessonObject];
    NSMutableArray *tempArrayLessons = [allLessonObject mutableCopy];
    
    
    
    
    userScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
    userVipPassesLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];
    
    
    currentLesson = currentProgressLevelObject.currentUnit;
    maximumLessonNumber = currentProgressLevelObject.maxUnit;
    
    
    self.lessonsInfoArray = [NSMutableArray new];
    
    for (NSInteger i=0; i<=maximumLessonNumber; i++) {
        if([tempArrayLessons count] <=  i){
            break;
        }
        LessonObject *lessonObject = [tempArrayLessons objectAtIndex:i];
        
        if (i+1 == maximumLessonNumber+1) {
            [self.lessonsInfoArray addObject:@{
                                               @"lessonStatus":@(2),
                                               @"lessonNumber":@(i+1),
                                               @"lessonImage": lessonObject.lesson_home_page_image,
                                               @"leasonTeaserImage": lessonObject.lesson_teaser
                                               }];
            
        }
        else {
            
            [self.lessonsInfoArray addObject:@{
                                               @"lessonStatus":@(1),
                                               @"lessonNumber":@(i+1),
                                               @"lessonImage":lessonObject.lesson_home_page_image,
                                               @"leasonTeaserImage": lessonObject.lesson_teaser
                                               }];
        }
    }
    
    for (NSInteger i=maximumLessonNumber+1; i<[tempArrayLessons count]; i++) {
        
        LessonObject *lessonObject = [tempArrayLessons objectAtIndex:i];
        
        [self.lessonsInfoArray addObject:@{
                                           @"lessonStatus":@(3),
                                           @"lessonNumber":@(i+1),
                                           @"lessonImage": lessonObject.lesson_home_page_image,
                                           @"leasonTeaserImage": lessonObject.lesson_teaser
                                           }];
        
    }
    NSString *teaserImageUrl;
    if([self.lessonsInfoArray count] > maximumLessonNumber){
        teaserImageUrl = [[self.lessonsInfoArray objectAtIndex:maximumLessonNumber] objectForKey:@"leasonTeaserImage"];
    }else{
        teaserImageUrl = [[self.lessonsInfoArray objectAtIndex:maximumLessonNumber-1] objectForKey:@"leasonTeaserImage"];
    }
    [mainTeaserImage setImageWithURL:[NSURL URLWithString:teaserImageUrl] placeholderImage:[UIImage imageNamed:@""]];
    
    [mainTeaserImage setHidden:NO];
    
    [mainCollectionView reloadData];
    if([self.lessonsInfoArray count] > maximumLessonNumber){
        [mainCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:maximumLessonNumber inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
        
    }else{
        [mainCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:maximumLessonNumber-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    }
    //[self createFeedParser];
    [self loadRSS];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self.view.layer removeAllAnimations];
    feedParser.delegate = nil;
    [feedParser stopParsing];
    feedParser = nil;
    [super viewDidDisappear:animated];
    isActivScreen = NO;
}
#pragma mark Orientations
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskPortrait;
    
}

#pragma mark UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.lessonsInfoArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    HomePageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    //[cell setDataArray:dataArray[indexPath.row]];
    
    id lessonData = [self.lessonsInfoArray objectAtIndex:indexPath.row];
    [cell setLessonData:lessonData];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(204, 100);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    id lessonData = [self.lessonsInfoArray objectAtIndex:indexPath.row];
    NSInteger status = [[lessonData objectForKey:@"lessonStatus"] integerValue];
    
    if (status == 3) {
        
        [ALERT_MANAGER showCustomAlertWithTitle:nil andMessage:[LABEL_MANAGER setLocalizeLabelText:@"lesson_locked_message"]];
        return;
    }
    else {
        
        [ANALYTICS_MANAGER sendAnalyticsForEvent:kHomePageScreen action:@"Free_Lesson_Entered" additionalData:nil];
        currentProgressLevelObject.currentUnit = (int)[[lessonData objectForKey:@"lessonNumber"] integerValue]-1;
        LessonObject *currentLessonObject = [DATA_MANAGER takeLessonObjectObject:currentProgressLevelObject.currentUnit];
        
        if (status == 2) {
            
            if ([USER_MANAGER isUserDisabled]) {
                
                if (![currentLessonObject isFree]) {
                    
                    [self showAlert:[LABEL_MANAGER setLocalizeLabelText:@"error_message_2"]];
                    return;
                }
            }
            
            NSDictionary *dictHeaders = @{
                                          kLessonId : [NSString stringWithFormat:@"%d", [currentLessonObject.lesson_id intValue]]
                                          };
            NSArray *vehiclesDataArray = [DATA_MANAGER takeAllVehiclesObjectInLesson:currentProgressLevelObject.currentUnit];
            if( ([USER_MANAGER isActive] || ([USER_MANAGER isUserDisabled] && currentLessonObject.isFree)) && ([vehiclesDataArray count] == 0) ){
                [self addBaseActivityIndicator];
                [self chekInternet];
                [REQUEST_MANAGER getDataFor:kRequestGetLessonDataAPICall headers:dictHeaders withSuccessCallBack:^(NSDictionary *responseObject) {
                    [self removeActivityIndicator];
                    long statusNumber = [[responseObject objectForKey:kApiStatus] longValue];
                    if(statusNumber == -10){
                        [self showAlert:[LABEL_MANAGER setLocalizeLabelText:@"error_message_2"]];
                        
                    }else{
                        
                        NSDictionary *responseData = [responseObject objectForKey:kApiObjectResponse];
                        if(responseData){
                            [currentLessonObject setVehiclesData:responseData];
                        }
                        [APP_DELEGATE buildLessonStack];
                        [self removeActivityIndicator];
                    }
                } andWithErrorCallBack:^(NSString *errorMessage) {
                    if([errorMessage isEqualToString:@"Request failed: payment required (402)"]){
                        [self showAlert:[LABEL_MANAGER setLocalizeLabelText:@"error_message_2"]];
                    }
                    [self removeActivityIndicator];
                    
                }];
            }else{
                
                [APP_DELEGATE buildLessonStack];
            }
        }
        else {
            
            NSDictionary *dictHeaders = @{
                                          kLessonId : [NSString stringWithFormat:@"%d", [currentLessonObject.lesson_id intValue]]
                                          };
            NSArray *vehiclesDataArray = [DATA_MANAGER takeAllVehiclesObjectInLesson:currentProgressLevelObject.currentUnit];
            if (([USER_MANAGER isActive] || [USER_MANAGER isUserDisabled]) && ([vehiclesDataArray count] == 0) ){
                [self addBaseActivityIndicator];
                [self chekInternet];
                [REQUEST_MANAGER getDataFor:kRequestGetLessonDataAPICall headers:dictHeaders withSuccessCallBack:^(NSDictionary *responseObject) {
                    [self removeActivityIndicator];
                    long statusNumber = [[responseObject objectForKey:kApiStatus] longValue];
                    if(statusNumber == -10){
                        [self showAlert:[LABEL_MANAGER setLocalizeLabelText:@"error_message_2"]];
                        
                    }else{
                        
                        NSDictionary *responseData = [responseObject objectForKey:kApiObjectResponse];
                        if(responseData){
                            [currentLessonObject setVehiclesData:responseData];
                        }
                        [APP_DELEGATE buildLessonStack];
                        [self removeActivityIndicator];
                    }
                } andWithErrorCallBack:^(NSString *errorMessage) {
                    if([errorMessage isEqualToString:@"Request failed: payment required (402)"]){
                        [self showAlert:[LABEL_MANAGER setLocalizeLabelText:@"error_message_2"]];
                    }
                    [self removeActivityIndicator];
                    
                }];
            }else{
                
                [APP_DELEGATE buildLessonStack];
            }
        }
    }
}
#pragma mark - Inteface functions

- (IBAction)loginButtonPress:(id)sender {
    
    LoginViewController *viewController = [LoginViewController new];
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - Open lesson

- (IBAction)openLessonWithData:(id)sender {
    
    currentProgressLevelObject.currentUnit = (int)maximumLessonNumber;
    [APP_DELEGATE buildLessonStack];
    
}

- (IBAction)openUserZone:(id)sender {
    
    [APP_DELEGATE buildUserZoneStack];
}

- (IBAction)openVipZone:(id)sender {
    
    [ANALYTICS_MANAGER sendAnalyticsForEvent:kVipScreen action:@"Click_on_VIP" additionalData:nil];

    [self.view bringSubviewToFront:loadingView];
    loadingView.hidden = NO;
    //[activityIndicatorLoading startAnimating];
    [self addBaseActivityIndicator];
    id vipCourseObject = [DATA_MANAGER getVipCourseObject];
    if(vipCourseObject){
        //donwload resoureces
        NSArray *dataArray = [DATA_MANAGER getAllVipCategory];
        
        [CACHE_MANAGER setDelegate:self];
        [CACHE_MANAGER startDownloadingAndCachingResources:dataArray];
    }else{
        [self addBaseActivityIndicator];
        [self chekInternet];
        [REQUEST_MANAGER getDataFor:kRequestGetVipDataAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
            [self removeActivityIndicator];
            NSDictionary *responseDictionary = [responseObject objectForKey:kApiObjectResponse];
            [DATA_MANAGER makeVipCategoryCourseData:responseDictionary];
            [DATA_MANAGER saveData];
            [self openVipZone:0];
        } andWithErrorCallBack:^(NSString *errorMessage) {
            [self removeActivityIndicator];
        }];
        //makeVipCategoryCourseData
    }
    
}

#pragma mark - Cache Manager Delegate
- (void)cacheManagerFinishedDownloading:(id)sender {
    
    NSLog(@"========= Images were downloaded =========");
    
    [APP_DELEGATE buildVipZoneStack];
    
    [self removeActivityIndicator];
    loadingView.hidden = YES;
}


#pragma mark RSS
-(void)loadRSS{
    if(!isActivScreen){
        return;
    }
    
    [REQUEST_MANAGER getDataFor:kRequestGetRSSAPICall headers:nil withSuccessCallBack:^(NSDictionary *responseObject) {
        [self removeActivityIndicator];
        NSDictionary *responseData = [responseObject objectForKey:kApiObjectResponse];
        NSString *textData = [responseData objectForKey:kInstanceObjectText];
        
        UILabel *newLabel = [[UILabel alloc]initWithFrame:rssView.frame];
        newLabel.y = 0;
        newLabel.x = [rssView width];
        newLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
        newLabel.text = textData;
        [newLabel sizeToFit];
        [rssView addSubview:newLabel];
        [self showAnim:newLabel];
        
    } andWithErrorCallBack:^(NSString *errorMessage) {
        
        [self removeActivityIndicator];
        
    }];

}
-(void)createFeedParser{
    parsedItems = [[NSMutableArray alloc] init];
    // Create feed parser and pass the URL of the feed
    //NSURL *feedURL = [NSURL URLWithString:@"http://feeds.feedburner.com/thr/news"];
    NSURL *feedURL = [NSURL URLWithString:@"http://feeds.accesshollywood.com/AccessHollywood/LatestNews"];
    
    feedParser = [[MWFeedParser alloc] initWithFeedURL:feedURL];
    feedParser.delegate = self;
    feedParser.feedParseType = ParseTypeFull;//ParseTypeInfoOnly;//ParseTypeFull;
    feedParser.connectionType = ConnectionTypeAsynchronously;
    [feedParser parse];
}

#pragma mark MWFeedParserDelegate

- (void)feedParserDidStart:(MWFeedParser *)parser {
    //NSLog(@"Started Parsing: %@", parser.url);
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedInfo:(MWFeedInfo *)info {
    //NSLog(@"Parsed Feed Info: “%@”", info.title);
    self.title = info.title;
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedItem:(MWFeedItem *)item {
    //NSLog(@"Parsed Feed Item: “%@”", item.title);
    if (item) [parsedItems addObject:item];
}

- (void)feedParserDidFinish:(MWFeedParser *)parser {
    //NSLog(@"Finished Parsing%@", (parser.stopped ? @" (Stopped)" : @""));
    /*if([parsedItems count]){
     MWFeedItem *tempItem = [parsedItems objectAtIndex:0];
     tempItem.title = @"rwer rwerewr";
     }*/
    [self makeRssTextAnimation];
}

- (void)feedParser:(MWFeedParser *)parser didFailWithError:(NSError *)error {
    //NSLog(@"Finished Parsing With Error: %@", error);
    if (parsedItems.count == 0) {
        self.title = @"Failed"; // Show failed message in title
    } else {
        // Failed but some items parsed, so show and inform of error
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Parsing Incomplete"
                                                        message:@"There was an error during the parsing of this feed. Not all of the feed items could parsed."
                                                       delegate:nil
                                              cancelButtonTitle:@"Dismiss"
                                              otherButtonTitles:nil];
        [alert show];
    }
    //[self updateTableWithParsedItems];
}

- (void) makeRssTextAnimation{
    if([parsedItems count]){
        MWFeedItem *tempItem = [parsedItems objectAtIndex:0];
        [parsedItems removeObject:tempItem];
        UILabel *newLabel = [[UILabel alloc]initWithFrame:rssView.frame];
        newLabel.y = 0;
        newLabel.x = [rssView width];
        newLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
        newLabel.text = tempItem.title;
        [newLabel sizeToFit];
        [rssView addSubview:newLabel];
        [self showAnim:newLabel];
    }else{
        [self createFeedParser];
    }
    
}

- (void) showAnim:(UILabel*)newLabel {
    if(!newLabel){
        return;
    }
    int lastPoint = -[newLabel width]*2-[rssView width];
    int animationTime = -lastPoint*0.02;
    
    [UIView animateWithDuration:animationTime/2
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         newLabel.x = newLabel.x - [newLabel width]-([rssView width]/2);
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:animationTime/2
                                               delay:0
                                             options:UIViewAnimationOptionCurveLinear
                                          animations:^{
                                              
                                              newLabel.x = newLabel.x - [newLabel width]-([rssView width]/2);
                                              
                                          } completion:^(BOOL finished) {
                                              [newLabel removeFromSuperview];
                                          }];
                         [self loadRSS];
                        /* if(feedParser)
                             [self makeRssTextAnimation];*/
                     }];
    
}

@end
