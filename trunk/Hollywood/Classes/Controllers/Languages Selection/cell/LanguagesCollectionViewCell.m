//
//  LanguagesCollectionViewCell.m
//  Hollywood
//
//  Created by Kiril Kiroski on 7/12/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LanguagesCollectionViewCell.h"

@implementation LanguagesCollectionViewCell{
    
    __weak IBOutlet UIImageView *languageImageView;
    __weak IBOutlet UILabel *infoLabel;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setCellData:(id)lessonData{
    infoLabel.text = [(NSDictionary*)lessonData objectForKey:kLevelRanksRankName];
    NSString *imageName = [(NSDictionary*)lessonData objectForKey:kUserCode];
    
    languageImageView.image = [UIImage imageNamed:imageName];
}

-(void)getImageForLanguage{
    
}

@end
