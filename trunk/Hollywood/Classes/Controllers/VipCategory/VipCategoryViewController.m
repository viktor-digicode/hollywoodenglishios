//
//  VipCategoryViewController.m
//  Hollywood
//
//  Created by Kiril Kiroski on 9/29/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "VipCategoryViewController.h"
#import "ProgressUserObject.h"
#import "VipCategoryObject+CoreDataClass.h"
#import "VipQuizzesViewController.h"

@interface VipCategoryViewController ()<CacheManagerDelegate> {
    __weak IBOutlet UIView *topBarView;
    __weak IBOutlet UIView *questionBarView;
    __weak IBOutlet UILabel *questionBarLabel;
    __weak IBOutlet UILabel *vipScoreLabel;
    __weak IBOutlet UILabel *vipStaticTextLabel;

    __weak IBOutlet UILabel *titleLabel;

    __weak IBOutlet UICollectionView *mainCollectionView;
    
    __strong ProgressUserObject *currentProgressUserObject;
    int minFontSize;
    NSArray *dataArray;
}


@end

@implementation VipCategoryViewController

- (void)configureUI{
    [super configureUI];
    self.navigationController.navigationBarHidden = YES;
    topBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar_bg_up"]];
    questionBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar_bg_down"]];
    
    [mainCollectionView registerNib:[UINib nibWithNibName:@"VipCategoriCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
    titleLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:30.0];
    vipScoreLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:15.0];
    vipStaticTextLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:18.0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLabels) name:@"updateVipPasses" object:nil];
    self.imageViewUserRank.image = [UIImage imageNamed:[NSString stringWithFormat:@"Awards_%02d", [[USER_MANAGER takeMaxRank] intValue]+1]];
    dataArray = [NSArray new];
    dataArray = [DATA_MANAGER getAllVipCategory];
    
    //    [activityIndicatorView startAnimating];
    //    [CACHE_MANAGER setDelegate:self];
    //    [CACHE_MANAGER startDownloadingAndCachingResources:dataArray];
    
    [self refreshCollectionView];
    
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    [self updateLabels];
    
    titleLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"vip_zone_title"];
    questionBarLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"vip_zone_info"];
    vipStaticTextLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"vip_bar_label"];

}

- (void)loadData {
    [super loadData];
    
   /* dataArray = [NSArray new];
    dataArray = [DATA_MANAGER getAllVipCategory];
    
//    [activityIndicatorView startAnimating];
//    [CACHE_MANAGER setDelegate:self];
//    [CACHE_MANAGER startDownloadingAndCachingResources:dataArray];

    [self refreshCollectionView];

    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    [self updateLabels];*/
}

#pragma mark - Cache Manager Delegate
- (void)cacheManagerFinishedDownloading:(id)sender {
    
    NSLog(@"========= Images were downloaded =========");
    
    [self refreshCollectionView];
    
}

- (void)refreshCollectionView {

    dispatch_async(dispatch_get_main_queue(), ^ {
        [mainCollectionView reloadData];
        
        //[activityIndicatorView stopAnimating];

    });

}

- (void)updateLabels{
    //scoreLabel.text = [NSString stringWithFormat:@"%d",totalScore];
    vipScoreLabel.text = [NSString stringWithFormat:@"%d",currentProgressUserObject.vipPasses];
}

#pragma mark Interface functions

- (IBAction)goToHomePage:(id)sender {
    [AUDIO_MANAGER stopPlaying];
    [APP_DELEGATE buildHomePageStack];
}

- (IBAction)openUserZone:(id)sender {
    
    [APP_DELEGATE buildUserZoneStack];
}

#pragma mark UICollectionView
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    if(DEVICE_BIGER_SIDE > 568.0){
        if([dataArray count] == 5){
            return CGSizeMake(-10, 0);
        }else{
           return CGSizeMake(35, 0);
        }
    }
    return CGSizeMake(35, 0);
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [dataArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    VipCategoriCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    VipCategoryObject *tempOBj = [dataArray objectAtIndex:indexPath.row];
    [cell setVipCategoryData:tempOBj];

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /*if(DEVICE_BIGER_SIDE > 568.0){
       return CGSizeMake(101*1.175, 170*1.175);
    }*/
    if(DEVICE_BIGER_SIDE > 568.0){
        return CGSizeMake(119, 200);
    }
    return CGSizeMake(101, 170);
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   VipCategoryObject *tempOBj = [dataArray objectAtIndex:indexPath.row];
    if([tempOBj.availability boolValue]){
        
        [ANALYTICS_MANAGER sendAnalyticsForEvent:kVipScreen action:@"CategoryClick" additionalData:nil];

        VipQuizzesViewController *viewController = [VipQuizzesViewController new];
        VipCategoryObject *tempOBj = [dataArray objectAtIndex:indexPath.row];
        [viewController setVipCategoryObject:tempOBj];
        [self.navigationController pushViewController:viewController animated:YES];
    }

}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    //return UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    // Add inset to the collection view if there are not enough cells to fill the width.
    CGFloat cellSpacing = ((UICollectionViewFlowLayout *) collectionViewLayout).minimumLineSpacing;
    CGSize headerSize = ((UICollectionViewFlowLayout *) collectionViewLayout).headerReferenceSize;
    CGFloat headerSpacing = headerSize.width;
    CGFloat cellWidth = ((UICollectionViewFlowLayout *) collectionViewLayout).itemSize.width;
    NSInteger cellCount = [collectionView numberOfItemsInSection:section];
    CGFloat inset = (collectionView.bounds.size.width - (headerSpacing*2) - (cellCount * (cellWidth + cellSpacing))) * 0.5;
    inset = MAX(inset, 0.0);
    return UIEdgeInsetsMake(0.0, inset, 0.0, 0.0);
}

#pragma mark Orientations
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotate {
    
    return YES;
}

@end
