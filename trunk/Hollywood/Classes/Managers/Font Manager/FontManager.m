//
//  FontManager.m
//  Hollywood
//
//  Created by Kiril Kiroski on 4/14/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "FontManager.h"

@implementation FontManager{
    
}


+ (NSDictionary*)takeFontSize:(NSString*)currentString forVehicle:(KAVehiclesType)vehicleType{
    NSNumber *fontSize;
    NSNumber *lineNumber = @1;
    long charNumber = [currentString length];
    

    switch (vehicleType) {
        case KAClickToLearnVehiclesType:
            if(charNumber <= 6){
                lineNumber = @1;
                fontSize = @50;
            }else  if(charNumber <= 7){
                lineNumber = @1;
                fontSize = @40;
            }else  if(charNumber <= 8){
                lineNumber = @1;
                fontSize = @38;
            }else  if(charNumber <= 9){
                lineNumber = @1;
                fontSize = @34;
            }else  if(charNumber <= 10){
                lineNumber = @1;
                fontSize = @31;
            }else  if(charNumber <= 11){
                lineNumber = @1;
                fontSize = @28;
            }else  if(charNumber <= 12){
                lineNumber = @1;
                fontSize = @26;
            }else {
                lineNumber = @1;
                fontSize = @24;
            }
            break;
        case KAMultipleChoiceVehiclesType:
            if(charNumber < 10){
                lineNumber = @1;
                fontSize = @30;
            }else  if(charNumber == 10){
                lineNumber = @2;
                fontSize = @30;
            }else  if(charNumber == 11){
                lineNumber = @2;
                fontSize = @28;
            }else  if(charNumber == 12){
                lineNumber = @2;
                fontSize = @26;
            }else  if(charNumber == 13){
                lineNumber = @2;
                fontSize = @24;
            }else {
                lineNumber = @2;
                fontSize = @24;
            }
            break;
        case KASwipeVehicleSwipePictureType:
            lineNumber = @4;
            if(charNumber <= 20){
                fontSize = @60;
            }else  if(charNumber <= 24){
                fontSize = @52;
            }else  if(charNumber <= 28){
                fontSize = @44;
            }else  if(charNumber <= 32){
                fontSize = @38;
            }else  if(charNumber <= 36){
                fontSize = @31;
            }else  if(charNumber <= 40){
                fontSize = @28;
            }else  if(charNumber <= 44){
                fontSize = @26;
            }else {
                fontSize = @24;
            }
            break;
        case KASwipeVehicleSwipeTextType:
            lineNumber = @4;
            if(charNumber <= 20){
                fontSize = @60;
            }else  if(charNumber <= 24){
                fontSize = @52;
            }else  if(charNumber <= 28){
                fontSize = @44;
            }else  if(charNumber <= 32){
                fontSize = @38;
            }else  if(charNumber <= 36){
                fontSize = @31;
            }else  if(charNumber <= 40){
                fontSize = @28;
            }else  if(charNumber <= 44){
                fontSize = @26;
            }else {
                fontSize = @24;
            }
            break;
        case KAVoiceRecoginitionVehicleType:
            lineNumber = @3;
            if(charNumber <= 36){
                fontSize = @60;
            }else  if(charNumber <= 39){
                fontSize = @54;
            }else  if(charNumber <= 42){
                fontSize = @50;
            }else  if(charNumber <= 45){
                fontSize = @47;
            }else  if(charNumber <= 48){
                fontSize = @44;
            }else  if(charNumber <= 51){
                fontSize = @42;
            }else  if(charNumber <= 54){
                fontSize = @39;
            }else  if(charNumber <= 57){
                fontSize = @37;
            }else  if(charNumber <= 60){
                fontSize = @35;
            }else  if(charNumber <= 63){
                fontSize = @33;
            }else  if(charNumber <= 66){
                fontSize = @32;
            }else  if(charNumber <= 69){
                fontSize = @30;
            }else  if(charNumber <= 72){
                fontSize = @29;
            }else  if(charNumber <= 75){
                fontSize = @28;
            }else  if(charNumber <= 78){
                fontSize = @27;
            }else  if(charNumber <= 81){
                fontSize = @26;
            }else  if(charNumber <= 84){
                fontSize = @25;
            }else {
                fontSize = @24;
            }
            break;
        
            
            
        default:
            fontSize = @12;
    }
    fontSize = [NSNumber numberWithFloat:([fontSize floatValue]/2)];
    
    NSDictionary *fontDictionary = @{@"size":fontSize, @"line":lineNumber};
    return fontDictionary;
}

+ (NSDictionary*)takeFontSizeForQuestionArea:(NSString *)currentString {
    
    NSString *stringWithoutLink;
    if ([currentString rangeOfString:@"#<link>"].location == NSNotFound) {
        stringWithoutLink = currentString;
    } else {
        stringWithoutLink = [currentString stringByReplacingOccurrencesOfString:@"#<link>" withString:@""];
        stringWithoutLink = [stringWithoutLink stringByReplacingOccurrencesOfString:@"</link>" withString:@""];
        stringWithoutLink = [stringWithoutLink stringByReplacingOccurrencesOfString:@"<trans>" withString:@""];
        stringWithoutLink = [stringWithoutLink stringByReplacingOccurrencesOfString:@"</trans>#" withString:@""];

    }
    
    
    NSNumber *fontSize;
    NSNumber *lineNumber = @2;
    long charNumber = [stringWithoutLink length];
    
//    NSLog(@"Char number - %ld", charNumber);
    
    if(charNumber <= 70){
        lineNumber = @2;
        fontSize = @20;
    }else  if(charNumber > 70 && charNumber <= 74){
        lineNumber = @2;
        fontSize = @19;
    }else  if(charNumber > 74 && charNumber <= 78){
        lineNumber = @2;
        fontSize = @18;
    }else  if(charNumber > 78 && charNumber <= 82){
        lineNumber = @2;
        fontSize = @17;
    }else  if(charNumber > 82 && charNumber <= 88){
        lineNumber = @2;
        fontSize = @16;
    }else  if(charNumber > 88 && charNumber <= 94){
        lineNumber = @2;
        fontSize = @15;
    }else  if(charNumber > 94 && charNumber <= 100){
        lineNumber = @2;
        fontSize = @15;
    }else  if(charNumber > 100 && charNumber <= 108){
        lineNumber = @2;
        fontSize = @15;
    }else {
        lineNumber = @2;
        fontSize = @15;
    }
    
    
    NSDictionary *fontDictionary = @{@"size":fontSize, @"line":lineNumber};
    return fontDictionary;

}


-(void)showTempFont{
    
}

@end
