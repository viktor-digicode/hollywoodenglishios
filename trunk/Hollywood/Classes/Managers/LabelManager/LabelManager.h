//
//  LMLabelManager.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 6/21/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>

#define LABEL_MANAGER ((LabelManager *)[LabelManager sharedInstance])

@interface LabelManager : NSObject

- (NSString *)setLocalizeLabelText:(NSString *)text;
- (void)deleteLabellObjects:(int)labelOrder;
- (NSArray *)getArrayLabelManager;

@end


#ifdef LABELMANAGER_SINGLETON

@interface LabelManager ()
+ (id)sharedInstance;
@end

#define LABEL_MANAGER ((LabelManager *)[LabelManager sharedInstance])
#endif
