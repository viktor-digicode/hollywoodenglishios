//
//  ParameterManager.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 8/21/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParameterObject+CoreDataClass.h"

#define PARAMETER_MANAGER ((ParameterManager *)[ParameterManager sharedInstance])

@interface ParameterManager : NSObject

- (void)saveData;
- (void)deleteParameterObjects;
- (int)getParameterAttempts;
- (void)initializeStartData:(NSDictionary*)dictionary;
- (NSString *)getLocalizeParameterText:(NSString *)text;
- (NSArray *)getArrayParameterManager;

@end

#ifdef PARAMETERMANAGER_SINGLETON

@interface ParameterManager ()
+ (id)sharedInstance;
@end

#define PARAMETER_MANAGER ((ParameterManager *)[ParameterManager sharedInstance])
#endif

