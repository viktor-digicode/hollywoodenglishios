//
//  VocabManager.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 8/29/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VocabManager.h"

@implementation VocabManager

#ifdef VOCABMANAGER_SINGLETON



SINGLETON_GCD(VocabManager)
#endif




- (void)saveData
{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
            NSLog(@"You successfully saved your context.");
        } else if (error) {
            NSLog(@"Error saving context: %@", error.description);
        }
    }];
}

//- (void)initializeStartData:(NSDictionary*)dictionary
//{
//    NSDictionary *vocabDictonary = [dictionary valueForKey: kApiObjectVocabs];
//    
//    
//    for (NSDictionary *dict in vocabDictonary)
//    {
//        VocabsObject *importObject = [VocabsObject MR_createEntity];
////        [importObject setDataWithKey: dict];
//        
//        
////        [importObject setDataWithKey: dict.description andValue: [parameterValueDictionary valueForKey:dict.description]];
//        
//    }
//    
//    [self saveData];
//}

- (void)deleteVocabObjects
{
    NSArray *dataArray = [[VocabsObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
    
    for(int i = 0; i < [dataArray count]; i++)
    {
        VocabsObject *vocabObject = [dataArray objectAtIndex: i];
        [vocabObject MR_deleteEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    }
    
    [self saveData];
}

- (VocabsObject *)getVocabById:(NSNumber *)vocabID
{
    for(VocabsObject *vocab in [self getArrayVocabManager])
    {
        if([[vocab vocab_id] isEqualToNumber: vocabID])
        {
            return vocab;
        }
    }
    return nil;
}

- (VocabsObject *)getVocabForString:(NSString *)vocabText
{
    for(VocabsObject *vocab in [self getArrayVocabManager])
    {
        if([[vocab word] isEqualToString: vocabText])
        {
            return vocab;
        }
    }
    return nil;
}


- (NSArray *)getArrayVocabManager
{
    return [[VocabsObject MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
