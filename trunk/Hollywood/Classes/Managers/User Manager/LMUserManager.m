//  LMUserManager.m
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//


#import "LMUserManager.h"
#import <SSKeychain.h>
//#import "RageKAInAppPurchasesHelper.h"

@interface LMUserManager () {
    BOOL isLoggedInAutomatically;
    BOOL isLoggedIn;
    BOOL isMSISDNUser;
    NSString *tokenString;
    NSNumber *userStatus;
    NSNumber *userToken;
    
}

@property(strong, nonatomic) LMUser *user;

- (BOOL)lastLoginUserType;
- (NSString *)lastLoginConfigPath;

@end


@implementation LMUserManager


#ifdef USER_SINGLETON
SINGLETON_GCD(LMUserManager)
#endif


- (id)init
{
    self = [super init];
    if (self) {
        if([[[NSUserDefaults standardUserDefaults] objectForKey:kUserManagerInitialized] boolValue]){
            //if ([[NSUserDefaults standardUserDefaults] objectForKey:kUserManagerInitialized]) {
            if ([self lastLoginUserType]) {
                [self initializeWithUser];
            } else {
                NSString *uid_icloud = [SDCloudUserDefaults stringForKey:@"uid"];
                if (uid_icloud && ![uid_icloud isEqualToString:@""]) {
                    [self initializeWithNativeUser];
                }else{
                    [self initializeWithGuest];
                }
            }
        } else {
        }
    }
    return self;
}
- (void)logOutUser
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kLastValidMSISDNPath];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kLastValidMSISDN];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kLastValidUniqueID];
    [[NSUserDefaults standardUserDefaults] setObject:@NO forKey:kLastValidLoginUser];
    if(![self isNative]){
        [[NSUserDefaults standardUserDefaults] setObject:@NO forKey:kUserManagerInitialized];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)initializeWithNativeUser
{
    self.user = nil;
    NSString *cloud_uuid = [SDCloudUserDefaults stringForKey:kMonthlyPurchaseId];
    if(!cloud_uuid){
      cloud_uuid = [SDCloudUserDefaults stringForKey:@"uid"];
    }
    self.user = [[LMUser alloc] initNativeUser:cloud_uuid];
    [[NSUserDefaults standardUserDefaults] setObject:[SDCloudUserDefaults stringForKey:kMonthlyPurchaseId] forKey:kUserMSISDN];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kUserCode];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.user synchronizeNativeUserToFile];
}

- (void)initializeWithGuest
{
    
    //to do: default prefix for guest to be decided
    [self setOperatorPrefix:@"00"];
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *userApplicationID = [userDefault objectForKey:kUserApplicationID];
    
    self.user = [[LMUser alloc] initWithMSISDN:[[self defaultPrefix] stringByAppendingString:kUserManagerDefault]];
    NSString *appName=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
    NSString *productIdentifier = [SSKeychain passwordForService:appName account:@"productIdentifier"];
    if(productIdentifier){
        self.user.isNative = 1;
    }
    [userDefault setObject:self.user.msisdn forKey:kUserMSISDN];
    [userDefault setObject:@"" forKey:kUserCode];
    [userDefault synchronize];
    [self.user synchronizeToFile];
    self.user.status = KAUserGuest;
    if(userApplicationID){
        self.user.applicationUserID = userApplicationID;
    }
    [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kUserManagerInitialized];
}

- (void)initializeWithUser
{
    DLog(@" objectForKey:uid %@ ", [SDCloudUserDefaults stringForKey:@"uid"]);
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *userApplicationID = [userDefault objectForKey:kUserApplicationID];
    
    [self initializeWithUser:[[LMUser alloc] initWithMSISDN:[[NSUserDefaults standardUserDefaults] objectForKey:kLastValidMSISDN]]];
    [userDefault setObject:self.user.msisdn forKey:kUserMSISDN];
    [userDefault setObject:@"" forKey:kUserCode];
    [userDefault synchronize];
    if(userApplicationID){
        self.user.applicationUserID = userApplicationID;
    }
}
- (void)makeToBENativeUser
{
    self.user.isNative = 1;
    self.user.status = KAUserStatusActive;
}
- (void)initializeWithUser:(LMUser *)user
{
    self.user = user;
    [self.user synchronizeToFile];
}

- (void)setUserData:(NSDictionary*)tempData{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@YES forKey:kLastValidLoginUser];
    [self.user setUserData:tempData];
    
    ProgressUserObject *currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    currentProgressUserObject.score = [self.user.userScore intValue];
    currentProgressUserObject.vipPasses = [self.user.vipPasses intValue];
    [DATA_MANAGER saveData];
}

- (void)setUserMSISD:(NSString*)msisdn{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:msisdn forKey:kLastValidMSISDN];
    self.user.msisdn = msisdn;
}

- (BOOL)processAutologin
{
    
    if ([self isInitialized]) {
        return YES;
    }
    return NO;
}

- (BOOL)loginWithMSISDN:(NSString *)msisdn code:(NSString *)code
{
        return NO;
}
//
- (void)loginWithMSISDN:(NSString *)msisdn code:(NSString *)code WithCompletitionBlock:(IsLoginResultBlock)completitionBlock
{
    
}

//Dummy user login
- (void)loginDummyUser {
    
    self.user = [[LMUser alloc] initWithMSISDN:@"55555"];
    self.user.number = kUserManagerDefault;
    [self initializeWithUser:self.user];
    [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kUserManagerInitialized];
    [self.user synchronizeToFile];
    isLoggedIn = YES;

}


// system send token with sms
- (BOOL)takeLoginTokenWithMSISDN:(NSString *)msisdn
{
   
    return NO;
}

- (void)makeLoginTokenWithMSISDN:(NSString *)msisdn WithCompletitionBlock:(UserStatusResultBlock)completitionBlock
{
   
    
}

// only for test to see token on LMLoginCodeViewController
- (NSNumber *)showMeTokenForMSISDN:(NSString *)msisdn
{
   
    return @0;
}

// for setup billingDate
//and for login with tocken
- (void)loadUserValuesWithCompletitionBlock:(ManagerResultBlock)completitionBlock
{
    
}

// for native user registration
- (void)registerNativeUserValuesWithCompletitionBlock:(ManagerResultBlock)completitionBlock
{
    
}

// for native user billing
- (void)makeBillingForNativeUserWithCompletitionBlock:(ManagerResultBlock)completitionBlock
{
    
   
}
// for native user read status
- (void)loadNativeUserValuesWithCompletitionBlock:(ManagerResultBlock)completitionBlock
{
    
}
- (void)buyPurchases
{
    
}

- (BOOL)chekApiStatus:(int)currentStatus
{
    switch (currentStatus) {
        case KAApiTokenStatusSuccess:
            return YES;
            break;
            
        case KAApiInvalidToken:
            return NO;
            break;
            
        default:
            return NO;
            break;
    }
    
    return NO;
}

- (BOOL)AuthenticatedUserStatus:(int)currentUserStatus
{
    switch (currentUserStatus) {
        case KAUserStatusNotRegistered:
            return NO;
            break;
            
        case KAUserStatusActive:
            return YES;
            break;
            
        case KAUserStatusDisabled:
            return YES;
            break;
            
        case KAUserStatusCanceled:
            return NO;
            break;
            
        case KAUserStatusToBeCanceled:
            return YES;
            break;
            
        default:
            return NO;
            break;
    }
}

- (void)initProductIdentifier:(NSString *)productIdentifier
{
    self.productIdentifier = productIdentifier;
}

- (NSString *)getPurchasedPackage
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [[defaults objectForKey:@"packagePurchased"] description];
}

- (BOOL)chekUserStatus:(int)currentUserStatus
{
    return NO;
}

- (void)UserStatus
{
    DLog(@"USER STATUS:%@ ", [self statusDescription:self.user.status]);
}

- (KAUserStatus)getUserStatus
{
    return self.user.status;
}

- (NSString *)userMsisdn
{
    return self.user.msisdn;
}
- (NSString *)userMsisdnString
{
    if([self isGuest]){
        return @"GUEST";
    }
    return self.user.msisdn;
}

- (NSString *)userPhone
{
    return [self.user.msisdn substringFromIndex:2];
}

- (NSString *)getUserPurchasedPackage
{
   return @"";
}

- (NSString *)getUserApplicationId {
    if (![self.user.applicationUserID isKindOfClass:[NSString class]]){
        return [NSString stringWithFormat:@"%@",self.user.applicationUserID];
    }
    return self.user.applicationUserID;
}

- (void)setUserApplicationId:(NSString *)userAppId {
    
    self.user.applicationUserID = userAppId;
    if (![userAppId isKindOfClass:[NSString class]]){
         self.user.applicationUserID = [NSString stringWithFormat:@"%@",userAppId];
     }
    [[NSUserDefaults standardUserDefaults] setObject:self.user.applicationUserID forKey:kUserApplicationID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (BOOL)isAuthenticated
{
    return [self AuthenticatedUserStatus:self.user.status];
    //return [self AuthenticatedUserStatus:self.user.status] && (![self isNextBillingDatePassed]);
}

- (BOOL)isActive{
    return (self.user.status == KAUserStatusActive) ? YES : NO;
}
- (BOOL)isGuest
{
    return (self.user.status == KAUserGuest) ? YES : NO;
}
- (BOOL)isNative
{
    return self.user.isNative;
}

- (BOOL)isUserDisabled {
    return (self.user.status == KAUserStatusDisabled) ? YES : NO;
}

- (BOOL)isInitialized
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kUserManagerInitialized] boolValue];
}

- (BOOL)isNextBillingDatePassed
{
    if (!self.user.billingDate)
        return NO;
    return [[NSDate date] timeIntervalSinceDate:self.user.billingDate] > 0;
}

- (NSString*)isNextBillingDateDay
{
    if (!self.user.billingDate)
        return @"";
    //60*60*24 = 86400
    return [NSString stringWithFormat:@"%.0f",-1*  [[NSDate date] timeIntervalSinceDate:self.user.billingDate]/86400];
}
- (NSString*)nextBillingDateString
{
    if (!self.user.billingDate)
        return @"";
    //60*60*24 = 86400
    return [NSString stringWithFormat:@"%.0f",1+(-1*  [[NSDate date] timeIntervalSinceDate:self.user.billingDate]/86400)];
}
#pragma mark - User credits


- (BOOL)canOpenLesson
{
    //if([self isGuest])
    //  return YES;
    
    return (self.user.numberOfCredits > 0);
}

- (void)setNumberOfCredits:(NSInteger)tempCredits
{
    self.user.numberOfCredits = tempCredits;
}

- (NSInteger)numberOfCredits
{
    return self.user.numberOfCredits;
    // for camFind
}



- (void)takeCredit
{
    self.user.numberOfCredits--;
}



- (NSString *)takeNativeDateStringForSettings{
    NSDate *date = [SDCloudUserDefaults objectForKey:[kMonthlyPurchaseId stringByAppendingString:@"_"]];
    int daysToAdd = -1;
    NSDate *newDate1 = [date dateByAddingTimeInterval:60*60*24*daysToAdd];
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"dd/MM/yy"]; // 2012-12-11T13:26:40Z
    return [dateFormatter2 stringFromDate:newDate1];
}

- (NSString *)takeNativeDateStringFromDate{
    NSDate *date = [SDCloudUserDefaults objectForKey:[kMonthlyPurchaseId stringByAppendingString:@"_"]];
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"dd/MM/yy"]; // 2012-12-11T13:26:40Z
    return [dateFormatter2 stringFromDate:date];
}
- (NSString *)takeStringFromDate {
    NSDate *date = self.user.billingDate;
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"dd/MM/yy"]; // 2012-12-11T13:26:40Z
    return [dateFormatter2 stringFromDate:date];
}

#pragma mark - chek Last Login User
- (void)chekLastLoginUser:(NSString *)tempUserMSISDN
{
    if (self.user && [self.user.msisdn isEqualToString:tempUserMSISDN]) {
        DLog(@"Old User");
    } else {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:kLastValidMSISDN]) {
            DLog(@"NEW User");
            [self resetUserData];
        }
    }
}

- (void)resetUserData
{
}

#pragma mark - Helpers

- (NSString *)statusDescription:(KAUserStatus)status
{
    if([self isNative]){
        return Localized(kUserStatusIAP);
    }
    switch (status) {
        case KAUserGuest:
            return Localized(kUserStatusGuestString);
            break;
            
        case KAUserStatusNotRegistered:
            return Localized(kUserStatusNotRegistered);
            break;
            
        case KAUserStatusActive:
            return Localized(kUserStatusActiveString);
            break;
            
        case KAUserStatusToBeCanceled:
            return Localized(kUserStatusToBeDeactivatedString);
            break;
            
        case KAUserStatusCanceled:
            return Localized(kUserStatusDeactivatedString);
            break;
            
        case KAUserStatusDisabled:
            return Localized(kUserStatusDisabledString);
            break;
    }
    return @"";
}

- (id)getDateObjectFromString:(NSString *)dateString
{
    if(dateString==(id) [NSNull null] || [dateString length]==0 || [dateString isEqualToString:@""])
    {
        return nil;
    }
    if([dateString isEqual: [NSNull null]]){
        return nil;
    }
    NSString *newDate = [NSString stringWithFormat:@"20%@",dateString];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    //[dateFormatter setDateFormat:SERVER_DATETIME_FORMAT];
    return [dateFormatter dateFromString:newDate];
}

#pragma mark - In app
-(void)makeInApp{
}
#pragma mark - Private

- (NSString *)deviceIdentifier
{
    static NSString *deviceIdentifier = 0;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{ deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; });
    return deviceIdentifier;
}


#pragma mark - User Operator properties

- (void)setOperatorPrefix:(NSString *)prefix {
    
    self.userOperatorPrefix = prefix;
}

- (NSString *)defaultPrefix
{
    return self.userOperatorPrefix;
}

-(void)setOperatorSiteID:(NSInteger)siteID {
    
    self.userOperatorSiteID = 101;//siteID;
}

- (NSInteger)getUserSiteID {
    
    return self.userOperatorSiteID;
}

-(void)setOperatorProductID:(NSInteger)productID {
    
    self.userOperatorProductID = 17;//productID;
}

- (NSInteger)getUserProductID {
    
    return self.userOperatorProductID;
}

#pragma mark - Opertor logo and splash get/set

- (void)saveOpertorSplash:(NSString *)stringSplashIos {
    
    
}

- (void)saveOperatorLogoIos:(NSString *)stringLogoIos {
    
}


- (NSString *)getUserSplashIos {
    
    return @"";
}

- (NSString *)getUserLogoIos {
    
    return @"";
}

#pragma mark -

- (NSString *)defaultMSISDN
{
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

- (NSString *)defaultUniqueID
{
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

- (BOOL)lastLoginUserType
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kLastValidLoginUser] boolValue];
}

- (NSString *)loginConfigPath:(LMUser *)user
{
    NSString *group = (user.status == KAUserGuest) ? @"guest" : @"user";
    return [[NSString documentsDir] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.plist", group, self.user.msisdn]];
}

- (NSString *)lastLoginConfigPath
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:kLastValidMSISDNPath];
}
#pragma mark chek for nofications 
-(void)setLastLoginTime{
    if([self isActive]){
        NSDate* now = [NSDate date];
        [[NSUserDefaults standardUserDefaults] setObject:now forKey:kLastTimeActiveUserLoginDate];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
//we not use
-(BOOL)loginBefore3Days{
    NSDate* lastLoginDate = [[NSUserDefaults standardUserDefaults]  objectForKey:kLastTimeActiveUserLoginDate];
    if(!lastLoginDate){
        return NO;
    }
    //NSTimeInterval aa = [lastLoginDate timeIntervalSinceNow] ;
    if ([lastLoginDate timeIntervalSinceNow] < -1*D_DAY*7)
    {
        return YES;
        
    }
    else
    {
        return NO;
    }
}
-(NSInteger)userWeeklyUsage{
    //TODO: make Calculations
    return 35;
}

-(NSArray *)takeLevelData{
    return self.user.levelData;
}
-(NSNumber *)takeCurrentLevel{
    if(!self.user)
        return 0;
    if(!self.user.currentLevel)
        return 0;
    return self.user.currentLevel;
}



-(NSNumber *)takeMaxLevel{
    if(!self.user)
        return 0;
    if(!self.user.maxOrderedLesson)
        return 0;
    return self.user.maxOrderedLesson;
}

-(NSNumber *)takeMaxRank{
    if(!self.user)
        return 0;
    if(!self.user.maxOrderRank)
        return 0;
    return self.user.maxOrderRank;
}

-(NSString *)takeContentVersion{
    if(self.user && self.user.contentVersion)
        return self.user.contentVersion;
    
    return @"0";
}
#pragma mark new functions
-(UIImage*)takeCurrentUserImage{
    UIImage *userImage;
    userImage = [UIImage imageNamed:@"menuHamburger"];
    //userImage = [UIImage imageNamed:@"roki_profile"];
    return userImage;
}

- (void)setValidationForPackageOption:(NSString *)packageOption andDate:(NSDate *)onDatePurchased
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    NSDate *next = nil;
    
//    if([packageOption isEqualToString:kProductName_1])
//    {
//        [offsetComponents setMonth:3];
//        next = [calendar dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0];
//    }
    if([packageOption isEqualToString: kProductName_2])
    {
        if([[NSUserDefaults standardUserDefaults] objectForKey:kPackagePurchased] == nil)
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"Month Subscription" forKey: kPackagePurchased];
        }
        [offsetComponents setMonth:1];
        next = [calendar dateByAddingComponents:offsetComponents toDate:onDatePurchased options:0];
    }
//    else if([packageOption isEqualToString:kProductName_3])
//    {
//        [offsetComponents setWeekday:1];
//        next = [calendar dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0];
//    }
    else
    {
        NSLog(@"NO package activated");
    }
    [[NSUserDefaults standardUserDefaults] setObject:next forKey: kDateExpirationForActiveUser];
}

- (NSString *)getPackagePurchasedDescription
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *package = [defaults objectForKey: kPackagePurchased];
    
    if([package isEqualToString:@""] || package == nil)
    {
        NSString *appName=[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleNameKey];
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormatter dateFromString: [SSKeychain passwordForService: appName account: kTransactionDate]];
        [USER_MANAGER setValidationForPackageOption:[SSKeychain passwordForService: appName account: kProductIdentifier] andDate: date];
        return [defaults objectForKey: kPackagePurchased];
    }
    else
    {
        return package;
    }
}

- (NSDate *)getDateExpirationForActiveUser
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:kDateExpirationForActiveUser];
}

@end
