//
//  LMAnalyticsManager.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 10/4/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "LMAnalyticsManager.h"
#import <GoogleAnalytics/GAI.h>
#import <GoogleAnalytics/GAIFields.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>

@implementation LMAnalyticsManager

#ifdef ANALYTICS_SINGLETON
SINGLETON_GCD(LMAnalyticsManager)
#endif


#pragma mark - init Analytics

- (id)initWithProductName:(NSString *)productName {
    
    self = [super init];
    if (self) {

        //        [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelError];
        self.productName = productName;
        NSLog(@"#####  productName:%@", self.productName);
    }
    return self;
}

#pragma mark - Send screen

- (void)sendAnalyticsForScreen:(NSString *)screenName {
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:screenName];
    
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"zzz YYYY-MM-dd"];
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [tracker set:[GAIFields customDimensionForIndex:LMDIMENSION_REVISION] value:[defaults objectForKey:kUserCourseRevision]];
    [tracker set:[GAIFields customDimensionForIndex:LMDIMENSION_VERSION] value:[defaults objectForKey:kUserCourseVersion]];
    
    [tracker set:[GAIFields customDimensionForIndex:LMDIMENSION_USERID] value:[USER_MANAGER getUserApplicationId]];
    [tracker set:[GAIFields customDimensionForIndex:LMDIMENSION_TIMESTAMP] value:dateString];

    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)sendAnalyticsForSceeen:(NSString *)screenName withAction:(NSString *)action {
    
}

- (void)sendAnalyticsForEvent:(NSString *)eventName action:(NSString *)actionName additionalData:(NSString *)data {
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"zzz YYYY-MM-dd"];
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [tracker set:[GAIFields customDimensionForIndex:LMDIMENSION_REVISION] value:[defaults objectForKey:kUserCourseRevision]];
    [tracker set:[GAIFields customDimensionForIndex:LMDIMENSION_VERSION] value:[defaults objectForKey:kUserCourseVersion]];
    
    [tracker set:[GAIFields customDimensionForIndex:LMDIMENSION_USERID] value:[USER_MANAGER getUserApplicationId]];
    [tracker set:[GAIFields customDimensionForIndex:LMDIMENSION_TIMESTAMP] value:dateString];
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:eventName
                                                          action:actionName
                                                           label:data
                                                           value:nil] build]];
}


- (void)sendAnalyticsForEvent:(NSString *)eventName
                       action:(NSString *)actionName
                    labelName:(NSString *)label
                        value:(NSNumber *)value {
    
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"zzz YYYY-MM-dd"];
    NSString *dateString = [dateFormatter stringFromDate:currentDate];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [tracker set:[GAIFields customDimensionForIndex:LMDIMENSION_REVISION] value:[defaults objectForKey:kUserCourseRevision]];
    [tracker set:[GAIFields customDimensionForIndex:LMDIMENSION_VERSION] value:[defaults objectForKey:kUserCourseVersion]];
    
    [tracker set:[GAIFields customDimensionForIndex:LMDIMENSION_USERID] value:[USER_MANAGER getUserApplicationId]];
    [tracker set:[GAIFields customDimensionForIndex:LMDIMENSION_TIMESTAMP] value:dateString];

    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:eventName
                                                          action:actionName
                                                           label:label
                                                           value:value] build]];

}

#pragma mark - Events

- (void)sendCategorySelectedEvent:(NSInteger)categoryId {
    
    //Args to send
    
    NSDate *currentDate = [NSDate date];
    
    //Device id
    
    NSLog(@"Send event name: %@, TimeStamp: %@", kProductName, currentDate);
    
}

- (void)sendQuizSelectedEvent:(NSInteger)quizId withActionName:(NSString *)actionName {
    

    [self sendAnalyticsForEvent:kVipQuizzesScreen action:actionName additionalData:nil];
}

- (void)sendQuizVehiclePresented:(NSInteger)vehicleId withActionName:(NSString *)actionName {
    
    //Args to send
    
    NSDate *currentDate = [NSDate date];
    
    //Device id
    
    NSLog(@"Quiz selected: %@\nAction name: %@\nTimeStamp: %@\nVehicle ID: %ld", kProductName, actionName, currentDate, (long)vehicleId);
}

- (void)sendSwipeBetweenInstance:(NSString *)actionName {
    
    //Args to send
    
    NSDate *currentDate = [NSDate date];
    
    //Device id
    
    NSLog(@"Swipe gesture: %@\nAction name: %@\nTimeStamp: %@\n", kProductName, actionName, currentDate);

}

- (void)enterTheVehicleEvent:(NSString *)vehicleName {
    
    NSDate *currentDate = [NSDate date];
    
    //Device id
    
    NSLog(@"Swipe gesture: %@\nAction name: %@\nTimeStamp: %@\n", kProductName, vehicleName, currentDate);

}

- (void)leaveTheVehicleEvent:(NSString *)vehicleName otherData:(NSDictionary *)statsDictionary {
    
    NSDate *currentDate = [NSDate date];
    
    //Device id
    
    NSLog(@"Swipe gesture: %@\nAction name: %@\nTimeStamp: %@\n", kProductName, vehicleName, currentDate);

}

@end
