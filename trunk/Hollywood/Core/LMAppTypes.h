//
//  LMAppTypes.h
//  Hollywood
//
//  Created by Kiril Kiroski on 4/14/16.
//  Copyright © 2016 aa. All rights reserved.
//
#import <Foundation/Foundation.h>

typedef enum {
   
    KAVideoVehicleType = 1,
    KASwipeVehicleType = 2,
    KASwipeVehicleSwipePictureType = 21,
    KASwipeVehicleSwipeTextType = 22,
    KASwipeCarouselVehicleType = 3,
    KAFillMissingWordsVehicleType = 4,
    KASpellingVehicleType = 5,
    KAClickToLearnVehiclesType = 6,
    KAVoiceRecoginitionVehicleType = 7,
    KAMultipleChoiceVehiclesType = 8,
} KAVehiclesType;

typedef enum {
    
    KADefaultState = 0,
    KAPlayBeforeAudioState = 1,
    KAStarttState = 2,
    KAFeedbackState = 6,
    KAPlayAfterAudioState = 9,
    
} KAVehicleState;

typedef enum {
    KAUserGuest = -1,
    KAUserStatusNotRegistered = 8,
    KAUserStatusActive = 3,
    KAUserStatusDisabled = 4,
    KAUserStatusCanceled = 5,
    KAUserStatusToBeCanceled = 6,
} KAUserStatus;

typedef enum {
    KAApiTokenStatusSuccess = 0,
    KAApiInvalidToken = 1,
} KAApiTokenStatus;
