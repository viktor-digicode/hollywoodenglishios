//
//  Constants.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/24/16.
//  Copyright © 2016 aa. All rights reserved.
//

/* URLs */
#define STAGING 1

#if STAGING == 0
#define kApiBaseUrl @"http://hollywood.la-mark-il.com:8080/hollywoodenglish/api/"
#define kApiBaseaAplicationId @"40" //@"51"//
#define kApiBaseCourseMode  @"stage"

//#define kProductName_1  @"aa.com.hollywood123.month3subscription"
#define kProductName_2  @"com.lamark.Hollywood.monthsubscription"
//#define kProductName_3  @"aa.com.hollywood123.weeksubscription"

#endif

#if STAGING == 1
#define kApiBaseUrl @"http://newcms.kantoo.com:8080/hollywoodenglish/api/"
#define kApiBaseaAplicationId @"46"
#define kApiBaseCourseMode  @"production"//@"stage"

#endif

#define kProductName_2  @"com.lamark.Hollywood.monthsubscription"

/* WIDTH  &  HEIGHT*/
#define LM_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define LM_HEIGHT ([UIScreen mainScreen].bounds.size.height)

#define DEVICE_BIGER_SIDE (LM_WIDTH >LM_HEIGHT ? LM_WIDTH : LM_HEIGHT)
#define DEVICE_SMALL_SIDE (LM_WIDTH >LM_HEIGHT ? LM_HEIGHT : LM_WIDTH)

#define SCENE_SIZE_HEIGHT (DEVICE_BIGER_SIDE >= 568? 568:DEVICE_BIGER_SIDE)
#define SCENE_SIZE_WIDTH 320

#define INSTANCE_START_POSITION_X 44
#define SPACE_QUESTIONS_ANSWERS 20

/*data*/
#define SEARCH_TERM @"title"
#define SEARCH_PREDICATE @"course_name contains[c] %@" //@"title contains[c] %@"
#define LEVEL_SEARCH_PREDICATE @"level_name contains[c] %@" //@"title contains[c] %@"
//#define LABEL_TEXT_SEARCH_PREDICATE @"key contains[c] %@"   //label text contains in
//#define LABEL_TEXT_SEARCH_PREDICATE @"key LIKE[c] %@"
//#define LABEL_TEXT_SEARCH_PREDICATE @"key contains[c] %@"
#define TEXT_SEARCH_PREDICATE @"key == %@"



#define kApiObjectResponse   @"response"
#define kApiObjectLevels     @"levels"
#define kApiObjectLanguages  @"languages"

#define kIAPmonthPriceLabel @"IAP_month_price_label"
#define kIAPweekPriceLabel @"IAP_week_price_label"
#define kIAP3MonthsPriceLabel @"IAP_3_month_price_label"

#define kApiObjectLabels     @"labels"
#define kApiObjectAppParameters @"appParameters"
#define kApiObjectLabelList  @"labelList"
#define kApiObjectAchievements @"achievements"
#define kApiObjectVocabs @"vocabs"

#define kCourseObjectCourseSourceLanguage   @"course_source_language"
#define kCourseObjectCourseName   @"course_name"
#define kCourseObjectCourseId   @"id" //old was courseId 
#define kCourseObjectCourseLevels   @"levels"
#define kCourseObjectCourseDescription   @"course_description "

#define kCourseLevelsLevelId @"id" //old was @"level_id"
#define kCourseLevelsLevelDescription @"level_description"
#define kCourseLevelsLevelOrder @"level_order"
#define kCourseLevelsLevelRanks @"ranks"//old was  @"level_ranks"
#define kCourseLevelsLevelName @"level_name"

#define kLevelRanksRankName @"name"
#define kLevelRanksRankLessons @"lessons"
#define kLevelRanksRankLessonBackground @"backgroundImage"
#define kLevelRanksRankIcon @"icon"
#define kLevelRanksRankNumber @"rank_number" 
#define kLevelRanksRankTeaserText @"rank_teaser_text" 
#define kLevelRanksRankDiplomaDesign @"diplomaDesign"
#define kLevelRanksRankId @"id"
#define kLevelRanksRankAchievedAudio @"rank_achieved_audio" 
#define kLevelRanksRankRankAchievedText @"rank_rank_achieved_text" 

#define kRankLessonsLessonHomePageImage @"thumbnailImage"
#define kRankLessonsLessonId @"id"
#define kRankLessonsLessonTeaser @"teaserImage"
#define kRankLessonsLessonVehicles @"instances"
#define kRankLessonsLessonDescription @"lesson_description" 
#define kRankLessonsLessonName @"lesson_name" 
#define kRankLessonsInitial @"initial"
#define kLessonSummaryImageInitial @"summaryImage"
//free  

#define kProgressMaxLevel @"maxLevel"
#define kProgressCurrentLevel @"currentLevel"
#define kProgressScore @"score"
#define kProgressVipPasses @"vipPasses"
#define kProgressRank @"rank"

#define kProgressLessonOrder @"lessonOrder"
#define kProgressRankOrder @"rankOrder"
#define kProgressLevelOrder @"levelOrder"
#define kProgressLessonCompleted @"isLessonCompleted"
#define kProgressStarNumber @"stars"
#define kLessonId @"lessonId"

#define kBaseClassVipCourse  @"vip_course"
#define kVipCourseVipCategories  @"vipCategories"
#define kVipCategoriesCategoryId  @"id"
#define kVipCategoriesImageUrl  @"imageId"
#define kVipCategoriesCategoryName  @"name"
#define kVipCategoriesQuizList  @"quizzes"
#define kVipCategoriesDisplayOrder  @"display_order"
#define kVipCategoriesFirstQuizInstanceList  @"first_quiz_Instance_list"
#define kVipCategoriesImageName  @"imageId"
#define kVipCategoriesAvailability  @"availability"
#define kUserDataQuizList @"quizList"

#define kQuizListDescription @"description"
#define kQuizListOrderInCategory @"order_in_category" 
#define kQuizListQuizId @"id"
#define kQuizListQuizName @"quizName"
#define kQuizListAvEndDate @"availabilityEndDate"
#define kQuizListImageUrl @"image"
#define kQuizListPrice @"price" 
#define kQuizListAvailability @"availability" 
#define kQuizPurchased @"purchased" 
#define kQuizListAvStartDate @"availabilityStartDate"
#define kQuizListIsPromo @"isPromo"
#define kQuizListTeaser @"teaser" 
#define kQuizListImageName @"quizSummaryImage"
#define kQuizListInstanceList @"instances" 


#define kWordPattern @"\\<[[a-zA-Z0-9\\w\\s\\'’]'+]*\\>"
#define kVocabPattern @"\\[[[a-zA-Z0-9\\w\\s\\'’]'+]*\\]"
#define kUnderlineWord @"__________"

/*Colors*/
#define APP_GOLD_COLOR [UIColor colorWithRed:138.0/255.0 green:128.0/255.0 blue:72.0/255.0 alpha:1.0]
#define APP_GOLD_COLOR_NEW [UIColor colorWithRed:112.0/255.0 green:102.0/255.0 blue:55.0/255.0 alpha:1.0]
#define APP_CORRECT_COLOR color(67, 170, 0, 1.0)
#define APP_INCORRECT_COLOR color(255, 4, 0, 1.0)
#define APP_BLUE_COLOR color(40, 174, 229)

//#define APP_VOCAB_COLOR color(110, 181, 229, 1)
#define APP_VOCAB_COLOR color(255, 255, 255, 1)
#define APP_VOCAB_TRANSLATION_COLOR color(110, 181, 229, 1)
/* Singletons */

#define AUDIO_SINGLETON 1
#define USER_SINGLETON 1
#define ANALYTICS_SINGLETON 1
#define LABELMANAGER_SINGLETON 1
#define PARAMETERMANAGER_SINGLETON 1
#define ALERTMANAGER_SINGLETON 1
#define ACHIEVEMENT_SINGLETON 1
#define VOCABMANAGER_SINGLETON 1

/* Sounds */

#define kSoundTypeBackground @"SFX_Background"
#define kSoundTypeButtonTap @"SFX_ButtonTap"
#define kSoundTypeSwipe @"SFX_Swipe"
#define kSoundTypeError @"SFX_Error"
#define kSoundTypeSuccess @"SFX_Success"

#define kSoundTypeResults @"ButtonClickFX"
#define kSoundTypeDeletePicture @"PicDeletedFX"
#define kSoundTypeTakePicture @"TakePicFX"

#define kSoundFileType @"mp3"


#define kUserMSISDN       @"msisdn"
#define kUserApplicationID       @"applicationUserID"
#define kUserCode         @"code"
#define kUserDeviceId     @"device_id"
#define kSearchWords      @"words"
#define kPreferencesCheckedList @"CheckedList"
#define kPreferencesflag @"flag"
#define kWord      @"word"
#define kAudio @"audio"
#define kFree @"free"
#define kUserDataView @"userDataView"
#define kUserCourseID @"userCourseID"

#define kUserCourseRevision @"courseRevision"
#define kUserCourseVersion @"courseVersion"
#define kUserLabelVersion @"labelVersion"
#define kParameterVersion @"appParametersVersion"

#define kUserEmail          @"userEmail"
#define kUserPassword       @"userPassword"
#define kUserApplicationId  @"applicationUserID"

#define kUserQuizId          @"quizId"

#define kUserManagerInitialized @"user_manager_initialized"
#define kUserManagerDefault @"000000000"
#define kLastValidMSISDNPath @"last_msisdn"
#define kLastValidLoginUser @"last_valid_login_user"
#define kLastValidMSISDN @"last_valid_msisdn"
#define kLastValidUniqueID @"last_valid_unique_ID"
#define kLastValidUserStatus @"last_valid_user_status"
#define kLastValidBillingDate @"last_valid_user_billing_date"
#define kLastTimeActiveUserLoginDate @"lastTimeActiveUserLogin"
#define kStartAplicationDate @"startAplicationDate"
#define kLastSaveDateDate @"lastSaveDateDate"
#define kLastValidContentVersion @"last_valid_content_version"
#define kLastValidLevelData @"last_valid_level_data"
#define kLastValidCurrentLevel @"last_valid_current_level"
#define kApiStatus @"status"
/* Purchases */

#define kMonthlyPurchaseId @"com.lamark.FrenchCourseGeneric.monthly"
#define kMonthlyPurchaseIdShort @"com.lamark.FrenchCourseGeneric.monthly"
#define kKeywordPurchaseIdShort @"ios"


/* Constants */

#define kCamfindSearchesForGuest 5
#define kCamfindSearchesForUser 25
#define kCamfindSearchesForOther 0

#define kUserStatusGuestString @"Guest"
#define kUserStatusNotRegistered @"NotRegistered"
#define kUserStatusActiveString @"Active"
#define kUserStatusToBeDeactivatedString @"ToBeCanceled"
#define kUserStatusDeactivatedString @"Cancelled"
#define kUserStatusDisabledString @"Disabled"
#define kUserStatusIAP @"IAP"


/* VihecleObjects */

#define kVehicleObjectVideoTextInfo @"text_info"    //does not exist into json file
#define kVehicleObjectVideoHighQuality @"highQuality"
#define kVehicleObjectVideoLowQuality @"lowQuality"
#define kVehicleObjectVideoPreview @"video_preview" //does not exist into json file
#define kVehicleObjectVideoType @"videoType"
#define kVehicleObjectFreeVideoLength @"freeVideoLength"
#define kVehicleObjectVideo @"video"
#define kVehicleObjectInstanceAnswer @"instanceAnswers"
#define kVehicleObjectInstanceImage @"instanceImage"

#define kVehicleObjectInstance @"instance"
#define kVehicleObjectFillWholeSentence @"fillWholeSentence"
#define kVehicleObjectSentence @"sentence"
#define kVehicleObjectWords @"words"
#define kVehicleObjectTargetText @"targetText"
#define kVehicleObjectTargetAudio @"targetAudio"
#define kVehicleObjectMainImageId @"mainImageId"

/* InstanceObjects */

#define kInstanceObjectCorrectAnswer @"correctAnswer"
#define kInstanceObjectAnswerImage @"answerImage"
#define kInstanceObjectAnswerClickAudio @"answerClickAudio"
#define kInstanceObjectAfterClickImage @"afterClickImage"
#define kInstanceObjectAfterClickText @"afterClickText"
#define kInstanceObjectImage @"image"
#define kInstanceObjectIsCorrect @"isCorrect"
#define kInstanceObjectItemPostion @"itemPostion"
#define kInstanceObjectAnswerText @"answerText"
#define kInstanceObjectText @"text"
#define kInstanceObjectAfterClickAudio @"afterClickAudio"
#define kInstanceObjectCarouselSegmentQuestionType @"carouselSegmentQuestionType"
#define kInstanceObjectCarouselSegmentQuestionText @"carouselSegmentQuestionText"
#define kInstanceObjectCarouselSegmentQuestionAudio @"carouselSegmentQuestionAudio"
#define kInstanceObjectAfterAudio @"afterAudio"
#define kInstanceObjectDefaultImage @"defaultImage"     //does not exist into json file
#define kInstanceObjectTargetLanguageText @"sourceLanguageText"     //does not exist into json file
#define kInstanceObjectSourceLanguageText @"targetLanguageText"     //does not exist into json file


/* AchievementsObject */

#define kAchievementsObjectType @"achievementType"
#define kAchievementsObjectAfterImage @"afterImage"
#define kAchievementsObjectAchivmentText @"achivmentText"
#define kAchievementsObjectTeaserText @"teaserText"
#define kAchievementsObjectCriteria @"criteria"
#define kAchievementsObjectAward @"award"
#define kAchievementsArray @"achievementsArray"

/* VocabObject */

#define kVocabObjectVocabId @"vocabId"
#define kVocabWordAudio @"wordAudio"
#define kVocabNative @"nativeVocabs"


/* AppParameters */

#define kVoiceVehicleRecordingTimeout @"voiceVehicleRecordingTimeout"
#define kCorrectHighlightSeconds @"correctHighlightSeconds"
#define kIncorrectHighlightSeconds @"incorrectHighlightSeconds"
#define kQuizSummaryLowestPerformance @"quizSummaryLowestPerformance"
#define kTwoStarRequiredPercent @"twoStarRequiredPercent"
#define kMaxUnsubscribedUsers @"maxUnsubscribedUsers"
#define kEarnVipPassesPointsTarget @"earnVipPassesPointsTarget"
#define kQuizSummaryExcellentPerformance @"quizSummaryExcellentPerformance"
#define kGetReadyCountdownSeconds @"getReadyCountdownSeconds"
#define kMaxIncorrectTries @"maxIncorrectTries"
#define kToyyyShortcode @"ToyyyShortcode"
#define kQuizSummaryGoodPerformance @"quizSummaryGoodPerformance"
#define kCorrectAnswerScore @"correctAnswerScore"
#define kVipPassesEarnedOnPointsTarget @"vipPassesEarnedOnPointsTarget"
#define kIncorrectAnswerScore @"incorrectAnswerScore"
#define kFillMissingWordsShowCorrectAnswerSeconds @"fillMissingWordsShowCorrectAnswerSeconds"
#define kQuizSummaryLowPerformance @"quizSummaryLowPerformance"
#define kVocabPopupDisplaySeconds @"vocabPopupDisplaySeconds"
#define kOneStarRequiredPercent @"oneStarRequiredPercent"
#define kFirstLaunchVipPasses @"firstLaunchVipPasses"
#define kNoStarStepPercent @"noStarStepPercent"
#define kHideVideoProgressBarAfterSeconds @"hideVideoProgressBarAfterSeconds"
#define kQuizSummaryMediumPerformance @"quizSummaryMediumPerformance"
#define kFillMissingWordsMaxIncorrectTries @"fillMissingWordsMaxIncorrectTries"
#define kThreeStarRequiredPercent @"threeStarRequiredPercent"
#define kSpellingMaxIncorrectTries @"spellingMaxIncorrectTries"


/* Additional constants */

#define kDateExpirationForActiveUser @"dateExpirationForActiveUser"
#define kHelpPageURL @"http://hlw.kantoo.com/faq/index.html"

#define kPackagePurchased @"packagePurchased"
#define kTransactionDate @"transactionDate"
#define kProductIdentifier @"productIdentifier"


/* packages for IAP purchase */

#define kBestLabel @"best_label"
#define kBasicLabel @"basic_label"
#define kMostProfitable @"most_profitable_label"

#define kWeekLabel @"week_label"
#define kMonthLabel @"month_label"
#define kMonthsLabel @"months_label"

#define kPerWeekLabel @"per_week_label"

#define SCREEN_WIDTH    ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT    ([UIScreen mainScreen].bounds.size.height)
